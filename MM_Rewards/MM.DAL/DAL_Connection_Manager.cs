﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MM.DAL
{
    public static class DAL_Connection_Manager
    {
        #region Variables and Constants
        private static String str_Conn_String;
        private const string str_Conn_Name = "MM_Connection";
        #endregion

        #region Properties

        public static String MM_ConnectionString
        {
            get
            {
                if (str_Conn_String == null)
                {
                    str_Conn_String = ConfigurationManager.ConnectionStrings[str_Conn_Name].ConnectionString;
                }
                return str_Conn_String;
            }
        }
        #endregion

        #region Methods

        public static SqlConnection GetDBConnection()
        {
            SqlConnection MM_Connection;
            try
            {
                MM_Connection = new SqlConnection(MM_ConnectionString);
                MM_Connection.Open();
                return MM_Connection;
            }
            catch (SqlException)
            {
                throw new ArgumentException("Unable to open the connection");
            }
        }

        public static void Closeconnection(SqlConnection MM_Connection)
        {
            try
            {
                if ((MM_Connection != null) && (MM_Connection.State != ConnectionState.Closed))
                {
                    MM_Connection.Close();
                }
                MM_Connection = null;
            }
            catch (SqlException ex)
            {
                MM_Connection = null;
                throw new ArgumentException(ex.Message);
            }
            finally
            {
                MM_Connection = null;
            }
        }

        #endregion        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;


namespace MM.DAL
{
    public class DAL_Common
    {
        public DataSet DAL_GetData(string DAL_usp_Name)
        {
            try
            {
                return SqlHelper.ExecuteDataset(DAL_Connection_Manager.MM_ConnectionString, CommandType.StoredProcedure, DAL_usp_Name);
            }
            catch (SqlException sqlex)
            {
                throw new ArgumentException(sqlex.Message);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public DataSet DAL_GetData(string DAL_usp_Name, SqlParameter[] objDALParam)
        {
            try
            {
                return SqlHelper.ExecuteDataset(DAL_Connection_Manager.MM_ConnectionString, CommandType.StoredProcedure, DAL_usp_Name, objDALParam);
            }
            catch (SqlException sqlex)
            {
                throw new ArgumentException(sqlex.Message);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public DataSet DAL_GetDataN(string DAL_usp_Name, SqlParameter[] objDALParam)
        {
            try
            {
                DataSet ds = new DataSet();
                using (SqlConnection sqlcon = new SqlConnection(DAL_Connection_Manager.MM_ConnectionString))

                {
                   
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = sqlcon;
                    cmd.CommandText = DAL_usp_Name;
                    cmd.CommandTimeout = 3600;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddRange(objDALParam);

                    
                    SqlDataAdapter sqlAdap = new SqlDataAdapter(cmd);
                    sqlAdap.Fill(ds);
                }
                return ds;
            }
            catch (SqlException sqlex)
            {
                throw new ArgumentException(sqlex.Message);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public object DAL_SaveData(string DAL_usp_Name, SqlParameter[] objDALParam, char DAL_ExecutionType = 'N')
        {
            try
            {
                if (DAL_ExecutionType == 'S')
                {
                    return SqlHelper.ExecuteScalar(DAL_Connection_Manager.MM_ConnectionString, CommandType.StoredProcedure, DAL_usp_Name, objDALParam);
                }
                else
                {
                    return SqlHelper.ExecuteNonQuery(DAL_Connection_Manager.MM_ConnectionString, CommandType.StoredProcedure, DAL_usp_Name, objDALParam);
                }
            }
            catch (SqlException sqlex)
            {
                throw new ArgumentException(sqlex.Message);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public object DAL_SaveData(string DAL_usp_Name, SqlParameter[] objDALParam, SqlTransaction objDALTrans, char DAL_ExecutionType = 'N')
        {
            try
            {
                if (DAL_ExecutionType == 'S')
                {
                    return SqlHelper.ExecuteScalar(objDALTrans, CommandType.StoredProcedure, DAL_usp_Name, objDALParam);
                }
                else
                {
                    return SqlHelper.ExecuteNonQuery(objDALTrans, CommandType.StoredProcedure, DAL_usp_Name, objDALParam);
                }
            }
            catch (SqlException sqlex)
            {
                throw new ArgumentException(sqlex.Message);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static DataSet GetDataFromSQL(string strSQL)
        {
            string connectionStr = GetConnString();
            try
            {
                return SqlHelper.ExecuteDataset(connectionStr, CommandType.Text, strSQL);
            }
            catch (SqlException sqlex)
            {
                throw new ArgumentException(sqlex.Message);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private static string GetConnString()
        {
            return ConfigurationManager.ConnectionStrings["MM_Connection"].ConnectionString;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MM.DAL;
using System.Data.SqlClient;

namespace Budget_Automation
{
   public class Program
    {
        static int CountIndirect = 0;
        static int CountDirect = 0;

        //'Swaraj Engines Limited.' add this data seprately each time
        static void Main(string[] args)
        {
          try
            {

                DataSet dsEmail = new DataSet();
                DAL_Common objCommon = new DAL_Common();
                dsEmail = objCommon.DAL_GetData("usp_GetHODForBudget");//usp_GetHODForBudgetGCO
                foreach (DataRow dr in dsEmail.Tables[0].Rows)
                {
                    int TotalCount = 0;
                    int Budget = 0;
                    int BudgetPerHead = 0;
                    int BudgetFinal = 0;

                    string TokenNumber = dr["TokenNumber"].ToString();
                    string EmployeeLevelName_ES = dr["EmployeeLevelName_ES"].ToString();
                    //First Level
                                        //if ((EmployeeLevelName_ES.Equals("President")) || (EmployeeLevelName_ES.Equals("L1-Strategic"))
                                        //   || (EmployeeLevelName_ES.Equals("L2-Strategic")))
                    if ((EmployeeLevelName_ES == "President") || (EmployeeLevelName_ES == "L1-Strategic") || (EmployeeLevelName_ES == "L2-Strategic"))
                    {
                        TotalCount = 0;
                        CountDirect = 0;
                        CountIndirect = 0;
                        Budget = 0;
                        BudgetPerHead = 280;
                        BudgetFinal = 0;

                        CountDirect = GetDirectCount(TokenNumber);
                        TotalCount = GetReportiesCount(TokenNumber);
                        CountIndirect = TotalCount - CountDirect;

                        if (TotalCount >= 1)
                        {
                            Budget = TotalCount * BudgetPerHead;

                            if (Budget < 140000)
                            {
                                Budget = 140000;
                            }
                            if (Budget > 280000)
                            {
                                Budget = 280000;
                            }
                            if (Budget % 500 == 0)
                            {
                                BudgetFinal = Budget;
                            }
                            else
                            {
                                BudgetFinal = Budget;
                                int check = Budget % 500;
                                if (check == 100)
                                {
                                    BudgetFinal = Budget + 400;
                                }
                                if (check == 200)
                                {
                                    BudgetFinal = Budget + 300;
                                }
                                if (check == 300)
                                {
                                    BudgetFinal = Budget + 200;
                                }
                                if (check == 400)
                                {
                                    BudgetFinal = Budget + 100;
                                }
                            }

                        }
                    }//second Level                  
                    else if ((EmployeeLevelName_ES=="L2-Executive") || (EmployeeLevelName_ES=="L3-Executive"))
                    {
                        TotalCount = 0;
                        CountDirect = 0;
                        CountIndirect = 0;
                        Budget = 0;
                        BudgetPerHead = 280;
                        BudgetFinal = 0;

                        CountDirect = GetDirectCount(TokenNumber);
                        TotalCount = GetReportiesCount(TokenNumber);
                        CountIndirect = TotalCount - CountDirect;
                        if (TotalCount >= 1)
                        {
                            Budget = TotalCount * BudgetPerHead;

                            if (Budget < 28000)
                            {
                                Budget = 28000;
                            }
                            if (Budget > 140000)
                            {
                                Budget = 140000;
                            }
                            if (Budget % 500 == 0)
                            {
                                BudgetFinal = Budget;
                            }
                            else
                            {
                                BudgetFinal = Budget;
                                int check = Budget % 500;
                                if (check == 100)
                                {
                                    BudgetFinal = Budget + 400;
                                }
                                if (check == 200)
                                {
                                    BudgetFinal = Budget + 300;
                                }
                                if (check == 300)
                                {
                                    BudgetFinal = Budget + 200;
                                }
                                if (check == 400)
                                {
                                    BudgetFinal = Budget + 100;
                                }
                            }
                        }

                    }//Third Level
                    else if ((EmployeeLevelName_ES=="L3-Department Head") || (EmployeeLevelName_ES=="L4-Department Head") ||
                        (EmployeeLevelName_ES=="L5-Department Head"))
                         {
                        //if ((EmployeeLevelName_ES.Equals("L3-Department Head")) || (EmployeeLevelName_ES.Equals("L4-Department Head")) ||
                        //    (EmployeeLevelName_ES.Equals("L5-Department Head")) || (EmployeeLevelName_ES.Equals("L5-Managerial")) ||
                        //    (EmployeeLevelName_ES.Equals("L6-Managerial")) || (EmployeeLevelName_ES.Equals("L7-Managerial")))

                        TotalCount = 0;
                        CountDirect = 0;
                        CountIndirect = 0;
                        Budget = 0;
                        BudgetPerHead = 1200;
                        BudgetFinal = 0;

                            CountDirect = GetDirectCount(TokenNumber);
                            TotalCount = GetReportiesCount(TokenNumber);
                            CountIndirect = TotalCount - CountDirect;
                            if (TotalCount >= 1)
                            {
                                Budget = TotalCount * BudgetPerHead;
                                BudgetFinal = Budget;
                            }

                         }//Fourth Level
                    else if ((EmployeeLevelName_ES == "L5-Managerial") || (EmployeeLevelName_ES == "L6-Managerial") || (EmployeeLevelName_ES == "L7-Managerial"))
                    {
                        TotalCount = 0;
                        CountDirect = 0;
                        CountIndirect = 0;
                        Budget = 0;
                        BudgetPerHead = 1000;
                        BudgetFinal = 0;

                        CountDirect = GetDirectCount(TokenNumber);
                        TotalCount = GetReportiesCount(TokenNumber);
                        CountIndirect = TotalCount - CountDirect;
                        if (TotalCount >= 1)
                        {
                            Budget = TotalCount * BudgetPerHead;
                            BudgetFinal = Budget;
                        }
                    }
                        //Saving Data to Review Budget
                    if (TotalCount >= 0)// || Budget > 0)  //afs
                        {
                        SqlParameter[] param = new SqlParameter[13];
                        param[0] = new SqlParameter("@TokenNumber", dr["TokenNumber"].ToString());
                        param[1] = new SqlParameter("@FirstName", dr["FirstName"].ToString());//afs
                        param[2] = new SqlParameter("@LastName", dr["LastName"].ToString());
                        param[3] = new SqlParameter("@CompanyName", dr["CompanyCode"].ToString());
                        param[4] = new SqlParameter("@BusinessUnit", dr["BusinessUnit"].ToString());
                        param[5] = new SqlParameter("@Division", dr["DivisionName_PA"].ToString());                     
                        param[6] = new SqlParameter("@Levels", dr["EmployeeLevelName_ES"].ToString());
                        param[7] = new SqlParameter("@DirectReportees", CountDirect);
                        param[8] = new SqlParameter("@InDirectReportees", CountIndirect);
                        param[9] = new SqlParameter("@ReporteesCount", TotalCount);
                        param[10] = new SqlParameter("@BudgetPerHead", BudgetPerHead);
                        param[11] = new SqlParameter("@TotalBudget", Budget);
                        param[12] = new SqlParameter("@FinalBudget", BudgetFinal);
                       param[13] = new SqlParameter("@IsUpdated", 0);
                        //param[14] = new SqlParameter("@OldBudget", 0);
                        //param[15] = new SqlParameter("@CreatedDate", DateTime.Now);
                        //param[16] = new SqlParameter("@BudgetUpdatedDate", null);
                        //param[17] = new SqlParameter("@BudgetUpdatedBy", '0');


                        //objCommon.DAL_SaveData("usp_InsertReviewBudget", param);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private static int GetDirectCount(string p)
        {
            DataSet dsSubemployeerw = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@HODToken", p);
            dsSubemployeerw = objCommon.DAL_GetData("usp_GetReporteesCount", param); //AfS
//            dsSubemployeerw = objCommon.DAL_GetData("usp_GetReporteesCountGCO", param);
            CountDirect = dsSubemployeerw.Tables[0].Rows.Count;
            return CountDirect;
        }
        
        private static int GetReportiesCount(string p)
        {
            DataSet dsSubemployeerw = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@HODToken", p);

            dsSubemployeerw = objCommon.DAL_GetData("usp_GetReporteesCount", param); //AFS
//            dsSubemployeerw = objCommon.DAL_GetData("usp_GetReporteesCountGCO", param);
            CountIndirect = CountIndirect + dsSubemployeerw.Tables[0].Rows.Count;
//            Console.WriteLine(CountIndirect);
            if (dsSubemployeerw.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSubemployeerw.Tables[0].Rows)
                {
                    GetReportiesCount(dr["TokenNumber"].ToString());
                    
                }
            }
            return CountIndirect;
        }
      

    }
}


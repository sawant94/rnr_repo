﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using MM.DAL;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace MAS_UploadUtility
{
    class SendMail
    {
        public bool sendMail(string strFromID, string strSenderName, string strToID, string strRecipientName, string strCCID, string strSubject, string strMessage)
        {
            bool blnSuccess = false;
            MailMessage mailMsg = new MailMessage();
            MailAddress fromAddr = new MailAddress(strFromID, strSenderName);

            mailMsg.IsBodyHtml = true;
            mailMsg.From = fromAddr;
            //mailMsg.CC = CCAddr;
            mailMsg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            strToID = strToID.Replace(';', ',');
            strCCID = strCCID.Replace(';', ',');
            mailMsg.To.Add(strToID);
            mailMsg.Subject = strSubject;
            mailMsg.Body = strMessage;

            string SMTPServer = ConfigurationSettings.AppSettings.Get("SMTP");
            SmtpClient objSmtpClient = new SmtpClient(SMTPServer);
            try
            {
                objSmtpClient.Send(mailMsg);
                blnSuccess = true;
            }
            catch (Exception GeneralEx)
            {
                throw new ArgumentException(GeneralEx.Message);
            }
            finally
            {
                objSmtpClient = null;
                mailMsg = null;
                fromAddr = null;
            }
            return blnSuccess;
        }
    }
}

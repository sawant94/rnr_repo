﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using MM.DAL;
using System.IO;
using System.Reflection;
namespace MAS_UploadUtility
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Upload Started");
            LogData("");
            LogData("----------------------------START----------------------------");
            UploadData();
            LogData("-----------------------------END-----------------------------");
            LogData("");
            Console.WriteLine("Upload Completed");
        }
        #region Send Mail
        private static void SendMail(string strBodyPart, int intsuccessCounter, int interrCounter)
        {
            SendMail mail = new SendMail();
            try
            {
                if (mail.sendMail(
                ConfigurationSettings.AppSettings["MailFrom"].ToString(),
                "EMPLOYEE UPLOAD UTILITY",
                ConfigurationSettings.AppSettings["MailTo"].ToString(),
                "REWARDS ADMINISTRATOR",
                "",
                "EMPLOYEE FILE UPLOAD ",
                strBodyPart + " SUCCEEDED: " + intsuccessCounter.ToString() + "<BR/><BR/>" + " FAILED: " + interrCounter.ToString()))
                {
                    LogData("\t Mail sent to Rewards Administrator [ " + ConfigurationSettings.AppSettings["MailTo"].ToString() + " ] ", "\t Mail sent to Rewards Administrator [ " + ConfigurationSettings.AppSettings["MailTo"].ToString() + " ] ");
                }
            }
            catch (Exception lobjException)
            {
                LogData("<BR>Mail Not Send");
                LogData("<BR>Message :- " + lobjException.Message.ToString());
                LogData("<BR>Error :- " + lobjException.StackTrace.ToString());
            }
        }
        #endregion
        #region Upload Data from Excel to Database
        public static void UploadData()
        {
            string thePath = ConfigurationSettings.AppSettings["ExcelPath"].ToString();
            string conn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + thePath + ";Extended Properties=Excel 12.0;";
            DataTable dtExcel = new DataTable();
            DataTable dtLevel = new DataTable();
            DataTable dtBudget = new DataTable();
            int errCounter = 0;
            int successCounter = 0;
            string BodyPart = "";
            try
            {
                dtLevel = GetLevelsData();
                dtBudget = GetBudgetGreaterThanZeroData();
                using (OleDbConnection connection = new OleDbConnection(conn))
                {
                    connection.Open();
                    OleDbCommand command = new OleDbCommand("select * from [Employee Details$A2:BA65000]", connection);
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {
                        dtExcel.Load(dr);
                        dtExcel.Columns.Add("LEVEL", typeof(String));
                        dtExcel.Columns.Add("Delete", typeof(String));
                        foreach (DataColumn dc in dtExcel.Columns) // trim column names
                        {
                            dc.ColumnName = dc.ColumnName.Trim();
                        }
                        foreach (DataRow dr1 in dtExcel.Rows) // trim string data
                        {
                            foreach (DataColumn dc in dtExcel.Columns)
                            {
                                if (dc.DataType == typeof(string))
                                {
                                    object o = dr1[dc];
                                    if (!Convert.IsDBNull(o) && o != null)
                                    {
                                        dr1[dc] = o.ToString().Trim();
                                    }
                                }
                            }
                        }
                        foreach (DataRow drE in dtExcel.Rows)
                        {
                            try
                            {
                                if (SysVal((drE["Personnel Area"] ?? string.Empty).ToString(), (drE["Employee Group"] ?? string.Empty).ToString(), (drE["Email"] ?? string.Empty).ToString(), (drE["F13"] ?? string.Empty).ToString()) == true) // ,(drE["F13"] ?? string.Empty).ToString() added by priya 3/8/2016
                                {
                                    drE["Delete"] = "N";
                                }
                                else if (CheckTokenValidation((drE["F2"] ?? string.Empty).ToString()) == true)
                                {
                                    drE["Delete"] = "Y";
                                }
                                else if (CheckValidation((drE["Name"] ?? string.Empty).ToString()) == true || CheckValidation((drE["First Name"] ?? string.Empty).ToString()) == true || CheckValidation((drE["Last Name"] ?? string.Empty).ToString()) == true)
                                {
                                    drE["Delete"] = "Y";
                                }
                                foreach (DataRow drLevel in dtLevel.Rows)
                                {
                                    if ((drE["F36"] ?? string.Empty).ToString().Trim() == drLevel["Code"].ToString().Trim())
                                    {
                                        drE["LEVEL"] = drLevel["LEVEL"].ToString();
                                    }
                                }
                                if ((drE["LEVEL"] ?? string.Empty).ToString() != string.Empty)
                                {
                                    //if (Convert.ToInt32(drE["LEVEL"]) > 19)
                                    //Changed By Archana K. on 31/05/2016
                                    if (Convert.ToInt32(drE["LEVEL"]) > 27)
                                    {
                                        drE["Delete"] = "Y";
                                    }
                                }
                                if ((drE["F2"] ?? string.Empty).ToString() != string.Empty)
                                {
                                    foreach (DataRow drBudgetAmt in dtBudget.Rows)
                                    {
                                        if ((drE["F2"] ?? string.Empty).ToString().Trim() == drBudgetAmt["TokenNumber"].ToString().Trim())
                                        {
                                            if (Convert.ToInt32(drE["LEVEL"]) > 8)
                                            {
                                                drE["LEVEL"] = "8";
                                            }
                                        }
                                    }
                                }
                                if ((drE["Employee Date of Joining"] ?? string.Empty).ToString() != string.Empty)
                                {
                                    drE["Employee Date of Joining"] = drE["Employee Date of Joining"].ToString().Replace(".", "-");
                                }
                                if ((drE["Date of Birth"] ?? string.Empty).ToString() != string.Empty)
                                {
                                    drE["Date of Birth"] = drE["Date of Birth"].ToString().Replace(".", "-");
                                }
                                if (drE["Delete"].ToString().Trim() == string.Empty)
                                {
                                    drE["Delete"] = "Y";
                                }
                            }
                            catch (Exception lobjException)
                            {
                                LogData("1) TOKEN NUMBER :- " + (drE["F2"] ?? string.Empty).ToString());
                                LogData(lobjException.Message.ToString() + "<BR>");
                                LogData(lobjException.StackTrace.ToString() + "<BR>");
                            }
                        }
                        DataView dv = new DataView(dtExcel);
                        dv.RowFilter = "Delete = 'N'";
                        DataTable dtMasInsert = dv.ToTable();
                        BodyPart += "------------------------------------------------------------" + "<BR/><BR/>";
                        BodyPart += "Rows found: " + dtMasInsert.Rows.Count.ToString() + "<BR/><BR/>";
                        foreach (DataRow drMasInserts in dtMasInsert.Rows)
                        {
                            try
                            {
                                //if (drMasInserts["F2"].ToString().Trim() == "200200")
                                //{
                                //    Console.Write("asdfasdf");
                                //}
                                int j = 0;
                                j = InsertMasRecord(drMasInserts);
                                successCounter = successCounter + 1;
                            }
                            catch (Exception lobjExceptions)
                            {
                                LogData("2) TOKEN NUMBER :- " + (drMasInserts["F2"] ?? string.Empty).ToString());
                                LogData(lobjExceptions.Message.ToString() + "<BR>");
                                LogData(lobjExceptions.StackTrace.ToString() + "<BR>");
                                errCounter = errCounter + 1;
                            }
                        }
                    }
                }
                try
                {
                    try
                    {
                        var file1 = new FileInfo(thePath);
                        var to = ConfigurationSettings.AppSettings["ExcelPathTo"].ToString();
                        if (!Directory.Exists(to))
                            Directory.CreateDirectory(to);
                        file1.CopyTo(to + file1.Name + ".done" + DateTime.Now.ToString().Replace(":", "_").Replace(" ", "").Replace("/", ""));
                    }
                    catch (Exception lobjException)
                    {
                        LogData(thePath);
                        LogData("<BR><BR>3) File Not Moved");
                        LogData(lobjException.Message.ToString() + "<BR>");
                        LogData(lobjException.StackTrace.ToString() + "<BR>");
                    }
                    //file.MoveTo(thePath + ".done." + DateTime.Now.ToString().Replace(":", "_"));
                }
                catch (Exception lobjException)
                {
                    LogData("<BR><BR>3) File Not Moved");
                    LogData(lobjException.Message.ToString() + "<BR>");
                    LogData(lobjException.StackTrace.ToString() + "<BR>");
                }
                Console.WriteLine("Completed Successfully");
               // SendMail(BodyPart, successCounter, errCounter);
            }
            catch (Exception lobjExceptions)
            {
                LogData(lobjExceptions.Message.ToString());
                LogData(lobjExceptions.StackTrace.ToString());
            }
            finally
            {
                if (dtExcel != null)
                {
                    dtExcel = null;
                }
                if (dtLevel != null)
                {
                    dtLevel = null;
                }
                if (dtBudget != null)
                {
                    dtBudget = null;
                }
            }
        }
        #endregion
        #region Log
        private static void LogData(string strMessage, string strDisplayMessage = "")
        {
            string dirpath = ConfigurationSettings.AppSettings["LogDirPath"].ToString();
            string FileName = DateTime.Now.Date.ToString("dd") + "_" + DateTime.Now.Date.ToString("MMM") + "_" + DateTime.Now.Date.Year + ".txt";
            //string FileName = DateTime.Now.ToString().Replace(":", "_") + ".txt";
            StreamWriter logFile;
        A:
            try
            {
                string FilePath = dirpath + "\\" + FileName;
                if (!File.Exists(FilePath.ToString()))
                {
                    logFile = new StreamWriter(FilePath);
                }
                else
                {
                    logFile = File.AppendText(FilePath.ToString());
                }
                logFile.WriteLine(strMessage);
                if (strDisplayMessage != "")
                    Console.WriteLine(strDisplayMessage);
                logFile.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                //Console.ReadLine();
                goto A;
            }
            finally
            {
            }
        }
        #endregion
        #region Insert Records
        private static int InsertMasRecord(DataRow drMasInsert)
        {
            int Id = 0;
            DAL_Common objDALCommon = new DAL_Common();
            try
            {
                SqlParameter[] objSqlParam = new SqlParameter[34];
                objSqlParam[0] = new SqlParameter("@TokenNumber", (drMasInsert["F2"] ?? string.Empty));
                objSqlParam[1] = new SqlParameter("@EmployeeName", (drMasInsert["Name"] ?? string.Empty));
                objSqlParam[2] = new SqlParameter("@FirstName", (drMasInsert["First Name"] ?? string.Empty));
                objSqlParam[3] = new SqlParameter("@LastName", (drMasInsert["Last Name"] ?? string.Empty));
                objSqlParam[4] = new SqlParameter("@DesigCode", (drMasInsert["Emp Position"] ?? string.Empty));
                objSqlParam[5] = new SqlParameter("@Designation", (drMasInsert["F7"] ?? string.Empty));
                objSqlParam[6] = new SqlParameter("@MangerName", (drMasInsert["Manager Name"] ?? string.Empty));
                objSqlParam[7] = new SqlParameter("@MangerNumber", (drMasInsert["F9"] ?? string.Empty));
                objSqlParam[8] = new SqlParameter("@ManagerPositionNumber", (drMasInsert["Manager Position"] ?? string.Empty));
                objSqlParam[9] = new SqlParameter("@ManagerPositionName", (drMasInsert["F11"] ?? string.Empty));
                objSqlParam[10] = new SqlParameter("@EmpGradeName_E", (drMasInsert["Employee Group"] ?? string.Empty));
                objSqlParam[11] = new SqlParameter("@EmpGradeCode_E", (drMasInsert["F13"] ?? string.Empty));
                objSqlParam[12] = new SqlParameter("@DivisionName_PA", (drMasInsert["Personnel Area"] ?? string.Empty));
                objSqlParam[13] = new SqlParameter("@Division_PA", (drMasInsert["F15"] ?? string.Empty));
                objSqlParam[14] = new SqlParameter("@LocationName_PSA", (drMasInsert["Personnel Subarea"] ?? string.Empty));
                objSqlParam[15] = new SqlParameter("@Location_PSA", (drMasInsert["F17"] ?? string.Empty));
                objSqlParam[16] = new SqlParameter("@CompanyCode", (drMasInsert["F19"] ?? string.Empty));
                objSqlParam[17] = new SqlParameter("@ProcessName", (drMasInsert["Business Unit"] ?? string.Empty));
                objSqlParam[18] = new SqlParameter("@BusinessUnit", (drMasInsert["Business Unit"] ?? string.Empty));
                objSqlParam[19] = new SqlParameter("@ProcessCode", (drMasInsert["F21"] ?? string.Empty));
                objSqlParam[20] = new SqlParameter("@OrgUnitName", (drMasInsert["Division"] ?? string.Empty));
                objSqlParam[21] = new SqlParameter("@OrgUnitCode", (drMasInsert["F23"] ?? string.Empty));
                objSqlParam[22] = new SqlParameter("@EmployeeCostCenterName", (drMasInsert["Cost Center"] ?? string.Empty));
                objSqlParam[23] = new SqlParameter("@EmployeeCostCenterNumber", (drMasInsert["F29"] ?? string.Empty));
                objSqlParam[24] = new SqlParameter("@DOB", (drMasInsert["Date of Birth"] ?? string.Empty));
                objSqlParam[25] = new SqlParameter("@EmailID", (drMasInsert["Email"] ?? string.Empty));
                objSqlParam[26] = new SqlParameter("@Gender", (drMasInsert["Gender"] ?? string.Empty));
                objSqlParam[27] = new SqlParameter("@DOJ", (drMasInsert["Employee Date of Joining"] ?? string.Empty));
                objSqlParam[28] = new SqlParameter("@EmployeeLevelName_ES", (drMasInsert["Employee Subgroup"] ?? string.Empty));
                objSqlParam[29] = new SqlParameter("@EmployeeLevelCode_ES", (drMasInsert["F36"] ?? string.Empty));
                objSqlParam[30] = new SqlParameter("@PayrollAreaText1", (drMasInsert["Payroll Area"] ?? string.Empty));
                objSqlParam[31] = new SqlParameter("@PayrollAreaText", (drMasInsert["F38"] ?? string.Empty));
                objSqlParam[32] = new SqlParameter("@Levels", (drMasInsert["LEVEL"] ?? string.Empty));
                objSqlParam[33] = new SqlParameter("@Status", (drMasInsert["Status"] ?? string.Empty));
                Id = (int)objDALCommon.DAL_SaveData("usp_UpdateExcelData", objSqlParam, 'N');
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return Id;
        }
        #endregion
        #region Validation
        private static bool CheckTokenValidation(string strValue)
        {
            try
            {
                switch (strValue.Trim())
                {
                    case "":
                        return true;
                    default:
                        return false;
                }
            }
            catch (Exception lobjException)
            {
                throw lobjException;
            }
        }
        private static bool CheckValidation(string strValue)
        {
            try
            {
                switch (strValue.Trim())
                {
                    case "":
                        return true;
                }
                //Jola Told to remove . from Validation Name, First Name and Last Name
                //string[] keys = new string[] { "#", "##", ".", "0", ",", "NOT ASSIGNED" };
                //Jola Told to remove . from Validation Name, First Name and Last Name
                string[] keys = new string[] { "#", "##", "0", ",", "NOT ASSIGNED" };
                string sKeyResult = keys.FirstOrDefault<string>(s => strValue.Contains(s));
                switch (sKeyResult)
                {
                    case "#":
                        return true;
                    case "##":
                        return true;
                    //Jola Told to remove . from Validation Name, First Name and Last Name
                    //case ".":
                    //    return true;
                    //Jola Told to remove . from Validation Name, First Name and Last Name
                    case ".":
                        return true;
                    case "0":
                        return true;
                    case ",":
                        return true;
                    case "NOT ASSIGNED":
                        return true;
                }
                return false;
            }
            catch (Exception lobjException)
            {
                throw lobjException;
            }
        }
        private static bool SysVal(string strValue, string strEmployeeGroup, string strEmail, string EG) //,string EG added by priya 3/8/2016
        {
            try
            {
                //below line commented by priya on 3/8/2016 and the if added below
                //if ((strEmployeeGroup.Trim().ToUpper().Contains("PERMANENT") && strEmployeeGroup.Trim().ToUpper().Contains("OFFICER")) || (strEmployeeGroup.Trim().ToUpper().Contains("PROBATIONER") && strEmployeeGroup.Trim().ToUpper().Contains("OFFICER")) || (strEmployeeGroup.Trim().ToUpper().Contains("TRAINEES") && strEmployeeGroup.Trim().ToUpper().Contains("WORKMEN")) || (strEmployeeGroup.Trim().ToUpper().Contains("TRAINEE") && strEmployeeGroup.Trim().ToUpper().Contains("OFFICER")))
                if ((strEmployeeGroup.Trim().ToUpper().Contains("PERMANENT") && EG.Trim().ToUpper().Contains("1")) || (strEmployeeGroup.Trim().ToUpper().Contains("PROBATIONER") && EG.Trim().ToUpper().Contains("2")) || (strEmployeeGroup.Trim().ToUpper().Contains("TRAINEE") && EG.Trim().ToUpper().Contains("4")) || (strEmployeeGroup.Trim().ToUpper().Contains("TRAINEE") && EG.Trim().ToUpper().Contains("C")))
                {
                    if (strValue.Trim().ToUpper().Contains("SYSTECH-MES") == true)
                    {
                        if (strEmail.Trim().ToUpper().Contains("MAHINDRA.COM") == true)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception lobjException)
            {
                throw lobjException;
            }
        }
        #endregion
        #region Fetch Data
        public static DataTable GetLevelsData()
        {
            try
            {
                DataSet _ds;
                DAL_Common objDALCommon = new DAL_Common();
                SqlParameter[] objSqlParam = new SqlParameter[0];
                _ds = objDALCommon.DAL_GetData("GetLevelsExcel", objSqlParam);
                return _ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public static DataTable GetBudgetGreaterThanZeroData()
        {
            try
            {
                DataSet _ds;
                DAL_Common objDALCommon = new DAL_Common();
                SqlParameter[] objSqlParam = new SqlParameter[0];
                _ds = objDALCommon.DAL_GetData("usp_GetBudgetGreaterThanZero", objSqlParam);
                return _ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion
    }
}


﻿// for select and unselect of a grid row

function SelectRow(row) {
    //alert(row.className);
    row.className = row.className == 'row01' ? 'selectedevenTd' : 'selectedoddTd';
}

function UnselectRow(row) {
    row.className = row.className == 'selectedevenTd' ? 'row01' : '';
}
function validateText() {
    alert("Test");
}
//======================================

function CheckTextLimit(obj) {
    var txtStoryName = $('#ContentPlaceHolder1_txtStoryName').val().trim().length;
    var txtSynopsis = $('#ContentPlaceHolder1_txtSynopsis').val().trim().length;
    var txtBusinessImpact = $('#ContentPlaceHolder1_txtBusinessImpact').val().trim().length;
    var txtBusinessUnit = $('#ContentPlaceHolder1_txtBusinessUnit').val().trim().length;

    if (txtStoryName < 30 && txtStoryName > 100) {
        alert("Please enter minimum 30 and maximum 100 character in Text for the Title.");
        return false;
    }
    else if (txtSynopsis < 30 && txtSynopsis > 300) {
        alert("Please enter minimum 30 and maximum 300 character in Text for the Synopsis.");
        return false;
    }
    if (txtBusinessImpact < 30 && txtBusinessImpact > 1000) {
        alert("Please enter minimum 30 and maximum 1000 character in Reason for BusinessImpact.");
        return false;
    }
    else if (txtBusinessUnit<10 && txtBusinessUnit > 100) {
        alert("Please enter minimum 30 and maximum 100 character in Reason for BusinessUnit.");
        return false;
    }
}




function RequiredField_CheckBoxList(objCheckBoxList, strSuffix) 
{ 
    var checkbox = objCheckBoxList.getElementsByTagName("input");
    var counter = 0;
    for (var i = 0; i < checkbox.length; i++) 
    {
        if (checkbox[i].checked) 
        {
            return '';
        }
    }

    return 'Please select atleast one option in ' + strSuffix + ' \n';
}



function RequiredField_CheckBoxListMore(objCheckBoxList, strSuffix) {
    var checkbox = objCheckBoxList.getElementsByTagName("input");
    var counter = 0;
    var checkdisabled = 0;
    for (var i = 0; i < checkbox.length; i++) {
        if (checkbox[i].checked) {
            counter++;
        }
        if (checkbox[i].disabled) {
            checkdisabled++;
         }
    }
    if (counter > 0 && counter!=checkdisabled ) 
    {
        return '';
    }
    else {
   
        return 'Please select atleast one option in ' + strSuffix + ' \n';
    } 
}

function ValidCharacters(obj, type, spstr,strMsg) {

    var chars;
    if (type == "alphabets") { chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; }
    else if (type == "numbers") { chars = "0123456789"; }
    else if (type == "alphabetsnumbers") { chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" }


    var str = obj.value;
    //spstr = spstr.toString().replace(/appostophe/g, "'");
    var validchars = chars + spstr;

    validchars = validchars.toString().replace(/ /g, "Space ");

    if (spstr.toString().indexOf(" ") >= 0) 
    {
        spstr = spstr.toString().replace(" ", "Space ");
    }
    for (var j = 0; j < str.length; j++) 
    {
        var val = str.charAt(j);
        if (validchars.indexOf(val) == -1) {
            if (val == "*") 
            {
                val = "~";
                str = str.toString().replace(/\*/g, "~");
            }
            str = str.toString().replace(new RegExp(val, "g"), "~");
            
        }
    }



    if (obj.value != str) 
    {
        str = str.toString().replace(/~/g, "");
            obj.value = str;
            return strMsg;
        }
        else
            return '';

}



function CheckText_Blank(strValue, strSuffix) {
    if (trim(strValue) == '') {
        return 'Please Enter ' + strSuffix + ' \n';
    }
    else {
        return '';
    }
}

function chkDate_Blank(strValue, strSuffix) {
    if (strValue == '') {
        return 'Please Select ' + strSuffix + ' \n';
    }
    else {
        return '';
    }
}

function IsBlank(strValue) {
    if (trim(strValue) == '') {
        return true;
    }
    else {
        return false;
    }
}


function CheckText_Zero(strValue, strSuffix) {
    if (trim(strValue) == 0) {
        return 'Please Enter Non-Zero Number in ' + strSuffix + ' \n';
    }
    else {
        return '';
    }
}

function GetDate(strDate) {
    var arrDate = strDate.split("-");
    var tDate = arrDate[1] + "/" + arrDate[2] + "/" + arrDate[0]
    tDate = new Date(tDate);
    return tDate;

}

function CheckDropdown_Blank(strValue, strSuffix) {

    if (trim(strValue) == '-- Select --' || trim(strValue) == '--' || trim(strValue) == '') {
        return 'Please Select ' + strSuffix + '. \n';
    }
    else {
        return '';
    }
}

function ValidateDecimal(objIDValue, strsuffix) {
    if (isNaN(objIDValue)) {
        return 'Please Enter Valid' + strsuffix +'.';
    }
    else if (trim(objIDValue) != '') {
        var strIsZero;

        strIsZero = CheckText_Zero(objIDValue, strsuffix)
        if (strIsZero != '') {
            return strIsZero;
        }

        var RegExp_Decimal = /^-?[0-9]{0,8}(\.[0-9]{1,2})?$|^-?(100)(\.[0]{1,2})?$/;
        if (!RegExp_Decimal.test(trim(objIDValue))) {
            //return strsuffix + " Must Have Valid Range" + ' \n';
            return "Please enter Valid " + strsuffix + '. Only two or less Decimal Places are allowed.\n';
        }
        else {
            return '';
        }
    }
    return '';
}

function ValidateInteger(objIDValue, strsuffix) {
    if (trim(objIDValue) != '') {

        var strIsZero;

        strIsZero = CheckText_Zero(objIDValue, strsuffix)
        if (strIsZero != '') {
            return strIsZero;
        }

        var RegExp_Integer = /^[0-9]+$/;
        if (!RegExp_Integer.test(trim(objIDValue))) {
            return strsuffix + " should contain Number." + '\n';
        }
        else {
            return '';
        }
    }
    return '';
}

function ValidateInteger_WithNegative(objIDValue, strsuffix) {
    if (trim(objIDValue) != '') {
        var strIsZero;

        strIsZero = CheckText_Zero(objIDValue, strsuffix)
        if (strIsZero != '') {
            return strIsZero;
        }

        // var RegExp_Integer = /0|-?[1-9]\d?/;
        //var RegExp_Integer = /^(?:[1-9][0-9]*|0)$/;
        //if (!RegExp_Integer.test(trim(objIDValue)) || trim(objIDValue) == 0) {
        if (isNaN(objIDValue) || trim(objIDValue) == 0) {
            return strsuffix + " should contain Number." + '\n';
        }
        else {
            return '';
        }
    }
    return '';
}


function ValidateAlphabet(objIDValue, strsuffix) {
    if (trim(objIDValue) != '') {
        var RegExp_Character = /^[a-z A-Z]+$/;
        if (!RegExp_Character.test(trim(objIDValue))) {
            return strsuffix + " should contain only Characters (a-z).\n";
        }
        else {
            return '';
        }
    }
    return '';
}


function ValidateAlphaNumeric(objIDValue, strsuffix) {
    if (trim(objIDValue) != '') {
        var RegExp_Alphanumeric = /^[0-9 a-z A-Z]+$/;
        if (!RegExp_Alphanumeric.test(objIDValue)) {
            return strsuffix + " should contain only Character (a-z) and Number (0-9). \n";
        }
        else {
            return '';
        }
    }
    return '';
}

function ValidateEmail(objIDValue) {
    if (trim(objIDValue) != '') {
        //var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[\.]{1}[a-zA-Z]{2,4}$/;--Not Used
        var email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //--In Use
        if (!email.test(trim(objIDValue))) {
            return 'Please Enter Valid Email. \n';
        }
        else {
            return '';
        }
    }
    return '';
}

function ConfirmDelete() {
    var varDelete;
    varDelete = confirm("Do you want to Delete this Record .");
    if (varDelete == true) {
        return true;
    }
    else {
        return false;
    }

}

function ChkList(objID, strSuffix) {
    for (var i = 0; i < objID.rows.length; i++) {
        for (var j = 0; j < objID.rows[i].cells.length; j++) {
            var listControl = objID.rows[i].cells[j].childNodes[0];
            if (listControl != undefined && listControl.checked == true)
                return '';
        }
    }
    //alert("Please Select Atleast One Roles");
    return 'Please Select Atleast One ' + strSuffix + '\n';
}

function trim(mystr) {
    var l = 0; var r = mystr.length - 1;
    while (l < mystr.length && mystr[l] == ' ')
    { l++; }
    while (r > l && mystr[r] == ' ')
    { r -= 1; }
    return mystr.substring(l, r + 1);
}


function DisableTextbox(objCheckBoxID, objtxtID, objdtpstartDate, objdtpEndDate, e) {
    var txtid = document.getElementById(objtxtID);

    var CheckboxId = document.getElementById(objCheckBoxID);

    if (CheckboxId.checked == true) {
        txtid.control.enable();
    }
    else {
        txtid.control.disable();
    }

}

function ValidateName(objIDValue, strsuffix) {
    if (trim(objIDValue) != '') {
        var RegExp_Alphanumeric = /^[0-9 a-z A-Z ' ]+$/;
        if (RegExp_Alphanumeric.test(objIDValue) == false) {
            return strsuffix + " should contain only Character (a-z) and Number (0-9) and apostrophe. \n";
        }
        else {
            return '';
        }
    }
    return '';
}
function Validatesubject(objIDValue, strsuffix) {
    if (trim(objIDValue) != '') {
        var RegExp_Alphanumeric = /^[0-9 a-z A-Z '!]+$/;
        if (RegExp_Alphanumeric.test(objIDValue) == false) {
            return strsuffix + " should contain only Character (a-z) and Number (0-9) and apostrophe and exclaimation mark.\n";
        }
        else {
            return '';
        }
    }
    return '';
}
function CheckTextToId_Blank(strValue, strSuffix ,activitytype) {
    if (trim(activitytype) == '3' || trim(activitytype) == '4') {
        if (trim(strValue) == '') {
            return 'Please Enter ' + strSuffix + ' .\n';
        }
        else {
            return '';
        }


    }
    else {
        return '';
    }
}

function checkMailIds(strvalue, strSuffix) {
    var mailids = strvalue;
    var str = mailids.split(",");

    for (var i = 0; i < str.length; i++) {
        var obk = str[i];
        var obk = trim(obk.toString()); 

        if (ValidateEmail(obk.toString()) == false) {

          
        }
        else {

            return ' \n Please Enter Valid ' + strSuffix;//  + '.Keep Space Between Two EmailIDs';
        }

    }
    return '';
}

function Check_StartEndDate(SDate, EDate, strStart, strEnd) 
{
    var endDate = new Date(EDate);
    var startDate = new Date(SDate);

    var err='';

    err += chkDate_Blank(SDate, strStart+" Date");
    err += chkDate_Blank(EDate, strEnd + " Date");

    if (SDate != '' && EDate != '' && startDate > endDate) 
    {
        err += "Please ensure that the " + strEnd + " Date is greater than or equal to the " + strStart + " Date.";
    }

    return err;
}


//function ValidateAlphaNumericwithfrontslash(objIDValue, strsuffix) {
//    if (trim(objIDValue) != '') {
//        var RegExp_Alphanumeric = /^[0-9 a-z A-Z /]+$/;
//        if (!RegExp_Alphanumeric.test(objIDValue)) {
//            return strsuffix + " should contain only Character (a-z) and Number (0-9) and / . \n";
//        }
//        else {
//            return '';
//        }
//    }
//    return '';
//}

function CheckDropdown_Blank2(strValue, strSuffix) {
    debugger;
    if (trim(strValue) == 'Select' || trim(strValue) == '--' || trim(strValue) == '') {
        return 'Please Select ' + strSuffix + '. \n';
    }
    else {
        return '';
    }
}
//======================================
function blockSpecialChar(e) {
    var k = e.keyCode;
    return ((k >= 65 && k <= 90) || (k >= 97 && k <= 122) || (k >= 48 && k <= 57) || k == 32 || k == 34 || k == 38);
}
//======================================
function validateEmail(e) {
    var k = e.keyCode;
    return ((k >= 65 && k <= 90) || (k >= 97 && k <= 122) || (k >= 48 && k <= 57) || k == 32 || k == 34 || k == 38);
}
//======================================
function validateNumber(e) {
    var k = e.keyCode;
    return (k >= 48 && k <= 57);
}
//======================================
function validateText(e) {
    var k = e.keyCode;
    return ((k >= 65 && k <= 90) || (k >= 97 && k <= 122) || k == 32 || k == 34 || k == 38);
}
//======================================


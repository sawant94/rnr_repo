﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SharePostWithGroups.aspx.cs" Inherits="Idealake.Web.MMRewardsSocial.SharePostWithGroups" %>

<%@ Register Src="~/UserControls/SharePostWithGroups.ascx" TagPrefix="uc1" TagName="SharePostWithGroups" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <link href="../includes/css/jscroll.css" rel="stylesheet" />
    <link href="../includes/css/rewardssocial.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <uc1:SharePostWithGroups runat="server" ID="ucSharePostWithGroups" />
        </div>
    </form>
</body>
</html>

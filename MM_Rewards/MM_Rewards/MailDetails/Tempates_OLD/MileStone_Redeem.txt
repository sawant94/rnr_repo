<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml >
<head>
<title>Untitled Page</title>
</head>
<body>
<table width="598" border="0" cellspacing="0" cellpadding="0" style="border-top:20px solid #E92226;border-bottom:10px solid #E92226;border-left:1px solid #E92226;border-right:1px solid #E92226;">
<tr>
<td style="border-top:5px double #E92226;padding:10px 20px; color:#4E504F; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:20px;"><p style="color:#E92226; font-weight:bold; font-size:18px;">Dear <%RecipientName%></p>
    	<p>
    		<b>Congratulations!!!</b>
		</p>
		<p style="font-size:13px;">You have received the <%AwardType%> Award of Rs. <%AwardAmount%> from <%GiverHODName%>. You can redeem the amount won in the following ways : <BR/></p>
		<span style="font-size:13px;">
				Option 1. Amount credited through salary.<br/>
	
			Option 2. Mahindra Rewards Galaxy. <a href=' <%VLINK%> '>Click here</a> to see the options offered.  
	</span>
			<br/>
			<br/>

            <strong style="font-size:13px;"><u>Process to be followed:</u> </strong>
			<ol style="font-size:13px;" type="i">
				<li> You can follow the below mentioned link to redeem the award won by you- <a href=' <%REDEEMLINK%> '>Click here</a></li>
				<li>Once an option is selected, it cannot be changed. </li>
                <li>The Award should be redeemed within a maximum span of 3 months from the 
                    date of receiving this email. </li>
               <li>&nbsp;In case of Kind Award, the redemption should be exhausted in single go. 
                      Balance points will not be redeemed or encashed.</li>

			</ol>
			<p style="font-size:13px;">&nbsp;Kindly get in touch with your R&amp;R coordinator of respective locations for 
                any query or assistance. </p>

		<p style="font-size:13px;">Regards,<br /> Rewards &amp; Recognition Team </p></td></tr></table></body></html>
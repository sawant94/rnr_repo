<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml ">
<head>
    <title>Excellerator</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <table id="Table_01" width="1000" height="1415" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="http://www.afsrecognitionportal.com:8081/template/images/Emailer_01.jpg" width="1000" height="207"
                    alt="">
            </td>
        </tr>
        <tr>
            <td>
                <img style="vertical-align: top" src="http://www.afsrecognitionportal.com:8081/template/images/Emailer_02.jpg" width="87" height="436"
                    alt="">
            </td>
            <td style="width: 794px; vertical-align: top">
                <div style="width: 785px; height: 417px; font-family: arial; font-size: 17px; color: #323231;
                    padding: 0 0 7px 7px; line-height: 35px; vertical-align: top;">
                    <span style="font-weight: bold; color: rgb(169, 50, 52); font-size: 42px; text-transform: capitalize;">
                        Dear
                        <%RecipientName%>,</span><br />
                    <span style="display: block; color: rgb(0, 0, 0); font-size: 30px;">
                        <br />
                        Congratulations!!!</span>
                    <p style="color: #606062; font-size: 24px">
                        You have received the <%AwardType%> Award of Rs.<%AwardAmount%>/- from
                        <%GiverHODName%>.
                    </p>
                   <div style="border: 1px solid rgb(204, 204, 204); min-height: 400px; width: 750px;
                                        padding: 0px 10px; font-size: 18px;">
                                        <p>
                                            <%reason%>
                                        </p>
                                    </div>
                    
                </div>

            </td>
            <td>
                <img style="vertical-align: top; float: right" src="http://www.afsrecognitionportal.com:8081/template/images/Emailer_04.jpg" width="121"
                    height="436" alt="">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="http://www.afsrecognitionportal.com:8081/template/images/Emailer_05_disclaimer5.jpg" alt="" width="1000" height="288" usemap="#Map" style="vertical-align: top"
                    border="0">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="http://www.afsrecognitionportal.com:8081/template/images/Emailer_06_disclaimer5.jpg" width="1000" height="184"
                    alt="">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="http://www.afsrecognitionportal.com:8081/template/images/Emailer_07.jpg" width="1000" height="300"
                    alt="">
            </td>
        </tr>
    </table>
    <map name="Map">
        <area shape="rect" coords="188,220,90,190" target="_blank" href=' <% MyAwards%> '>
    </map>
</body>
</html>

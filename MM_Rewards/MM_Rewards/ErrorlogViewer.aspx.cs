﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

namespace MM_Rewards
{
    public partial class ErrorlogViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Get the list of files
            string DirPath = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["ErrorLogSave_Path"].ToString();
            if (Directory.Exists(DirPath))
            {
                string[] Files = Directory.GetFiles(DirPath);
                string Display = "";
                foreach (string filename in Files)
                {
                    Display = Display + "<a target='_new' href='/MM_RewardsErrorLog/" + filename.Substring(filename.LastIndexOf("\\") + 1) + "'>" + filename.Substring(filename.LastIndexOf("\\") + 1) + "</a><BR/>";
                }
                //Display the Files as Links
                FileList.Text = Display;
            }

        }
    }
}
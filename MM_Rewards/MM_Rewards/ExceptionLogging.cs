﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Configuration;

namespace MM_Rewards
{
    public class ExceptionLogging: System.Web.UI.Page
    {

        #region "Method to log Errors"
        //This function Logs the error into a text file			
        public void LogErrorMessage(Exception exception, string From_Page_Name)
        {
            //Dont Log Error when the exception is due to thread abort //	
            if (exception.GetType().FullName != "System.Threading.ThreadAbortException")
            {
                string strFileName = String.Empty;

                // Create StringBuilder to maintain publishing information.               
                StringBuilder errorInfo = new StringBuilder();

                // Record General information.
                errorInfo.AppendFormat("{0}", Environment.NewLine);
                errorInfo.AppendFormat("{0}*************************************", Environment.NewLine);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Error At", System.DateTime.Now.ToString());
                errorInfo.AppendFormat("{0}Error at:{0}{1}", System.DateTime.Now.ToString(), Environment.NewLine);
                errorInfo.AppendFormat("{0}*************************************", Environment.NewLine);
                errorInfo.AppendFormat("{0}", Environment.NewLine);
                errorInfo.AppendFormat("{0}General Information{0}", Environment.NewLine);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Error From Page", From_Page_Name);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Error Message", exception.Message);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Inner Exception", exception.InnerException);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Error Source", exception.Source);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Error Details", exception.StackTrace);
                errorInfo.AppendFormat("{0}", Environment.NewLine);
                //errorInfo.AppendFormat("{0}", Environment.NewLine);
                //Get the file name where to log message

                
               // if (!string.IsNullOrEmpty(Server.MapPath(ConfigurationManager.AppSettings.Get("ErrorLogSave_Path"))))
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("ErrorLogSave_Path"))) // by vandana
                {
                    //strFileName = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings.Get("ErrorLogSave_Path")).ToString(), "ErrorLog_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt");

                    strFileName = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings.Get("ErrorLogSave_Path")).ToString(), "ErrorLog_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt");


                    if (strFileName != null && strFileName.Length > 0)
                    {
                        try
                        {
                            //Create directory if it does not exist
                            if (!Directory.Exists(Path.GetDirectoryName(strFileName)))
                                Directory.CreateDirectory(Path.GetDirectoryName(strFileName));
                        }
                        catch
                        {

                        }
                        FileStream errorLogStream = null;
                        try
                        {

                            if (File.Exists(strFileName) == false)
                            {
                                // If file does not exists create a new file at the specified path
                                errorLogStream = File.Open(strFileName, FileMode.Create, FileAccess.ReadWrite);
                            }
                            else
                            {
                                errorLogStream = File.Open(strFileName, FileMode.Append, FileAccess.Write);
                            }
                        }
                        finally
                        {
                            if (errorLogStream != null)
                            {
                                using (StreamWriter errorWriter = new StreamWriter(errorLogStream))
                                {
                                    errorWriter.Write(errorInfo.ToString());
                                }
                                errorLogStream.Close();
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region "Method to Log User Defined Messages"
        public void LogUserDefinedMessage(string strMessage)
        {
            //This function Logs the error into a text file
            if (strMessage != string.Empty)
            {
                string strFileName = String.Empty;

                // Create StringBuilder to maintain publishing information.
                StringBuilder errorInfo = new StringBuilder();

                // Record General information.
                errorInfo.AppendFormat("{0}", Environment.NewLine);
                errorInfo.AppendFormat("{0}**************************************************", Environment.NewLine);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Error At", System.DateTime.Now.ToString());
                //errorInfo.AppendFormat("{0}Error at:{0}{1}" ,System.DateTime.Now.ToString(), Environment.NewLine);
                errorInfo.AppendFormat("{0}**************************************************", Environment.NewLine);
                errorInfo.AppendFormat("{0}", Environment.NewLine);
                errorInfo.AppendFormat("{0}General Information{0}", Environment.NewLine);
                errorInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, "Error Message", strMessage);
                errorInfo.AppendFormat("{0}", Environment.NewLine);
                //Get the file name where to log message		

                if (ConfigurationManager.AppSettings.Get("ErrorLog") != null)
                {
                    strFileName = Path.Combine(ConfigurationManager.AppSettings["ErrorLogSave_Path"].ToString(), "ErrorLog_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt");

                    if (strFileName != null && strFileName.Length > 0)
                    {
                        try
                        {
                            //Create directory if it does not exist
                            if (!Directory.Exists(Path.GetDirectoryName(strFileName)))
                                Directory.CreateDirectory(Path.GetDirectoryName(strFileName));
                        }
                        catch
                        {

                        }
                        FileStream errorLogStream = null;
                        try
                        {
                            if (File.Exists(strFileName) == false)
                            {
                                // If file does not exists create a new file at the specified path
                                errorLogStream = File.Open(strFileName, FileMode.Create, FileAccess.ReadWrite);
                            }
                            else
                            {
                                errorLogStream = File.Open(strFileName, FileMode.Append, FileAccess.Write);
                            }
                        }
                        finally
                        {
                            if (errorLogStream != null)
                            {
                                using (StreamWriter errorWriter = new StreamWriter(errorLogStream))
                                {
                                    errorWriter.Write(errorInfo.ToString());
                                }
                                errorLogStream.Close();
                            }
                        }
                    }
                }
            }
        }
        #endregion

    }
}
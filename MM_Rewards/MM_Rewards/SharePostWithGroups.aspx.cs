﻿using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Idealake.Web.MMRewardsSocial
{
    public partial class SharePostWithGroups : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string postid = Request.QueryString["postid"];
            PostBase pagePost = WebContext.Current.GetPostById(Convert.ToInt32(postid));
            if (IsPostBack)
                ucSharePostWithGroups.SetCurrent(pagePost);
            else
                ucSharePostWithGroups.Current = pagePost;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_Rewards.Admin;
using System.Text;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Common;

namespace MM_Rewards
{
    public partial class Fileupload : System.Web.UI.Page
    {
        BL_ExcelUtility objBL_ExcelUtility = new BL_ExcelUtility();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        protected void Page_Load(object sender, EventArgs e)
        {
            BL_Rewards.BL_Common.Common.CheckUserLoggedIn(true); // Check that the usr is logged in, if not redirect him to homepage
        }

        //upload an excel file
        protected void btnupload_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Server.MapPath(@"~/Uploads/");
                if (flupload.HasFile)
                {
                    // generate a unique file name
                    string filename = DateTime.Now.ToString().Replace("/", "").Replace(":","").Replace(" ", "") + "_" + flupload.FileName;
                    path += filename;
                    flupload.SaveAs(path);
                    objBL_ExcelUtility.SaveData(path);

                    // Rebind the grid with the output log
                    BE_AwardClosureColl objBE_AwardClosureColl = new BE_AwardClosureColl();
                    BL_Redemption objBL_Redemption = new BL_Redemption();
                    objBE_AwardClosureColl = objBL_Redemption.GetDataClosure();
                    grdAwards.DataSource = objBE_AwardClosureColl;
                    grdAwards.DataBind();
                    //ThrowAlertMessage("File uploaded sucessfully.");
                   
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        private void ThrowAlertMessage(string strmessage)
        {
            try
            {
                if (strmessage.Contains("'"))
                {
                    strmessage = strmessage.Replace("'", "\\'");
                }
                string script = @"alert('" + strmessage + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        protected void grdAwards_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAwards.PageIndex = e.NewPageIndex;

                grdAwards.DataSource = new BL_Redemption().GetDataClosure();// objBE_AwardClosureColl;
                grdAwards.DataBind();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Fileupload.aspx");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Idealake.Web.MMRewardsSocial.Core;
using Idealake.Mahindra.RewardsSocial;
using MM_Rewards.UserControls;

namespace Idealake.Web.MMRewardsSocial
{
    public partial class UserProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User thisPageUser;
            string tokennumber = Request.QueryString["tokennumber"];
            if (string.IsNullOrWhiteSpace(tokennumber))
            {
                thisPageUser = WebContext.Current.CurrentUser;
            }
            else
            {
                thisPageUser = WebContext.Current.GetUserByTokenNumber(tokennumber);
            }

            if (IsPostBack)
            {
                ucUserSocialProfile.SetCurrent(thisPageUser);
                ucSearch.SetCurrent(WebContext.Current.CurrentUser);
            }
            else
            {
                ucUserSocialProfile.Current = thisPageUser;
                ucSearch.Current = WebContext.Current.CurrentUser;
            }
        }

        protected void ucSearch_SendHighFiveClicked(object sender, SendHighFiveEventArgs e)
        {
            //ucSearch.Visible = false;
            ucUserSocialProfile.HighFiveRecipientID = e.RecipientID;
        }
    }
}
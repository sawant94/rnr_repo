﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using MM.DAL;
using System.Net;
using System.Net.Mail;

namespace MM_Rewards
{
    internal class Common : System.Web.UI.Page
    {
        internal string  GetUserDetails(string TokenNumber)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_GetUserDetailsByTokenNumber", objsqlparam);
            string Levels = "";

            Session["TokenNumber"] = _ds.Tables[0].Rows[0]["TokenNumber"].ToString();
            Session["UserName"] = _ds.Tables[0].Rows[0]["UserName"].ToString();
            Session["Division"] = _ds.Tables[0].Rows[0]["ProcessName"].ToString();
            Session["Location"] = _ds.Tables[0].Rows[0]["PLantName_PsA"].ToString();
            Session["Levels"] = _ds.Tables[0].Rows[0]["Levels"].ToString();
            Session["Expenses"] = _ds.Tables[0].Rows[0]["Expenses"].ToString();
            Session["Balance"] = _ds.Tables[0].Rows[0]["Balance"].ToString();
            Session["EmailID"] = _ds.Tables[0].Rows[0]["EmailID"].ToString();

            if (Session["Levels"] != null)
            {
                Levels = (string)Session["Levels"];
            }
            return Levels;
        }

        public bool sendMail(string strFromID, string strSenderName, string strToID, string strRecipientName, string strCCID, string strSubject, string strMessage)
        {
            bool blnSuccess = false;
            MailMessage mailMsg = new MailMessage();
            MailAddress fromAddr = new MailAddress(strFromID, strSenderName);

            mailMsg.IsBodyHtml = true;
            mailMsg.From = fromAddr;
            //mailMsg.CC = CCAddr;
            mailMsg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            strToID = strToID.Replace(';', ',');
            strCCID = strCCID.Replace(';', ',');
            mailMsg.To.Add(strToID);
            mailMsg.Subject = strSubject;
            mailMsg.Body = strMessage;

            if (strCCID != "")
            {
                mailMsg.CC.Add(strCCID);
            }

            string SMTPServer = System.Configuration.ConfigurationManager.AppSettings.Get("SMTP");
            SmtpClient objSmtpClient = new SmtpClient(SMTPServer);
            try
            {
                objSmtpClient.Send(mailMsg);

                blnSuccess = true;
            }
            catch (Exception GeneralEx)
            {
                throw new ArgumentException(GeneralEx.Message);
            }
            finally
            {
                objSmtpClient = null;
                mailMsg = null;
                fromAddr = null;
            }
            return blnSuccess;

        }

        internal DataSet GetMailDetails(string ActivityType,string Location)
        {
            SqlParameter[] objsqlparam = new SqlParameter[2];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@ActivityType", ActivityType);
            objsqlparam[1] = new SqlParameter("@Location", Location);
            _ds = objDAL.DAL_GetData("usp_GetMailDetailsFromActivity", objsqlparam);

            return _ds;
        }
    }
}
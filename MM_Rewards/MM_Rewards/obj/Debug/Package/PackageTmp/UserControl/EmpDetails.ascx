﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpDetails.ascx.cs" Inherits="MM_Rewards.UserControl.EmpDetails" %>
<div>
 <table width="100%" cellspacing="0" cellpadding="0" 
            class="container" >
            <tr>
                <td>
                    <asp:Label ID="lblTokenNumber" runat="server" Text="Enter TokenNumber/Name" 
                        CssClass="txtB"></asp:Label><span style="color:Red">*</span>
                </td>
                <td >
                    
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtTokenNumber" runat="server" Width="125px"></asp:TextBox>
               
                 &nbsp;
                    <asp:Button ID="btnget" runat="server" Text="GetDetails"  OnClick="btnget_Click" OnClientClick="return validatebtn()"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="overflow: auto; max-height: 200px;">
                        <asp:GridView ID="grdEmployeeList" runat="server" AutoGenerateColumns="False" CellPadding="5"
                            BorderColor="#336699" BorderStyle="Solid" BorderWidth="1px" 
                            CellSpacing="2" Font-Names="Arial"
                            Font-Size="Small" ForeColor="#003366"  Height="178px" 
                            onrowcommand="grdEmployeeList_RowCommand" >

                            <Columns>
                              <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" />
                                <asp:BoundField DataField="EmpName" HeaderText="Name"  />
                                <asp:BoundField DataField="ProcessName" HeaderText="Process Name" />
                                <asp:BoundField DataField="DivisionName" HeaderText="Division Name" />
                                <asp:BoundField DataField="LocationName" HeaderText="Location Name" />

                                <asp:BoundField DataField="IsCEO" HeaderText="IsCEO" Visible = "false"  />
                                <asp:BoundField DataField="IsRR" HeaderText="IsRR" Visible = "false"/>
                                <asp:BoundField DataField="IsAdministrator" HeaderText="IsAdministrator" Visible = "false" />
                            <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber")  %>'  CommandName="Select"
                                                Width="100px" runat="server" ForeColor="Red">
                                            Select</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                                      
                            </Columns>
                            <HeaderStyle BackColor="#336699" CssClass="textalign" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle BorderColor="#336699" BorderStyle="Solid" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFFFCC" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            </table>
</div>
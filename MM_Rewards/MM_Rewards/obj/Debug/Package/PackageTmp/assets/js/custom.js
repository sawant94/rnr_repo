$(window).on('load', function() {
    var winW = $(window).width();
    var winH = $(window).height();

    $('body').addClass($.browser.name);

});
$(window).on('resize', function() {
    var winW = $(window).width();
    var winH = $(window).height();

    $('body').addClass($.browser.name);

});

// css menu dropdown start
(function ($) {
$.fn.menumaker = function (options) {
var cssmenu = $(this),
settings = $.extend(
{
  format: "dropdown",
  sticky: false
},
options
);
return this.each(function () {
$(this)
.find(".button")
.on("click", function () {
  $(this).toggleClass("menu-opened");
  var mainmenu = $(this).next("ul");
  if (mainmenu.hasClass("open")) {
    mainmenu.slideToggle().removeClass("open");
  } else {
    mainmenu.slideToggle().addClass("open");
    if (settings.format === "dropdown") {
      mainmenu.find("ul").show();
    }
  }
});
cssmenu.find("li ul").parent().addClass("has-sub");
multiTg = function () {
cssmenu
  .find(".has-sub")
  .prepend('<span class="submenu-button"></span>');
cssmenu.find(".submenu-button").on("click", function () {
  $(this).toggleClass("submenu-opened");
  if ($(this).siblings("ul").hasClass("open")) {
    $(this).siblings("ul").removeClass("open").slideToggle();
  } else {
    $(this).siblings("ul").addClass("open").slideToggle();
  }
});
};
if (settings.format === "multitoggle") multiTg();
else cssmenu.addClass("dropdown");
if (settings.sticky === true) cssmenu.css("position", "fixed");
resizeFix = function () {
var mediasize = 1024;
if ($(window).width() > mediasize) {
  cssmenu.find("ul").show();
}
if ($(window).width() <= mediasize) {
  cssmenu.find("ul").hide().removeClass("open");
}
};
resizeFix();
return $(window).on("resize", resizeFix);
});
};
})(jQuery);

(function ($) {
$(document).ready(function () {
$("#cssmenu").menumaker({
format: "multitoggle"
});
});
})(jQuery);
// css menu dropdoen end

$(document).ready(function() {

    $('.testi-box').mouseover(function(){
        $(this).find('.testimain').addClass('show');
    });

    $('.testi-box').mouseleave(function(){
        $('.testimain').removeClass('show');
    });

    var winW = $(window).width();
    var winH = $(window).height();

    $(".dcs-inner ul, .mm-questions11").mCustomScrollbar({
        theme: "minimal-dark"
    });
    // $('.attempt-question li a').on('click', function() {
    //     // var header_height = $('.navbar').outerHeight();
    //     $('.mm-questions li').animate({
    //         scrollTop: $($(this).attr('data-target')).offset().top - 150
    //     }, 1000);
    // });

    $(window).scroll(function() {
    });

    $(document).on('click touchstart', function(e) {
        var container = $(".account,.notification,.setting");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.right-head ul li').removeClass('active');

        }

        
    });

    // header start here

    // on scroll start here
    if ($('.sp_product_data_section').length > 0) {
        var offset_top = $('.sp_product_data_section').offset().top;
    }
    var header_height = $('.middle_header').outerHeight();
    $(document).scroll(function() {
        var getscroll = $(window).scrollTop();
        if (getscroll > 50) {
            $('header').addClass('scroll_down');
        } else {
            $('header').removeClass('scroll_down');
        }

    });

    // footer start here
    if (winW < 768) {
    }
    // footer end here

    // products cutom js start here
    if (winW > 1025) {
    }
    // product custom js end here

    $(document).on('click', '.send-ecard .add-emp', function() {
        $(".send-ecard .step-1").addClass('hide');
        $(".send-ecard .step-2").removeClass('hide');
        // $(".search-emp-modal").removeClass('show');
        $('.search-emp-modal').modal('hide');
    });
    $(document).on('click', '.send-ecard .send-ecard-btn', function() {
        $(".send-ecard .step-2").fadeOut('hide');
        $(".send-ecard .success-ecard").removeClass('hide');
    });
    // comman drop down start here
    $(document).on('click', '.mahindra_drop_down_div span', function() {
        if ($(this).parents('.mahindra_drop_down_div').hasClass('mahindra_open_drop')) {
            $(this).parents('.mahindra_drop_down_div').removeClass('mahindra_open_drop');
            // $(this).next('ul').slideUp();
        } else {
            $(this).parents('.mahindra_drop_down_div').addClass('mahindra_open_drop');
            // $(this).next('ul').slideDown();
        }
    });
    $(document).on('click', '.mahindra_drop_down_div ul li', function() {
        $(this).parents('.mahindra_drop_down_div').find('li').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.mahindra_drop_down_div').find('span small').text($(this).text());
        $(this).parents('.mahindra_drop_down_div').removeClass('mahindra_open_drop');
        $(this).parents('.mahindra_drop_down_div').find('span small').removeClass('active');
        $(this).addClass('active');
    });
    // comman drop down end here


    // budget chart start
    var options = {
        series: [44, 55],
        chart: {
            width: 400,
            type: 'donut',
            data: {
                labels: ['Used Budget', 'Available Budget'],
                datasets: [{
                    data: [44, 55],
                    backgroundColor: [
                        '#7798e8',
                        '#0fdec1'
                    ],
                    borderWidth: 0
                }]
            },
        },
        dataLabels: {
            enabled: false
        },
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }]
    };

    var chart = new ApexCharts(document.querySelector("#budgetchart"), options);
    chart.render();
    // budget chart end
    $('.rnr-radio-btn table tr td span label').text('');
    setTimeout(function(){
        if ($('.view_clicked').length > 0) {
            //do your thing
            //alert();
            $("html,body").animate({
                scrollTop: $(".d_panel").offset().top - 80
            }, 100);
        }
    }, 100);
});

  $(".radio.other-recipent input#radio-2").click(function() {
       $(".hide-box-2").show();
        $(".hide-box-1").hide();
    });

   $(".radio.direct-reportee input#radio-1").click(function() {
       $(".hide-box-1").show();
         $(".hide-box-2").hide();
    });


    $(".radio.other-recipent input#radio-4").click(function() {
        $(".hide-box-2").show();
        $(".hide-box-1").hide();
    });

      $(".radio.direct-reportee input#radio-3").click(function() {
       $(".hide-box-1").show();
         $(".hide-box-2").hide();
    });

      $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").attr('checked', this.checked);
        });



    Array.prototype.forEach.call(
  document.querySelectorAll(".file-upload__button"),
  function(button) {
    const hiddenInput = button.parentElement.querySelector(
      ".file-upload__input"
    );
    const label = button.parentElement.querySelector(".file-upload__label");
    const defaultLabelText = "";

    // Set default text for label
    label.textContent = defaultLabelText;
    label.title = defaultLabelText;

    button.addEventListener("click", function() {
      hiddenInput.click();
    });

    hiddenInput.addEventListener("change", function() {
      const filenameList = Array.prototype.map.call(hiddenInput.files, function(
        file
      ) {
        return file.name;
      });

      label.textContent = filenameList.join(", ") || defaultLabelText;
      label.title = label.textContent;
    });
  }
);
   


  //  $(".budget-mobile .budget-available").click(function(){
  //   $(".budget-mobile .budget-used").show();
  //     $(".budget-mobile .total-buget").show();
  // });
  // $(".budget-mobile .budget-available").click(function(){
  //    $(".budget-mobile .budget-used").hide();
  //     $(".budget-mobile .total-buget").hide();
  // });

  jQuery(document).ready(function($)
{
  
  $(".budget-mobile .budget-availablee").click(function()
  {
    
    $(".budget-mobile .budget-used").slideToggle( "slow");
    
    if ($(".budget-mobile .budget-availablee").text() == "")
      {     
        $(".budget-mobile .budget-availablee").html("X")
      }
    else 
      {   
        $(".budget-mobile .budget-availablee").text("X")
      }
    
  });  

  
  
});


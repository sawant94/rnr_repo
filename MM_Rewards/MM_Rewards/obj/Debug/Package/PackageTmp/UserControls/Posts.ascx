﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Posts.ascx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserControls.Posts" %>

<script type="text/javascript">

    function showGroup(id) {
        $("#iframeSharePostWithGroup")[0].src = '../SharePostWithGroups.aspx?postid=' + id;
        $(".divGroups").show();
        $(".divGroups").dialog({ modal: true, width: 500, height: 500, title: 'Share Post with Groups' });

    }

    $(document).ready(function () {
        var currentUser = '<%= Current.TokenNumber %>';
        var loggedInUser = '<%= @Idealake.Web.MMRewardsSocial.Core.WebContext.Current.CurrentUser.TokenNumber %>';

        if (loggedInUser == currentUser) {
            $(".sharepost").show();

            $(".sharepost").click(function () {
                $(".divShare").not($(this).find(".divShare")).hide();
                $(this).find(".divShare").toggle();
            });
        }
        else {
            $(".sharepost").hide();
        }

    });
</script>

<div class="posts">
    <asp:Repeater runat="server" ID="rptPosts" OnItemCommand="rptPosts_ItemCommand" OnItemDataBound="rptPosts_ItemDataBound">
        <ItemTemplate>
            <div class="post">
                <div class="postspacing">
                    <a id='Post-<%# Eval("ID") %>'></a>
                    <div class="poster">
                        <img src='Photos/<%# Eval("Source.Photo") %>' alt='<%# Eval("Source.FullName") %>' />
                        <div>
                            <h3><%# Eval("Source.FullName") %></h3>
                            <p><%# string.Format("{0:dd MMMM yy, h:mm tt}",Eval("CreatedOn")) %></p>
                        </div>
                        <div class="sharepost" id="divShare" runat="server">
                            <img id="imgShare" src="../includes/Images/downarrow.png" class="share" />
                            <div class="divShare" style="display: none">
                                <asp:LinkButton ID="lnkAllFriends" runat="server" CommandArgument='<%# Eval("ID") %>' CommandName="all">All Friends</asp:LinkButton>
                                <asp:LinkButton ID="lnkOnlyMe" runat="server" CommandArgument='<%# Eval("ID") %>' CommandName="me">Only Me</asp:LinkButton>
                                <%--<asp:LinkButton ID="lnkGroups" runat="server" CommandArgument='<%# Eval("ID") %>' CommandName="group" CssClass="group">Groups</asp:LinkButton>--%>
                                <%--<asp:HyperLink ID="hylinkGroups" runat="server" Style="cursor: pointer">Groups</asp:HyperLink>--%>
                            </div>
                        </div>
                    </div>

                    <div class="posttype">
                        <img src="../includes/Images/posttype<%# Eval("PostType") %>.png"><div class="posttitle">
                            <asp:Label ID="lblPost" runat="server" Text=""> <%# FormatTitle((Idealake.Mahindra.RewardsSocial.PostBase)Container.DataItem) %> </asp:Label>
                        </div>
                    </div>
                    <div class="titleandcontent">
                        <div class="title">
                        </div>
                        <div class="content">
                            <%# Convert.ToInt32( Eval("PostType"))==1?Eval("Content").ToString(): HttpUtility.HtmlEncode(Eval("Content") ) %>
                        </div>
                    </div>

                    <div class="postactions">
                        <div class="likebutton">
                            <asp:ImageButton runat="server" ID="lbtnLike" Text="Like" CssClass="likeimage" CommandArgument='<%# Eval("ID") %>' CommandName="likepost" ImageUrl="~/includes/Images/like.png" />
                            <span class="likecount"><%# Eval("LikeCount") %></span>
                        </div>
                        <div class="likebutton">
                            <asp:ImageButton runat="server" ID="lbtnComment" Text="Comment" CssClass="likeimage" CommandArgument='<%# Eval("ID") %>' CommandName="commentpost" ImageUrl="~/includes/Images/comment.png" /><span class="likecount"><%# Eval("CommentCount") %></span>
                        </div>

                    </div>
                    <div class="likecountnamediv">
                        <asp:Label ID="lblSingleLike" runat="server"></asp:Label><asp:LinkButton ID="lnkLikeNameCount" CssClass="userlink" runat="server" CommandName="displayalluser" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton> <asp:Literal ID="ltlExtraText" runat="server"></asp:Literal>
                    </div>
                    <div class="maincomment">
                        <asp:Repeater runat="server" ID="rptPostComments" OnItemCommand="rptPostComments_ItemCommand" OnItemDataBound="rptPostComments_ItemDataBound">
                            <ItemTemplate>
                                <div class="grid">
                                    <asp:LinkButton runat="server" ID="lbtnDelete" Text="Like" CommandArgument='<%# Eval("ID") %>' CommandName="delete" CssClass="commentdeletebutton" Visible='<%# Eval("CanDelete") %>'><img src="../includes/Images/Close.png" /></asp:LinkButton>
                                    <div class="avatar">
                                        <img src='Photos/<%# Eval("Source.Photo") %>' alt='<%# Eval("Source.FullName") %>' />
                                    </div>
                                    <div class="maincomment-author">
                                        <span class="author"><%# Eval("Source.FullName") %>: </span><span class="friendcomment"><%# HttpUtility.HtmlEncode(Eval("Content")) %></span>
                                        <span class="dateclass"><%# string.Format("{0:dd MMMM, h:mm tt}",Eval("CreatedOn")) %></span>
                                        <div class="postactions">
                                            <div class="likebutton">
                                                <asp:ImageButton runat="server" ID="lbtnCommentLike" Text="Like" CssClass="likeimage" CommandArgument='<%# Eval("ID") %>' CommandName="likecomment" ImageUrl="~/includes/Images/like.png" />
                                                <span class="likecount"><%# Eval("LikeCount") %></span>
                                            </div>
                                        </div>
                                        <div class="likecountnamediv">
                                            <asp:Label ID="lblSingleCommentLike" runat="server"></asp:Label><asp:LinkButton ID="lnkCommentLikeNameCount" CssClass="userlink" runat="server" CommandName="displayalluser" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            <asp:Literal ID="ltlCommentLikeExtraText" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%--<div class="grid">
                                <div class="avatar">
                                  <img src='Photos/<%# Eval("Source.Photo") %>' alt='<%# Eval("Source.FullName") %>' width="38px" height="38px"/>
                                </div>
                                <div class="maincomment-author">
                                  <span class="author">Karishma T.</span><span class="friendcomment"> Yeah me too.</span>
                                  <span class="dateclass">March 31 at 12:00pm</span>                              
                                </div>
                              </div>--%>
                    </div>
                    <div class="commentClass">
                        <asp:TextBox runat="server" ID="txtComment" Rows="4" Columns="50"  placeholder="Write Comments Here"  CssClass="textarea"  ></asp:TextBox>
                    </div>
                    <div class="viewpost">
                        <%--<asp:Button ID="btnAddComment" runat="server" CommandArgument='<%# Eval("ID") %>' CommandName="comment" Text="Comment"></asp:Button>--%>
                        <asp:LinkButton ID="btnAddComment"  runat="server" CommandArgument='<%# Eval("ID") %>' CommandName="comment" Text="Comment" CssClass="comment"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>


    <asp:Panel runat="server" ID="pnlViewAllUserResults">
        <div class="searchresults">
            <asp:ListView runat="server" ID="lvViewAllUser">
                <EmptyDataTemplate>
                    No users match your search.
                </EmptyDataTemplate>
                <ItemTemplate>
                    <div class="friend poster">
                        <div class="userInfo">
                            <a href='UserProfile.aspx?tokennumber=<%#Eval("TokenNumber") %>' class="profileImage">
                                <img style="width: 50px; height: 50px;" src='../Photos/<%# Eval("Photo") %>' alt='<%# Eval("FullName") %>' />
                            </a>
                            <div class="details">
                                <a href='UserProfile.aspx?tokennumber=<%#Eval("TokenNumber") %>'>
                                    <p><%# Eval("FullName") %></p>
                                </a>
                                <span class="designation"><%# Eval("Designation")%></span>, 
                          <span class="depart"><%# Eval("Department")%></span>
                            </div>
                        </div>

                    </div>
                </ItemTemplate>

            </asp:ListView>
        </div>

    </asp:Panel>

    <div id="divGroups" class="divGroups" style="display: none">
        <iframe id="iframeSharePostWithGroup" frameborder="no" style="border-style: none; border: 0px; width: 540px; border-collapse: collapse; margin: 0px; padding: 0px; overflow-x: hidden; height: 260px"
            scrolling="yes"></iframe>
    </div>
</div>

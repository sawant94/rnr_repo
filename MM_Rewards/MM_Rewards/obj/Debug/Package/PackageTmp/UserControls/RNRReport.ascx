﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RNRReport.ascx.cs" Inherits="MM_Rewards.UserControls.RNRReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
   <script src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("[id$=dateInput_text]").attr('readonly', 'readonly');
            //$("[id$=dtpEnddate_dateInput_text]").attr('readonly', 'readonly');
        });
    </script>

   <script type="text/javascript">

       function ValidatePage() {

           var errMsg = '';
           var dpStartDate;
           var dpEndDate;
           dpStartDate = $find('<%=dtpStartdate.ClientID %>');
           dpEndDate = $find('<%=dtpEnddate.ClientID %>');

           errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");
           if (errMsg != '') {
               alert(errMsg);
               return false;
           }
           else {
               return true;
           }
       }

    </script>
<div >
    <div class="form01">
        <table>
            <tr>
                <td colspan="3">
                    <h1>
                        Weekly Report</h1>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Start Date<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="3">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    End Date<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="3">
                    </telerik:RadDatePicker>
                      <asp:CompareValidator ID="dateCompareValidator" runat="server" 
                        ControlToValidate="dtpEnddate" ControlToCompare="dtpStartdate" 
                        Operator="GreaterThan" Type="date" 
                        ErrorMessage="The end date must be greater than the start date."
                        CssClass="errorMessage">
                    </asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnSearch" Width="150px" CssClass="btnLeft" runat="server" Text="Generate Report"
                            TabIndex="3" OnClick="btnSearch_Click"  OnClientClick="return ValidatePage()" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <%-- <asp:Label runat="server">From Date</asp:Label>

    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
        TabIndex="3">
    </telerik:RadDatePicker>
    <asp:Label ID="lblToDate" runat="server">To Date</asp:Label>

    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
        TabIndex="3">
    </telerik:RadDatePicker>
    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />--%>
    <div runat="server" id="rpthold">
       <%-- <rsweb:ReportViewer ID="rptRNRReport" runat="server" Width="100%" Font-Names="Verdana" CssClass="rpTab" 
            Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
            WaitMessageFont-Size="14pt">
            <LocalReport>
                <DataSources>
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>--%>
         <rsweb:ReportViewer ID="rptRNRReport" runat="server" style="height:100%;width:100%;overflow:auto;position:relative;" AsyncRendering="True" InteractiveDeviceInfos="(Collection)">
            <LocalReport>
                <DataSources>
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
</div>

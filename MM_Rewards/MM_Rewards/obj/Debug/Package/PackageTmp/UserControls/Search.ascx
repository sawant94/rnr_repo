﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserControls.Search" %>
<script type="text/javascript">

    $(document).ready(function () {

        $(".sendhighfivebutton1").click(function () {
            $(".sendhighfivemessage").not($(this).next(".sendhighfivemessage")).hide();
            $(this).next(".sendhighfivemessage").toggle();
        });

        $(".cancel").click(function () {
            $(".sendhighfivemessage").hide();
            $(".msgText").val('');
        });

    });
</script>

<div class="searchsection">
    <span class="fl labelSearch">Search:</span>
    <span class="textSearch">
        <asp:TextBox runat="server" ID="txtSearchUsers" /></span>
    <span class="search">
        <asp:Button runat="server" ID="btnSearchUsers" Text="" OnClick="btnSearchUsers_Click" class="searchButton" /></span>
    <div class="forright">
        <div class="notificationsection">

            <asp:LinkButton runat="server" ID="btnNotification" OnClick="btnNotification_Click">
                Notification  
            <span>

                <img src="../includes/Images/bellnotify.png" /></span>
                <asp:Literal runat="server" ID="ltlNotification"></asp:Literal>
            </asp:LinkButton>

            <%--<a href="javascript:void(0);" class="notifybtn">2</a>--%>
        </div>
    </div>
    <%--    <div class="wrappernotify" style="display: none">
        <div class="container">
            <div class="tabHold">
                <div class="tabData">
                    <div class="content" id="content_1">
                        <div class="mainnotification">
                            <div class="friendrequest">
                                <div class="notification">
                                    <span>
                                        <img src="../includes/Images/friend.png"></span>
                                </div>
                                <a href="#">Shivnath Gawde</a>
                                <p>18 Friends</p>
                            </div>
                            <div class="acceptdecline">
                                <a href="#" class="acceptBtn">Accept</a>
                                <a href="#" class="decBtn">Decline</a>
                            </div>
                        </div>
                        <div class="mainnotification">
                            <div class="friendrequest">
                                <div class="notification">
                                    <span>
                                        <img src="../includes/Images/friend.png"></span>
                                </div>
                                <a href="#">Sanjeev Patil</a>
                                <p>18 Friends</p>
                            </div>
                            <div class="acceptdecline">
                                <a href="#" class="acceptBtn">Accept</a>
                                <a href="#" class="decBtn">Decline</a>
                            </div>
                        </div>
                        <div class="bgcolor">
                            <div class="mainnotification">
                                <div class="friendrequest">
                                    <div class="notification">
                                        <span>
                                            <img src="../includes/Images/friend.png"></span>
                                    </div>
                                    <a href="#">Shivnath Gawde</a>There are many variations of passages of Lorem Ipsum available.. 
                      <p>18 Friends</p>
                                </div>
                            </div>
                        </div>
                        <div class="bgcolor">
                            <div class="mainnotification">
                                <div class="friendrequest">
                                    <div class="notification">
                                        <span>
                                            <img src="../includes/Images/friend.png"></span>
                                    </div>
                                    <a href="#">Shivnath Gawde</a>There are many variations of passages of Lorem Ipsum available..
                      <p>18 Friends</p>
                                </div>
                            </div>
                        </div>
                        <div class="bgcolor">
                            <div class="mainnotification">
                                <div class="friendrequest">
                                    <div class="notification">
                                        <span>
                                            <img src="../includes/Images/friend.png"></span>
                                    </div>
                                    <a href="#">Shivnath Gawde</a>There are many variations of passages of Lorem Ipsum available..
                      <p>18 Friends</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    <asp:Panel runat="server" ID="pnlNotificationResults" CssClass="wrappernotify" Visible="false">
        <asp:ListView runat="server" ID="lvnotificationclickresult" DataKeyNames="ID" OnItemCommand="lvnotificationclickresult_ItemCommand" EnableViewState="true">
            <LayoutTemplate>
                <div class="container">
                    <div class="tabHold">
                        <div class="tabData">
                            <div class="content" id="content_1">
                                <div runat="server" id="itemPlaceholder" />
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="bgcolor">
                    <div class='mainnotification  <%# Convert.ToBoolean(Eval("UnRead"))==false? "readNotf":"unreadNotf" %>'>
                        <div class="notification">
                            <div class="friendrequest">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="gotodetails" CommandArgument='<%#Eval("ID") %>'><%# Eval("Message") %></asp:LinkButton>
                            </div>
                        </div>
                        <asp:Panel ID="Panel1" runat="server" CssClass="acceptdecline" Visible='<%#Eval("HasButtons")%>'>
                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Accept" CommandName="acceptfriendrequest" CommandArgument='<%#Eval("ID") %>' CssClass="acceptBtn"></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" Text="Reject" CommandName="rejectfriendrequest" CommandArgument='<%#Eval("ID") %>' CssClass="decBtn"></asp:LinkButton>
                        </asp:Panel>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
</div>

<asp:Panel runat="server" ID="pnlSearchResults" ClientIDMode="Static">
    <div class="searchresults">
        <asp:ListView runat="server" ID="lvSearchResults" OnItemCommand="lvSearchResults_ItemCommand" OnItemDataBound="lvSearchResults_ItemDataBound" OnItemEditing="lvSearchResults_ItemEditing" OnItemCanceling="lvSearchResults_ItemCanceling">
            <EmptyDataTemplate>
                No users match your search.
            </EmptyDataTemplate>
            <ItemTemplate>
                <div class="friend poster">
                    <%--<a href='UserProfile.aspx?tokennumber=<%#Eval("TokenNumber") %>'>
                        <img style="width: 100px; height: 100px;" src='../Photos/<%# Eval("Photo") %>' alt='<%# Eval("FullName") %>' />
                        <p><%# Eval("FullName") %></p>                     
                    </a>
                      <div class="details">
                          <span class="design">Designation : <%# Eval("Designation")%></span>
                          <span class="depart">Department : <%# Eval("Department")%></span>
                      </div>--%>
                    <div class="userInfo">
                        <a href='UserProfile.aspx?tokennumber=<%#Eval("TokenNumber") %>' class="profileImage">
                            <img style="width: 50px; height: 50px;" src='../Photos/<%# Eval("Photo") %>' alt='<%# Eval("FullName") %>' />
                        </a>
                        <div class="details">
                            <a href='UserProfile.aspx?tokennumber=<%#Eval("TokenNumber") %>'><p><%# Eval("FullName") %></p></a>
                            <span class="designation"><%# Eval("Designation")%></span>, 
                          <span class="depart"><%# Eval("Department")%></span>
                        </div>
                    </div>
                    <div class="fr">
                        <%--<div class="profileconnectto">
                            
                        </div>--%>

                        <asp:Panel runat="server" ID="pnlHighFive" class="sendhighfive">
                            <asp:LinkButton ID="btnGetConnected" runat="server" Visible="false" CommandName="getconnected" CommandArgument='<%# Eval("Id") %>'>Get Connected</asp:LinkButton>
                            <asp:LinkButton runat="server" ID="lnkSendHighFive" CommandName="sendhighfive" CommandArgument='<%# Eval("Id") %>'>Send High-Five</asp:LinkButton>
                        </asp:Panel>
                    </div>
                </div>
            </ItemTemplate>
            <EditItemTemplate>
                <%--<div class="friend">
                    <div>
                        <a href='UserProfile.aspx?tokennumber=<%#Eval("TokenNumber") %>'>
                            <img style="width: 50px; height: 50px;" src='../Photos/<%# Eval("Photo") %>' alt='<%# Eval("FullName") %>' />
                            <p><%# Eval("FullName") %></p>
                        </a>
                        <div class="details">
                            <p><%# Eval("FullName") %></p>
                            <span class="design"><%# Eval("Designation")%></span>, 
                          <span class="depart"><%# Eval("Department")%></span>
                        </div>
                    </div>
                    <div>
                        <div class="profileconnectto">
                            <asp:LinkButton ID="btnGetConnected" runat="server" Text="Get Connected" Visible="false" CommandName="getconnected" CommandArgument='<%# Eval("Id") %>' />
                        </div>

                        <asp:Panel runat="server" ID="pnlHighFive" class="sendhighfive">
                            <div class="sendhighfivemessage" style="display:block">
                                <label>Message</label>
                                <asp:TextBox CssClass="msgText" runat="server" EnableViewState="true" ID="txtHighFiveMessage" TextMode="MultiLine"  Rows="5" Columns="50" />
                                <asp:LinkButton runat="server" ID="btnSendHighFive" CommandArgument='<%# Eval("Id") %>' CommandName="sendhighfive">Send</asp:LinkButton>
                                <asp:LinkButton CssClass="cancel" runat="server" ID="btnCancelHighFive" Text="Cancel" CommandArgument='<%# Eval("Id") %>' CommandName="cancel" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>--%>
            </EditItemTemplate>
        </asp:ListView>
    </div>

</asp:Panel>
<%--<asp:Panel runat="server" ID="pnlNotificationResults" CssClass="notificationresultsinactive" EnableViewState="true">
    <div class="notificationclickresult">
        <asp:ListView runat="server" ID="lvnotificationclickresult" DataKeyNames="ID" OnItemCommand="lvnotificationclickresult_ItemCommand" EnableViewState="true">
            <EmptyDataTemplate>
                No notification found.
            </EmptyDataTemplate>
            <ItemTemplate>
                <div class="notification">
                    <a class="notificationlink" href='<%#Eval("DetailURL") %>'>
                        <p><%# Eval("Message") %></p>
                    </a>

                    <asp:Panel runat="server" Visible='<%#Eval("HasButtons")%>'>
                        <asp:LinkButton runat="server" Text="Accept" CommandName="acceptfriendrequest" CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                        <asp:LinkButton runat="server" Text="Reject" CommandName="rejectfriendrequest" CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                    </asp:Panel>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Panel>--%>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FriendRequests.ascx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserControls.FriendRequests" %>
<div class="friendrequests">
    <asp:ListView ID="lvConnectionRequests" runat="server" OnItemCommand="lvConnectionRequests_ItemCommand" DataKeyNames="ID">
        <ItemTemplate>
            <div class="friendrequest">
                <a href='/UserProfile.aspx?tokennumber=<%#Eval("Source.TokenNumber") %>'>
                    <img style="width: 50px; height: 50px;" src='../Photos/<%# Eval("Source.Photo") %>' alt="Friends Photo" />
                    <p><%# Eval("Source.FullName") %></p>
                </a>

                <div>
                    <asp:LinkButton ID="lbAccept" CssClass="acceptbtn" runat="server" CommandName="accept" CommandArgument='<%#Eval("ID") %>'>Accept</asp:LinkButton>
                    <asp:LinkButton ID="lbDecline" CssClass="declinebtn" runat="server" CommandName="decline" CommandArgument='<%#Eval("ID") %>'>Decline</asp:LinkButton>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <div class="clearfloats"></div>
</div>


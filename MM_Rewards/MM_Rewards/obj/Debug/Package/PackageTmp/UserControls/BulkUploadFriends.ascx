﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BulkUploadFriends.ascx.cs"
    Inherits="MM_Rewards.UserControls.BulkUploadFriends" %>
<h1>
    UPLOAD FRIENDS
</h1>
<div id="MainDiv">
    <div style="width: 500px;">
        <p style="font-size:10pt; font-weight:bold;padding:10px">Please Select Excel File to Upload </p>
        <table style="width: 98%; text-align: left">
            <tr>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" Font-Bold="True" CssClass="btnLeft"
                            TabIndex="5" CausesValidation="False" OnClick="btnUpload_Click" /></div>
                </td>
            </tr>
            <tr>
                <td style="text-align: center" colspan="2">
                    <asp:Label ID="lblError" runat="server" Font-Italic="True" CssClass="error"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</div>

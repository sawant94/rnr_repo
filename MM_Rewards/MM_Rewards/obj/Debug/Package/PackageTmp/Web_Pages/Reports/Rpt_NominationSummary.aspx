﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
     CodeBehind="Rpt_NominationSummary.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_NominationSummary" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>   
    <div class="form01">
        <table>
            <tr>
                <td colspan="3">
                    <h1>Quarterly Divisional Excellence Award Report</h1>
                </td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>                
                <td class="lblName">
                    <asp:Label ID="lblLeaderName" runat="server" Text="Leader Name"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>:
                </td>
                <td>
                    <asp:DropDownList ID="ddlLeaderName" runat="server" TabIndex="1"  Height="40px"  Width="145px" >                       
                    </asp:DropDownList>
                </td>
            </tr>           
           <tr>                
                <td class="lblName">
                    <asp:Label ID="lblFYear" runat="server" Text="Financial Year"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>:
                </td>
                <td>
                    <asp:DropDownList ID="ddlFYear" runat="server" TabIndex="1"  Height="40px"  Width="145px" >                                                
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>                
                <td class="lblName">
                    <asp:Label ID="lblQuarter" runat="server" Text="Leader Name"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>:
                </td>
                <td>
                    <asp:DropDownList ID="ddlQuarter" runat="server" TabIndex="1"  Height="40px"  Width="145px" >
                        <asp:ListItem Value="1" Text="Quarter 1"></asp:ListItem>                       
                        <asp:ListItem Value="2" Text="Quarter 2"></asp:ListItem>   
                        <asp:ListItem Value="3" Text="Quarter 3"></asp:ListItem> 
                        <asp:ListItem Value="4" Text="Quarter 4"></asp:ListItem> 
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" OnClick="btnGetDetails_Click"
                            Text="Generate Report" TabIndex="6" />
                    </div>
                </td>
            </tr>
        </table>

    </div>
    <div class="reportHold" runat="server" id="rpthold">
        <rsweb:ReportViewer ID="Nominationrptviewer" runat="server" InteractiveDeviceInfos="(Collection)" Width="100%">
            <LocalReport ReportPath="Web_Pages\Reports\Rpt_NominationSummary.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1"
                        Name="NominationSummary" />
<%--                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="ds_AllAwardsrppt" />--%>
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>

        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="GetNominationSummaryReports" TypeName="BL_Rewards.BL_Reports.Reports"
            OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                 <asp:ControlParameter ControlID="ddlLeaderName" Name="LeaderName"
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ddlFYear" Name="FinancialYear"
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ddlQuarter" Name="Quarter"
                    PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>

</asp:Content>

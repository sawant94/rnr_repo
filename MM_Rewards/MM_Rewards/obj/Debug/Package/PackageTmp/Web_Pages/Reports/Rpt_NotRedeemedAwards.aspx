﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Web_Pages/MM_Rewards.Master"
 CodeBehind="Rpt_NotRedeemedAwards.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_NotRedeemedAwards" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

<script type="text/javascript">

    function ValidatePage() {

        var errMsg = '';
        var vddl;
        var dpStartDate;
        var dpEndDate;
        vddl = document.getElementById('<%=ddl_division.ClientID %>');
        errMsg += CheckDropdown_Blank(vddl.value, " Division");
//        vddl = document.getElementById('<%=ddl_Location.ClientID %>');
//        errMsg += CheckDropdown_Blank(vddl.value, " Location");

        dpStartDate = $find('<%=dtpStartdate.ClientID %>');
        dpEndDate = $find('<%=dtpEnddate.ClientID %>');

        errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");
       

        if (errMsg != '') {
            alert(errMsg);
            return false;
        }
        else {
            return true;
        }
    }
    </script>
   <div class="form01" >
     <table >
      <tr>
                <td colspan="3">
                    <h1>Not Redeemed Awards</h1></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDivision" runat="server" Text="Division" ></asp:Label> <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddl_division" runat="server" TabIndex="1" 
                        onselectedindexchanged="ddl_division_SelectedIndexChanged1" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLoaction" runat="server" Text="Location"></asp:Label>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddl_Location" runat="server"  TabIndex="2">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Start Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="3">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td >
                    End Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="4">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
 <td></td>
             <td></td>
                <td>
                    <div class="btnRed">
                    <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" 
                        Text="Generate Report" TabIndex="5" onclick="btnGetDetails_Click" OnClientClick="return ValidatePage()"/>
                        </div>
                </td>
            </tr>
          </table>
            </div>
               <div class="reportHold" runat="server" id="rpthold"> 
          
            <rsweb:ReportViewer ID="rpvNotRedeemedAward" runat="server" InteractiveDeviceInfos="(Collection)"  CssClass="rpTab">
                <LocalReport ReportPath="Web_Pages\Reports\NotRedeemedAwards.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource_NotRedeemed" 
                            Name="DataSet_NotRedeemedAwards" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
           
            
                <asp:ObjectDataSource ID="ObjectDataSource_NotRedeemed" runat="server" 
                    SelectMethod="GetNotRedeemedAwardsReport" 
                    TypeName="BL_Rewards.BL_Reports.Reports">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddl_division" Name="Division" 
                            PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="ddl_Location" Name="Location" 
                            PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="dtpStartdate" Name="start" 
                            PropertyName="SelectedDate" Type="String" />
                        <asp:ControlParameter ControlID="dtpEnddate" Name="end" 
                            PropertyName="SelectedDate" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            
               </div>
</asp:Content>

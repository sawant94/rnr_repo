﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="RptUserBudgetQuarterly.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.RptUserBudgetQuarterly" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }

        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript">

        function ValidatePage() {

            var errMsg = '';
            var vddl;
            var dpStartDate;
            var dpEndDate;


            dpStartDate = $find('<%=dtpStartdate.ClientID %>');
            dpEndDate = $find('<%=dtpEnddate.ClientID %>');

            errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <div class="form01">
        <table>
            <tr>
                <td colspan="3">
                    <h1>User Budget Quarterly Report</h1>
                </td>
            </tr>
            <tr>
                <td class="lblName">Start Date <span style="color: Red">*</span>
                </td>
                <td>:
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="4">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">End Date <span style="color: Red">*</span>
                </td>
                <td>:
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="5">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" OnClick="btnGetDetails_Click"
                            Text="Generate Report" TabIndex="6" OnClientClick="return ValidatePage()" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:HiddenField ID="hndIsrnr" runat="server" />
                </td>
            </tr>
        </table>

    </div>
    <div class="reportHold" runat="server" id="rpthold">
        <rsweb:ReportViewer ID="BudgetQuarterly" runat="server" InteractiveDeviceInfos="(Collection)" Width="100%">
            <LocalReport ReportPath="Web_Pages\Reports\RptUserBudgetQuarterly.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1"
                        Name="dsUserBudgetQuarterly" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>

        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="GetUserBudgetQuarterlyReport" TypeName="BL_Rewards.BL_Reports.Reports"
            OldValuesParameterFormatString="original_{0}">
            <SelectParameters>               
                <asp:ControlParameter ControlID="dtpStartdate" DefaultValue="" Name="start"
                    PropertyName="SelectedDate" Type="String" />
                <asp:ControlParameter ControlID="dtpEnddate" Name="end"
                    PropertyName="SelectedDate" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>

</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_RewardsSales.Master"
    AutoEventWireup="true" CodeBehind="Rpt_ShowSalesReport.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.Rpt_ShowSalesReport" %>

<%@ Register Src="~/UserControl/SalesReportControl.ascx" TagName="SalesReportControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <uc1:SalesReportControl ID="ReportControl11" runat="server" />
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Rpt_UserFeedback.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_UserFeedback" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("[id$=dateInput_text]").attr('readonly', 'readonly');
            //$("[id$=dtpEnddate_dateInput_text]").attr('readonly', 'readonly');
        });
    </script>

    <script type="text/javascript">

        function ValidatePage() {

            var errMsg = '';
            var dpStartDate;
            var dpEndDate;
            dpStartDate = $find('<%=dtpStartdate.ClientID %>');
            dpEndDate = $find('<%=dtpEnddate.ClientID %>');

            errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <div class="form01">
        <table>
           <tr>
                <td colspan="3">
                    <h1>User Feedback Report</h1></td>
            </tr>
            <tr>
                <td class="lblName">
                    Start Date<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:raddatepicker id="dtpStartdate" runat="server" dateinput-dateformat="dd/MM/yyyy"
                        tabindex="3">
                    </telerik:raddatepicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    End Date<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:raddatepicker id="dtpEnddate" runat="server" dateinput-dateformat="dd/MM/yyyy"
                        tabindex="4">
                    </telerik:raddatepicker>
                      <asp:CompareValidator ID="dateCompareValidator" runat="server" 
                        ControlToValidate="dtpEnddate" ControlToCompare="dtpStartdate" 
                        Operator="GreaterThan" Type="date" 
                        ErrorMessage="The end date must be greater than the start date."
                        CssClass="errorMessage">
                    </asp:CompareValidator>
                </td>
            </tr>
            <tr>
               <td colspan="2">
               </td>
               <td>
                    <div class="btnRed">
                        <asp:Button ID="btnGetDetails" Width="150px" CssClass="btnLeft" runat="server" Text="Generate Report"
                            TabIndex="3" OnClick="btnGetDetails_Click" OnClientClick="return ValidatePage()" />
                    </div>
                </td>
            </tr>
        </table>
        </div>
       <div class="reportHold" runat="server" id="rpthold"> 
            <rsweb:ReportViewer ID="rptfeedback" runat="server" CssClass="rpTab" 
        InteractiveDeviceInfos="(Collection)" >

                <LocalReport ReportPath="Web_Pages\Reports\UserFeedBack.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                            Name="ds_FeedBackRpt" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
       
        
        
    
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetUserFeedback" TypeName="BL_Rewards.BL_Reports.Reports">
        <SelectParameters>
            <asp:ControlParameter ControlID="dtpStartdate" Name="start" 
                PropertyName="SelectedDate" Type="String" />
            <asp:ControlParameter ControlID="dtpEnddate" Name="end" 
                PropertyName="SelectedDate" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
       
        </div>
        
    
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Rpt_Rnr_Repot.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_Rnr_Repot" %>

<%@ Register Src="../../UserControl/RnrReportControl.ascx" TagName="ReportControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
 
    <script src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("[id$=dateInput_text]").attr('readonly', 'readonly');
            //$("[id$=dtpEnddate_dateInput_text]").attr('readonly', 'readonly');
        });
    </script>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<h1>
        RnR Report</h1>--%>
    <uc1:ReportControl ID="RptKindAward" runat="server" />
</asp:Content>

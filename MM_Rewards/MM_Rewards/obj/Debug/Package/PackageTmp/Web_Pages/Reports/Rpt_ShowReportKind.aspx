﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Rpt_ShowReportKind.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_BudgetUtilizationKind" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function ValidatePage() {
            var errMsg = '';
            var vddl;


            vddl = document.getElementById('<%=rlbBusiness.ClientID %>');

            if (vddl != null) {
                var chkList = vddl.getElementsByTagName("input");
                var CountStatement = 0;

                for (i = 0; i < chkList.length; i++) {
                    if (chkList[i].checked) {
                        CountStatement = CountStatement + 1;
                    }
                }

                if (CountStatement == 0) {
                    errMsg += "Please Select Business Unit.\n";
                }
            }


            vddl = document.getElementById('<%=rlbdivision.ClientID %>');

            if (vddl != null) {
                var chkList = vddl.getElementsByTagName("input");
                var CountStatement = 0;

                for (i = 0; i < chkList.length; i++) {
                    if (chkList[i].checked) {
                        CountStatement = CountStatement + 1;
                    }
                }

                if (CountStatement == 0) {
                    errMsg += "Please Select Division.\n";
                }
            }

            vddl = document.getElementById('<%=rlbLocation.ClientID %>');

            if (vddl != null) {
                var chkList = vddl.getElementsByTagName("input");
                var CountStatement = 0;

                for (i = 0; i < chkList.length; i++) {
                    if (chkList[i].checked) {
                        CountStatement = CountStatement + 1;
                    }
                }

                if (CountStatement == 0) {
                    errMsg += "Please Select Location.\n";
                }
            }


            dpStartDate = $find('<%=dtpStartdate.ClientID %>');
            dpEndDate = $find('<%=dtpEnddate.ClientID %>');
            errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }

        function onItemChecked(sender, e) {
            debugger;
            var item = e.get_item();
            var items = sender.get_items();
            var checked = item.get_checked();
            var firstItem = sender.getItem(0);
            if (item.get_text() == "ALL") {
                items.forEach(function (itm) { itm.set_checked(checked); });
            }
            else {
                if (sender.get_checkedItems().length == items.get_count() - 1) {
                    firstItem.set_checked(!firstItem.get_checked());
                }
            }
        }
    </script>
    <div class="form01">
        <table class="budgetReport">
            <tr>
                <td colspan="3">
                    <h1>
                        Budget Utilization Details For Kind Services</h1>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDepartment" runat="server" Text="Business Unit"></asp:Label>
                    &nbsp;<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadListBox ID="rlbBusiness" runat="server" CheckBoxes="true" Width="200px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnClientItemChecked="onItemChecked"
                        OnItemCheck="rlbBusiness_ItemCheck">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadListBox ID="rlbdivision" runat="server" CheckBoxes="true" Width="200px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnItemCheck="rlbdivision_ItemCheck"
                        OnClientItemChecked="onItemChecked">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadListBox ID="rlbLocation" runat="server" CheckBoxes="true" Width="200px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnClientItemChecked="onItemChecked"
                        OnItemCheck="rlbLocation_ItemCheck">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Start Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="3">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    End Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="4">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    HOD Token Number&nbsp;
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtHODTokenNumber" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hndIsrnr" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text="Generate Report"
                            TabIndex="3" OnClick="btnGetDetails_Click" OnClientClick="return ValidatePage()" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnExportToExcel" CssClass="btnLeft" runat="server" Text="Export To Excel"
                            TabIndex="3" OnClick="btnExportToExcel_Click" OnClientClick="return ValidatePage()" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="reportHold" runat="server" id="rpthold">
        <rsweb:ReportViewer ID="rpvBudgetUtilization" runat="server" InteractiveDeviceInfos="(Collection)"
            CssClass="rpTab">
            <LocalReport ReportPath="Web_Pages\Reports\BudgetUtilizationDetails.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource Name="BudgetUtilization" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
</asp:Content>

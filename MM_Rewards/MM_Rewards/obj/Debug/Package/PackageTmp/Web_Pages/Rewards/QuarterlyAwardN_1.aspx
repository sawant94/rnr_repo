﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="QuarterlyAwardN_1.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.QuarterlyAwardN_1" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
                .info_input {
            display: inline-block;
    padding: 1px 7px;
    position: absolute;
    left: 75px;
    line-height: 14px;
    border-radius: 50px;
    border: 0;
    background: #24a0ed;
    color: #fff;
    top: 0px;
    z-index: 1;
        }
            .info_input.sec_info_input {
                left: 105px;
            }

        .standard-input input {
            width: 98%;
            height: 47px;
            margin: 1px;
            border: 0px;
            padding: 10px 20px;
        }
        .gridinput {
            border: 0;
            box-shadow: #dadada;
            box-shadow: 0 1px 6px 0 rgb(32 33 36 / 28%);
            margin-top: 10px;
            height: 30px;
            width: 100px;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons {
            margin: 0;
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
            float: none;
        }

            #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
                display: inline-block;
            }

        .send-ecard .nom-box-top h5 input {
            font-weight: 600;
            font-size: 20px;
            margin-bottom: 0;
            padding: 0;
        }
        
        #ContentPlaceHolder1_grdProjectGrid td:nth-child(2) {
            text-align: left !important;
        }  
        
        #ContentPlaceHolder1_grdQuarterlyAward td:nth-child(1) {
            text-align: left !important;
        } 

        .send-ecard .search-emp-m input {
            /*padding: 0;*/
            min-height: auto;
        }

        .send-ecard .search-emp-m ul input {
            padding: 0;
            font-size: 16px;
            width: 100%;
            color: #203442;
            font-weight: 500;
        }

        .send-ecard label.nom-box-lbl input {
            font-size: 11px;
            color: #fff!important;
            padding: 0;
        }

        .send-ecard .search-emp-m .send-ecard-message textarea {
            font-size: 18px;
            padding: 0 20px;
            min-height: 40px;
            border: 0;
            font-family: Calibri!important;
        }

        .send-awards.send-ecard input {
            margin: 0 auto;
            width: auto;
            padding: 10px 35px;
        }

        .send-awards input {
            width: 270px;
            padding: 9px;
            font-size: 20px;
            font-weight: 500;
            border-radius: 6px;
        }

        .grdAwards_new th {
            padding: 0px 10px !important;
        }

        div.grid01 table tr th:last-child td {
            padding: 0px 10px !important;
        }
        .Hide{
            display:none;
        }

        input.rnr-btn {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 110px;
        }

        input.rnr-btnGrid {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 20px;
        }

        .rnr-green input {
            font-weight: 600;
            margin: 0 0 -4px;
            font-size: 35px;
            -webkit-appearance: none;
            background: transparent;
            border: 0;
            color: #30ce8d!important;
        }

        #ContentPlaceHolder1_TxtAmount {
            background-color: #eee;
            padding: 0 12px;
            width: 100%;
        }

         table.grdAwards_new td:nth-child(1){
            white-space: inherit !important;
            
        }

         #ContentPlaceHolder1_grdProjectGrid_rdAwardDecisionProjectGrid_0 td:nth-child(1){
            white-space: inherit !important;
            display: inherit !important;
        }

         div.grid01 table tr th:last-child, div.listDtls table tr th:last-child {
            text-align: center !important;
        }

        .container-margin {
            position: relative;
            margin-bottom: 120px;

        }

        div.grid01 table td table tr {
            float: inherit !important;
        }

        #SummaryPanel {
            padding-top: 0px;
        }

        .rnr-radio-btn table tr td span::after {
            content: attr(data-value);
            position: absolute;
            width: 125px;
            left: 30px;
        }

        .rnr-table {
            margin-top: 0;
        }

        .tam_text {
            font-size: 20px;
            font-weight: 600;
        }

        abbr[data-title] {
  position: relative;
  text-decoration: underline dotted;
}

abbr[data-title]:hover::after,
abbr[data-title]:focus::after {
  content: attr(data-title);
  position: absolute;
  left: 60px;
  top: -15px;
  transform: translateX(-50%);
  width: auto;
  white-space: nowrap;
  background: #f9f9f9;
  color: #000;
  border-radius: 2px;
  box-shadow: 1px 1px 5px 0 rgba(0, 0, 0, 0.4);
  font-size: 14px;
  padding: 3px 5px;
}

.rnr-breadcrumb {
    padding: 2px 20px 0 0;
}

.info_input2 {
    display: inline-block;
    padding: 1px 7px;
    position: absolute;
    left: -25px;
    line-height: 14px;
    border-radius: 50px;
    border: 0;
    background: #24a0ed;
    color: #fff;
    top: -38px;
    z-index: 1;
}

.rnr-h3-top {
    color: #fff;
    font-size: 28px;
    font-weight: 300;
    text-align: center;
    transform: translateY(-10px);
    margin-top: 0px;
    position: relative;
    top: -14px;
}

.rnr-tab-pane.tab-content {
    position: relative;
    margin-top: 0;
    top: -25px;
}

.rnr-inner-top {
    position: fixed;
    width: 100%;
    top: 60px;
    z-index: 1;
    background-color: #203442;
    height: 65px;
}

ul.breadcrumb {
    background: transparent; 
}

ul.breadcrumb li + li:before {
    color: #000;
}

.rnr-tabs h3.rnr-h3-top {
    color: #000;
}

.appendin {
    display: block;
    width: 225px;
}

.span-des {
    top: 40px;
    position: relative;
    left: 29px;
        }
        .btn-Nomination {            
    border: none;
    border-radius: 5px;
    color: #fff;
    background-color: #30ce8d;
    padding: 5px 5px;
    font-weight: 600;
    font-size: 16px;

        }

        td span.AlignRight {
            width: 175px;
            display: block;
        }
    </style>

    <section class="main-rnr">
        <div class="rnr-inner-top" style="">
            <div class="rnr-breadcrumb">
            
           <ul class="breadcrumb">
                    <li><a href="/Web_Pages/HomePage.aspx">HOME</a></li>
                    <li>Quarterly Divisional Excellence Awards</li>
                </ul>                                
            </div>
            
        </div>

        <div class="rnr-tabs" style="top: 155px; position: relative; z-index: 0;">      
                 <h3 class="rnr-h3-top">Quarterly Divisional Excellence Awards for FY- 
                <asp:Label ID="lblFYear" runat="server"></asp:Label>
                and Quarter- 0<asp:Label ID="lblQuarter" runat="server"></asp:Label>

            </h3>
            <div class="rnr-tab-pane tab-content">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <asp:HiddenField ID="hdnStartDate" runat="server" />
                    <asp:HiddenField ID="hdnEndDate" runat="server" />
                    <asp:HiddenField ID="hdnQuarter" runat="server" />

                    <div class="container" style="margin-bottom: 30px;">
                      <div class="row">
                        <div class="col-md-6">                            
                            <p class="tam_text">
                                <span>Total Count in List :   </span><span class="t_amount">
                                    <asp:Label ID="lblCount" runat="server"></asp:Label></span>
                            </p>
                              
                            <%--<span id="infotext" Class="info_tt">For awarding separate amounts, Click on 'View' button.</span>--%>
                        </div>
                        <div class="col-md-6 text-right">
                            <asp:Button ID="btnNominationMaster" runat="server" Text="ADD NOMINATIONS"  OnClick="btnNominationMaster_Click"  CssClass="btn-Nomination"/>
                            &nbsp;
                            <asp:Button ID="btnExport" runat="server" Text="Export Summary"  CssClass="btn-Nomination" OnClick="btnExport_Click"/>
                            &nbsp;
                            <asp:Button ID="btnExportDetail" runat="server" Text="Export Detail"  CssClass="btn-Nomination" OnClick="btnExportDetail_Click" />

                        </div>
                          </div>
                        <div class="rnr-table" id="SummaryPanel">
                            <div id="Div2" class="table-scroll">
                                <div class="grid01" id="div3" runat="server">
                                    
                                    <asp:GridView ID="grdProjectGrid" runat="server" AllowSorting="true" Visible="True" AllowPaging="true"
                                        AutoGenerateColumns="false" AlternatingRowStyle-CssClass="row01" Style="margin: 0 auto;"
                                        CssClass="grdAwards_new" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                        Height="100%" Width="100%" OnRowDataBound="grdProjectGrid_RowDataBound"  OnPageIndexChanging="grdProjectGrid_PageIndexChanging" OnRowCommand="grdProjectGrid_RowCommand" >
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>Select</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rdSelect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px" Visible="false">
                                                <HeaderTemplate>ReferenceNo</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMasterReferenceNoProjectGrid" runat="server" Text='<%# Bind("MasterReferenceNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left"  ItemStyle-Width="300px">
                                                <HeaderTemplate>Title</HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStoryProjectGrid" runat="server" Text='<%# Bind("Story") %>' CssClass="AlignRight"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>Nominated By</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreatedByNameProjectGrid" runat="server" Text='<%# Bind("CreatedByName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>Leader's Name</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAFCLNameProjectGrid" runat="server" Text='<%# Bind("AFLCName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>Function</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBusinessUnitProjectGrid" runat="server" Text='<%# Bind("BusinessUnit") %>'></asp:Label>
                                                    <asp:HiddenField ID="hdnSynopsisProjectGrid" runat="server" Value='<%# Bind("Synopsis") %>' />
                                                    <asp:HiddenField ID="hdnBusinessImpact" runat="server" Value='<%# Bind("BusinessImpact") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>                                          
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50px">
                                                <HeaderTemplate>Participants</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeCountProjectGrid" runat="server" Text='<%# Bind("EmployeeCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            
                                            <%--Comment by jayesh on 2nd july 2021--%>
                                           <%-- <asp:TemplateField HeaderStyle-CssClass="appendin" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>Amount/Team Member</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnAwardAmountIndividualProjectGrid" runat="server" Value='<%# Bind("AwardAmountIndividual") %>' />
                                                    <asp:RadioButtonList ID="rdAwardAmountProjectGrid" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdAwardAmountProjectGrid_SelectedIndexChanged">
                                                        <asp:ListItem Value="2000">2K</asp:ListItem>
                                                        <asp:ListItem Value="5000">5K</asp:ListItem>
                                                        <asp:ListItem Value="10000">10K</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>Total Amount</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalAmountProjectGrid" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                               --%>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>Action</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnAwardDecisionProjectGrid" runat="server" Value='<%# Bind("AwardDecision") %>' />
                                                    <asp:RadioButtonList ID="rdAwardDecisionProjectGrid" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="Approve" style="display: inherit" >Approve</asp:ListItem>
                                                        <asp:ListItem Value="Reject" >Reject</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                                <HeaderTemplate>View</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnViewProjectGrid" runat="server" Text="View"
                                                        CssClass="table-anchor rnr-green" CommandArgument='<%# Bind("MasterReferenceNo") %>' CommandName="View" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                            BorderStyle="Solid" Font-Italic="true" />
                                        <EmptyDataTemplate>
                                            <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No Nominations Present"></asp:Literal>
                                        </EmptyDataTemplate>
                                        <PagerStyle CssClass="dataPager" />
                                        <SelectedRowStyle CssClass="selectedoddTd" />
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3" style="">
                                <asp:Button ID="btnSubmitProjectGrid" runat="server" Text="Submit" Width="200px" CssClass="rnr-btn" OnClick="btnSubmitProjectGrid_Click"  />
                            </div>
                        </div>
                    </div>
                    <br />
                    <%--<div class="modal fade search-emp-modal" id="DetailPanel" runat="server" role="dialog" aria-labelledby="exampleModalLabel">--%>
                    <div class="container container-margin">
                        <div class="d_panel"></div>
                        <div id="DetailPanel" runat="server">
                            <div class="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mahindra_drop_down_div rnr-dd award-top-dd standard-input">
                                            <label for="">Title</label>
                                            <asp:TextBox ID="txtStoryName" runat="server" TabIndex="5" MaxLength="200" ReadOnly="true" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mahindra_drop_down_div rnr-dd award-top-dd standard-input">
                                            <label for="">Leader's Remark</label>
                                            <asp:TextBox ID="txtRemark" runat="server" TabIndex="5" MaxLength="200" ReadOnly="true" ></asp:TextBox>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <div class="rnr-input">
                                            <label for="">Synopsis</label>
                                            <label class="min-char" for=""><span id="spnTxtCount"></span></label>
                                            <asp:TextBox ID="txtSynopsis" runat="server" TextMode="MultiLine" Rows="4" ReadOnly="true" TabIndex="5" MaxLength="200" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <div class="rnr-input">
                                            <label for="">Business Impact</label>
                                            <label class="min-char" for=""><span id="Span1"></span></label>
                                            <asp:TextBox ID="txtBusinessImpact" runat="server" TextMode="MultiLine" Rows="4" ReadOnly="true" TabIndex="5" MaxLength="200" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <br />

                                </div>
                            </div>
                            <div class="rnr-table">
                                <div id="Div1" class="table-scroll">
                                    <div class="grid01" id="divNominees" runat="server">
                                        <asp:GridView ID="grdQuarterlyAward" runat="server" AllowSorting="true" Visible="True" AllowPaging="true"
                                            AutoGenerateColumns="false" AlternatingRowStyle-CssClass="row01" Style="margin: 0 auto;"
                                            CssClass="grdAwards_new" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                            Height="100%" Width="100%" >
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="300px">
                                                    <HeaderTemplate>Title</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("Id") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Story") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Visible="true">
                                                    <HeaderTemplate>Reference Number</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMasterReferenceNo" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200px">
                                                    <HeaderTemplate>Participants Name</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnRecipientTokenGrid" runat="server" Value='<%# Bind("RecipientToken") %>' />
                                                        <asp:Label ID="lblRecipientNameGrid" runat="server" Text='<%# Bind("RecipientName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200px">
                                                    <HeaderTemplate>Participants Token</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRecipientTokenGrid" runat="server" Text='<%# Bind("RecipientToken") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>Business Unit</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnAFLCToken" runat="server" Value='<%# Bind("AFLCToken") %>' />
                                                        <asp:HiddenField ID="hdnAFLCName" runat="server" Value='<%# Bind("AFLCName") %>' />
                                                        <asp:Label ID="lblBusinessUnitGrid" runat="server" Text='<%# Bind("BusinessUnit") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                            </Columns>
                                            <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                BorderStyle="Solid" Font-Italic="true" />
                                            <EmptyDataTemplate>
                                                <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No Nominations Present"></asp:Literal>
                                            </EmptyDataTemplate>
                                            <PagerStyle CssClass="dataPager" />
                                            <SelectedRowStyle CssClass="selectedoddTd" />
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script type="text/javascript" src="../../Scripts/jquery-3.5.1.min.js"></script>
    <script>


        $(document).ready(function () {
            $('.appendin').append('<span class="span-des" draggable="false"><abbr data-title="For awarding separate amounts, Click on View button."><asp:Button ID="btnBehaviourdisplay" runat="server" cssClass="info_input2 sec_info_input" Text="i" /></abbr></span>');
            $("#ContentPlaceHolder1_btnBehaviourdisplay").hover(function () {
                //alert('Hi');
                $("#infotext").modal('toggle');
            });
        });

    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="PersonalizedMessage.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.PersonalizedMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style> 
    <style>
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            padding-top: 5%; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            border: 1px solid #888;
            width: 600px;
            position: relative;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            box-shadow: 0px 0px 5px 3px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0px 0px 5px 3px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0px 0px 5px 3px rgba(0, 0, 0, 0.3);
            overflow: hidden;
        }

        .modal_inner {
            height: auto;
            overflow-y: auto;
            padding: 20px;
        }

        /* The Close Button */
        .closemodal {
            color: #fff;
            float: right;
            font-size: 28px;
            font-weight: bold;
            position: absolute;
            right: 15px;
            top: 0;
        }

            .closemodal:hover,
            .closemodal:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .tbl_rec_list {
            width: 100%;
            border-collapse: collapse;
        }

            .tbl_rec_list tr td {
                padding: 3px 7px;
                border: 1px solid #888;
            }

            @media screen and (max-width: 360px){
                div.ftHold {
                    position: fixed;
                }
            }
    </style>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script>
        function ClosePopup() {
            $('.modal').css('display', 'none');
            scrollWin();
        }
    </script>
  <script type="text/javascript" language="javascript">


      function ValidatePage() {

          var r = confirm("Do you want to submit record ?");
          //alert(r);
          if (r != true) {
              return false;
          }

          var errMsg = '';

          var txtmessage = document.getElementById('<%= txtmessage.ClientID %>');
          errMsg = CheckText_Blank(txtmessage.value, 'Message');
     
          if (errMsg != '') {
              alert(errMsg);
              return false;
          }
          else {
              return true;
          }
      }
      function ClearFields() {

          var grdMessageList = document.getElementById('<%= grdMessageList.ClientID %>');
          var txttokennumber = document.getElementById('<%= txttokennumber.ClientID %>');
          var txtmessage = document.getElementById('<%= txtmessage.ClientID %>');
          var txtrecipientname = document.getElementById('<%= txtrecipientname.ClientID %>');
          var divedit = document.getElementById('<%= divedit.ClientID %>');
          hdnid = document.getElementById('<%= hdnid.ClientID %>');                  
          hdnSelectedRowIndex = document.getElementById('<%= hdnSelectedRowIndex.ClientID %>');
          txttokennumber.value = "";
          txtmessage.value = "";
          txtrecipientname.value = "";
          hdnid.value = "";

          if (hdnSelectedRowIndex.value == "")
              return false;
          var row = grdMessageList.rows[parseInt(hdnSelectedRowIndex.value) + 1];
          row.style.backgroundColor = '#ffEECC';
          UnselectRow(row);
           hdnSelectedRowIndex.value = "";
          $('.modal').css('display', 'none');

          return false;
      }




    </script>

    <div class="form01">
        <h1>
            PERSONALIZE MESSAGE
        </h1>
        <div>
            <%--<iframe src="https://qaapps.mahindra.com/sap/bc/ui5_ui5/sap/ZHR_MILESTONES/index.html" width="100%" height="400" style="border: 2px solid #272727;"></iframe>--%>

            <iframe src="https://www.tutorialspoint.com/" width="100%" height="500" style="border: 2px solid #272727;"></iframe>
        </div>
        <div class="grid01" style="overflow-x: auto;">
            <asp:HiddenField ID="hdnSelectedRowIndex" runat="server" />
            <asp:GridView ID="grdMessageList" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                AlternatingRowStyle-CssClass="row01" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                AllowPaging="true" onpageindexchanging="grdMessageList_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="RecipientTokenNumber" HeaderText="Token Number" />
                    <asp:BoundField DataField="RecipientName" HeaderText="Employee Name" />
                    <asp:BoundField DataField="Years" HeaderText="Year" />
                    <asp:BoundField DataField="MilestoneDate" HeaderText="Milestone Date" />
                    <asp:BoundField DataField="RNREmailID" HeaderText="EmailID"  />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%-- <asp:Label ID="lblTemplate" runat="server" Visible="true" Text='<%# Eval("Template") %>' style="display:none" ></asp:Label>--%>
                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("ID")  %>' CommandName="Select"
                                OnClick="lnkSelect_Click" runat="server" CssClass="selLink" TabIndex="1">                              
                            Select</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                    BorderStyle="Solid" Font-Italic="true" />
                <EmptyDataTemplate>
                    <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                </EmptyDataTemplate>
                <PagerStyle CssClass="dataPager" />
                <SelectedRowStyle CssClass="selectedoddTd" />
            </asp:GridView>
        </div>
    </div>
    <%-- vaibhav added popupdiv class --%>
    <div runat="server" id="myModal" class="modal">
        <div class="modal-content">
            <label style="padding: 10px 15px; background-color: #d31621; color: #fff; display: block;">Milestone Message</label>
            <span onclick="ClosePopup();" class="closemodal">&times;</span>
            <div class="modal_inner">
                <div class="form01 formformobonly" id="divedit" runat="server">
                    <table>
                        <tr>
                            <td class="lblName">
                                <asp:Label ID="lbltokennumber" runat="server" Text="Recipient TokenNumber"></asp:Label>
                            </td>
                            <td class="hidden-mob">:
                            </td>
                            <td>
                                <asp:TextBox ID="txttokennumber" runat="server" TabIndex="1" Enabled="false"></asp:TextBox>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="lblName">
                                <asp:Label ID="lblrecipientname" runat="server" Text="Recipient Name"></asp:Label>
                            </td>
                            <td class="hidden-mob">:
                            </td>
                            <td>
                                <asp:TextBox ID="txtrecipientname" runat="server" TabIndex="2" Enabled="false"></asp:TextBox>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblmessage" runat="server" Text="Personalize Message" CssClass="lblName"></asp:Label><span
                                    style="color: Red">*</span>
                            </td>
                            <td class="hidden-mob">:
                            </td>
                            <td>
                                <%-- vaibhav changes MaxLength 150 to 500 --%>
                                <asp:TextBox ID="txtmessage" runat="server" MaxLength="500" TextMode="MultiLine"
                                    Rows="5" Width="250px" TabIndex="3"></asp:TextBox>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="btnRed">
                                    <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btnLeft" OnClientClick="return ValidatePage()"
                                        TabIndex="4" OnClick="btnsave_Click" />
                                </div>
                                <div class="btnRed">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" OnClientClick="return ClearFields()"
                                        TabIndex="5" />
                                </div>
                                <div style="clear: both;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hdnid" runat="server" />
                                <asp:HiddenField ID="hdnRREmailID" runat="server" />
                                <asp:Literal ID="ltyear" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="ltMilestoneDate" runat="server" Visible="false"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
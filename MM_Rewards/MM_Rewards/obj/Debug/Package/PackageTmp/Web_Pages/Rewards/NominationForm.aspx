﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="NominationForm.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.NominationForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .standard-input input {
            width: 98%;
            height: 47px;
            margin: 1px;
            border: 0px;
            padding: 10px 20px;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons {
            margin: 0;
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
            float: none;
        }

            #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
                display: inline-block;
            }

        .send-ecard .nom-box-top h5 input {
            font-weight: 600;
            font-size: 20px;
            margin-bottom: 0;
            padding: 0;
        }

        .send-ecard .search-emp-m input {
            /*padding: 0;*/
            min-height: auto;
        }

        .send-ecard .search-emp-m ul input {
            padding: 0;
            font-size: 16px;
            width: 100%;
            color: #203442;
            font-weight: 500;
        }

        .send-ecard label.nom-box-lbl input {
            font-size: 11px;
            color: #fff!important;
            padding: 0;
        }

        .send-ecard .search-emp-m .send-ecard-message textarea {
            font-size: 18px;
            padding: 0 20px;
            min-height: 40px;
            border: 0;
            font-family: Calibri!important;
        }

        .send-awards.send-ecard input {
            margin: 0 auto;
            width: auto;
            padding: 10px 35px;
        }

        .send-awards input {
            width: 270px;
            padding: 9px;
            font-size: 20px;
            font-weight: 500;
            border-radius: 6px;
        }

        input.rnr-btn {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 110px;
        }

        .rnr-green input {
            font-weight: 600;
            margin: 0 0 -4px;
            font-size: 35px;
            -webkit-appearance: none;
            background: transparent;
            border: 0;
            color: #30ce8d!important;
        }

        #ContentPlaceHolder1_TxtAmount {
            background-color: #eee;
            padding: 0 12px;
            width: 100%;
        }
        .up_btn_box {
            padding-top: 6px;
        }
        .up_btn_box input {
            display: inline-block;
            vertical-align: middle;
            margin-right: 10px;
        }
        .file-upload input {
            margin-top: -5px;
            margin-left: 17px;
        }
    </style>

    <section class="main-rnr">
        <div class="rnr-inner-top" style="">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href="/Web_Pages/HomePage.aspx">HOME</a></li>
                    <li>Nomination Form</li>
                </ul>

            </div>
        </div>

        <div class="rnr-tabs" style="margind-top: -40px;">

            <h3 class="rnr-h3-top">Quarterly Divisional Excellence Awards for FY
                <asp:Label ID="lblYear" runat="server"></asp:Label>
                and Quarter-0<asp:Label ID="lblQuarter" runat="server"></asp:Label>
                <asp:HiddenField ID="hdnQuarter" runat="server" />
            </h3>
            <div class="rnr-tab-pane tab-content" style="padding-top: 30px;">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <%--<div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd standard-input">
                                    <label for="">Financial Year</label>
                                    <asp:TextBox ID="txtFYear" runat="server" MaxLength="200" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Quarter</label>
                                    <asp:DropDownList ID="ddlQuarter" runat="server" Width="200px" CssClass="selectDivision" Enabled="false">
                                        <asp:ListItem Value="1" Text="Q1">Q1</asp:ListItem>
                                        <asp:ListItem Value="2" Text="Q2">Q2</asp:ListItem>
                                        <asp:ListItem Value="3" Text="Q3">Q3</asp:ListItem>
                                        <asp:ListItem Value="4" Text="Q4">Q4</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <div class="container">
                        
                    <div class="row hide">
                            <div class="col-md-6">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd file-attch" style="width: 100%;margin-bottom: 25px;">
                                    <label for="">Upload Nominee Excel</label>
                                    <div class="file-upload">
                                        <input type="file" name="ctl00$ContentPlaceHolder1$flupload" id="ContentPlaceHolder1_flupload">

                                        <br>
                                        <br> 
                                        <br>                                       
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-6">
                                <div class="up_btn_box">
                                        <input type="submit" name="ctl00$ContentPlaceHolder1$Button1" value="Upload" id="ContentPlaceHolder1_Button1" class="rnr-btn">
                                        <input type="submit" name="ctl00$ContentPlaceHolder1$Button2" value="Template Download" id="ContentPlaceHolder1_Button2" class="rnr-btn">
                                        <input type="submit" name="ctl00$ContentPlaceHolder1$Button5" value="Final Upload" id="ContentPlaceHolder1_Button5" class="rnr-btn">
                            </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Leader's Name</label>
                                    <asp:DropDownList ID="ddlALFCMember" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                                            
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd standard-input">
                                    <label for="">Title *</label>
                                    <asp:TextBox ID="txtStoryName" runat="server" TabIndex="2" MaxLength="200" ></asp:TextBox>

                                </div>                          
                                
                            </div>
                        </div>
                    </div>
                    <%--<div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Award Amount (For Individual)</label>
                                    <asp:DropDownList ID="ddlAwardAmount" runat="server" Width="200px" CssClass="selectDivision" TabIndex="4">
                                        <asp:ListItem Value="2500" Text="2500" Selected="True">2500</asp:ListItem>
                                        <asp:ListItem Value="5000" Text="5000">5000</asp:ListItem>
                                        <asp:ListItem Value="10000" Text="10000">10000</asp:ListItem>
                                    </asp:DropDownList>


                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="rnr-input">
                                    <label for="">Synopsis[Max 100 words] *</label>
                                    <label class="min-char" for=""><span id="spnTxtCount"></span></label>
                                    <asp:TextBox ID="txtSynopsis" runat="server" TextMode="MultiLine" Rows="4" TabIndex="3"  ></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="rnr-input">
                                    <label for="">Business Impact[Max 300 words] *</label>
                                    <label class="min-char" for=""><span id="Span1"></span></label>
                                    <asp:TextBox ID="txtBusinessImpact" runat="server" TextMode="MultiLine" Rows="4" TabIndex="4"  ></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="rnr-input">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd standard-input">
                                    <label for="">Business/Function *</label>
                                    <asp:TextBox ID="txtBusinessUnit" runat="server" TabIndex="5" MaxLength="200" ></asp:TextBox>
                                    </div>
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="rnr-input">
                                <div class="rnr-input search-emp-m" style="border: 0; width: 100%;">
                                    <label for="">Search Nominee</label>
                                    <div class="search-emp" data-toggle="modal" data-target="#ContentPlaceHolder1_searchempmodal">
                                        <div class="mess-dflex">
                                            <p>click</p>
                                        </div>
                                        <asp:TextBox ID="txtSearchEmp" runat="server" Rows="4" Enabled="false" TabIndex="7"></asp:TextBox>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.8 363.8">
                                            <title>Search</title>
                                            <g id="G8" data-name="Layer 2">
                                                <g id="G9" data-name="Layer 1">
                                                    <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                                </g>
                                            </g>
                                        </svg>

                                    </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="container">
                        <div class="row">

                            <div class="col-md-3" style="">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="200px" CssClass="rnr-btn" OnClientClick="return CheckTextLimit()" OnClick="btnSubmit_Click"  TabIndex="8" />
                            </div>
                            <div class="col-md-3" style="">
                                <asp:Button ID="btnRedirect" runat="server" Text="Back To Quarterly Award" Width="300px" CssClass="rnr-btn" OnClick="btnRedirect_Click" />

                            </div>
                        </div>
                    </div>
                    <div class="container">
                                            <div class="rnr-table">
                        <div id="Div1" class="table-scroll">
                            <div class="grid01" style="background-color: transparent; width: 100%; overflow-x: auto; overflow-y: auto;" id="divNominees" runat="server">
                                <asp:GridView ID="grdNominees" runat="server" AllowSorting="true" Visible="True" AllowPaging="true"
                                    AutoGenerateColumns="false" AlternatingRowStyle-CssClass="row01" Style="margin: 0 auto;"
                                    CssClass="grdAwards_new" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>' OnRowCommand="grdNominees_RowCommand" OnPageIndexChanging="grdNominees_PageIndexChanging">
                                    <Columns>
                                        <asp:BoundField DataField="RecipientToken" HeaderText="Recipient" ReadOnly="true"
                                            Visible="true" ItemStyle-Width="100px"></asp:BoundField>
                                        <asp:BoundField DataField="RecipientName" HeaderText="Recipient Name"
                                            ReadOnly="true" Visible="true" ItemStyle-Width="50px"></asp:BoundField>
                                        <%--<asp:BoundField DataField="ManagerName" HeaderText="Manager Name"
                                            ReadOnly="true" Visible="true" ItemStyle-Width="150px"></asp:BoundField>
                                        <asp:BoundField DataField="Story" HeaderText="Story" ReadOnly="true"
                                            Visible="true" ItemStyle-Width="20px"></asp:BoundField>
                                        <asp:BoundField DataField="Synopsis" HeaderText="Synopsis" ReadOnly="true"
                                            Visible="true" ItemStyle-Width="20px"></asp:BoundField>
                                        <asp:BoundField DataField="BusinessImpact" HeaderText="Business Impact"
                                            ReadOnly="true" ItemStyle-Width="100px"></asp:BoundField>
                                        <asp:BoundField DataField="AwardAmount" HeaderText="AwardAmount"
                                            ReadOnly="true" ItemStyle-Width="100px"></asp:BoundField>--%>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                            <HeaderTemplate>Action</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Button ID="btnRemove" runat="server" Text="Remove" Width="50px"
                                                    CssClass="table-anchor rnr-green" CommandArgument='<%# Bind("RecipientToken") %>' CommandName="Remove" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                        BorderStyle="Solid" Font-Italic="true" />
                                    <EmptyDataTemplate>
                                        <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No Nominations Present"></asp:Literal>
                                    </EmptyDataTemplate>
                                    <PagerStyle CssClass="dataPager" />
                                    <SelectedRowStyle CssClass="selectedoddTd" />
                                </asp:GridView>

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- search emp modal start-->
        <%--<div class="modal fade search-emp-modal" id="searchempmodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" keyboard="false">--%>
        <div class="modal fade search-emp-modal" id="searchempmodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header rnr-modal-head">
                        <h5 class="modal-title">Search Nominee</h5>
                    </div>
                    <div class="modal-body">
                        <div class="rnr-search-bar">
                            <div class="search-form form">
                                <label for="" class="search-top-lbl">Refine Search</label>
                                <label>

                                    <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2" onkeypress="return blockSpecialChar(event)"></asp:TextBox>

                                    <div class="btnRed SearchButtons">
                                        <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text="" OnClick="btnGetDetails_Click" /><%--OnClick="btnGetDetails_Click" TabIndex="3" OnClientClick="return ValidateEmp()"--%>
                                    </div>
                                </label>

                            </div>
                        </div>

                        <div class="rnr-table">
                            <div id="Div2" class="table-scroll">
                                <div id="divEmailList" runat="server" class="grid01">
                                    <asp:GridView ID="grdEmpList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                        Visible="true" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                        OnPageIndexChanging="grdEmpList_PageIndexChanging" OnRowCommand="grdEmpList_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                            <asp:BoundField DataField="Name" HeaderText="Nominee Name" SortExpression="Name" />
                                            <asp:BoundField DataField="BusinessUnit" HeaderText="Business Unit" SortExpression="BusinessUnit" />
                                            <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                            <asp:BoundField DataField="LocationName_PSA" HeaderText="Location" SortExpression="LocationName_PSA" />
                                             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select" ToolTip="Select Nominees for Award"
                                                        runat="server" CssClass="selLink1" TabIndex="4">
                                                Select</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="dataPager" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnClose" runat="server" Text="Close"  OnClick="btnClose_Click" CssClass="rnr-btn" />
                    </div>
                </div>
            </div>
        </div>
        <!-- search-emp-modal end -->
        
    </section>
</asp:Content>

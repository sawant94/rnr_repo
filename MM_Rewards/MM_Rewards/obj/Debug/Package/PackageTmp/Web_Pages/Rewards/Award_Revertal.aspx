﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="Award_Revertal.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.Award_Revertal" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
   <script src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("[id$=dateInput_text]").attr('readonly', 'readonly');
            //$("[id$=dtpEnddate_dateInput_text]").attr('readonly', 'readonly');
        });
    </script>
  
<script type="text/javascript">


    function btnGet_OnClientClick() {
        if (dtpStartdate.get_selectedDate() != null || dtpEnddate.get_selectedDate() != null) {
            var dtpStartdate = $find('<%#dtpStartdate.ClientID %>');
            var dtpEnddate = $find('<%#dtpEnddate.ClientID %>');

            var err = '';
            err += Check_StartEndDate(dtpStartdate.get_selectedDate() == null ? '' : dtpStartdate.get_selectedDate(), dtpEnddate.get_selectedDate() == null ? '' : dtpEnddate.get_selectedDate(), "Start", "End");

            if (err != "") {
                alert(err);
                return false;
            }
            return true;
        }
        return true;
    }

    function Check_StartEndDate(SDate, EDate, strStart, strEnd) {
        var endDate = new Date(EDate);
        var startDate = new Date(SDate);

        var err = '';

        err += chkDate_Blank(SDate, strStart + " Date");
        err += chkDate_Blank(EDate, strEnd + " Date");

        if (SDate != '' && EDate != '' && startDate > endDate) {
            err += "Please ensure that the " + strEnd + " Date is greater than or equal to the " + strStart + " Date.";
        }

        return err;
    }

    function chkDate_Blank(strValue, strSuffix) {
        if (strValue == '') {
            return 'Please Select ' + strSuffix + ' \n';
        }
        else {
            return '';
        }
    }

    </script>
        
        <div class="form01">
        <h1>Awards Revertal</h1>
            <table>
                <tr>
                    <td class="lblName">
                        Start Date
                    </td>
                    <td> :</td>
                    <td>
                        <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="1">
                        </telerik:RadDatePicker>
                      
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        End Date 
                    </td>
                     <td> :</td>
                    <td>
                        <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="2">
                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="2">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="2"></DatePopupButton>
                        </telerik:RadDatePicker>
                          <asp:CompareValidator ID="dateCompareValidator" runat="server" 
                        ControlToValidate="dtpEnddate" ControlToCompare="dtpStartdate" 
                        Operator="GreaterThan" Type="date" 
                        ErrorMessage="The end date must be later than the start date."
                        CssClass="errorMessage">
                    </asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Token Number 
                    </td>
                     <td> :</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTokenNumber"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                   
                    <td >
                        <div class="btnRed">
                            <asp:Button ID="btnGet" runat="server" Text="GET" CssClass="btnLeft" OnClick="btnGet_Click"
                                OnClientClick="return btnGet_OnClientClick()" TabIndex="3" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
            </table>

            <div class="grid01">
                <asp:GridView ID="grdAwards" runat="server" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                    AutoGenerateColumns="false" DataKeyNames="ID" OnPageIndexChanging="grdAwards_PageIndexChanging"
                    AlternatingRowStyle-CssClass="row01">
                    <Columns>
                        <asp:BoundField DataField="AwardedDate" HeaderText="Award Date" SortExpression="AwardedDate"
                            DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="RecipientTokenNumber" HeaderText="Recipient Token No" SortExpression="RecipientTokenNumber" />
                        <asp:BoundField DataField="RecipientName" HeaderText="Recipient Name" SortExpression="RecipientName" />
                        <asp:BoundField DataField="GiverTokenNumber" HeaderText="Giver Token No" SortExpression="GiverTokenNumber" />
                        <asp:BoundField DataField="GiverName" HeaderText="Giver Name" SortExpression="GiverName" />
                        <asp:BoundField DataField="AwardType" HeaderText="AwardType" SortExpression="AwardType" />
                        <asp:BoundField DataField="ReasonForNomination" HeaderText="Reason For Nomination"
                            SortExpression="ReasonForNomination" />
                        <asp:BoundField DataField="AwardAmount" HeaderText="Award Amount" SortExpression="AwardAmount" />
                        <asp:TemplateField>
                                <ItemTemplate>
                                <%--<%# Eval("EmailedDate") == null && ((Convert.ToBoolean(Eval("Giver_IsAdmin")) == true) || (Convert.ToBoolean(Eval("Is_Auto")) == false) ) ? true : false%>--%>
                                    <asp:LinkButton ID="btnRevert" Text = "Revert" Visible="true"
                                    runat="server" OnClick="btnRevert_Click" OnClientClick="return confirm('Do you really want to revert this award?')" />
                                </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                        BorderStyle="Solid" Font-Italic="true" />
                    <EmptyDataTemplate>
                        <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                    </EmptyDataTemplate>
                    <PagerStyle CssClass="dataPager" />
                    <SelectedRowStyle CssClass="selectedoddTd" />
                </asp:GridView>
            </div>
        </div>


</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="AwardsByMe.aspx.cs" Inherits="MM_Rewards.Web_Pages.AwardsByMe" EnableEventValidation="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
    <link href="../../includes/css/tabsStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("[id$=dateInput_text]").attr('readonly', 'readonly');
            //$("[id$=dtpEnddate_dateInput_text]").attr('readonly', 'readonly');
        });
    </script>

    <script type="text/javascript">
        function btnGet_OnClientClick() {
            var dtpStartdate = $find('<%#dtpStartdate.ClientID %>');
            var dtpEnddate = $find('<%#dtpEnddate.ClientID %>');

            var err = '';
            err += Check_StartEndDate(dtpStartdate.get_selectedDate() == null ? '' : dtpStartdate.get_selectedDate(), dtpEnddate.get_selectedDate() == null ? '' : dtpEnddate.get_selectedDate(), "Start", "End");

            if (err != "") {
                alert(err);
                return false;
            }
            return true;
        }

        function Check_StartEndDate(SDate, EDate, strStart, strEnd) {
            var endDate = new Date(EDate);
            var startDate = new Date(SDate);

            var err = '';

            err += chkDate_Blank(SDate, strStart + " Date");
            err += chkDate_Blank(EDate, strEnd + " Date");

            if (SDate != '' && EDate != '' && startDate > endDate) {
                err += "Please ensure that the " + strEnd + " Date is greater than or equal to the " + strStart + " Date.";
            }

            return err;
        }

        function chkDate_Blank(strValue, strSuffix) {
            if (strValue == '') {
                return 'Please Select ' + strSuffix + ' \n';
            }
            else {
                return '';
            }
        }
    </script>
    <style>
        .RadPicker, .RadPicker td {
            width: 100%!important;
        }

        .RadPicker {
            padding-right: 10px;
        }

        .rnr-input.rnr-date span:after, .rnr-input.rnr-date span:before {
            display: none;
        }

        html body .RadInput_Default .riTextBox, html body .RadInput_Read_Default {
            border-color: #d0d0d0;
            border: 0!important;
            height: 100%!important;
            padding: 10px!important;
            background: transparent!important;
            font-family: Calibri !important;
            font-size: 16px !important;
        }

        .rnr-table-head span {
            font-size: 20px;
            font-weight: 600;
            margin: 10px 0 20px;
        }

        .rnr-table-head a span {
            color: #30CE8D;
            font-size: 12px;
            margin: 0px 0 0px;
        }
        .rnr-tabs {
            margin-top: -60px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upAwards">
        <ContentTemplate>
            <section class="main-rnr">
                <div class="rnr-inner-top">
                    <div class="rnr-breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href='/Web_Pages/HomePage.aspx'>HOME</a></li>
                            <li><a href='/Web_Pages/Rewards/RecipientAward.aspx'>Awards</a></li>
                            <li>Awards Given by me</li>
                        </ul>
                        <ul class="budget-widget">
                            <li class="total-buget">
                                <div class="budget-txt">
                                    <span class="big-b">Total Budget</span>
                                    <span class="small-b">For the Year <b>
                                        <asp:Label ID="lblYear" runat="server"></asp:Label></b> </span>

                                </div>
                                <h4 class="budget-value">
                                    <asp:Label ID="lblBudget" runat="server"></asp:Label>

                                </h4>
                            </li>
                            <li class="budget-used">
                                <div class="budget-txt">
                                    <span class="big-b">Budget</span>
                                    <span class="small-b">Used</span>
                                </div>
                                <h4 class="budget-value">
                                    <asp:Label ID="lblUsedBudget" runat="server"></asp:Label>

                                </h4>
                            </li>
                            <li class="budget-available">
                               <%-- <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172"
                                    style="fill: #000000;">
                                    <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                        <path d="M0,172v-172h172v172z" fill="none"></path>
                                        <g fill="#ffffff">
                                            <path d="M86,0c-47.50716,0 -86,38.49284 -86,86c0,47.50716 38.49284,86 86,86c47.50716,0 86,-38.49284 86,-86c0,-47.50716 -38.49284,-86 -86,-86zM86,14.33333c39.58464,0 71.66667,32.08203 71.66667,71.66667c0,39.58464 -32.08203,71.66667 -71.66667,71.66667c-39.58463,0 -71.66667,-32.08203 -71.66667,-71.66667c0,-39.58463 32.08203,-71.66667 71.66667,-71.66667zM86,41.65625c-1.31576,0 -2.40755,-0.02799 -3.58333,0.22396c-1.17578,0.25195 -2.26758,0.89583 -3.13542,1.56771c-0.86784,0.67188 -1.51172,1.5957 -2.01562,2.6875c-0.5039,1.0918 -0.67187,2.37956 -0.67187,4.03125c0,1.6237 0.16797,2.91146 0.67188,4.03125c0.50391,1.11979 1.14778,2.01563 2.01563,2.6875c0.86784,0.67188 1.95964,1.06381 3.13542,1.34375c1.17578,0.27995 2.26758,0.44792 3.58333,0.44792c1.28776,0 2.65951,-0.16797 3.80729,-0.44792c1.14778,-0.27994 2.04362,-0.67187 2.91146,-1.34375c0.86784,-0.67187 1.51172,-1.56771 2.01563,-2.6875c0.50391,-1.0918 0.89583,-2.40755 0.89583,-4.03125c0,-1.65169 -0.39192,-2.93945 -0.89583,-4.03125c-0.5039,-1.0918 -1.14778,-2.01562 -2.01562,-2.6875c-0.86784,-0.67187 -1.76367,-1.31576 -2.91146,-1.56771c-1.14778,-0.25195 -2.51953,-0.22396 -3.80729,-0.22396zM77.26563,65.61979v64.27604h17.46875v-64.27604z"></path>
                                        </g>
                                    </g>
                                </svg>--%>
                                <div class="budget-txt">
                                    <span class="big-b">Budget</span>
                                    <span class="small-b">Available</span>

                                </div>
                                <h4 class="budget-value">
                                    <asp:Label ID="lblAvailableBudget" runat="server"></asp:Label>
                                </h4>

                            </li>
                        </ul>

                        <div class="budget-mobile">
                            <ul class="budget-widget">
                                <li class="budget-available">
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172"
                                        style="fill: #000000;">
                                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                            <path d="M0,172v-172h172v172z" fill="none"></path>
                                            <g fill="#ffffff">
                                                <path d="M86,0c-47.50716,0 -86,38.49284 -86,86c0,47.50716 38.49284,86 86,86c47.50716,0 86,-38.49284 86,-86c0,-47.50716 -38.49284,-86 -86,-86zM86,14.33333c39.58464,0 71.66667,32.08203 71.66667,71.66667c0,39.58464 -32.08203,71.66667 -71.66667,71.66667c-39.58463,0 -71.66667,-32.08203 -71.66667,-71.66667c0,-39.58463 32.08203,-71.66667 71.66667,-71.66667zM86,41.65625c-1.31576,0 -2.40755,-0.02799 -3.58333,0.22396c-1.17578,0.25195 -2.26758,0.89583 -3.13542,1.56771c-0.86784,0.67188 -1.51172,1.5957 -2.01562,2.6875c-0.5039,1.0918 -0.67187,2.37956 -0.67187,4.03125c0,1.6237 0.16797,2.91146 0.67188,4.03125c0.50391,1.11979 1.14778,2.01563 2.01563,2.6875c0.86784,0.67188 1.95964,1.06381 3.13542,1.34375c1.17578,0.27995 2.26758,0.44792 3.58333,0.44792c1.28776,0 2.65951,-0.16797 3.80729,-0.44792c1.14778,-0.27994 2.04362,-0.67187 2.91146,-1.34375c0.86784,-0.67187 1.51172,-1.56771 2.01563,-2.6875c0.50391,-1.0918 0.89583,-2.40755 0.89583,-4.03125c0,-1.65169 -0.39192,-2.93945 -0.89583,-4.03125c-0.5039,-1.0918 -1.14778,-2.01562 -2.01562,-2.6875c-0.86784,-0.67187 -1.76367,-1.31576 -2.91146,-1.56771c-1.14778,-0.25195 -2.51953,-0.22396 -3.80729,-0.22396zM77.26563,65.61979v64.27604h17.46875v-64.27604z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    <div class="budget-txt">
                                        <span class="big-b">Budget</span>
                                        <span class="small-b">Available</span>
                                    </div>
                                    <h4 class="budget-value">
                                        <asp:Label ID="lblAvailableBudgetMobile" runat="server"></asp:Label>
                                    </h4>
                                </li>

                                <li class="budget-used">
                                    <div class="budget-txt">
                                        <span class="big-b">Budget</span>
                                        <span class="small-b">Used</span>
                                    </div>
                                    <h4 class="budget-value">
                                        <asp:Label ID="lblUsedBudgetMobile" runat="server"></asp:Label>
                                    </h4>
                                </li>
                                <li class="total-buget">
                                    <div class="budget-txt">
                                        <span class="big-b">Total Budget</span>
                                        <span class="small-b">For the Year <b>
                                            <asp:Label ID="lblYearMobile" runat="server"></asp:Label></b> </span>
                                    </div>
                                    <h4 class="budget-value">
                                        <asp:Label ID="lblBudgetMobile" runat="server"></asp:Label>
                                    </h4>
                                </li>

                            </ul>

                        </div>


                    </div>
                </div>

                <div class="rnr-tabs">
                    <h3 class="rnr-h3-top">Awards Given by Me</h3>
                    <div class="tab-list">

                        <ul class="nav nav-tabs">
                            <li id="section1" class="nav-item active" runat="server">

                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 46.094 46.094">
                                    <g id="Group_3797" data-name="Group 3797" transform="translate(-7 -7)">
                                        <g id="Group_3797-2" data-name="Group 3797" transform="translate(8 8)">
                                            <path class="cp-stroke" id="Path_7571" data-name="Path 7571" d="M39.024,30.047h0A11.056,11.056,0,0,0,50.047,19.024h0A11.056,11.056,0,0,0,39.024,8h0A11.056,11.056,0,0,0,28,19.024h0A11.056,11.056,0,0,0,39.024,30.047Z" transform="translate(-16.976 -8)" fill="none" stroke-width="2" />
                                            <path class="cp-stroke" id="Path_7572" data-name="Path 7572" d="M34.842,54.06a15.285,15.285,0,0,1-9.59,0A12.326,12.326,0,0,0,11.8,57.7,16.981,16.981,0,0,0,8,68.391a4.07,4.07,0,0,0,4.079,4.079H48.016a4.07,4.07,0,0,0,4.079-4.079,16.981,16.981,0,0,0-3.8-10.693A12.352,12.352,0,0,0,34.842,54.06Z" transform="translate(-8 -28.375)" fill="none" stroke-width="2" />
                                        </g>
                                    </g>
                                </svg>
                                <asp:Button ID="btnTabDirectReportis" OnClick="btnTabDirectReportis_Click" runat="server" Text="Direct Reportees" />
                            </li>
                            <li id="section2" class="nav-item" runat="server">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.647 35.643">
                                    <path class="custom-path" id="Path_7573" data-name="Path 7573" d="M39.287,5.077,5.588,18.953a.959.959,0,0,0-.595.991.93.93,0,0,0,.753.872L21.01,24.622l3.806,15.264a1.065,1.065,0,0,0,.872.753h.079a1,1,0,0,0,.912-.595l13.876-33.7a.974.974,0,0,0-.2-1.07A.947.947,0,0,0,39.287,5.077ZM26.045,36.516,22.675,23,37.86,7.813Z" transform="translate(-4.989 -4.996)" />
                                </svg>
                                <asp:Button ID="btnTabOther" runat="server" OnClick="btnTabOther_Click" Text="Other Recipients" />
                            </li>
                            <li id="section3" class="nav-item" runat="server">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.28 41.038">
                                    <path class="custom-path" id="Path3" data-name="Path 7574" d="M44.481,3.14a1.415,1.415,0,0,0-1.493.156L26.173,16.376V4.413a1.415,1.415,0,0,0-2.313-1.1L.515,22.417a1.415,1.415,0,0,0,0,2.193l23.345,19.1a1.415,1.415,0,0,0,2.313-1.1V30.652l16.815,13.08a1.415,1.415,0,0,0,2.285-1.118V4.413a1.415,1.415,0,0,0-.792-1.273ZM42.444,39.721,25.628,26.641a1.415,1.415,0,0,0-2.285,1.118V39.629L3.649,23.514,23.343,7.4V19.269a1.415,1.415,0,0,0,2.285,1.118L42.444,7.307Z" transform="translate(0.006 -2.995)" />
                                </svg>
                                <asp:Button ID="btnTabRevert" runat="server" OnClick="btnTabRevert_Click" Text="Awards Reverted" />
                            </li>
                            <li id="section4" class="nav-item" runat="server">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.28 41.038">
                                    <path class="custom-path" id="Path1" data-name="Path 7574" d="M44.481,3.14a1.415,1.415,0,0,0-1.493.156L26.173,16.376V4.413a1.415,1.415,0,0,0-2.313-1.1L.515,22.417a1.415,1.415,0,0,0,0,2.193l23.345,19.1a1.415,1.415,0,0,0,2.313-1.1V30.652l16.815,13.08a1.415,1.415,0,0,0,2.285-1.118V4.413a1.415,1.415,0,0,0-.792-1.273ZM42.444,39.721,25.628,26.641a1.415,1.415,0,0,0-2.285,1.118V39.629L3.649,23.514,23.343,7.4V19.269a1.415,1.415,0,0,0,2.285,1.118L42.444,7.307Z" transform="translate(0.006 -2.995)" />
                                </svg>
                                <asp:Button ID="btnTabEcards" runat="server" OnClick="btnTabEcards_Click" Text="Ecards" />
                            </li>
                        </ul>
                    </div>
                    <asp:HiddenField ID="hdnActiveAwardType" runat="server" />
                    <div class="rnr-tab-pane tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                      <label for="">Select Amount</label>
                       <span><small>Select Amount</small>
                       </span>
                       <ul class="mahindra_drop_scroll">
                          <li>1</li>
                          <li>2</li>
                          <li>3</li>
                       </ul>
                    </div> -->
                                        <div class="date-search-panel">
                                            <div class="rnr-input rnr-date">
                                                <label for="">Start Date</label>
                                                <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                                                    TabIndex="1">
                                                </telerik:RadDatePicker>

                                            </div>
                                            <div class="rnr-input rnr-date">
                                                <label for="">End Date</label>
                                                <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                                                    TabIndex="2">
                                                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="2">
                                                    </DateInput>
                                                    <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="2"></DatePopupButton>
                                                </telerik:RadDatePicker>

                                            </div>
                                            <asp:Button ID="btnGet" runat="server" Text="Search" ToolTip="Search for Direct Reportees" CssClass="rnr-btn" OnClick="btnGet_Click" OnClientClick="return btnGet_OnClientClick()" TabIndex="3" />
                                            <asp:Button ID="btnGetRecipientDetail" runat="server" ToolTip="Search for Other Recipients" Text="Search" Visible="false" CssClass="rnr-btn" OnClick="btnGetRecipientDetail_Click" OnClientClick="return btnGet_OnClientClick()" />
                                            <asp:Button ID="btnGetRevertDetail" runat="server" Text="Search" ToolTip="Search for Reverted Awards" Visible="false" CssClass="rnr-btn" OnClick="btnGetRevertDetail_Click" OnClientClick="return btnGet_OnClientClick()" />
                                            <asp:Button ID="btnEcardsDetail" runat="server" Text="Search" ToolTip="Search for E-Cards" Visible="false" CssClass="rnr-btn" OnClick="btnEcardsDetail_Click" OnClientClick="return btnGet_OnClientClick()" />
                                            <%--<button class="rnr-btn" type="button" name="button">Search</button>--%>
                                        </div>

                                        <div class="rnr-table">
                                            <div class="rnr-table-head">
                                                <%--<h3>Direct Reportees Recipient Details</h3>--%>
                                                <asp:Label ID="gridtitle" runat="server" Text="Direct Reportees Recipient Details"></asp:Label>
                                                <a href="#" class="down-excel" runat="server" onserverclick="ExportExcel">
                                                    <span>Download </span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17.211" height="16.497" viewBox="0 0 17.211 16.497">
                                                        <defs>
                                                            <style>
                                                                .a {
                                                                    fill: #30ce8d;
                                                                }

                                                                .b {
                                                                    fill: #fafafa;
                                                                }
                                                            </style>
                                                        </defs><g transform="translate(0 -10.626)"><g transform="translate(0 10.626)"><path class="a" d="M9.9,10.706a.359.359,0,0,0-.3-.072L.278,12.786A.359.359,0,0,0,0,13.139V25.33a.359.359,0,0,0,.3.354l9.323,1.434a.359.359,0,0,0,.413-.354V10.987A.358.358,0,0,0,9.9,10.706Z" transform="translate(0 -10.626)" /><path class="a" d="M284.863,67h-7.171a.359.359,0,1,1,0-.717H284.5V54.094h-6.813a.359.359,0,1,1,0-.717h7.171a.359.359,0,0,1,.359.359V66.644A.359.359,0,0,1,284.863,67Z" transform="translate(-268.01 -51.94)" /></g>
                                                            <g transform="translate(2.862 15.638)">
                                                                <path class="b" d="M89.1,166.223a.359.359,0,0,1-.3-.168l-3.586-5.737a.359.359,0,0,1,.6-.4l.012.02,3.586,5.737a.359.359,0,0,1-.3.548Z" transform="translate(-85.149 -159.758)" />
                                                                <path class="b" d="M85.713,166.2a.359.359,0,0,1-.3-.549L89,159.916a.359.359,0,1,1,.62.36l-.012.02-3.586,5.737A.359.359,0,0,1,85.713,166.2Z" transform="translate(-85.348 -159.737)" />
                                                            </g>
                                                            <g transform="translate(9.322 12.063)">
                                                                <path class="a" d="M363.026,67a.359.359,0,0,1-.359-.359V53.735a.359.359,0,0,1,.717,0V66.643A.359.359,0,0,1,363.026,67Z" transform="translate(-359.798 -53.376)" />
                                                                <path class="a" d="M284.863,374.093h-7.171a.359.359,0,1,1,0-.717h7.171a.359.359,0,1,1,0,.717Z" transform="translate(-277.333 -362.619)" />
                                                                <path class="a" d="M284.863,310.093h-7.171a.359.359,0,0,1,0-.717h7.171a.359.359,0,1,1,0,.717Z" transform="translate(-277.333 -300.77)" />
                                                                <path class="a" d="M284.863,246.093h-7.171a.359.359,0,1,1,0-.717h7.171a.359.359,0,1,1,0,.717Z" transform="translate(-277.333 -238.922)" />
                                                                <path class="a" d="M284.863,182.093h-7.171a.359.359,0,1,1,0-.717h7.171a.359.359,0,1,1,0,.717Z" transform="translate(-277.333 -177.073)" />
                                                                <path class="a" d="M284.863,118.093h-7.171a.359.359,0,0,1,0-.717h7.171a.359.359,0,1,1,0,.717Z" transform="translate(-277.333 -115.225)" />
                                                            </g>
                                                        </g></svg>
                                                </a>
                                            </div>
                                            <div id="table-scroll" class="table-scroll">
                                                <div class="grid01" style="background-color: transparent; width: 100%; overflow-x: auto;" id="Detail1" runat="server">

                                                    <asp:GridView ID="grdAwards" runat="server" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                                        AutoGenerateColumns="false" DataKeyNames="ID" OnPageIndexChanging="grdAwards_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="row01" Style="width: 100%; margin: 0 auto;" CssClass="grdAwards_new">
                                                        <Columns>
                                                            <asp:BoundField DataField="AwardedDate" HeaderText="Award Date" SortExpression="AwardedDate"
                                                                DataFormatString="{0:dd/MM/yyyy}" />
                                                            <asp:BoundField DataField="RecipientName" HeaderText="Recipient Name" SortExpression="RecipientName" />
                                                            <asp:BoundField DataField="AwardType" HeaderText="Award Type" SortExpression="AwardType" />
                                                            <asp:BoundField DataField="ReasonForNomination" HeaderText="Reason For Nomination"
                                                                SortExpression="ReasonForNomination" />
                                                            <asp:BoundField DataField="AwardAmount" HeaderText="Award Amount" SortExpression="AwardAmount" />
                                                            <asp:BoundField DataField="RedeemDate" HeaderText="Redeem Date" SortExpression="RedeemDate"
                                                                DataFormatString="{0:dd/MM/yyyy}" />

                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>Revert</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%-- <%# Eval("EmailedDate") == null && ((Convert.ToBoolean(Eval("Giver_IsAdmin")) == true) || (Convert.ToBoolean(Eval("Is_Auto")) == false) ) ? true : false%>--%>
                                                                    <asp:LinkButton ID="btnRevert" Visible='<%# Eval("EmailedDate") == null && ((Convert.ToBoolean(Eval("Giver_IsAdmin")) == true) || (Convert.ToBoolean(Eval("IsAuto")) == false) ) ? true : false%>'
                                                                        runat="server" OnClick="btnRevert_Click" OnClientClick="return confirm('Do you really want to revert this award?')">
                                            <img src="../../Images/revert.png" title="Revert Award" />
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                            BorderStyle="Solid" Font-Italic="true" />
                                                        <EmptyDataTemplate>
                                                            <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                                                        </EmptyDataTemplate>
                                                        <PagerStyle CssClass="dataPager" />
                                                        <SelectedRowStyle CssClass="selectedoddTd" />
                                                    </asp:GridView>

                                                    <asp:GridView ID="grdAwardsOtherReciptent" runat="server" Visible="false" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                                        AutoGenerateColumns="false" DataKeyNames="ID" OnPageIndexChanging="grdAwardsOtherReciptent_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="row01" Style="width: 100%; margin: 0 auto;" CssClass="grdAwards_new">
                                                        <Columns>
                                                            <asp:BoundField DataField="AwardedDate" HeaderText="Award Date" SortExpression="AwardedDate"
                                                                DataFormatString="{0:dd/MM/yyyy}" />
                                                            <asp:BoundField DataField="RecipientName" HeaderText="Recipient Name" SortExpression="RecipientName" />
                                                            <asp:BoundField DataField="AwardType" HeaderText="Award Type" SortExpression="AwardType" />
                                                            <asp:BoundField DataField="ReasonForNomination" HeaderText="Reason For Nomination"
                                                                SortExpression="ReasonForNomination" />

                                                            <asp:BoundField DataField="AwardAmount" HeaderText="Award Amount" SortExpression="AwardAmount" />
                                                            <asp:BoundField DataField="RedeemDate" HeaderText="Redeem Date" SortExpression="RedeemDate"
                                                                DataFormatString="{0:dd/MM/yyyy}" />

                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>Revert</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnRevert" Visible='<%# Eval("EmailedDate") == null && ((Convert.ToBoolean(Eval("Giver_IsAdmin")) == true) || (Convert.ToBoolean(Eval("IsAuto")) == false) ) ? true : false%>'
                                                                        runat="server" OnClick="btnRevert_Click1" OnClientClick="return confirm('Do you really want to revert this award?')">
                                                <img src="../../Images/revert.png" title="Revert Award" />
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                            BorderStyle="Solid" Font-Italic="true" />
                                                        <EmptyDataTemplate>
                                                            <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                                                        </EmptyDataTemplate>
                                                        <PagerStyle CssClass="dataPager" />
                                                        <SelectedRowStyle CssClass="selectedoddTd" />
                                                    </asp:GridView>



                                                    <asp:GridView ID="gvRevertAwardDetail" runat="server" Visible="false" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                                        AutoGenerateColumns="false" DataKeyNames="ID" OnPageIndexChanging="gvRevertAwardDetail_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="row01" Style="width: 100%; margin: 0 auto;" CssClass="grdAwards_new">
                                                        <Columns>
                                                            <asp:BoundField DataField="AwardedDate" HeaderText="Award Date" SortExpression="AwardedDate"
                                                                DataFormatString="{0:dd/MM/yyyy}" />
                                                            <asp:BoundField DataField="RecipientName" HeaderText="Recipient Name" SortExpression="RecipientName" />
                                                            <asp:BoundField DataField="AwardType" HeaderText="Award Type" SortExpression="AwardType" />
                                                            <asp:BoundField DataField="ReasonForNomination" HeaderText="Reason For Nomination"
                                                                SortExpression="ReasonForNomination" />

                                                            <asp:BoundField DataField="AwardAmount" HeaderText="Award Amount" SortExpression="AwardAmount" />

                                                            <asp:BoundField DataField="ClosureDate" HeaderText="Revert Date" SortExpression="RevertDate"
                                                                DataFormatString="{0:dd/MM/yyyy}" />

                                                            <%--  <asp:TemplateField>
                                    <HeaderTemplate>Revert</HeaderTemplate>
                                    <ItemTemplate>
                                           
                                        <asp:LinkButton ID="btnRevert" Visible='<%# Eval("EmailedDate") == null && ((Convert.ToBoolean(Eval("Giver_IsAdmin")) == true) || (Convert.ToBoolean(Eval("IsAuto")) == false) ) ? true : false%>'
                                            runat="server" OnClick="btnRevert_Click1" OnClientClick="return confirm('Do you really want to revert this award?')">
                                                <img src="../../Images/revert.png" title="Revert Award" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                            BorderStyle="Solid" Font-Italic="true" />
                                                        <EmptyDataTemplate>
                                                            <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                                                        </EmptyDataTemplate>
                                                        <PagerStyle CssClass="dataPager" />
                                                        <SelectedRowStyle CssClass="selectedoddTd" />
                                                    </asp:GridView>

                                                    <asp:GridView ID="grdEcardsDetails" runat="server" Visible="false" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                                        AutoGenerateColumns="false" DataKeyNames="ID" OnPageIndexChanging="grdEcardsDetails_PageIndexChanging"
                                                        AlternatingRowStyle-CssClass="row01" Style="width: 100%; margin: 0 auto;" CssClass="grdAwards_new">
                                                        <Columns>
                                                            <asp:BoundField DataField="AwardedDate" HeaderText="Award Date" SortExpression="AwardedDate"
                                                                DataFormatString="{0:dd/MM/yyyy}" />
                                                            <asp:BoundField DataField="RecipientName" HeaderText="Recipient Name" SortExpression="RecipientName" />

                                                            <asp:BoundField DataField="ReasonForNomination" HeaderText="Reason For Nomination" HeaderStyle-HorizontalAlign="Left"
                                                                SortExpression="ReasonForNomination" ItemStyle-Width="300px" />


                                                        </Columns>
                                                        <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                            BorderStyle="Solid" Font-Italic="true" />
                                                        <EmptyDataTemplate>
                                                            <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                                                        </EmptyDataTemplate>
                                                        <PagerStyle CssClass="dataPager" />
                                                        <SelectedRowStyle CssClass="selectedoddTd" />
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </ContentTemplate>
    </asp:UpdatePanel>


    <script>


        $(document).ready(function () {
            $('#ContentPlaceHolder1_section1').click(function () {
                $('#ContentPlaceHolder1_section1').addClass('active');
                $('#ContentPlaceHolder1_section3').removeClass('active');
                $('#ContentPlaceHolder1_section2').removeClass('active');
                $('#ContentPlaceHolder1_section4').removeClass('active');
            });

            $('#ContentPlaceHolder1_section2').click(function () {
                $('#ContentPlaceHolder1_section2').addClass('active');
                $('#ContentPlaceHolder1_section1').removeClass('active');
                $('#ContentPlaceHolder1_section3').removeClass('active');
                $('#ContentPlaceHolder1_section4').removeClass('active');
            });

            $('#ContentPlaceHolder1_section3').click(function () {
                $('#ContentPlaceHolder1_section3').addClass('active');
                $('#ContentPlaceHolder1_section1').removeClass('active');
                $('#ContentPlaceHolder1_section2').removeClass('active');
                $('#ContentPlaceHolder1_section4').removeClass('active');
            });
            $('#ContentPlaceHolder1_section4').click(function () {
                $('#ContentPlaceHolder1_section4').addClass('active');
                $('#ContentPlaceHolder1_section1').removeClass('active');
                $('#ContentPlaceHolder1_section2').removeClass('active');
                $('#ContentPlaceHolder1_section3').removeClass('active');
            });
        });
    </script>
</asp:Content>

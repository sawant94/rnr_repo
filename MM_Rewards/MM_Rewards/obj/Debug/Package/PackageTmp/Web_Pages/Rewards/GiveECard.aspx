﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GiveECard.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.GiveECard"
    MasterPageFile="~/Web_Pages/MM_Rewards.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 0;
        }

        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            padding-top: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.8); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            /*background-color: #fefefe;
            margin: auto;
            width: 45%;
            position: relative;
            overflow: hidden;*/
        }

        .modal_inner {
            height: 480px;
            overflow-y: auto;
            padding: 10px 20px;
        }

        /* The Close Button */
        .closemodal {
            color: #fff;
            float: right;
            font-size: 24px;
            font-weight: bold;
            position: absolute;
            right: 15px;
            top: 0;
        }

            .closemodal:hover,
            .closemodal:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        div.form01 table.grid_new td {
            padding: 3px 10px;
        }

        #ContentPlaceHolder1_tblRecipientList {
            width: 100%;
        }

            #ContentPlaceHolder1_tblRecipientList td input {
                border-bottom: 1px solid #272727;
                font-family: 'Conv_EUROSTILELTSTD' !important;
                width: 100%;
            }

        .chkBox input[type=radio] {
            float: none;
        }

        .chkBox label {
            vertical-align: middle;
        }

        .chkBox input[type=radio] {
            float: none;
            margin-right: 0;
            opacity: 0;
        }


        #ContentPlaceHolder1_radListCardType tr {
            display: inline-block;
            margin-right: 15px;
        }

        .chkBox input[type=radio] + label {
            background-image: url(../../images/new_images/radiornr.png);
            background-repeat: no-repeat;
            background-size: 17px;
            background-position: left 0;
            padding-left: 30px;
            cursor: pointer;
            margin-left: -22px;
        }

        .chkBox input[type=radio]:checked + label {
            background-position: left -28px;
        }

        @media screen and (max-width: 767px) {
            div.form01 table td {
                padding: 5px 0;
                font-size: 14px;
            }

            .chkBox input[type=radio] + label {
                background-size: 19px;
                margin-left: -19px;
            }

            .chkBox input[type=radio]:checked + label {
                background-position: left -24px;
            }

            .modal_inner {
                padding: 10px 0;
                height: 460px !important;
            }

            .modal {
                padding-top: 10px;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            div.navHold {
                z-index: 1;
            }
        }

        @media screen and (min-width: 1920px) {
            div.form01 table td {
                padding: 10px 10px;
                font-size: 16px;
            }
        }

        .modal-backdrop.show {
            z-index: 9;
        }

        .search-emp-modal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        .search-emp-modal .rnr-search-bar div.search-form label .SearchButtons {
            margin: 0;
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
            float: none;
        }

            .search-emp-modal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
                display: inline-block;
            }

        .rnr-tabs li.nav-item {
            margin-bottom: 15px;
        }

        #ContentPlaceHolder1_drpdown {
            border: 0;
            width: 100%;
        }

        @media only screen and (max-width: 767px) {
            .rnr-tabs li.nav-item:last-child {
                width: 265px;
            }

            .pgData {
                position: relative;
                top: -1px;
            }
        }
    </style>



    <script src="../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script>
        function ClosePopup() {
            $('.modal').css('display', 'none');
        }
    </script>

    <style>
        .loader {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 1;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        /* Add animation to "page content" */
        .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
        }

        @-webkit-keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0;
            }

            to {
                bottom: 0px;
                opacity: 1;
            }
        }

        @keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0;
            }

            to {
                bottom: 0;
                opacity: 1;
            }
        }

        .send-ecard .nom-box-top h5 input {
            font-weight: 600;
            font-size: 20px;
            margin-bottom: 0;
            padding: 0;
        }

        .send-ecard .search-emp-m input {
            /*padding: 0;*/
            min-height: auto;
        }

        .send-ecard .search-emp-m ul input {
            padding: 0;
            font-size: 16px;
            width: 100%;
            color: #203442;
            font-weight: 500;
        }

        .send-ecard label.nom-box-lbl input {
            font-size: 11px;
            color: #fff!important;
            padding: 0;
        }

        .send-ecard .search-emp-m .send-ecard-message textarea {
            font-size: 18px;
            padding: 0 20px;
            min-height: 40px;
            border: 0;
            font-family: Calibri!important;
        }

        .modal-footer input.btnReset {
            color: #000;
            font-weight: 600;
            text-transform: uppercase;
            font-size: 16px;
            margin-right: 40px;
            background: transparent!important;
            opacity: 0.5;
        }

        .rnr-tabs .tab-list {
            flex-direction: column;
        }

        .rnr-tabs .nav-tabs {
            border: 0;
            display: flex;
            flex-direction: row;
        }

        #ContentPlaceHolder1_btnPreviewECard {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
        }

        #ContentPlaceHolder1_txtEmpReason {
            background: #eee;
            margin: 0 20px;
            width: calc(100% - 40px);
            display: block;
        }

        .img-responsive {
            margin: 0 10px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function ValidateOnSubmitLocal() {

            var r = confirm("Do you want to submit record ?");
            if (r != true) {
                return false;
            }

            // Display your loading image (centered on your screen)

            var errMsg = '';
            var f = 0;

            var name = document.getElementById('<%=txtEmpName.ClientID %>');
            var message = document.getElementById('<%=txtEmpReason.ClientID %>');
            if (message.value == "") {
                alert("Please Enter The Message");
                return false;
            }
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                ClosePopup();
                $("#loderid").show();
                return true;
            }

        }


    </script>
    <asp:HiddenField ID="hndCardType" runat="server" />
    <div class="loader" runat="server"  id="loderid" style="display:none">
        <div class="gif_loader" id="gifloader" runat="server" >
            <img src="/MailDetails/loader.gif" class="img-responsive loader_img"  />
        </div>
    </div>
    <section class="main-rnr">
        <div class="rnr-inner-top">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href="/Web_Pages/HomePage.aspx">HOME</a></li>
                    <li><a href="#">Awards</a></li>
                    <li>Send E Card</li>
                </ul>
                <ul class="budget-widget hide">
                    <li class="total-buget"></li>
                    <li class="budget-used"></li>
                    <li class="budget-available"></li>
                </ul>
            </div>
        </div>

        <div class="rnr-tabs">
            <h3 class="rnr-h3-top">Select E-card type you want to give</h3>
            <div class="tab-list">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" >
                        <img src="../../Images/agile.png" class="img-responsive" width="45" />
                        <%--<svg xmlns="http://www.w3.org/2000/svg" width="47.274" height="40.404" viewBox="0 0 47.274 40.404">                       
                            <g id="Group_3773" data-name="Group 3773" transform="translate(-4 -1.752)">
                                <path class="cp-stroke" id="Path_7556" data-name="Path 7556" d="M37.575,38.265l1.873-2.57M19.781,40.549l-3.2-3.212m0,0-4.836-4.854L5,43.9H50.274L42.31,31.769,39.448,35.7M16.58,37.337l10.877-19.2m0,0V5.5m0,12.635L39.448,35.7" transform="translate(0 -2.748)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path class="custom-path" id="Path_7557" data-name="Path 7557" d="M123.337,145.988a.728.728,0,1,0,.782,1.228Zm4.176.614.461-.563Zm4.586,1.31-.147-.713Zm2.492-.942a.728.728,0,1,0-.616-1.319Zm-10.472.246a3.355,3.355,0,0,1,1.38-.49,2.021,2.021,0,0,1,1.553.439l.922-1.127a3.475,3.475,0,0,0-2.63-.76,4.805,4.805,0,0,0-2.008.709Zm2.934-.051a7.311,7.311,0,0,0,2.341,1.44,5.443,5.443,0,0,0,2.853.02l-.295-1.426a4.031,4.031,0,0,1-2.115.02,5.967,5.967,0,0,1-1.863-1.18Zm5.195,1.46a3.435,3.435,0,0,0,1.933-1.083c.181-.2.326-.406.41-.517a1.377,1.377,0,0,1,.086-.108.36.36,0,0,1-.084.053l-.616-1.319a1.187,1.187,0,0,0-.353.261,2.666,2.666,0,0,0-.2.237c-.117.155-.2.279-.328.417a1.983,1.983,0,0,1-1.147.633Z" transform="translate(-101.187 -122.795)" />
                                <path class="custom-path" id="Path_7558" data-name="Path 7558" d="M177.39,7l-14.244.017v9.456l14.862-.017-4.388-4.564Z" transform="translate(-136.186 -4.926)" />
                            </g>
                        </svg>--%>
                        <asp:Button ID="btnANL" runat="server" Text="Agile" OnClick="btnANL_Click"/>
                    </li>
                    <li class="nav-item">
                        <img src="../../Images/Bold-1.png" class="img-responsive" width="45" />
                      <%-- <svg xmlns="http://www.w3.org/2000/svg" width="47.274" height="40.404" class="alter-man-svg" viewBox="0 0 34.598 42.323">
                     <path class="cp-stroke" id="Path_7483" data-name="Path 7483" d="M6371.342,493.819h8.821l-.519-9.043s4.669,1.26,5.484-.593,0-6.968,0-6.968,4.077-1.26,3.336-2.817-2.742-4-2.965-7.412-1.26-13.49-14.6-13.49-14.915,9.028-14.915,13.49c0,5.123,1.424,8.921,7.206,13.814v13.019h4.521V478.771a14.268,14.268,0,0,1-6.892-12.527c.18-7.022,7.485-8.228,10.081-8.228s8.747,2.351,8.747,8.747-5.351,6.818-8.3,6.523-5.115-2.962-5.115-6.3,2.659-4.522,5.115-4.522,4.374.889,4.374,4.3-3.928,3.558-3.928,3.558" transform="translate(-6354.982 -452.495)" fill="none" stroke-width="2" />
                        </svg>--%>
                        <asp:Button ID="btnAT" runat="server" Text="Bold" OnClick="btnAT_Click" />
                    </li>
                    <li class="nav-item">
                        <img src="../../Images/Collaborate.png" class="img-responsive" width="35" />
                        <%--<svg xmlns="http://www.w3.org/2000/svg" width="33.076" height="42.184" viewBox="0 0 33.076 42.184">
                            <g id="Group_3772" data-name="Group 3772" transform="translate(-691.259 -475.883)">
                                <path class="cp-stroke" id="Path_7481" data-name="Path 7481" d="M6602.618,504.505l-3.984,4.592h-12.359s-3.229.917-3.229-3.229V499.98s-3.119-.068-3.322-1.756,3.322-6.281,3.322-6.281-.3-15.061,13.156-15.061,14.589,12.221,14.589,15.061-2.77,10.806-2.77,10.806v14.318" transform="translate(-5887.457)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Path_7482" data-name="Path 7482" d="M6637.715,619.9v-9.168" transform="translate(-5931.468 -102.836)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-width="2" />
                                <path class="custom-path" id="ic_plus_one_24px" d="M10,8H8v4H4v2H8v4h2V14h4V12H10Z" transform="translate(704.936 478.491)" />
                                <path class="custom-path" id="ic_plus_one_24px-2" data-name="ic_plus_one_24px" d="M8.8,8H7.2v3.2H4v1.6H7.2V16H8.8V12.8H12V11.2H8.8Z" transform="translate(697.526 475.069)" />
                            </g>

                        </svg>--%>
                        <asp:Button ID="btnDPC" runat="server" Text="Collaboration"  OnClick="btnDPC_Click" />
                    </li>
                    <li class="nav-item">
                        <asp:DropDownList ID="drpdown" runat="server" Width="" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="drpdown_SelectedIndexChanged">
                            <asp:ListItem Value="0">Select ThankYou Card</asp:ListItem>
                            <asp:ListItem Value="1">Thank You Card 1</asp:ListItem>
                            <asp:ListItem Value="2">Thank You Card 2</asp:ListItem>
                            <asp:ListItem Value="3">Thank You Card 3</asp:ListItem>
                        </asp:DropDownList>

                    </li>
                </ul>
                <ul class="nav nav-tabs" role="tablist">
                    <%--<li class="nav-item">--%>
                    <%--<a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">--%>
                    <%--<svg xmlns="http://www.w3.org/2000/svg" width="33.076" height="42.184" viewBox="0 0 33.076 42.184">
                            <g id="G5" data-name="Group 3772" transform="translate(-691.259 -475.883)">
                                <path class="cp-stroke" id="Path7" data-name="Path 7481" d="M6602.618,504.505l-3.984,4.592h-12.359s-3.229.917-3.229-3.229V499.98s-3.119-.068-3.322-1.756,3.322-6.281,3.322-6.281-.3-15.061,13.156-15.061,14.589,12.221,14.589,15.061-2.77,10.806-2.77,10.806v14.318" transform="translate(-5887.457)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Path8" data-name="Path 7482" d="M6637.715,619.9v-9.168" transform="translate(-5931.468 -102.836)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-width="2" />
                                <path class="custom-path" id="Path9" d="M10,8H8v4H4v2H8v4h2V14h4V12H10Z" transform="translate(704.936 478.491)" />
                                <path class="custom-path" id="Path10" data-name="ic_plus_one_24px" d="M8.8,8H7.2v3.2H4v1.6H7.2V16H8.8V12.8H12V11.2H8.8Z" transform="translate(697.526 475.069)" />
                            </g>
                        </svg>
                        <asp:Button ID="btnTC1" runat="server" Text="Thank You Card 1" OnClick="btnTC1_Click" />--%>
                    <%--</a>--%>
                    <%-- </li>
                    <li class="nav-item">--%>
                    <%--<a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">--%>
                    <%--<svg xmlns="http://www.w3.org/2000/svg" width="33.076" height="42.184" viewBox="0 0 33.076 42.184">
                            <g id="G6" data-name="Group 3772" transform="translate(-691.259 -475.883)">
                                <path class="cp-stroke" id="Path11" data-name="Path 7481" d="M6602.618,504.505l-3.984,4.592h-12.359s-3.229.917-3.229-3.229V499.98s-3.119-.068-3.322-1.756,3.322-6.281,3.322-6.281-.3-15.061,13.156-15.061,14.589,12.221,14.589,15.061-2.77,10.806-2.77,10.806v14.318" transform="translate(-5887.457)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Path12" data-name="Path 7482" d="M6637.715,619.9v-9.168" transform="translate(-5931.468 -102.836)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-width="2" />
                                <path class="custom-path" id="Path13" d="M10,8H8v4H4v2H8v4h2V14h4V12H10Z" transform="translate(704.936 478.491)" />
                                <path class="custom-path" id="Path14" data-name="ic_plus_one_24px" d="M8.8,8H7.2v3.2H4v1.6H7.2V16H8.8V12.8H12V11.2H8.8Z" transform="translate(697.526 475.069)" />
                            </g>
                        </svg>
                        <asp:Button ID="btnTC2" runat="server" Text="Thank You Card 2" OnClick="btnTC2_Click" />--%>
                    <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                    <%--<a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">--%>
                    <%--<svg xmlns="http://www.w3.org/2000/svg" width="33.076" height="42.184" viewBox="0 0 33.076 42.184">
                            <g id="G7" data-name="Group 3772" transform="translate(-691.259 -475.883)">
                                <path class="cp-stroke" id="Path15" data-name="Path 7481" d="M6602.618,504.505l-3.984,4.592h-12.359s-3.229.917-3.229-3.229V499.98s-3.119-.068-3.322-1.756,3.322-6.281,3.322-6.281-.3-15.061,13.156-15.061,14.589,12.221,14.589,15.061-2.77,10.806-2.77,10.806v14.318" transform="translate(-5887.457)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Path16" data-name="Path 7482" d="M6637.715,619.9v-9.168" transform="translate(-5931.468 -102.836)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-width="2" />
                                <path class="custom-path" id="Path17" d="M10,8H8v4H4v2H8v4h2V14h4V12H10Z" transform="translate(704.936 478.491)" />
                                <path class="custom-path" id="Path18" data-name="ic_plus_one_24px" d="M8.8,8H7.2v3.2H4v1.6H7.2V16H8.8V12.8H12V11.2H8.8Z" transform="translate(697.526 475.069)" />
                            </g>
                        </svg>
                        <asp:Button ID="btnTC3" runat="server" Text="Thank You Card 3" OnClick="btnTC3_Click" />--%>
                    <%--</a>--%>
                    <%--</li>--%>
                </ul>
            </div>

            <div class="rnr-tab-pane tab-content">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <div class="container">
                        <div class="row">

                            <div class="col-md-12" id="Step1" runat="server">
                                <div class="rnr-input search-emp-m step-1">
                                    <div class="ecard-search-head">
                                        <img src="../../Images/agile.png" class="img-responsive" width="26" />
                                        <%--<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 47.274 40.404">
                                            <g id="G1" data-name="Group 3773" transform="translate(-4 -1.752)">
                                                <path stroke="#30ce8d" id="Path1" data-name="Path 7556" d="M37.575,38.265l1.873-2.57M19.781,40.549l-3.2-3.212m0,0-4.836-4.854L5,43.9H50.274L42.31,31.769,39.448,35.7M16.58,37.337l10.877-19.2m0,0V5.5m0,12.635L39.448,35.7" transform="translate(0 -2.748)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                <path id="Path2" fill="#30ce8d" data-name="Path 7557" d="M123.337,145.988a.728.728,0,1,0,.782,1.228Zm4.176.614.461-.563Zm4.586,1.31-.147-.713Zm2.492-.942a.728.728,0,1,0-.616-1.319Zm-10.472.246a3.355,3.355,0,0,1,1.38-.49,2.021,2.021,0,0,1,1.553.439l.922-1.127a3.475,3.475,0,0,0-2.63-.76,4.805,4.805,0,0,0-2.008.709Zm2.934-.051a7.311,7.311,0,0,0,2.341,1.44,5.443,5.443,0,0,0,2.853.02l-.295-1.426a4.031,4.031,0,0,1-2.115.02,5.967,5.967,0,0,1-1.863-1.18Zm5.195,1.46a3.435,3.435,0,0,0,1.933-1.083c.181-.2.326-.406.41-.517a1.377,1.377,0,0,1,.086-.108.36.36,0,0,1-.084.053l-.616-1.319a1.187,1.187,0,0,0-.353.261,2.666,2.666,0,0,0-.2.237c-.117.155-.2.279-.328.417a1.983,1.983,0,0,1-1.147.633Z" transform="translate(-101.187 -122.795)" />
                                                <path fill="#30ce8d" id="Path3" data-name="Path 7558" d="M177.39,7l-14.244.017v9.456l14.862-.017-4.388-4.564Z" transform="translate(-136.186 -4.926)" />
                                            </g>
                                        </svg>--%>
                                        <h4>
                                            <asp:Label ID="lblEcardTypeSetp1" runat="server"></asp:Label>
                                        </h4>
                                    </div>
                                    <div class="search-emp" data-toggle="modal" data-target="#ContentPlaceHolder1_searchEmpDetailsmodal">
                                        <input type="text" name="" value="" placeholder="Enter Name / Token Number" readonly>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.8 363.8">
                                            <title>Search</title>
                                            <g id="Layer_2" data-name="Layer 2">
                                                <g id="Layer_1-2" data-name="Layer 1">
                                                    <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                                </g>
                                            </g>
                                        </svg>

                                    </div>
                                </div>


                            </div>
                            <div class="col-md-12" id="Step2" runat="server">
                                <div class="rnr-input search-emp-m step-2">
                                    <div class="ecard-search-head">
                                        <img src="../../Images/bold-1.png" class="img-responsive" width="26" />
<%--                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 47.274 40.404">
                                            <g id="G2" data-name="Group 3773" transform="translate(-4 -1.752)">
                                                <path stroke="#30ce8d" id="Path4" data-name="Path 7556" d="M37.575,38.265l1.873-2.57M19.781,40.549l-3.2-3.212m0,0-4.836-4.854L5,43.9H50.274L42.31,31.769,39.448,35.7M16.58,37.337l10.877-19.2m0,0V5.5m0,12.635L39.448,35.7" transform="translate(0 -2.748)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                <path id="Path5" fill="#30ce8d" data-name="Path 7557" d="M123.337,145.988a.728.728,0,1,0,.782,1.228Zm4.176.614.461-.563Zm4.586,1.31-.147-.713Zm2.492-.942a.728.728,0,1,0-.616-1.319Zm-10.472.246a3.355,3.355,0,0,1,1.38-.49,2.021,2.021,0,0,1,1.553.439l.922-1.127a3.475,3.475,0,0,0-2.63-.76,4.805,4.805,0,0,0-2.008.709Zm2.934-.051a7.311,7.311,0,0,0,2.341,1.44,5.443,5.443,0,0,0,2.853.02l-.295-1.426a4.031,4.031,0,0,1-2.115.02,5.967,5.967,0,0,1-1.863-1.18Zm5.195,1.46a3.435,3.435,0,0,0,1.933-1.083c.181-.2.326-.406.41-.517a1.377,1.377,0,0,1,.086-.108.36.36,0,0,1-.084.053l-.616-1.319a1.187,1.187,0,0,0-.353.261,2.666,2.666,0,0,0-.2.237c-.117.155-.2.279-.328.417a1.983,1.983,0,0,1-1.147.633Z" transform="translate(-101.187 -122.795)" />
                                                <path fill="#30ce8d" id="Path6" data-name="Path 7558" d="M177.39,7l-14.244.017v9.456l14.862-.017-4.388-4.564Z" transform="translate(-136.186 -4.926)" />
                                            </g>
                                        </svg>--%>
                                        <h4>
                                            <asp:Label ID="lblEcardTypeSetp2" runat="server"></asp:Label></h4>
                                    </div>
                                    <div class="nominee-box">
                                        <div class="nom-box-top" data-toggle="modal" data-target="#ContentPlaceHolder1_searchEmpDetailsmodal">
                                            <h5>
                                                <asp:TextBox ID="txtEmpName" runat="server" ForeColor="#272727" Enabled="False"></asp:TextBox></h5>
                                            <a href="#">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.8 363.8">
                                                    <title>Search</title>
                                                    <g id="G3" data-name="Layer 2">
                                                        <g id="G4" data-name="Layer 1">
                                                            <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>

                                        </div>
                                        <div class="nom-box-main">
                                            <label for="" class="nom-box-lbl">
                                                <asp:TextBox ID="txtEmpDesignation" runat="server" ForeColor="#272727" Enabled="False"></asp:TextBox>
                                            </label>
                                            <ul>
                                                <li>
                                                    <span class="sp1">
                                                        <asp:TextBox ID="txtEmpDivision" runat="server" ForeColor="#272727" Enabled="False"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnLoactionCode" runat="server" />
                                                    </span>
                                                    <label for="" class="nom-detail-lbl">Division</label>
                                                </li>

                                                <li>
                                                    <span class="sp1">
                                                        <asp:TextBox ID="txtEmpDepartmentName" runat="server" ForeColor="#272727" Enabled="False"></asp:TextBox>
                                                    </span>
                                                    <label for="" class="nom-detail-lbl">Department</label>
                                                </li>
                                                <li>
                                                    <span class="sp1">
                                                        <asp:TextBox ID="txtEmpLevel" runat="server" ForeColor="#272727" Enabled="False"></asp:TextBox></span>
                                                    <label for="" class="nom-detail-lbl">Level</label>
                                                </li>
                                                <li>
                                                    <span class="sp1">
                                                        <asp:TextBox ID="txtEmpLocation" runat="server" Enabled="False" ForeColor="Black"></asp:TextBox>
                                                        <asp:HiddenField ID="HdnDivisionCode" runat="server" />
                                                    </span>
                                                    <label for="" class="nom-detail-lbl">Location</label>
                                                </li>
                                            </ul>

                                            <div class="send-ecard-message">
                                                <div class="mess-dflex">
                                                    <p>Enter Message Below</p>
                                                </div>
                                                <asp:TextBox ID="txtEmpReason" runat="server" TextMode="MultiLine" MaxLength="350" TabIndex="5"></asp:TextBox>

                                                <div class="mess-dflex">
                                                    <p>Message (min 30 character and max 150 character)</p>
                                                    <span id="spnTxtCount">(150/150)</span>
                                                </div>

                                            </div>

                                            <div class="rnr-input search-emp-m" style="border: 0; width: 100%;">
                                                <div class="search-emp" data-toggle="modal" data-target="#ContentPlaceHolder1_searchempmodal">

                                                    <div class="mess-dflex">
                                                        <p>Search Employee for CC</p>
                                                    </div>
                                                    <asp:TextBox ID="txtMailID" runat="server" Rows="4" Enabled="false"></asp:TextBox>
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.8 363.8">
                                                        <title>Search</title>
                                                        <g id="G8" data-name="Layer 2">
                                                            <g id="G9" data-name="Layer 1">
                                                                <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="send-awards send-ecard text-center">
                                        <asp:Button ID="btnPreviewECard" runat="server" CssClass="send-ecard-btn rnr-btn" Text="Preview E-Card" OnClientClick="Previewvalidation()" OnClick="ShowPreviewEcard" />
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdnRecipientTokenNumber" runat="server" />
                                <asp:HiddenField ID="HiddenBalance" runat="server" />
                                <asp:HiddenField ID="hdnGiverName" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- search emp modal start-->
    <%--<div style="" class="registerPopup" id="popupContainer" runat="server">--%>
    <%--<div runat="server" id="SuccessModal" class="modal" style="display: none;">--%>

    <div class="modal fade search-emp-modal" id="searchEmpDetailsmodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Search Employee</h5>
                </div>
                <div class="modal-body">
                    <div class="rnr-search-bar">
                        <div role="search" method="get" class="search-form form" action="">
                            <label for="" class="search-top-lbl">Refine Search</label>
                            <label>
                                <asp:TextBox ID="txtTokenNumberTop" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                                <%--<input type="search" class="search-field" placeholder="Enter Name / Token Number" value="" name="s" title="" />--%>

                                <div class="btnRed SearchButtons">
                                    <%--<input type="submit" class="search-submit button"/>--%>
                                    <asp:Button ID="btnGetEmpDetails" CssClass="btnLeft" runat="server" Text=""
                                        OnClick="btnGetEmpDetails_Click" TabIndex="3" />
                                </div>
                            </label>
                        </div>
                    </div>

                    <div class="rnr-table">
                        <div id="table-scroll" class="table-scroll">
                            <div class="grid01" id="divEmpList" runat="server" style="margin-top: 0;">
                                <asp:GridView ID="grdRecipientList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                    OnRowCommand="grdRecipientList_RowCommand" AllowPaging="true"
                                    PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                    AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdRecipientList_PageIndexChanging">
                                    <Columns>
                                        <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                        <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                        <asp:BoundField DataField="DivisionName_PA" HeaderText="Division Name" SortExpression="DivisionName_PA" />
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select"
                                                    runat="server" CssClass="selLink" TabIndex="4">
                                            Select</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="dataPager" />
                                    <%--<SelectedRowStyle CssClass="selectedoddTd" />--%>
                                </asp:GridView>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
<%--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">Cancel</span>
                    </button>--%>
                    <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" CssClass="rnr-btn" />

                    <%--<button type="button" name="button" class="add-emp rnr-btn">Add</button>--%>
                </div>
            </div>
        </div>
    </div>
    <!-- search-emp-modal end -->

    <!--  certificate-modal start-->
    <div class="modal fade search-emp-modal" id="certificatemodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">ECARD PREVIEW</h5>
                </div>
                <div class="modal-body">
                    <div class="congratulation-box">
                        <img id="imgData" runat="server" src="" alt="Image" />
                        <asp:HiddenField ID="hdn_imgDataName" runat="server" />
                        <asp:HiddenField ID="hdn_imgDataUrl" runat="server" />
                    </div>

                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnReset" runat="server" CssClass="btn btnReset" Text="Reset" OnClick="btnReset_Click"
                        TabIndex="7" />
                    <asp:Button ID="btnSubmit" runat="server" CssClass="add-emp rnr-btn" Text="Send E Card"
                        OnClick="btnSubmit_Click" OnClientClick="return ValidateOnSubmitLocal()" TabIndex="6" />
                </div>
            </div>
        </div>
    </div>
    <!-- certificate-modal end -->


    <!-- search emp modal start-->
    <div class="modal fade search-emp-modal" id="searchempmodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Search Employee</h5>
                </div>
                <div class="modal-body">
                    <div class="rnr-search-bar">
                        <div class="search-form form">
                            <label for="" class="search-top-lbl">Refine Search</label>
                            <label>

                                <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                                <div class="btnRed SearchButtons">
                                    <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text=""
                                        OnClick="btnGetDetails_Click" TabIndex="3" OnClientClick="return ValidateEmp()" />
                                </div>
                            </label>

                        </div>
                    </div>

                    <div class="rnr-table">
                        <div id="Div1" class="table-scroll">
                            <div id="divEmailList" runat="server" class="grid01">
                                <asp:GridView ID="grdEmpList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                    OnRowCommand="grdEmpList_RowCommand" Visible="true"
                                    AllowPaging="true" OnPageIndexChanging="grdEmpList_PageIndexChanging" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                                    <Columns>
                                        <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                        <asp:BoundField DataField="EmailID" HeaderText="Email Id" SortExpression="EmailID" />
                                        <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                        <asp:BoundField DataField="DivisionName_PA" HeaderText="Division Name" SortExpression="LocationName_PSA" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select" ToolTip="Select Employee for adding CC"
                                                    runat="server" CssClass="selLink1" TabIndex="4">
                                                Select</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="dataPager" />

                                </asp:GridView>
                            </div>



                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="close" data-dismiss="modal" id="ContentPlaceHolder1_searchempmodal" aria-label="Close" onclick="return closeSearchPopup(this);">
                        <span aria-hidden="true">Cancel</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- search-emp-modal end -->

    <script>
        $(document).ready(function () {
            $("body").addClass("send-ecard Chrome").attr("id", "send-ecard");
            $(".radio.other-recipent input#ContentPlaceHolder1_SelectNomineeName").click(function () {
                $(".hide-box-2").show();
                $(".hide-box-1").hide();
            });

            $(".radio.direct-reportee input#ContentPlaceHolder1_SelectNomineeName_1").click(function () {
                $(".hide-box-1").show();
                $(".hide-box-2").hide();
            });
            $("#ContentPlaceHolder1_btnPreviewECard").click(function () {
                var name = document.getElementById('<%=hndCardType.ClientID %>');

                if (name.value == "ANL") {
                    if (ImgANL.style.display === "none") {
                        ImgANL.style.display = "block";
                    }
                    else { ImgANL.style.display = "none"; }
                }
                else if (name.value == "AT") {
                    if (ImgAT.style.display === "none") {
                        ImgAT.style.display = "block";
                    }
                    else { ImgAT.style.display = "none"; }
                }
                else if (name.value == "DPC") {
                    if (ImgDPC.style.display === "none") {
                        ImgDPC.style.display = "block";
                    }
                    else { ImgDPC.style.display = "none"; }
                }
                else if (name.value == "TC1") {
                    if (ImgTC1.style.display === "none") {
                        ImgTC1.style.display = "block";
                    }
                    else { ImgTC1.style.display = "none"; }
                }
                else if (name.value == "TC2") {
                    if (ImgTC2.style.display === "none") {
                        ImgTC2.style.display = "block";
                    }
                    else { ImgTC2.style.display = "none"; }
                }
                else if (name.value == "TC3") {
                    if (ImgTC3.style.display === "none") {
                        ImgTC3.style.display = "block";
                    }
                    else { ImgTC3.style.display = "none"; }
                }


            })

        });
        $('#ContentPlaceHolder1_txtEmpReason').keyup(function (e) {
            var tval = $('#ContentPlaceHolder1_txtEmpReason').val();
            tlength = tval.length;
            set = 150;
            remain = parseInt(set - tlength);
            var value = 0;

            if (tlength > set) {
                $('#ContentPlaceHolder1_txtEmpReason').val((tval).substring(0, tlength - 1));
                value = '(' + set + '/150)';
                $('#spnTxtCount').text(value);
            }
            else {
                var value = '(' + remain + '/150)';
                $('#spnTxtCount').text(value);
            }


        });
        //$('#ContentPlaceHolder1_txtEmpReason').keypress(function (e) {
        //    var tval = $('#ContentPlaceHolder1_txtEmpReason').val();
        //    tlength = tval.length;
        //    set = 150;
        //    remain = parseInt(set - tlength);
        //    //$('p').text(remain);
        //    if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
        //        $('#ContentPlaceHolder1_txtEmpReason').val((tval).substring(0, tlength - 1));
        //        return false;
        //    }
        //});
        //$('#ContentPlaceHolder1_txtEmpReason').keypress(function (e) {
        //    var tval = $('#ContentPlaceHolder1_txtEmpReason').val(),
        //        tlength = tval.length,
        //        set = 30,
        //        remain = parseInt(set - tlength);
        //    //$('p').text(remain);
        //    if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
        //        $('#ContentPlaceHolder1_txtEmpReason').val((tval).substring(0, tlength - 1));
        //        return false;
        //    }
        //});

        //function Previewvalidation()
        //{
        //    if ($('#ContentPlaceHolder1_btnPreviewECard').val() == "") {
        //        alert("Please select recipient details.")
        //    }
        //}
        function Previewvalidation() {
            var txtLength = $('#ContentPlaceHolder1_txtEmpReason').val().length;
            if (txtLength < 30) {
                alert("Please enter minimum 30 character.");
                $('#ContentPlaceHolder1_certificatemodal').removeClass('show');
                return false;
            }
            if (txtLength > 150) {
                alert("Please enter maximum 150 character. ");
                return false;
            }
            $('#ContentPlaceHolder1_loderid').css('display', '');
            //if ($('#ContentPlaceHolder1_btnPreviewECard').val() == "") {
            //    alert("Please select recipient details.");
            //}
        }
        function ValidateEmp() {
            var errorMsg = '';
            var txtId;
            txtName = document.getElementById('<%= txtName.ClientID %>');

            if ($(txtName).val() == "") {
                alert("Please enter First Name or Last Name or Token Number");
                return false;
            }
            else {
                return true;
            }
        }
        //For close modal popup by satyam

        function closeSearchPopup(obj) {
            $('#' + $(obj).attr('id')).removeClass('in show');
            $('#' + $(obj).attr('id')).removeAttr('style');
            $('#' + $(obj).attr('id')).hide('modal');
        }
        //End
    </script>


</asp:Content>



﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="QuarterlyAward.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.QuarterlyAward" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
    <%-- <script type="text/javascript">
        $(document).ready(function () {
            $('.rnr-radio-btn table tr td span label').text('');
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .info_input {
            display: inline-block;
            padding: 1px 7px;
            position: absolute;
            left: 75px;
            line-height: 14px;
            border-radius: 50px;
            border: 0;
            background: #24a0ed;
            color: #fff;
            top: 0px;
            z-index: 1;
        }

            .info_input.sec_info_input {
                left: 105px;
            }

        .standard-input input {
            width: 98%;
            height: 47px;
            margin: 1px;
            border: 0px;
            padding: 10px 20px;
        }

        .gridinput {
            border: 0;
            box-shadow: #dadada;
            box-shadow: 0 1px 6px 0 rgb(32 33 36 / 28%);
            margin-top: 10px;
            height: 30px;
            width: 100px;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons {
            margin: 0;
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
            float: none;
        }

            #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
                display: inline-block;
            }

        .send-ecard .nom-box-top h5 input {
            font-weight: 600;
            font-size: 20px;
            margin-bottom: 0;
            padding: 0;
        }

        #ContentPlaceHolder1_grdProjectGrid td:nth-child(2) {
            text-align: left !important;
        }

        #ContentPlaceHolder1_grdQuarterlyAward td:nth-child(1) {
            text-align: left !important;
        }

        .send-ecard .search-emp-m input {
            /*padding: 0;*/
            min-height: auto;
        }

        .send-ecard .search-emp-m ul input {
            padding: 0;
            font-size: 16px;
            width: 100%;
            color: #203442;
            font-weight: 500;
        }

        .send-ecard label.nom-box-lbl input {
            font-size: 11px;
            color: #fff!important;
            padding: 0;
        }

        .send-ecard .search-emp-m .send-ecard-message textarea {
            font-size: 18px;
            padding: 0 20px;
            min-height: 40px;
            border: 0;
            font-family: Calibri!important;
        }

        .send-awards.send-ecard input {
            margin: 0 auto;
            width: auto;
            padding: 10px 35px;
        }

        .send-awards input {
            width: 270px;
            padding: 9px;
            font-size: 20px;
            font-weight: 500;
            border-radius: 6px;
        }

        .grdAwards_new th {
            padding: 0px 10px !important;
        }

        div.grid01 table tr th:last-child td {
            padding: 0px 7px !important;
        }

        input.rnr-btn {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 110px;
        }

        input.rnr-btnGrid {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 20px;
        }

        .rnr-green input {
            font-weight: 600;
            margin: 0 0 -4px;
            font-size: 35px;
            -webkit-appearance: none;
            background: transparent;
            border: 0;
            color: #30ce8d!important;
        }

        #ContentPlaceHolder1_grdQuarterlyAward_rdAwardAmountGridHeader td:nth-child(1) {
            padding-left: 3px !important;
            width: 10px;
        }

        #ContentPlaceHolder1_grdQuarterlyAward_rdAwardAmountGridHeader td:nth-child(2) {
            width: 10px;
        }

        #ContentPlaceHolder1_grdQuarterlyAward_rdAwardAmountGridHeader {
            width: 50% !important;
        }

        #ContentPlaceHolder1_grdProjectGrid tr.row01 td:nth-child(4) table {
            float: left !important;
        }

        #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_1, #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_2,
        #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_3, #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_4,
        #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_5, #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_6,
        #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_7, #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_8,
        #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_9, #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_10,
        #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_11, #ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_12 {
            float: left !important;
        }

        /*table#ContentPlaceHolder1_grdQuarterlyAward td:nth-child(4) {
            float: left;
        }*/




        #ContentPlaceHolder1_TxtAmount {
            background-color: #eee;
            padding: 0 12px;
            width: 100%;
        }

        table.grdAwards_new td:nth-child(1) {
            white-space: inherit !important;
        }

        .container-margin {
            position: relative;
            margin-bottom: 120px;
        }

        #SummaryPanel {
            padding-top: 0px;
        }

        .rnr-radio-btn table tr td span::after {
            content: attr(data-value);
            position: absolute;
            width: 125px;
            left: 30px;
        }

        .rnr-table {
            margin-top: 0;
        }

        .tam_text {
            font-size: 20px;
            font-weight: 600;
        }

        abbr[data-title] {
            position: relative;
            text-decoration: underline dotted;
        }

            abbr[data-title]:hover::after,
            abbr[data-title]:focus::after {
                content: attr(data-title);
                position: absolute;
                left: 60px;
                top: -15px;
                transform: translateX(-50%);
                width: auto;
                white-space: nowrap;
                background: #f9f9f9;
                color: #000;
                border-radius: 2px;
                box-shadow: 1px 1px 5px 0 rgba(0, 0, 0, 0.4);
                font-size: 14px;
                padding: 3px 5px;
            }

        .rnr-breadcrumb {
            padding: 2px 20px 0 0;
        }

        .info_input2 {
            display: inline-block;
            padding: 1px 7px;
            position: absolute;
            left: -25px;
            line-height: 14px;
            border-radius: 50px;
            border: 0;
            background: #24a0ed;
            color: #fff;
            top: -38px;
            z-index: 1;
        }

        .rnr-h3-top {
            color: #fff;
            font-size: 28px;
            font-weight: 300;
            text-align: center;
            transform: translateY(-10px);
            margin-top: 0px;
            position: relative;
            top: -14px;
        }

        .rnr-tab-pane.tab-content {
            position: relative;
            margin-top: 0;
            top: -25px;
        }

        .rnr-inner-top {
            position: fixed;
            width: 100%;
            top: 60px;
            z-index: 1;
            background-color: #203442;
            height: 65px;
        }

        ul.breadcrumb {
            background: transparent;
        }

            ul.breadcrumb li + li:before {
                color: #000;
            }

        .rnr-tabs h3.rnr-h3-top {
            color: #000;
        }

        .appendin {
            display: block;
            width: 225px;
        }

        .span-des {
            top: 40px;
            position: relative;
            left: 29px;
        }

        .btn-Nomination {
            border: none;
            border-radius: 5px;
            color: #fff;
            background-color: #30ce8d;
            padding: 5px 5px;
            font-weight: 600;
            font-size: 16px;
        }

        td span.AlignRight {
            word-wrap: break-word;
            width: 175px;
            display: block;
        }

        table#ContentPlaceHolder1_grdProjectGrid_rdAwardAmountProjectGrid_0 {
            display: inherit !important;
        }

        #ContentPlaceHolder1_grdProjectGrid_rdAwardDecisionProjectGrid_0 td:nth-child(1){
            white-space: inherit !important;
            display: inherit !important;
        }

        div.grid01 table td table tr {
            float: inherit !important;
        }

        #ContentPlaceHolder1_grdProjectGrid {
            background-color: #f7f6f1 !important;
        }

        th.appendin {
            top: 1px !important;
        }

         /*div.grid01 table tr th:last-child, div.listDtls table tr th:last-child {
            text-align: center !important;
        }*/

    </style>

    <section class="main-rnr">
        <div class="rnr-inner-top" style="">
            <div class="rnr-breadcrumb">

                <ul class="breadcrumb">
                    <li><a href="/Web_Pages/HomePage.aspx">HOME</a></li>
                    <li>Quarterly Divisional Excellence Awards</li>
                </ul>

                <ul class="budget-widget">
                    <li class="total-buget">
                        <div class="budget-txt">
                            <span class="big-b">Total Budget</span>
                            <span class="small-b">For this Quarter <b></b></span>
                        </div>
                        <h4 class="budget-value">
                            <asp:Label ID="lblBudget" runat="server"></asp:Label></h4>
                    </li>
                    <li class="budget-used">
                        <div class="budget-txt">
                            <span class="big-b">Budget</span>
                            <span class="small-b">Used</span>
                        </div>
                        <h4 class="budget-value">
                            <asp:Label ID="lblExpenses" runat="server"></asp:Label></h4>
                    </li>
                    <li class="budget-available">
                        <%-- <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172"
                            style="fill: #000000;">
                            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                <path d="M0,172v-172h172v172z" fill="none"></path>
                                <g fill="#ffffff">
                                    <path d="M86,0c-47.50716,0 -86,38.49284 -86,86c0,47.50716 38.49284,86 86,86c47.50716,0 86,-38.49284 86,-86c0,-47.50716 -38.49284,-86 -86,-86zM86,14.33333c39.58464,0 71.66667,32.08203 71.66667,71.66667c0,39.58464 -32.08203,71.66667 -71.66667,71.66667c-39.58463,0 -71.66667,-32.08203 -71.66667,-71.66667c0,-39.58463 32.08203,-71.66667 71.66667,-71.66667zM86,41.65625c-1.31576,0 -2.40755,-0.02799 -3.58333,0.22396c-1.17578,0.25195 -2.26758,0.89583 -3.13542,1.56771c-0.86784,0.67188 -1.51172,1.5957 -2.01562,2.6875c-0.5039,1.0918 -0.67187,2.37956 -0.67187,4.03125c0,1.6237 0.16797,2.91146 0.67188,4.03125c0.50391,1.11979 1.14778,2.01563 2.01563,2.6875c0.86784,0.67188 1.95964,1.06381 3.13542,1.34375c1.17578,0.27995 2.26758,0.44792 3.58333,0.44792c1.28776,0 2.65951,-0.16797 3.80729,-0.44792c1.14778,-0.27994 2.04362,-0.67187 2.91146,-1.34375c0.86784,-0.67187 1.51172,-1.56771 2.01563,-2.6875c0.50391,-1.0918 0.89583,-2.40755 0.89583,-4.03125c0,-1.65169 -0.39192,-2.93945 -0.89583,-4.03125c-0.5039,-1.0918 -1.14778,-2.01562 -2.01562,-2.6875c-0.86784,-0.67187 -1.76367,-1.31576 -2.91146,-1.56771c-1.14778,-0.25195 -2.51953,-0.22396 -3.80729,-0.22396zM77.26563,65.61979v64.27604h17.46875v-64.27604z"></path>
                                </g>
                            </g>
                        </svg>--%>
                        <div class="budget-txt">
                            <span class="big-b">Budget</span>
                            <span class="small-b">Available</span>
                        </div>
                        <h4 class="budget-value">
                            <asp:Label ID="lblBalance" runat="server"></asp:Label></h4>
                    </li>
                </ul>
            </div>

        </div>

        <div class="rnr-tabs" style="top: 155px; position: relative; z-index: 0;">
            <h3 class="rnr-h3-top">Quarterly Divisional Excellence Awards for FY- 
                <asp:Label ID="lblFYear" runat="server"></asp:Label>
                and Quarter- 0<asp:Label ID="lblQuarter" runat="server"></asp:Label>
                <asp:HiddenField ID="hdnQuarter" runat="server" />
            </h3>
            <div class="rnr-tab-pane tab-content">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <asp:HiddenField ID="hdnStartDate" runat="server" />
                    <asp:HiddenField ID="hdnEndDate" runat="server" />
                    <div class="container" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="tam_text">
                                    <span>Total Count in List :   </span><span class="t_amount">
                                        <asp:Label ID="lblCount" runat="server"></asp:Label></span>
                                </p>

                                <%--<span id="infotext" Class="info_tt">For awarding separate amounts, Click on 'View' button.</span>--%>
                            </div>
                            <div class="col-md-6 text-right">
                                <asp:Button ID="btnNominationMaster" runat="server" Text="ADD NOMINATIONS" OnClick="btnNominationMaster_Click" CssClass="btn-Nomination" />
                                                            &nbsp;
                            <asp:Button ID="btnExport" runat="server" Text="Export Summary"  CssClass="btn-Nomination" OnClick="btnExport_Click"  />
                            &nbsp;
                            <asp:Button ID="btnExportDetail" runat="server" Text="Export Details"  CssClass="btn-Nomination" OnClick="btnExportDetail_Click" />
                            </div>
                        </div>
                        <div class="rnr-table" id="SummaryPanel">
                            <div id="Div2" class="table-scroll">
                                <div class="grid01" id="div3" runat="server">
                                    <asp:GridView ID="grdProjectGrid" runat="server" AllowSorting="true" Visible="True" AllowPaging="true"
                                        AutoGenerateColumns="false" AlternatingRowStyle-CssClass="row01" Style="margin: 0 auto;"
                                        CssClass="grdAwards_new" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                        Height="100%" Width="100%" OnRowDataBound="grdProjectGrid_RowDataBound" OnRowCommand="grdProjectGrid_RowCommand" OnPageIndexChanging="grdProjectGrid_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>Select</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rdSelect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px" Visible="false">
                                                <HeaderTemplate>ReferenceNo</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMasterReferenceNoProjectGrid" runat="server" Text='<%# Bind("MasterReferenceNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="300px">
                                                <HeaderTemplate>Title</HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStoryProjectGrid" runat="server" Text='<%# Bind("Story") %>' CssClass="AlignRight"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>Nominated By</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreatedByNameProjectGrid" runat="server" Text='<%# Bind("CreatedByName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Visible="false">
                                                <HeaderTemplate>Function</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBusinessUnitProjectGrid" runat="server" Text='<%# Bind("BusinessUnit") %>'></asp:Label>
                                                    <asp:HiddenField ID="hdnSynopsisProjectGrid" runat="server" Value='<%# Bind("Synopsis") %>' />
                                                    <asp:HiddenField ID="hdnBusinessImpact" runat="server" Value='<%# Bind("BusinessImpact") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50px">
                                                <HeaderTemplate>Participants</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeCountProjectGrid" runat="server" Text='<%# Bind("EmployeeCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderStyle-CssClass="appendin" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>Amount/Team Member</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnAwardAmountIndividualProjectGrid" runat="server" Value='<%# Bind("AwardAmountIndividual") %>' />
                                                    <asp:RadioButtonList ID="rdAwardAmountProjectGrid" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdAwardAmountProjectGrid_SelectedIndexChanged">
                                                        <asp:ListItem Value="2000">2K</asp:ListItem>
                                                        <asp:ListItem Value="5000">5K</asp:ListItem>
                                                        <asp:ListItem Value="10000">10K</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>Total Amt</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalAmountProjectGrid" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>Action</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnAwardDecisionProjectGrid" runat="server" Value='<%# Bind("AwardDecision") %>' />
                                                    <asp:RadioButtonList ID="rdAwardDecisionProjectGrid" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="Approve">Approve</asp:ListItem>
                                                        <asp:ListItem Value="Reject">Reject</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                <HeaderTemplate>View</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnViewProjectGrid" runat="server" Text="View"
                                                        CssClass="table-anchor rnr-green" CommandArgument='<%# Bind("MasterReferenceNo") %>' CommandName="View" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                            BorderStyle="Solid" Font-Italic="true" />
                                        <EmptyDataTemplate>
                                            <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No Nominations Present"></asp:Literal>
                                        </EmptyDataTemplate>
                                        <PagerStyle CssClass="dataPager" />
                                        <SelectedRowStyle CssClass="selectedoddTd" />
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3" style="">
                                <asp:Button ID="btnSubmitProjectGrid" runat="server" Text="Submit" Width="200px" CssClass="rnr-btn" OnClick="btnSubmitProjectGrid_Click" />
                            </div>
                        </div>
                    </div>

                    <br />
                    <%--<div class="modal fade search-emp-modal" id="DetailPanel" runat="server" role="dialog" aria-labelledby="exampleModalLabel">--%>
                    <div class="container container-margin">
                        <div class="d_panel"></div>
                        <div id="DetailPanel" runat="server">
                            <div class="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mahindra_drop_down_div rnr-dd award-top-dd standard-input">
                                            <label for="">Title</label>
                                            <asp:TextBox ID="txtStoryName" runat="server" TabIndex="5" MaxLength="200" ReadOnly="true" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mahindra_drop_down_div rnr-dd award-top-dd standard-input">
                                            <label for="">Leader's Remark</label>
                                            <asp:TextBox ID="txtRemark" runat="server" TabIndex="5" MaxLength="200" ></asp:TextBox>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <div class="rnr-input">
                                            <label for="">Synopsis</label>
                                            <label class="min-char" for=""><span id="spnTxtCount"></span></label>
                                            <asp:TextBox ID="txtSynopsis" runat="server" TextMode="MultiLine" Rows="4" ReadOnly="true" TabIndex="5" MaxLength="200" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <div class="rnr-input">
                                            <label for="">Business Impact</label>
                                            <label class="min-char" for=""><span id="Span1"></span></label>
                                            <asp:TextBox ID="txtBusinessImpact" runat="server" TextMode="MultiLine" Rows="4" ReadOnly="true" TabIndex="5" MaxLength="200" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="rnr-input search-emp-m col-md-6" style="border: 0;">
                                        <div class="search-emp" data-toggle="modal" data-target="#ContentPlaceHolder1_searchempmodal">
                                            <label for="">Search Employee for CC</label>
                                            <%--<div class="mess-dflex">
                                                        <p>Search Employee for CC</p>
                                                    </div>--%>
                                            <asp:TextBox ID="txtMailID" runat="server" Rows="4" Enabled="false"></asp:TextBox>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.8 363.8">
                                                <title>Search</title>
                                                <g id="G8" data-name="Layer 2">
                                                    <g id="G9" data-name="Layer 1">
                                                        <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <%--                                   <div class="col-md-6">
                                        <div class="rnr-input">
                                            <label for="">CC Email ID</label>
                                            <label class="min-char" for=""><span id="Span2"></span></label>
                                            <asp:TextBox ID="txtEmail" runat="server" TextMode="MultiLine" Rows="4" ReadOnly="true" TabIndex="5" MaxLength="200" onkeypress="return blockSpecialChar(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    --%>
                                </div>
                            </div>
                            <div class="rnr-table">
                                <div id="Div1" class="table-scroll">
                                    <div class="grid01" id="divNominees" runat="server">
                                        <asp:GridView ID="grdQuarterlyAward" runat="server" AllowSorting="true" Visible="True" AllowPaging="true"
                                            AutoGenerateColumns="false" AlternatingRowStyle-CssClass="row01" Style="margin: 0 auto;"
                                            CssClass="grdAwards_new" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                            Height="100%" Width="100%" OnRowDataBound="grdQuarterlyAward_RowDataBound" OnPageIndexChanging="grdQuarterlyAward_PageIndexChanging">
                                            <Columns>
                                                <%--PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'--%>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="300px">
                                                    <HeaderTemplate>Title</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("Id") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Story") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Visible="false">
                                                    <HeaderTemplate>Reference Number</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMasterReferenceNo" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px">
                                                    <HeaderTemplate>Participants Name</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnRecipientTokenGrid" runat="server" Value='<%# Bind("RecipientToken") %>' />
                                                        <asp:Label ID="lblRecipientNameGrid" runat="server" Text='<%# Bind("RecipientName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <HeaderTemplate>Business Unit</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnAFLCToken" runat="server" Value='<%# Bind("AFLCToken") %>' />
                                                        <asp:HiddenField ID="hdnAFLCName" runat="server" Value='<%# Bind("AFLCName") %>' />
                                                        <asp:Label ID="lblBusinessUnitGrid" runat="server" Text='<%# Bind("BusinessUnit") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <HeaderTemplate>
                                                        <asp:RadioButtonList ID="rdAwardAmountGridHeader" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rdAwardAmountGridHeader_SelectedIndexChanged" AutoPostBack="true" ToolTip="Select All">
                                                            <asp:ListItem Value="2000">2K</asp:ListItem>
                                                            <asp:ListItem Value="5000">5K</asp:ListItem>
                                                            <asp:ListItem Value="10000">10K</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnAwardAmountGrid" runat="server" Value='<%# Bind("AwardAmount") %>' />

                                                        <asp:RadioButtonList ID="rdAwardAmountGrid" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdAwardAmountGrid_SelectedIndexChanged">
                                                            <asp:ListItem Value="2000">2K</asp:ListItem>
                                                            <asp:ListItem Value="5000">5K</asp:ListItem>
                                                            <asp:ListItem Value="10000">10K</asp:ListItem>
                                                        </asp:RadioButtonList>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                BorderStyle="Solid" Font-Italic="true" />
                                            <EmptyDataTemplate>
                                                <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No Nominations Present"></asp:Literal>
                                            </EmptyDataTemplate>
                                            <PagerStyle CssClass="dataPager" />
                                            <SelectedRowStyle CssClass="selectedoddTd" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="tam_text">
                                            <span>Total :   </span><span class="t_amount">
                                                <asp:Label ID="lblTotal" runat="server"></asp:Label></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="nominee-name" style="margin-top: 10px;">
                                            <div class="rnr-radio-btn">
                                                <div class="radio direct-reportee" style="margin-top: 0!important;">
                                                    <asp:RadioButtonList ID="rdAwardDicision" runat="server">
                                                        <asp:ListItem Text="Approve" data-value="Approve"></asp:ListItem>
                                                        <asp:ListItem Text="Reject" data-value="Reject"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6" style="">
                                        <asp:Button ID="btnSubmitDetailedData" runat="server" Text="Submit" Width="200px" CssClass="rnr-btn" OnClick="btnSubmitDetailedData_Click" />
                                    </div>
                                    <asp:HiddenField ID="hdnRefNumber" runat="server" />
                                    <asp:HiddenField ID="hdnAwdAmt" runat="server" />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- search emp modal start-->
    <div class="modal fade search-emp-modal" id="searchempmodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Search Employee</h5>
                </div>
                <div class="modal-body">
                    <div class="rnr-search-bar">
                        <div class="search-form form">
                            <label for="" class="search-top-lbl">Refine Search</label>
                            <label>

                                <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                                <div class="btnRed SearchButtons">
                                    <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text=""
                                        OnClick="btnGetDetails_Click" TabIndex="3" OnClientClick="return ValidateEmp()" />
                                </div>
                            </label>

                        </div>
                    </div>
                    <br />
                    <div class="rnr-table">
                        <div id="Div4" class="table-scroll">
                            <div id="divEmailList" runat="server" class="grid01">
                                <asp:GridView ID="grdEmpList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                    OnRowCommand="grdEmpList_RowCommand" Visible="true"
                                    AllowPaging="true" OnPageIndexChanging="grdEmpList_PageIndexChanging" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                                    <Columns>
                                        <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                        <asp:BoundField DataField="EmailID" HeaderText="Email Id" SortExpression="EmailID" />
                                        <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                        <asp:BoundField DataField="DivisionName_PA" HeaderText="Division Name" SortExpression="LocationName_PSA" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select" ToolTip="Select Employee for adding CC"
                                                    runat="server" CssClass="selLink1" TabIndex="4">
                                                Select</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="dataPager" />

                                </asp:GridView>
                            </div>



                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <%--<button type="button" class="close" data-dismiss="modal" id="ContentPlaceHolder1_searchempmodal_Close" aria-label="Close" onclick="return closeSearchPopup(this);">
                        <span aria-hidden="true">Cancel</span>
                    </button>--%>
                    <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" CssClass="rnr-btn" />

                </div>
            </div>
        </div>
    </div>
    <!-- search-emp-modal end -->


    <script type="text/javascript" src="../../Scripts/jquery-3.5.1.min.js"></script>
    <script>


        $(document).ready(function () {
            $('.appendin').append('<span class="span-des" draggable="false"><abbr data-title="For awarding separate amounts, Click on View button."><asp:Button ID="btnBehaviourdisplay" runat="server" cssClass="info_input2 sec_info_input" Text="i" /></abbr></span>');
            $("#ContentPlaceHolder1_btnBehaviourdisplay").hover(function () {
                //alert('Hi');
                $("#infotext").modal('toggle');
            });
        });

        //rakesh
        //function SelectRow() {
        //    debugger
        //    alert('Hiiiiiiiiiii');
        //    var result = $("input[name:'RefNumbergrp']:checked").val();
        //    //var result1 = ("#RefNumbergrp").checked
        //    if (result.length <= 0)
        //        alert('Please select data ');
        //    //    return true;
        //    //else
        //        //return false;
        //}
        //$("#ContentPlaceHolder1_btnSubmitProjectGrid").click(function () {
        //    alert('Hi');
        //    var result = $("input[name:'RefNumbergrp']:checked").val();
        //    alert(result+" Test");
        //    //var result1 = ("#RefNumbergrp").checked
        //    if (result.length <= 0)
        //        alert('Please select data ');
        //});
    </script>
</asp:Content>


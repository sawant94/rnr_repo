﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecipientAward.aspx.cs" MasterPageFile="~/Web_Pages/MM_Rewards.Master" Inherits="MM_Rewards.RecipientAward" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />--%>

    <style>
        .rnr-radio-btn .radio table tr td {
            width: 82%;
        }

        .rnr-radio-btn table tr td span {
            position: relative;
        }

            .rnr-radio-btn table tr td span::after {
                content: attr(data-value);
                position: absolute;
                width: 125px;
                left: 30px;
            }
        @media (max-width: 767px) {
            .rnr-tabs {
                margin-top: -20px;
            }
        }
        
        @media (max-width: 768px) {
            .rnr-radio-btn .radio table tr {
                display: table!important;
            }
        }
        .rnr-radio-btn {
            /*margin-bottom: 15px;*/
        }
    </style>
     <script type="text/javascript" src="../../Scripts/jquery-3.5.1.min.js"></script>
    <%--<script src="../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>--%>
    <script src="../../Scripts/Validations.js" type="text/javascript"></script>

    <script>
        function ClosePopup() {
            $('.semodal').css('display', 'none');
            scrollWin();
        }

        function scrollWin() {
            window.scrollBy(0, 500);
        }
    </script>
    <script type="text/javascript">
        function ValidateTextLength(source, args) {
            var length = document.getElementById('<%=txtEmpReason.ClientID %>').value.length;
            //this would make it valid unless the length is greater than 250
            if (length >= 30 && length <= 350) {
                args.IsValid = true;
            }
            else {
                alert('You may only enter 350 characters. You have entered ' + length + ' characters in Reason for nomination');
                args.IsValid = false;
            }
        }



        $('#ContentPlaceHolder1_ddlAwardType option:nth-child(1)').attr('disabled', true);
        $('#ContentPlaceHolder1_ddlHodReportEmployee option:nth-child(1)').attr('disabled', true);
    </script>

    <script type="text/javascript">

        function OnSelectedIndexChange1() {

            var Value = AwardType.options[AwardType.selectedIndex].text;
            var Awardvalue = Value.split('.');
            var Balance = document.getElementById('<%=HiddenBalance.ClientID %>');
            var TotalBal = Math.floor(Balance.defaultValue);

            if (Math.floor(Awardvalue[1]) > TotalBal) {
                alert('You do not have budget for the selected Award. Please select another award.');
                document.getElementById('<%=btnGetEmpDetails.ClientID  %>').disabled = true;
            }
            else {
                document.getElementById('<%=btnGetEmpDetails.ClientID  %>').disabled = false;
            }
        }
        function ValidatePage() {
            var errorMsg = '';
            var txtId;

            txtId = document.getElementById('<%=ddlAwardType.ClientID %>')
            errorMsg += CheckDropdown_Blank(txtId.value, " Award Type");

            txtId = document.getElementById('<%= txtTokenNumberTop.ClientID %>');
            errorMsg += CheckText_Blank(txtId.value, 'Token Number / Name');
            errorMsg += ValidateName(txtId.value, 'Token Number / Name');

            if (errorMsg != '') {
                alert(errorMsg);
                return false;
            }
            else {
                return true;
            }
            scrollWin();
        }

        function ValidateEmp() {
            var errorMsg = '';
            var txtId;
            txtName = document.getElementById('<%= txtName.ClientID %>');

            if ($(txtName).val() == "") {
                alert("Please enter First Name or Last Name or Token Number");
                return false;
            }
            else {
                return true;
            }
        }

        //For close modal popup byy satyam

        function closeSearchPopup(obj) {
            $('#' + $(obj).attr('id')).removeClass('in show');
            $('#' + $(obj).attr('id')).removeAttr('style');
            $('#' + $(obj).attr('id')).hide('modal');
        }
        //End
        function ValidateOnSubmit() {
            debugger;
            var r = confirm("Award Email Send Immediately to Employee , You can't Reverse Award once submit, Do you want to submit record?");
            //alert(r);
            if (r != true) {
                return false;
            }

            $('#ContentPlaceHolder1_txtReasonPreview').val($('#ContentPlaceHolder1_txtEmpReason').val());
            //var firstName = $('#ContentPlaceHolder1_lblfirstname').val();
            //$('#ContentPlaceHolder1_txtReasonPreview').val(textLength);
            //var firstName = '<%=Session["FirstName"]%>';
            //return false;
            //if (firstName == null || firstName == '' || firstName ==' '||firstName=='Nominee Name' ) {
            //    alert('Please select Recipient Name To Give Award!!!');
            //    return false;
            //}
            var errorMsg = '';
            var txtId;

            var AwardType = document.getElementById('<%=ddlGifts.ClientID %>');


            var Value = AwardType.options[AwardType.selectedIndex].text;

            errorMsg += CheckDropdown_Blank(Value, " Award Type");
            txtId = document.getElementById('<%=txtEmpReason.ClientID  %>')
            errorMsg += CheckText_Blank(txtId.value, "Reason of Nomination");

            if (AwardType.options[AwardType.selectedIndex].value.split('|')[1] == "1") {

                if (!confirm("The award you have selected will be auto redeemed by salary.Do you wish to proceed?"))
                { return false; }
            }


            if (errorMsg != '') {
                alert(errorMsg);
                return false;
            }
            else {
                return true;
            }
        }
        function scrollPreview() {
            if (jQuery.browser.msie && jQuery.browser.version.substring(0, 1) == 7) {
                alert('You are using IE 7');
                window.scrollTo(100, 500);
                return false;
            }
            return true;
        }
        /****Done by Kiran on 02 july 2013**/
        $(document).ready(function () {

            //            var t = "<%=this.scroll1 %>";
            //            if(t=="1")
            //            {
            //                if (jQuery.browser.msie && jQuery.browser.version.substring(0, 1) == 7) {
            //                alert('You are using IE 7');
            //                window.scrollTo(100, 500);
            //                 return false;
            //             }
            //         }

            $('.closePop').click(function () {
                $('.registerPopup').fadeOut('fast');
                $('.popupGreyBox').fadeOut('fast');
                $('.popupGreyBox1').fadeOut('fast');
                //$(".popuptbl").show()
                //$('.modal').css('display', 'block');
            });



        });

        function CheckTextLimit(obj) {
            var textLenth = $('#ContentPlaceHolder1_txtEmpReason').val().trim().length;

            var textEmpResonLenth = $('#ContentPlaceHolder1_txtEmpReason').val().trim().length;
            if (textLenth < 30) {
                alert("Please enter minimum 30 character in Text for the certificate.");
                return false;
            }
            else if (textLenth > 150) {
                alert("Please enter maximum 150 character in Text for the certificate.");
                return false;
            }
            if (textEmpResonLenth < 30) {
                alert("Please enter minimum 30 character in Reason for nomination.");
                return false;
            }
            else if (textEmpResonLenth > 150) {
                alert("Please enter maximum 150 character in Reason for nomination.");
                return false;
            }
            else {
                $('#award-preview-popup').modal('show');
            }

        }

        $(document).ready(function () {
            var t = "<%=this.scroll1 %>";
            if (t == "1") {
                if (jQuery.browser.msie && jQuery.browser.version.substring(0, 1) == 7) {
                    window.scrollTo(100, 500);
                    return false;
                }
            }
        });

    </script>


    <style type="text/css">
        div.form01 table td input, table select, div.form01 table td {
            font-size: 14px;
        }

        .popupGreyBox {
            display: none;
            background: none repeat scroll 0 0 Black;
            bottom: 0;
            height: 1111px !important;
            left: 0;
            opacity: 0.49;
            filter: alpha(opacity=50);
            position: absolute;
            right: 0;
            top: 0;
            width: 100%;
            z-index: 100;
        }

        .registerPopup {
            display: none;
            background-color: White;
            position: absolute;
            width: 1050px;
            left: 0;
            z-index: 101;
            top: 0%;
        }

        .popHead {
            background: none repeat scroll 0 0 #F2F2F3;
            clear: both;
            font-weight: bold;
            height: 100%;
            overflow: hidden;
            padding: 5px 10px;
        }

            .popHead .closePop {
                float: right;
            }

                .popHead .closePop span {
                    background: url("/Includes/images/popupClose.gif") no-repeat scroll 0 0 transparent;
                    cursor: pointer;
                    display: block;
                    height: 10px;
                    width: 10px;
                }

        .ptpshowup {
            display: block;
        }

        .Timepick {
            display: inline-block;
        }

        .othertable {
            position: absolute;
            left: 40%;
            bottom: 0;
        }

        .tbl_rec_list {
            width: 80%;
            border-collapse: collapse;
        }

            .tbl_rec_list tr td {
                padding: 10px 10px;
                border: 1px solid #888;
            }

        div.btnRed {
            display: inline-block;
            float: none;
            margin: 0 0 0 15px;
        }

        .tbl_rec_list {
            margin-top: 15px;
        }

            .tbl_rec_list tr th {
                font-weight: normal;
                font-size: 16px;
                border: 1px solid #272727;
                background-color: #272727;
                color: #fff;
                padding: 10px 10px;
            }

        div.grid01 table td, div.listDtls table td {
            font-size: 16px;
            vertical-align: middle;
        }

        .modal_inner div.grid01 {
            margin: 0;
        }

        @media screen and (max-width: 1024px) {
            .othertable {
                position: inherit;
                left: 0;
                margin-bottom: 20px;
            }

            .tbl_rec_list {
                width: 100%;
            }
        }

        @media screen and (min-width: 1920px) {
            .othertable {
                left: 28%;
            }

            .registerPopup {
                left: 10%;
            }
        }
        /*css for tooltip*/
        .tooltip {
            position: relative;
            display: inline-block;
            /*border-bottom: 1px dotted black;*/
            opacity: 1;
            color: #e31837;
        }


        .tooltiptext ul.textnav .active a {
            position: relative;
        }

        .tooltiptext.main {
            display: block;
            visibility: visible;
        }

        ul.infographics1 li img.imgicons {
            float: left;
            width: 50px;
            height: 50px;
            padding: 6px;
            background-color: #fff;
            margin: -7px 15px 12px 0;
            border-radius: 5px;
            border: 0px;
            box-sizing: border-box;
        }

        .tooltiptext {
            width: 279px;
            color: #fff;
            display: none;
            position: absolute;
            z-index: 1;
            top: 90px;
            left: 150px;
        }

            .tooltiptext h4 {
                border: 3px solid #fdd303;
                display: inline-block;
                width: auto;
                float: none;
                margin: 5px auto 10px;
                padding: 10px 30px;
                text-align: center;
                font-size: 28px;
            }

            .tooltiptext:hover {
                display: block;
            }

            .tooltiptext ul {
                list-style-type: decimal;
                padding: 10px;
            }

            .tooltiptext p {
                margin-bottom: 0;
                font-size: 15px;
                float: left;
                width: 100%;
                white-space: normal;
                text-align: left;
            }

            .tooltiptext ul li {
                font-size: 14px;
            }

        .tiparea .tab-content {
            position: absolute;
            left: 30px;
            top: 20px;
            width: auto;
            background-color: rgba(227, 24, 55, 1);
            background-image: url(../../images/new_images/innerpagebg.png);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            border-radius: 6px;
            padding: 0;
        }

        .tiparea {
            float: left;
            width: 100%;
            /* background-color: #fff; */
            z-index: 1071;
            position: relative;
            /* background-color: rgba(227, 24, 55, 1); */
            /* background-image: url(../../images/new_images/innerpagebg.png); */
            /* background-repeat: no-repeat; */
            /* background-size: cover; */
            /* background-position: center; */
            /* border-radius: 6px; */
            /* padding: 85px 15px 30px; */
        }

        .tooltiptext ul.textnav {
            list-style-type: none;
            position: absolute;
            left: 150px;
            top: -40px;
            transform: translate(-50%, 0%);
            width: 250px;
            padding: 0;
        }

            .tooltiptext ul.textnav a {
                float: left;
                width: 50px;
                height: 50px;
                background: #e31837;
                margin: 5px;
                border-radius: 50%;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border: 4px solid #fff;
                text-align: center;
                line-height: 43px;
                transition: 0.2s all;
            }

            .tooltiptext ul.textnav .active a {
                border: 2px solid #fdd303;
            }


        ul.infographics1 li {
            width: 100%;
            height: auto;
            float: left;
            background: transparent;
            margin: 0 0 5px;
            border-radius: 0;
            text-align: left;
            word-break: break-word;
            white-space: normal;
            padding: 0;
            color: #fff;
            vertical-align: middle;
            justify-content: center;
            align-items: center;
            font-size: 13px;
            position: relative;
            background-image: none;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            display: inline-block;
        }

        ul.infographics1:before {
            content: "";
            width: calc(100% - 50px);
            height: 5px;
            background: #fff;
            position: absolute;
            left: 20px;
            top: 50%;
            display: none;
        }

        ul.infographics1 {
            float: left;
            width: 100%;
            DISPLAY: inline-block;
            list-style: none;
            position: relative;
            padding-left: 0;
        }

        .tooltiptext .tab-pane {
            float: left;
            width: 720px;
            text-align: center;
            padding: 15px 15px 30px 50%;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            height: 400px;
        }

        .tooltiptext #spotawarddata p, .tooltiptext #excelleratordata p, .tooltiptext #coffeedata p {
            position: relative !important;
            font-size: 14px;
            float: left !important;
            text-align: left;
            margin: 0px 0 10px;
            width: 100%;
            float: right;
        }

        #spotawarddata h4, #excelleratordata h4, #coffeedata h4 {
            border-width: 0 0 2px 0;
            padding: 0 0 5px;
            position: relative;
            top: 0px;
            width: 100%;
            float: left;
            text-align: left;
            margin-bottom: 10px;
        }

        #coffeedata {
            padding: 105px 15px 30px 50%;
        }


        .tooltiptext ul.textnav a img {
            height: 25px;
        }


        .blink_me {
            animation: blinker 1s linear infinite;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }


        @media (max-width:1024px) {
            .tooltiptext .tab-pane {
                width: 500px;
            }

            #spotawarddata h4, #excelleratordata h4, #coffeedata h4 {
                border-width: 2px 2px 2px 2px;
                padding: 5px 15px;
                position: relative;
                top: 0;
                left: 0;
            }

            ul.infographics1 {
                padding-left: 0;
            }

            div#spotawarddata, div#excelleratordata, div#coffeedata {
                background-image: none !important;
                padding: 15px;
            }

            .tooltiptext #spotawarddata p, .tooltiptext #excelleratordata p, .tooltiptext #coffeedata p {
                right: auto;
                width: 100%;
                position: relative;
                top: auto;
                font-size: 14px;
            }

            div.form01 table td.lblName {
                position: relative;
            }

            ul.infographics1 li {
                width: 100%;
                height: auto;
                float: left;
                background: transparent;
                margin: 0 0 5px;
                border-radius: 0;
                text-align: left;
                word-break: break-word;
                white-space: normal;
                padding: 0;
                color: #fff;
                vertical-align: middle;
                justify-content: center;
                align-items: center;
                font-size: 14px;
                position: relative;
                background-image: none;
                background-repeat: no-repeat;
                background-size: cover;
                background-position: center;
                display: inline-block;
            }

            ul.infographics1:before {
                display: none;
            }

            .tooltiptext h4 {
                font-size: 18px;
            }
        }



        @media (max-width:991px) {
            .tooltiptext {
                top: 180px;
                left: 0;
            }
        }



        @media (max-width:768px) {


            .tooltiptext .tab-pane {
                width: 300px;
            }

            .tooltiptext {
                width: 159px;
                top: 70px;
                left: -30px;
            }
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons {
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
        }

            #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
            }

        #ContentPlaceHolder1_searchnomineemodal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        #ContentPlaceHolder1_searchnomineemodal .rnr-search-bar div.search-form label .SearchButtons {
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
        }

            #ContentPlaceHolder1_searchnomineemodal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
            }
        .info_input {
            display: inline-block;
    padding: 1px 7px;
    position: absolute;
    left: 75px;
    line-height: 14px;
    border-radius: 50px;
    border: 0;
    background: #24a0ed;
    color: #fff;
    top: 0px;
    z-index: 1;
        }

        .info_input1 {
            display: inline-block;
            padding: 1px 7px;
            position: relative;
            left: -50px;
            line-height: 14px;
            border-radius: 50px;
            border: 0;
            background: #24a0ed;
            color: #fff;
            top: 0px;
            z-index: 1;
            cursor: pointer;
        }

        .info_input2 {
    display: inline-block !important;
    padding: 1px 7px !important;
    position: absolute !important;
    left: 116px !important;
    line-height: 14px !important;
    border-radius: 50px !important;
    border: 0 !important;
    background: #24a0ed !important;
    color: #fff !important;
    top: 2px !important;
    z-index: 999 !important;
    width: auto !important;
    min-height: 0 !important;
}
            .info_input.sec_info_input {
                left: 105px;
            }
        .rnr-radio-btn .radio table tr td span.info_tt {
            position: absolute;
    background: #eee;
    padding: 5px 10px;
    border-radius: 10px;
    z-index: 1;
    top: 10px;
    left: 0;
    transform: translateX(115px) translateY(-18px);
    opacity: 0;
    visibility: hidden;
    transition: 0.4s ease;
        }
        .info_input.sec_info_input:hover + .info_tt {
            opacity: 1;
            visibility: visible;
        }
        .rnr-radio-btn .radio table tr td span.info_tt ul {
            margin: 0;
        }
        .rnr-radio-btn .radio table tr td span.info_tt ul li {
        }







        .info_input2.sec_info_input1 {
                left: 105px;
            }
        span.info_tt1 {
            position: absolute;
    background: #eee;
    padding: 5px 22px;
    border-radius: 10px;
    z-index: 9999;
    top: 10px;
    left: 0;
    transform: translateX(70px) translateY(15px);
    opacity: 0;
    visibility: hidden;
    transition: 0.4s ease;
    width: 320px;
        }
        .info_input2.sec_info_input1:hover + .info_tt1 {
            opacity: 1;
            visibility: visible;
        }
        span.info_tt1 ul {
            margin: 0;
            list-style-type: circle;
            font-weight: 500;
        }
        span.info_tt1 ul li {
        }


        .modal-img {
            text-align: center;
        }

        .close-icon {
            color: white;
            opacity: 1;
        }

        .header-bg {
            background-color: #e31837;
        }

        /*.rnr-tabs .nav-item.disabled_area:hover {
    opacity: 0.9;
    border: 2px solid #30ce8d;
    transition: all 0.5s ease-in-out;
    cursor: not-allowed! important;
}*/

        /*.rnr-tabs .nav-item.disabled_area:hover svg path.cp-stroke {
    stroke: #ffc107;
}*/

        /*.rnr-tabs .nav-item.disabled_area:hover input {
    fill: #ffc107!important;
    color: #ffc107!important;
    opacity: 0.5;
}

        rnr-tabs .nav-item.disabled_area input {
       opacity: 0.3;
}*/
    </style>
    <script type="text/javascript">
        function showModal() {
            $('.modal').css('display', 'block');
        }

    </script>

     <script type="text/javascript" src="../../Scripts/jquery-3.5.1.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>--%>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <%--<script>
        $(document).ready(function () {
            $("#tooltext").click(function () {
                $("#idtooltiptext").toggleClass("main");
            });
        });
</script>--%>
    <script>
       
        $(function () {
            $("#tooltext").on("click", function (e) {
                $("#idtooltiptext").addClass("main");
                e.stopPropagation()
            });
            $(document).on("click", function (e) {
                if ($(e.target).is("#idtooltiptext") === false) {
                    $("#idtooltiptext").removeClass("main");
                }
            });
        });
        $(document).ready(function () {
            $(".award_for_btn table tbody tr:nth-child(3) td span").append('<asp:Button ID="btnBehaviourdisplay" runat="server" cssClass="info_input sec_info_input" Text="i" />');
            $(".award_for_btn table tbody tr:nth-child(3) td span").append('<span Class="info_tt"><ul><li>Collaborate</li><li>Agile</li><li>Bold</li></ul></span>');
            $(".certificate-i .certificate-label").append('<asp:Button ID="btnBehaviourdisplay1" runat="server" cssClass="info_input2 sec_info_input1" Text="i" />');
            $(".certificate-i .certificate-label").append('<span Class="info_tt1"><ul><li>This will be displayed in the F22 Appraisal Sheet</li><li>Talent Panel will be able to access this comment</li></ul></span>');
            $("#btnHoverImg").hover(function () {
                //alert('Hi');
                $("#ImageInfo").modal('toggle');
            });
        })

        $(document).ready(function () {
            
        })
    </script>


    <!--Done by Kiran on 02 july 2013 end--ImageInfo-->

    <%--<asp:HiddenField ID="lvl" runat="server" />--%>
    <!--Done by Swapneel on 07 Apr 2014-->
    <asp:HiddenField ID="hdnNomEmail" runat="server" />
    <asp:Label ID="lblNote" runat="server" Font-Italic="True" Text="" Visible="false"></asp:Label>
    <section class="main-rnr">
        <div class="rnr-inner-top">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href='index-HOD.html'>HOME</a></li>
                    <li><a href="#">Awards</a></li>
                    <li>Give Awards</li>
                </ul>
                <ul class="budget-widget">
                    <li class="total-buget">
                        <div class="budget-txt">
                            <span class="big-b">Total Budget</span>
                            <span class="small-b">For this Year <b></b></span>
                        </div>
                        <h4 class="budget-value">
                            <asp:Label ID="lblBudget" runat="server" ></asp:Label></h4>
                    </li>
                    <li class="budget-used">
                        <div class="budget-txt">
                            <span class="big-b">Budget</span>
                            <span class="small-b">Used</span>
                        </div>
                        <h4 class="budget-value">
                            <asp:Label ID="lblExpenses" runat="server"></asp:Label></h4>
                    </li>
                    <li class="budget-available">
                       <%-- <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172"
                            style="fill: #000000;">
                            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                <path d="M0,172v-172h172v172z" fill="none"></path>
                                <g fill="#ffffff">
                                    <path d="M86,0c-47.50716,0 -86,38.49284 -86,86c0,47.50716 38.49284,86 86,86c47.50716,0 86,-38.49284 86,-86c0,-47.50716 -38.49284,-86 -86,-86zM86,14.33333c39.58464,0 71.66667,32.08203 71.66667,71.66667c0,39.58464 -32.08203,71.66667 -71.66667,71.66667c-39.58463,0 -71.66667,-32.08203 -71.66667,-71.66667c0,-39.58463 32.08203,-71.66667 71.66667,-71.66667zM86,41.65625c-1.31576,0 -2.40755,-0.02799 -3.58333,0.22396c-1.17578,0.25195 -2.26758,0.89583 -3.13542,1.56771c-0.86784,0.67188 -1.51172,1.5957 -2.01562,2.6875c-0.5039,1.0918 -0.67187,2.37956 -0.67187,4.03125c0,1.6237 0.16797,2.91146 0.67188,4.03125c0.50391,1.11979 1.14778,2.01563 2.01563,2.6875c0.86784,0.67188 1.95964,1.06381 3.13542,1.34375c1.17578,0.27995 2.26758,0.44792 3.58333,0.44792c1.28776,0 2.65951,-0.16797 3.80729,-0.44792c1.14778,-0.27994 2.04362,-0.67187 2.91146,-1.34375c0.86784,-0.67187 1.51172,-1.56771 2.01563,-2.6875c0.50391,-1.0918 0.89583,-2.40755 0.89583,-4.03125c0,-1.65169 -0.39192,-2.93945 -0.89583,-4.03125c-0.5039,-1.0918 -1.14778,-2.01562 -2.01562,-2.6875c-0.86784,-0.67187 -1.76367,-1.31576 -2.91146,-1.56771c-1.14778,-0.25195 -2.51953,-0.22396 -3.80729,-0.22396zM77.26563,65.61979v64.27604h17.46875v-64.27604z"></path>
                                </g>
                            </g>
                        </svg>--%>
                        <div class="budget-txt">
                            <span class="big-b">Budget</span>
                            <span class="small-b">Available</span>
                        </div>
                        <h4 class="budget-value">
                            <asp:Label ID="lblBalance" runat="server"></asp:Label></h4>
                    </li>
                </ul>

                <div class="budget-mobile">
                    <ul class="budget-widget">

                        <li class="budget-available">
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172"
                                style="fill: #000000;">
                                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                    <path d="M0,172v-172h172v172z" fill="none"></path>
                                    <g fill="#ffffff">
                                        <path d="M86,0c-47.50716,0 -86,38.49284 -86,86c0,47.50716 38.49284,86 86,86c47.50716,0 86,-38.49284 86,-86c0,-47.50716 -38.49284,-86 -86,-86zM86,14.33333c39.58464,0 71.66667,32.08203 71.66667,71.66667c0,39.58464 -32.08203,71.66667 -71.66667,71.66667c-39.58463,0 -71.66667,-32.08203 -71.66667,-71.66667c0,-39.58463 32.08203,-71.66667 71.66667,-71.66667zM86,41.65625c-1.31576,0 -2.40755,-0.02799 -3.58333,0.22396c-1.17578,0.25195 -2.26758,0.89583 -3.13542,1.56771c-0.86784,0.67188 -1.51172,1.5957 -2.01562,2.6875c-0.5039,1.0918 -0.67187,2.37956 -0.67187,4.03125c0,1.6237 0.16797,2.91146 0.67188,4.03125c0.50391,1.11979 1.14778,2.01563 2.01563,2.6875c0.86784,0.67188 1.95964,1.06381 3.13542,1.34375c1.17578,0.27995 2.26758,0.44792 3.58333,0.44792c1.28776,0 2.65951,-0.16797 3.80729,-0.44792c1.14778,-0.27994 2.04362,-0.67187 2.91146,-1.34375c0.86784,-0.67187 1.51172,-1.56771 2.01563,-2.6875c0.50391,-1.0918 0.89583,-2.40755 0.89583,-4.03125c0,-1.65169 -0.39192,-2.93945 -0.89583,-4.03125c-0.5039,-1.0918 -1.14778,-2.01562 -2.01562,-2.6875c-0.86784,-0.67187 -1.76367,-1.31576 -2.91146,-1.56771c-1.14778,-0.25195 -2.51953,-0.22396 -3.80729,-0.22396zM77.26563,65.61979v64.27604h17.46875v-64.27604z"></path>
                                    </g>
                                </g>
                            </svg>
                            <div class="budget-txt">
                                <span class="big-b">Budget</span>
                                <span class="small-b">Available</span>
                            </div>
                            <h4 class="budget-value">
                                <asp:Label ID="lblBalanceMobile" runat="server"></asp:Label></h4>
                        </li>

                        <li class="budget-used">
                            <div class="budget-txt">
                                <span class="big-b">Budget</span>
                                <span class="small-b">Used</span>
                            </div>
                            <h4 class="budget-value">
                                <asp:Label ID="lblExpensesMobile" runat="server"></asp:Label></h4>
                        </li>
                        <li class="total-buget">
                            <div class="budget-txt">
                                <span class="big-b">Total Budget</span>
                                <span class="small-b">For this Year <b></b></span>
                            </div>
                            <h4 class="budget-value">
                                <asp:Label ID="lblBudgetMobile" runat="server"></asp:Label></h4>
                        </li>

                    </ul>

                </div>



            </div>
        </div>

        <div class="rnr-tabs">
            <h3 class="rnr-h3-top">Select the Type of Award to be Given</h3>
            
            <div class="tab-list">   
                <ul class="nav nav-tabs" role="tablist">
                    <li>
                        <input id="btnInfodisplay" type="button" value="Test" Class="info_input sec_info_input"  />
                    </li>   
                   <%-- <li class="nav-item">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="46" viewBox="0 0 32 46">
                            <g id="Group_3584" data-name="Group 3584" transform="translate(0 0)">
                                <g id="Path_7472" data-name="Path 7472" transform="translate(7 16)" fill="#fff">
                                    <path d="M 17 28.36334991455078 L 9.456318855285645 24.49480056762695 L 8.999999046325684 24.26078987121582 L 8.543679237365723 24.49480056762695 L 0.9999990463256836 28.36334991455078 L 0.9999990463256836 1 L 17 1 L 17 28.36334991455078 Z" stroke="none" />
                                    <path class="custom-path" d="M 1.999999046325684 2 L 1.999999046325684 26.72671127319336 L 8.087368965148926 23.60498046875 L 8.999999046325684 23.13695907592773 L 9.912629127502441 23.60498046875 L 15.99999904632568 26.72671127319336 L 15.99999904632568 2 L 1.999999046325684 2 M -1.9073486328125e-06 0 L 18 0 L 18 30 L 8.999999046325684 25.38460922241211 L -1.9073486328125e-06 30 L -1.9073486328125e-06 0 Z" stroke="none" />
                                </g>
                                <g id="Group_3583" data-name="Group 3583">
                                    <g id="ic_radio_button_unchecked_24px" transform="translate(-9.318 -9.318)" fill="#fff">
                                        <path d="M 25.31760597229004 40.3176155090332 C 21.30929565429688 40.3176155090332 17.54201507568359 38.75780487060547 14.70972537994385 35.92550659179688 C 11.87742519378662 33.09321594238281 10.3176155090332 29.3259162902832 10.3176155090332 25.31760597229004 C 10.3176155090332 21.30929565429688 11.87742519378662 17.54201507568359 14.70972537994385 14.70972537994385 C 17.54201507568359 11.87742519378662 21.30929565429688 10.3176155090332 25.31760597229004 10.3176155090332 C 29.3259162902832 10.3176155090332 33.09321594238281 11.87742519378662 35.92550659179688 14.70972537994385 C 38.75780487060547 17.54201507568359 40.3176155090332 21.30929565429688 40.3176155090332 25.31760597229004 C 40.3176155090332 29.32592582702637 38.75780487060547 33.09321594238281 35.92550659179688 35.92550659179688 C 33.09321594238281 38.75780487060547 29.32592582702637 40.3176155090332 25.31760597229004 40.3176155090332 Z" stroke="none" />
                                        <path class="custom-path" d="M 25.31760597229004 39.3176155090332 C 29.05881500244141 39.3176155090332 32.57498550415039 37.86181640625 35.21840667724609 35.21840667724609 C 37.86181640625 32.57498550415039 39.3176155090332 29.05881500244141 39.3176155090332 25.31760597229004 C 39.3176155090332 21.5764045715332 37.86181640625 18.06024551391602 35.21840667724609 15.41682529449463 C 32.57498550415039 12.77341556549072 29.05881500244141 11.3176155090332 25.31760597229004 11.3176155090332 C 21.5764045715332 11.3176155090332 18.06024551391602 12.77341556549072 15.41682529449463 15.41682529449463 C 12.77341556549072 18.06024551391602 11.3176155090332 21.5764045715332 11.3176155090332 25.31760597229004 C 11.3176155090332 29.05881500244141 12.77341556549072 32.57498550415039 15.41682529449463 35.21840667724609 C 18.06024551391602 37.86181640625 21.5764045715332 39.3176155090332 25.31760597229004 39.3176155090332 M 25.31760597229004 41.3176155090332 C 16.47761535644531 41.3176155090332 9.317615509033203 34.15761566162109 9.317615509033203 25.31760597229004 C 9.317615509033203 16.47761535644531 16.47761535644531 9.317615509033203 25.31760597229004 9.317615509033203 C 34.15761566162109 9.317615509033203 41.3176155090332 16.47761535644531 41.3176155090332 25.31760597229004 C 41.3176155090332 34.15761566162109 34.15761566162109 41.3176155090332 25.31760597229004 41.3176155090332 Z" stroke="none" />
                                    </g>
                                    <path class="cp-stroke" id="ic_radio_button_unchecked_24px-2" data-name="ic_radio_button_unchecked_24px" d="M18.818,28.318a9.5,9.5,0,1,1,9.5-9.5A9.5,9.5,0,0,1,18.818,28.318Z" transform="translate(-2.818 -2.818)" fill="#fff" stroke-width="2" />
                                    <path class="custom-path cp2" id="ic_star_border_24px" d="M11.474,5.429,8.068,5.136,6.737,2,5.406,5.141,2,5.429,4.586,7.67,3.809,11,6.737,9.233,9.664,11,8.892,7.67Z" transform="translate(9.5 9.5)" stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                </g>
                            </g>
                        </svg>

                        <asp:Button ID="btnSpotAward" runat="server" Text="Spot Awards" OnClick="btnSpotAward_Click" />

                    </li>
                    <li class="nav-item" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="46.795" height="46.93" viewBox="0 0 46.795 46.93">
                            <g id="Group_3587" data-name="Group 3587" transform="translate(26.157 -5.019) rotate(45)">
                                <path class="custom-path" id="Path1" data-name="Path 7472" d="M0,0H12.506L11.1,7.825l-2.387-1.5L6.253,12.671,3.762,6.334,1.408,7.825Z" transform="translate(11.985 36.306)" />
                                <g id="Path_7473" data-name="Path 7473" transform="translate(0 32.258) rotate(-90)" fill="#fff">
                                    <path d="M 1.203179836273193 9.202295303344727 L 2.630786895751953 1.160085678100586 L 14.43874549865723 3.264756679534912 L 19.10613441467285 8.37752628326416 L 1.203179836273193 9.202295303344727 Z" stroke="none" />
                                    <path class="custom-path" d="M 3.440481185913086 2.320172786712646 L 2.406347274780273 8.145807266235352 L 16.92979621887207 7.476727485656738 L 13.9290885925293 4.189680576324463 L 3.440481185913086 2.320172786712646 M 1.821081161499023 4.76837158203125e-06 L 14.94842147827148 2.339844703674316 L 21.2824821472168 9.278324127197266 L 1.9073486328125e-06 10.25878429412842 L 1.821081161499023 4.76837158203125e-06 Z" stroke="none" />
                                </g>
                                <g id="Path_7475" data-name="Path 7475" transform="translate(25.584 32.258) rotate(-90)" fill="#fff">
                                    <path d="M 2.630785942077637 9.098703384399414 L 1.203178882598877 1.056493639945984 L 19.10613441467285 1.88126277923584 L 14.43874454498291 6.994032382965088 L 2.630785942077637 9.098703384399414 Z" stroke="none" />
                                    <path class="custom-path" d="M 3.44047999382019 7.938616275787354 L 13.92908763885498 6.069108486175537 L 16.92979621887207 2.782061338424683 L 2.406346797943115 2.112982273101807 L 3.44047999382019 7.938616275787354 M 1.821081042289734 10.25878429412842 L 1.073608359547507e-06 4.531249942374416e-06 L 21.28248023986816 0.9804645180702209 L 14.94842147827148 7.918944358825684 L 1.821081042289734 10.25878429412842 Z" stroke="none" />
                                </g>
                                <g id="Path_7474" data-name="Path 7474" transform="translate(7.098 0)" fill="#fff">
                                    <path d="M 18.61595726013184 32.59410095214844 L 3.310150623321533 32.59410095214844 C 1.444942355155945 27.23739624023438 0.7040108442306519 22.26541709899902 1.107373952865601 17.80572128295898 C 1.43942403793335 14.1345100402832 2.545124053955078 10.79659080505371 4.393754005432129 7.884680271148682 C 6.940245151519775 3.873521327972412 10.18851566314697 1.791049242019653 11.34798336029053 1.136565566062927 C 12.48130798339844 1.80508828163147 15.64484119415283 3.924594402313232 18.04204368591309 7.95659065246582 C 19.77211380004883 10.86649036407471 20.77585411071777 14.19958019256592 21.02539443969727 17.86330032348633 C 21.32817649841309 22.30863571166992 20.51768112182617 27.26150894165039 18.61595726013184 32.59410095214844 Z" stroke="none" />
                                    <path class="custom-path" d="M 11.33364486694336 2.304798126220703 C 9.996077537536621 3.135356903076172 7.3721923828125 5.058917999267578 5.237993240356445 8.42064094543457 C 3.475534439086914 11.19682121276855 2.420873641967773 14.38471984863281 2.103313446044922 17.89580154418945 C 1.729635238647461 22.02716064453125 2.376432418823242 26.62986755371094 4.026142120361328 31.59410095214844 L 17.90583419799805 31.59410095214844 C 19.58451843261719 26.67630577087402 20.29977416992188 22.11160469055176 20.03281402587891 18.00796127319336 C 19.80529403686523 14.51074981689453 18.86453437805176 11.33176040649414 17.23664474487305 8.559289932250977 C 15.2353458404541 5.150859832763672 12.65197658538818 3.164346694946289 11.33364486694336 2.304798126220703 M 11.35824394226074 0 C 11.35824394226074 0 28.66676330566406 8.398521423339844 19.31618499755859 33.59410095214844 C 18.3805046081543 33.59410095214844 4.638143539428711 33.59410095214844 2.604513168334961 33.59410095214844 C -6.547115325927734 8.398521423339844 11.35824394226074 0 11.35824394226074 0 Z" stroke="none" />
                                </g>
                                <path class="cp-stroke" id="Path2" d="M2.939,5.877A2.939,2.939,0,1,1,5.877,2.939,2.938,2.938,0,0,1,2.939,5.877Z" transform="translate(15.155 11.267)" fill="#fff" stroke-width="2" />
                            </g>
                        </svg>
                        <asp:Button ID="btnExcelleratorAward" runat="server" Text="Excellerator Awards" Width="135px" OnClick="btnExcelleratorAward_Click" />
                    </li>--%>
                    <li class="nav-item" id="exc_btn" runat="server" >
                        <span id="btnHoverImg" class="info_input1">i</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="46.795" height="46.93" viewBox="0 0 46.795 46.93">
                            <g id="G11" data-name="Group 3587" transform="translate(26.157 -5.019) rotate(45)">
                                <path class="custom-path" id="Path7" data-name="Path 7472" d="M0,0H12.506L11.1,7.825l-2.387-1.5L6.253,12.671,3.762,6.334,1.408,7.825Z" transform="translate(11.985 36.306)" />
                                <g id="G12" data-name="Path 7473" transform="translate(0 32.258) rotate(-90)" fill="#fff">
                                    <path d="M 1.203179836273193 9.202295303344727 L 2.630786895751953 1.160085678100586 L 14.43874549865723 3.264756679534912 L 19.10613441467285 8.37752628326416 L 1.203179836273193 9.202295303344727 Z" stroke="none" />
                                    <path class="custom-path" d="M 3.440481185913086 2.320172786712646 L 2.406347274780273 8.145807266235352 L 16.92979621887207 7.476727485656738 L 13.9290885925293 4.189680576324463 L 3.440481185913086 2.320172786712646 M 1.821081161499023 4.76837158203125e-06 L 14.94842147827148 2.339844703674316 L 21.2824821472168 9.278324127197266 L 1.9073486328125e-06 10.25878429412842 L 1.821081161499023 4.76837158203125e-06 Z" stroke="none" />
                                </g>
                                <g id="G13" data-name="Path 7475" transform="translate(25.584 32.258) rotate(-90)" fill="#fff">
                                    <path d="M 2.630785942077637 9.098703384399414 L 1.203178882598877 1.056493639945984 L 19.10613441467285 1.88126277923584 L 14.43874454498291 6.994032382965088 L 2.630785942077637 9.098703384399414 Z" stroke="none" />
                                    <path class="custom-path" d="M 3.44047999382019 7.938616275787354 L 13.92908763885498 6.069108486175537 L 16.92979621887207 2.782061338424683 L 2.406346797943115 2.112982273101807 L 3.44047999382019 7.938616275787354 M 1.821081042289734 10.25878429412842 L 1.073608359547507e-06 4.531249942374416e-06 L 21.28248023986816 0.9804645180702209 L 14.94842147827148 7.918944358825684 L 1.821081042289734 10.25878429412842 Z" stroke="none" />
                                </g>
                                <g id="G14" data-name="Path 7474" transform="translate(7.098 0)" fill="#fff">
                                    <path d="M 18.61595726013184 32.59410095214844 L 3.310150623321533 32.59410095214844 C 1.444942355155945 27.23739624023438 0.7040108442306519 22.26541709899902 1.107373952865601 17.80572128295898 C 1.43942403793335 14.1345100402832 2.545124053955078 10.79659080505371 4.393754005432129 7.884680271148682 C 6.940245151519775 3.873521327972412 10.18851566314697 1.791049242019653 11.34798336029053 1.136565566062927 C 12.48130798339844 1.80508828163147 15.64484119415283 3.924594402313232 18.04204368591309 7.95659065246582 C 19.77211380004883 10.86649036407471 20.77585411071777 14.19958019256592 21.02539443969727 17.86330032348633 C 21.32817649841309 22.30863571166992 20.51768112182617 27.26150894165039 18.61595726013184 32.59410095214844 Z" stroke="none" />
                                    <path class="custom-path" d="M 11.33364486694336 2.304798126220703 C 9.996077537536621 3.135356903076172 7.3721923828125 5.058917999267578 5.237993240356445 8.42064094543457 C 3.475534439086914 11.19682121276855 2.420873641967773 14.38471984863281 2.103313446044922 17.89580154418945 C 1.729635238647461 22.02716064453125 2.376432418823242 26.62986755371094 4.026142120361328 31.59410095214844 L 17.90583419799805 31.59410095214844 C 19.58451843261719 26.67630577087402 20.29977416992188 22.11160469055176 20.03281402587891 18.00796127319336 C 19.80529403686523 14.51074981689453 18.86453437805176 11.33176040649414 17.23664474487305 8.559289932250977 C 15.2353458404541 5.150859832763672 12.65197658538818 3.164346694946289 11.33364486694336 2.304798126220703 M 11.35824394226074 0 C 11.35824394226074 0 28.66676330566406 8.398521423339844 19.31618499755859 33.59410095214844 C 18.3805046081543 33.59410095214844 4.638143539428711 33.59410095214844 2.604513168334961 33.59410095214844 C -6.547115325927734 8.398521423339844 11.35824394226074 0 11.35824394226074 0 Z" stroke="none" />
                                </g>
                                <path class="cp-stroke" id="Path8" d="M2.939,5.877A2.939,2.939,0,1,1,5.877,2.939,2.938,2.938,0,0,1,2.939,5.877Z" transform="translate(15.155 11.267)" fill="#fff" stroke-width="2" />
                            </g>
                        </svg>
                        <asp:Button ID="btnExcellence" runat="server" Text="Excellence Awards" Width="135px" OnClick="btnExcellence_Click" />
                    </li>
                  <li class="nav-item disabled_area">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="46" viewBox="0 0 32 46">
                            <g id="G15" data-name="Group 3584" transform="translate(0 0)">
                                <g id="G16" data-name="Path 7472" transform="translate(7 16)" fill="#fff">
                                    <path d="M 17 28.36334991455078 L 9.456318855285645 24.49480056762695 L 8.999999046325684 24.26078987121582 L 8.543679237365723 24.49480056762695 L 0.9999990463256836 28.36334991455078 L 0.9999990463256836 1 L 17 1 L 17 28.36334991455078 Z" stroke="none" />
                                    <path class="custom-path" d="M 1.999999046325684 2 L 1.999999046325684 26.72671127319336 L 8.087368965148926 23.60498046875 L 8.999999046325684 23.13695907592773 L 9.912629127502441 23.60498046875 L 15.99999904632568 26.72671127319336 L 15.99999904632568 2 L 1.999999046325684 2 M -1.9073486328125e-06 0 L 18 0 L 18 30 L 8.999999046325684 25.38460922241211 L -1.9073486328125e-06 30 L -1.9073486328125e-06 0 Z" stroke="none" />
                                </g>
                                <g id="G17" data-name="Group 3583">
                                    <g id="G18" transform="translate(-9.318 -9.318)" fill="#fff">
                                        <path d="M 25.31760597229004 40.3176155090332 C 21.30929565429688 40.3176155090332 17.54201507568359 38.75780487060547 14.70972537994385 35.92550659179688 C 11.87742519378662 33.09321594238281 10.3176155090332 29.3259162902832 10.3176155090332 25.31760597229004 C 10.3176155090332 21.30929565429688 11.87742519378662 17.54201507568359 14.70972537994385 14.70972537994385 C 17.54201507568359 11.87742519378662 21.30929565429688 10.3176155090332 25.31760597229004 10.3176155090332 C 29.3259162902832 10.3176155090332 33.09321594238281 11.87742519378662 35.92550659179688 14.70972537994385 C 38.75780487060547 17.54201507568359 40.3176155090332 21.30929565429688 40.3176155090332 25.31760597229004 C 40.3176155090332 29.32592582702637 38.75780487060547 33.09321594238281 35.92550659179688 35.92550659179688 C 33.09321594238281 38.75780487060547 29.32592582702637 40.3176155090332 25.31760597229004 40.3176155090332 Z" stroke="none" />
                                        <path class="custom-path" d="M 25.31760597229004 39.3176155090332 C 29.05881500244141 39.3176155090332 32.57498550415039 37.86181640625 35.21840667724609 35.21840667724609 C 37.86181640625 32.57498550415039 39.3176155090332 29.05881500244141 39.3176155090332 25.31760597229004 C 39.3176155090332 21.5764045715332 37.86181640625 18.06024551391602 35.21840667724609 15.41682529449463 C 32.57498550415039 12.77341556549072 29.05881500244141 11.3176155090332 25.31760597229004 11.3176155090332 C 21.5764045715332 11.3176155090332 18.06024551391602 12.77341556549072 15.41682529449463 15.41682529449463 C 12.77341556549072 18.06024551391602 11.3176155090332 21.5764045715332 11.3176155090332 25.31760597229004 C 11.3176155090332 29.05881500244141 12.77341556549072 32.57498550415039 15.41682529449463 35.21840667724609 C 18.06024551391602 37.86181640625 21.5764045715332 39.3176155090332 25.31760597229004 39.3176155090332 M 25.31760597229004 41.3176155090332 C 16.47761535644531 41.3176155090332 9.317615509033203 34.15761566162109 9.317615509033203 25.31760597229004 C 9.317615509033203 16.47761535644531 16.47761535644531 9.317615509033203 25.31760597229004 9.317615509033203 C 34.15761566162109 9.317615509033203 41.3176155090332 16.47761535644531 41.3176155090332 25.31760597229004 C 41.3176155090332 34.15761566162109 34.15761566162109 41.3176155090332 25.31760597229004 41.3176155090332 Z" stroke="none" />
                                    </g>
                                    <path class="cp-stroke" id="Path1" data-name="ic_radio_button_unchecked_24px" d="M18.818,28.318a9.5,9.5,0,1,1,9.5-9.5A9.5,9.5,0,0,1,18.818,28.318Z" transform="translate(-2.818 -2.818)" fill="#fff" stroke-width="2" />
                                    <path class="custom-path cp2" id="Path2" d="M11.474,5.429,8.068,5.136,6.737,2,5.406,5.141,2,5.429,4.586,7.67,3.809,11,6.737,9.233,9.664,11,8.892,7.67Z" transform="translate(9.5 9.5)" stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                </g>
                            </g>
                        </svg>
                        <asp:Button ID="btnQuarterly" runat="server" style="" Text="Quarterly Divisional Excellence Award" Width="300px"   OnClick="btnQuarterly_Click"/>
                    </li>

                    <li class="nav-item">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="46" viewBox="0 0 32 46">
                            <g id="G7" data-name="Group 3584" transform="translate(0 0)">
                                <g id="G8" data-name="Path 7472" transform="translate(7 16)" fill="#fff">
                                    <path d="M 17 28.36334991455078 L 9.456318855285645 24.49480056762695 L 8.999999046325684 24.26078987121582 L 8.543679237365723 24.49480056762695 L 0.9999990463256836 28.36334991455078 L 0.9999990463256836 1 L 17 1 L 17 28.36334991455078 Z" stroke="none" />
                                    <path class="custom-path" d="M 1.999999046325684 2 L 1.999999046325684 26.72671127319336 L 8.087368965148926 23.60498046875 L 8.999999046325684 23.13695907592773 L 9.912629127502441 23.60498046875 L 15.99999904632568 26.72671127319336 L 15.99999904632568 2 L 1.999999046325684 2 M -1.9073486328125e-06 0 L 18 0 L 18 30 L 8.999999046325684 25.38460922241211 L -1.9073486328125e-06 30 L -1.9073486328125e-06 0 Z" stroke="none" />
                                </g>
                                <g id="G9" data-name="Group 3583">
                                    <g id="G10" transform="translate(-9.318 -9.318)" fill="#fff">
                                        <path d="M 25.31760597229004 40.3176155090332 C 21.30929565429688 40.3176155090332 17.54201507568359 38.75780487060547 14.70972537994385 35.92550659179688 C 11.87742519378662 33.09321594238281 10.3176155090332 29.3259162902832 10.3176155090332 25.31760597229004 C 10.3176155090332 21.30929565429688 11.87742519378662 17.54201507568359 14.70972537994385 14.70972537994385 C 17.54201507568359 11.87742519378662 21.30929565429688 10.3176155090332 25.31760597229004 10.3176155090332 C 29.3259162902832 10.3176155090332 33.09321594238281 11.87742519378662 35.92550659179688 14.70972537994385 C 38.75780487060547 17.54201507568359 40.3176155090332 21.30929565429688 40.3176155090332 25.31760597229004 C 40.3176155090332 29.32592582702637 38.75780487060547 33.09321594238281 35.92550659179688 35.92550659179688 C 33.09321594238281 38.75780487060547 29.32592582702637 40.3176155090332 25.31760597229004 40.3176155090332 Z" stroke="none" />
                                        <path class="custom-path" d="M 25.31760597229004 39.3176155090332 C 29.05881500244141 39.3176155090332 32.57498550415039 37.86181640625 35.21840667724609 35.21840667724609 C 37.86181640625 32.57498550415039 39.3176155090332 29.05881500244141 39.3176155090332 25.31760597229004 C 39.3176155090332 21.5764045715332 37.86181640625 18.06024551391602 35.21840667724609 15.41682529449463 C 32.57498550415039 12.77341556549072 29.05881500244141 11.3176155090332 25.31760597229004 11.3176155090332 C 21.5764045715332 11.3176155090332 18.06024551391602 12.77341556549072 15.41682529449463 15.41682529449463 C 12.77341556549072 18.06024551391602 11.3176155090332 21.5764045715332 11.3176155090332 25.31760597229004 C 11.3176155090332 29.05881500244141 12.77341556549072 32.57498550415039 15.41682529449463 35.21840667724609 C 18.06024551391602 37.86181640625 21.5764045715332 39.3176155090332 25.31760597229004 39.3176155090332 M 25.31760597229004 41.3176155090332 C 16.47761535644531 41.3176155090332 9.317615509033203 34.15761566162109 9.317615509033203 25.31760597229004 C 9.317615509033203 16.47761535644531 16.47761535644531 9.317615509033203 25.31760597229004 9.317615509033203 C 34.15761566162109 9.317615509033203 41.3176155090332 16.47761535644531 41.3176155090332 25.31760597229004 C 41.3176155090332 34.15761566162109 34.15761566162109 41.3176155090332 25.31760597229004 41.3176155090332 Z" stroke="none" />
                                    </g>
                                    <path class="cp-stroke" id="Path5" data-name="ic_radio_button_unchecked_24px" d="M18.818,28.318a9.5,9.5,0,1,1,9.5-9.5A9.5,9.5,0,0,1,18.818,28.318Z" transform="translate(-2.818 -2.818)" fill="#fff" stroke-width="2" />
                                    <path class="custom-path cp2" id="Path6" d="M11.474,5.429,8.068,5.136,6.737,2,5.406,5.141,2,5.429,4.586,7.67,3.809,11,6.737,9.233,9.664,11,8.892,7.67Z" transform="translate(9.5 9.5)" stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                </g>
                            </g>
                        </svg>

                        <asp:Button ID="btnEcard" runat="server" Text="E-Card" Width="135px" OnClick="btnEcard_Click" />
                    </li>
                </ul>
            </div>

            <div class="rnr-tab-pane tab-content" id="tblpage" runat="server" >
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <div class="container">
                        <div class="row rnr-custom-row">
                            <div class="col-md-6" style="display: none;">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Select Award</label>
                                    <asp:DropDownList ID="ddlAwardType" runat="server" Width="" TabIndex="1" OnSelectedIndexChanged="ddlAwardType_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd hide" >
                                    <label for="">Suggestions (Optional)</label>
                                    <asp:DropDownList ID="ddlPillars" runat="server" Width="200px" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlPillars_SelectedIndexChanged">
                                    </asp:DropDownList>

                                </div>
                                <div class="nominee-name">
                                    <label for="" class="nominee-lbl">Award For</label>
                                    <div class="rnr-radio-btn award_for_btn">
                                        <div class="radio direct-reportee">
                                            <asp:RadioButtonList ID="rbAwardedFor" runat="server" AutoPostBack="true" >
                                                <asp:ListItem Text="Effort" data-value="Effort" Selected="True" ></asp:ListItem>
                                                <asp:ListItem Text="Outcome" data-value="Outcome"></asp:ListItem>
                                                <asp:ListItem Text="Behaviour" data-value="Behaviour"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            
                                        <%--<asp:Button ID="btnBehaviourdisplay" runat="server" cssClass="info_input" Text="i" OnClick="btnBehaviourdisplay_Click" />--%>

                                            </div>
                                        </div>
                                    </div>
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Select Amount</label>
                                    <asp:DropDownList ID="ddlGifts" runat="server" Width="" TabIndex="2" class="awardamt" OnSelectedIndexChanged="ddlGifts_SelectedIndexChanged" AutoPostBack="true"
                                        Style="">
                                    </asp:DropDownList>
                                    <telerik:RadDatePicker ID="Date" DateInput-Culture="" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                                        TabIndex="1" Style="display: none;" OnSelectedDateChanged="Date_SelectedDateChanged"
                                        AutoPostBack="true" class="Datepick">
                                        <Calendar ID="Calendar1" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                            ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput ID="txtDate" runat="server" DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy"
                                            TabIndex="1">
                                        </DateInput>
                                        <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="1"></DatePopupButton>
                                    </telerik:RadDatePicker>
                                    <span id="spnTime" style="display: none;" runat="server"></span>
                                    <telerik:RadTimePicker ID="FromTime" runat="server" Style="display: none;" class="Timepick">
                                    </telerik:RadTimePicker>
                                    <div class="datetimemsg">Please type date in 'dd/mm/yyyyy' format e.g. 29/04/2019 and time in 'hh:mm' format e.g. 07:00</div>

                                </div>
                                <div class="rnr-input certificate-i">
                                    <label class="certificate-label" for="">Text On Certificate</label>
                                    <label class="min-char" for=""><span id="spnTxtCount">Char (150/150)</span></label>
                                    <asp:TextBox ID="txtEmpReason" runat="server" TextMode="MultiLine" Rows="4" TabIndex="5" MaxLength="200"  onkeypress="return blockSpecialChar(event)" ></asp:TextBox><%--onPaste="return blockSpecialChar(event)"--%>
                                    <%--<asp:CustomValidator ID="cvTest" runat="server" ControlToValidate="txtEmpReason" ClientValidationFunction="ValidateTextLength" ErrorMessage="Text is too long" />--%>
                                                                     
                                </div>
                                <br />
                                <div class="rnr-input search-emp-m">
                                    <div class="search-emp" data-toggle="modal" data-target="#ContentPlaceHolder1_searchempmodal">
                                        <label for="">Search Employee for CC</label>
                                        <asp:TextBox ID="txtMailID" runat="server" Rows="4" Enabled="false" ></asp:TextBox>
                                        <svg id="Svg1" xmlns="http://www.w3.org/2000/svg" runat="server" viewBox="0 0 363.8 363.8">
                                            <title>Search</title>
                                            <g id="Layer_2" data-name="Layer 2">
                                                <g id="Layer_1-2" data-name="Layer 1">
                                                    <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <%--<div class="col-md-6">
                            </div>--%>
                            <div class="col-md-6">
                                <div class="nominee-name">
                                    <label for="" class="nominee-lbl">Nominee Details</label>
                                    <div class="rnr-radio-btn">
                                        <div class="radio direct-reportee">
                                            <asp:RadioButtonList ID="SelectNomineeName" runat="server" OnSelectedIndexChanged="SelectNomineeName_SelectedIndexChanged" AutoPostBack="true">

                                                <asp:ListItem Text="Direct Reportees" Selected="True" data-value="Direct Reportees"></asp:ListItem>
                                                <asp:ListItem Text="Other Recipents" data-value="Other Recipents"></asp:ListItem>

                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div id="sel_drp" class="mahindra_drop_down_div rnr-dd  hide-box-1" runat="server">
                                        <label for="">Select Nominee</label>
                                        <asp:DropDownList ID="ddlHodReportEmployee" runat="server" Width="200px" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlHodReportEmployee_SelectedIndexChanged">
                                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div id="sel_nom" class="rnr-input search-emp-m" runat="server" style="display: none;">
                                        <div class="search-emp" data-toggle="modal" data-target="#ContentPlaceHolder1_searchnomineemodal">
                                            <label for="">Enter Name / Token Number</label>

                                            <asp:TextBox ID="txtOtherReportee" runat="server" Rows="4" 
                                                Enabled="false" ></asp:TextBox>

                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.8 363.8">
                                                <title>Search</title>
                                                <g id="G1" data-name="Layer 2">
                                                    <g id="G2" data-name="Layer 1">
                                                        <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="nominee-box">
                                        <div class="nom-box-top">
                                            <h5>
                                                <span>
                                                    <asp:Label ID="lblfirstname" runat="server" ></asp:Label>
                                                    <asp:HiddenField ID="hdnLoactionCode" runat="server" />
                                                    <%--Added for web service by swapneel--%>
                                                    <asp:HiddenField ID="hdnEmailId" runat="server" />
                                                </span>
                                                <span>
                                                    <asp:HiddenField ID="hdnEmpDepartmentName" runat="server" />
                                                    <asp:Label ID="lblLastName" runat="server" Text=""></asp:Label>
                                                </span>
                                            </h5>
                                            <%--<a href="#">Change</a>--%>
                                        </div>
                                        <div class="nom-box-main">
                                            <label for="" class="nom-box-lbl">
                                                <asp:HiddenField ID="hdnEmpDesignation" runat="server" />
                                                <asp:Label ID="lblEmpLevel" runat="server" ></asp:Label>
                                            </label>
                                            <ul>
                                                <li>
                                                    <span class="sp1">
                                                        <asp:HiddenField ID="hdnEmpName" runat="server" />
                                                        <asp:Label ID="lblTokenno" runat="server"></asp:Label>
                                                    </span>
                                                    <label for="" class="nom-detail-lbl">Token Number</label>
                                                </li>
                                                <li>
                                                    <span class="sp1">
                                                        <asp:HiddenField ID="hdnEmpLocation" runat="server" />
                                                        <asp:Label ID="lblEmpDivision" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="HdnDivisionCode" runat="server" />
                                                    </span>
                                                    <label for="" class="nom-detail-lbl">Department</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                            </div>
                            

                        </div>
                                                        <div class="col-md-12">

                                <div class="send-awards">
                                        <button type="button" name="button" id="btn_SendAward"  data-toggle="modal" data-target="#award-preview-popup" class="rnr-btn" >Send Award</button>
                                    
                                </div>

                            </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <table>
        <tr>
            <td colspan="4">
                <asp:Label CssClass="lblName" runat="server" ID="lblGetEmpResponse" ForeColor="#e31837"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="lblName">&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td colspan="2">
                <asp:Label ID="lblGiftHamper" runat="server" Text="Note: Hamper to be selected upto Rs.1000 Only"
                    Font-Bold="true" Visible="false"></asp:Label>
                <asp:Label runat="server" ID="lblAwardType" ForeColor="#e31837"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenBalance" runat="server" />
    <!--Done by Kiran on 02 july 2013-->
    <div style="" class="popupGreyBox" id="ptpgrey" runat="server">
    </div>
    <div style="" class="registerPopup" id="popupContainer" runat="server">
        <div class="popHead">
            <div class="hTitle" style="float: left;">
                Preview for Employee
            </div>
            <div class="closePop">
                <span></span>
            </div>
        </div>
        <div class="popList" style="height: 638px; overflow-y: auto;">
            <asp:Literal ID="ltPMessage" runat="server"></asp:Literal>
        </div>
    </div>

    <!-- search emp modal start-->
    <div class="modal fade search-emp-modal" id="searchempmodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Search Employee</h5>
                </div>
                <div class="modal-body">
                    <div class="rnr-search-bar">
                        <div class="search-form form">
                            <label for="" class="search-top-lbl">Refine Search</label>
                            <label>
                                <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2" onkeypress="return blockSpecialChar(event)"></asp:TextBox>
                                <div class="btnRed SearchButtons">
                                    <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text=""
                                        OnClick="btnGetDetails_Click" TabIndex="3" OnClientClick="return ValidateEmp()" />
                                </div>
                            </label>

                        </div>
                    </div>

                    <div class="rnr-table">
                        <div id="table-scroll" class="table-scroll">
                            <div id="divEmailList" runat="server" class="grid01">
                                <asp:GridView ID="grdEmpList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                    OnRowCommand="grdEmpList_RowCommand" Visible="true"
                                    AllowPaging="true" OnPageIndexChanging="grdEmpList_PageIndexChanging" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                                    <Columns>
                                        <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                        <asp:BoundField DataField="EmailID" HeaderText="Email Id" SortExpression="EmailID" />
                                        <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                        <asp:BoundField DataField="DivisionName_PA" HeaderText="Division Name" SortExpression="LocationName_PSA" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select" ToolTip="Select Employee for adding CC"
                                                    runat="server" CssClass="selLink1" TabIndex="4">
                                                Select</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="dataPager" />
                                    <%--<SelectedRowStyle CssClass="selectedoddTd" />--%>
                                </asp:GridView>
                            </div>



                        </div>
                        <%--<p class="result-p">4 Results Found</p>--%>
                    </div>
                </div>
                <div class="modal-footer">
<%--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ContentPlaceHolder1_searchempmodal" onclick="return closeSearchPopup(this);">
                        <span aria-hidden="true">Cancel</span>
                    </button>--%>
                <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" CssClass="rnr-btn" />

                </div>
            </div>
        </div>
    </div>
    <!-- search-emp-modal end -->

    <!-- search nominee modal start-->
    <div class="modal fade search-emp-modal" id="searchnomineemodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Search Nominee</h5>
                </div>
                <div class="modal-body">
                    <div class="rnr-search-bar">
                        <div class="search-form form">
                            <label for="" class="search-top-lbl">Refine Search</label>
                            <label>
                                <asp:TextBox ID="txtTokenNumberTop" runat="server" MaxLength="100" TabIndex="2" Rows="4" onkeypress="return blockSpecialChar(event)"></asp:TextBox>
                                <div class="btnRed SearchButtons">
                                    <asp:Button ID="btnGetEmpDetails" CssClass="btnLeft" runat="server" Text=""
                                        OnClick="btnGetEmpDetails_Click" OnClientClick="return ValidatePage()" TabIndex="3" />
                                </div>
                            </label>
                        </div>
                    </div>
                    <div class="rnr-table">
                        <div id="Div1" class="table-scroll">
                            <div id="divEmpList" runat="server" class="grid01">
                                <asp:GridView ID="grdRecipientList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                    OnRowCommand="grdRecipientList_RowCommand"
                                    AllowPaging="true" OnPageIndexChanging="grdRecipientList_PageIndexChanging" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                                    <Columns>
                                        <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                        <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                        <asp:BoundField DataField="DivisionName_PA" HeaderText="Division Name" SortExpression="LocationName_PSA" />
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select" ToolTip="Select Employee to Give Award"
                                                    runat="server" CssClass="selLink1" TabIndex="4">   <%--OnClientClick="ShowPopup();"--%>
                                            Select</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="dataPager" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="close" data-dismiss="modal" id="ContentPlaceHolder1_searchnomineemodal" aria-label="Close" onclick="return closeSearchPopup(this);">
                        <span aria-hidden="true">Cancel</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- search-nominee-modal end -->
    <!-- award preview popup modal start-->
    <div class="modal fade search-emp-modal" id="award-preview-popup" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Award Preview</h5>
                </div>
                <div class="modal-body">
                    <div class="nominee-box">
                        <div class="nom-box-top">
                            <h5>
                                <span>
                                    <asp:Label ID="lblfirstnamePreview" runat="server"></asp:Label></span>
                                <span>
                                    <asp:Label ID="lblLastNamePreview" runat="server"></asp:Label></span>
                            </h5>
                            <div class="award-pop-right">

                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="46" viewBox="0 0 32 46">
                                    <g id="G3" data-name="Group 3584" transform="translate(0 0)">
                                        <g id="G4" data-name="Path 7472" transform="translate(7 16)" fill="#fff">
                                            <path d="M 17 28.36334991455078 L 9.456318855285645 24.49480056762695 L 8.999999046325684 24.26078987121582 L 8.543679237365723 24.49480056762695 L 0.9999990463256836 28.36334991455078 L 0.9999990463256836 1 L 17 1 L 17 28.36334991455078 Z" stroke="none" />
                                            <path d="M 1.999999046325684 2 L 1.999999046325684 26.72671127319336 L 8.087368965148926 23.60498046875 L 8.999999046325684 23.13695907592773 L 9.912629127502441 23.60498046875 L 15.99999904632568 26.72671127319336 L 15.99999904632568 2 L 1.999999046325684 2 M -1.9073486328125e-06 0 L 18 0 L 18 30 L 8.999999046325684 25.38460922241211 L -1.9073486328125e-06 30 L -1.9073486328125e-06 0 Z" stroke="none" fill="#30ce8d" />
                                        </g>
                                        <g id="G5" data-name="Group 3583">
                                            <g id="G6" transform="translate(-9.318 -9.318)" fill="#fff">
                                                <path d="M 25.31760597229004 40.3176155090332 C 21.30929565429688 40.3176155090332 17.54201507568359 38.75780487060547 14.70972537994385 35.92550659179688 C 11.87742519378662 33.09321594238281 10.3176155090332 29.3259162902832 10.3176155090332 25.31760597229004 C 10.3176155090332 21.30929565429688 11.87742519378662 17.54201507568359 14.70972537994385 14.70972537994385 C 17.54201507568359 11.87742519378662 21.30929565429688 10.3176155090332 25.31760597229004 10.3176155090332 C 29.3259162902832 10.3176155090332 33.09321594238281 11.87742519378662 35.92550659179688 14.70972537994385 C 38.75780487060547 17.54201507568359 40.3176155090332 21.30929565429688 40.3176155090332 25.31760597229004 C 40.3176155090332 29.32592582702637 38.75780487060547 33.09321594238281 35.92550659179688 35.92550659179688 C 33.09321594238281 38.75780487060547 29.32592582702637 40.3176155090332 25.31760597229004 40.3176155090332 Z" stroke="none" />
                                                <path d="M 25.31760597229004 39.3176155090332 C 29.05881500244141 39.3176155090332 32.57498550415039 37.86181640625 35.21840667724609 35.21840667724609 C 37.86181640625 32.57498550415039 39.3176155090332 29.05881500244141 39.3176155090332 25.31760597229004 C 39.3176155090332 21.5764045715332 37.86181640625 18.06024551391602 35.21840667724609 15.41682529449463 C 32.57498550415039 12.77341556549072 29.05881500244141 11.3176155090332 25.31760597229004 11.3176155090332 C 21.5764045715332 11.3176155090332 18.06024551391602 12.77341556549072 15.41682529449463 15.41682529449463 C 12.77341556549072 18.06024551391602 11.3176155090332 21.5764045715332 11.3176155090332 25.31760597229004 C 11.3176155090332 29.05881500244141 12.77341556549072 32.57498550415039 15.41682529449463 35.21840667724609 C 18.06024551391602 37.86181640625 21.5764045715332 39.3176155090332 25.31760597229004 39.3176155090332 M 25.31760597229004 41.3176155090332 C 16.47761535644531 41.3176155090332 9.317615509033203 34.15761566162109 9.317615509033203 25.31760597229004 C 9.317615509033203 16.47761535644531 16.47761535644531 9.317615509033203 25.31760597229004 9.317615509033203 C 34.15761566162109 9.317615509033203 41.3176155090332 16.47761535644531 41.3176155090332 25.31760597229004 C 41.3176155090332 34.15761566162109 34.15761566162109 41.3176155090332 25.31760597229004 41.3176155090332 Z" stroke="none" fill="#30ce8d" />
                                            </g>
                                            <path id="Path3" data-name="ic_radio_button_unchecked_24px" d="M18.818,28.318a9.5,9.5,0,1,1,9.5-9.5A9.5,9.5,0,0,1,18.818,28.318Z" transform="translate(-2.818 -2.818)" fill="#fff" stroke="#30ce8d" stroke-width="2" />
                                            <path id="Path4" d="M11.474,5.429,8.068,5.136,6.737,2,5.406,5.141,2,5.429,4.586,7.67,3.809,11,6.737,9.233,9.664,11,8.892,7.67Z" transform="translate(9.5 9.5)" fill="#30ce8d" stroke="#30ce8d" stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                        </g>
                                    </g>
                                </svg>

                                <div class="tb-text">
                                    <span class="t1">
                                        <asp:Label ID="lblAwardNamePreview" runat="server"></asp:Label></span>
                                    <span class="t2">
                                        <asp:Label ID="lblAwardAmountPreview" runat="server"></asp:Label></span>
                                </div>

                            </div>
                        </div>
                        <div class="nom-box-main">
                            <label for="" class="nom-box-lbl">
                                <asp:Label ID="lblEmpLevelPreview" runat="server"></asp:Label></label>
                            <ul>
                                <li>
                                    <span class="sp1">
                                        <asp:Label ID="lblTokennoPreview" runat="server"></asp:Label></span>
                                    <label for="" class="nom-detail-lbl">Token Number</label>
                                </li>
                                <li>
                                    <span class="sp1">
                                        <asp:Label ID="lblEmpDivisionPreview" runat="server"></asp:Label>
                                    </span>
                                    <label for="" class="nom-detail-lbl">Department</label>
                                </li>
                                <%--<li>
                                    <span class="sp1">
                                        <asp:Label ID="lblDepartmentPreview" runat="server"></asp:Label></span>
                                    <label for="" class="nom-detail-lbl">Department</label>
                                </li>
                                <li>
                                    <span class="sp1">
                                        <asp:Label ID="lblBusinessUnitPreview" runat="server"></asp:Label></span>
                                    <label for="" class="nom-detail-lbl">Business Unit</label>
                                </li>--%>
                            </ul>
                            <p>
                                <asp:TextBox ID="txtReasonPreview" runat="server" ReadOnly="true" TextMode="MultiLine" Rows="5"></asp:TextBox>
                            </p>
                            <span class="reason-span">Text For Certificate </span>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">Cancel</span>
                    </button>
                    <asp:Button ID="btnSubmit" runat="server" CssClass="rnr-btn" Text="Submit" OnClick="btnSubmit_Click"
                        OnClientClick="return ValidateOnSubmit()" TabIndex="6" />
                    <%--                    <asp:Button ID="btnReset" runat="server" CssClass="rnr-btn rnr-btn-reset" Text="Reset" OnClick="btnReset_Click"
                        TabIndex="7" />--%>
                    <asp:HiddenField ID="hdnRecipientTokenNumber" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <!-- award preview popup modal end -->
    <div class="modal fade search-emp-modal" runat="server" id="SuccessModal" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Success</h5>
                </div>
                <div class="modal-body">
                    <div class="">
                        <p>
                            <h4>Award Submitted Successfully  !!! </h4>

                            <br />
                            Dear&nbsp;<asp:Label ID="Hodlbl" runat="server"></asp:Label>,
                        <br />
                               We would like to inform you that the award email notification 
                         sent to employee immediately and you cannot reverse this award. 
<%--                            We would like to inform you that the award processing takes 48 hours & 
                        there is a provision for reversing the award within 48 hours 
                        in case you have awarded it by mistake. --%>
<%--                       <br />
                            <asp:Label ID="lblFirst" runat="server">&nbsp; </asp:Label>
                            <asp:Label ID="lbllast" runat="server"> </asp:Label>,&nbsp;
                            will receive the award via mailer and R&R portal after 48 hours.--%>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">Cancel</span>
                    </button>--%>
                    <asp:Button ID="Button1" runat="server" OnClick="closeemailpopup_Click" Text="Done" CssClass="rnr-btn" />
                    <%--                    <asp:Button ID="btnReset" runat="server" CssClass="rnr-btn rnr-btn-reset" Text="Reset" OnClick="btnReset_Click"
                        TabIndex="7" />--%>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </div>
            </div>
        </div>
    </div>


    <!--Done by Kiran on 02 july 2013-->
    <script type="text/javascript">
        //document.onload = hide();

        function hide() {
            //debugger
            if (document.getElementById) {
                nav1 = document.getElementById("div1");
                nav2 = document.getElementById("div2");
                //                nav1 = document.getElementsByTagName('div');
                //                nav2 = document.div2.style;
                nav1.style.display = 'block';
                nav2.style.display = 'none';
            }
        }



    </script>

    <script>
        $('#ContentPlaceHolder1_txtEmpReason').keyup(function (e) {
            var tval = $('#ContentPlaceHolder1_txtEmpReason').val();
            tlength = tval.length;
            set = 150;
            remain = parseInt(set - tlength);
            var value = 0;

            if (tlength > set) {
                $('#ContentPlaceHolder1_txtEmpReason').val((tval).substring(0, tlength - 1));
                value = '(' + set + '/150)';
                $('#spnTxtCount').text(value);
            }
            else {
                var value = '(' + remain + '/150)';
                $('#spnTxtCount').text(value);
            }

            $('#ContentPlaceHolder1_txtReasonPreview').val($('#ContentPlaceHolder1_txtEmpReason').val());

        });
        $('#btn_SendAward').click(function (e) {
            $('#ContentPlaceHolder1_txtReasonPreview').val($('#ContentPlaceHolder1_txtEmpReason').val());

        });
        $('#ContentPlaceHolder1_txtName').keypress(function (event) {
            if (event.keyCode === 10 || event.keyCode === 13) {
                event.preventDefault();
                alert("'Enter' key has been restricted, please click on Search button!");
            }
        });

        $('#ContentPlaceHolder1_txtTokenNumberTop').keypress(function (event) {
            if (event.keyCode === 10 || event.keyCode === 13) {
                event.preventDefault();
                alert("'Enter' key has been restricted, please click on Search button!");
            }
        });
        $('.rnr-radio-btn table tr td span label').text('');
    </script>
    <!--Old Window Start-->
    <div id='div2' style="display: none;">
        <h1>Your budget for F12 have lapsed. Budgets for F13 will be uploaded soon.
        </h1>
    </div>
    <div id='div3'>
        <div class="form01">
            <div class="formformob" style="position: relative;">
                <table>
                    <tr style="display: none;">
                        <td class="lblName">Name</td>
                        <td class="hidden-xs">:</td>
                        <td>
                            <asp:TextBox ID="txtHODName" runat="server" Enabled="False"></asp:TextBox>
                        </td>
                        <td class="lblName">Division&nbsp;
                        </td>
                        <td class="hidden-xs">:</td>
                        <td>
                            <asp:TextBox ID="txtHODProcess" runat="server" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td class="lblName">Location&nbsp;</td>
                        <td class="hidden-xs">:</td>
                        <td>
                            <asp:TextBox ID="txtHODLocation" runat="server" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td class="lblName" style="color: #e31837;" colspan="4">BUDGET ROLL UP</td>
                    </tr>
                    <tr style="display: none;">
                        <td class="lblName">Added</td>
                        <td class="hidden-xs">:</td>
                        <td>
                            <img alt="" src="../../Images/rupee_icon.jpg" runat="server" id="img3" />&nbsp
                            <asp:Label ID="lblAdded" runat="server"></asp:Label>
                        </td>

                        <td class="lblName">Deducted</td>
                        <td class="hidden-xs">:</td>
                        <td>
                            <img alt="" src="../../Images/rupee_icon.jpg" runat="server" id="img5" />&nbsp
                            <asp:Label ID="lblDeducted" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFromDate" AssociatedControlID="Date" runat="server" Text="Date" Style="display: none;"></asp:Label>

                            <div id="idtooltiptext" class="tooltiptext">
                                <div id="tiparea" class="tiparea">
                                    <ul class="textnav">
                                        <li>
                                            <a data-toggle="tab" href="#spotawarddata" title="Spot Award">
                                                <img src="../../Images/award.png" />
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#excelleratordata" title="Excellerator">
                                                <img src="../../Images/ribbon.png" />
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="spotawarddata" class="tab-pane fade" style="background-image: url(../../Images/awards.jpg);">
                                            <h4>Spot Award</h4>
                                            <p>
                                                To encourage instant recognition for the exemplary contribution of employees through demonstration of
                                                <br />
                                                traits (sample) such as:
  
                                            </p>
                                            <div style="text-align: center; float: left; width: 100%;">
                                                <ul class="infographics1">
                                                    <li>
                                                        <img class="imgicons" src="../../Images/spot1.png" />
                                                        Bright Ideas/ initiatives that are implemented successfully</li>
                                                    <li>
                                                        <img class="imgicons" src="../../Images/spot2.png" />
                                                        Going beyond the call of duty / exceeding performance expectations</li>
                                                    <li>
                                                        <img class="imgicons" src="../../Images/spot3.png" />
                                                        Making a difference in the business processes</li>
                                                    <li>
                                                        <img class="imgicons" src="../../Images/spot4.png" />
                                                        Acknowledging the efforts put in by the individuals</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div id="excelleratordata" class="tab-pane fade" style="background-image: url(../../Images/excellerator.jpg);">
                                            <h4>Excellerator</h4>
                                            <p>
                                                To recognize the desired behavior and committed efforts leading to high business impact through
                                                <br />
                                                demonstration of traits (sample) such as:
                                            </p>
                                            <div style="text-align: center; float: left; width: 100%;">
                                                <ul class="infographics1">
                                                    <li>
                                                        <img class="imgicons" src="../../Images/excellator1.png" />
                                                        Exemplary Customer Orientation </li>
                                                    <li>
                                                        <img class="imgicons" src="../../Images/excellator2.png" />
                                                        Innovation at Work </li>
                                                    <li>
                                                        <img class="imgicons" src="../../Images/excellator3.png" />
                                                        Sustainable Cost reduction initiative</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div id="coffeedata" class="tab-pane fade" style="background-image: url(../../Images/98687-OLDAZU-940.jpg);">
                                            <h4>Coffee With You</h4>

                                            <p>
                                                It is a personal interaction between a manager & his/her skip level subordinates. The employee gets to
                                                <br />
                                                spend a quality time with his/her skip level manager.
                                            </p>
                                            <br />
                                            <br />
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

    <!--Old Window End-->
    <div class="modal fade search-emp-modal" id="ImageInfo" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-header header-bg">
                        <button type="button" class="close close-icon" data-dismiss="modal">&times;</button>
                    </div>
                <div class="modal-content">
                   <%-- <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>--%>
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                    <div class="modal-body modal-img">
                        <img src="../../Images/RewardAndRecognition.png" alt="" />
                    </div>
                 </div>
            </div>
    </div>    
    <!-- Efforts popup start-->
    
    <div class="modal fade search-emp-modal" id="excellenceaward" runat="server" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header rnr-modal-head">
              <h5 class="modal-title">Effort Driven Decision Making </h5>
            </div>
            <div class="modal-body drivenpanel">
           
					
					<div class="col-sm-12">
					<div class="row">
					  <div class="col-sm-6">
					  
					  <div class="rnr-table">
                     
                      <div id="Div4" class="table-scroll">
                        <table id="Table1" class="main-table">
                        <thead>
                          <tr>
                            <th scope="col">Impact/ Level of Effort</th>
                            <th scope="col">Team Member  </th>
                            <th scope="col">Key Contributor</th>
                            <th scope="col">Team Lead</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Own Role </td>
                            <td>1000</td>
                            <td>1000</td>
                            <td>1000</td>
                            
                          </tr>
						  
						  <tr>
                            <td>Own Team </td>
                            <td>1000/2000</td>
                            <td>1000/2000</td>
                            <td>2000</td>
                            
                          </tr>
						  
						  <tr>
                            <td>Business </td>
                            <td>2000</td>
                            <td>2000</td>
                            <td>5000</td>
                            
                          </tr>
						 
                       
                        </tbody>
                      </table>
                      </div>
                    </div></div>
					  <div class="col-sm-6">
					  <div class="rightcontent">
					  <h3>What was the impact of the employee(s) contribution</h3>
					  <ul>
					  <li><strong>Own Role:</strong> The impact was observed in an employee's role outcomes</li>
					  <li><strong>Own Team:</strong> The impact was observed in a team's outcomes </li>
					  <li><strong>Business:</strong> The impact was observed in the Function or Business outcomes</li>
					  </ul>
					  
					  <h3>What was the level of effort made by the employee(s)</h3>
					  <ul>
					  <li><strong>Team Member:</strong> Served as a team member, active participant in meetings/decision making, contribution/recommendations/solutions followed up to achieve results</li>
					  <li><strong>Key Contributor:</strong> Acted as a key contributor, partnered with stakeholders, escalated issues, led Meetings, proposed recommendations, solutions, participated in implementing changes </li>
					  <li><strong>Team Lead:</strong> acted as team lead, drove decisions, followed through the execution/implementation, shared learning's with others</li>
					  </ul>
					  </div>
					  </div>
					  </div>
					  </div>

           
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnCLoseEffortModal" runat="server" class="add-emp rnr-btn" Text="Close" />
<%--              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">Cancel</span>
              </button>
              <button type="button" name="button" class="add-emp rnr-btn">Save</button>--%>
            </div>
          </div>
        </div>
      </div>
    <!--Efforts popup end-->
    <!-- Bevaviour popup start-->
    <div class="modal fade search-emp-modal" id="behaviourdecision" runat="server" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header rnr-modal-head">
              <h5 class="modal-title">Behaviour Driven Decision Making </h5>
            </div>
            <div class="modal-body drivenpanel">
               
					
					<div class="col-sm-12">
					<div class="row">
					  <div class="col-sm-6">
					  
					  <div class="rnr-table">
                     
                      <div id="Div5" class="table-scroll">
                        <table id="main-table" class="main-table">
                        <thead>
                          <tr>
                            <th scope="col">Impact</th>
                            <th scope="col">Amount</th>
                            
                            
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Own Role </td>
                            <td>1000</td>
                            
                          </tr>
						  
						  <tr>
                            <td>Own Team </td>
                            <td>2000</td>
                            
                          </tr>
						  
						  <tr>
                            <td>Business </td>
                            <td>5000</td>
                            
                          </tr>
						 
                       
                        </tbody>
                      </table>
                      </div>
                    </div></div>
					  <div class="col-sm-6">
					  <div class="rightcontent">
					  <h3>What was behaviour displayed by employee?</h3>
					  <ul>
					  <li>The Behaviour displayed by the employee (aglity, Bold, Colaboration or any other (specify)) had an impact on any of the following:</li>
					  <li><strong>Own Role:</strong> The impact was observed in an employee's role outcomes </li>
					  <li><strong>Own Team:</strong> The impact was observed in a team's outcomes </li>
					  <li><strong>Business:</strong> The impact was observed in the Function or Business outcomes</li>
					  </ul>
					  
					  
					  </div>
					  </div>
					  </div>
					  </div>

           
            </div>
            <div class="modal-footer">
<%--              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">Cancel</span>
              </button>--%>
                <asp:Button ID="btnBehaviourModalClose" runat="server" Text="Close" class="add-emp rnr-btn" />
<%--              <button type="button" id="btnBehaviourModalClose" runat="server" name="button" class="add-emp rnr-btn" onclick="" >Close</button>--%>
            </div>
          </div>
        </div>
      </div>
    <!--Bevaviour popup end-->
</asp:Content>

﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Rpt_ExportToExcel.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.Rpt_ExportToExcel" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <title></title>
    <link href="/includes/css/master.css" rel="stylesheet" type="text/css" />
    
 </head>
<body style= "background-color:White;">
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div class="reportHold1" runat="server" id="rpthold">
    <rsweb:ReportViewer ID="rptExportToExcel" runat="server" InteractiveDeviceInfos="(Collection)" width="95%">
        <LocalReport ReportPath="Web_Pages\Reports\ExportToExcel.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
</div>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetCloserDataForExcel" 
        TypeName="BL_Rewards.Admin.BL_RecipientClosure"></asp:ObjectDataSource>
        

</form>
</body>
</html>

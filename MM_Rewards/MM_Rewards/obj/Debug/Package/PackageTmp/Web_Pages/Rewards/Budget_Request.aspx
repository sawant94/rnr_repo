﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Budget_Request.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.Budget_Request" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function validatebtn() {
            var errMsg = '';
            var btnGET = document.getElementById('<%=txtSearchBy.ClientID %>');
            errMsg += ValidateName(btnGET.value, "Token Number / Name");
            errMsg += CheckText_Blank(btnGET.value, 'Token Number / Name')

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
     function validateSave() {

         var r = confirm("Do you want to submit record ?");
         //alert(r);
         if (r != true) {
             return false;
         }


            var errMsg = '';
            var txtTokenNumber = document.getElementById('<%=txtTokenNumber.ClientID %>');
            var txtRequestAmount = document.getElementById('<%=txtRequestAmount.ClientID %>');
            var txtReasons = document.getElementById('<%=txtReasons.ClientID %>');

            errMsg += ValidateName(txtTokenNumber.value, "Token Number / Name");
            errMsg += CheckText_Blank(txtTokenNumber.value, 'Token Number / Name')

            errMsg += ValidateDecimal(txtRequestAmount.value, "Request Amount");
            errMsg += CheckText_Blank(txtRequestAmount.value, 'Request Amount')

            errMsg += ValidateName(txtReasons.value, "Reasons");
            errMsg += CheckText_Blank(txtReasons.value, 'Reasons')

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
        function validate(sender, key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes
           // alert('hello')
            //alert(keycode)
            if ((keycode >= 48 && keycode <= 57)
                 || (keycode >= 96 && keycode <= 105)
                 || keycode == 110
                 || (keycode >= 35 && keycode <= 40)  // 35 end,36 home,37 to 40 are arrow keys
                 || keycode == 8 // backspace
                 || keycode == 9  // tab
                 || keycode == 46  // delete
                 || keycode == 190
                || keycode == 110
                ) {
                return true;
            }
            else {
                sender.value = "";
                return false;
            }
        }
        </script>
 <h1>Budget Request</h1>
    <div class="form01">
        <table>
            <tr> <!--style="display: none;"-->
                <td class="lblName">
                    IsCE / HOD &nbsp<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td class="showRadio">
                    <asp:RadioButtonList ID="rblISCEHOD" runat="server" RepeatDirection="Horizontal"
                        Width="200px" CssClass="chkBox">
                        <asp:ListItem Text="CE" Selected="True" Value="C" />
                        <asp:ListItem Text="HOD" Value="H" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Enter Name/Token No &nbsp<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtSearchBy" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                    &nbsp;
                    <div class="btnRed">
                        <asp:Button ID="btnGetEmpDetails" CssClass="btnLeft" runat="server" Text="Get Emp Details" OnClientClick="return validatebtn()"
                            TabIndex="3" OnClick="btnGetEmpDetails_Click" /></div>
                </td>
            </tr>
        </table>
        <div id="divEmpList" runat="server" visible="false" class="grid01">
            <asp:GridView ID="grdEmployeeList" runat="server" AutoGenerateColumns="False" DataKeyNames="RequesteeTokenNumber"
                AlternatingRowStyle-CssClass="row01" AllowPaging="true" 
                onrowcommand="grdEmployeeList_RowCommand" onpageindexchanging="grdEmployeeList_PageIndexChanging" 
                PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                <Columns>
                    <asp:BoundField DataField="RequesteeTokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                    <asp:BoundField DataField="FullName" HeaderText="Employee Name" SortExpression="FullName" />
                    <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
                    <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                    <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("RequesteeTokenNumber") %>' CommandName="Select"
                                runat="server" CssClass="selLink" TabIndex="4">
                                            Select</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="dataPager" />
                <SelectedRowStyle CssClass="selectedoddTd" />
            </asp:GridView>
        </div>
        <table id="tblRequest" visible="false" runat="server">
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblTokenNUmber" runat="server" Text=""></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtTokenNumber" ReadOnly="true" BackColor="#FFFFCC" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Enter Amount&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRequestAmount" runat="server"  MaxLength="10" TabIndex="2" onkeyup="return validate(this,event)"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                   Reasons / Justification&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtReasons" TextMode="MultiLine" runat="server" MaxLength="500" width="300px" Rows="4"
                        TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2">
                 </td>
                <td align="center" >
                    <div class="btnRed">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btnLeft" Text="Submit" 
                            TabIndex="6" onclick="btnSubmit_Click"  OnClientClick="return validateSave()" /></div>
                        <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" 
                                onclick="btnCancel_Click" /></div>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField runat="server" ID="hiddenTokenNuber" />
</asp:Content>

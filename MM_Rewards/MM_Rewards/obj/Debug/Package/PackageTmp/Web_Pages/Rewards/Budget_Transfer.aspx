﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Budget_Transfer.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.Budget_Transfer" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <style>
        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons {
            margin: 0;
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
            float: none;
        }

            #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
                display: inline-block;
            }

        .send-ecard .nom-box-top h5 input {
            font-weight: 600;
            font-size: 20px;
            margin-bottom: 0;
            padding: 0;
        }

        .send-ecard .search-emp-m input {
            /*padding: 0;*/
            min-height: auto;
        }

        .send-ecard .search-emp-m ul input {
            padding: 0;
            font-size: 16px;
            width: 100%;
            color: #203442;
            font-weight: 500;
        }

        .send-ecard label.nom-box-lbl input {
            font-size: 11px;
            color: #fff!important;
            padding: 0;
        }

        .send-ecard .search-emp-m .send-ecard-message textarea {
            font-size: 18px;
            padding: 0 20px;
            min-height: 40px;
            border: 0;
            font-family: Calibri!important;
        }

        .send-awards.send-ecard input {
            margin: 0 auto;
            width: auto;
            padding: 10px 35px;
        }

        .send-awards input {
            width: 270px;
            padding: 9px;
            font-size: 20px;
            font-weight: 500;
            border-radius: 6px;
        }

        input.rnr-btn {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 110px;
        }

        .rnr-green input {
            font-weight: 600;
            margin: 0 0 -4px;
            font-size: 35px;
            -webkit-appearance: none;
            background: transparent;
            border: 0;
            color: #30ce8d!important;
        }
        #ContentPlaceHolder1_TxtAmount {
            background-color: #eee;
            padding: 0 12px;
            width: 100%;
        }
    </style>
    <script src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        }
        function compareamount() {
            var avaliable = $("#ContentPlaceHolder1_txtavaliablebalance").val();
            var avaliable = parseInt(avaliable);
            //alert(avaliable);
            var amount = $("#ContentPlaceHolder1_TxtAmount").val();
            if (amount != Math.floor(amount)) {
                alert('Amount Should not be Decimal');
                return 0;
            }
            if (amount == "") {
                alert('Please enter the Amount.');
                return 0;
            }
            var amount = parseInt(amount);
            //alert(amount);

            if (avaliable < amount) {
                alert('Available Budget Must Be Greater Than The Transfer Budget');
                return 0;// false;
            }
            if ((amount % 500) != 0) {
                alert('Amount must be multiple of 500');
                return 1;// false;
            }

            if (amount == 0) {
                alert("Amount 0 will not be transfered.")
                return 3;
            }
            return 2;// true;
        }
        function confirmSubmit() {
            var result = confirm('Are you sure to transfer the budget ?');
            if (result) {
                var mm = compareamount();
                if (mm == 0 || mm == 1 || mm == 3) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else
                return false;
        }
    </script>
    
    <section class="main-rnr">
        <div class="rnr-inner-top">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href='/Web_Pages/HomePage.aspx'>HOME</a></li>
                    <li><a href="/Web_Pages/Rewards/RecipientAward.aspx">Awards</a></li>
                    <li>Budget</li>
                </ul>
                <ul class="budget-widget hide">
                    <li class="total-buget"></li>
                    <li class="budget-used"></li>
                    <li class="budget-available"></li>
                </ul>
            </div>
        </div>

        <div class="rnr-budget">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-12">
                        <h3 class="rnr-h3-top">My Budget- <small>For The Year <span class="year-value">
                            <asp:Label ID="lblYear" runat="server"></asp:Label>
                        </span></small></h3>
                        <div class="budget-chart">
                            <ul class="middle-chart">
                                <li class="chart-b-value">
                                    <asp:Label ID="lblBudget" runat="server"></asp:Label></li>
                                <li>Total Budget</li>
                                <li>For the Year <span>
                                    <asp:Label ID="lblFYear" runat="server"></asp:Label></span> </li>
                            </ul>
                            <canvas id="budgetChart" width="300" height="300"></canvas>
                            <ul class="budget-legends">
                                <li class="used-bdg">
                                    <h2 class="rnr-red">
                                        <asp:Label ID="lblExpenses" runat="server" Text=""></asp:Label></h2>
                                    <span>Used Budget</span> </li>
                                <li class="available-bdg">
                                    <h2 class="rnr-green">
                                        <asp:Label ID="lblBalence" runat="server" Text=""></asp:Label>
                                        <asp:TextBox ID="txtavaliablebalance" runat="server" Width="100%" Visible="false" Style="border-bottom: none;"></asp:TextBox></h2>
                                    <span>Available Budget</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6 col-md-12">
                        <h3 class="rnr-h3-top h3-bugdget2">Budget Allocation</h3>
                        <div class="rnr-input search-emp-m step-1" runat="server" id="step1">
                            <div class="ecard-search-head">
                                <svg id="Group_3788" data-name="Group 3788" xmlns="http://www.w3.org/2000/svg" width="46.624" height="29.163" viewBox="0 0 46.624 29.163">
                                    <path id="Path_7559" data-name="Path 7559" d="M2,5.982v5.365H45.978L35.407,1V5.982Z" transform="translate(-0.353)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                    <path id="Path_7560" data-name="Path 7560" d="M45.624,32.365V27H1L11.726,37.348V32.365Z" transform="translate(0 -9.185)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                    <path id="Path_7561" data-name="Path 7561" d="M30.318,27.282A11.483,11.483,0,0,0,41.636,15.641,11.483,11.483,0,0,0,30.318,4,11.483,11.483,0,0,0,19,15.641,11.483,11.483,0,0,0,30.318,27.282Z" transform="translate(-6.359 -1.06)" fill="#30ce8d" fill-rule="evenodd" />
                                    <path id="Path_7562" data-name="Path 7562" d="M30.2,12.016a.322.322,0,0,0-.139.093L30,12.182v1.441l.076.088.076.088.955.016c.525.009,1.02.026,1.1.039a2.865,2.865,0,0,1,.937.289,1.313,1.313,0,0,1,.419.308,1.525,1.525,0,0,1,.273.385c0,.007-.813.013-1.807.013-1.98,0-1.924,0-1.994.139a2.091,2.091,0,0,0-.035.643V16.2l.08.08.08.08h1.893c1.041,0,1.9.008,1.9.019a.7.7,0,0,1-.056.193,1.985,1.985,0,0,1-1.436,1.189,5.424,5.424,0,0,1-1.505.15c-.7.013-.776.018-.838.058-.116.074-.121.108-.121.834,0,.654,0,.67.049.737.027.037.22.265.43.505.4.459.684.793.926,1.087.08.1.191.23.247.295.258.3,1.47,1.8,2.213,2.732.316.4.592.733.613.743.047.024,1.643.042,1.806.021a.341.341,0,0,0,.183-.073.335.335,0,0,0,.1-.281c-.033-.139-2.578-3.341-3.614-4.547-.184-.215-.353-.413-.376-.44l-.04-.05L32.7,19.5a4.428,4.428,0,0,0,1.991-.8,4.391,4.391,0,0,0,.776-.821A3.748,3.748,0,0,0,36,16.58l.039-.206.784-.006.784-.006.079-.079.079-.079v-.571c0-.613-.01-.674-.117-.739a4.419,4.419,0,0,0-.842-.042l-.792-.012-.047-.189a3.331,3.331,0,0,0-.407-.968l-.091-.142,1.054-.012c1.128-.013,1.116-.011,1.193-.136.034-.056.039-.129.039-.62,0-.609,0-.633-.139-.726a31.346,31.346,0,0,0-3.7-.045C31.915,12,30.244,12.005,30.2,12.016Z" transform="translate(-10.245 -3.886)" fill="#fff" fill-rule="evenodd" />
                                </svg>
                                <h4>Transfer Your Budget</h4>
                            </div>
                            <div class="search-emp" data-toggle="modal" data-target="#ContentPlaceHolder1_searchempmodal">
                                <!-- <label for="">Search Employee for CC</label> -->
                                <input type="text" class="bud-color" name="" value="" placeholder="Enter Name / Token Number" readonly>
                                <%--<p>Enter Name / Token Number</p> --%>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.8 363.8">
                                    <title>Search</title>
                                    <g id="Layer_2" data-name="Layer 2">
                                        <g id="Layer_1-2" data-name="Layer 1">
                                            <path d="M263.3,243.5a148.77,148.77,0,0,0-9.5-200c-58-58-152.4-58-210.3,0s-58,152.4,0,210.3a148.77,148.77,0,0,0,200,9.5L344,363.8,363.8,344Zm-29.2-9.4a120.71,120.71,0,1,1,35.4-85.4A120,120,0,0,1,234.1,234.1Z"></path>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="rnr-input search-emp-m step-2 hide" runat="server" id="step2">
                            <div class="ecard-search-head">
                                <svg id="Svg1" data-name="Group 3788" xmlns="http://www.w3.org/2000/svg" width="46.624" height="29.163" viewBox="0 0 46.624 29.163">
                                    <path id="Path1" data-name="Path 7559" d="M2,5.982v5.365H45.978L35.407,1V5.982Z" transform="translate(-0.353)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                    <path id="Path2" data-name="Path 7560" d="M45.624,32.365V27H1L11.726,37.348V32.365Z" transform="translate(0 -9.185)" fill="none" stroke="#30ce8d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                    <path id="Path3" data-name="Path 7561" d="M30.318,27.282A11.483,11.483,0,0,0,41.636,15.641,11.483,11.483,0,0,0,30.318,4,11.483,11.483,0,0,0,19,15.641,11.483,11.483,0,0,0,30.318,27.282Z" transform="translate(-6.359 -1.06)" fill="#30ce8d" fill-rule="evenodd" />
                                    <path id="Path4" data-name="Path 7562" d="M30.2,12.016a.322.322,0,0,0-.139.093L30,12.182v1.441l.076.088.076.088.955.016c.525.009,1.02.026,1.1.039a2.865,2.865,0,0,1,.937.289,1.313,1.313,0,0,1,.419.308,1.525,1.525,0,0,1,.273.385c0,.007-.813.013-1.807.013-1.98,0-1.924,0-1.994.139a2.091,2.091,0,0,0-.035.643V16.2l.08.08.08.08h1.893c1.041,0,1.9.008,1.9.019a.7.7,0,0,1-.056.193,1.985,1.985,0,0,1-1.436,1.189,5.424,5.424,0,0,1-1.505.15c-.7.013-.776.018-.838.058-.116.074-.121.108-.121.834,0,.654,0,.67.049.737.027.037.22.265.43.505.4.459.684.793.926,1.087.08.1.191.23.247.295.258.3,1.47,1.8,2.213,2.732.316.4.592.733.613.743.047.024,1.643.042,1.806.021a.341.341,0,0,0,.183-.073.335.335,0,0,0,.1-.281c-.033-.139-2.578-3.341-3.614-4.547-.184-.215-.353-.413-.376-.44l-.04-.05L32.7,19.5a4.428,4.428,0,0,0,1.991-.8,4.391,4.391,0,0,0,.776-.821A3.748,3.748,0,0,0,36,16.58l.039-.206.784-.006.784-.006.079-.079.079-.079v-.571c0-.613-.01-.674-.117-.739a4.419,4.419,0,0,0-.842-.042l-.792-.012-.047-.189a3.331,3.331,0,0,0-.407-.968l-.091-.142,1.054-.012c1.128-.013,1.116-.011,1.193-.136.034-.056.039-.129.039-.62,0-.609,0-.633-.139-.726a31.346,31.346,0,0,0-3.7-.045C31.915,12,30.244,12.005,30.2,12.016Z" transform="translate(-10.245 -3.886)" fill="#fff" fill-rule="evenodd" />
                                </svg>
                                <h4>Transfer Your Budget</h4>
                            </div>
                            <div class="nominee-box">
                                <div class="nom-box-top" data-toggle="modal" data-target="#ContentPlaceHolder1_searchempmodal">
                                    <h5>
                                        <%--                                        <asp:Label ID="lblManagerName" runat="server"></asp:Label>--%>
                                        <asp:TextBox ID="TxtManagerName" runat="server" Width="100%" Enabled="False" Style="border-bottom: none; margin: 6px"></asp:TextBox>
                                    </h5>
                                </div>
                                <div class="nom-box-main">
                                    <label for="" class="nom-box-lbl">
                                        <asp:Label ID="lblDesignation" runat="server"></asp:Label>

                                    </label>
                                    <ul>
                                        <li>
                                            <label for="" class="nom-detail-lbl">
                                                <asp:Label ID="lblDivision" runat="server"></asp:Label>
                                                <%--<input type="text" name="" value="" placeholder="Enter Your Message Here">--%>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="" class="nom-detail-lbl">
                                                <asp:Label ID="lblLocation" runat="server"></asp:Label></label>
                                        </li>
                                        <li>
                                            <label for="" class="nom-detail-lbl">
                                                <asp:Label ID="lblDepartment" runat="server"></asp:Label></label>
                                        </li>
                                        <li>
                                            <label for="" class="nom-detail-lbl">
                                                <asp:Label ID="lblLevel" runat="server"></asp:Label></label>
                                        </li>
                                        <li>
                                            <label for="" class="nom-detail-lbl">
                                                <asp:TextBox ID="TxtManagerTokenNO" runat="server" Width="100%" Enabled="False" Style="border-bottom: none;"></asp:TextBox>

                                            </label>
                                        </li>
                                    </ul>

                                    <ul>
                                        <li>
                                            <label for="" class="nom-detail-lbl">Enter Amount Below (Multiple of Rs 500/-)</label>
                                            <asp:TextBox ID="TxtAmount" runat="server" Width="100%" Enabled="true" onkeypress="javascript:return isNumber(event)"></asp:TextBox>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="send-awards send-ecard">
                                <asp:Button ID="btntransfer" runat="server" CssClass="send-ecard-btn rnr-btn" Text="Budget Transfer" TabIndex="-1" OnClick="btntransfer_Click" OnClientClick="return confirmSubmit();" />
                            </div>
                        </div>
                        <div class="rnr-input search-emp-m success-ecard hide" id="Success" runat="server" style="margin-top: 0;">

                            <ul>
                                <li>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="96.933" height="61.577" viewBox="0 0 96.933 61.577">
                                        <g id="Group_3791" data-name="Group 3791" transform="translate(-12.034 -7.644)">
                                            <path id="Path_7564" data-name="Path 7564" d="M15.377.7C13.7,3.39,0,28.047,0,28.379a16.006,16.006,0,0,0,3.7,2.634c2.9,1.74,3.8,2.111,4.171,1.724.856-.893,15.362-26.973,15.362-27.619,0-.362-1.539-1.588-3.61-2.875C17.637,1.009,15.968,0,15.913,0A2.647,2.647,0,0,0,15.377.7ZM17.3,20.48c-6.811,12.156-6.464,10.8-3.595,14.113l1.339,1.548,1.388-.846a5.755,5.755,0,0,1,2.08-.847,7.379,7.379,0,0,1,4.89,2.68l.935,1.376.836-1.175c1.779-2.5,5.024-2.645,7.538-.341a4.188,4.188,0,0,1,1.622,3.179c.2,1.785.3,1.909,1.726,2.295a6.384,6.384,0,0,1,4.184,4.764c.246,1.46.43,1.67,1.769,2.03a5.586,5.586,0,0,1,3.319,8.186l-.806,1.367,2.869.67a46.821,46.821,0,0,0,5.341.889c2.092.185,2.692.083,3.91-.67,2.841-1.756,2.691-2.018-2.638-4.649a53.277,53.277,0,0,1-4.983-2.642c-.247-.394.268-1.538.693-1.538a53.674,53.674,0,0,1,5.064,2.272c3.725,1.8,5.2,2.308,7.145,2.447,2.215.158,2.513.085,3.1-.758a4.148,4.148,0,0,0,.654-2.016c0-.972-.607-1.435-5.981-4.559-3.29-1.912-6.084-3.744-6.21-4.072-.264-.687.038-1.325.628-1.325.224,0,3.232,1.653,6.686,3.673,6.5,3.8,7.966,4.27,9.979,3.193.887-.475,1.917-2.94,1.587-3.8a45.151,45.151,0,0,0-6.145-4.045c-6.985-4.154-8.555-5.264-8.555-6.051,0-1.208,1.748-.446,9.23,4.026,5.149,3.077,8.1,4.606,8.883,4.606,1.466,0,3.517-1.936,3.517-3.32,0-1.482-1.219-2.757-6.544-6.842A141.946,141.946,0,0,0,51.136,20.038c-3.832-2.062-3.983-2.106-5.553-1.636a9.046,9.046,0,0,0-3.671,2.691c-2.816,3.022-4.964,4.431-7.9,5.183-4.068,1.042-6.383.095-6.366-2.606.011-1.854,1.071-4.159,3.585-7.794a29.791,29.791,0,0,0,2.014-3.14,5.476,5.476,0,0,0-2.1-.491A28.175,28.175,0,0,1,23.819,9.78C23.591,9.678,20.659,14.493,17.3,20.48Z" transform="translate(12.034 7.644)" fill="#30ce8d" fill-rule="evenodd" />
                                            <path id="Path_7565" data-name="Path 7565" d="M29.245,61.565c-2.12.532-4.669-2.187-4.007-4.274,1.473-2.67,5-8.407,7.311-10,1.2-.942,1.4-.955,2.9-.179a3.324,3.324,0,0,1,1.995,3.216C37.452,52.018,35.824,56.624,29.245,61.565Z" transform="translate(7.035 -1.616)" fill="#c4c4c4" />
                                            <path id="Path_7566" data-name="Path 7566" d="M37.8,68.243a2.692,2.692,0,0,1-2.5-1.071,4.988,4.988,0,0,1-.8-2.86c.07-1.282,4.663-6.409,6.95-8.812a3.4,3.4,0,0,1,3.389,2.362c.727,1.961,0,2.044-1.127,4.047C42.809,63.511,39.392,66.8,37.8,68.243Z" transform="translate(5.173 -3.395)" fill="#c4c4c4" />
                                            <path id="Path_7567" data-name="Path 7567" d="M44.359,68.844a20.491,20.491,0,0,1,2.6-3.551L48.916,63.5a9.058,9.058,0,0,1,1.642.567,4.4,4.4,0,0,1,1.822,3.439c0,1.77-3.068,5.167-4.6,6.644-1.321.332-3.21-.791-3.823-2.273A4.442,4.442,0,0,1,44.359,68.844Z" transform="translate(3.313 -4.986)" fill="#c4c4c4" />
                                            <path id="Path_7568" data-name="Path 7568" d="M19.215,48.1a18.057,18.057,0,0,1,1.9-2.163c1.15-.671,2.017-.484,3.369.723,1.886,1.685,1.788,3.431-.335,5.942-2.041,2.414-3.27,2.656-4.95.976C17.626,52,17.63,50.406,19.215,48.1Z" transform="translate(8.449 -1.416)" fill="#c4c4c4" />
                                            <path id="Path_7569" data-name="Path 7569" d="M45.253,12.512c1.3-1.381,3.21-1.956,8.575-2.582,6.147-.718,10.836-.395,17.987,1.24a17.21,17.21,0,0,0,8.26.267c.608-.1,1.9,1.928,6.7,10.494,3.273,5.838,5.942,10.795,5.932,11.015l-6.2,5.007a52.635,52.635,0,0,1-6.47-4.483c-6.048-4.247-18.624-13.061-20.547-14.343a8.2,8.2,0,0,0-5.229-1.193c-2.243.014-4.452,1.3-6.71,3.907a13.9,13.9,0,0,1-8.889,4.9s-.9-.009-1.2-.4c-.332-.43,0-1.391,0-1.391l3.283-6.263A75.6,75.6,0,0,1,45.253,12.512Z" transform="translate(4.611 5.741)" fill="#c4c4c4" />
                                            <path id="Path_7570" data-name="Path 7570" d="M92,4.655c.217-.217,1.883-1.34,3.7-2.5S99.1.047,99.213.032l8.569,14.7c4.1,7.3,7.449,13.445,7.449,13.666l-3.7,2.619c-2.9,1.738-3.8,2.11-4.167,1.724-.258-.271-3.909-6.613-8.115-14.094Z" transform="translate(-6.265 7.638)" fill="#c4c4c4" />
                                        </g>
                                    </svg>

                                </li>
                                <li>
                                    <h3>Transfer Success</h3>
                                </li>
                                <li>
                                    <h5>Rs. <span>
                                        <asp:Label ID="lblTransferAmt" runat="server"></asp:Label></span> from your budget has been successfully transferred to <span>
                                            <asp:Label ID="lblHODName" runat="server"></asp:Label></span> </h5>

                                </li>
                            </ul>
                            <a class="new-transfer" href="/Web_Pages/Rewards/Budget_Transfer.aspx">Make another transfer</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- search emp modal start-->
    <div class="modal fade search-emp-modal" id="searchempmodal" runat="server" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header rnr-modal-head">
                    <h5 class="modal-title">Search Employee</h5>
                </div>
                <div class="modal-body">
                    <div class="rnr-search-bar">
                        <div role="search" method="get" class="search-form form" action="">
                            <label for="" class="search-top-lbl">Refine Search(Eligible For Direct Employee Count >= 1)</label>
                            <label>
                                <asp:TextBox ID="txtTokenNumberTop" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>

                                <div class="btnRed SearchButtons">
                                    <asp:Button ID="btnGetDetails" OnClick="btnGetDetails_Click" CssClass="btnLeft" runat="server" Text="" TabIndex="3" />
                                </div>
                            </label>
                        </div>
                    </div>
                    <div class="rnr-table">
                        <div id="table-scroll" class="table-scroll">
                            <div id="Div1" class="">
                                <div class="grid01" id="divEmpList" runat="server" style="margin-top: 0;">
                                    <asp:GridView ID="grdRecipient" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                        AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                                        AlternatingRowStyle-CssClass="row01" OnRowCommand="grdRecipient_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                            <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                            <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                            <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                            <asp:BoundField DataField="DivisionName_PA" HeaderText="Division Name" SortExpression="DivisionName_PA" />
                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select"
                                                        runat="server" CssClass="selLink" TabIndex="4">
                                            Select</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="dataPager" />
                                        <%--<SelectedRowStyle CssClass="selectedoddTd" />--%>
                                    </asp:GridView>
                                </div>
                                <asp:HiddenField ID="hdnRecipientTokenNumber" runat="server" />
                                <asp:HiddenField ID="HiddenBalance" runat="server" />
                                <asp:HiddenField ID="hdnGiverName" runat="server" />
                                <asp:HiddenField ID="HdnDivisionCode" runat="server" />
                                <asp:HiddenField ID="hdnLoactionCode" runat="server" />

                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                       <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" CssClass="rnr-btn" />

<%--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return closeSearchPopup(this);">
                        <span aria-hidden="true">Cancel</span>
                    </button>--%>
                    <%--                    <button type="button" name="button" class="add-emp rnr-btn">Add</button>--%>
                </div>
            </div>
        </div>
    </div>
    <!-- search-emp-modal end -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
    <script>
        var YearlyBudget = parseInt('<%=Session["TotalYearlyBudget"]%>');
        var AvaliableBudget = parseInt('<%=Session["Balance"]%>');
        var UsedBudget =parseInt('<%=Session["Expenses"]%>');
        var UsedBudgetPer = (UsedBudget / YearlyBudget) * 100;
        var AvailableBudgetPer = (AvaliableBudget / YearlyBudget) * 100;       
        var ctx = document.getElementById("budgetChart");
        var budgetChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Used Budget', 'Available Budget'],
                datasets: [{
                    label: '# of Tomatoes',
                    data: [parseInt(UsedBudgetPer), parseInt(AvailableBudgetPer)],
                    backgroundColor: [
                      'rgb(48, 206, 141)',
                      'rgb(227, 24, 55)'
                    ],
                    borderWidth: 0
                }]
            },
            options: {
                cutoutPercentage: 65,
                responsive: false,
                legend: {
                    display: false,
                    labels: {
                        fontColor: 'rgb(255, 99, 132)'
                    }
                }

            }
        });
        $(document).ready(function () {
            $("body").addClass("send-ecard budget Chrome").attr("id", "budget");
        })
        function closeSearchPopup(obj) {
            $('#' + $(obj).attr('id')).removeClass('in show');
            $('#' + $(obj).attr('id')).removeAttr('style');
            $('#' + $(obj).attr('id')).hide('modal');
        }
    </script>
</asp:Content>

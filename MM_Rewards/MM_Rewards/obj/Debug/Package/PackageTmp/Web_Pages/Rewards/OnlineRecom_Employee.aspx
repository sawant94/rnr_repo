﻿<%@ Page Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" 
CodeBehind="OnlineRecom_Employee.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.OnlineRecom_Employee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function validatebtn() {
            var errMsg = '';
            var btnGET = document.getElementById('<%=txtSearchBy.ClientID %>');
            errMsg += ValidateName(btnGET.value, "Token Number / Name");
            errMsg += CheckText_Blank(btnGET.value, 'Token Number / Name')

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
        function validatebtnREmp() {
            var errMsg = '';
            var btnGET = document.getElementById('<%=txtRecEmployee.ClientID %>');
            errMsg += ValidateName(btnGET.value, "Token Number / Name");
            errMsg += CheckText_Blank(btnGET.value, 'Token Number / Name')

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
        function validateSave() {
            debugger;
            var errMsg = '';
            var txtRecEmployeeToken = document.getElementById('<%=txtRecEmployeeToken.ClientID %>');
            var txtRecEName = document.getElementById('<%=txtRecEName.ClientID %>');
            var txtRDivision = document.getElementById('<%=txtRDivision.ClientID %>');
            var txtRDept = document.getElementById('<%=txtRDept.ClientID %>');
            var txtRLevel = document.getElementById('<%=txtRLevel.ClientID %>');
            var txtRLocation = document.getElementById('<%=txtRLocation.ClientID %>');
            var txtEmpReason = document.getElementById('<%=txtEmpReason.ClientID %>');
            var txtId = document.getElementById('<%=ddlAwardType.ClientID %>')

            errMsg += ValidateName(txtRecEmployeeToken.value, "Token Number / Name");
            errMsg += CheckText_Blank(txtRecEmployeeToken.value, 'Token Number / Name')

            errMsg += ValidateName(txtRecEName.value, "Token Number / Name");
            errMsg += CheckText_Blank(txtRecEName.value, 'Name')

            errMsg += CheckText_Blank(txtRDivision.value, 'Division')

           errMsg += CheckText_Blank(txtRDept.value, 'Department')

            errMsg += CheckText_Blank(txtRLevel.value, 'Level')

            errMsg += CheckText_Blank(txtRLocation.value, 'Location')

            errMsg += CheckDropdown_Blank(txtId.value, " Award Type");

            errMsg += CheckText_Blank(txtEmpReason.value, 'Reasons')

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }

    </script>

    <div class="form01">
        <table>
            <%--<tr> 
                <td class="lblName">
                    IsCE / HOD &nbsp<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:RadioButtonList ID="rblISCEHOD" runat="server" RepeatDirection="Horizontal"
                        Width="200px" CssClass="chkBox">
                        <asp:ListItem Text="CE" Selected="True" Value="C" />
                        <asp:ListItem Text="HOD" Value="H" />
                    </asp:RadioButtonList>
                </td>
            </tr>--%>
            <tr>
                <td class="lblName">
                    Enter HOD Name / Token No.&nbsp<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtSearchBy" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                    &nbsp;
                    <div class="btnRed">
                        <asp:Button ID="btnGetEmpDetails" CssClass="btnLeft" runat="server" Text="Get Emp Details" 
                            TabIndex="3"  OnClientClick="return validatebtn()" OnClick="btnGetEmpDetails_Click" /></div>
                </td>
            </tr>
        </table>
        <div id="divEmpList" runat="server" class="grid01" visible="false">
            <asp:GridView ID="grdEmployeeList" runat="server" AutoGenerateColumns="False" 
                AlternatingRowStyle-CssClass="row01" AllowPaging="true" OnRowCommand="grdEmployeeList_RowCommand" onPageIndexChanging="grdEmployeeList_PageIndexChanging"    
                PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                <Columns>
                    <asp:BoundField DataField="RequesteeTokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                    <asp:BoundField DataField="FullName" HeaderText="Employee Name" SortExpression="FullName" />
                    <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
                    <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                     <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("RequesteeTokenNumber") %>' CommandName="Select"
                                runat="server" CssClass="selLink" TabIndex="4">
                                            Select</asp:LinkButton>
                            <%--<asp:Label ID="lblLevel" runat="server" Visible="false" Text='<%# Eval("Levels") %>'></asp:Label>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField Visible="false" DataField="Levels" HeaderText="Levels" SortExpression="" />--%>
                </Columns>
            </asp:GridView>
        </div>
        <table id="tblRequest" runat="server" visible="false">
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblTokenNUmber" runat="server" Text="HOD Token Number"></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtTokenNumber" ReadOnly="true" BackColor="#FFFFCC" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Recommended Employee Name&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRecEmployee" runat="server"  MaxLength="50" TabIndex="2" ></asp:TextBox>
                    <div class="btnRed">
                        <asp:Button ID="btnRecomEmployees" CssClass="btnLeft" runat="server" Text="Get Emp Details" 
                            TabIndex="3"  OnClientClick="return validatebtnREmp()" OnClick="btnRecomEmployees_Click" /></div>
                </td>
            </tr>
        </table>
        <div id="divREmpList" runat="server" class="grid01" visible="false">
            <asp:GridView ID="grdREmployeeList" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="grdREmployeeList_PageIndexChanging"  
                AlternatingRowStyle-CssClass="row01" AllowPaging="true" OnRowCommand="grdREmployeeList_RowCommand"  
                PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                <Columns>
                    <asp:BoundField DataField="RequesteeTokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                    <asp:BoundField DataField="FullName" HeaderText="Employee Name" SortExpression="FullName" />
                    <asp:BoundField DataField="Division" HeaderText="Division" SortExpression="Division" />
                    <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                    <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("RequesteeTokenNumber") %>' CommandName="Select"
                                runat="server" CssClass="selLink" TabIndex="4">
                                            Select</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <table id="tblRecEmplList" runat="server" visible="false">
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblREmployee" runat="server" Text="Recommended Employee Token Number"></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRecEmployeeToken" ReadOnly="true" BackColor="#FFFFCC" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblRempName" runat="server" Text="Recommended Employee Name"></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRecEName" ReadOnly="true" BackColor="#FFFFCC" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Division&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRDivision" runat="server"  MaxLength="50" TabIndex="2" ReadOnly="true" BackColor="#FFFFCC" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Department&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRDept" runat="server"  MaxLength="50" TabIndex="2" ReadOnly="true" BackColor="#FFFFCC" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Level&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRLevel" runat="server"  MaxLength="50" TabIndex="2" ReadOnly="true" BackColor="#FFFFCC" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Location&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRLocation" runat="server"  MaxLength="50" TabIndex="2" ReadOnly="true" BackColor="#FFFFCC" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Type of Award&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddlAwardType" runat="server" Width="200px" TabIndex="1">
                        </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Reason for nomination&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                   <asp:TextBox ID="txtEmpReason" runat="server" TextMode="MultiLine" Width="300px"
                            Rows="4" TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2">
                 </td>
                <td align="center" >
                    <div class="btnRed">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btnLeft" Text="Submit" 
                            TabIndex="6" onclick="btnSubmit_Click"  OnClientClick="return validateSave()" /></div>
                        <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft"  onclick="btnCancel_Click" /></div>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField runat="server" ID="hiddenTokenNumber" />
</asp:Content>

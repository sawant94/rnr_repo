﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Budget_Approval.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.Budget_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
<script type="text/javascript" language="javascript">
    function validate(sender, key) {
        //getting key code of pressed key
        var keycode = (key.which) ? key.which : key.keyCode;
        //comparing pressed keycodes
        if ((keycode >= 48 && keycode <= 57)
                 || (keycode >= 96 && keycode <= 105)
                 || keycode == 110
                 || (keycode >= 35 && keycode <= 40)  // 35 end,36 home,37 to 40 are arrow keys
                 || keycode == 8 // backspace
                 || keycode == 9  // tab
                 || keycode == 46  // delete
                || keycode == 190
                || keycode == 110
                ) {
            return true;
        }
        else {
            sender.value = "";
            return false;
        }
    }

    function validateSave() {

        var r = confirm("Do you want to submit record ?");
        //alert(r);
        if (r != true) {
            return false;
        }


        var errMsg = '';
        var txtAmount = document.getElementById('<%=txtAmount.ClientID %>');
        var txtRemarks = document.getElementById('<%=txtRemarks.ClientID %>');
        var txtBudget = document.getElementById('<%=txtBudget.ClientID %>');
        
        errMsg += CheckText_Blank(txtAmount.value, 'Approved Amount')
        errMsg += CheckText_Blank(txtRemarks.value, 'Remarks')

        var Budget = Math.floor(txtBudget.value);
        var ApprovedAmt = Math.floor(txtAmount.value);
        if (ApprovedAmt > Budget) {

            errMsg += "Your Budget is less, Please Enter Proper Budget";
        }

        if (errMsg != '') {
            alert(errMsg);
            return false;
        }
        else {
            return true;
        }
    }

//    function fnEnableDisable() {
//        debugger;
//        var checkedRadio = $('input:checked', '#<%=rdpproval.ClientID %>');

//        if (checkedRadio.val() == 1) {

//            txtID = document.getElementById('<%=txtAmount.ClientID %>');
//            txtID.disabled = true;

//        }

//        else if (checkedRadio.val() == 0) {

//        }

//    }
</script>
    <h1>
        Budget Approval
    </h1>
    <div class="grid01">
        <div style="overflow: auto; max-height: 380px;">
            <asp:GridView ID="grdBudgetApproval" runat="server" AllowPaging="true" PageSize='<%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                AutoGenerateColumns="false" DataKeyNames="ID" AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdBudgetApproval_PageIndexChanging" >
                <Columns>
                    <asp:BoundField DataField="ID" Visible="false" HeaderText="ID" SortExpression="" />
                     <asp:BoundField DataField="RequestorTokenNumber" HeaderText="Requestor TokenNumber"
                        SortExpression="" />
                    <asp:BoundField DataField="FullName" HeaderText="Requestor Name" SortExpression="" />
                    <asp:BoundField DataField="RequestedAmount" HeaderText="Requested Amount" SortExpression="" />
                    <asp:BoundField DataField="RequestedDate" HeaderText="Requested On" SortExpression="RequestedDate"/>
                    <asp:BoundField DataField="ShortText" HeaderText="Request Reason" SortExpression="" />
                    <asp:BoundField DataField="RequesteeTokenNumber" HeaderText="Requestee TokenNumber"
                        SortExpression=""/>
                    <asp:BoundField DataField="RequesteeName" HeaderText="Requestee Name" SortExpression="" />
                    <asp:BoundField DataField="Budget" HeaderText="Requestee Budget" SortExpression="" />
                    <asp:BoundField DataField="IsCEHOD" HeaderText="Type" SortExpression="" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton CssClass="editLink" ID="lnkEdit" runat="server"  OnClick="editLink_Click" Text="Edit" CommandName="Select" />
                             <asp:Label ID="lblFulltext" runat="server" Visible="false" Text='<%#Eval("RequestReason") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1" BorderStyle="Solid" Font-Italic="true" />
                <EmptyDataTemplate>
                    <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                </EmptyDataTemplate>
                <SelectedRowStyle CssClass="selectedoddTd" />
            </asp:GridView>
        </div>
    </div>
    <div class="form01" id="divedit" visible="false" runat="server">
     <table>
      <tr>
            <td class="lblName">
                <asp:Label ID="lblReqteeTN" runat="server" Text="Requestor TokenNumber"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtReqtorTN" runat="server"  BackColor="#FFFFCC" TabIndex="1" Enabled="false" Width="250px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td class="lblName">
                <asp:Label ID="lblReqteeName" runat="server"  Text="Requestor Name"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtReqtorName" runat="server" BackColor="#FFFFCC" TabIndex="1" Enabled="false" Width="250px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td class="lblName">
                <asp:Label ID="lblReqAmt" runat="server"  Text="Requested Amount"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtReqAmt" runat="server" BackColor="#FFFFCC" TabIndex="1" Enabled="false"  
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblName">
                <asp:Label ID="lblReason" runat="server" Text="Reason"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtReason" runat="server"  BackColor="#FFFFCC" TabIndex="1" Enabled="false" TextMode="MultiLine"
                    Rows="5" Width="250px"></asp:TextBox>
            </td>
        </tr>
        
         <tr>
            <td class="lblName">
                <asp:Label ID="lblBudget" runat="server"  Text="Budget"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtBudget" runat="server" BackColor="#FFFFCC" TabIndex="1" Enabled="false"  
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblName">
                <asp:Label ID="lblApproval" runat="server" Text="Approve / Reject"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:RadioButtonList ID="rdpproval" runat="server" AutoPostBack="true" 
                    RepeatDirection="Horizontal" Width="200px" 
                    CssClass="chkBox" 
                    OnSelectedIndexChanged="rdpproval_SelectedIndexChanged">
                    <asp:ListItem Text="Approved" Value= "1" Selected="True" />
                    <asp:ListItem Text="Reject" Value= "0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="lblName"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtAmount" runat="server" Rows="5" TabIndex="3" onkeyup="return validate(this,event)"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lblName"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtRemarks" runat="server" MaxLength="150" TextMode="MultiLine"
                    Rows="5" Width="250px" TabIndex="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td>
                <div class="btnRed">
                    <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btnLeft"  OnClientClick="return validateSave()"
                        TabIndex="4" onclick="btnsave_Click" />
                </div>
                <div class="btnRed">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft"
                        TabIndex="5" onclick="btnCancel_Click" />
                </div>
            </td>
        </tr>
        <tr>
                <td>
                    <asp:HiddenField ID="hdnid" runat="server" />
                </td>
            </tr>
    </table>
    </div>
</asp:Content>

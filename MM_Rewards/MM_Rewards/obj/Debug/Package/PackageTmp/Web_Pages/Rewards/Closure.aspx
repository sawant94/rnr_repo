﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Closure.aspx.cs" Inherits="MM_Rewards.Closer"
    EnableEventValidation="false" MasterPageFile="~/Web_Pages/MM_Rewards.Master" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var chkAll = '';

        function CheckUnCheckAll() {
            debugger
            grdCloserList = document.getElementById('<%= grdCloserList.ClientID %>');
            hdnTotalItems = document.getElementById('<%= hdnTotalItems.ClientID %>');
            hdnItemsChecked = document.getElementById('<%= hdnItemsChecked.ClientID %>');

            if (grdCloserList.rows.length > 0) {
                if (chkAll == '')
                    chkAll = grdCloserList.rows[0].cells[0].getElementsByTagName("INPUT")[0];

                if (chkAll.checked)
                    hdnItemsChecked.value = grdCloserList.rows.length - 1;
                else
                    hdnItemsChecked.value = "0";

                for (i = 1; i <= grdCloserList.rows.length - 1; i++) {
                    chk_Closure = grdCloserList.rows[i].cells[0].getElementsByTagName("INPUT")[0];
                    dpStart = grdCloserList.rows[i].cells[9].getElementsByTagName("INPUT")[0].id;
                    dpStart = $find(dpStart);
                    txtAmount = grdCloserList.rows[i].cells[10].getElementsByTagName("INPUT")[0];

                    if (chkAll.checked) {
                        chk_Closure.checked = true;
                        txtAmount.disabled = false;
                        dpStart.set_enabled(true);

                        txtAmount.value = grdCloserList.rows[i].cells[8].innerHTML;
                        AwardedDate = grdCloserList.rows[i].cells[4].innerHTML;
                        var date1 = Date.parse(AwardedDate);
                        dpStart.set_selectedDate(new Date());

                        //grdCloserList.rows[i].style.backgroundColor = '#FFFFCC';
                        //grdCloserList.rows[i].style.backgroundImage = "none";

                        SelectRow(grdCloserList.rows[i]);
                    }
                    else {
                        chk_Closure.checked = false;
                        txtAmount.disabled = true;
                        dpStart.set_enabled(false);

                        txtAmount.value = "";
                        dpStart.clear();

                        //grdCloserList.rows[i].style.backgroundColor = ''; // '#FFEEDD';
                        ////grdCloserList.rows[i].style.backgroundImage = "none";

                        UnselectRow(grdCloserList.rows[i]);
                    }
                }
            }
        }


        function UploadExcel() {

            window.open("/FileUpload.aspx", "FileUpload", "status=0,toolbar=0,resizable=yes,width=1000,height=500,left=10,top=50,scrollbars=yes");

        }
        function validate(sender, key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes
            if ((keycode >= 48 && keycode <= 57)
                 || (keycode >= 96 && keycode <= 105)
                 || keycode == 110
                 || (keycode >= 35 && keycode <= 40)  // 35 end,36 home,37 to 40 are arrow keys
                 || keycode == 8 // backspace
                 || keycode == 9  // tab
                ) {
                return true;
            }
            else {

                sender.value = "";
                return false;

            }
        }


        function SwitchControls(chkSelect, txtAmount, dpStart, rowIndex, e) {
        debugger    
            grdCloserList = document.getElementById('<%= grdCloserList.ClientID %>');
            if (chkAll == '') {
                chkAll = grdCloserList.rows[0].cells[0].getElementsByTagName("INPUT")[0];
            }

            var chkSelect = document.getElementById(chkSelect);
            var txtAmount = document.getElementById(txtAmount);
            var dpStart = $find(dpStart);

            hdnTotalItems = document.getElementById('<%= hdnTotalItems.ClientID %>');
            hdnItemsChecked = document.getElementById('<%= hdnItemsChecked.ClientID %>');

            var row = grdCloserList.rows[parseInt(rowIndex) + 1];

            if (chkSelect.checked) {
                hdnItemsChecked.value++;
                txtAmount.disabled = false;
                dpStart.set_enabled(true);

                //                row.style.backgroundColor = '#FFFFCC';
                //                row.style.backgroundImage = "none";
                SelectRow(row);
            }
            else {
                //row.style.backgroundColor = '';
                UnselectRow(row);
                hdnItemsChecked.value--;
                txtAmount.value = "";
                dpStart.clear();
                txtAmount.disabled = true;
                dpStart.set_enabled(false);
            }

            if (hdnTotalItems.value == hdnItemsChecked.value && hdnItemsChecked.value != "0")
                chkAll.checked = true;
            else
                chkAll.checked = false;
        }

        function btnGet_ClientClick() {
            var dpStartDate = $find('<%=dpStartDate.ClientID %>');
            var dpEndDate = $find('<%=dpEndDate.ClientID %>');

            var err = '';
            err += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");

            if (err != "") {
                alert(err);
                return false;
            }
            return true;
        }
      
        
    </script>
    <div class="form01">
        <h1>
            Award Closure</h1>
        <asp:HiddenField ID="hdnTotalItems" Value="0" runat="server" />
        <%--<asp:HiddenField ID="hdna" Value="0" runat="server" />--%>
        <asp:HiddenField ID="hdnItemsChecked" Value="0" runat="server" />
        <div class="empDtpick">
                <ul >
                    <li>Start Date  <span style="color: Red">*</span>: </li>
                    <li>
                        <telerik:RadDatePicker ID="dpStartDate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="1">
                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="1">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="1"></DatePopupButton>
                        </telerik:RadDatePicker>
                    </li>
                    <li>End Date  <span style="color: Red">*</span>:</li> 
                    <li>
                        <telerik:RadDatePicker ID="dpEndDate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="2">
                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="2">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="2"></DatePopupButton>
                        </telerik:RadDatePicker>
                    </li>
                    <li>
                        <div class="btnRed">
                            <asp:Button ID="btnGet" CssClass="btnLeft" runat="server" Text="Get closure Detail"
                           TabIndex="3" onclick="btnGet_Click" OnClientClick="return btnGet_ClientClick()" /></div>
                    </li>
                </ul>
            </div>
        <asp:LinkButton ID="btnExportToExcel" Visible="true" runat="server" Text="Export Data" OnClick="btnExportToExcel_Click" CssClass="ExcelUpload"></asp:LinkButton>
        <div class="grid01">
            <div class="actabHold" style="overflow: scroll; height: 600px;">
                <asp:GridView ID="grdCloserList" runat="server" Width="1500px" AutoGenerateColumns="False"
                    OnRowCommand="grdCloserList_RowCommand" DataKeyNames="Id" OnRowDataBound="grdCloserList_RowDataBound"
                    AlternatingRowStyle-CssClass="row01" AllowPaging="true" OnPageIndexChanging="grdCloserList_PageIndexChanging"
                    PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>' >
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" CssClass="chkBox" AutoPostBack="false" onclick="CheckUnCheckAll();" />
                                <%--  OnCheckedChanged="chkAll_CheckChanged"  --%>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div>
                                    <asp:CheckBox ID="chkcloser" CssClass="chkBox" runat="server" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Awardid" HeaderText="Award ID" Visible="true" />
                        <asp:BoundField DataField="Tokennumber" HeaderText="Recipient Token Number " SortExpression="RecipientTokenNumber" />
                        <asp:BoundField DataField="RecipientName" HeaderStyle-Width="150px" HeaderText="Recipient Name"
                            SortExpression="RecipientName" />
                        <asp:BoundField DataField="AwardedDate" HeaderStyle-Width="100px" HeaderText="Awarded Date"
                            SortExpression="AwardedDate" />
                        <asp:BoundField DataField="AwardType" HeaderText="Award Type" SortExpression="AwardType" />
                        <asp:BoundField DataField="RedeemedDate" HeaderStyle-Width="100px" HeaderText="Redeem Date"
                            SortExpression="RedeemedDate" />
                        <asp:BoundField DataField="RedeemedType" HeaderText="Redeem Type" SortExpression="RedeemedType" />
                        <asp:BoundField DataField="Amount" HeaderText="Award Amount" SortExpression="AwardAmount" />
                        <asp:TemplateField HeaderText="Date Of Closure">
                            <ItemTemplate>
                                <telerik:RadDatePicker ID="dtpclosuredate" runat="server" Width="120px" Enabled="false"
                                    DateInput-DateFormat="dd/MM/yyyy" TabIndex="4">
                                </telerik:RadDatePicker>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Closing Amount">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmount" Width="90px" runat="server" onkeyup="return validate(this,event)"
                                    Enabled="false" TabIndex="5">
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ReasonForNomination" HeaderText="Reason For Nomination"
                            HeaderStyle-Width="400px" />
                    </Columns>
                    <PagerStyle CssClass="dataPager" />
                    <SelectedRowStyle CssClass="selectedoddTd" />
                    <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                        BorderStyle="Solid" Font-Italic="true" />
                    <EmptyDataTemplate>
                        <asp:Literal ID="ltrlNorecords" runat="server" Text="No record To display "></asp:Literal>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
        <table id="tblsubmit" runat="server">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnSave" runat="server" Text="Submit" CssClass="btnLeft" OnClick="btnSave_Click"
                            TabIndex="1" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btnLeft" OnClick="btnCancel_Click"
                            TabIndex="2" />
                    </div>
                    <a href="JavaScript:UploadExcel();" tabindex="3" class="linkBtn">Upload Closure Excel</a>
                </td>
            </tr>
        </table>
    </div>
    <%-- <div>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetCloserDataForExcel" TypeName="BL_Rewards.Admin.BL_RecipientClosure"></asp:ObjectDataSource>

        <rsweb:ReportViewer ID="ReportViewer1" runat="server">
        </rsweb:ReportViewer>
    </div>--%>
</asp:Content>

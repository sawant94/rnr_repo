﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesAward.aspx.cs" MasterPageFile="~/Web_Pages/MM_RewardsSales.Master"
    Inherits="MM_Rewards.SalesAward" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
   <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
     <script type="text/javascript">



        function OnSelectedIndexChange1() {

            var Value = AwardType.options[AwardType.selectedIndex].text;
            var Awardvalue = Value.split('.');
            var Balance = document.getElementById('<%=HiddenBalance.ClientID %>');
            var TotalBal = Math.floor(Balance.defaultValue);

            if (Math.floor(Awardvalue[1]) > TotalBal) {
                alert('You do not have budget for the selected Award. Please select another award.');
                document.getElementById('<%=btnGetEmpDetails.ClientID  %>').disabled = true;
            }
            else {
                document.getElementById('<%=btnGetEmpDetails.ClientID  %>').disabled = false;
            }
        }
        function ValidatePage() {
            var errorMsg = '';
            var txtId;

            txtId = document.getElementById('<%=ddlAwardType.ClientID %>')
            errorMsg += CheckDropdown_Blank(txtId.value, " Award Type");

            txtId = document.getElementById('<%= txtTokenNumberTop.ClientID %>');
            errorMsg += CheckText_Blank(txtId.value, 'Token Number / Name');
            errorMsg += ValidateName(txtId.value, 'Token Number / Name');

            if (errorMsg != '') {
                alert(errorMsg);
                return false;
            }
            else {
                return true;
            }
        }


       

        function ValidateOnSubmit() {

            var errorMsg = '';
            var txtId;


            var AwardType = document.getElementById('<%=ddlGifts.ClientID %>');


            var Value = AwardType.options[AwardType.selectedIndex].text;


            errorMsg += CheckDropdown_Blank(Value, " Award Type");
            //            if (Value == '-- Select --') {
            //                document.getElementById('<%=btnSubmit.ClientID  %>').disabled = true;
            //            }
            //            else {
            //                document.getElementById('<%=btnSubmit.ClientID  %>').disabled = false;
            //            }

            txtId = document.getElementById('<%=txtEmpReason.ClientID  %>')
            errorMsg += CheckText_Blank(txtId.value, "Reason of Nomination");

            if (AwardType.options[AwardType.selectedIndex].value.split('|')[1] == "1") {

                if (!confirm("The award you have selected will be auto redeemed by salary.Do you wish to proceed?"))
                { return false; }
            }


            if (errorMsg != '') {
                alert(errorMsg);
                return false;
            }
            else {
                return true;
            }
        }
        function scrollPreview() {
            if (jQuery.browser.msie && jQuery.browser.version.substring(0, 1) == 7) {
                alert('You are using IE 7');
                window.scrollTo(100, 500);
                return false;
            }
            return true;
        }
        /****Done by Kiran on 02 july 2013**/
        $(document).ready(function () {
            //            var t = "<%=this.scroll1 %>";
            //            if(t=="1")
            //            {
            //                if (jQuery.browser.msie && jQuery.browser.version.substring(0, 1) == 7) {
            //                alert('You are using IE 7');
            //                window.scrollTo(100, 500);
            //                 return false;
            //             }
            //         }

            $('.closePop').click(function () {
                $('.registerPopup').fadeOut('fast');
                $('.popupGreyBox').fadeOut('fast');
                $('.popupGreyBox1').fadeOut('fast');
            });
        });

        $(document).ready(function () {
            var t = "<%=this.scroll1 %>";
            if (t == "1") {
                if (jQuery.browser.msie && jQuery.browser.version.substring(0, 1) == 7) {
                    window.scrollTo(100, 500);
                    return false;
                }
            }
        });
        /****Done by Kiran on 02 july 2013 end**/
        
    </script>
    <!--Done by Kiran on 02 july 2013-->
    <style type="text/css">
        .popupGreyBox
        {
            display: none;
            background: none repeat scroll 0 0 Black;
            bottom: 0;
            height: 1111px !important;
            left: 0;
            opacity: 0.49;
            filter: alpha(opacity=50);
            position: absolute;
            right: 0;
            top: 0;
            width: 100%;
            z-index: 100;
        }
        .registerPopup
        {
            display: none;
            background-color: White; /*left: 25%;*/
            position: absolute; /*width: 598px;*/
            width: 1050px;
            left: 10%;
            z-index: 101; /*bottom: -433px;*/
            top: 0%;
        }
        .popHead
        {
            background: none repeat scroll 0 0 #F2F2F3;
            clear: both;
            font-weight: bold;
            height: 100%;
            overflow: hidden;
            padding: 5px 10px;
        }
        .popHead .closePop
        {
            float: right;
        }
        .popHead .closePop span
        {
            background: url("/Includes/images/popupClose.gif") no-repeat scroll 0 0 transparent;
            cursor: pointer;
            display: block;
            height: 10px;
            width: 10px;
        }
        .ptpshowup
        {
            display: block;
        }
        .Timepick
        {
            display: inline-block;
        }
    </style>
    <!--Done by Kiran on 02 july 2013 end-->
    <asp:HiddenField ID="lvl" runat="server" />
    <!--Done by Swapneel on 07 Apr 2014-->
    <asp:HiddenField ID="hdnNomEmail" runat="server" />
    <asp:Label ID="lblNote" runat="server" Font-Italic="True" Text="" Visible="false"></asp:Label>
    <div id='div1' style="display: none;">
        <h1>
            Your budget for F12 have lapsed. Budgets for F13 will be uploaded soon.
        </h1>
    </div>
    <div id='div2'>
        <h1>
            DETAILS OF PERSON MAKING NOMINATIONS</h1>
        <div class="form01">
            <table>
                <tr style="display:none">
                    <td class="lblName">
                        Name <span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtHODName" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr style="display:none">
                    <td class="lblName">
                        Division&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtHODProcess" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr style="display:none">
                    <td class="lblName">
                        Location&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtHODLocation" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Budget for the Year
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="2">
                        <img alt="" src="../../Images/rupee_icon.jpg" runat="server" id="img1" />&nbsp<asp:Label
                            ID="lblBudget" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Amount Used
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="2">
                        <img alt="" src="../../Images/rupee_icon.jpg" runat="server" id="img2" />&nbsp<asp:Label
                            ID="lblExpenses" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Balance
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="2">
                        <img alt="" src="../../Images/rupee_icon.jpg" runat="server" id="img3" />&nbsp<asp:Label
                            ID="lblBalance" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Type of award <span style="color: Red">*</span>
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="lblFromDate" AssociatedControlID="Date" runat="server" Text="Date"
                            Style="display: none;"></asp:Label>
                    </td>
                    <td>
                        :
                    </td>
                    <td class="awardtype" style="width: 220px" colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlAwardType" runat="server" Width="200px" TabIndex="1" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlAwardType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <br />
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlGifts" runat="server" Width="200px" TabIndex="2" class="awardamt"
                                        Style="display: none;">
                                    </asp:DropDownList>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 220px;">
                                    <telerik:RadDatePicker ID="Date" DateInput-Culture="" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                                        TabIndex="1" Style="display: none;" OnSelectedDateChanged="Date_SelectedDateChanged"
                                        AutoPostBack="true" class="Datepick">
                                        <Calendar ID="Calendar1" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                            ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput ID="txtDate" runat="server" DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy"
                                            TabIndex="1">
                                        </DateInput>
                                        <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="1"></DatePopupButton>
                                    </telerik:RadDatePicker>
                                </td>
                                <td>
                                    <span id="spnTime" style="display: none;" runat="server">Time :</span>
                                    <telerik:RadTimePicker ID="FromTime" runat="server" Style="display: none;" class="Timepick">
                                    </telerik:RadTimePicker>
                                </td>
                            </tr>
                        </table>
                        <%--<asp:Label ID="Label1" runat="server" Text="Hamper to be selected upto Rs.1000 Only" Font-Bold="true" Visible="false"></asp:Label>--%>
                        <%--Added by Gaurav--%>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        <asp:Label runat="server" ID="lblAwardType" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h1>
                            DETAILS OF NOMINATED EMPLOYEE</h1>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Enter Name/Token No&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txtTokenNumberTop" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                                    &nbsp;
                                    <div class="btnRed">
                                        <asp:Button ID="btnGetEmpDetails" CssClass="btnLeft" runat="server" Text="Get Emp Details"
                                            OnClick="btnGetEmpDetails_Click" OnClientClick="return ValidatePage()" TabIndex="3" /></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="lblName" colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div id="divEmpList" runat="server" visible="false" class="grid01">
                            <asp:GridView ID="grdRecipientList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                OnRowCommand="grdRecipientList_RowCommand" Visible="False" AlternatingRowStyle-CssClass="row01"
                                AllowPaging="true" OnPageIndexChanging="grdRecipientList_PageIndexChanging" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                                <Columns>
                                    <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                    <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                    <asp:BoundField DataField="EmployeeLevelName_ES" HeaderText="Emp Level" SortExpression="EmployeeLevelName_ES" />
                                    <asp:BoundField DataField="ProcessName" HeaderText="Division Name" SortExpression="LocationName_PSA" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select"
                                                runat="server" CssClass="selLink" TabIndex="4">
                                            Select</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="dataPager" />
                                <SelectedRowStyle CssClass="selectedoddTd" />
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label CssClass="lblName" runat="server" ID="lblGetEmpResponse" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="form01">
            <table id="tblRecipientList" runat="server" visible="false">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Name&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpName" runat="server" BackColor="#FFFFCC" ForeColor="Black"
                            Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Division&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpDivision" runat="server" BackColor="#FFFFCC" ForeColor="Black"
                            Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        <asp:HiddenField ID="hdnLoactionCode" runat="server" />
                        <%--Added for web service by swapneel--%>
                        <asp:HiddenField ID="hdnEmailId" runat="server" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Department&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpDepartmentName" runat="server" BackColor="#FFFFCC" ForeColor="Black"
                            Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Designation&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpDesignation" runat="server" BackColor="#FFFFCC" ForeColor="Black"
                            Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Level&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpLevel" runat="server" BackColor="#FFFFCC" ForeColor="Black"
                            Enabled="False"></asp:TextBox>
                    </td>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Location&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpLocation" runat="server" BackColor="#FFFFCC" Enabled="False"
                            ForeColor="Black"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                        <asp:HiddenField ID="HdnDivisionCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Reason for Nomination&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpReason" runat="server" TextMode="MultiLine" Width="300px"
                            Rows="4" TabIndex="5" MaxLength="200"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <%--<tr>
                    <td class="lblName">
                        Enter First name/Last name/Token number for adding CC &nbsp;<%--<span style="color: Red">*</span>--%>
                        <%--Added by gaurav
                    </td>
                    <td>
                        :
                    </td>
                    <%-- <td>
                        <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                        &nbsp;
                    </td>
                    <td>
                        <%--            <div class="btnRed">
                            <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text="Get Details"
                                OnClick="btnGetDetails_Click" TabIndex="3" OnClientClick="return ValidateEmp()" /></div>
                        <table>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                                </td>
                                <td style="padding-left: 30px;">
                                    <div class="btnRed">
                                        <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text="Get Details"
                                            OnClick="btnGetDetails_Click" TabIndex="3" OnClientClick="return ValidateEmp()" /></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <%--<tr>
                    <td class="lblName">
                        EmailID
                        <%--&nbsp;<span style="color: Red">*</span>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtMailID" runat="server" TextMode="MultiLine" Width="300px" Rows="4"
                            Enabled="true"></asp:TextBox>
                    </td>
                </tr>--%>
                <tr>
                    <td colspan="4">
                        <div id="divEmailList" runat="server" visible="false" class="grid01">
                            <asp:GridView ID="grdEmpList" runat="server" AutoGenerateColumns="False" DataKeyNames="TokenNumber"
                                OnRowCommand="grdEmpList_RowCommand" Visible="true" AlternatingRowStyle-CssClass="row01"
                                AllowPaging="true" OnPageIndexChanging="grdEmpList_PageIndexChanging" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                                <Columns>
                                    <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" SortExpression="TokenNumber" />
                                    <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                    <asp:BoundField DataField="EmailID" HeaderText="EmailID" SortExpression="EmailID" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber") %>' CommandName="Select"
                                                runat="server" CssClass="selLink" TabIndex="4">
                                            Select</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="dataPager" />
                                <SelectedRowStyle CssClass="selectedoddTd" />
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <%--<tr id="trDeliveryAddress">
                    <td class="lblName">
                        Delivery Address</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtEmpDeliveryAddress" runat="server" TextMode="MultiLine" Width="300px"
                            Rows="4" TabIndex="5"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr id="trDeliveryPinCode">
                    <td class="lblName">
                        Pin Code</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtEmpPinCode" runat="server" ForeColor="Black" ></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>--%>
                <tr>
                    <td align="center" colspan="2">
                    </td>
                    <td align="center" colspan="2">
                        &nbsp; &nbsp;
                        <div class="btnRed">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="btnLeft" Text="Submit" OnClick="btnSubmit_Click"
                                OnClientClick="return ValidateOnSubmit()" TabIndex="6" /></div>
                        <div class="btnRed">
                            <asp:Button ID="btnReset" runat="server" CssClass="btnLeft" Text="Reset" OnClick="btnReset_Click"
                                TabIndex="7" /></div>
                        <div class="btnRed">
                            <%--<asp:Button ID="btnPreview" runat="server" CssClass="btnLeft" Text="Preview" OnClick="btnPreview_Click"
                                TabIndex="7" />--%></div>
                        &nbsp; &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4" height="5">
                    </td>
                </tr>
                <tr>
                    <td colspan="4" height="5">
                        <asp:HiddenField ID="hdnRecipientTokenNumber" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="HiddenBalance" runat="server" />
    <!--Done by Kiran on 02 july 2013-->
    <div style="" class="popupGreyBox" id="ptpgrey" runat="server">
    </div>
    <div style="" class="registerPopup" id="popupContainer" runat="server">
        <div class="popHead">
            <div class="hTitle" style="float: left;">
                Preview for Employee</div>
            <div class="closePop">
                <span></span>
            </div>
        </div>
        <div class="popList">
            <asp:Literal ID="ltPMessage" runat="server"></asp:Literal>
        </div>
    </div>
    <!--Done by Kiran on 02 july 2013-->
    <script type="text/javascript">
        //document.onload = hide();

        function hide() {
            //debugger
            if (document.getElementById) {
                nav1 = document.getElementById("div1");
                nav2 = document.getElementById("div2");
                //                nav1 = document.getElementsByTagName('div');
                //                nav2 = document.div2.style;
                nav1.style.display = 'block';
                nav2.style.display = 'none';
            }
        }
    </script>
</asp:Content>

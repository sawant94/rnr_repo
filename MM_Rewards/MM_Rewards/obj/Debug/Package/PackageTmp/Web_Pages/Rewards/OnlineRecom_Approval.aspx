﻿<%@ Page Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="OnlineRecom_Approval.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.OnlineRecom_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
<script type="text/javascript" language="javascript">
    function validateSave() {
        var errMsg = '';
        var txtRemarks = document.getElementById('<%=txtRemarks.ClientID %>');

        errMsg += CheckText_Blank(txtRemarks.value, 'Remarks')

        if (errMsg != '') {
            alert(errMsg);
            return false;
        }
        else {
            return true;
        }
    }

</script>
<h1>
        Online Employee Recommendation Approval
    </h1>
    <div class="grid01">
        <div style="overflow: auto; max-height: 380px;">
            <asp:GridView ID="grdEmpRecomm" runat="server" AllowPaging="true" PageSize='<%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                AutoGenerateColumns="false" DataKeyNames="ID" AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdEmpRecomm_PageIndexChanging" >
                <Columns>
                    <asp:BoundField DataField="ID" Visible="false" HeaderText="ID" SortExpression="" />
                    <asp:BoundField DataField="RequestorTokenNumber" HeaderText="Requestor HOD TokenNumber" SortExpression="" />
                    <asp:BoundField DataField="FullName" HeaderText="Requestor HOD Name" SortExpression="" />
                    <asp:BoundField DataField="TokenNumber" HeaderText="Recommended Employee TokenNumber" SortExpression="" />
                    <asp:BoundField DataField="RecommendedEmployee" HeaderText="Recommended Employee Name" SortExpression="" />
                    <asp:BoundField DataField="AwardType" HeaderText="Award Type" SortExpression="" />
                    <asp:BoundField DataField="AwardedDate" HeaderText="Recommended Awarded Date" SortExpression="AwardedDate"/>
                    <asp:BoundField DataField="RequestReason" HeaderText="Recommended Reason" SortExpression="" />
                    <%--<asp:BoundField DataField="IsCEHOD" HeaderText="Type" SortExpression="" />--%>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton CssClass="editLink" ID="lnkEdit" runat="server"  Text="Select" CommandName="Select"  OnClick="editLink_Click"/>
                             <asp:Label ID="lblFulltext" runat="server" Visible="false" Text='<%#Eval("RequestReason") %>'></asp:Label>
                             <asp:Label ID="lblAmount" runat="server" Visible="false" Text='<%#Eval("AwardAmount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1" BorderStyle="Solid" Font-Italic="true" />
                <EmptyDataTemplate>
                    <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                </EmptyDataTemplate>
                <SelectedRowStyle CssClass="selectedoddTd" />
            </asp:GridView>
        </div>
    </div>
    <div class="form01" id="divedit" visible="false" runat="server">
     <table>
      <tr>
            <td class="lblName">
                <asp:Label ID="lblReqHODTN" runat="server" Text="Requestor HOD TokenNumber"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtReqHODTN" runat="server"  BackColor="#FFFFCC" TabIndex="1" Enabled="false" Width="250px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td class="lblName">
                <asp:Label ID="lblReqHODName" runat="server"  Text="Requestor HOD Name"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtReqHODName" runat="server" BackColor="#FFFFCC" TabIndex="1" Enabled="false" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblName">
                <asp:Label ID="lblRecEmpTN" runat="server" Text="Recommended Employee TokenNumber"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtRecEmpTN" runat="server"  BackColor="#FFFFCC" TabIndex="1" Enabled="false" Width="250px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td class="lblName">
                <asp:Label ID="lblRecEmpName" runat="server"  Text="Recommended Employee Name"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtRecEmpName" runat="server" BackColor="#FFFFCC" TabIndex="1" Enabled="false" Width="250px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td class="lblName">
                <asp:Label ID="lblAwardType" runat="server"  Text="Award Type"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtAwardType" runat="server" BackColor="#FFFFCC" TabIndex="1" Enabled="false"  
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblName">
                <asp:Label ID="lblAwardAmt" runat="server"  Text="Award Amount"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtAwardAmt" runat="server" BackColor="#FFFFCC" TabIndex="1" Enabled="false"  
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblName">
                <asp:Label ID="lblReason" runat="server" Text="Reason"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtReason" runat="server"  BackColor="#FFFFCC" TabIndex="1" Enabled="false" TextMode="MultiLine"
                    Rows="5" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblName">
                <asp:Label ID="lblApproval" runat="server" Text="Approve / Reject"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:RadioButtonList ID="rdpproval" runat="server" AutoPostBack="true" 
                    RepeatDirection="Horizontal" Width="200px" 
                    CssClass="chkBox" >
                    <asp:ListItem Text="Approved" Value= "1" Selected="True" />
                    <asp:ListItem Text="Reject" Value= "0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lblName"></asp:Label>
                <span style="color: Red">*</span>
            </td>
            <td>
                :
            </td>
            <td>
                <asp:TextBox ID="txtRemarks" runat="server" MaxLength="150" TextMode="MultiLine"
                    Rows="5" Width="250px" TabIndex="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td>
                <div class="btnRed">
                    <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btnLeft" 
                        TabIndex="4" onclick="btnsave_Click" OnClientClick="return validateSave()" />
                </div>
                <div class="btnRed">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft"
                        TabIndex="5" onclick="btnCancel_Click" />
                </div>
            </td>
        </tr>
        <tr>
                <td>
                    <asp:HiddenField ID="hdnid" runat="server" />
                </td>
            </tr>
    </table>
    </div>

</asp:Content>
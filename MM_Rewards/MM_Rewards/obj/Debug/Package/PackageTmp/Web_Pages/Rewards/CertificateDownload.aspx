﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="CertificateDownload.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.CertificateDownload" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <style>
        div.grid01 table th, div.listDtls table th {
            /*padding: 5px 5px;*/
            white-space: normal;
        }

        .selectDivision option {
            max-height: 200px;
            overflow-y: auto;
        }

        .budgetCalModal {
            top: 10%;
        }

            .budgetCalModal .modal-header {
                background-color: #e31837;
                padding: 8px 15px;
            }

                .budgetCalModal .modal-header label {
                    color: #fff;
                    margin-bottom: 0;
                }

                .budgetCalModal .modal-header .closemodal input {
                    background: transparent;
                    border: none;
                    color: #fff;
                }

        .tblhodbudget {
            width: 100%;
        }

            .tblhodbudget tr td {
                padding: 5px 10px;
            }

                .tblhodbudget tr td:first-child {
                    width: 150px;
                }

                .tblhodbudget tr td input {
                    width: 100%;
                }

        #ContentPlaceHolder1_BudgetUploadedID {
            text-align: center;
            padding: 20px 0;
            border: 2px solid #272727;
            border-radius: 10px;
            margin-top: 50px;
            color: #e31837;
        }

            #ContentPlaceHolder1_BudgetUploadedID h4 {
                font-size: 18px;
                font-weight: bold;
                font-family: calibri;
            }
        .table-scroll th, .table-scroll td {
            /*padding: 5px;*/
            white-space: normal;
        }
        td input.rnr-btn {
            font-size: 12px;

        }
        .table-scroll {
            height: 320px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <div id="TblID" style="display: block" runat="server">
        <table style="width: 100%;">
            <tr>
                <td style="padding: 5px;" class="auto-style1">Award Type:
        
                </td>
                <td style="padding: 5px;"></td>
                <td style="padding: 5px;"></td>
            </tr>
        </table>
    </div>


    <section class="main-rnr">
        <div class="rnr-inner-top" style="">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href="/Web_Pages/HomePage.aspx">HOME</a></li>
                    <%--<li><a href='index-user.html'>HOME</a></li>--%>
                    <!--   <li><a href="#">Awards</a></li> -->
                    <li>Certificate Download</li>
                </ul>

            </div>
        </div>

        <div class="rnr-tabs" style="margin-top: -40px;">

            <h3 class="rnr-h3-top">Certificate Download</h3>


            <div class="rnr-tab-pane tab-content">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                        <label for="">Select Type</label>
                                        <asp:DropDownList ID="ddlAwardType" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="Excellence">Excellence</asp:ListItem>
                                            <%--<asp:ListItem Value="Quarter">Quarterly Divisional Excellence Award</asp:ListItem>--%>
                                            <asp:ListItem Value="ECard">Ecard</asp:ListItem>
                                            <asp:ListItem Value="Spot">Spot</asp:ListItem>
                                            <asp:ListItem Value="Excellerator">Excellerator</asp:ListItem>
                                            <%--<asp:ListItem Value="MileStone">MileStone</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="Silverstars">Silverstars</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-6" style="">
                                    <asp:Button ID="btnShow" runat="server" Text="Show" Width="200px" CssClass="rnr-btn" OnClick="btnShow_Click" />
                                </div>

                                <div class="rnr-table">
                                    <div id="Div1" class="table-scroll">
                                        <div class="grid01" style="background-color: transparent; width: 100%; overflow-x: auto;" id="AwardDetail" runat="server">
                                            <%--<asp:Label ID="gridtitle" runat="server" Text="Download Certificate"></asp:Label>--%>
                                            <asp:GridView ID="grdCertifiate" runat="server" AllowSorting="true" AllowPaging="true"
                                                AutoGenerateColumns="false" AlternatingRowStyle-CssClass="row01" Style="width: 100%; margin: 0 auto;" CssClass="grdAwards_new"
                                                PageSize=' <%#Convert.ToInt32(10)%>' OnRowCommand="grdCertifiate_RowCommand" OnRowEditing="grdCertifiate_RowEditing" OnPageIndexChanging="grdCertifiate_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="true" Visible="true" ItemStyle-Width="10px"></asp:BoundField>
                                                    <asp:BoundField DataField="Name" HeaderText="AwardTo" ReadOnly="true" Visible="true" ItemStyle-Width="100px"></asp:BoundField>
                                                    <asp:BoundField DataField="GiverTokenNumber" HeaderText="AwardBy" ReadOnly="true" Visible="true" ItemStyle-Width="50px"></asp:BoundField>
     <%--                                               <asp:BoundField DataField="AwardID" HeaderText="Award" ReadOnly="true" Visible="false" ItemStyle-Width="150px"></asp:BoundField>--%>
                                                    <asp:BoundField DataField="AwardType" HeaderText="Type" ReadOnly="true" Visible="true" ItemStyle-Width="100px"></asp:BoundField>
                                                    <asp:BoundField DataField="AwardAmount" HeaderText="Amount" ReadOnly="true" ItemStyle-Width="100px"></asp:BoundField>
                                                    <asp:BoundField DataField="AwardedDate" HeaderText="Awarded Date" ReadOnly="true" Visible="true" ItemStyle-Width="100px"></asp:BoundField>
                                                    <asp:BoundField DataField="ReasonForNomination" HeaderText="Reason" ReadOnly="true" Visible="true" ItemStyle-Width="300px"></asp:BoundField>
                                                     <asp:TemplateField ItemStyle-Width="100px" Visible="false">
                                                        <HeaderTemplate>Image</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblAwardImageName" runat="server" Text='<%#Bind("ImageURL") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                                        <HeaderTemplate>Download</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDownload" runat="server" Text="Download" Width="80px" CssClass="rnr-btn"
                                                                CommandArgument='<%# Bind("ID") %>' CommandName="Edit" />
                                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("RecipientTokenNumber")%>' />
                                                            <asp:HiddenField ID="HiddenField2" runat="server" Value='<%#Eval("GiverTokenNumber")%>' />
                                                            <asp:HiddenField ID="HiddenField3" runat="server" Value='<%#Eval("AwardType")%>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                    BorderStyle="Solid" Font-Italic="true" />
                                                <EmptyDataTemplate>
                                                    <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                                                </EmptyDataTemplate>
                                                <PagerStyle CssClass="dataPager" />
                                                <SelectedRowStyle CssClass="selectedoddTd" />
                                            </asp:GridView>
                                        </div>
                                        <div class="grid01" style="background-color: transparent; width: 100%; overflow-x: auto;" id="AwardEcard" runat="server">
                                            <asp:GridView ID="grdEcard" runat="server" AllowSorting="true" AllowPaging="true"
                                                AutoGenerateColumns="false" AlternatingRowStyle-CssClass="row01" Style="width: 100%; margin: 0 auto;" CssClass="grdAwards_new"
                                                PageSize=' <%#Convert.ToInt32(10)%>' OnRowCommand="grdEcard_RowCommand" OnRowEditing="grdEcard_RowEditing" OnPageIndexChanging="grdEcard_PageIndexChanging" >
                                                <Columns>
                                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="true" Visible="true" ItemStyle-Width="20px"></asp:BoundField>
                                                    <asp:BoundField DataField="Name" HeaderText="Award To" ReadOnly="true" Visible="true" ItemStyle-Width="100px"></asp:BoundField>
                                                    <asp:BoundField DataField="GiverTokenNumber" HeaderText="Award By" ReadOnly="true" Visible="true" ItemStyle-Width="50px"></asp:BoundField>
                                                    <asp:BoundField DataField="Message" HeaderText="Reason" ReadOnly="true" Visible="true" ItemStyle-Width="150px"></asp:BoundField>
                                                    <asp:BoundField DataField="CardType" HeaderText="Type" ReadOnly="true" Visible="true" ItemStyle-Width="20px"></asp:BoundField>
                                                    <asp:BoundField DataField="DateTime" HeaderText="Award On" ReadOnly="true" ItemStyle-Width="100px"></asp:BoundField>
                                                     <asp:TemplateField ItemStyle-Width="100px" Visible="false">
                                                        <HeaderTemplate>Image</HeaderTemplate>
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblCardImageName" runat="server" Text='<%#Bind("ImageURL") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                                        <HeaderTemplate>Download</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDownloadEcard" runat="server" Text="Download" Width="80px" CssClass="rnr-btn"
                                                                CommandArgument='<%# Bind("ID") %>' CommandName="EditEcard" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                                                    BorderStyle="Solid" Font-Italic="true" />
                                                <EmptyDataTemplate>
                                                    <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                                                </EmptyDataTemplate>
                                                <PagerStyle CssClass="dataPager" />
                                                <SelectedRowStyle CssClass="selectedoddTd" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>


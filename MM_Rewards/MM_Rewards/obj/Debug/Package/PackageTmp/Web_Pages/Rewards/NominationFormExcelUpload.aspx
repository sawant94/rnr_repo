﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="NominationFormExcelUpload.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.NominationFormExcelUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 0;
        }

        div.pgData {
            padding: 0;
        }
    </style>

    <style>
        div.form01 table td input {
            margin-right: 0;
        }

        input.btnReset.testi_cancel {
            color: #333;
            background: transparent!important;
        }

        .mahindra_drop_down_div.rnr-dd label.fl_label {
            position: relative;
            top: 0;
            left: 0;
        }
    </style>
    <section class="main-rnr">
        <div class="rnr-inner-top">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href='/Web_Pages/HomePage.aspx'>HOME</a></li>

                    <li>Upload Nominee</li>
                </ul>

            </div>
        </div>

        <div class="rnr-tabs">
            <h3 class="rnr-h3-top">Upload Nominee</h3>


            <div class="rnr-tab-pane tab-content Feedback-wrap">
                <div class="tab-pane Feedback-inn">
                    <div class="container">
                        <div class="row rnr-custom-row">
                            <div class="col-md-6">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd file-attch" style="width: 100%;">
                                    <label for="">Upload Nominee Excel</label>
                                    <div class="file-upload">
                                        <asp:FileUpload ID="flupload" runat="server" />

                                        <br />
                                        <br /> 
                                        <br />                                       
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-6">
                                        <asp:Button ID="Button1" Text="Upload" OnClick="Upload" runat="server" CssClass="rnr-btn" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3">
                                        <asp:Button ID="Button2" Text="Template Download" OnClick="TemplateDownload_Click" runat="server" CssClass="rnr-btn"/>
                            </div>
                            <div class="col-md-3">
                                        <asp:Button ID="Button5" Text="Final Upload" OnClick="FinalUpload_Click" runat="server"  CssClass="rnr-btn"/>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/Web_Pages/Rewards/Copy of GiveECard.aspx.cs" Inherits="MM_Rewards.Web_Pages.Rewards.CopyGiveECard"
    MasterPageFile="~/Web_Pages/MM_Rewards.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <style>
        .modal {
            display: block; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            padding-top: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.8); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            width: 45%;
            position: relative;
            overflow: hidden;
        }

        .modal_inner {
            height: 480px;
            overflow-y: auto;
            padding: 10px 20px;
        }

        /* The Close Button */
        .closemodal {
            color: #fff;
            float: right;
            font-size: 24px;
            font-weight: bold;
            position: absolute;
            right: 15px;
            top: 0;
        }

            .closemodal:hover,
            .closemodal:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        div.form01 table.grid_new td {
            padding: 3px 10px;
        }
        #ContentPlaceHolder1_tblRecipientList {
            width: 100%;
        }

        #ContentPlaceHolder1_tblRecipientList td input {
            border-bottom: 1px solid #272727;
            font-family: 'Conv_EUROSTILELTSTD' !important;
            width: 100%;
        }

        .chkBox input[type=radio] {
            float: none;
        }

        .chkBox label {
            vertical-align: middle;
        }

         .chkBox input[type=radio] {
                float: none;
                margin-right: 0;
                opacity: 0;
            }


        #ContentPlaceHolder1_radListCardType  tr{
            display: inline-block;
            margin-right: 15px;
        }

        .chkBox input[type=radio] + label {
            background-image:url(../../images/new_images/radiornr.png);
            background-repeat: no-repeat;
            background-size: 17px;
            background-position: left 0;
            padding-left: 30px;
            cursor: pointer;
            margin-left: -22px;
        }
        .chkBox input[type=radio]:checked + label {
            background-position: left -28px;
        }

        @media screen and (max-width: 767px) {
            div.form01 table td {
                padding: 5px 0;
                font-size: 14px;
            }
            .chkBox input[type=radio] + label {
                background-size: 19px;
                margin-left: -19px;
            }
            .chkBox input[type=radio]:checked + label {
                background-position: left -24px;
            }
            .modal_inner {
                padding: 10px 0;
                height: 460px !important;
            }
            .modal {
                padding-top: 10px;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            div.navHold {
                z-index: 1;
            }
            .modal-content {
                width: 60%;
            }
        }

        @media screen and (min-width: 1920px) {
            div.form01 table td {
                padding: 10px 10px;
                font-size: 16px;
            }
        }
    </style>

   
  
    <script src="../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script>
        function ClosePopup() {
            $('.modal').css('display', 'none');
        }
    </script>

    <style>
.loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}


</style>
    
   
    <div class="loader" style="display:none" id="loderid"></div>
    <div class="form01 formformob1">
        <h1>Select Employee to Send E-Card</h1>
        <table class="tblblock">
            <tr>
                <td class="lblName">
                    Select E-Card Type<span style="color: #e31837;">*</span>
                </td>
                <td class="hidden-xs">:</td>
                <td>
                    <asp:HiddenField ID="hdnGiverName" runat="server" />
                    <asp:RadioButtonList ID="radListCardType" runat="server" TabIndex="1" CssClass="chkBox">
                        <asp:ListItem Value="ANL">Accepting No Limits</asp:ListItem>
                        <asp:ListItem Value="AT">Alternative Thinking</asp:ListItem>
                        <asp:ListItem Value="DPC">Driving Positive Change</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Enter Name / Token No.<span style="color: #e31837;">*</span>
                </td>
                <td class="hidden-xs">:</td>
                <td>
                    <asp:TextBox ID="txtTokenNumberTop" runat="server" MaxLength="100" TabIndex="2"></asp:TextBox>
                    
                </td>
            </tr>
            <tr>
                <td class="hidden-mob">&nbsp;</td>
                <td class="hidden-mob hidden-xs">&nbsp;</td>
                <td>
                    
                </td>
            </tr>
        </table>

        

        <div>
            <asp:Label runat="server" ID="lblGetEmpResponse" ForeColor="#e31837"></asp:Label>
        </div>

        <div runat="server" id="myModal" class="modal">
            <div class="modal-content">
                <label style="padding: 10px 15px; background-color: #e31837; color: #fff; display: block; font-size: 16px;">Award Redemption</label>
                <span onclick="ClosePopup();" class="closemodal">&times;</span>
                <div class="modal_inner">
                    <table id="tblRecipientList" runat="server" class="grid_new">
                     
                        <tr>
                            <td colspan="3" style="">
                                <div class="btnRed">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btnLeft" Text="Send E Card"
                                        OnClick="btnSubmit_Click"  TabIndex="6" />

                                     <asp:Button ID="btnSend" runat="server" CssClass="btn btnLeft" Text="Send E Award"
                                        OnClick="btnSend_Click"  TabIndex="6" />
                                </div>
                               
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

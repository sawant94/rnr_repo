﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="MM_Rewards.Web_Pages.FAQ" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../includes/css/master.css" rel="stylesheet" type="text/css" />
    <style>
        body {
            overflow: auto;
        }
    </style>
 </head>
<body style= "background-color:White;">
<form id="form1" runat="server">
  <div class='faqData'>
  <h1>FAQs</h1>
  <div class="faqList">
    <h2>Index</h2>
    <ul>
      <li><a href="#budget">Budget</a></li>
      <li><a href="#awardCrite">Award Eligibility Criteria (Giver & Receiver)</a></li>
      <li><a href="#procAward">Process for award allocation</a></li>
      <li><a href="#excelAward">Excellerator Award</a></li>
      <li><a href="#spotAward">Spot Award</a></li>
      <li><a href="#mileAward">Milestone Award</a></li>
      <li><a href="#smileCard">Smileys</a></li>
      <li><a href="#appCard">Appreciation Cards</a>
        <ul>
          <li><a href="#appCard">Hard Copy</a></li>
          <li><a href="#appCard">E-card</a></li>
        </ul>
      </li>
      <li><a href="#awardRed">Redemption</a>
        <ul>
          <li><a href="#awardRed">Online</a></li>
          <li><a href="#awardRed">Salary</a></li>
        </ul>
      </li>
    </ul>
  </div>
  
  <div class="listDtls">
    <h3 id="budget">Budget</h3>
    <ol>
      <li>
      	<h4>The HOD is allotted budget for which awards?</h4>
        
        The HOD’s are allotted budget to give the Excellerator awards & Spot awards.
      </li>
      
      <li><h4>How has the HOD budget being allocated?</h4>
        The budget has been calculated on the basis of<br />
        - Number of reportees every HOD has<br />
        - The eligibility of the HOD to give the award<br />
        - Eg Rs X * No of Reportees<br />
      </li>
      <li><h4>If the budget of the HOD gets exhausted then what can he do?</h4>
        Ideally budget allotted to the HOD should not be exhausted, in case if the budget is exhausted then he can recommend his HOD to allocate the award from his budget. </li>
    </ol>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="awardCrite">Award Eligibility Criteria</h3>
    	
    <table>
      <tbody>
      	 <tr>
          <th colspan="8" ><strong>Giver</strong></th>
        </tr>
        <tr>
          <td colspan="8" class="subHead"><strong>Excellerator</strong></td>
        </tr>
        <tr class="row01">
          <td><strong>Band/Level</strong></td>
          <td ><strong>L1ST</strong></td>
          <td ><strong>L2ST</strong></td>
          <td ><strong>L2EX</strong></td>
          <td ><strong>L3EX</strong></td>
          <td ><strong>L3DH</strong></td>
          <td ><strong>L4DH</strong></td>
          <td ><strong>L5DH</strong></td>
        </tr>
        <tr>
        <td></td>
          <td >EVP/CE</td>
          <td>Sr VP</td>
          <td>VP</td>
          <td>Sr GM</td>
          <td>GM</td>
          <td>DGM</td>
          <td>DGM</td>
        </tr>
        <tr class="row01">

          <td><strong>500</strong></td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
        </tr>
        <tr>
          <td><strong>750</strong></td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>No</td>
        </tr>
        <tr class="row01">
          <td><strong>1000</strong></td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>No</td>
          <td>No</td>
          <td>No</td>
          <td>No</td>
        </tr>
        <tr>
          <td colspan="8" class="subHead"><strong>Spot Award</strong></td>
        </tr>
        <tr class="row01">
          <td ><strong>Band/Level</strong></td>
          <td ><strong>L1ST</strong></td>
          <td ><strong>L2ST</strong></td>
          <td ><strong>L2EX</strong></td>
          <td ><strong>L3EX</strong></td>
          <td ><strong>L3DH</strong></td>
          <td ><strong>L4DH</strong></td>
          <td ><strong>L5DH</strong></td>
        </tr>
        <tr>
        <td></td>
          <td>EVP/CE</td>
          <td>Sr VP</td>
          <td>VP</td>
          <td>Sr GM</td>
          <td>GM</td>
          <td>DGM</td>
          <td>DGM</td>
        </tr>
        <tr class="row01">
          <td><strong>500</strong></td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
        </tr>
        <tr>
          <td><strong>1000</strong></td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
        </tr>
        <tr class="row01">
          <td><strong>1500</strong></td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
        </tr>
        <tr>
          <td><strong>2000</strong></td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
          <td>Yes</td>
        </tr>
        </tbody>
        </table>
        
    <table>
        <tbody>
        <tr>
          <th colspan="8" >Receiver</th>
        </tr>
        <tr class="row01">
          <td colspan="8" >All AFS probationary & permanent officers L4DH & below are eligible to receive awards.</td>
        </tr>
      </tbody>
    </table>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="procAward">Process for award allocation</h3>
    <ol>
      <li>
        <h4>Excellerator Award</h4>
        <p><strong>500 BHP/HP</strong></p>
        <p>Recipient to be awarded Rs. 5000/- (Rupees Five Thousand only) with an option for online redemption points or cash through salary. </p>
        <p>Every Department Head (not below the level of DHL5) is assigned an individual budget from where he can disburse this award. The nomination is made by reporting manager and approved by Department Head (not below the level of DHL5)</p>
        <p> Concerned Department Head to raise an entry in the R&R online module and will issue a duly signed letter to the employee. </p>
        <p> Three days after the DH has raised the entry the employee gets an auto generated mail wherein he gets an option for online redemption points or cash through salary. </p>
        <p><strong>750 BHP/HP</strong></p>
        <p> Recipient to be awarded Rs. 15000/- (Rupees Fifteen Thousand only) cash through salary. </p>
        <p> Every Functional Head/ Plant Head (not below the level of DHL4) is assigned an individual budget from where he can disburse this award. The nomination is made by reporting manager and approved by Functional Head/ Plant Head (not below the level of DHL4) </p>
        <p>Concerned Functional Head/ Plant Head to raise an entry in the R&R online module and will issue a letter duly signed to the employee. </p>
        <p>Subsequently the employee will get the cash through salary.</p>
        <p><strong>1000 BHP/ HP</strong></p>
        <p>Recipient to be awarded Rs. 25000/- (Rupees Twenty Five Thousand only) cash through salary. </p>
        <p>Every Sr. Vice President/ Vice President (not below the level of EXL2) is assigned a budget from where he can disburse this award. The nomination is made by reporting manager and approved by Sr. Vice President/ Vice President (not below the level of EXL2). The nomination for 1000 BHP/HP should be intimated to their respective Chief Executive.</p>
        <p>Concerned Sr. Vice President/ Vice President will raise an entry in the R&R online module and will issue a letter duly signed to the employee. </p>
        <p>Subsequently the employee will get the cash through salary.</p>
      </li>
      <li>
        <h4>Spot Award</h4>
        <p>Every Department Head (not below the level of DHL5) is assigned a budget from where he can disburse this award. The nomination will be made by reporting manager and approved by Department Head (not below the level of DHL5) </p>
        <p>Concerned Department Head will raise an entry in the R&R online module.</p>
        <p>Department Head will issue a duly signed card to the employee mentioning his unique id. </p>
        <p>Three days after the DH has raised the entry the employee will get an auto generated mail wherein he will get an option for online redemption points or cash through salary. </p>
      </li>
      <li>
        <h4>Milestone Award</h4>
        <p>Officers completing 5, 10, 15 and 20 years of continuous service in the financial year shall receive Rs 10000/- each which can be redeemed online.</p>
        <p>The Department Head shall issue a duly signed letter to the employee on the day of completion of 5, 10, 15 and 20 years of service tenure.</p>
        <p>On the same day a letter of appreciation will be sent to his residential address.</p>
        <p>Subsequently the employee can go online and redeem the reward amount.</p>
      </li>
    </ol>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="excelAward">Excellerator Award</h3>
    <ol>
      <li>
        <h4>Can a HOD give awards to any other employee other than his own department/process?</h4>
        <p>The HOD can give awards to any AFS permanent or probationer employee even though he is not from his department/process. E.g. CFT members</p>
      </li>
      <li>
        <h4>How will Process HR get to know about any award given by L4DH/HOD?</h4>
        <p>There is a R&R coordinator for each division. If the process HR needs any details about the budget/award they can get in touch with their R&R coordinator.</p>
      </li>
      <li>
        <h4>Will the Excellerator award amount subject to Income Tax?</h4>
        <p>The entire award amounts whether redeemed in cash or kind is subject to Income Tax.</p>
      </li>
      <li>
        <h4>Are there a Min-Max number of awards to be given every quarter?</h4>
        <p>There is no specific number of awards to be given every quarter/ year. However the budget allotted to the HODs is for an entire financial year. </p>
      </li>
      <li>
        <h4>Is the budget separate for Excellerator Awards & Spot Awards?</h4>
        <p>The budget allotted to the HOD on the R&R portal is combined for both Excellerator Awards & Spot Awards.</p>
      </li>
    </ol>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="spotAward">Spot Award</h3>
    <ol>
      <li>
        <h4>When should the Spot card be given?</h4>
        <p>Once the HOD has given the award within he should give the employee the Spot card.</p>
      </li>
      <li>
        <h4>Can a HOD give Spot award to any other employee other than his own department/process?</h4>
        <p>The HOD can give awards to any AFS permanent or probationer employee even though he is not from his department/process. E.g. CFT members</p>
      </li>
      <li>
        <h4>Will the Spot award amount subject to Income Tax?</h4>
        <p>All the award amounts whether redeemed in cash or kind are subject to Income Tax.</p>
      </li>
      <li>
        <h4>Is the budget separate for Excellerator Awards & Spot Awards?</h4>
        <p>The budget allotted to the HOD on the R&R portal is combined for both Excellerator Awards & Spot Awards. </p>
      </li>
    </ol>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="mileAward">Milestone Award</h3>
    <ol>
      <li>
        <h4>Since what date has the Milestone Awards scheme bought into effect?</h4>
        <p>The Milestone Awards have been bought into effect from 1st November 2011. All the employees who complete their 5, 10, 15 & 20 yrs of service on or after this date will be eligible for the award.</p>
      </li>
      <li>
        <h4>How will an HOD get to know that his employee is due for Milestone award?</h4>
        <p>If an employee is due for Milestone Award then the HOD will get a mail from BenefitsPlus(an external vendor) asking him for his personalized message for the employee.</p>
      </li>
      <li>
        <h4>If the HODs Spot cards/ Appreciation cards are exhausted then who shall he contact?</h4>
        <p>If the HODs Spot cards/ Appreciation cards are exhausted then he should contact his divisional R&R coordinator or the process HR.</p>
      </li>
    </ol>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="smileCard">Smileys</h3>
    <ol>
      <li>
        <h4>What is the eligibility of giving smileys?</h4>
        <p>Smileys can be given by any officer across any function</p>
      </li>
      <li>
        <h4>Is there any connotation for the three colours of smileys?</h4>
        <p>The smileys depict the three Rise colours. They do not have any connotation attached to it.</p>
      </li>
      <li>
        <h4>Where can the employees get the smileys?</h4>
        <p>All the HODs (L5DH & above) are given a R&R kit which consist of the smileys. Similarly the smileys are also available with the R&R co-ordinator/ process HR.</p>
      </li>
    </ol>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="appCard">Appreciation Cards</h3>
    <ol>
      <li>
        <h4>What is the eligibility of giving Appreciation cards?</h4>
        <p>All the HODs (L5DH & above) can give the Appreciation cards both hard copy & the e-card.</p>
      </li>
      <li>
        <h4>Whom should the HOD contact in case his stock of appreciation cards gets over?</h4>
        <p>In case the stock of appreciation cards get over the HOD can get in touch with his divisional R&R co-ordinator/ process HR.</p>
      </li>
      <li>
        <h4>What is the purpose of giving appreciation card?</h4>
        <p>The appreciation card can be given to instantaneously appreciate positive behaviours, efforts & contributions.</p>
      </li>
    </ol>
    
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  
  <div class="listDtls">
    <h3 id="awardRed">Award Redemption</h3>
    <table>
      <tbody>
        <tr>
          <th colspan="2"> Award Redemption Type</th>
        </tr>
        <tr class="row01">
          <td>Spot Award (500/ 1000/ 1500/ 2000)</td>
          <td>Cash (salary) or Kind (e-shopping)</td>
        </tr>
        <tr>
          <td>Excellerator 500 BHP/HP</td>
          <td>Cash (salary) or Kind (e-shopping)</td>
        </tr>
        <tr class="row01">
          <td>Excellerator 750 BHP/HP</td>
          <td>Only Cash (salary)</td>
        </tr>
        <tr>
          <td>Excellerator 1000 BHP/HP</td>
          <td>Only Cash (salary)</td>
        </tr>
        <tr class="row01">
          <td>Milestone Award</td>
          <td>Only Kind (e-shopping)</td>
        </tr>
      </tbody>
    </table>
    <ol>
      <li>
        <h4>If an employee shops for lesser/greater that the award amount, what will happen?</h4>
        <p>If the employee shops for lesser than the award amount then the remaining points will be lapsed.</p>
        <p>If the employee shops for greater than the award amount then the employee has to pay the additional amount by himself </p>
      </li>
      <li>
        <h4>Is there any time period within which the award should be redeemed?</h4>
        <p>The award should be redeemed within three months of receiving the award or it will lapse.</p>
      </li>
      <li>
        <h4>When will the employee get the award amount if he chooses the cash option?</h4>
        <p>Once the employee chooses the cash option the award amount will be credited through his salary in the next month.</p>
      </li>
      <li>
        <h4>What is process for redeeming the award in kind?</h4>
        <p>Once the employee chooses the kind option, he will receive an id and password from an external vendor. Then the employee can visit the online portal and choose a product for the points allotted to him. In a week’s time of buying the product the employee will receive the product at his given address.</p>
      </li>
    </ol>
    <div class="topBtn"><a href="#" class="topLink">top</a></div>
  </div>
  </div>
</form>
</body>
</html>

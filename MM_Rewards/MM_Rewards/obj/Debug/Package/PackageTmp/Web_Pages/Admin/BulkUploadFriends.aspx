﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="BulkUploadFriends.aspx.cs" Inherits="MM_Rewards.BulkUploadFriends" %>

<%@ Register Src="~/UserControls/BulkUploadFriends.ascx" TagPrefix="uc1" TagName="BulkUploadFriends" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
<style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:BulkUploadFriends runat="server" id="ucBulkUploadFriends" />
</asp:Content>

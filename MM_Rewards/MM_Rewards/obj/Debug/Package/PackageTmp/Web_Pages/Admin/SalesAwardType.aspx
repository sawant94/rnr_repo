﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_RewardsSales.Master"
    AutoEventWireup="true" CodeBehind="SalesAwardType.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.SalesAwardType"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
     <script type="text/javascript" language="javascript">
        function validateSave() {
            var errMsg = '';
            var txtAwardName = document.getElementById('<%=txtAwardName.ClientID %>');
            errMsg += CheckText_Blank(txtAwardName.value, 'AwardName');

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }       
    </script>
    <h1>
        Sales Award Type Management</h1>
    <div class="grid01">
        <asp:HiddenField ID="hdnSelectedRowIndex" runat="server" />
        <asp:GridView ID="grdAwardList" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdAwardList_RowDataBound"
            DataKeyNames="ID" AlternatingRowStyle-CssClass="row01">
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                <asp:BoundField DataField="AwardName" HeaderText="Award Type" />
                <asp:BoundField DataField="IsActive" HeaderText="Active" />
                <asp:BoundField DataField="IsbudgetTracking" HeaderText="Is Budget Award" HeaderStyle-Width="150" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("ID")  %>' CommandName="Select"
                            OnClick="lnkSelect_Click" runat="server" CssClass="selLink" TabIndex="1">
                                            Select</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="dataPager" />
            <SelectedRowStyle CssClass="selectedoddTd" />
        </asp:GridView>
    </div>
    <div class="form01">
        <table>
            <tr>
                <td class="lblName" style="text-align: right">
                    <asp:Label ID="lblAwardName" runat="server" Text="Enter Sales Award Type"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtAwardName" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="lblactive" runat="server" Text="Is Active" CssClass="lblName"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:CheckBox ID="chkActive" CssClass="chkBox" runat="server" Checked="true" TabIndex="3">
                    </asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="lblbugdetAward" runat="server" Text="Is Bugdet Award" CssClass="lblName"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:CheckBox ID="chkbugdetaward" CssClass="chkBox" runat="server" TabIndex="3">
                    </asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="style1">
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnsaveAwardname" runat="server" Text="Save" CssClass="btnLeft" OnClick="btnsaveAwardname_Click"
                            OnClientClick="return validateSave()" TabIndex="7" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" OnClientClick="return ClearFields()"
                            OnClick="btnCancel_Click" TabIndex="8" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:HiddenField ID="hdnid" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PlaceholderAddtionalPageHead">
    <style type="text/css">
        .style1
        {
            text-align: right;
        }
    </style>
</asp:Content>

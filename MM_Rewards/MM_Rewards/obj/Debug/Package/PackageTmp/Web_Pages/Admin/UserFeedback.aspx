﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="UserFeedback.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.User_Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 34px;
        }
        div.pgData {
            padding: 0 0px;
        }
    </style>
    <style>
        div.form01 table td {
            font-size: 16px;
        }

        .showRadio input[type=radio] {
            float: none;
            margin-right: 0;
            opacity: 0;
        }

        #ContentPlaceHolder1_rdbfeedbacktype tr {
            display: inline-block;
            margin-right: 15px;
        }

        .showRadio input[type=radio] + label {
            background-image: url(../../images/new_images/radiornr.png);
            background-repeat: no-repeat;
            background-size: 22px;
            background-position: left 0;
            padding-left: 30px;
            cursor: pointer;
            margin-left: -22px;
        }

        .showRadio input[type=radio]:checked + label {
            background-position: left -28px;
        }

        @media screen and (max-width: 767px) {

            div.form01 table td {
                padding: 5px 10px;
            }

            div.innerHold {
                height: 95vh;
            }

            .showRadio input[type=radio] + label {
                background-size: 19px;
                margin-left: -19px;
            }

            .showRadio input[type=radio]:checked + label {
                background-position: left -24px;
            }
            .rnr-radio-btn .radio table tr {
                display: table!important;
            }
        }

        input.btnReset.testi_cancel {
            color: #333;
            background: transparent!important;
        }

        .rnr-radio-btn .radio span {
            position: relative;
        }

        .rnr-radio-btn table tr td span::after {
            content: attr(data-value);
            position: absolute;
            width: 153px;
            left: 30px;
        }
        section.main-rnr {
            margin-top: 26px;
        }
        @media screen and (max-width: 1113px) {
            section.main-rnr {
                margin-top: 10px!important;
            }
        }
    </style>
    <script type="text/javascript">

        function ValidatePage() {

            var r = confirm("Do you want to submit Your Feedback ?");
            //alert(r);
            if (r != true) {
                return false;
            }


            var errMsg = '';
            var vddl;

            vddl = document.getElementById('<%=txtFeedbackDetail.ClientID %>');
            errMsg += CheckText_Blank(vddl.value, "Feedback Details");
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <%--<div class="form01 formformobonly formuserfeedback">
        <h1>User FeedBack</h1>
        <table>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblTokenNumber" runat="server" Text="Token Number"></asp:Label><span style="color: #e31837;">*</span>
                </td>
                <td class="hidden-mob">:
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblFeedback" runat="server" Text="Feedback Type"></asp:Label><span style="color: #e31837;">*</span>
                </td>
                <td class="hidden-mob">:
                </td>
                <td class="showRadio"></td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDetails" runat="server" Text="Feedback Details"></asp:Label><span style="color: #e31837;">*</span>
                </td>
                <td class="hidden-mob">:
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="hidden-mob"></td>
                <td class="hidden-mob"></td>
                <td>
                    <div class="btnRed">
                    </div>
                    <div class="btnRed">
                    </div>
                </td>
            </tr>
        </table>
    </div>--%>

    <section class="main-rnr">
        <div class="rnr-inner-top">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href='/Web_Pages/HomePage.aspx'>HOME</a></li>

                    <li>Feedback</li>
                </ul>

            </div>
        </div>

        <div class="rnr-tabs">
            <h3 class="rnr-h3-top">Feedback</h3>


            <div class="rnr-tab-pane tab-content Feedback-wrap">
                <div class="tab-pane Feedback-inn">
                    <div class="container">
                        <div class="row rnr-custom-row">
                            <div class="col-md-12">

                                <div class="rnr-input search-emp-m">
                                    <div class="search-emp">
                                        <label for="">Search Employee</label>
                                        <asp:TextBox ID="txtTokenNumber" runat="server" Enabled="false"></asp:TextBox>
                                        <%--<input type="text" name="" value="" placeholder="Enter Name / Token Number">--%>
                                        <!-- <p>Enter Name / Token Number</p> -->

                                    </div>
                                </div>


                                <div class="nominee-name">


                                    <div class="rnr-radio-btn">
                                        <div class="radio direct-reportee">
                                            <asp:RadioButtonList ID="rdbfeedbacktype" CssClass="chkBox" runat="server" TabIndex="1" ToolTip="Select type of feedback">
                                                <asp:ListItem Value="K" Selected="True" data-value="Positive Feedback">Positive Feedback</asp:ListItem>
                                                <asp:ListItem Value="I" data-value="Scope for Improvement">Scope for Improvement</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <%--<input type="radio" id="radio-1" name="example" checked/>
                            <label for="radio-1"></label>
                            <span>Positive Feedback</span>--%>
                                        </div>
                                        <%--<div class="radio other-recipent">
                            <input type="radio" id="radio-2" name="example"/>
                            <label for="radio-2"></label>
                            <span>Scope For Improvement</span>
                        </div>--%>
                                    </div>



                                    <div class="rnr-input">
                                        <label for="">Feedback details</label>
                                        <asp:TextBox ID="txtFeedbackDetail" runat="server" TextMode="MultiLine" Rows="5" TabIndex="2"></asp:TextBox>
                                        <%--<textarea name="name" rows="3" placeholder="Please enter the Feedback" ></textarea>--%>
                                    </div>



                                    <div class="send-awards">
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnReset testi_cancel" OnClick="btnCancel_Click"
                                            TabIndex="4" />
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="rnr-btn" OnClick="btnSave_Click"
                                            OnClientClick="return ValidatePage() " TabIndex="3" />

                                        <%--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">CANCEL</span>
                       </button>--%>
                                        <%--<button type="button" name="button" class="add-emp rnr-btn">Send Feedback</button>--%>
                                    </div>

                                </div>


                            </div>




                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script src="/assets/js/all.js"></script>
    <script>
        $(document).ready(function () {
            var var_id = $('.file-upload > input').attr('id');
            //alert(var_id);
            $("label.file-upload__button").attr("for", var_id);

            $('body').removeClass().addClass('user-feedback Chrome').attr('id', 'user-feedback');
            $('.rnr-radio-btn table tr td span label').text('');
        })
    </script>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Web_Pages/MM_Rewards.Master" CodeBehind="LongServiceFileUpload.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.LongServiceFileUpload" ValidateRequest="false" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

        <script type="text/javascript">

        function flupload_onchange() {

            var flupload = document.getElementById('<%=flupload.ClientID %>');
            var rExp = "^.+(.xlsx|.XLSX|.xls|.XLS)$";
            if (flupload.value == "") {
                alert("Please upload a file first!");
                return false;
            }
            else {
                if (!flupload.value.match(rExp)) {
                    alert("Only xlsx file is allowed!");
                    flupload.value = "";
                    return false;
                }
            }
        }
   
    </script>

    <div class="popup">
        <h1>
            Upload Excel</h1>
        <div class="form01">
            <div>
                <asp:FileUpload ID="flupload" runat="server" filter=".xsls" onchange=" return flupload_onchange();" /></div>
            <div>
                &nbsp;</div>
            <div class="btnRed">
                <asp:Button ID="btnupload" runat="server" OnClick="btnupload_Click" Text="upload" OnClientClick="return flupload_onchange()" CssClass="btnLeft" /></div>
            <div>
                &nbsp;</div>
            <div>
                &nbsp;</div>
            <div>
                &nbsp;</div>
            <div>
                &nbsp;</div>

                <asp:Label ID="errlbl" runat="server" style="color:Red; font-weight:bold;" visible="false"/>
          
        </div>
    </div>
  </asp:Content>
  



﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_RewardsSales.Master"
    AutoEventWireup="true" CodeBehind="SalesBudgetAllocation.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.SalesBudgetAllocation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript">
        function btnSave_ClientClick() {
            var hdnTokenNumber = document.getElementById("<%=hdnTokenNumber.ClientID %>");
            hdnCEOEMailID = document.getElementById("<%=hdnCEOEMailID.ClientID %>");
            var lblTotal = document.getElementById('<%=lblTotal.ClientID %>');
            var hdnTotal = document.getElementById('<%=hdnTotal.ClientID %>');

            var grd = document.getElementById('<%= grdRecipient.ClientID  %>');
            var err = '';

            if (hdnTokenNumber.value == "") {
                err = "Please Select a CE! \n";
            }

            var check = false;
            var frm = document.forms[0];
            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox") {
                    if (frm.elements[i].checked) {
                        check = true;
                    }
                }
            }

            if (!check) {
                err += 'Please select atleast one recipient';
            }

            if (err == '') {
                return true;
            }
            else {
                alert(err);
                return false;
            }
        }

        function SwitchControls(chkSelect, hdnBudget1, txtBudget1, dpStart, dpEnd, rowIndex, e) {

            var chkSelect = document.getElementById(chkSelect);
            var txtBudget = document.getElementById(txtBudget1);
            var dpStart = $find(dpStart);
            var dpEnd = $find(dpEnd);
            var btnSave = document.getElementById("<%=btnSave.ClientID %>");

            // for highlighting selected row 
            grdRecipient = document.getElementById('<%= grdRecipient.ClientID %>');
            var row = grdRecipient.rows[parseInt(rowIndex) + 1];

            if (chkSelect.checked) {
                txtBudget_onblur_SubRoutine(txtBudget.value, hdnBudget1);
                txtBudget.disabled = false;
                dpStart.set_enabled(true);
                dpEnd.set_enabled(true);
                btnSave.disabled = false;
                // row.style.backgroundColor = '#FFFFCC';
                SelectRow(row);
            }
            else {
                UnselectRow(row);
                //row.style.backgroundColor = '#FFEEDD';
                txtBudget_onblur_SubRoutine('', hdnBudget1);
                txtBudget.disabled = true;
                dpStart.set_enabled(false);
                dpEnd.set_enabled(false);



                var check = false;
                var frm = document.forms[0];
                for (i = 0; i < frm.elements.length; i++) {
                    if (frm.elements[i].type == "checkbox") {
                        if (frm.elements[i].checked) {
                            check = true;
                        }
                    }
                }

                if (!check) {
                    btnSave.disabled = true;
                }
            }
        }

        var hdnSelectedRowIndex = "";

        function SelectCEO(str, strBudget, btn, EMailID, rowIndex) {
            hdnSelRowIndex = document.getElementById('<%=hdnSelectedRowIndex.ClientID %>');
            var hdnTokenNumber = document.getElementById('<%=hdnTokenNumber.ClientID %>');
            var lblTotal = document.getElementById('<%=lblTotal.ClientID %>');
            var hdnTotal = document.getElementById('<%=hdnTotal.ClientID %>');
            hdnCEOEMailID = document.getElementById("<%=hdnCEOEMailID.ClientID %>");

            hdnSelRowIndex.value = rowIndex;
            hdnTokenNumber.value = str;
            hdnCEOEMailID.value = EMailID;

            lblTotal.innerHTML = parseFloat(strBudget);
            hdnTotal.value = parseFloat(strBudget);

            var pnlRecipient = document.getElementById('<%=pnlRecipient.ClientID %>');
            pnlRecipient.style.display = "block";
            var pnlGrid = document.getElementById('<%=pnlGrid.ClientID %>');
            pnlGrid.style.display = "none";

            // for highlighting selected row 
            grdCEO = document.getElementById('<%= grdCEO.ClientID %>');

            if (hdnSelectedRowIndex != "") {
                var row = grdCEO.rows[parseInt(hdnSelectedRowIndex) + 1];
                //row.style.backgroundColor = '#FFEEDD';
                UnselectRow(row);
            }

            var row = grdCEO.rows[parseInt(rowIndex) + 1];
            // row.style.backgroundColor = '#FFFFCC';
            hdnSelectedRowIndex = rowIndex;
            SelectRow(row);
        }

        function SwitchControls(chkSelect, hdnBudget1, txtBudget1, dpStart, dpEnd, rowIndex, e) {

            var chkSelect = document.getElementById(chkSelect);
            var txtBudget = document.getElementById(txtBudget1);
            var dpStart = $find(dpStart);
            var dpEnd = $find(dpEnd);
            var btnSave = document.getElementById("<%=btnSave.ClientID %>");

            // for highlighting selected row 
            grdRecipient = document.getElementById('<%= grdRecipient.ClientID %>');
            var row = grdRecipient.rows[parseInt(rowIndex) + 1];

            if (chkSelect.checked) {
                txtBudget_onblur_SubRoutine(txtBudget.value, hdnBudget1);
                txtBudget.disabled = false;
                dpStart.set_enabled(true);
                dpEnd.set_enabled(true);
                btnSave.disabled = false;
                // row.style.backgroundColor = '#FFFFCC';
                SelectRow(row);
            }
            else {
                UnselectRow(row);
                //row.style.backgroundColor = '#FFEEDD';
                txtBudget_onblur_SubRoutine('', hdnBudget1);
                txtBudget.disabled = true;
                dpStart.set_enabled(false);
                dpEnd.set_enabled(false);



                var check = false;
                var frm = document.forms[0];
                for (i = 0; i < frm.elements.length; i++) {
                    if (frm.elements[i].type == "checkbox") {
                        if (frm.elements[i].checked) {
                            check = true;
                        }
                    }
                }

                if (!check) {
                    btnSave.disabled = true;
                }
            }
        }

        function validate(sender, key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes
            if ((keycode >= 48 && keycode <= 57)
                 || (keycode >= 96 && keycode <= 105)
                 || keycode == 110
                 || (keycode >= 35 && keycode <= 40)  // 35 end,36 home,37 to 40 are arrow keys
                 || keycode == 8 // backspace
                 || keycode == 9  // tab
                 || keycode == 46  // delete
                ) {
                return true;
            }
            else {
                sender.value = "";
                return false;
            }
        }

        function RequiredField_TextBox(str) {
            var errMsg = "";
            var control;

            if (str == "CE") {
                control = document.getElementById("<%= txtSearchUser.ClientID %>");
            }
            else {
                control = document.getElementById("<%= txtSearchRecipients.ClientID %>");
            }

            var errMsg = CheckText_Blank(control.value, str);
            errMsg += ValidateName(control.value, str);
            if (errMsg != "") {
                alert(errMsg);
                return false;
            }
            else
            { return true; }
        }


        function txtBudget_onblur(hdnBudget, txtBudget) {
            var txtBudget = document.getElementById(txtBudget);
            var hdnBudget = document.getElementById(hdnBudget);
            var lblTotal = document.getElementById('<%=lblTotal.ClientID %>');

            var err = '';
            err = ValidateDecimal(txtBudget.value, "Budget");

            if (err != '') {
                txtBudget.value = "";
            }
            else {

                if (parseFloat(txtBudget.value) == 0) {
                    txtBudget.value = "";
                }
                else {

                    if (txtBudget.value != "") {
                        var valtxtBudget = parseFloat(txtBudget.value);
                        var valhdnBudget = parseFloat(hdnBudget.value);
                        var vallblTotal = parseFloat(lblTotal.innerHTML);

                        if (valhdnBudget == 0) {
                            valhdnBudget = valtxtBudget * -1;
                        }
                        else {
                            valhdnBudget = valhdnBudget - valtxtBudget;
                        }

                        vallblTotal = parseFloat(lblTotal.innerHTML) + valhdnBudget;


                        if (parseFloat(vallblTotal) < 0) {
                            alert("Insufficient Credit!");
                            txtBudget.value = hdnBudget.value == 0 ? "" : hdnBudget.value;
                        }
                        else {
                            hdnBudget.value = valtxtBudget;
                            lblTotal.innerHTML = vallblTotal;
                        }
                    }
                    else {

                        if (hdnBudget.value != "") {
                            lblTotal.innerHTML = parseFloat(lblTotal.innerHTML) + parseFloat(hdnBudget.value);
                            hdnBudget.value = 0;
                        }
                    }
                }
            }
        }

        function txtBudget_onblur_SubRoutine(txtBudgetV, hdnBudget) {
            var hdnBudget = document.getElementById(hdnBudget);
            var lblTotal = document.getElementById('<%=lblTotal.ClientID %>');

            var err = '';
            err = ValidateDecimal(txtBudgetV, "Budget");

            if (err != '') {
                txtBudgetV = "";
            }
            else {
                if (txtBudgetV != "") {
                    var valtxtBudget = parseFloat(txtBudgetV);
                    var valhdnBudget = parseFloat(hdnBudget.value);
                    var vallblTotal = parseFloat(lblTotal.innerHTML);

                    if (valhdnBudget == 0) {
                        valhdnBudget = valtxtBudget * -1;
                    }
                    else {
                        valhdnBudget = valhdnBudget - valtxtBudget;
                    }

                    vallblTotal = parseFloat(lblTotal.innerHTML) + valhdnBudget;


                    if (parseFloat(vallblTotal) < 0) {
                        alert("Insufficient Credit!");
                        txtBudget.value = hdnBudget.value == 0 ? "" : hdnBudget.value;
                    }
                    else {
                        hdnBudget.value = valtxtBudget;
                        lblTotal.innerHTML = vallblTotal;
                    }
                }
                else {

                    if (hdnBudget.value != "") {
                        lblTotal.innerHTML = parseFloat(lblTotal.innerHTML) + parseFloat(hdnBudget.value);
                        hdnBudget.value = 0;
                    }
                }
            }

        }

        function Hide_grdRecipients()  // BtnCancel_OnClientClick
        {
            var pnlGrid = document.getElementById('<%=pnlGrid.ClientID %>');
            pnlGrid.style.display = "none";
            return false;
        }
    </script>
    <h1>
        Sales Budget Allocataion</h1>
    <div class="form01">
        <table>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblSearchCEOs" runat="server" Text="Senders Token Number / Name"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtSearchUser" runat="server" MaxLength="100" TabIndex="1"></asp:TextBox>
                    <div class="btnRed">
                        <asp:Button ID="btnGetCEOs" CssClass="btnLeft" runat="server" Width="100px" Text="Get"
                            OnClick="btnGetCEOs_Click" OnClientClick="return RequiredField_TextBox('CE');"
                            TabIndex="2" /></div>
                </td>
            </tr>
        </table>
    </div>
    <div class="grid01">
        <asp:GridView ID="grdCEO" runat="server" AllowPaging="true" AutoGenerateColumns="false"
            AlternatingRowStyle-CssClass="row01" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
            <Columns>
                <asp:BoundField DataField="TokenNumber" HeaderText="TokenNumber"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="DivisionName" HeaderText="Division"></asp:BoundField>
                <asp:BoundField DataField="Location" HeaderText="Location"></asp:BoundField>
                <asp:BoundField DataField="Budget" HeaderText="Budget" DataFormatString="{0:0.00}">
                </asp:BoundField>
                <asp:BoundField DataField="Balance" HeaderText="Balance" DataFormatString="{0:0.00}">
                </asp:BoundField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <a href="JavaScript:<%# String.Format("SelectCEO('{0}','{1}',this,'{2}','{3}')", Eval("TokenNumber"),Eval("Balance"),Eval("EMailID"),((GridViewRow)Container).RowIndex) %>"
                            tabindex="3" class="selLink">Select</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                BorderStyle="Solid" Font-Italic="true" />
            <EmptyDataTemplate>
                <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
            </EmptyDataTemplate>
            <PagerStyle CssClass="dataPager" />
            <SelectedRowStyle CssClass="selectedoddTd" />
        </asp:GridView>
    </div>
    <asp:Panel ID="pnlRecipient" runat="server" Style="display: none">
        <asp:HiddenField ID="hdnSelectedRowIndex" Value="" runat="server" />
        <asp:HiddenField ID="hdnTokenNumber" runat="server" Value="" />
        <asp:HiddenField ID="hdnCEOEMailID" runat="server" Value="" />
        <div class="form01">
            <table>
                <tr>
                    <td class="lblName">
                        <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label>
                        <span style="color: Red">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDivision" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="lblName">
                        Enter&nbsp;Recipient&nbsp;Token&nbsp;Number/Name<span style="color: Red">*</span>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtSearchRecipients" runat="server" MaxLength="100" TabIndex="4"></asp:TextBox>
                        <div class="btnRed">
                            <asp:Button ID="btnGetRecipients" CssClass="btnLeft" runat="server" Text="Get Employee Details"
                                OnClick="btnGetRecipients_Click" OnClientClick="return RequiredField_TextBox('Recipient');"
                                TabIndex="5" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="grid01">
            <asp:Panel ID="pnlGrid" runat="server">
                <asp:Label ID="lblRecipient" runat="server" Text="FOR Recipients" CssClass="vdHead"></asp:Label>
                <div class="actabHold">
                    <asp:GridView ID="grdRecipient" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                        AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdRecipient_PageIndexChanging"
                        DataKeyNames="EmailID" OnRowDataBound="grdRecipient_RowDataBound" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" Text="" TabIndex="6" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TokenNumber" HeaderText="TokenNumber"></asp:BoundField>
                            <%--<asp:BoundField DataField="EmailID" HeaderText="EmailID"></asp:BoundField>--%>
                            <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                            <asp:BoundField DataField="DivisionName" HeaderText="Division"></asp:BoundField>
                            <asp:BoundField DataField="Location" HeaderText="Location"></asp:BoundField>
                            <asp:TemplateField HeaderText="Budget">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnBudget" runat="server" Value="0" />
                                    <%-- onkeyup="return validate(this,event)"--%>
                                    <asp:TextBox ID="txtBudget" runat="server" Width="90px" onkeyup="return validate(this,event)"
                                        Enabled="false" TabIndex="7"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date">
                                <ItemTemplate>
                                    <telerik:RadDatePicker Enabled="false" Width="120px" ID="dpStartDate" runat="server"
                                        DateInput-DisabledStyle-BackColor="LightGray" DateInput-DateFormat="dd/MM/yyyy"
                                        TabIndex="8">
                                    </telerik:RadDatePicker>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Date">
                                <ItemTemplate>
                                    <telerik:RadDatePicker ID="dpEndDate" Width="120px" Enabled="false" runat="server"
                                        DateInput-DisabledStyle-BackColor="LightGray" DateInput-DateFormat="dd/MM/yyyy"
                                        TabIndex="9">
                                    </telerik:RadDatePicker>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                            BorderStyle="Solid" Font-Italic="true" />
                        <EmptyDataTemplate>
                            <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                        </EmptyDataTemplate>
                        <PagerStyle CssClass="dataPager" />
                        <SelectedRowStyle CssClass="selectedoddTd" />
                    </asp:GridView>
                </div>
                <br />
                Balance after Allocation : Rs.
                <asp:Label ID="lblTotal" runat="server" Text="0.0"></asp:Label>
                <asp:HiddenField ID="hdnTotal" runat="server" Value="0.0" />
            </asp:Panel>
        </div>
        <div class="btnRed">
            <asp:Button ID="btnSave" runat="server" CssClass="btnLeft" OnClick="btnSave_Click"
                Text="SAVE" OnClientClick="return btnSave_ClientClick()" TabIndex="10" /></div>
        <div class="btnRed">
            <asp:Button ID="btnCancel" runat="server" CssClass="btnLeft" Text="CANCEL" OnClientClick="return Hide_grdRecipients()"
                TabIndex="11" />
        </div>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Budget_Cal_Review.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.Budget_Cal_Review" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <style>
        div.grid01 table th, div.listDtls table th {
            padding: 5px 5px;
            white-space: normal;
        }

        .selectDivision option {
            max-height: 200px;
            overflow-y: auto;
        }

        .budgetCalModal {
            top: 10%;
        }

            .budgetCalModal .modal-header {
                background-color: #e31837;
                padding: 8px 15px;
            }

                .budgetCalModal .modal-header label {
                    color: #fff;
                    margin-bottom: 0;
                }

                .budgetCalModal .modal-header .closemodal input {
                    background: transparent;
                    border: none;
                    color: #fff;
                }

        .tblhodbudget {
            width: 100%;
        }

            .tblhodbudget tr td {
                padding: 5px 10px;
            }

                .tblhodbudget tr td:first-child {
                    width: 150px;
                }

                .tblhodbudget tr td input {
                    width: 100%;
                }

        #ContentPlaceHolder1_BudgetUploadedID {
            text-align: center;
            padding: 20px 0;
            border: 2px solid #272727;
            border-radius: 10px;
            margin-top: 50px;
            color: #e31837;
        }

            #ContentPlaceHolder1_BudgetUploadedID h4 {
                font-size: 18px;
                font-weight: bold;
                font-family: calibri;
            }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        }
        function Decimalvalue() {

            var NewBudget = $("#ctl00$ContentPlaceHolder1$txtNewBudget").val();
            if (NewBudget != Math.floor(NewBudget)) {
                alert('Amount Should not be Decimal');
                return false;
            }
            else {
                return true;

            }
        }
        function compareamount() {
            var avaliable = $("#ContentPlaceHolder1_txtNewBudget").val();
            if (avaliable != Math.floor(avaliable)) {
                alert('Amount Should not be Decimal');
                return false;
            }
            var avaliable = parseInt(avaliable);

            if ((avaliable % 500) != 0) {
                alert('Amount must be multiple of 500');
                return false;
            }
            return true;
        }
        function confirmSubmit() {
            var result = confirm('Are you sure to Submit the all HOD Budget ? Once budget will submit, it will replicate in HOD account and can not be revert');
            if (result)
                return true;
            else
                return false;
        }

    </script>
    <h1>BUDGET REVIEW FOR FINANCIAL YEAR
        <asp:Label ID="FinicialyearStartID" runat="server"></asp:Label>
        - 
        <asp:Label ID="FinicialyearEndID" runat="server"></asp:Label>

    </h1>
    <div id="BudgetUploadedID" style="display: none" runat="server">
        <h4>Budget Has been Uploaded for financial Year
            <asp:Label ID="FinicialyearStartID1" runat="server"></asp:Label>
            - 
            <asp:Label ID="FinicialyearEndID1" runat="server"></asp:Label>
        </h4>
    </div>
    <div id="TblID" style="display: block" runat="server">
        <table style="width: 100%;">
            <tr>
                <td style="padding: 5px;">Total Budget:</td>
                <td style="padding: 5px;">
                    <asp:TextBox ID="CalTotalBudget" runat="server" Enabled="false"></asp:TextBox>
                </td>

                <td style="padding: 5px;">Total Last Year Budget:
  
                </td>
                <td style="padding: 5px;">
                    <asp:TextBox ID="calLastYrBudget" runat="server" Enabled="false"></asp:TextBox>

                </td>
                <td style="padding: 5px;">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnLeft" Text="Submit Budget" TabIndex="-1" OnClientClick="return confirmSubmit();" OnClick="btnSubmit_Click" Width="152px" />

                </td>

            </tr>
            <tr>
                <td style="padding: 5px;" class="auto-style1">Company Name:
        
                </td>
                <td style="padding: 5px;">
                    <asp:DropDownList ID="ddlCompanyCode" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlCompanyCode_SelectedIndexChanged">
                    </asp:DropDownList>

                </td>
                <td style="padding: 5px;">Token Number / First Name:</td>
                <td style="padding: 5px;">
                    <asp:TextBox ID="txtToken" runat="server" Width="192px" OnTextChanged="txtToken_TextChanged"></asp:TextBox>

                </td>
                <td style="padding: 5px;">
                    <asp:Button ID="btnExport" runat="server" Text="Export" Width="90px" CssClass="btnLeft" OnClick="btnExport_Click" />

                    &nbsp;
                    <asp:Button ID="btnUpdatedBudget" runat="server" Text="Rpt" Width="48px" CssClass="btnLeft" OnClick="btnUpdatedBudget_Click"/>



                </td>
            </tr>
            <tr>

                <td style="padding: 5px;" class="auto-style1">Business Unit:</td>
                <td style="padding: 5px;">
                    <asp:DropDownList ID="ddlBusinessUnit" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>

                <td style="padding: 5px;">Employee Level:</td>
                <td style="padding: 5px;">
                    <asp:DropDownList ID="ddlEmployeeLevelName" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployeeLevelName_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td style="padding: 5px;">
                    <asp:Button ID="btnDisplay" runat="server" Text="Search" Width="152px" CssClass="btnLeft" OnClick="btnDisplay_Click" />
                </td>

            </tr>
            <tr>
                <td style="padding: 5px;">Division</td>
                <td style="padding: 5px;">
                    <asp:DropDownList ID="ddlDivision" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1" AutoPostBack="true" OnSelectedIndexChanged="ddlDivision_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td style="padding: 5px;">&nbsp;</td>
                <td style="padding: 5px;">&nbsp;</td>

                <td style="padding: 5px;">
                    <asp:Button ID="btnReset" runat="server" Text="Reset" Width="90px" CssClass="btnLeft" OnClick="btnReset_Click" />
                &nbsp;
                    

                    <asp:Button ID="btnSort" runat="server" Text="Sort" Width="47px" CssClass="btnLeft" OnClick="btnSort_Click" />

                <td style="padding: 5px;">&nbsp;</td>
            </tr>
        </table>

        <br />

    </div>
    <div class="grid01" style="background-color: transparent; width: 100%; overflow-x: auto; display: none" id="Detail1" runat="server">
        <asp:Label ID="gridtitle" runat="server" Text="HOD Budget Review"></asp:Label>
       <%-- ConfigurationManager.AppSettings["PageSize"]--%>
        <asp:GridView ID="grdReviewBudget" runat="server" AllowSorting="true" AllowPaging="true" OnPageIndexChanging="grdReviewBudget_PageIndexChanging"
             AutoGenerateColumns="false" OnRowCommand="grdReviewBudget_RowCommand" AlternatingRowStyle-CssClass="row01" Style="width: 100%; margin: 0 auto;" CssClass="grdAwards_new" 
            OnRowCancelingEdit="grdReviewBudget_RowCancelingEdit" OnRowEditing="grdReviewBudget_RowEditing" 
            PageSize=' <%# Convert.ToInt32(8) %>' OnRowUpdating="grdReviewBudget_RowUpdating">
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="true" Visible="true"></asp:BoundField>
                
                <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" ReadOnly="true" ItemStyle-Width="100px"></asp:BoundField>
                
                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" ItemStyle-Width="250px"></asp:BoundField>
                <asp:BoundField DataField="CompanyName" HeaderText="Company Name" ReadOnly="true" ItemStyle-Width="500px"></asp:BoundField>
                <asp:BoundField DataField="BusinessUnit" HeaderText="Business Unit" ReadOnly="true" ItemStyle-Width="400px"></asp:BoundField>
                <asp:BoundField DataField="Levels" HeaderText="Level" ReadOnly="true" ItemStyle-Width="200px"></asp:BoundField>
                <asp:BoundField DataField="DirectReportees" HeaderText="Direct" ReadOnly="true" ItemStyle-Width="20px"></asp:BoundField>
                <asp:BoundField DataField="InDirectReportees" HeaderText="In Direct" ReadOnly="true" ItemStyle-Width="20px"></asp:BoundField>
                <asp:BoundField DataField="ReporteesCount" HeaderText="Total Reportees" ReadOnly="true" ItemStyle-Width="20px"></asp:BoundField>

                <asp:BoundField DataField="BudgetPerHead" HeaderText="PerHead" ReadOnly="true" DataFormatString="{0:n0}" ItemStyle-Width="30px"></asp:BoundField>
                <asp:BoundField DataField="TotalBudget" HeaderText="Total Budget" ReadOnly="true" DataFormatString="{0:n0}" ItemStyle-Width="100px"></asp:BoundField>

                <asp:TemplateField HeaderText="Final Budget" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <asp:Label ID="lblUpdatedBudgetHOD" runat="server" Text='<%#Bind("FinalBudget","{0:n0}") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtUpdatedBudgetHOD" runat="server" Text='<%#Bind("FinalBudget") %>' BackColor="Yellow"
                            MaxLength="9" onkeypress="javascript:return isNumber(event)"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="LastYearBudget" HeaderText="Last Year Budget" ReadOnly="true" DataFormatString="{0:n0}" ItemStyle-Width="100px"></asp:BoundField>
                <asp:BoundField DataField="BudgetDifference" HeaderText="Difference" ReadOnly="true" DataFormatString="{0:n0}" ItemStyle-Width="50px"></asp:BoundField>

                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px">
                    <HeaderTemplate>Change Budget</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" Width="50px" CssClass="btnLeft" 
                            CommandArgument='<%# Bind("TokenNumber") %>' CommandName="Edit" />
                        <asp:HiddenField ID="HiddenField2" runat="server" Value='<%#Eval("IsUpdated")%>' />

                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("IsUpdated")%>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="80px" CssClass="btnLeft" CommandArgument='<%# Bind("TokenNumber") %>' CommandName="Update" />
                        &nbsp
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80px" CssClass="btnLeft" CommandArgument='<%# Bind("TokenNumber") %>' CommandName="Cancel" />
                    </EditItemTemplate>

                </asp:TemplateField>           

            </Columns>
            <EmptyDataRowStyle ForeColor="#272727" Height="5" BorderColor="Black" BorderWidth="1"
                BorderStyle="Solid" Font-Italic="true" />
            <EmptyDataTemplate>
                <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
            </EmptyDataTemplate>
            <PagerStyle CssClass="dataPager" />
            <SelectedRowStyle CssClass="selectedoddTd" />
        </asp:GridView>
    </div>

    <div class="modal budgetCalModal" id="myModal" runat="server" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <label>Update HOD Budget</label>
                    <span class="closemodal">
                        <asp:Button ID="closeemailpopup" runat="server" OnClick="closeemailpopup_Click" Text="&times;" /></span>
                </div>
                <div class="modal-body">
                    <%--<table runat="server" class="formformob tblhodbudget" style="position: relative;">
                        <tr>
                            <td class="lblName">Token Number<span style="color: #e31837;">*</span></td>
                            <td class="hidden-xs">:</td>
                            <td>
                                <asp:TextBox ID="txtTokenNumber" runat="server" MaxLength="100" TabIndex="10" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblName">Name<span style="color: #e31837;">*</span></td>
                            <td class="hidden-xs">:</td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblName">Final Budget<span style="color: #e31837;">*</span></td>
                            <td class="hidden-xs">:</td>
                            <td>
                                <asp:TextBox ID="txtBudget" runat="server" MaxLength="100" TabIndex="2" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblName">New Budget<span style="color: #e31837;">*</span></td>
                            <td class="hidden-xs">:</td>
                            <td>
                                <asp:TextBox ID="txtNewBudget" runat="server" MaxLength="9" TabIndex="2" onkeypress="javascript:return isNumber(event)"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="NewBudget" runat="server" CssClass="btnLeft" Text="Update Budget" OnClick="NewBudget_Click" OnClientClick="return compareamount();" />
                            </td>
                        </tr>
                    </table>--%>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

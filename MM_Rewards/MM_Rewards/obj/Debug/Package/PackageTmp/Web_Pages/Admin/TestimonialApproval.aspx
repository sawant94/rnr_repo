﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="TestimonialApproval.aspx.cs" Inherits="MM_Rewards.Web_Pages.TestimonialApproval" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript" language="javascript">


        function ValidatePage() {

            var r = confirm("Do you want to submit record ?");
            //alert(r);
            if (r != true) {
                return false;
            }


            var errMsg = '';
            var f = 0;
            var txttestimonial = document.getElementById('<%= txttestimonial.ClientID %>');
            var rdbstatus = document.getElementById('<%= rdbstatus.ClientID %>');
            var Remark = document.getElementById('<%= txtRemark.ClientID %>');
            errMsg = CheckText_Blank(txttestimonial.value, 'Testimonial');
            var inputs = rdbstatus.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    f = 1;
                    break;
                }
            }
            if (f == 0) {
                errMsg += "Please Select Status \n";
            }
            errMsg += CheckText_Blank(Remark.value, 'Remark');
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
        function ClearFields() {

            var grdTestimonialList = document.getElementById('<%= grdTestimonialList.ClientID %>');
            var txttestimonial = document.getElementById('<%= txttestimonial.ClientID %>');
            var rdbstatus = document.getElementById('<%= rdbstatus.ClientID %>');
            var Remark = document.getElementById('<%= txtRemark.ClientID %>');
            hdnid = document.getElementById('<%= hdnid.ClientID %>');
            var divedit = document.getElementById('<%= divedit.ClientID %>');
            var inputs = rdbstatus.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    inputs[i].checked = false;
                }
            }
            hdnSelectedRowIndex = document.getElementById('<%= hdnSelectedRowIndex.ClientID %>');
            divedit.style.display = 'none';
            txttestimonial.value = "";
            rdbstatus.checked = false;
            Remark.value = "";
            hdnid.value = "";

            if (hdnSelectedRowIndex.value == "")
                return false;
            var row = grdTestimonialList.rows[parseInt(hdnSelectedRowIndex.value) + 1];
            //row.style.backgroundColor = '#ffEECC';
            UnselectRow(row);
            hdnSelectedRowIndex.value = "";
            return false;
        }




    </script>
    <div>
        <h1>
            Testimonial Approval</h1>
        <div class="grid01">
            <asp:HiddenField ID="hdnSelectedRowIndex" runat="server" />
            <asp:HiddenField ID="hdnTokennumber" runat="server" />
            <asp:GridView ID="grdTestimonialList" runat="server" AutoGenerateColumns="False"
                OnRowDataBound="grdTestimonialList_RowDataBound" DataKeyNames="ID" AlternatingRowStyle-CssClass="row01"
                PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                OnPageIndexChanging="grdTestimonialList_PageIndexChanging" AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" />
                    <asp:BoundField DataField="EmployeeName" HeaderText="Employee Name" />
                    <asp:BoundField DataField="ShortTestimonial" HeaderText="Testimonial" />
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("ApproverRemarks") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%-- <asp:Label ID="lblTemplate" runat="server" Visible="true" Text='<%# Eval("Template") %>' style="display:none" ></asp:Label>--%>
                            <asp:LinkButton ID="lnkSelect" CommandName="Select" OnClick="lnkSelect_Click" runat="server"
                                CssClass="selLink" TabIndex="1" Visible='<%# Eval("Status") %>'>
                                            Select</asp:LinkButton>
                            <asp:Label ID="ImageName" runat="server" Visible="false" Text='<%#Eval("ImageName") %>'></asp:Label>
                            <asp:Label ID="lblFulltext" runat="server" Visible="false" Text='<%#Eval("TestimonoialText") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                    BorderStyle="Solid" Font-Italic="true" />
                <EmptyDataTemplate>
                    <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                </EmptyDataTemplate>
                <PagerStyle CssClass="dataPager" />
                <SelectedRowStyle CssClass="selectedoddTd" />
            </asp:GridView>
        </div>
    </div>
    <div class="form01" id="divedit" visible="false" runat="server">
        <table>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblImage" runat="server" Text="Image"></asp:Label><span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:Image ID="imguploadedimage" runat="server" Height="100px" Width="100px" />
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lbltext" runat="server" Text="Testimonial Text"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txttestimonial" runat="server" MaxLength="150" TextMode="MultiLine"
                        Rows="5" Width="250px" TabIndex="1"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblstatus" runat="server" Text="Status" CssClass="lblName"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:RadioButtonList ID="rdbstatus" runat="server" CssClass="chkBox" TabIndex="2">
                        <asp:ListItem Text="Approve" Value="true"></asp:ListItem>
                        <asp:ListItem Text="Reject" Value="false"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblremark" runat="server" Text="Remark" CssClass="lblName"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtRemark" runat="server" MaxLength="150" TextMode="MultiLine" Rows="5"
                        Width="250px" TabIndex="3"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btnLeft" OnClientClick="return ValidatePage()"
                            TabIndex="4" OnClick="btnsave_Click" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" OnClientClick="return ClearFields()"
                            TabIndex="5" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hdnid" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

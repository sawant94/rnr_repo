﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Employee_Designation.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.Employee_Designation"
    EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <script type="text/javascript" language="javascript">
        function validatebtn() {
            var errMsg = '';
            var btnGET = document.getElementById('<%=txtTokenNumber.ClientID %>');
            errMsg += ValidateName(btnGET.value, "Token Number / Name");
            errMsg += CheckText_Blank(btnGET.value, 'Token Number / Name')

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }

        function ValidatePage() {

            var r = confirm("Do you want to submit record");
            //alert(r);
            if (r != true) {
                return false;
            }

            var errMsg = '';
            //             var Vchklstbox = document.getElementById('<%=chkdesignation.ClientID %>');
            //             errMsg += ChkList(Vchklstbox, 'Designation')

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <div class="form01">
          <h1> Employee Access Roles</h1>
        <table>
           <tr>
                <td class="lblName">
                    Enter&nbsp;Token&nbsp;Number/Name<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtTokenNumber" runat="server" MaxLength="50" TabIndex="1"></asp:TextBox>
                   
                    <div class="btnRed">  
                                    <asp:Button ID="btnget" runat="server" Text="GetDetails" OnClick="btnget_Click" OnClientClick="return validatebtn()"
                        TabIndex="2" CssClass="btnLeft" />
                    </div>
  
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:HiddenField ID="hdnRecipientTokenNumber" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="grid01">
                            <asp:GridView ID="grdEmployeeList" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="row01" 
                                OnRowCommand="grdEmployeeList_RowCommand" AllowPaging="true" 
                                onpageindexchanging="grdEmployeeList_PageIndexChanging"  PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'>
                                <Columns>
                                    <asp:BoundField DataField="TokenNumber" HeaderText="Token Number" />
                                    <asp:BoundField DataField="EmpName" HeaderText="Name" />
                                    <asp:BoundField DataField="ProcessName" HeaderText="Process Name" />
                                    <asp:BoundField DataField="DivisionName" HeaderText="Division Name" />
                                    <asp:BoundField DataField="LocationName" HeaderText="Location Name" />
                                    <asp:BoundField DataField="IsCEO" HeaderText="IsCEO" Visible="false" />
                                    <asp:BoundField DataField="IsRR" HeaderText="IsRR" Visible="false" />
                                    <asp:BoundField DataField="IsAdministrator" HeaderText="IsAdministrator" Visible="false" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber")  %>' CommandName="Select"
                                               runat="server" CssClass="selLink" TabIndex="3">
                                            Select</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                             <PagerStyle  CssClass="dataPager"  />
                                <SelectedRowStyle CssClass="selectedoddTd" />
                            </asp:GridView>
                      
                    </div>
                </td>
            </tr>
        </table>
        <table id="tblcheck" runat="server">
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDesignation" runat="server" Text="Select Role"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td >
                    :
                </td>
                <td>
                    <asp:CheckBoxList ID="chkdesignation" runat="server" Width="138px" TabIndex="4" CssClass="chkBox">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnsetdesignation" runat="server" Text="Save" CssClass="btnLeft"
                            OnClick="btnsetdesignation_Click" OnClientClick="return ValidatePage()" TabIndex="5" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" OnClick="btnCancel_Click"
                            TabIndex="6" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="AwardTypeManagement.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.AwardTypeManagement"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript" language="javascript">


        function ValidatePage() {

            var r = confirm("Do you want to submit record ?");
            //alert(r);
            if (r != true) {
                return false;
            }

            var errMsg = '';
            var btnGET = document.getElementById('<%=txtAwardName.ClientID %>');
            var txtMailTemplate_Redeem = document.getElementById('<%=txtMailTemplate_Redeem.ClientID %>');
            var txtMailTemplate_Auto = document.getElementById('<%=txtMailTemplate_Auto.ClientID %>');
            txtMailTemplate_Confirmation = document.getElementById('<%=txtMailTemplate_Confirmation.ClientID %>');

            errMsg += CheckText_Blank(btnGET.value, 'Award Type')
            errMsg += ValidateName(btnGET.value, "Award Type");
            errMsg += CheckText_Blank(txtMailTemplate_Redeem.value, 'Non-AutoRedeem Awards Template')
            errMsg += CheckText_Blank(txtMailTemplate_Auto.value, 'Non-AutoRedeem Awards Template')
            errMsg += CheckText_Blank(txtMailTemplate_Confirmation.value, 'Confirmation Template')


            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }

        function ClearFields() {

            grdAwardList = document.getElementById('<%= grdAwardList.ClientID %>');
            txtAwardName = document.getElementById('<%= txtAwardName.ClientID %>');
            chkActive = document.getElementById('<%= chkActive.ClientID %>');
            hdnid = document.getElementById('<%= hdnid.ClientID %>');
            txtMailTemplate_Redeem = document.getElementById('<%= txtMailTemplate_Redeem.ClientID %>');
            var txtMailTemplate_Auto = document.getElementById('<%=txtMailTemplate_Auto.ClientID %>');
            txtMailTemplate_Confirmation = document.getElementById('<%=txtMailTemplate_Confirmation.ClientID %>');

            hdnSelectedRowIndex = document.getElementById('<%= hdnSelectedRowIndex.ClientID %>');

            txtAwardName.value = "";
            chkActive.checked = true;
            hdnid.value = "";
            txtMailTemplate_Redeem.value = "";
            txtMailTemplate_Auto.value = "";
            txtMailTemplate_Confirmation.value = "";

            if (hdnSelectedRowIndex.value == "") return false;
            var row = grdAwardList.rows[parseInt(hdnSelectedRowIndex.value) + 1];
            //row.style.backgroundColor = '#ffEECC';
            UnselectRow(row);
            hdnSelectedRowIndex.value = "";
            return false;
        }

        /*   function onGridViewRowSelected(rowIndex, ID, AwardName, Active, template) {
        debugger;
        grdAwardList = document.getElementById('<%= grdAwardList.ClientID %>');
        hdnSelectedRowIndex = document.getElementById('<%= hdnSelectedRowIndex.ClientID %>');
            
        if (hdnSelectedRowIndex.value != "") {
        var row = grdAwardList.rows[parseInt(hdnSelectedRowIndex.value) + 1];
        // row.style.backgroundColor = '#FFEEDD';
        UnselectRow(row);

        }
        hdnSelectedRowIndex.value = rowIndex;

        //grdAwardList = document.getElementById('<%= grdAwardList.ClientID %>');
        txtAwardName = document.getElementById('<%= txtAwardName.ClientID %>');
        chkActive = document.getElementById('<%= chkActive.ClientID %>');
        hdnid = document.getElementById('<%= hdnid.ClientID %>');
        txtMailTemplate_Redeem = document.getElementById('<%= txtMailTemplate_Redeem.ClientID %>');

        txtAwardName.value = AwardName;
        if (Active == "False")
        chkActive.checked = false;
        else
        chkActive.checked = true;

        hdnid.value = ID;
        txtMailTemplate_Redeem.value = template;

        var row = grdAwardList.rows[parseInt(rowIndex) + 1];
        //row.style.backgroundColor = '#FFFFCC';
        SelectRow(row);
        return false;
        } */
    </script>
    <h1>
        Award Type Management</h1>
    <div class="grid01">
        <asp:HiddenField ID="hdnSelectedRowIndex" runat="server" />
        <asp:GridView ID="grdAwardList" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdAwardList_RowDataBound"
            DataKeyNames="ID" AlternatingRowStyle-CssClass="row01">
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                <asp:BoundField DataField="AwardName" HeaderText="Award Type" />
                <asp:BoundField DataField="IsActive" HeaderText="Active" />
                <%-- <asp:BoundField DataField="Template" HeaderText="" Visible="false" />--%>
                <%--  <asp:TemplateField >
                        <ItemTemplate>
                            
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%-- <asp:Label ID="lblTemplate" runat="server" Visible="true" Text='<%# Eval("Template") %>' style="display:none" ></asp:Label>--%>
                        <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("ID")  %>' CommandName="Select"
                            OnClick="lnkSelect_Click" runat="server" CssClass="selLink" TabIndex="1">
                                            Select</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="dataPager" />
            <SelectedRowStyle CssClass="selectedoddTd" />
        </asp:GridView>
    </div>
    <div class="form01">
        <table>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblAwardName" runat="server" Text="Enter Award Type"></asp:Label><span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtAwardName" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblactive" runat="server" Text="Is Active" CssClass="lblName"></asp:Label><span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:CheckBox ID="chkActive" CssClass="chkBox" runat="server" Checked="true" TabIndex="3">
                    </asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td>
                    Mail template for Non-Autoredeemed Awards<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtMailTemplate_Redeem" TextMode="MultiLine" Height="150" Width="400" TabIndex="4" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Mail template for AutoRedeemed Awards<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtMailTemplate_Auto" TextMode="MultiLine" Height="150" Width="400" TabIndex="5" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Mail template for Confirmation<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtMailTemplate_Confirmation" TextMode="MultiLine" Height="150" Width="400" TabIndex="6" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td style="padding-left: 17%;">
                    <div class="btnRed">
                        <asp:Button ID="btnsaveAwardname" runat="server" Text="Save" CssClass="btnLeft" OnClick="btnsaveAwardname_Click"
                            OnClientClick="return ValidatePage()" TabIndex="7" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Clear" CssClass="btnReset" OnClientClick="return ClearFields()"
                            TabIndex="8" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hdnid" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

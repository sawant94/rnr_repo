﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Award_Management.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.Award_Management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript">

        function OnlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charcode = window.event.keyCode;
                }
                else if (e) {
                    var charcode = e.which;
                } else  {
                    return true;
                }

                if ((charcode > 64 && charcode < 91) || (charcode > 96 && charcode < 123)) {
                    return true;
                } else {
                    alert("Please Enter Alphabets Only");
                    return false;
                }

            } catch (e) {
                alert(e.description);
            }
        }

        function validateDropDown() {
            var ddlID = document.getElementById('<%=drpAwardType.ClientID %>');
            var tbl = document.getElementById('<%=tblCheckListBx.ClientID %>');
            var word = ddlID.value.split("|")
            if (word[1] == "True") {
                tbl.style.display = "none";
            }
            else {
                tbl.style.display = "block";
            }
        }

        function validateSave() {

            var r = confirm("Do you want to submit record ?");
            //alert(r);
            if (r != true) {
                return false;
            }

            var errMsg = '';
            var txtName = document.getElementById('<%=txtName.ClientID %>');
            var txtAmount = document.getElementById('<%=txtAmount.ClientID %>');
            var drpAwardType = document.getElementById('<%=drpAwardType.ClientID %>');
            var chklEligibility = document.getElementById('<%=chklEligibility.ClientID %>');
            var chkRecipientEligibility = document.getElementById('<%=chkRecipientEligiblity.ClientID %>');
            var tbl = document.getElementById('<%=tblCheckListBx.ClientID %>');
            var word = drpAwardType.value.split("|")

            errMsg += CheckDropdown_Blank(drpAwardType.value, 'Award Type');
            errMsg += CheckText_Blank(txtName.value, 'Name');
            errMsg += CheckText_Blank(txtAmount.value, 'Amount');
            //wether to validate CheckListBox or Not, Acc to condition
            if (word[1] == "False" || drpAwardType.value == 'Select') {
                errMsg += RequiredField_CheckBoxList(chklEligibility, 'Giver Eligibility');
                errMsg += RequiredField_CheckBoxList(chkRecipientEligibility, 'Recipient Eligibility');
            }
            if (txtAmount.value != "" && parseFloat(txtAmount.value) < 1) {
                errMsg += "Amount : Amount can not be less than 1";
            }
            errMsg += ValidateDecimal(txtAmount.value, 'Amount');
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }

        function validate(sender, key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes
            if ((keycode >= 48 && keycode <= 57)
                 || (keycode >= 96 && keycode <= 105)
                 || keycode == 110
                 || (keycode >= 35 && keycode <= 40)  // 35 end,36 home,37 to 40 are arrow keys
                 || keycode == 8 // backspace
                 || keycode == 9  // tab
                 || keycode == 46  // delete
                     || keycode == 190
                || keycode == 110
                ) {
                return true;
            }
            else {
                sender.value = "";
                return false;
            }
        }

        function btnCancel_ClientClick() {
            var txtName = document.getElementById('<%=txtName.ClientID %>');
            var txtAmount = document.getElementById('<%=txtAmount.ClientID %>');
            var drpAwardType = document.getElementById('<%=drpAwardType.ClientID %>');
            var chklEligibility = document.getElementById('<%=chklEligibility.ClientID %>');
            var chkRecipientEligiblity = document.getElementById('<%=chkRecipientEligiblity.ClientID %>');
            var chkAll = document.getElementById('<%=chkAll.ClientID %>');
            var chkallrecipient = document.getElementById('<%=chkallrecipient.ClientID %>');
            var hdnSelectedIndex = document.getElementById('<%=hdnSelectedIndex.ClientID %>');
            var chkautoredeem = document.getElementById('<%=chkautoredeem.ClientID %>');
            txtName.value = "";
            txtAmount.value = "";

            if (hdnSelectedIndex.value != "") {
                var grdAwards = document.getElementById('<%=grdAwards.ClientID %>');
                var row = grdAwards.rows[parseInt(hdnSelectedIndex.value) + 1];
                if (parseInt(hdnSelectedIndex.value) % 2 == 0)
                    row.className = '';
                else
                    row.className = 'row01';
                hdnSelectedIndex.value = "";
            }

            drpAwardType.selectedIndex = 0;
            chkAll.checked = false;
            chkRecipientEligiblity.checked = false;
            chkallrecipient.checked = false;
            chkautoredeem.checked = false;
            var checkbox = chklEligibility.getElementsByTagName("input");
            var counter = 0;
            for (var i = 0; i < checkbox.length; i++) {
                checkbox[i].checked = false;
            }
            var checkbox = chkRecipientEligiblity.getElementsByTagName("input");
            var counter = 0;
            for (var i = 0; i < checkbox.length; i++) {
                checkbox[i].checked = false;
            }
            alert('All Fields cleared');
        }

        function CheckUncheckAllListBox(obj) {
            var checkobj = obj;
            chkAll = document.getElementById('<%=chkAll.ClientID %>');
            if (checkobj.name == chkAll.name) {
                //  chkAll = document.getElementById('<%=chkAll.ClientID %>');

                chklEligibility = document.getElementById('<%=chklEligibility.ClientID %>');

                hdnNoOfCheckedNodes = document.getElementById('<%=hdnNoOfCheckedNodes.ClientID %>');
                hdnTotalLevels = document.getElementById('<%=hdnTotalLevels.ClientID %>');
            }
            else {
                chkAll = document.getElementById('<%=chkallrecipient.ClientID %>');

                chklEligibility = document.getElementById('<%=chkRecipientEligiblity.ClientID %>');

                hdnNoOfCheckedNodes = document.getElementById('<%=hdnNoOfCheckedNodesRecipient.ClientID %>');
                hdnTotalLevels = document.getElementById('<%=hdnTotalLevelsrecipients.ClientID %>');

            }
            var checkbox = chklEligibility.getElementsByTagName("input");
            var counter = 0;
            for (var i = 0; i < checkbox.length; i++) {
                if (chkAll.checked) {
                    if (checkbox[i].disabled == false) {
                        checkbox[i].checked = true;
                    }
                    hdnNoOfCheckedNodes.value = hdnTotalLevels.value;
                }
                else {
                    if (checkbox[i].disabled == false) {
                        checkbox[i].checked = false;
                    }
                    hdnNoOfCheckedNodes.value = "0";
                }

            }
            //  alert(hdnTotalLocations.value + " || " + hdnNoOfCheckedNodes.value);
        }


        function chk_Click(sender, flag) {
            // debugger;
            // alert(sender);
            if (flag == "g") {
                chkAll = document.getElementById('<%=chkAll.ClientID %>');
                hdnNoOfCheckedNodes = document.getElementById('<%=hdnNoOfCheckedNodes.ClientID %>');
                hdnTotalLevels = document.getElementById('<%=hdnTotalLevels.ClientID %>');

                if (sender.checked) {
                    hdnNoOfCheckedNodes.value++; // = hdnNoOfCheckedNodes.value + 1;
                }
                else {
                    hdnNoOfCheckedNodes.value--; // = hdnNoOfCheckedNodes.value - 1;
                }

            }
            else if (flag = "r") {
                chkAllRecipient = document.getElementById('<%=chkallrecipient.ClientID %>');
                RecipienthdnNoOfCheckedNodes = document.getElementById('<%=hdnNoOfCheckedNodesRecipient.ClientID %>');
                RecipienthdnTotalLevels = document.getElementById('<%=hdnTotalLevelsrecipients.ClientID %>');
                if (sender.checked) {
                    RecipienthdnNoOfCheckedNodes.value++; // = hdnNoOfCheckedNodes.value + 1;
                }
                else {
                    RecipienthdnNoOfCheckedNodes.value--; // = hdnNoOfCheckedNodes.value - 1;
                }
            }

            if (hdnTotalLevels.value == hdnNoOfCheckedNodes.value && hdnNoOfCheckedNodes.value != "0") {
                chkAll.checked = true;
            }
            else {
                chkAll.checked = false;
            }

            if (RecipienthdnTotalLevels.value == RecipienthdnNoOfCheckedNodes.value && RecipienthdnNoOfCheckedNodes.value != "0") {
                chkAllRecipient.checked = true;
            }
            else {
                chkAllRecipient.checked = false;
            }

        }


        function changeDivDisplay(divId) {
            div = document.getElementById(divId);
            if (div.style.display == 'block')
            { div.style.display = 'none' }
            else
            { div.style.display = 'block' }
        }


    </script>
    <h1>
        Award Management
        <%--<asp:HyperLink ID="lnkAdd" runat="server" NavigateUrl="~/Web_Pages/Admin/Award_Management_AE.aspx"
                    Text="Add New Award"></asp:HyperLink>--%>
    </h1>
    <div class="grid01">
       
            <asp:GridView ID="grdAwards" runat="server" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                AutoGenerateColumns="false" DataKeyNames="ID,Autoredeem,IsSystem" OnPageIndexChanging="grdAwards_PageIndexChanging"
                AlternatingRowStyle-CssClass="row01">
                <Columns>
                    <asp:TemplateField HeaderText="Award Type">
                        <ItemTemplate>
                            <asp:Label ID="fsf" runat="server" Text='<%# ((BE_Rewards.BE_Admin.BE_AwardManagement)Container.DataItem).AwardTypeText%>'></asp:Label>
                            <asp:Label ID="lblTypeDeactive" runat="server" Text="(De-Activated)" ForeColor="GrayText"
                                Visible='<%# Convert.ToBoolean(((BE_Rewards.BE_Admin.BE_AwardManagement)Container.DataItem).TypeIsActive) == false ? true : false %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="AwardTypeText" HeaderText="Type" SortExpression="AwardTypeText" />--%>
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
                    <asp:BoundField DataField="Active" HeaderText="Active" SortExpression="Active" />
                    <%--<asp:BoundField DataField="TypeIsActive" HeaderText="TypeIsActive" SortExpression="TypeIsActive"  />--%>
                    <asp:CheckBoxField DataField="Active" HeaderText="Active" SortExpression="Active"
                        ReadOnly="true" Visible="false" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton CssClass="editLink" ID="btnEdit" runat="server" Text="Edit" CommandName="Select"
                                OnClick="btnEdit_Click" Visible='<%# Convert.ToBoolean(Eval("TypeIsActive")) == false ? false : true %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkEdit" runat="server" NavigateUrl='<%# String.Format("Award_Management_AE.aspx?AwardID={0}", Eval("ID") )%>'>Edit</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                </Columns>
                <EmptyDataRowStyle ForeColor="#e31837" Height="5" BorderColor="Black" BorderWidth="1"
                    BorderStyle="Solid" Font-Italic="true" />
                <EmptyDataTemplate>
                    <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                </EmptyDataTemplate>
                <PagerStyle CssClass="dataPager" />
                <SelectedRowStyle CssClass="selectedoddTd" />
            </asp:GridView>
       
    </div>
    <div class="form01">
        <table>
            <tr>
                <td class="lblName">
                    Award Type<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="drpAwardType" runat="server" TabIndex="1" 
                        onchange="validateDropDown();" AutoPostBack="True" 
                        onselectedindexchanged="drpAwardType_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Name<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" MaxLength="100" TabIndex="2" OnKeyPress="return OnlyAlphabets(event,this)"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Amount<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtAmount" runat="server" MaxLength="12" onkeyup="return validate(this,event)"
                        TabIndex="3"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table id="tblCheckListBx" runat="server">
            <tr>
                <td class="lblName">
                    Giver Eligibility<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:CheckBox ID="chkAll" CssClass="chkBox" runat="server" onclick="CheckUncheckAllListBox(this)"
                        AutoPostBack="false" Text="ALL" TabIndex="4" />
                    <asp:HiddenField ID="hdnTotalLevels" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnNoOfCheckedNodes" runat="server" Value="0" />
                    <div class="linkShow">
                        <a onclick="changeDivDisplay('lnkBox')">Show association </a>
                        <div id="lnkBox" class="lnkBox" onclick="changeDivDisplay('lnkBox')">
                            <asp:Label ID="lblTable" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="chkBoxHold">
                        <asp:CheckBoxList ID="chklEligibility" CssClass="chkBox" runat="server" TabIndex="5"
                            RepeatLayout="UnorderedList">
                        </asp:CheckBoxList>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Recipient Eligibility<span style="color: #e31837">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:CheckBox ID="chkallrecipient" CssClass="chkBox" runat="server" onclick="CheckUncheckAllListBox(this)"
                        AutoPostBack="false" Text="ALL" TabIndex="5" />
                    <asp:HiddenField ID="hdnTotalLevelsrecipients" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnNoOfCheckedNodesRecipient" runat="server" Value="0" />
                    <br />
                    <br />
                    <div class="chkBoxHold">
                        <asp:CheckBoxList ID="chkRecipientEligiblity" CssClass="chkBox" runat="server" TabIndex="5"
                            RepeatLayout="UnorderedList">
                        </asp:CheckBoxList>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:CheckBox CssClass="chkBox" ID="chkautoredeem" runat="server" />
                    This award shall be auto redeem by salary
                </td>
            </tr>
        </table>
        <div style="width: 100%; display: inline; float: left; padding-bottom: 10px;padding-top: 10px;">
            <div style="float: left;">
                Active
            </div>
            <span style="float: left;">:</span>
            <div style="width: 200px; float: left; padding-left:20px;">
                <asp:CheckBox ID="chkActive" CssClass="chkBox" runat="server" Checked="true" TabIndex="6">
                </asp:CheckBox></div>
        </div>
        <div style="padding-left: 40%;">
            <div class="btnRed">
                <asp:Button ID="btnSave" runat="server" CssClass="btnLeft" OnClick="btnSave_Click"
                    Text="Save" OnClientClick="return validateSave()" TabIndex="7" />
            </div>
            <div class="btnRed">
               <%-- <asp:Button ID="btnCancel" runat="server" CssClass="btnLeft" Text="CLEAR" OnClientClick="btnCancel_ClientClick();return false;" OnClick="btnCancel_Click" TabIndex="8"  />--%>
                  <asp:Button ID="btnClear" runat="server" CssClass="btnReset" Text="Clear" OnClientClick="btnCancel_ClientClick();return false;" OnClick="btnClear_Click" TabIndex="8"/>

            </div>
            <div class="btnRed">
                 
                 <asp:Button ID="btnCancel" runat="server" CssClass="btnReset" Text="Cancel"  OnClick="btnCancel_Click" TabIndex="9"  />
            </div>
        </div>
    </div>
    <div>
        <asp:HiddenField ID="hdnMode" Value="" runat="server" />
        <asp:HiddenField ID="hdnSelectedIndex" Value="" runat="server" />
        <asp:HiddenField ID="hdnIsSystem" Value="" runat="server" />
    </div>
</asp:Content>

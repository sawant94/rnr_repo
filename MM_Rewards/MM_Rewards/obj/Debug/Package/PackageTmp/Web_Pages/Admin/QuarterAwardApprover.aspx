﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="QuarterAwardApprover.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.QuarterAwardApprover" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
        .standard-input input {
            width: 98%;
            height: 47px;
            margin: 1px;
            border: 0px;
            padding: 10px 20px;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label input {
            display: inline-block;
            height: 100%;
            padding: 20px 9px;
            width: calc(100% - 54px);
            margin: 0;
            border: 0;
        }

        #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons {
            margin: 0;
            margin-left: 0;
            display: inline-block;
            vertical-align: top;
            height: 60px;
            width: 50px;
            float: none;
        }

            #ContentPlaceHolder1_searchempmodal .rnr-search-bar div.search-form label .SearchButtons input {
                background-image: url(../../assets/images/Search.png);
                background-repeat: no-repeat;
                padding-left: 28px !important;
                background-position: center;
                background-color: transparent !important;
                padding: 0!important;
                width: inherit;
                height: inherit;
                background-size: 22px;
                display: inline-block;
            }

        .send-ecard .nom-box-top h5 input {
            font-weight: 600;
            font-size: 20px;
            margin-bottom: 0;
            padding: 0;
        }

        .send-ecard .search-emp-m input {
            /*padding: 0;*/
            min-height: auto;
        }

        .send-ecard .search-emp-m ul input {
            padding: 0;
            font-size: 16px;
            width: 100%;
            color: #203442;
            font-weight: 500;
        }

        .send-ecard label.nom-box-lbl input {
            font-size: 11px;
            color: #fff!important;
            padding: 0;
        }

        .send-ecard .search-emp-m .send-ecard-message textarea {
            font-size: 18px;
            padding: 0 20px;
            min-height: 40px;
            border: 0;
            font-family: Calibri!important;
        }

        .send-awards.send-ecard input {
            margin: 0 auto;
            width: auto;
            padding: 10px 35px;
        }

        .send-awards input {
            width: 270px;
            padding: 9px;
            font-size: 20px;
            font-weight: 500;
            border-radius: 6px;
        }

        input.rnr-btn {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 110px;
        }
        input.rnr-btnGrid {
            background: linear-gradient(#61f5a3,#49d587);
            color: #fff;
            font-size: 16px;
            letter-spacing: .6px;
            text-transform: uppercase;
            font-weight: 600;
            padding: 8px 12px;
            text-decoration: none;
            box-shadow: 0 3px 6px #84DBAB80;
            border-radius: 3px;
            display: block;
            text-align: center;
            border: 0;
            min-width: 20px;
        }

        .rnr-green input {
            font-weight: 600;
            margin: 0 0 -4px;
            font-size: 35px;
            -webkit-appearance: none;
            background: transparent;
            border: 0;
            color: #30ce8d!important;
        }

        #ContentPlaceHolder1_TxtAmount {
            background-color: #eee;
            padding: 0 12px;
            width: 100%;
        }
    </style>
    <section class="main-rnr">
        <div class="rnr-inner-top" style="">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href="/Web_Pages/HomePage.aspx">HOME</a></li>
                    <li>AFLC Member Master</li>
                </ul>

            </div>
        </div>

        <div class="rnr-tabs" style="margin-top: -40px;">

            <h3 class="rnr-h3-top">AFLC Member Master</h3>
            <div class="rnr-tab-pane tab-content">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">    
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 rnr-input">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Name Of Sub AFLC Member</label>
                                    <asp:DropDownList ID="ddlSubAFLCMember" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6 rnr-input">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Role</label>
                                    <asp:DropDownList ID="ddlRole" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Value="N-1">AFLC Member (N-1)</asp:ListItem>
                                    <asp:ListItem Value="S">Secretory</asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6 rnr-input">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd">
                                    <label for="">Name Of AFLC Member</label>
                                    <asp:DropDownList ID="ddlAFLCMember" runat="server" Width="200px" CssClass="selectDivision" TabIndex="1">
                                    </asp:DropDownList>
                                </div>
                            </div>
                           </div>
                        </div>           
                    <div class="container">
                        <div class="row">
                        </div>
                    </div>
                    <div class="container" >
                        <div class="row">
                           
                            </div>
                        </div>
                    <div class="container">
                        <div class="row">
                        <div class="col-md-3" style="">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="200px" CssClass="rnr-btn" OnClick="btnSubmit_Click" />
                         </div>
                            <div class="col-md-3" style="">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="200px" CssClass="rnr-btn" OnClick="btnCancel_Click" />
                         </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
<%--    <script type="text/javascript">
        $(document).ready(function () {
            $('#ddlAFLCScretory').show;
            var value = $('#ddlRole').Val();
            if (value == 'S') { $('#ddlAFLCScretory').show}
        });
    </script>--%>
</asp:Content>

﻿<%@ Page Title="User Excel Upload - Budget" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="UserExcelUpload_Budget.aspx.cs" Inherits="MM_Rewards.Web_Pages.UserExcelUpload_Budget" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript">
        function checkFile() {
            var validFilesTypes = ["xls", "xlsx"];
            var fileName = document.getElementById("<%= FileUpload1.ClientID %>").value;
            var label = document.getElementById("<%=lblError.ClientID%>");
            var ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length).toLowerCase();
            var isValidFile = false;
            for (var i = 0; i < validFilesTypes.length; i++) {
                if (ext == validFilesTypes[i]) {
                    isValidFile = true;
                    break;
                }
            }
            if (!isValidFile) {
                label.innerHTML = "Invalid File. Please upload a File with" +
                " extension:\n\n" + validFilesTypes.join(", ");
                return isValidFile;
            }
            else {
                return isValidFile;
            }

        }
        function chkBlank() {
            var txtboxValue = document.getElementById("<%= txtSearchBox.ClientID %>").value;
            if (txtboxValue == "") {
                var label = document.getElementById("<%=lblError2.ClientID%>");
                label.innerHTML = "Enter the Token Number";
                return false;
            }
            else
                return true;
        }

        function chkBlanktxt() {
            var txtboxValue = document.getElementById("<%= txtTokenNumber.ClientID %>").value;
            var Calendar1 = $find("<%= telStartDate.ClientID %>");
            startDate = Calendar1.get_selectedDate();
            var Calendar2 = $find("<%= telEndDate.ClientID %>");
            var endDate = Calendar2.get_selectedDate();
            if (txtboxValue == "") {
                var label = document.getElementById("<%=LblErrorSave.ClientID%>");
                label.innerHTML = "Enter the Token Number";
                return false;
            }
            else {
                if (startDate < endDate) {
                    return true;
                }
                else {
                    var label = document.getElementById("<%=LblErrorSave.ClientID%>");
                    label.innerHTML = "Start Date Should be Smaller than End Date";
                    return false; 
                }
            }
                
        }
        function chkNumbers(key) {        

            var keycode = (key.which) ? key.which : key.keyCode;
            var budgettxt = document.getElementById('<%=txtBudget.ClientID%>');            
            if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <h1>ShubhLabh Employees Budget Management </h1>
    <br />
    <br />
      <br />
    <div id="MainDiv">
    <h1>UPLOAD SHUBHLABH EMPLOYEES BUDGET</h1>    
    <br />
        <div style="width:500px;">
       <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="10pt">Please Select Excel File to Upload </asp:Label>
    <br />
    <br />
    <table style="width: 98%;" align="center">
        <tr>
            <td align="right">
                <asp:FileUpload ID="FileUpload1" runat="server" TabIndex="1"/>
            </td>
            <td>
            <div class="btnRed"><asp:Button ID="BtnUpload" runat="server" Font-Bold="True" 
                    onclick="BtnUpload_Click" Text="Upload Excel"  CssClass="btnLeft" TabIndex="5"
                    CausesValidation="False" /></div>
            </td>
            
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblError" runat="server" Font-Italic="True" Text=""></asp:Label>                
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    </div>
    <br />
    <br />
      <br />
    <h1>SEARCH EMPLOYEES BUDGET </h1>    
    <br />
        <div id="searchBoxDiv" style="width:799px;">
          <table style="width: 652px">
              <tr>
              <td class="style2"><asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="10pt">
              Enter Token Number / Name <span style="color:Red;">*</span> </asp:Label></td>                  
              <td style="width:160px;"><asp:TextBox ID="txtSearchBox" runat="server" MaxLength="100" TabIndex="10"></asp:TextBox>
                  </td>
              <td ><div class="btnRed"><asp:Button ID="BtnSearch" runat="server" TabIndex="15"  CausesValidation="false" AccessKey="4" Text="Get Employee Details" Font-Bold="True" 
                      onclick="BtnSearch_Click" CssClass="btnLeft" /></div>                      
              </td>
              </tr>     
              <tr><td></td><td style="width:150px;"  colspan="2"><asp:Label ID="lblError2" runat="server" Font-Italic="True" Text=""></asp:Label>  </td></tr>               
              </table>
        </div>
    <br />
    <br />    
    <asp:Label runat="server" ID="lblDetailsList" Visible="false"  Font-Bold="true" Font-Italic="True" Text=""></asp:Label>
         <br /><br />
         <div id="GridDiv" class="grid01" style="width:100%;">         
           <asp:GridView ID="grdExcelUpload" runat="server" AllowPaging="True" AutoGenerateColumns="False"  DataKeyNames="TokenNumber"
                            PageSize='<%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                            AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdExcelUpload_PageIndexChanging"
                            OnRowDataBound="grdExcelUpload_RowDataBound" OnRowCommand="grdExcelUpload_RowCommand" >
                            <PagerStyle CssClass="dataPager" />
                            <SelectedRowStyle CssClass="selectedoddTd" />
                            <AlternatingRowStyle CssClass="row01"></AlternatingRowStyle>
                             <Columns>                                                          
                                <asp:BoundField DataField="TokenNumber"   HeaderText="Token Number"></asp:BoundField>
                                <asp:BoundField DataField="EmployeeName"   HeaderText="Employee Name"></asp:BoundField>
                                <asp:BoundField DataField="StartDate" DataFormatString="{0:dd/MM/yyyy}"  HeaderText="Start Date" HtmlEncodeFormatString="false" HtmlEncode="false"></asp:BoundField>
                                <asp:BoundField DataField="EndDate" DataFormatString="{0:dd/MM/yyyy}"  HtmlEncodeFormatString="false" HtmlEncode="false" HeaderText="End Date"></asp:BoundField>
                                <asp:BoundField DataField="Budget" HeaderText="Budget" DataFormatString="{0:0.00}"></asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton CssClass="editLink" ID="btnEdit" runat="server" Text="Edit" CommandName="Select"
                                            OnClick="btnEdit_Click" CommandArgument="TokenNumber"/>
                                    </ItemTemplate>
                               </asp:TemplateField>

                                </Columns>
           </asp:GridView>
           
         </div>

</div>            
<asp:Panel runat="server" ID="formUpdatePanel" Visible="false">
<div id="formEditDiv">
    <table class="style1">
     <tr>
            <td style="width:150px; height:30px;">
                <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtEmpName"  runat="server" TabIndex="19" ReadOnly="true"></asp:TextBox>  </td>
        </tr>
        <tr>
            <td style="width:150px; height:30px;">
                <asp:Label ID="lblTokenNumber" runat="server" Text="Token Number"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtTokenNumber" runat="server" TabIndex="20"></asp:TextBox>  </td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="lblFirstName" runat="server" Text="Start Date"></asp:Label>
            </td>           
            <td >
                <telerik:RadDatePicker ID="telStartDate" runat="server" DateInput-DateFormat="dd/MM/yyyy" TabIndex="4">
                <Calendar runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" Height="30px" ></Calendar>
                <DateInput runat="server" DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="8"></DateInput>
                 <DatePopupButton ImageUrl="" HoverImageUrl=""  TabIndex="22"></DatePopupButton>
                 </telerik:RadDatePicker>                
            </td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="lblLastName" runat="server" Text="End Date"></asp:Label>
            </td>
            <td colspan="3">
                <telerik:RadDatePicker ID="telEndDate" runat="server" DateInput-DateFormat="dd/MM/yyyy" TabIndex="4">
                <Calendar ID="Calendar1" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" Height="30px" ></Calendar>
                <DateInput ID="DateInput1" runat="server" DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="8"></DateInput>
                 <DatePopupButton ImageUrl="" HoverImageUrl=""  TabIndex="25"></DatePopupButton>
                 </telerik:RadDatePicker> 
            </td>
        </tr>        
         <tr>
            <td style="height:30px;">
                <asp:Label ID="lblBudget" runat="server" Text="Budget"></asp:Label>
            </td>
            <td colspan="3">
              <asp:TextBox ID="txtBudget" runat="server" TabIndex="28" onkeypress="return chkNumbers(event)"></asp:TextBox>
</td>
        </tr>
        
         <tr>
            <td>
                &nbsp;</td>
            <td colspan="3">
                &nbsp;</td>
        </tr>
         <tr>
            <td colspan=4 align="center">
              <div class="btnRed"><asp:Button ID="BtnSave" runat="server"  CausesValidation="false" TabIndex="30"  Text="Save" Font-Bold="True" 
               onclick="BtnSave_Click" CssClass="btnLeft" /></div>
               &nbsp;
               <div class="btnRed"><asp:Button ID="BtnCancel" runat="server" TabIndex="32"  CausesValidation="false"  Text="Cancel" Font-Bold="True" 
               onclick="BtnCancel_Click" CssClass="btnLeft" /></div>
               &nbsp;&nbsp;
                </td>
        </tr>
        <tr>
           <td></td>
        </tr>
    </table>


</div>  
</asp:Panel>
<div style="margin-left:200px;"><asp:Label ID="LblErrorSave" runat="server" Font-Italic="True"></asp:Label></div>
    <style type="text/css">        

   .hideGridColumn
    {
        display:none;
    }
 
     
        .style1
        {
            width: 100%;
        }

   
 
     
        .style2
        {
            height: 63px;
            width: 205px;
        }
        

   
 
     
    </style>
  
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fileupload.aspx.cs" Inherits="MM_Rewards.Fileupload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <link href="includes/css/master.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function flupload_onchange() {

            var flupload = document.getElementById('<%=flupload.ClientID %>');
            var rExp = "^.+(.xlsx|.XLSX|.xls|.XLS)$";
            if (flupload.value == "") {
                alert("Please upload a file first!");
                return false;
            }
            else {
                if (!flupload.value.match(rExp)) {
                    alert("Only xlsx file is allowed!");
                    flupload.value = "";
                    return false;
                }
            } 
        }
   
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="popup">
        <h1>
            Upload Excel</h1>
        <div class="form01">
            <div>
                <asp:FileUpload ID="flupload" runat="server" filter=".xsls" onchange=" return flupload_onchange();" /></div>
            <%--<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ErrorMessage="Only xlsx file is allowed!"
                        ValidationExpression ="^.+(.xlsx|.XLSX)$" ControlToValidate="flupload" > </asp:RegularExpressionValidator>--%>
           <div>&nbsp;</div>
            <div class="btnRed"  >
                <asp:Button ID="btnupload" runat="server" OnClick="btnupload_Click" Text="upload" OnClientClick="return flupload_onchange()"
                    CssClass="btnLeft" /></div>

         <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" Text="" ErrorMessage=" Please upload a file first!"
                Display="Dynamic" ControlToValidate="flupload" SetFocusOnError="true">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                    ShowMessageBox="true" ShowSummary="false" />
            </asp:RequiredFieldValidator>--%>
            <div>&nbsp;</div> <div>&nbsp;</div> <div>&nbsp;</div> <div>&nbsp;</div>
            <div id="divGrid" class="grid01">
                <asp:GridView ID="grdAwards" runat="server" AllowPaging="true" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                    AutoGenerateColumns="false" DataKeyNames="ID" OnPageIndexChanging="grdAwards_PageIndexChanging"
                    AlternatingRowStyle-CssClass="row01">
                    <Columns>
                        <asp:BoundField DataField="AwardID" HeaderText="AwardID" SortExpression="AwardID" />
                        <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName"
                            Visible="false" />
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                        <asp:BoundField DataField="RecipientTokenNumber" HeaderText="RecipientTokenNumber"
                            SortExpression="RecipientTokenNumber" />
                        <asp:BoundField DataField="RecipientName" HeaderText="RecipientName" SortExpression="RecipientName" />
                        <%--<asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkRedeem" runat="server" Text="Redeem" Visible='<%# Eval("RedeemDate") == null? true : false %>'
                                            OnClientClick='<%# String.Format("lnkRedeem_ClientClick({0});return false;", Eval("ID")) %>'></asp:LinkButton>
                                        <asp:Label ID="lblReedemed" runat="server" Text="Redeemed" Visible='<%# Eval("RedeemDate") == null? false : true %>'
                                             ForeColor="GrayText"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                    </Columns>
                    <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                        BorderStyle="Solid" Font-Italic="true" />
                    <EmptyDataTemplate>
                        <asp:Literal ID="ltrlgrdAwards" runat="server" Text="No records to display"></asp:Literal>
                    </EmptyDataTemplate>
                    <PagerStyle CssClass="dataPager" />
                    <SelectedRowStyle CssClass="selectedoddTd" />
                </asp:GridView>
            </div>
        </div>
    </div>
    </form>
</body>
</html>

﻿<!doctype html>
<html lang="en-US" class="no-js">

<head>
    <title>Login</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/includes/css/font-awesome.min.css">
    <link href="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/includes/css/freaking-custom.css" rel="stylesheet">
    <link href="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/includes/css/animate.css" rel="stylesheet" type="text/css">
    <link href="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/includes/css/bootstrap.min.css" rel="stylesheet">
    <link href="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/includes/css/style.css" rel="stylesheet" type="text/css">
    <link href="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/includes/scss/responsive.css" rel="stylesheet" type="text/css">
    <style>

    </style>
</head>
<body class="login_page">
<br/>
<br/>
<br/>
    <table class="" style="width:600px;margin: 0 auto; border: 1px solid #f7f7f7;padding-top: 25px">
        <tr>
            <td style="vertical-align: top;">
                <img src="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/Templates/images/logo.png" class="img-responsive mlogo" style="width: 280px;" />
            </td>
            <td style="vertical-align: top; text-align: right;">
                <img src="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/Templates/images/mlogo.png" class="img-responsive melogo" style="width: 90px; display: inline-block;margin-top: 5px; margin-right: 10px;" /></td>
        </tr>
        <tr>
            <td style="vertical-align: top;" colspan="2">
                <img src="D:/Rakesh/RNR/MM_Rewards/MM_Rewards/Templates/images/card1.png" class="img-responsive card1" style="display: block; margin-top: -25px;" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #d9d9d9;">
                <p style="text-align: center; margin: 15px 0;">
                    <span style="text-transform: uppercase;font-size: 22px; color: #e31837; display: inline-block;font-weight: 600; vertical-align: middle;">thank you</span><span style="width: 240px;display: inline-block;vertical-align: middle;padding-bottom: 5px;font-size: 18px;border-bottom: 2px solid #949494;margin: 0 10px;">#Name</span>
                </p>
                <p style="text-align: center; margin: 15px 0;">
                    <span style="text-transform: uppercase;font-size: 14px; color: #e31837; display: inline-block;font-weight: 600; vertical-align: middle;">for</span><span style="width: 380px;display: inline-block;vertical-align: middle;padding-bottom: 5px;font-size: 18px;border-bottom: 2px solid #949494;margin: 0 10px;">#Message.</span>
                </p>
                <p style="text-align: center; font-size: 17px; font-weight: 700; margin: 20px 0 30px;">Let’s continue to #LearnGrowRise</p>
            </td>
        </tr>
    </table>
</body>

</html>

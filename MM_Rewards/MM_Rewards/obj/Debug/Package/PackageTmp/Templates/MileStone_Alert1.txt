<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Milestone Alert1</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <table id="Table_01" width="1000" height="1415" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_01.jpg" width="1000" height="207"
                    alt="">
            </td>
        </tr>
        <tr>
            <td>
                <img style="vertical-align: top" src="images/Emailer_02.jpg" width="87" height="436"
                    alt="">
            </td>
            <td style="width: 795px; vertical-align: top">
                <div style="width: 760px; height: 417px; font-family: arial; font-size: 17px; color: #323231;
                    padding: 0 0 7px 7px; line-height: 35px; vertical-align: top;">
                    <span style="font-weight: bold; color: rgb(169, 50, 52); font-size: 42px; text-transform:capitalize;">Dear
                        <%HODName%>,</span><br>
                    <span style="display: block; color: rgb(0, 0, 0); font-size: 30px;">
                        <br />
                        Congratulations!!!</span>
                    <p style="color: #606062; font-size: 24px">
                        This is to inform you that
                        <%RecipientName%>
                        has been awarded with Rs.
                        <%AwardAmount%>. on completing
                        <%Years%>
                        years of service with Mahindra and Mahindra on<%MileStoneDate%>.</p>
                 </div>
            </td>
            <td>
                <img style="vertical-align: top; float: left" src="images/Emailer_04.jpg" width="121"
                    height="436" alt="">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="images/Emailer_05_disclaimer3.jpg" alt="" width="1000" height="288" usemap="#Map" style="vertical-align: top"
                    border="0">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_06_disclaimer3.jpg" width="1000" height="184"
                    alt="">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_07.jpg" width="1000" height="300"
                    alt="">
            </td>
        </tr>
    </table>
    <map name="Map">
        <area shape="rect" coords="114,256,206,278" target="_blank" href="<%PersonalMsgLink%>">
    </map>
</body>
</html>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BL = Idealake.Mahindra.RewardsSocial;
using DAL = Idealake.Mahindra.RewardsSocial.Data;
using System.Web.Configuration;

namespace Idealake.Web.MMRewardsSocial.Core
{
    public class WebContext : DAL.DBContext
    {
        private static WebContext current;
        public static WebContext Current
        {
            get
            {
                if (current == null)
                    current = new WebContext();

                return current;
            }
        }

        public override BL.User CurrentUser
        {
            get
            {
                BL.User currentUser;
                //if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["demouser"]))
                //{
                //    currentUser = GetUserByTokenNumber(WebConfigurationManager.AppSettings["demouser"]);
                //}
                //else
                //{
                    string currentUserName = HttpContext.Current.User.Identity.Name;
                    if (HttpContext.Current.Session["TokenNumber"] != null)
                    {
                        currentUser = GetUserByTokenNumber(HttpContext.Current.Session["TokenNumber"].ToString());
                    }
                    else
                    {
                        string currentUserToken = currentUserName.Substring(currentUserName.IndexOf(@"\") + 1);
                        currentUser = GetUserByTokenNumber(currentUserToken);
                    }
                //}
                return currentUser;
            }
        }

        private WebContext()
            : base(WebConfigurationManager.ConnectionStrings["MMRewardsSocialDB"].ConnectionString)
        {

        }
    }
}
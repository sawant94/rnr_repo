<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Web service Gift Hamper Details</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <table id="Table_01" width="1000" height="1127" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_01_disclaimer1.jpg" width="1000"
                    height="207" alt="">
            </td>
        </tr>
        <tr>
            <td>
                <img style="vertical-align: top" src="images/Emailer_02_disclaimer1.jpg" width="87"
                    height="620" alt="">
            </td>
            <td style="width: 796px; vertical-align: top">
                <div style="width: 760px; height: 417px; font-family: arial; font-size: 17px; color: #323231;
                    padding: 0 0 7px 7px; line-height: 33px; vertical-align: top;">
                    <span style="font-weight: bold; color: rgb(169, 50, 52); font-size: 42px; text-transform: capitalize;">
                        Dear
                        <%MailReceiverName%>, </span>
                   <br>
                    <br>
                    <div style="border: 1px solid rgb(204, 204, 204); min-height: 200px; width: 740px;
                        padding: 0px 10px; font-size: 15px;">
                        <p>
                                 BenefitsPLUS service are not working following are Request Details<br>
                            Request Id -
                            <%RequestId%><br>
                            Request Date -
                            <%RequestDate%><br>
                            Nominator Name -
                            <%NominatorName%><br>
                            Nominator EmpID -
                            <%NominatorEmpID%><br>
                            Recipient Name -
                            <%RecipientName%><br>
                            Recipient EmpId -
                            <%RecipientEmpId%><br>
                            Delivery Address -
                            <%DeliveryAddress%><br>
                            Pincode -
                            <%Pincode%><br>
                            Gift Amount -
                            <%GiftAmount%><br>
                            Request Status -
                            <%RequestStatus%><br>
                           
                        </p>
                    </div>
                </div>
            </td>
            <td>
                <img style="vertical-align: top; float: left" src="images/Emailer_04_disclaimer1.jpg"
                    width="121" height="620" alt="">
            </td>
        </tr>
        <!--<tr><td colspan="3"><img  src="images/Emailer_05.jpg" alt="" width="1000" height="288" usemap="#Map" style="vertical-align:top" border="0"></td>	</tr>-->
        <!--<tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_06_disclaimer1.jpg" width="1000"
                    height="184" alt="">
            </td>
        </tr>-->
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_08_disclaimer1.jpg" width="1000"
                    height="300" alt="">
            </td>
        </tr>
    </table>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BL_Rewards.BL_Rewards;
using BE_Rewards.BE_Rewards;
using System.Net;
using System.Web.Services.Protocols;


namespace MM_Rewards
{
    /// <summary>
    /// Summary description for GiftHamper1 
    /// </summary>

    [WebService(Namespace = "http://www.tempri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
 
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class MahindraAwardService : System.Web.Services.WebService
    {
        //BP_AwardService.MahindraService servLive = new BP_AwardService.MahindraService();
        BPGVService.MahindraService servLive = new BPGVService.MahindraService();
        //MahindraService1.MahindraService servLive = new MahindraService1.MahindraService();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        [WebMethod]
        public int GetRequestStatus(string RequestId, string RequestStatus)
        {
            BL_RecipientAward objRecipientAward = new BL_RecipientAward();
            BE_GiftHampers objBE_GiftHampers = new BE_GiftHampers();
            try
            {
                if (objRecipientAward.Check_RequestExist(RequestId) == 1)
                {
                    objBE_GiftHampers.RequestId = RequestId;
                    objBE_GiftHampers.RequestStatus = RequestStatus;
                    return objRecipientAward.UpdateRequestStatus(objBE_GiftHampers);
                }
                return 1;
            }
            catch (Exception objException)
            {
                objExceptionLogging.LogErrorMessage(objException, "MahindraAwardService.asmx");
                return 99;
            }
        }

        [WebMethod]
        public int UpdatePoints(string RequestId, string NomEmailID, string Points_Assigned, string Points_Used, string Bal, string Award_Id)
        {
            BL_RecipientAward objRecipientAward = new BL_RecipientAward();
            BE_GiftHampers objBE_GiftHampers = new BE_GiftHampers();
            try
            {
                if (objRecipientAward.Check_RequestExist(RequestId) == 1)
                {
                    objBE_GiftHampers.RequestId = RequestId;
                    objBE_GiftHampers.NomEmailID = NomEmailID;
                    objBE_GiftHampers.PointsAssigned = Points_Assigned;
                    objBE_GiftHampers.PointsUsed = Points_Used;
                    objBE_GiftHampers.Balance = Bal;
                    objBE_GiftHampers.AwardId = Award_Id;
                    return objRecipientAward.UpdateRequestPoint(objBE_GiftHampers);
                }
                return 1;
            }
            catch (Exception objException)
            {
                objExceptionLogging.LogErrorMessage(objException, "MahindraAwardService.asmx");
                return 99;
            }
            // mail for  budget 
        }

        [WebMethod]
        public string SendRequestDetails(BE_GiftHampers objBE_GiftHampers)
        {
            try
            {
                var objService = servLive.GVInsert
                (
                    objBE_GiftHampers.RequestId,
                    objBE_GiftHampers.RequestDate,
                    objBE_GiftHampers.NomName,
                    // "301114",
                    objBE_GiftHampers.NomEmpId,
                    objBE_GiftHampers.NomEmailID,
                    objBE_GiftHampers.RecName,
                    // "213120",
                    objBE_GiftHampers.RecEmpId,
                    objBE_GiftHampers.RecEmailID.ToLower(),
                    Convert.ToInt64(Convert.ToDouble(objBE_GiftHampers.GiftAmount)).ToString(),
                    objBE_GiftHampers.RequestStatus,
                    //"MAHINDRA HEAVY ENGINES (P) LTD",
                    //"Auto-MHEL",
                    //"Mumbai",
                    //string.Empty,
                    //string.Empty,
                    //string.Empty,

                    objBE_GiftHampers.AwardBusinessUnit,
                    objBE_GiftHampers.AwardDivision,
                    objBE_GiftHampers.AwardLocation,
                    "GIFT HAMPER",
                    //"",
                    "69",
                    "71Aaian3IDlqOBl4aPL12zpWLkePCiX7rS1kRMOdglE="
                );
                return objService.InnerText.ToString();
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        [WebMethod]
        public int GetVoucherRequestStatus(string RequestId, string RequestStatus)
        {
            BL_RecipientAward objRecipientAward = new BL_RecipientAward();
            // BE_GiftHampers objBE_GiftHampers = new BE_GiftHampers();
            BE_GiftVouchers objBE_GiftVouchers = new BE_GiftVouchers();
            try
            {
                if (objRecipientAward.Check_VoucherRequestExist(RequestId) == 1)
                {
                    objBE_GiftVouchers.RequestId = RequestId;
                    objBE_GiftVouchers.RequestStatus = RequestStatus;
                    objExceptionLogging.LogUserDefinedMessage(RequestId + RequestStatus + "record inserted successful in web service voucher");
                    return objRecipientAward.UpdateRequestStatusVoucher(objBE_GiftVouchers);
                }
                return 1;
            }
            catch (Exception objException)
            {
                objExceptionLogging.LogErrorMessage(objException, "MahindraAwardService.asmx");
                objExceptionLogging.LogErrorMessage(objException, "Failed to insert value in web service voucher");
                return 99;
            }
        }

        [WebMethod]
        public string SendVoucherRequestDetails(BE_GiftVouchers objBE_GiftVouchers)
        {
            try
            {
                var objService = servLive.GVInsert
                (
                    objBE_GiftVouchers.RequestId,
                    objBE_GiftVouchers.RequestDate,
                    objBE_GiftVouchers.NomName,
                    objBE_GiftVouchers.NomEmpId,
                    objBE_GiftVouchers.NomEmail,
                    objBE_GiftVouchers.RecName,
                    objBE_GiftVouchers.RecEmpId,
                    objBE_GiftVouchers.RecEmail,
                    Convert.ToInt64(Convert.ToDouble(objBE_GiftVouchers.GiftAmount)).ToString(),
                    objBE_GiftVouchers.RequestStatus,
                    objBE_GiftVouchers.AwardBusinessUnit,
                    objBE_GiftVouchers.AwardDivision,
                    objBE_GiftVouchers.AwardLocation,
                    "GIFT VOUCHER",
                    //"",
                    "69",
                    "71Aaian3IDlqOBl4aPL12zpWLkePCiX7rS1kRMOdglE="
                );
                return objService.InnerText.ToString();
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        [WebMethod]
        public string SendRequestDetails_ESM(BE_GiftVouchers objBE_ESM)
        {
            try
            {
                var objService = servLive.GVInsert

                (
                    objBE_ESM.RequestId,
                    objBE_ESM.RequestDate,
                    objBE_ESM.NomName,
                    objBE_ESM.NomEmpId,
                    objBE_ESM.NomEmail,
                    objBE_ESM.RecName,
                    objBE_ESM.RecEmpId,
                    objBE_ESM.RecEmail,
                    objBE_ESM.GiftAmount,
                    //Convert.ToInt32(objMailDetails.AwardAmount);
                    // Convert.ToInt64(Convert.ToDouble(objBE_ESM.GiftAmount)).ToString(),
                    objBE_ESM.RequestStatus,
                    objBE_ESM.AwardBusinessUnit,
                    objBE_ESM.AwardDivision,
                    objBE_ESM.AwardLocation,
                    "GIFT VOUCHER", //gourav:Spot Gift Hamper:0:5
                    //objBE_ESM.AwardID,
                    "69",
                    "71Aaian3IDlqOBl4aPL12zpWLkePCiX7rS1kRMOdglE="
                );

                return objService.InnerText.ToString();
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        //added by nilesh j 23-12-2014
        [WebMethod]
        public string SendRequestDetailsKind(BE_SpotKind ObjBESpotKind)
        {
            try
            {
                var objService = servLive.GVInsert
                (
                    ObjBESpotKind.RequestId,
                    ObjBESpotKind.RequestDate,
                    ObjBESpotKind.NomName,
                    ObjBESpotKind.NomEmpId,
                    ObjBESpotKind.NomEmailID,
                    ObjBESpotKind.RecName,
                    ObjBESpotKind.RecEmpId,
                    ObjBESpotKind.RecEmailID.ToLower(),
                    Convert.ToInt64(Convert.ToDouble(ObjBESpotKind.GiftAmount)).ToString(),
                    ObjBESpotKind.RequestStatus,
                    ObjBESpotKind.AwardBusinessUnit,
                    ObjBESpotKind.AwardDivision,
                    ObjBESpotKind.AwardLocation,
                    "SPOT CASH",
                    "69",
                    "71Aaian3IDlqOBl4aPL12zpWLkePCiX7rS1kRMOdglE="
                );
                return objService.InnerText.ToString();
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        //added by nilesh j 07-01-2015
        [WebMethod]
        public int GetKindRequestStatus(string RequestId, string RequestStatus)
        {
            BL_RecipientAward objRecipientAward = new BL_RecipientAward();
            BE_SpotKind objBE_SpotKind = new BE_SpotKind();
            try
            {
                if (objRecipientAward.Check_KindRequestExist(RequestId) == 1)
                {
                    objBE_SpotKind.RequestId = RequestId;
                    objBE_SpotKind.RequestStatus = RequestStatus;
                    return objRecipientAward.UpdateRequestStatusKind(objBE_SpotKind);
                }
                return 1;
            }
            catch (Exception objException)
            {
                objExceptionLogging.LogErrorMessage(objException, "MahindraAwardService.asmx");
                return 99;
            }
        }

        //Pre-authentication only applies after the Web service successfully authenticates the first time. Pre-authentication has no impact on the first Web request.

        //private void ConfigureProxy(WebClientProtocol proxy,
        //                             string domain, string username,
        //                             string password)
        //{
        //    // To improve performance, force pre-authentication
        //    username = "rnr";
        //    password = "pass@123";
        //    domain = "Mahindra";
        //    proxy.PreAuthenticate = true;
        //    // Set the credentials
        //    CredentialCache cache = new CredentialCache();
        //    cache.Add(new Uri(proxy.Url),
        //               "Negotiate",
        //                new NetworkCredential(username, password, domain));
        //    proxy.Credentials = cache;
        //    proxy.ConnectionGroupName = username;
        //}



    }
}

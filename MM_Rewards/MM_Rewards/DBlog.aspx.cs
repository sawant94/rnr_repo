﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MM.DAL;


namespace MM_Rewards
{
    public partial class DBlog : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLog = new ExceptionLogging();
         
        protected void Page_Load(object sender, EventArgs e)
        {

            
        }
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "IDName", script, true);
        }

        public  void query_result()
        {
            try
            {
                
                string query = txtQuery.Text;
                DataSet ds = new DataSet();
                ds = DAL_Common.GetDataFromSQL(query);
                grd_queryresult.DataSource = ds;
                grd_queryresult.DataBind();
            }

            catch (Exception ex)
            {
                ThrowAlertMessage(ex.Message);
                objExceptionLog.LogErrorMessage(ex, "DBlog.aspx");

            }
           
        }

        protected void btnresult_Click(object sender, EventArgs e)
        {
            query_result();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Common;
using BE_Rewards.BE_Admin;
using System.Data;
using Microsoft.Reporting.WebForms;
namespace MM_Rewards.UserControl
{
    //-----------------------------------------------------------------------------------------------------
    public partial class RnrReportControl : System.Web.UI.UserControl
    {
        bool Isrnr;
        StringBuilder sbDiv = new StringBuilder();
        StringBuilder sb, sbLoc, sbDept, sbBusibess;
        //-----------------------------------------------------------------------------------------------------
        #region properties
        //-----------------------------------------------------------------------------------------------------
        public string Heading { get; set; }
        public string Rid { get; set; }
        //-----------------------------------------------------------------------------------------------------
        #endregion
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        #region Function
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        private void LoadBusinessDropDown()
        {
            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();
            rlbBusiness.DataSource = objCommon.LoadReportBusinessDropDown();
            rlbBusiness.DataTextField = "BusinessUnit";
            rlbBusiness.DataBind();
            rlbBusiness.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
        }
        //-----------------------------------------------------------------------------------------------------
        private void LoadDivisionDropDown()
        {
            sbBusibess = new StringBuilder();
            if (rlbBusiness.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }
                rlbdivision.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, sbBusibess.ToString().Trim(','), Session["TokenNumber"].ToString());
                rlbdivision.DataValueField = "DivisionCode_PA";
                rlbdivision.DataTextField = "DivisionName_PA";
                rlbdivision.DataBind();
                rlbdivision.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }
            else
            {
                rlbdivision.Items.Clear();
                rlbLocation.Items.Clear();
            }
        }
        //-----------------------------------------------------------------------------------------------------
        private void LoadLocationDropDown()
        {
            sb = new StringBuilder();
            if (rlbdivision.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sb.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                rlbLocation.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), sb.ToString().Trim(','));
                rlbLocation.DataValueField = "LocationCode_PSA";
                rlbLocation.DataTextField = "LocationName_PSA";
                rlbLocation.DataBind();
                rlbLocation.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }
            else
            {
                rlbLocation.Items.Clear();
            }
        }
        //-----------------------------------------------------------------------------------------------------
        private void LoadDepartmentDropDown()
        {
            if (rlbdivision.CheckedItems.Count > 0 && rlbLocation.CheckedItems.Count > 0 && rlbBusiness.CheckedItems.Count > 0)
            {
                sbLoc = new StringBuilder();
                sbBusibess = new StringBuilder();
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbBusibess.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }
                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sbDiv.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                if (rlbLocation.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbLoc.Append(rlbLocation.CheckedItems[0].Value);
                }
                else
                {
                    for (int j = 0; j < rlbLocation.CheckedItems.Count; j++)
                    {
                        sbLoc.Append(rlbLocation.CheckedItems[j].Value + ",");
                    }
                }
                ViewState["sbBusibess"] = sbBusibess.ToString().Trim(',');
                ViewState["sbDiv"] = sbDiv.ToString().Trim(',');
                ViewState["sbLoc"] = sbLoc.ToString().Trim(',');
            }
        }
        //-----------------------------------------------------------------------------------------------------
        #region backup
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        #endregion backup
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        #endregion
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        #region Events
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Common objCommon = new Common();
                    LoadBusinessDropDown();
                    if (Rid != null)
                    {
                        switch (Rid)
                        {
                            case "RnR":
                                {
                                    Heading = "RnR Report";
                                    hndMethodName.Value = "GetRnr_Report";
                                    hndReportpath.Value = @"Web_Pages\Reports\Rpt_RnrReprt.rdlc";
                                    hndDatasSetName.Value = "Rnr_Report";
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    if (Session["TokenNumber"] == null)
                    {
                        Response.Redirect("/Web_Pages/HomePage.aspx");
                    }
                    if (!string.IsNullOrEmpty(Heading))
                    {
                        lbltitle.Text = Heading;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        #region backup
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        #endregion backup
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Reports.Reports objRewards = new BL_Rewards.BL_Reports.Reports();
                List<BE_RnrReport> lstReports = new List<BE_RnrReport>();
                ObjectDataSource obj = new ObjectDataSource("BL_Rewards.BL_Reports.Reports", hndMethodName.Value);
                obj.SelectParameters.Add(new Parameter("Business", DbType.String, ViewState["sbBusibess"].ToString()));
                obj.SelectParameters.Add(new Parameter("Division", DbType.String, ViewState["sbDiv"].ToString()));
                obj.SelectParameters.Add(new Parameter("Location", DbType.String, ViewState["sbLoc"].ToString()));
                obj.SelectParameters.Add(new Parameter("start", DbType.String, Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("end", DbType.String, Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.OldValuesParameterFormatString = "original_{0}";
                obj.DataBind();
                ReportDataSource rds = new ReportDataSource(hndDatasSetName.Value, obj);
                AllAwardrptviewer.LocalReport.DataSources.Clear();
                AllAwardrptviewer.LocalReport.DataSources.Add(rds);
                AllAwardrptviewer.LocalReport.ReportPath = hndReportpath.Value;
                AllAwardrptviewer.LocalReport.Refresh();
                rpthold.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected void rlbBusiness_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadDivisionDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected void rlbdivision_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadLocationDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected void rlbLocation_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadDepartmentDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        #endregion
        //-----------------------------------------------------------------------------------------------------
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_Rewards.BL_Common;
using BE_Rewards.BE_Rewards;
using Microsoft.Reporting.WebForms;
using System.Text;
using BE_Rewards.BE_Admin;
using System.Data;

namespace MM_Rewards.UserControl
{
    public partial class ReportControl1 : System.Web.UI.UserControl
    {
        bool Isrnr;
        StringBuilder sbDiv = new StringBuilder();//, sbDiv,
        StringBuilder sb, sbLoc, sbDept, sbBusibess;
        #region properties
        public string Heading { get; set; }//  page heading
        public string Rid { get; set; }// Report id 
        #endregion

        #region Function

        //Get item for Business unit drop down for current user
        private void LoadBusinessDropDown()
        {
            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();

            rlbBusiness.DataSource = objCommon.LoadReportBusinessDropDown();
            rlbBusiness.DataTextField = "BusinessUnit";
            rlbBusiness.DataBind();
            rlbBusiness.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
        }

        //Get item for Division drop down for current user
        private void LoadDivisionDropDown()
        {
            sbBusibess = new StringBuilder();

            if (rlbBusiness.CheckedItems.Count > 0)
            {

                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();


                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }

                rlbdivision.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, sbBusibess.ToString().Trim(','), Session["TokenNumber"].ToString());
                rlbdivision.DataValueField = "DivisionCode_PA";
                rlbdivision.DataTextField = "DivisionName_PA";
                rlbdivision.DataBind();
                rlbdivision.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }

            else
            {
                rlbdivision.Items.Clear();
                rlbLocation.Items.Clear();
            }
        }

        /*Inserted by kiran*/
        private void LoadLocationDropDown()
        {
            sb = new StringBuilder();
            if (rlbdivision.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                    //for (int a = 0; a < rlbdivision.Items.Count; a++)
                    //{
                    //    rlbdivision.Items[a].Checked = true;
                    //}
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sb.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                rlbLocation.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), sb.ToString().Trim(','));
                rlbLocation.DataValueField = "LocationCode_PSA";
                rlbLocation.DataTextField = "LocationName_PSA";
                rlbLocation.DataBind();
                rlbLocation.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }
            else
            {
                /*Inserted by Kiran*/
                rlbLocation.Items.Clear();
                //  rlbDepartment.Items.Clear();
            }
        }

        private void LoadDepartmentDropDown()
        {
            if (rlbdivision.CheckedItems.Count > 0 && rlbLocation.CheckedItems.Count > 0 && rlbBusiness.CheckedItems.Count > 0)
            {
                //sbDiv = new StringBuilder();
                sbLoc = new StringBuilder();
                sbBusibess = new StringBuilder();
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();

                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbBusibess.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }

                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                    //for (int a = 0; a < rlbdivision.Items.Count; a++)
                    //{
                    //    rlbdivision.Items[a].Checked = true;
                    //}
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sbDiv.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                if (rlbLocation.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbLoc.Append(rlbLocation.CheckedItems[0].Value);
                    //for (int a = 0; a < rlbLocation.Items.Count; a++)
                    //{
                    //    rlbLocation.Items[a].Checked = true;
                    //}
                }
                else
                {
                    for (int j = 0; j < rlbLocation.CheckedItems.Count; j++)
                    {
                        sbLoc.Append(rlbLocation.CheckedItems[j].Value + ",");
                    }
                }
                //rlbDepartment.DataSource = objCommon.LoadDepartmentDropDown(sbLoc.ToString().Trim(','), sbDiv.ToString().Trim(','), Session["TokenNumber"].ToString());
                //rlbDepartment.DataValueField = "DivisionCode_PA";
                //rlbDepartment.DataTextField = "DivisionName_PA";
                //rlbDepartment.DataBind();
                //rlbDepartment.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));


                ViewState["sbBusibess"] = sbBusibess.ToString().Trim(',');
                ViewState["sbDiv"] = sbDiv.ToString().Trim(',');
                ViewState["sbLoc"] = sbLoc.ToString().Trim(',');
            }
            //else
            //{
            //    rlbDepartment.Items.Clear();
            //}
        }


        /*End*/

        #region backup
        //Get item for Division drop down for current user
        //private void LoadDivisionDropDown()
        //{

        //    BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
        //    BL_Rewards.BL_Common.Common objCommon = new Common();

        //    ddl_division.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, Session["TokenNumber"].ToString());
        //    ddl_division.DataValueField = "DivisionCode_PA";
        //    ddl_division.DataTextField = "DivisionName_PA";
        //    ddl_division.DataBind();
        //    hndIsrnr.Value = Isrnr.ToString();
        //    ddl_division.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
        //    ddl_division.Items.Insert(1, new ListItem("ALL", "ALL"));
        //    //if (!Isrnr)// if not an rnr user
        //    //{
        //    //    ddl_division.Items.Insert(1, new ListItem("ALL", "ALL"));
        //    //}

        //    ddl_division.SelectedIndex = 0;
        //}
        // Get location drop down item as per selected value for Division for current user
        //private void LoadLocationDropDown()
        //{
        //    if (ddl_division.SelectedIndex > 0)
        //    {
        //        BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
        //        BL_Rewards.BL_Common.Common objCommon = new Common();
        //        ddl_Location.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), ddl_division.SelectedValue.ToString());
        //        ddl_Location.DataValueField = "LocationCode_PSA";
        //        ddl_Location.DataTextField = "LocationName_PSA";
        //        ddl_Location.DataBind();
        //        ddl_Location.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
        //        ddl_Location.Items.Insert(1, new ListItem("ALL", "ALL"));
        //        //if (!(Convert.ToBoolean(hndIsrnr.Value))) // if not an rnr user
        //        //{
        //        //    ddl_Location.Items.Insert(1, new ListItem("ALL", "ALL"));
        //        //}
        //        ddl_Location.SelectedIndex = 0;
        //    }
        //    else
        //    {
        //        ddl_Location.Items.Clear();
        //    }
        //}
        // Get department drop down item  as per selected value for Division and location for current user
        //private void LoadDepartmentDropDown()
        //{
        //    if
        //        (ddl_division.SelectedIndex != 0 && ddl_Location.SelectedIndex != 0)
        //    {
        //        BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
        //        BL_Rewards.BL_Common.Common objCommon = new Common();
        //        ddl_Department.DataSource = objCommon.LoadDepartmentDropDown(ddl_Location.SelectedValue, ddl_division.SelectedValue, Session["TokenNumber"].ToString());
        //        ddl_Department.DataValueField = "DivisionCode_PA";
        //        ddl_Department.DataTextField = "DivisionName_PA";
        //        ddl_Department.DataBind();
        //        ddl_Department.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
        //        ddl_Department.Items.Insert(1, new ListItem("ALL", "ALL"));
        //        //if (!(Convert.ToBoolean(hndIsrnr.Value)))// not an rnr user
        //        //{
        //        //    ddl_Department.Items.Insert(1, new ListItem("ALL", "ALL"));
        //        //}
        //        ddl_Department.SelectedIndex = 0;
        //    }
        //    else
        //    {
        //        ddl_Department.Items.Clear();
        //    }
        //}
        #endregion backup
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Common objCommon = new Common();
                    //   LoadDivisionDropDown();
                    LoadBusinessDropDown();
                    if (Rid != null)
                    {
                        //string qid=Request.QueryString["qid"].ToString();
                        switch (Rid) //set report properties by comparing Rid  in respective hidden field 
                        {
                            case "ALL_KND":
                                {


                                    Heading = "Kind Award";
                                    hndMethodName.Value = "GetKindAward";
                                    hndReportpath.Value = @"Web_Pages\Reports\KindAward.rdlc";
                                    hndDatasSetName.Value = "Kind_DetailDS";
                                    break;
                                }
                            case "ALL_AWD":
                                {
                                    Heading = "ALL AWARDS";
                                    hndMethodName.Value = "GetAllAward";
                                    hndReportpath.Value = @"Web_Pages\Reports\AllAwards.rdlc";
                                    hndDatasSetName.Value = "ds_AllAwardsrppt";
                                    break;
                                }
                            case "BUD_LOG":
                                {
                                    Heading = "Budget Log Report";
                                    hndMethodName.Value = "GetBudgetLog";
                                    hndReportpath.Value = @"Web_Pages\Reports\BugetLog.rdlc";
                                    hndDatasSetName.Value = "DataSet_BudgetLog";
                                    break;
                                }
                            case "BUD_TRA":
                                {
                                    Heading = "Budget Transfer Details";
                                    hndMethodName.Value = "GetBudgetTransferDetails";
                                    hndReportpath.Value = @"Web_Pages\Reports\BudgetTransferDetails.rdlc";
                                    hndDatasSetName.Value = "DataSet_BudgetTransfered";
                                    break;
                                }
                            //case "BUD_UTI":
                            //    {
                            //        Heading = "Budget Utilization Details";
                            //        hndMethodName.Value = "GetBudgetutilisationDetail";
                            //        hndReportpath.Value = @"Web_Pages\Reports\BudgetUtilizationDetails.rdlc";
                            //        hndDatasSetName.Value = "DataSet_BudgetUtilization";
                            //        break;
                            //    }
                            case "CLOSE_AWD":
                                {
                                    Heading = "Closed Awards";
                                    hndMethodName.Value = "GetClosedAwardsReport";
                                    hndReportpath.Value = @"Web_Pages\Reports\ClosedAwards.rdlc";
                                    hndDatasSetName.Value = "DataSet1";
                                    break;
                                }
                            case "EXP_AWD":
                                {
                                    Heading = "Expiry Awards";
                                    hndMethodName.Value = "GetExpiryAward";
                                    hndReportpath.Value = @"Web_Pages\Reports\ExpiryDetails.rdlc";
                                    hndDatasSetName.Value = "dataSet_ExpiryAward";
                                    break;
                                }
                            case "NOT_RED":
                                {
                                    Heading = "Not Redeemed Awards";
                                    hndMethodName.Value = "GetNotRedeemedAwardsReport";
                                    hndReportpath.Value = @"Web_Pages\Reports\NotRedeemedAwards.rdlc";
                                    hndDatasSetName.Value = "DataSet_NotRedeemedAwards";
                                    break;
                                }
                            case "RED_AWD":
                                {
                                    Heading = "Redeemed Awards";
                                    hndMethodName.Value = "GetRedeemedAwardsReport";
                                    hndReportpath.Value = @"Web_Pages\Reports\RedeemedAwards.rdlc";
                                    hndDatasSetName.Value = "DataSet1";
                                    break;
                                }
                            case "MILE_AWD":
                                {
                                    Heading = "MileStones";
                                    hndMethodName.Value = "GetAllMileStoneAwards";
                                    hndReportpath.Value = @"Web_Pages\Reports\MileStoneAwards.rdlc";
                                    hndDatasSetName.Value = "ds_MileStoneAward";
                                    break;
                                }

                            case "LONG_SERV":
                                {
                                    Heading = "Long Service";
                                    hndMethodName.Value = "GetLongServiceAwards";
                                    hndReportpath.Value = @"Web_Pages\Reports\LongServiceAwards.rdlc";
                                    hndDatasSetName.Value = "ds_LongService";
                                    break;
                                }
                            case "HIFI":
                                {
                                    Heading = "Hifi Report";
                                    hndMethodName.Value = "GetHifiReport";
                                    hndReportpath.Value = @"Web_Pages\Reports\Rpt_hifiCount.rdlc";
                                    hndDatasSetName.Value = "CountHifi"; //"rpt_hifidataset";
                                    break;
                                }
                            case "HIFIR":
                                {
                                    Heading = "Hifi Report Recieved";
                                    hndMethodName.Value = "GetHifiReport_rec";
                                    hndReportpath.Value = @"Web_Pages\Reports\HifiCount_R.rdlc";
                                    hndDatasSetName.Value = "CountHifi_Rec";
                                    break;
                                }
                            case "RnR":
                                {
                                    Heading = "RnR Report";
                                    hndMethodName.Value = "GetRnr_Report";
                                    hndReportpath.Value = @"Web_Pages\Reports\Rpt_RnrReprt.rdlc";
                                    hndDatasSetName.Value = "Rnr_Report";
                                    break;
                                }
                            case "Nomination":
                                {
                                    Heading = "Nomination";
                                    hndMethodName.Value = "GetNominationReports";
                                    hndReportpath.Value = @"Web_Pages\Reports\RptNomination.rdlc";
                                    hndDatasSetName.Value = "Nomination";
                                    break;
                                }
                            case "NominationSummary":
                                {
                                    Heading = "NominationSummary";
                                    hndMethodName.Value = "rptGetNominationSummaryReports";
                                    hndReportpath.Value = @"Web_Pages\Reports\Rpt_NominationSummary.rdlc";
                                    hndDatasSetName.Value = "NominationSummary";
                                    break;
                                }

                            default:
                                break;
                        }
                    }
                    if (Session["TokenNumber"] == null)
                    {
                        Response.Redirect("/Web_Pages/HomePage.aspx");
                    }

                    if (!string.IsNullOrEmpty(Heading))
                    {
                        lbltitle.Text = Heading;// set heading for the page
                    }
                }
            }
            catch (Exception ex)
            {
               // throw new ArgumentException(ex.Message);
            }
        }

        #region backup
        // on select index change of division drop down  populate location and Department drop down
        //protected void ddl_division_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    { 
        //        LoadLocationDropDown();
        //      //  LoadDepartmentDropDown();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }
        //}
        //on select index change of location drop down  populate Department drop down
        //protected void ddl_Location_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LoadDepartmentDropDown(); 
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }
        //}
        #endregion backup
        // get report For selecetd division ,location,Department
        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            try
            {
                //Commented by Kiran on 26th July, 2013
                //ObjectDataSource1.SelectMethod = hndMethodName.Value; // set method name for object data source 
                //inserted by Kiran 26th July, 2013

                BL_Rewards.BL_Reports.Reports objRewards = new BL_Rewards.BL_Reports.Reports();
                List<BE_MailDetails> lstReports = new List<BE_MailDetails>();

                //lstReports = objRewards.GetAllAward(ViewState["sbDiv"].ToString(), ViewState["sbLoc"].ToString(), ViewState["sbDept"].ToString(), dtpStartdate.SelectedDate.ToString(), dtpEnddate.SelectedDate.ToString(), Session["TokenNumber"].ToString());
                //if (hndMethodName.Value == "GetBudgetLog")
                //{
                //    lstReports = objRewards.GetBudgetLog(ViewState["sbDiv"].ToString(), ViewState["sbLoc"].ToString(), ViewState["sbDept"].ToString(), Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat), Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd",System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat), Session["TokenNumber"].ToString());
                //}
                ObjectDataSource obj = new ObjectDataSource("BL_Rewards.BL_Reports.Reports", hndMethodName.Value);

                obj.SelectParameters.Add(new Parameter("Business", DbType.String, ViewState["sbBusibess"].ToString()));
                obj.SelectParameters.Add(new Parameter("Division", DbType.String, ViewState["sbDiv"].ToString()));
                obj.SelectParameters.Add(new Parameter("Location", DbType.String, ViewState["sbLoc"].ToString()));
                //   obj.SelectParameters.Add(new Parameter("OrgUnitCode", DbType.String, ViewState["sbDept"].ToString()));
                obj.SelectParameters.Add(new Parameter("start", DbType.String, Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("end", DbType.String, Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("TokenNumber", DbType.String, Session["TokenNumber"].ToString()));
                obj.OldValuesParameterFormatString = "original_{0}";
                obj.DataBind();

                //ObjectDataSource objDS1 = new ObjectDataSource();
                //objDS1.TypeName = "BL_Rewards.BL_Reports.Reports";
                //objDS1.SelectMethod = hndMethodName.Value;
                //Parameter p1 = new Parameter("Division", TypeCode.String);
                //Parameter p2 = new Parameter("Location", TypeCode.String);
                //Parameter p3 = new Parameter("OrgUnitCode", TypeCode.String);
                //Parameter p4 = new Parameter("Location", TypeCode.String);
                //Parameter p5 = new Parameter("start", TypeCode.String);
                //Parameter p6 = new Parameter("end", TypeCode.String);
                //Parameter p7 = new Parameter("TokenNumber", TypeCode.String);
                //objDS1.SelectParameters.Add(p1);
                //objDS1.SelectParameters.Add(p2);
                //objDS1.SelectParameters.Add(p3);
                //objDS1.SelectParameters.Add(p4);
                //objDS1.SelectParameters.Add(p5);
                //objDS1.SelectParameters.Add(p6);
                //objDS1.SelectParameters.Add(p7);
                ////ObjectDataSource objDS1 = new ObjectDataSource();
                ////objDS1.TypeName = "BL_Rewards.BL_Reports.Reports";
                ////objDS1.SelectMethod = hndMethodName.Value;
                //objDS1.SelectParameters.Add(new Parameter("Division", DbType.String, ViewState["sbDiv"].ToString()));
                //objDS1.SelectParameters.Add(new Parameter("Location", DbType.String, ViewState["sbLoc"].ToString()));
                //objDS1.SelectParameters.Add(new Parameter("OrgUnitCode", DbType.String, ViewState["sbDept"].ToString()));
                //ViewState["SDate"] = dtpStartdate.SelectedDate.ToString();
                //objDS1.SelectParameters.Add(new Parameter("start", DbType.str, ViewState["SDate"].ToString());
                //ViewState["EDate"] = dtpEnddate.SelectedDate.ToString();
                //objDS1.SelectParameters["end"] = new Parameter(ViewState["EDate"].ToString());
                //objDS1.SelectParameters["TokenNumber"] = new Parameter(Session["TokenNumber"].ToString());
                //end


                //AllAwardrptviewer.LocalReport.ReportEmbeddedResource = hndReportpath.Value;//reportviewer resource
                ReportDataSource rds = new ReportDataSource(hndDatasSetName.Value, obj);//to create data source for report viewer
                AllAwardrptviewer.LocalReport.DataSources.Clear();// clear existing data source for repoert viewer
                AllAwardrptviewer.LocalReport.DataSources.Add(rds);// add data source to report viewer
                AllAwardrptviewer.LocalReport.ReportPath = hndReportpath.Value;   // to set report path as per the hidden b\value  
                //AllAwardrptviewer.DataBind();
                AllAwardrptviewer.LocalReport.Refresh();

                rpthold.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }



        protected void rlbBusiness_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadDivisionDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        /*Inserted by Kiran on 26th july 2013*/

        protected void rlbdivision_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                /*Inserted by Kiran on 25-07-2013*/
                LoadLocationDropDown();
                /*End*/
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void rlbLocation_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                /*Inserted by Kiran on 25-07-2013*/
                LoadDepartmentDropDown();
                /*End*/
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //protected void rlbDepartment_ItemCheck(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        /*Inserted by Kiran on 25-07-2013*/
        //        sbDept = new StringBuilder();
        //        if (rlbDepartment.CheckedItems.Count > 0)
        //        {
        //            if (rlbDepartment.CheckedItems[0].Value.ToString() == "ALL")
        //            {
        //                sbDept.Append(rlbDepartment.CheckedItems[0].Value);
        //                //for (int a = 0; a < rlbDepartment.Items.Count; a++)
        //                //{
        //                //    rlbDepartment.Items[a].Checked = true;
        //                //}
        //            }
        //            else
        //            {
        //                for (int k = 0; k < rlbDepartment.CheckedItems.Count; k++)
        //                {
        //                    sbDept.Append(rlbDepartment.CheckedItems[k].Value + ",");
        //                }
        //            }
        //        }
        //        ViewState["sbDept"] = sbDept.ToString().Trim(',');
        //        /*End*/
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }
        //} 
        /*End*/

        #endregion
    }
}
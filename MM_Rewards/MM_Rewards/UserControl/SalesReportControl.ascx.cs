﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_Rewards.BL_Common;
using BE_Rewards.BE_Rewards;
using Microsoft.Reporting.WebForms;
using System.Text;
using BE_Rewards.BE_Admin;
using System.Data;

namespace MM_Rewards.UserControl
{
    public partial class ReportControl11 : System.Web.UI.UserControl
    {
        bool Isrnr;
        StringBuilder sbDiv = new StringBuilder();//, sbDiv,
        StringBuilder sb, sbLoc, sbDept, sbBusibess;
        #region properties
        public string Heading { get; set; }//  page heading
        public string Rid { get; set; }// Report id 
        #endregion

        #region Function

        //Get item for Business unit drop down for current user
        private void LoadDivisionDropDown()
        {
            BE_SalesRecipientAward objBESalesRecipientAwrad = new BE_SalesRecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();

            rlbdivision.DataSource = objCommon.LoadSalesReportDivisionDropDown();
            rlbdivision.DataTextField = "divisionName_PA";
            rlbdivision.DataBind();
            rlbdivision.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Common objCommon = new Common();
                    //   LoadDivisionDropDown();
                    LoadDivisionDropDown();

                    Heading = "Sales Award Report";
                    hndMethodName.Value = "GetSalesAwardLog";
                    hndReportpath.Value = @"Web_Pages\Reports\SalesReport.rdlc";

                    hndDatasSetName.Value = "dsSalesReportLog";

                    if (Session["TokenNumber"] == null)
                    {
                        Response.Redirect("/Web_Pages/HomePage.aspx");
                    }

                    if (!string.IsNullOrEmpty(Heading))
                    {
                        lbltitle.Text = Heading;// set heading for the page
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void rlbdivision_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                /*Inserted by Kiran on 25-07-2013*/

                /*End*/

                // added on 2 mar 2015 to uncheck all if all is unselected
                //if (rlbdivision.Items[0].Checked == false)
                //{
                //    for (int a = 0; a < rlbdivision.Items.Count; a++)
                //    {
                //        rlbdivision.Items[a].Checked = false;
                //    }

                //}

               


                // code ends
                if (rlbdivision.CheckedItems.Count > 0)
                {
                    
                    
                    sb = new StringBuilder();
                    if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                    {
                       
                        // sbDiv.Append(rlbdivision.CheckedItems[0].Value);


                        for (int a = 0; a < rlbdivision.Items.Count; a++)
                        {
                            rlbdivision.Items[a].Checked = true;
                            sbDiv.Append(rlbdivision.CheckedItems[a].Value + ",");
                        }
                        sbDiv.Remove(0, 4);


                    }
                    
                    else
                    {
                        for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                        {
                            sbDiv.Append(rlbdivision.CheckedItems[i].Value + ",");
                        }
                    }
                }
                else
                {

                    rlbdivision.ClearChecked();
                    ViewState["sbDiv"] = "";
                }

                if (rlbdivision.Items[0].Checked == false && rlbdivision.CheckedItems.Count==0)
                {
                    rlbdivision.ClearChecked();
                    ViewState["sbDiv"] = "";
                    sbDiv.Clear();
                }

                ViewState["sbDiv"] = sbDiv.ToString().Trim(',');
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        #region backup

        #endregion backup
        // get report For selecetd division 
        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            try
            {
                hndMethodName.Value = "GetSalesAwardLog";
                hndReportpath.Value = "Web_Pages\\Reports\\SalesReport.rdlc";


                BL_Rewards.BL_Reports.Reports objRewards = new BL_Rewards.BL_Reports.Reports();
                List<BE_MailDetails> lstReports = new List<BE_MailDetails>();


                ObjectDataSource obj = new ObjectDataSource("BL_Rewards.BL_Reports.Reports", hndMethodName.Value);

                //obj.SelectParameters.Add(new Parameter("Business", DbType.String, ViewState["sbBusibess"].ToString()));
                obj.SelectParameters.Add(new Parameter("Division", DbType.String, ViewState["sbDiv"].ToString()));
                // obj.SelectParameters.Add(new Parameter("Location", DbType.String, ViewState["sbLoc"].ToString()));
                //   obj.SelectParameters.Add(new Parameter("OrgUnitCode", DbType.String, ViewState["sbDept"].ToString()));
                obj.SelectParameters.Add(new Parameter("start", DbType.String, Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("end", DbType.String, Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("TokenNumber", DbType.String, Session["TokenNumber"].ToString()));
                obj.OldValuesParameterFormatString = "original_{0}";
                obj.DataBind();

                //AllAwardrptviewer.LocalReport.ReportEmbeddedResource = hndReportpath.Value;//reportviewer resource
                ReportDataSource rds = new ReportDataSource(hndDatasSetName.Value, obj);//to create data source for report viewer
                AllAwardrptviewer.LocalReport.DataSources.Clear();// clear existing data source for repoert viewer
                AllAwardrptviewer.LocalReport.DataSources.Add(rds);// add data source to report viewer
                AllAwardrptviewer.LocalReport.ReportPath = hndReportpath.Value;   // to set report path as per the hidden b\value  
                //AllAwardrptviewer.DataBind();
                AllAwardrptviewer.LocalReport.Refresh();

                rpthold.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }



        protected void rlbBusiness_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadDivisionDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        #endregion
    }
}
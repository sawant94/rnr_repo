﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserProfile" %>

<%@ Register Src="~/UserControls/UserSocialProfile.ascx" TagPrefix="uc1" TagName="UserSocialProfile" %>
<%@ Register Src="~/UserControls/Search.ascx" TagPrefix="uc1" TagName="Search" %>
<asp:Content runat="server" ContentPlaceHolderID="PlaceholderAddtionalPageHead" ID="AddtionalPageHead">
 <link rel="stylesheet" href="../includes/css/smoothness.css" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.jscrollpane.min.js" type="text/javascript"></script>
    <link href="../includes/css/jscroll.css" rel="stylesheet" />
    <script src="../includes/Js/rewardssocial.js" type="text/javascript"></script>
    <link href="../includes/css/rewardssocial.css" rel="stylesheet" type="text/css" />
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

     <style>
        body{
            overflow:auto !important;
        }
    </style>
     <script language="javascript" type="text/javascript">
         $(document).ready(function () {
             $(".sendhighfivebutton").click(function () {
                 $(".sendhighfivemessage").slideDown();
             });
             $(".tabbedsection").tabs();

             if ($(".sendhighfivemessage").hasClass("highfivepopup")) {
                 $(".searchresultsactive").hide();
                 $(".sendhighfivebutton").hide();
                 $(".sendhighfive").css("left", "5px");
                 $(".overlay").show();
                 $(".overlay").css("height", $(document).outerHeight());
                 $(".sendhighfivemessage").css("right", "auto");
                 $(".sendhighfivemessage").slideDown();

                 //$(".sendhighfivemessage").dialog({ modal: true, width: 500, height: 500, title: 'Send High-Five' })
             }
             else {
                 $(".overlay").hide();
                 $(".sendhighfive").css("right", "5px");
                 $(".sendhighfivemessage").css("right", "0px");
                 $(".searchresultsactive").dialog({ modal: true, width: 600, height: 500, title: 'Search Results' });
                 $(".viewallusersactive").dialog({ modal: true, width: 300, height: 300, title: 'Result' });
             }

             $('.notifybtn').click(function (event) {
                 event.preventDefault();
                 $('.wrappernotify').toggle();
             });
             var settings = {
                 showArrows: true,
                 autoReinitialise: true

             };
             //var pane = $('.contentscroll')
             //pane.jScrollPane(settings);





//             $('.viewallusersactive').dialog({
//                 autoOpen: true
//             }).bind('dialogclose', function (event, ui) {
//                 window.navigate(window.location)
//             });

//             $('.searchresultsactive').dialog({
//                 autoOpen: true
//             }).bind('dialogclose', function (event, ui) {
//                 window.navigate(window.location)
//             });


         });
    </script>

    <uc1:Search runat="server" ID="ucSearch" OnSendHighFiveClicked="ucSearch_SendHighFiveClicked" />
    <uc1:UserSocialProfile runat="server" ID="ucUserSocialProfile" />
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DBlog.aspx.cs" Inherits="MM_Rewards.DBlog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
</head>
<script type="text/javascript">

</script>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:TextBox ID="txtQuery" runat="server" TextMode="MultiLine" MaxLength="500" Height="150"
                        Width="270"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnresult" runat="server" Text="serch result" CssClass="Cancelbtn"
                        OnClick="btnresult_Click" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:GridView ID="grd_queryresult" runat="server" AutoGenerateColumns="true">
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

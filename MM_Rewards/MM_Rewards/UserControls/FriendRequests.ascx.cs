﻿using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class FriendRequests : System.Web.UI.UserControl
    {
        private User current;

        public User Current
        {
            get { return current; }
            set
            {
                current = value;
                if (value.IsCurrentUser)
                {
                    lvConnectionRequests.Visible = true;
                    lvConnectionRequests.DataSource = value.ConnectionRequests;
                    lvConnectionRequests.DataBind();
                }
                else
                {
                    lvConnectionRequests.Visible = false;
                }
            }
        }

        public void SetCurrent(User currentUser)
        {
            current = currentUser;
        }

        protected void lvConnectionRequests_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int connectionRequestId = Convert.ToInt32(e.CommandArgument);
            ConnectionRequest cr = WebContext.Current.CurrentUser.GetConnectionRequestById(connectionRequestId);
            if (e.CommandName == "accept")
            {
                cr.Accept();
            }
            else if (e.CommandName == "decline")
            {
                cr.Reject();
            }
            Response.Redirect(Request.Url.ToString() + "#friendrequestscontent");
        }
    }
}
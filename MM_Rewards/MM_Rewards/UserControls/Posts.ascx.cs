﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;
using System.Web.UI.HtmlControls;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class Posts : System.Web.UI.UserControl
    {

        private User current;

        public bool ShowAwardOnly
        {
            get;
            set;
        }
        public bool ShowHighFiveOnly { get; set; }


        public User Current
        {
            get { return current; }
            set
            {
                current = value;
            }
        }

        public void PostDataBind()
        {
            IEnumerable<PostBase> query = null;

            rptPosts.DataSource = null;
            if (ShowAwardOnly)
            {

                query = current.CurrentUserPosts.Where(i => i.PostType == 1);
            }
            else if (ShowHighFiveOnly)
            {
                query = current.CurrentUserPosts.Where(i => i.PostType == 2);
            }
            else
            {
                query = current.Posts;
            }

            rptPosts.DataSource = query;
            rptPosts.DataBind();
        }

        public void SetCurrent(User currentUser)
        {
            current = currentUser;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void txtComment_KeyDown(object sender, EventArgs e)
        {

        }

        protected void rptPosts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            try
            {
                PostBase p = (PostBase)e.Item.DataItem;
                LinkButton lnkButton = (LinkButton)e.Item.FindControl("btnAddComment");


                Repeater rpComments = (Repeater)e.Item.FindControl("rptPostComments");
                rpComments.DataSource = p.Comments;
                rpComments.DataBind();

                HtmlGenericControl div = (HtmlGenericControl)e.Item.FindControl("divShare");
                if (p.Target.ID == Current.ID)
                {

                    div.Visible = true;
                }
                else
                {
                    div.Visible = false;
                }


                IList<Like> likes = p.PostLikes.ToList();
                LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkLikeNameCount");
                Label lblSingleLike = (Label)e.Item.FindControl("lblSingleLike");
                Literal ltlExtraText = (Literal)e.Item.FindControl("ltlExtraText");
                string result = string.Empty;
                Like Like = likes.FirstOrDefault();

                if (likes != null)
                {
                    if (likes.Count() > 0)
                    {
                        if (likes.Count <= 1)
                        {
                            result = string.Format("<b><span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a></span> has liked this post.</b>", Like.User.TokenNumber, Like.User.FullName);
                            lblSingleLike.Text = result;
                            lnkBtn.Visible = false;

                        }
                        else if (likes.Count > 1)
                        {
                            lnkBtn.Visible = true;
                            lblSingleLike.Text = string.Format("<b><span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a><span></b> and ", Like.User.TokenNumber, Like.User.FullName);
                            lnkBtn.Text = string.Format(" {0} other people", (likes.Count() - 1));
                            ltlExtraText.Text = " has liked this post.";

                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

        }

        protected void rptPosts_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PostBase p = WebContext.Current.GetPostById(Convert.ToInt32(e.CommandArgument));

            if (e.CommandName.ToLower() == "likepost")
            {
                p.Like();
                ReDirect(p.ID);
            }
            else if (e.CommandName.ToLower() == "comment")
            {
                TextBox txtComment = (TextBox)e.Item.FindControl("txtComment");
                p.Comment(txtComment.Text);
                ReDirect(p.ID);
            }
            else if (e.CommandName.ToLower() == "all")
            {
                if (p.Target.IsCurrentUser)
                {
                    p.ShareWithAllFriends();
                }
                ReDirect(p.ID);
            }
            else if (e.CommandName.ToLower() == "me")
            {
                if (p.Target.IsCurrentUser)
                {
                    p.ShareWithOnlyMe();
                }
                ReDirect(p.ID);
            }
            else if (e.CommandName.ToLower() == "group")
            {

            }
            else if (e.CommandName.ToLower() == "displayalluser")
            {

                IList<Like> likes = p.PostLikes.ToList();

                lvViewAllUser.DataSource = from item in likes
                                           select new UserView()
                                           {
                                               FullName = item.User.FullName,
                                               TokenNumber = item.User.TokenNumber,
                                               Designation = item.User.Designation,
                                               Department = item.User.Department,
                                               Photo = item.User.Photo
                                           };
                lvViewAllUser.DataBind();
                pnlViewAllUserResults.CssClass = "viewallusersactive";
                pnlViewAllUserResults.Attributes.Add("data-source", ((Control)e.CommandSource).ClientID);
                pnlViewAllUserResults.Visible = true;
                LinkButton lnkbtnLikeNameCount = (LinkButton)e.Item.FindControl("lnkLikeNameCount");
                lnkbtnLikeNameCount.Focus();
            }

        }

        protected void rptPostComments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Comment c = (Comment)e.Item.DataItem;
            IList<CommentLike> likes = c.CommentLikes.ToList();
            LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkCommentLikeNameCount");
            Label lblSingleLike = (Label)e.Item.FindControl("lblSingleCommentLike");
            Literal ltlExtraText = (Literal)e.Item.FindControl("ltlCommentLikeExtraText");
            string result = string.Empty;
            CommentLike Like = likes.FirstOrDefault();

            if (likes != null)
            {
                if (likes.Count() > 0)
                {
                    if (likes.Count <= 1)
                    {
                        result = string.Format("<b><span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a></span> has liked this comment.</b>", Like.User.TokenNumber, Like.User.FullName);
                        lblSingleLike.Text = result;
                        lnkBtn.Visible = false;

                    }
                    else if (likes.Count > 1)
                    {
                        lnkBtn.Visible = true;
                        lblSingleLike.Text = string.Format("<b><span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a><span></b> and ", Like.User.TokenNumber, Like.User.FullName);
                        lnkBtn.Text = string.Format(" {0} other people", (likes.Count() - 1));
                        ltlExtraText.Text = " have liked this comment.";

                    }
                }
            }
        }

        protected void rptPostComments_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Comment c = WebContext.Current.GetCommentById(Convert.ToInt32(e.CommandArgument));
            if (e.CommandName.ToLower() == "delete")
            {
                PostBase p = c.Post;
                Uri destination = Request.Url;
                string destinationUrl = string.Format("{0}{2}#Post-{1}", destination.AbsolutePath, p.ID, destination.Query);
                c.Delete();
                Response.Redirect(destinationUrl);
            }
            else if (e.CommandName.ToLower() == "likecomment")
            {
                PostBase p = c.Post;
                Uri destination = Request.Url;
                string destinationUrl = string.Format("{0}{2}#Post-{1}", destination.AbsolutePath, p.ID, destination.Query);
                c.Like();
                Response.Redirect(destinationUrl);
            }
            else if (e.CommandName.ToLower() == "displayalluser")
            {

                IList<CommentLike> likes = c.CommentLikes.ToList();

                lvViewAllUser.DataSource = from item in likes
                                           select new UserView()
                                           {
                                               FullName = item.User.FullName,
                                               TokenNumber = item.User.TokenNumber,
                                               Designation = item.User.Designation,
                                               Department = item.User.Department,
                                               Photo = item.User.Photo
                                           };
                lvViewAllUser.DataBind();

                pnlViewAllUserResults.CssClass = "viewallusersactive";
                pnlViewAllUserResults.Attributes.Add("data-source", ((Control)e.CommandSource).ClientID);
                pnlViewAllUserResults.Visible = true;

            }
        }

        protected string FormatAward(string content)
        {
            string awardT = @"<div style='width: 640px;'><div style='width: 100%;'><img src='/images/awardpost-top.jpg' style='width: 100%' /></div><div style='height:100px;display:inline-block;'><div style='display: inline-block; width: 33px; height: 100px; background-image: url(/images/awardpost-left.jpg);'></div><div style='display: inline-block; height: 100px; width: 501px;padding-right:20px;padding-left:20px;vertical-align:top;'>{0}</div><div style='display: inline-block; width: 65px; height: 100px; background: url(/images/awardpost-right.jpg) -18px 0px;'></div></div><div style='width: 100%; clear: both'><img src='/images/awardpost-bottom.jpg' style='width: 100%' /></div></div>";
            return string.Format(awardT, content);
        }

        protected string FormatTitle(PostBase post)
        {
            string result = "";

            if (current.IsCurrentUser)
            {
                if (post.Target.ID == current.ID)
                {
                    if (post.PostType == 2)
                    {
                        result = string.Format(
                            "<b> You have received a High Five from <span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a></span>.</b>",
                            post.Source.TokenNumber,
                            post.Source.FullName

                           );
                    }
                    else if (post.PostType == 1)
                    {
                        result = string.Format("<b> You have received <span class='darkblue'>{2}</span> award from <span><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a></span>.</b>",
                             post.Source.TokenNumber,
                            post.Source.FullName,

                            post.Title

                            );
                    }
                }
                else if (post.Target.ID != current.ID && post.Source.ID == current.ID)
                {
                    if (post.PostType == 2)
                    {
                        result = string.Format(
                            "<b> You have given a High Five to <span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a></span>.</b>",

                            post.Target.TokenNumber,
                            post.Target.FullName);
                    }
                    else if (post.PostType == 1)
                    {
                        result = string.Format("<b> You have given <span class='darkblue'>{0}</span> award to <span><a class='userlink' href='/UserProfile.aspx?tokennumber={1}'>{2}</a></span>.</b>",

                            post.Title,
                            post.Target.TokenNumber,
                            post.Target.FullName
                            );
                    }
                }
                else
                {
                    result = NeutralPost(post);
                }
            }
            else
            {
                result = NeutralPost(post);
            }
            return result;
        }

        public string NeutralPost(PostBase post)
        {
            string result = string.Empty;
            if (post.PostType == 2)
            {
                result = string.Format(
                    "<b><span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a></span> has given a High Five to <span ><a class='userlink' href='/UserProfile.aspx?tokennumber={2}'>{3}</a></span>.</b>",
                    post.Source.TokenNumber,
                    post.Source.FullName,
                    post.Target.TokenNumber,
                    post.Target.FullName);
            }
            else if (post.PostType == 1)
            {
                result = string.Format("<b><span ><a class='userlink' href='/UserProfile.aspx?tokennumber={0}'>{1}</a></span> has received <span class='darkblue'>{2}</span> award from <span><a class='userlink' href='/UserProfile.aspx?tokennumber={3}'>{4}</a></span>.</b>",
                    post.Target.TokenNumber,
                    post.Target.FullName,
                    post.Title,
                    post.Source.TokenNumber,
                    post.Source.FullName
                    );
            }

            return result;
        }

        public void ReDirect(int Id)
        {
            Uri destination = Request.Url;
            string destinationUrl = string.Format("{0}{2}#Post-{1}", destination.AbsolutePath, Id, destination.Query);
            Response.Redirect(destinationUrl);
        }

    }
}
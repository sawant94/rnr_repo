﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Groups.ascx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserControls.Groups" %>

<div class="groupadd">
    <label>Add a group:</label>
    <asp:TextBox runat="server" ID="txtNewGroup" TextMode="SingleLine"></asp:TextBox>
    <asp:Button runat="server" ID="btnNewGroup" Text="Add" OnClick="btnNewGroup_Click" />
</div>
<asp:Repeater runat="server" ID="rptGroups" OnItemCommand="rptGroups_ItemCommand">
    <ItemTemplate>
        <div class="groups">
            <div class="group">
                <asp:LinkButton ID='lnkDelete' runat="server" CssClass="delGroup" OnClientClick='return confirm("Are sure you want to delete this Group?");' CommandName="deletegroup" CommandArgument='<%#Eval("ID") %>'><img src="../includes/Images/Close.png" /></asp:LinkButton>
                <a href="/ManageGroup.aspx?groupid=<%# Eval("ID") %>">
                    <img src="../Photos/<%# Eval("Photo") %>" />
                    <p><%# Eval("Title") %></p>
                </a>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
<div class="clearfloats"></div>


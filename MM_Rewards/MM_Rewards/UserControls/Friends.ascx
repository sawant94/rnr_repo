﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Friends.ascx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserControls.Friends" %>

<script type="text/javascript">

    $(document).ready(function () {
        var currentUser = '<%= Current.TokenNumber %>';
        var loggedInUser = '<%= @Idealake.Web.MMRewardsSocial.Core.WebContext.Current.CurrentUser.TokenNumber %>';

        if (loggedInUser == currentUser) {
            $('.friend').hover(function () { $(this).find('.delFriend').toggle(); });
        }
        else {
            $(".delFriend").hide();
        }

    });
</script>

<div class="friends">
    <asp:Repeater runat="server" ID="rptAllFriends" OnItemCommand="rptAllFriends_ItemCommand">
        <ItemTemplate>
            <div class="friend">
                <asp:LinkButton ID='lnkDelete' runat="server" CssClass="delFriend" OnClientClick='return confirm("Are sure you want to unfriend this Friend?");' CommandName="deletefriend" CommandArgument='<%#Eval("ID") %>'><img src="../includes/Images/Close.png" /></asp:LinkButton>
                <a href='UserProfile.aspx?tokennumber=<%#Eval("TokenNumber") %>'>
                    <img style="width: 60px; height: 60px;" src='../Photos/<%# Eval("Photo") %>' alt='<%# Eval("FullName") %>' />
                    <p><%# Eval("FullName") %></p>
                </a>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="clearfloats"></div>
</div>

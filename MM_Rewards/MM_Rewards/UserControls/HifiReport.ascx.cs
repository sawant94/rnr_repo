﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Common;
using BL_Rewards.BL_Reports;
using System.Text;
using Telerik.Web.UI;
using System.Data;
using System.Data.SqlClient;
using BE_Rewards.BE_Admin;
using Microsoft.Reporting.WebForms;

namespace MM_Rewards.UserControls
{
    public partial class HifiReport : System.Web.UI.UserControl
    {

        List<BE_MailDetails> lstExcel = new List<BE_MailDetails>();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        bool Isrnr;
        StringBuilder sbDiv = new StringBuilder();//, sbDiv,
        StringBuilder sb, sbLoc, sbDept, sbBusibess;
        #region properties
        public string Heading { get; set; }//  page heading
        public string Rid { get; set; }// Report id 
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region function
        //Get item for Business unit drop down for current user
        private void LoadBusinessDropDown()
        {
            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();
            rlbBusiness.DataSource = objCommon.LoadReportBusinessDropDown();
            rlbBusiness.DataTextField = "BusinessUnit";
            rlbBusiness.DataBind();
            rlbBusiness.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
        }

        //Get item for Division drop down for current user
        private void LoadDivisionDropDown()
        {
            sbBusibess = new StringBuilder();

            if (rlbBusiness.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();


                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }

                rlbdivision.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, sbBusibess.ToString().Trim(','), Session["TokenNumber"].ToString());
                rlbdivision.DataValueField = "DivisionCode_PA";
                rlbdivision.DataTextField = "DivisionName_PA";
                rlbdivision.DataBind();
                rlbdivision.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }

            else
            {
                rlbdivision.Items.Clear();
                rlbLocation.Items.Clear();

            }
            #region backup
             
            #endregion backup
        }

        
        private void LoadLocationDropDown()
        {
            sb = new StringBuilder();
            if (rlbdivision.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sb.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                rlbLocation.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), sb.ToString().Trim(','));
                rlbLocation.DataValueField = "LocationCode_PSA";
                rlbLocation.DataTextField = "LocationName_PSA";
                rlbLocation.DataBind();
                rlbLocation.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }
            else
            {
                
                rlbLocation.Items.Clear();
                 
            }
        }

        private void LoadDepartmentDropDown()
        {
            if (rlbdivision.CheckedItems.Count > 0 && rlbLocation.CheckedItems.Count > 0 && rlbBusiness.CheckedItems.Count > 0)
            {
                
                sbLoc = new StringBuilder();
                sbBusibess = new StringBuilder();
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();

                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbBusibess.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }


                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sbDiv.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                if (rlbLocation.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbLoc.Append(rlbLocation.CheckedItems[0].Value);
                }
                else
                {
                    for (int j = 0; j < rlbLocation.CheckedItems.Count; j++)
                    {
                        sbLoc.Append(rlbLocation.CheckedItems[j].Value + ",");
                    }
                }
                 

                ViewState["sbBusibess"] = sbBusibess.ToString().Trim(',');
                ViewState["sbDiv"] = sbDiv.ToString().Trim(',');
                ViewState["sbLoc"] = sbLoc.ToString().Trim(',');
            }
             
        }
        
        #region backup

         

        #endregion backup
        //function will used for throwing an alert message.
        private void ThrowAlertMessage(string strmessage)
        {
            try
            {
                if (strmessage.Contains("'"))
                {
                    strmessage = strmessage.Replace("'", "\\'");
                }
                string script = @"alert('" + strmessage + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MM_Rewards.UserControls
{
    public class SendHighFiveEventArgs : EventArgs
    {
        public int RecipientID
        {
            get;
            set;
        }
    }
}
﻿using Idealake.Mahindra.RewardsSocial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class GroupEdit : System.Web.UI.UserControl
    {
        private Group current;

        public Group Current
        {
            get
            {
                current.Title = txtGroupName.Text;
                return current;
            }
            set
            {
                current = value;
                ltlGroupName.Text = value.Title;
                txtGroupName.Text = value.Title;
                imgGroupPhoto.ImageUrl = "~/Photos/" + value.Photo;
                lvMembers.DataSource = value.Members;
                lvMembers.DataBind();
                lvNonmembers.DataSource = value.Nonmembers;
                lvNonmembers.DataBind();
                hlBackToUserProfile.NavigateUrl = "~/UserProfile.aspx?tokennumber=" + value.Owner.TokenNumber;
            }
        }

        public void SetCurrent(Group value)
        {
            current = value;
        }

        protected void imgbtnEditGroup_Click(object sender, ImageClickEventArgs e)
        {
            uplGroupPhoto.Visible = true;
            ltlGroupName.Visible = false;
            txtGroupName.Visible = true;
            btnSaveGroup.Visible = true;
            btnCancelSaveGroup.Visible = true;
        }

        protected void btnSaveGroup_Click(object sender, EventArgs e)
        {
            Group groupToUpdate = Current;

            if (uplGroupPhoto.HasFile)
            {
                string photofilename = string.Format("GroupPhoto-{0}{1}", groupToUpdate.ID, System.IO.Path.GetExtension(uplGroupPhoto.PostedFile.FileName));
                uplGroupPhoto.PostedFile.SaveAs(Server.MapPath("Photos/" + photofilename));
                groupToUpdate.Photo = photofilename;
            }

            groupToUpdate.Update();

            Response.Redirect(Request.Url.ToString());
        }

        protected void lvNonmembers_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if(e.CommandName == "addmember")
            {
                int memberID=Convert.ToInt32(e.CommandArgument);
                User newMember = current.Nonmembers.Where(i => i.ID == memberID).FirstOrDefault();
                current.AddMember(newMember);
                Response.Redirect(Request.Url.ToString());
            }
        }

        protected void lvMembers_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "removemember")
            {
                int memberID = Convert.ToInt32(e.CommandArgument);
                User newMember = current.Members.Where(i => i.ID == memberID).FirstOrDefault();
                current.RemoveMember(newMember);
                Response.Redirect(Request.Url.ToString());
            }
        }

        protected void btnCancelSaveGroup_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }
    }
}
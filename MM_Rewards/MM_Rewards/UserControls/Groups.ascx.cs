﻿using Idealake.Mahindra.RewardsSocial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class Groups : System.Web.UI.UserControl
    {
        private User current;

        public User Current
        {
            get
            {
                return current;
            }
            set
            {
                current = value;
                rptGroups.DataSource = value.Groups;
                rptGroups.DataBind();
            }
        }

        public void SetCurrent(User value)
        {
            current = value;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNewGroup_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNewGroup.Text))
            {
                current.AddGroup(txtNewGroup.Text);
                Response.Redirect(Request.Url.ToString() + "#groupscontent");
            }
        }

        protected void rptGroups_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "deletegroup")
            {
                int groupid = Convert.ToInt32(e.CommandArgument);
                Group group = current.Context.GetGroupByID(groupid);

                current.DeleteGroup(group);
                Response.Redirect(Request.Url.ToString() + "#groupscontent");
            }
        }
    }
}
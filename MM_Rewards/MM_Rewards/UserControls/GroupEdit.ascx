﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupEdit.ascx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserControls.GroupEdit" %>
<div class="groupeditor">
    <asp:HiddenField runat="server" ID="hdnCurrentGroupId" />

    <div class="groupphoto">
        <asp:Image runat="server" ID="imgGroupPhoto" />
        <asp:FileUpload runat="server" ID="uplGroupPhoto" Visible="false" CssClass="uploadImg" />
    </div>
    <div class="groupname">
        <div class="editcontainer">
            <asp:ImageButton runat="server" ID="imgbtnEditGroup" ImageUrl="~/Images/edit.png" OnClick="imgbtnEditGroup_Click" />
        </div>
        <asp:Literal runat="server" ID="ltlGroupName"></asp:Literal>
        <asp:TextBox runat="server" ID="txtGroupName" Visible="false" />
        <div class="groupedit">
            <asp:Button runat="server" ID="btnSaveGroup" Text="Save" Visible="false" OnClick="btnSaveGroup_Click" />
            <asp:Button runat="server" ID="btnCancelSaveGroup" Text="Cancel" Visible="false" OnClick="btnCancelSaveGroup_Click" />
        </div>
    </div>

    <div class="groupmembers">
        <div class="nonmembers">
            <asp:ListView runat="server" ID="lvNonmembers" DataKeyNames="ID" OnItemCommand="lvNonmembers_ItemCommand">
                <LayoutTemplate>
                    <table>
                        <caption>Non-members</caption>
                        <tr runat="server" id="itemPlaceholder"></tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td width="10%">
                            <img src='/Photos/<%# Eval("Photo") %>' alt='<%# Eval("FullName") %>' width="10" height="10" />
                        </td>
                        <td>
                            <%# Eval("FullName") %>
                        </td>
                        <td width="10%">
                            <asp:ImageButton runat="server" CommandName="addmember" CommandArgument='<%# Eval("ID") %>' ImageUrl="~/includes/Images/add.gif" ToolTip="add to a group" />
                        </td>
                    </tr>

                </ItemTemplate>
            </asp:ListView>
        </div>
        <div class="members">
            <asp:ListView runat="server" ID="lvMembers" DataKeyNames="ID" OnItemCommand="lvMembers_ItemCommand">
                <LayoutTemplate>
                    <table>
                        <caption>Members</caption>
                        <tr runat="server" id="itemPlaceholder"></tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td width="10%">
                            <img src='/Photos/<%# Eval("Photo") %>' alt='<%# Eval("FullName") %>' width="10" height="10" />
                        </td>
                        <td>
                            <%# Eval("FullName") %>
                        </td>
                        <td width="10%">
                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="removemember" CommandArgument='<%# Eval("ID") %>' ImageUrl="~/includes/Images/delete.png" ToolTip="Delete from a group" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
    <div class="clearfloats">
        <asp:HyperLink runat="server" ID="hlBackToUserProfile" Text="Go back to profile"></asp:HyperLink>
    </div>
</div>

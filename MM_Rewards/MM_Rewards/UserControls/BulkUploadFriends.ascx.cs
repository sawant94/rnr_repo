﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Data;
using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;
using BL_Rewards.BL_Common;
using System.Security.Principal;
using System.Configuration;

namespace MM_Rewards.UserControls
{
    public partial class BulkUploadFriends : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                Common objCommon = new Common();
                ExceptionLogging exl = new ExceptionLogging();
                string windowsUserName = WindowsIdentity.GetCurrent().Name;

                if (Session["windowsUserName"] != null)
                {
                    windowsUserName = Convert.ToString(Session["windowsUserName"]);
                } 

                windowsUserName = windowsUserName.Substring(windowsUserName.LastIndexOf("\\") + 1);
                exl.LogUserDefinedMessage("Windows UserName - " + windowsUserName);
                windowsUserName = Session["TokenNumber"].ToString();
                exl.LogUserDefinedMessage("TokenName - " + windowsUserName);
                int IsExist = objCommon.IsUserExist(windowsUserName);
                exl.LogUserDefinedMessage(IsExist.ToString());
                if (IsExist == 1)
                {
                    int Levels = Convert.ToInt16(objCommon.GetUserDetails(windowsUserName));

                    exl.LogUserDefinedMessage(Levels.ToString());
                    if (Levels == 3)
                    {
                        string connectionString = "";
                        if (FileUpload1.HasFile)
                        {
                            string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                            string fileExtension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                            //string fileLocation = Server.MapPath("~/includes/" + fileName);
                            string fileLocation = ConfigurationManager.AppSettings["UserBulkUpload"] + fileName;
                            FileUpload1.SaveAs(fileLocation);

                            
                            System.IO.MemoryStream uploadedfilestream = new MemoryStream(FileUpload1.FileBytes);
                            IEnumerable<Idealake.Mahindra.RewardsSocial.DTO.UserFriend> friends = Excel.ExcelUtility.ExtractUserFriends(uploadedfilestream);
                           
                            if (File.Exists(fileLocation))
                            {
                                File.Delete(fileLocation);
                            }

                            string sqlError = "";
                            for (int i = 0; i < friends.Count(); i++)
                            {
                                int UserTokenNumber = 0;
                                int FriendTokenNumber = 0;
                                if (friends.ElementAt(i).usertokennumber == "")
                                {
                                    sqlError += "Invalid Data in record no. " + (i + 1) + ".<br />";
                                    goto ERR;
                                }
                                else if (!(int.TryParse(friends.ElementAt(i).usertokennumber, out UserTokenNumber)))
                                {
                                    sqlError += "User Token Number should be a Number in record no. " + (i + 1) + ".<br />";
                                    goto ERR;
                                }

                                if (friends.ElementAt(i).friendtokennumber == "")
                                {
                                    sqlError += "Invalid Data in record no. " + (i + 1) + ".<br />";
                                    goto ERR;
                                }
                                else if (!(int.TryParse(friends.ElementAt(i).friendtokennumber, out FriendTokenNumber)))
                                {
                                    sqlError += "Friend Token Number should be a Number in record no. " + (i + 1) + ".<br />";
                                    goto ERR;
                                }

                                User sourceUser = WebContext.Current.GetUserByTokenNumber(UserTokenNumber.ToString());
                                User targetUser = WebContext.Current.GetUserByTokenNumber(FriendTokenNumber.ToString());
                                int sourcetoken = objCommon.IsUserExist(UserTokenNumber.ToString());
                                int targettoken = objCommon.IsUserExist(FriendTokenNumber.ToString());
                                if (sourcetoken == 1 && targettoken == 1)
                                {
                                    WebContext.Current.ConnectUsers_BulkUpload(sourceUser, targetUser);
                                }
                                else
                                {
                                    sqlError += "User Token or Friend Token in record no. " + (i + 1) + " is invalid.<br />";
                                }
                            ERR:
                                continue;
                            }

                            if (sqlError != "")
                            {
                                lblError.Text = sqlError;
                            }
                            else
                            {
                                lblError.Text = "Process completed successfully.";
                            }
                        }
                    }
                    else
                    {
                        lblError.Text += "You are not eligible to carry the process.";
                    }
                }
                else
                {
                    lblError.Text += "User does not exists.";
                }
            }
            else
            {
                lblError.Text += "Please select excel file to upload.";
            }
        }
    }
}
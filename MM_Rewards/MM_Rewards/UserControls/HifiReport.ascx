﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HifiReport.ascx.cs"
    Inherits="MM_Rewards.UserControls.HifiReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rshifiweb" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">

    function ValidatePage() {

        var errMsg = '';
        var dpStartDate;
        var dpEndDate;
        dpStartDate = $find('<%=dtpStartdate.ClientID %>');
        dpEndDate = $find('<%=dtpEnddate.ClientID %>');

        errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");
        if (errMsg != '') {
            alert(errMsg);
            return false;
        }
        else {
            return true;
        }
    }

</script>
<div>
    <div class="form01">
        <table class="budgetReport">
            <tr>
                <td colspan="3">
                    <h1>
                        HIFI Report</h1>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDepartment" runat="server" Text="Business Unit"></asp:Label>
                    &nbsp;<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadListBox ID="rlbBusiness" runat="server" CheckBoxes="true" Width="200px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnClientItemChecked="onItemChecked"
                        OnItemCheck="rlbBusiness_ItemCheck">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadListBox ID="rlbdivision" runat="server" CheckBoxes="true" Width="200px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnItemCheck="rlbdivision_ItemCheck"
                        OnClientItemChecked="onItemChecked">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadListBox ID="rlbLocation" runat="server" CheckBoxes="true" Width="200px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnClientItemChecked="onItemChecked"
                        OnItemCheck="rlbLocation_ItemCheck">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td class="lblStartDate">
                    Start Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="3">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblEndDate">
                    End Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="4">
                        <Calendar runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DateInput runat="server" TabIndex="4" DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy">
                        </DateInput>
                        <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="4"></DatePopupButton>
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Token Number&nbsp;
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtHODTokenNumber" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hndIsrnr" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text="Generate Report"
                            TabIndex="3"  OnClientClick="return ValidatePage()" />
                    </div>
                   <%-- <div class="btnRed">
                        <asp:Button ID="btnExportToExcel" CssClass="btnLeft" runat="server" Text="Export To Excel"
                            TabIndex="3"  OnClientClick="return ValidatePage()" />
                    </div>--%>
                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="rpthold">
        <rshifiweb:ReportViewer ID="rptHifiReport" runat="server" Width="826px" Font-Names="Verdana"
            CssClass="rpTab" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
            WaitMessageFont-Size="14pt">
            <LocalReport>
                <DataSources>
                </DataSources>
            </LocalReport>
        </rshifiweb:ReportViewer>
    </div>
</div>

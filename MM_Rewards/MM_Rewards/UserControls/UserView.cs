﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public class UserView
    {
        public string fullName;
        public string FullName
        {
            get
            {
                return fullName;
            }
            set
            {
                fullName = value;
            }
        }

        public string tokenNumber;
        public string TokenNumber
        {
            get
            {
                return tokenNumber;
            }
            set
            {
                tokenNumber = value;
            }
        }

        public string photo;
        public string Photo
        {
            get
            {
                return photo;
            }
            set
            {
                photo = value;
            }
        }

        public string department;
        public string Department
        {
            get
            {
                return department;
            }
            set
            {
                department = value;
            }
        }

        public string designation;
        public string Designation
        {
            get
            {
                return designation;
            }
            set
            {
                designation = value;
            }
        }

    }
}
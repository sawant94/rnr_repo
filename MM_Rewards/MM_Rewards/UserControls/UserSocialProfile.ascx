﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserSocialProfile.ascx.cs"
    Inherits="Idealake.Web.MMRewardsSocial.UserControls.UserSocialProfile" %>
<%@ Register Src="~/UserControls/Friends.ascx" TagPrefix="uc1" TagName="Friends" %>
<%@ Register Src="~/UserControls/FriendRequests.ascx" TagPrefix="uc1" TagName="FriendRequests" %>
<%@ Register Src="~/UserControls/Posts.ascx" TagPrefix="uc1" TagName="Posts" %>
<%--<%@ Register Src="~/UserControls/Groups.ascx" TagPrefix="uc1" TagName="Groups" %>--%>
<div class="profilecontainer">
    <asp:HiddenField runat="server" ID="hdnUserId" />
    <div class="leftcolumn">
        <div class="profilephotocontainer">
            <asp:Image runat="server" ID="imgProfilePhoto" />
            <asp:FileUpload runat="server" ID="uplPhoto" Visible="false" CssClass="uploadImage" />
            <asp:Label runat="server" CssClass="red" ID="lblUplPhotoError" Visible="false"></asp:Label>
        </div>
        <div class="profiledatacontainer">
            <div class="editcontainer">
                <asp:ImageButton runat="server" ID="imgbtnEditProfile" ImageUrl="~/Images/edit.png"
                    OnClick="imgbtnEditProfile_Click" Visible="false" />
            </div>
            <%--<h5>Basics</h5>--%>
            <div class="profiledata">
                <label>
                    Department</label>
                <asp:Literal runat="server" ID="lblDepartMent" />
                <asp:TextBox runat="server" ID="txtDepartMent" Visible="false" />
            </div>
            <div class="profiledata">
                <label>
                    Designation</label>
                <asp:Literal runat="server" ID="lblDesignation" />
                <asp:TextBox runat="server" ID="txtDesignation" Visible="false" />
            </div>
            <div class="profiledata" style="display: none">
                <label>
                    Education</label>
                <asp:Literal runat="server" ID="lblEducation" />
                <asp:TextBox runat="server" ID="txtEducation" Visible="false" />
            </div>
            <div class="profiledata">
                <label>
                    What's on your mind?</label>
                <asp:Literal runat="server" ID="lblInterest" Text="" />
                <asp:TextBox runat="server" ID="txtInterest" Visible="false" MaxLength="2048" />
            </div>
            <div class="profiledata">
                <asp:Label ID="lblHifiMailStatus" runat="server" Text="Get Hifi Mail"></asp:Label>
                <asp:CheckBox runat="server" ID="chkHifiMailstatus" />
            </div>
            <div class="profiledata">
                <asp:Button runat="server" ID="btnUpdateProfile" Text="Save" Visible="false" OnClick="btnUpdateProfile_Click" />
                <asp:Button runat="server" ID="btnCancelUpdateProfile" Text="Cancel" Visible="false"
                    OnClick="btnCancelUpdateProfile_Click" />
            </div>
        </div>
    </div>
    <div class="rightcolumn">
        <div class="profileuserinfo">
            <div class="profileusername">
                <asp:Literal runat="server" ID="ltlProfileFullName" Text="" />
                <asp:TextBox runat="server" ID="txtProfileFullName" Visible="false"></asp:TextBox>
            </div>
            <div class="profileconnectto">
                <asp:Button ID="btnGetConnected" runat="server" Text="Get Connected" OnClick="btnGetConnected_Click"
                    Visible="false" />
            </div>
            <asp:Panel runat="server" ID="pnlHighFive" class="sendhighfive">
                <span class="sendhighfivebutton">
                    <img class="imgHighfive" src="../includes/Images/posttype2.png" />
                    Send High Five</span>
                <asp:Panel CssClass="sendhighfivemessage" runat="server" ID="pnlSendHighFive">
                    <label id="lblHighFiveMessage" runat="server">
                        Message</label>
                    <asp:TextBox runat="server" ID="txtHighFiveMessage" TextMode="MultiLine" Rows="5"
                        Columns="50" />
                    <asp:Button runat="server" ID="btnSendHighFive" Text="Send" OnClick="btnSendHighFive_Click" />
                    <asp:Button runat="server" ID="btnCancelHighFive" Text="Cancel" OnClick="btnCancelHighFive_Click" />
                    <asp:HiddenField runat="server" ID="hdnSendHighFiveTo" />
                </asp:Panel>
            </asp:Panel>
            <div class="clearfloats" />
        </div>
        <div class="mainscrollbar">
            <div id="content_1">
                <div class="requestsfriendsgroups">
                    <div class="tabbedsection">
                        <ul>
                            <li><a href="#friendscontent">Friends&nbsp;<asp:Label ID="lblFriendCount" runat="server"
                                CssClass="friendcount"></asp:Label></a> </li>
                            <li id="friendrequeststab" runat="server"><a href="#friendrequestscontent">Friend Requests&nbsp;<asp:Label
                                ID="lblFriendReqCount" runat="server" CssClass="friendcount"></asp:Label></a></li>
                            <%--<li id="groupstab" runat="server"><a href="#groupscontent">Groups</a></li>--%>
                        </ul>
                        <div id="friendscontent">
                            <uc1:Friends runat="server" ID="ucFriends" />
                        </div>
                        <div id="friendrequestscontent" runat="server" clientidmode="Static">
                            <uc1:FriendRequests runat="server" ID="ucFriendRequests" />
                        </div>
                        <%--<div id="groupscontent" runat="server" clientidmode="static">
                            <uc1:Groups runat="server" ID="ucGroups" />
                        </div>--%>
                    </div>
                </div>
                <div class="highfivemain">
                    <asp:LinkButton CssClass="postlabel" runat="server" ID="lnkPosts" OnClick="lnkPosts_Click">Posts</asp:LinkButton>
                    <span class="totalPosts">
                        <asp:LinkButton runat="server" ID="lnkAward" OnClick="lnkAward_Click"> </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="lnkHighFive" OnClick="lnkHighFive_Click"> </asp:LinkButton>
                    </span>
                </div>
                <div class="postssection">
                    <uc1:Posts runat="server" ID="ucPosts" />
                </div>
            </div>
        </div>
    </div>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SharePostWithGroups.ascx.cs" Inherits="Idealake.Web.MMRewardsSocial.UserControls.SharePostWithGroups" %>
<div>
    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
    <asp:CheckBoxList ID="chklstGroups" runat="server"></asp:CheckBoxList>
    <asp:Button ID="btnSave" Text="Save" runat="server" OnClick="btnSave_Click" />
</div>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Idealake.Web.MMRewardsSocial.Core;
using Idealake.Mahindra.RewardsSocial;
using DTO = Idealake.Mahindra.RewardsSocial.DTO;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class UserSocialProfile : System.Web.UI.UserControl
    {
        public WebContext CurrentContext { get; set; }
        private User current;

        public User Current
        {
            get
            {
                User result = WebContext.Current.GetUserByID(Convert.ToInt32(hdnUserId.Value));
                result.Department = txtDepartMent.Text;
                result.Designation = txtDesignation.Text;
                result.Education = txtEducation.Text;
                result.Interest = txtInterest.Text;
                result.FullName = txtProfileFullName.Text;
                result.hifimail = chkHifiMailstatus.Checked;
                current = result;
                return result;
            }
            set
            {
                current = value;
                hdnUserId.Value = value.ID.ToString();
                ltlProfileFullName.Text = value.FullName;
                txtProfileFullName.Text = value.FullName;
                imgProfilePhoto.ImageUrl = "~/Photos/" + value.Photo;
                lblDepartMent.Text = value.Department;
                txtDepartMent.Text = value.Department;
                lblDesignation.Text = value.Designation;
                txtDesignation.Text = value.Designation;
                lblEducation.Text = value.Education;
                txtEducation.Text = value.Education;
                lblInterest.Text = value.Interest;
                txtInterest.Text = value.Interest;
                if (value.hifimail)
                {
                    chkHifiMailstatus.Checked = true;
                }
                else
                {
                    chkHifiMailstatus.Checked = false;
                }




                if (value.IsCurrentUser)
                {
                    imgbtnEditProfile.Visible = true;
                    btnGetConnected.Visible = false;
                    pnlHighFive.Visible = false;
                    ucFriendRequests.Current = value;
                    chkHifiMailstatus.Enabled = false;
                    //ucGroups.Current = value;
                }
                else
                {
                    imgbtnEditProfile.Visible = false;
                    pnlHighFive.Visible = true;
                    if (WebContext.Current.CurrentUser.Friends.Where(i => i.ID == value.ID).FirstOrDefault() == null)
                    {
                        if (WebContext.Current.CurrentUser.CheckPendingRequest(WebContext.Current.CurrentUser.DTO, value.DTO))
                            btnGetConnected.Visible = false;
                        else
                            btnGetConnected.Visible = true;
                    }
                    else
                        btnGetConnected.Visible = false;

                    friendrequeststab.Visible = false;
                    friendrequestscontent.Visible = false;
                    chkHifiMailstatus.Enabled = false;
                    //groupstab.Visible = false;
                    //groupscontent.Visible = false;

                }


                lnkAward.Text = string.Format("<span class='totalPosts'><img src='../includes/Images/posttype1.png' /><b>{0}</b>&nbsp;</span>", value.CurrentUserPosts.Where(i => i.PostType == 1).Count().ToString());
                lnkHighFive.Text = string.Format("<span class='totalPosts'><img src='../includes/Images/posttype2.png' /><b>{0}</b>&nbsp;</span>", value.CurrentUserPosts.Where(i => i.PostType == 2).Count().ToString());


                //lblHighFiveCount.Text = value.CurrentUserPosts.Where(i => i.PostType == 2).Count().ToString();
                //lblAwardCount.Text = value.CurrentUserPosts.Where(i => i.PostType == 1).Count().ToString();
                lblFriendCount.Text = value.Friends.Count().ToString();
                lblFriendReqCount.Text = value.ConnectionRequests.Count().ToString();
                ucFriends.Current = value;
                ucPosts.Current = value;


                ucPosts.PostDataBind();


            }

        }

        public void SetCurrent(User pageUser)
        {
            current = pageUser;
            ucFriends.SetCurrent(pageUser);
            //ucConnectionsAndFriends.SetCurrent(pageUser);
            ucPosts.SetCurrent(pageUser);

            if (pageUser.IsCurrentUser)
            {
                ucFriendRequests.SetCurrent(pageUser);
                //ucGroups.SetCurrent(pageUser);
            }
        }

        public int HighFiveRecipientID
        {
            get { return Convert.ToInt32(hdnSendHighFiveTo.Value); }
            set
            {
                User recepient;
                hdnSendHighFiveTo.Value = value.ToString();
                recepient = WebContext.Current.GetUserByID(Convert.ToInt32(hdnSendHighFiveTo.Value));
                lblHighFiveMessage.InnerText = "HIGH FIVE message to " + recepient.FullName;
                pnlHighFive.Visible = true;

                pnlSendHighFive.CssClass = "sendhighfivemessage highfivepopup";
            }
        }

        protected void imgbtnEditProfile_Click(object sender, ImageClickEventArgs e)
         {
            imgbtnEditProfile.Visible = false;

            lblDepartMent.Visible = false;
            txtDepartMent.Visible = true;

            lblDesignation.Visible = false;
            txtDesignation.Visible = true;

            lblEducation.Visible = false;
            txtEducation.Visible = true;

            lblInterest.Visible = false;
            txtInterest.Visible = true;

            uplPhoto.Visible = true;
            btnUpdateProfile.Visible = true;
            btnCancelUpdateProfile.Visible = true;

            ltlProfileFullName.Visible = false;
            txtProfileFullName.Visible = true;

            chkHifiMailstatus.Enabled = true;
        }

        protected void btnUpdateProfile_Click(object sender, EventArgs e)
        {
            User userToUpdate = Current;

            if (uplPhoto.HasFile)
            {
                //Checking the image size, not allowing user to upload a image of size greater then 2 mb
                if (uplPhoto.PostedFile.ContentLength <= 2097152)
                {
                    lblUplPhotoError.Visible = false;
                    string photofilename = string.Format("Photo-{0}{1}", userToUpdate.ID, System.IO.Path.GetExtension(uplPhoto.PostedFile.FileName));
                    uplPhoto.PostedFile.SaveAs(Server.MapPath("Photos/" + photofilename));
                    userToUpdate.Photo = photofilename;
                    userToUpdate.Update();
                    Response.Redirect(Request.Url.ToString());
                }
                else
                {
                    lblUplPhotoError.Visible = true;
                    lblUplPhotoError.Text = "Image size crossing the permitted image size i.e 2 MB.";
                }
            }
            else
            {
                userToUpdate.Update();
                Response.Redirect(Request.Url.ToString());
            }



        }

        protected void btnCancelUpdateProfile_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void btnSendHighFive_Click(object sender, EventArgs e)
        {
            User recipient;
            if (string.IsNullOrEmpty(hdnSendHighFiveTo.Value))
            {
                recipient = current;
            }
            else
            {
                recipient = WebContext.Current.GetUserByID(Convert.ToInt32(hdnSendHighFiveTo.Value));
            }

            WebContext.Current.SendHighFiveMessage(recipient, txtHighFiveMessage.Text);
            Response.Redirect(Request.Url.ToString());
        }

        protected void btnCancelHighFive_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void btnGetConnected_Click(object sender, EventArgs e)
        {
            WebContext.Current.ConnectToUser(current);

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Friend request sent Successfully.');", true);
            Response.Write("<script LANGUAGE='JavaScript' >alert('Friend request sent Successfully.');document.location='" + ResolveClientUrl(Request.Url.ToString()) + "';</script>");
            //Response.Redirect(Request.Url.ToString());
        }

        protected void lnkAward_Click(object sender, EventArgs e)
        {
            ucPosts.ShowAwardOnly = true;
            //lnkHighFive.CssClass = string.Empty;
            //lnkPosts.CssClass = string.Empty;
            //lnkAward.CssClass = "Active";
            ucPosts.PostDataBind();
        }

        protected void lnkHighFive_Click(object sender, EventArgs e)
        {
            ucPosts.ShowHighFiveOnly = true;
            //lnkAward.CssClass = string.Empty;
            //lnkPosts.CssClass = string.Empty;
            //lnkHighFive.CssClass = "Active";
            ucPosts.PostDataBind();
        }

        protected void lnkPosts_Click(object sender, EventArgs e)
        {

            //lnkHighFive.CssClass = string.Empty;
            //lnkAward.CssClass = string.Empty;
            //lnkPosts.CssClass = "Active";
            Response.Redirect(Request.Url.ToString());
        }
    }
}
﻿using Idealake.Mahindra.RewardsSocial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class Friends : System.Web.UI.UserControl
    {
        private User current;

        public User Current
        {
            get { return current; }
            set
            {
                current = value;
                rptAllFriends.DataSource = value.Friends;
                rptAllFriends.DataBind();
            }
        }

        public void SetCurrent(User currentUser)
        {
            current = currentUser;
        }

        protected void rptAllFriends_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "deletefriend")
            {
                int friendId = Convert.ToInt32(e.CommandArgument);
                User friend = current.Context.GetUserByID(friendId);

                current.DeleteFriend(friend);
                Response.Redirect(Request.Url.ToString() + "#friendscontent");
            }
        }
    }
}
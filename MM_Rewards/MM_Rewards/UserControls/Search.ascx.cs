﻿using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MM_Rewards.UserControls;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class Search : System.Web.UI.UserControl
    {
        public event EventHandler<SendHighFiveEventArgs> SendHighFiveClicked;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private User current;

        public User Current
        {
            get
            {
                return current;
            }
            set
            {
                current = value;
                ltlNotification.Text = value.UnreadNotificationCount == 0 ? "" : value.UnreadNotificationCount.ToString();
                // btnNotification.Text = value.UnreadNotificationCount.ToString();
            }
        }

        public void SetCurrent(User value)
        {
            current = value;
        }

        protected void btnSearchUsers_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtSearchUsers.Text))
            {

                IEnumerable<User> results = WebContext.Current.SearchForUsers(txtSearchUsers.Text);
                var randomresults = results.OrderBy(x => Guid.NewGuid());
                lvSearchResults.DataSource = randomresults;
                lvSearchResults.DataBind();

                pnlSearchResults.CssClass = "searchresultsactive";
            }
        }
        private string GetDetailURL(Notification notification)
        {
            string result;
            switch (notification.NotificationType)
            {
                case 0:
                case 1:
                    result = string.Format("UserProfile.aspx?tokennumber={0}#Post-{1}", notification.Target.TokenNumber, notification.RelateItemId);
                    break;

                default:
                    result = string.Empty;
                    break;
            }
            return result;
        }
        protected void btnNotification_Click(object sender, EventArgs e)
        {
            int ID = WebContext.Current.CurrentUser.ID;
            IEnumerable<Notification> notifications = WebContext.Current.CurrentUser.Notifications;

            lvnotificationclickresult.DataSource = from item in notifications
                                                   select new
                                                   {
                                                       ID = item.ID,
                                                       Message = item.Message,
                                                       DetailURL = GetDetailURL(item),
                                                       UnRead = item.UnRead,
                                                       HasButtons = (item.NotificationType == 3)
                                                   };

            lvnotificationclickresult.DataBind();

            pnlNotificationResults.Visible = true;
            btnNotification.CssClass = "notifybtn";

        }

        protected void lvnotificationclickresult_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int notificationId = Convert.ToInt32(e.CommandArgument);
            Notification currentNotification = WebContext.Current.GetNotificationById(notificationId);
            if (e.CommandName == "acceptfriendrequest")
            {
                currentNotification.Action(1);

            }
            else if (e.CommandName == "rejectfriendrequest")
            {
                currentNotification.Action(2);
            }
            else if (e.CommandName == "gotodetails")
            {
                currentNotification.UnRead = false;
                currentNotification.Update();
                if (!string.IsNullOrEmpty(currentNotification.DetailURL))
                {
                    Response.Redirect(Request.Url.ToString());
                }
            }
            Response.Redirect(Request.Url.ToString());
        }

        protected void lvSearchResults_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            //Button commentTextBox = (Button)e.Item.FindControl("btnCancelHighFive");

            TextBox txtHighFiveMessage = (TextBox)e.Item.FindControl("txtHighFiveMessage");
            if (e.CommandName.ToLower() == "sendhighfive")
            {
                if (SendHighFiveClicked != null)
                {
                    SendHighFiveEventArgs evtSend = new SendHighFiveEventArgs();
                    evtSend.RecipientID = Convert.ToInt32(e.CommandArgument);
                    SendHighFiveClicked(this, evtSend);
                }

                //WebContext.Current.SendHighFiveMessage(current, txtHighFiveMessage.Text);
                //Response.Redirect(Request.Url.ToString());
            }
            //else if (e.CommandName.ToLower() == "cancelhighfive")
            //{
            //    Response.Redirect(Request.Url.ToString());
            //}
            else if (e.CommandName.ToLower() == "getconnected")
            {
                User connectTo = WebContext.Current.GetUserByID(Convert.ToInt32(e.CommandArgument));
                WebContext.Current.ConnectToUser(connectTo);
                Response.Write("<script LANGUAGE='JavaScript' >alert('Friend request sent Successfully.');document.location='" + ResolveClientUrl(Request.Url.ToString()) + "';</script>");
                //Response.Redirect(Request.Url.ToString());
            }
        }

        protected void lvSearchResults_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            User user = (User)e.Item.DataItem;

            LinkButton btnGetConnected = (LinkButton)e.Item.FindControl("btnGetConnected");
            Panel pnlHighFive = (Panel)e.Item.FindControl("pnlHighFive");
            if (user.ID == current.ID)
            {
                btnGetConnected.Visible = false;
                pnlHighFive.Visible = false;
            }
            else
            {
                pnlHighFive.Visible = true;
                if (WebContext.Current.CurrentUser.Friends.Where(i => i.ID == user.ID).FirstOrDefault() == null)
                {
                    //User user = WebContext.Current.GetUserByID(Convert.ToInt32(e.CommandArgument));
                    if (WebContext.Current.CurrentUser.CheckPendingRequest(WebContext.Current.CurrentUser.DTO, user.DTO))
                        btnGetConnected.Visible = false;
                    else
                        btnGetConnected.Visible = true;
                }
                else
                    btnGetConnected.Visible = false;
            }
        }

        protected void lvSearchResults_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            //ListViewItem item = lvSearchResults.Items[e.NewEditIndex];
            lvSearchResults.EditIndex = e.NewEditIndex;
            IEnumerable<User> results = WebContext.Current.SearchForUsers(txtSearchUsers.Text);
            lvSearchResults.DataSource = results;
            lvSearchResults.DataBind();

        }

        protected void lvSearchResults_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            lvSearchResults.EditIndex = -1;
            IEnumerable<User> results = WebContext.Current.SearchForUsers(txtSearchUsers.Text);
            lvSearchResults.DataSource = results;
            lvSearchResults.DataBind();
        }
    }
}
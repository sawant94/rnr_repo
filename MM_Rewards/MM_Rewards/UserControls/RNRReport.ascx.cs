﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Idealake.Mahindra.RewardsSocial.DTO;
using Idealake.Web.MMRewardsSocial.Core;
using System.IO;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;

namespace MM_Rewards.UserControls
{
    public partial class RNRReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dtpStartdate.SelectedDate = DateTime.Now.Date.AddDays(-7);
                dtpEnddate.SelectedDate = DateTime.Now.Date;
                loadReport(dtpStartdate.SelectedDate.Value,dtpEnddate.SelectedDate.Value);
            }
        }

        public void loadReport(DateTime startDate,DateTime endDate)
        {
            IEnumerable<Idealake.Mahindra.RewardsSocial.DTO.RNRReport> result = WebContext.Current.getRNRReport(startDate,endDate).ToList();

            //currentContext.ReportRepository.GetDepartmentWiseAbondonedProductReport(dtFromDate.SelectedDate, dtToDate.SelectedDate);

            rptRNRReport.ProcessingMode = ProcessingMode.Local;

            rptRNRReport.LocalReport.ReportPath = "Web_Pages/Reports/Rpt_RNRReport.rdlc";
            
            //rptRNRReport.LocalReport.Refresh();
            //rptRNRReport.LocalReport.DataSources["ds_GetRNRReport"].Value = result;
            //rptRNRReport.LocalReport.DataSources.Clear();

            ReportParameter rParam1 = new ReportParameter("rFromDate",dtpStartdate.SelectedDate.ToString());
            ReportParameter rParam2 = new ReportParameter("rToDate", dtpEnddate.SelectedDate.ToString());
            this.rptRNRReport.LocalReport.SetParameters( new ReportParameter[]{ rParam1, rParam2});


            ReportDataSource rptds = new ReportDataSource("dsRNRReport",  result);
            //rptds.DataSourceId = "dsRNRReport";
            rptRNRReport.LocalReport.DataSources.Clear();
            rptRNRReport.LocalReport.DataSources.Add(rptds);

            rptRNRReport.LocalReport.Refresh();
            rpthold.Style.Add("display", "block");
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            
            loadReport(dtpStartdate.SelectedDate.Value, dtpEnddate.SelectedDate.Value);
        }
    }
}
﻿using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Idealake.Web.MMRewardsSocial.UserControls
{
    public partial class SharePostWithGroups : System.Web.UI.UserControl
    {
        private PostBase current;

        public PostBase Current
        {
            get
            {
                return current;
            }
            set
            {
                current = value;
                chklstGroups.DataSource = WebContext.Current.CurrentUser.Groups;
                chklstGroups.DataTextField = "Title";
                chklstGroups.DataValueField = "ID";
                chklstGroups.DataBind();
                checkSharedGroups();
            }
        }

        public void SetCurrent(PostBase value)
        {
            current = value;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            saveSharedGroups();
        }

        private void saveSharedGroups()
        {
            List<Group> groups = new List<Group>();
            foreach (ListItem li in chklstGroups.Items)
            {
                if (li.Selected == true)
                {
                    Group group = WebContext.Current.GetGroupByID(Convert.ToInt32(li.Value));

                    groups.Add(group);
                }
            }

            current.ShareWith(groups);
        }

        private void checkSharedGroups()
        {
            if (WebContext.Current.CurrentUser.Groups != null)
            {
                if (WebContext.Current.CurrentUser.Groups.Count() > 0)
                {
                    foreach (ListItem li in chklstGroups.Items)
                    {
                        foreach (Group group in current.SharedWith)
                        {
                            if (group.ID == Convert.ToInt32(li.Value))
                            {
                                li.Selected = true;
                            }
                        }
                    }
                    lblError.Text = "";
                    lblError.Visible = false;
                }
                else
                {
                    lblError.Text = "Currently you have no groups to share with";
                    chklstGroups.Visible = false;
                    btnSave.Visible = false;
                }
            }
            else
            {
                lblError.Text = "Currently you have no groups to share with";
                chklstGroups.Visible = false;
                btnSave.Visible = false;
            }
        }
    }
}
﻿using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Idealake.Web.MMRewardsSocial
{
    public partial class ManageGroup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string groupid = Request.QueryString["groupid"];
            Group pageGroup = WebContext.Current.GetGroupByID(Convert.ToInt32(groupid));
            if (IsPostBack)
                ucGroupEdit.SetCurrent(pageGroup);
            else
                ucGroupEdit.Current = pageGroup;

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="ManageGroup.aspx.cs" Inherits="Idealake.Web.MMRewardsSocial.ManageGroup" %>

<%@ Register Src="~/UserControls/GroupEdit.ascx" TagPrefix="uc1" TagName="GroupEdit" %>
<asp:Content runat="server" ContentPlaceHolderID="PlaceholderAddtionalPageHead" ID="AddtionalPageHead">
 <link rel="stylesheet" href="../includes/css/smoothness.css" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.jscrollpane.min.js" type="text/javascript"></script>
    <link href="../includes/css/jscroll.css" rel="stylesheet" />
    <script src="../includes/Js/rewardssocial.js" type="text/javascript"></script>
    <link href="../includes/css/rewardssocial.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <uc1:GroupEdit runat="server" ID="ucGroupEdit" />
</asp:Content>

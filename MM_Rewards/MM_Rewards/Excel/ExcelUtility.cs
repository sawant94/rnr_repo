﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Idealake.Mahindra.RewardsSocial.DTO;
using ClosedXML.Excel;

namespace MM_Rewards.Excel
{
    public class ExcelUtility
    {
        public static IEnumerable<UserFriend> ExtractUserFriends(System.IO.Stream frienddataXlsx)
        {
            List<UserFriend> friends = new List<UserFriend>();
            var wb = new XLWorkbook(frienddataXlsx);
            var ws = wb.Worksheet(1);

            // Look for the first row used
            var firstRowUsed = ws.Cell("A2");

            var lastDataCell = ws.LastCellUsed();

            var rngData = ws.Range(firstRowUsed.Address, lastDataCell.Address);
            foreach (IXLRangeRow r in rngData.Rows())
            {
                char nextColumn = r.FirstCell().Address.ColumnLetter.ToCharArray()[0];
                string rowLetter = r.FirstCell().Address.RowNumber.ToString();
                UserFriend friend = new UserFriend();

                friend.usertokennumber = r.FirstCell().GetValue<string>();
                nextColumn++;

                friend.friendtokennumber = r.Cell(nextColumn.ToString()).GetValue<string>();
                nextColumn++;

                friends.Add(friend);
            }
            return friends;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using MM.DAL;
using BL_Rewards.BL_Common;
using System.DirectoryServices;
using System.Configuration;
using BE_Rewards;
using BL_Rewards.Admin;
using BE_Rewards.BE_Admin;
namespace MM_Rewards
{
    public partial class Authentication : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        DAL_Common objDAL = new DAL_Common();
        Common objCommon = new Common();
        List<MenuItem> lstMenuItems = new List<MenuItem>();
        BL_Testimonial objBL_Testimonial = new BL_Testimonial();
        BE_TestimonialColl objBE_TestimonialColl = new BE_TestimonialColl();
 
        protected void Page_Load(object sender, EventArgs e)
        {
           
            string name = WindowsIdentity.GetCurrent().Name;
            if (this.Session["windowsUserName"] == null)
                this.Response.Redirect("~/Web_Pages/CustomLogin.aspx");
            else
                name = this.Session["windowsUserName"].ToString();
            try
            {
                this.light.Style.Add("display", "none");
                this.fade.Style.Add("display", "none");
                if (this.Session["TokenNumber"] == null)
                {
                    if (!string.IsNullOrEmpty(name) && name != "")
                    {
                        string str = name.Substring(name.LastIndexOf("\\") + 1);
                        if (this.objCommon.IsUserExist(str) == 1)
                        {
                            myModal.Attributes["style"] = "display:none"; 
                            int int16 = (int)Convert.ToInt16(this.objCommon.GetUserDetails(str));
                            this.light.Style.Add("display", "none");
                            this.fade.Style.Add("display", "none");
                            this.CreateMenu(this.objCommon.GetUserMenu(str));
                        }
                        else
                        {
                            myModal.Attributes["style"] = "display:block"; 
                        }

                    }
                    else
                    {
                        this.light.Style.Add("display", "block");
                        this.fade.Style.Add("display", "block");
                        Button btnLogin = this.btnLogin;
                        string str = btnLogin.OnClientClick + "return btnLogin_ClientClick('" + this.txtLoginTokenNumber.ClientID + "','" + this.txtPassWord.ClientID + "')";
                        btnLogin.OnClientClick = str;
                    }
                }
                else
                    this.CreateMenu(this.objCommon.GetUserMenu(this.Session["TokenNumber"].ToString()));
                if (DateTime.Today >= new DateTime((int)Convert.ToInt16(ConfigurationManager.AppSettings["SBDYear"]), (int)Convert.ToInt16(ConfigurationManager.AppSettings["SBDMonth"]), (int)Convert.ToInt16(ConfigurationManager.AppSettings["SBDDay"])) && DateTime.Today <= new DateTime((int)Convert.ToInt16(ConfigurationManager.AppSettings["EBDYear"]), (int)Convert.ToInt16(ConfigurationManager.AppSettings["EBDMonth"]), (int)Convert.ToInt16(ConfigurationManager.AppSettings["EBDDay"])))
                {
                    this.HomeBody.Attributes.Add("class", "hmBanBD");
                }
                else
                {
                    //this.HomeBody.Attributes.Add("class", "hmBan");
                    //this.homebannDiv.Attributes.Add("class", "hmBan");//Rakesh
                    this.divBossday.Visible = false;
                }
                if (this.Session["UserName"] == null)
                    return;
                //this.lblWelcomeUser.Text = this.Session["UserName"].ToString(); //Rakesh              
                if (Session["requesturl"] != null)
                {
                   
                    HttpContext.Current.Response.Redirect("Rewards/PersonalizedMessage.aspx");
                   
                }                
            }
            catch (Exception ex)
            {
                this.objExceptionLogging.LogErrorMessage(ex, "HomePage.aspx");
            }
        }

        private void CreateMenu(List<BE_Common> lst)
        {
            try
            {
                homeMenu.Items.Clear();


                objBE_TestimonialColl = objBL_Testimonial.GetDataForSlider();
                //ImageUrl="~/Testimonial_new_images/26.png"
                if (objBE_TestimonialColl.Count==1)
                {
                    Image1.ImageUrl = "~/TestimoinalImage/" + objBE_TestimonialColl[0].ImageName;
                    lblImage1.Text = objBE_TestimonialColl[0].TestimonoialText;
                    lblName1.Text = objBE_TestimonialColl[0].EmployeeName;
                    Image1.Visible = true;
                    lblImage1.Visible = true;
                    lblName1.Visible = true;
                }
                if (objBE_TestimonialColl.Count == 2)
                {
                    //Image2.ImageUrl =Server.MapPath("~/Testimonial_new_images/" + objBE_TestimonialColl[1].ImageName);
                    Image2.ImageUrl = "~/TestimoinalImage/" + objBE_TestimonialColl[1].ImageName;
                    lblImage2.Text = objBE_TestimonialColl[1].TestimonoialText;
                    lblName2.Text = objBE_TestimonialColl[1].EmployeeName;
                    Image2.Visible = true;
                    lblImage2.Visible = true;
                    lblName2.Visible = true;
                }
                if (objBE_TestimonialColl.Count == 3)
                {
                    //Image3.ImageUrl =Server.MapPath("~/Testimonial_new_images/" + objBE_TestimonialColl[2].ImageName);
                    Image3.ImageUrl = "~/TestimoinalImage/" + objBE_TestimonialColl[2].ImageName;
                    lblImage3.Text = objBE_TestimonialColl[2].TestimonoialText;
                    lblName3.Text = objBE_TestimonialColl[2].EmployeeName;
                    Image3.Visible = true;
                    lblImage3.Visible = true;
                    lblName3.Visible = true;
                }
                if (objBE_TestimonialColl.Count == 4)
                {
                    //Image4.ImageUrl = Server.MapPath("~/Testimonial_new_images/" + objBE_TestimonialColl[3].ImageName);
                    Image4.ImageUrl = "~/TestimoinalImage/" + objBE_TestimonialColl[3].ImageName;
                    lblImage4.Text = objBE_TestimonialColl[3].TestimonoialText;
                    lblName4.Text = objBE_TestimonialColl[3].EmployeeName;
                    Image4.Visible = true;
                    lblImage4.Visible = true;
                    lblName4.Visible = true;
                }
                if (objBE_TestimonialColl.Count == 5)
                {
                    Image5.ImageUrl = "~/TestimoinalImage/" + objBE_TestimonialColl[4].ImageName;
                    lblImage5.Text = objBE_TestimonialColl[4].TestimonoialText;
                    lblName5.Text = objBE_TestimonialColl[4].EmployeeName;
                    Image5.Visible = true;
                    lblImage5.Visible = true;
                    lblName5.Visible = true;
                    ////rpttestimonial.DataSource = objBE_TestimonialColl;
                    ////rpttestimonial.DataBind();
                }

                int _BudgetExist_TokenNo = Convert.ToInt16(objCommon.GetBudgetExist_TokenNumber(Session["TokenNumber"].ToString()));
                int _Levels = Convert.ToInt16(objCommon.GetUserDetails(Session["TokenNumber"].ToString()));
                lvl.Value = _Levels.ToString();

                MenuItem mnumenu, mnuchild;
                mnumenu = new MenuItem("Home", "~/Web_Pages/HomePage.aspx", "", "~/Web_Pages/HomePage.aspx");
                lstMenuItems.Add(mnumenu);

                string menu = "";
                mnumenu = new MenuItem();

                foreach (BE_Common objBE_Common in lst)
                {
                    if (objBE_Common.ParentMenuName != menu && objBE_Common.ParentMenuName != "ROOT")
                    {
                        if (menu != "")
                            lstMenuItems.Add(mnumenu);
                        menu = objBE_Common.ParentMenuName;
                        mnumenu = new MenuItem(objBE_Common.ParentMenuName.ToLower());
                        mnuchild = new MenuItem(objBE_Common.MenuName.ToLower(), "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                        mnumenu.ChildItems.Add(mnuchild);
                        mnumenu.Selectable = false;
                    }
                    else if (objBE_Common.ParentMenuName.ToUpper() != "ROOT")
                    {
                        mnuchild = new MenuItem(objBE_Common.MenuName.ToLower(), "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                        mnumenu.ChildItems.Add(mnuchild);
                    }
                    else if (objBE_Common.ParentMenuName.ToUpper() == "ROOT")
                    {
                        //Higher levels do not receive awards, hence no need for redemption page
                        //AWARD RECEIVED BY ME //old=MY AWARDS
                        if (lvl.Value != "" && objBE_Common.MenuName.ToUpper() == "AWARD RECEIVED BY ME")// && Convert.ToInt32(lvl.Value.ToString()) < 7)
                        {
                            // lock has been removed , now MY Award screen is open to everyone

                            lstMenuItems.Add(mnumenu);
                            lstMenuItems.RemoveAll(x => x.Text == "");
                            mnumenu = new MenuItem(objBE_Common.MenuName.ToLower(), "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                        }
                        //9 and Lower levels do not give awards, hence no need for that page
                        else
                            if (lvl.Value != "" && objBE_Common.MenuName.ToUpper() == "GIVE AWARD")
                            {
                                int Show = Convert.ToInt16(objCommon.CheckEligibility_GiveAwards(Session["TokenNumber"].ToString()));
                                if (Show >= 0 && Show <=11)//9-- changed--Rakesh
                                {

                                    lstMenuItems.Add(mnumenu);
                                    lstMenuItems.RemoveAll(x => x.Text == "");
                                    mnumenu = new MenuItem(objBE_Common.MenuName.ToLower(), "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                                }


                            }
                            //No  constraint for other Levels/ users/ menu options
                            else
                            {
                                if (mnumenu.Text != "")
                                {
                                    lstMenuItems.Add(mnumenu);
                                }
                                mnumenu = new MenuItem(objBE_Common.MenuName.ToLower(), "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                            }
                    }
                }

                lstMenuItems.Add(mnumenu);

                //Added By Archana K. On 24-08-2016
                //Int64 _CashAwardExist_TokenNo = Convert.ToInt64(objCommon.GetCashAwardExist_TokenNumber(Session["TokenNumber"].ToString()));
                //if (_CashAwardExist_TokenNo != 0)
                //{

                //    mnumenu = new MenuItem("Letter of Appreciation", "~/Web_Pages/Rewards/CashAwardPDF.aspx", "", "~/Web_Pages/Rewards/CashAwardPDF.aspx");
                //    lstMenuItems.Add(mnumenu);
                //}

                //mnumenu = new MenuItem("Contact US", "~/Web_Pages/ContactUs.aspx", "", "~/Web_Pages/ContactUs.aspx");
                //lstMenuItems.Add(mnumenu);
             //   lstMenuItems.Add(new MenuItem("My R&R Wall", "~/UserProfile.aspx", "", "~/UserProfile.aspx"));
                //mnumenu = new MenuItem("Milestone", "https://epmobile.mahindra.com/sap/bc/ui5_ui5/sap/ZHR_MILESTONES/index.html?sap-client=100&sap-language=EN", "", "https://epmobile.mahindra.com/sap/bc/ui5_ui5/sap/ZHR_MILESTONES/index.html?sap-client=100&sap-language=EN");
                //mnumenu.Target = "_blank";
                //lstMenuItems.Add(mnumenu);


                mnumenu = new MenuItem("Help");
                //mnumenu.ChildItems.Add(new MenuItem("FAQ", "~/Web_Pages/FAQ.aspx", "", "~/Web_Pages/FAQ.aspx", "_blank"));
                //mnumenu.ChildItems.Add(new MenuItem("User Manual", "~/RnR_UserManual.pdf", "", "~/RnR_UserManual.pdf", "_blank"));
                //mnumenu.ChildItems.Add(new MenuItem("Letter Template 1000 BHP/HP", "~/Excellerator Letter 1000 BHP.doc", "", "~/Excellerator Letter 1000 BHP.doc", "_blank"));
                //mnumenu.ChildItems.Add(new MenuItem("Letter Template 750 BHP/HP", "~/Excellerator Letter 750 BHP.doc", "", "~/Excellerator Letter 750 BHP.doc", "_blank"));
                //mnumenu.ChildItems.Add(new MenuItem("Letter Template 500 BHP/HP", "~/Excellerator Letter 500 BHP.doc", "", "~/Excellerator Letter 500 BHP.doc", "_blank"));
                //mnumenu.ChildItems.Add(new MenuItem("Recognition Hand Book", "~/RR_Booklet_in_Soft.pdf", "", "~/RR_Booklet_in_Soft.pdf", "_blank"));
                //mnumenu.ChildItems.Add(new MenuItem("Recognition Booklet", "~/RR_Booklet_in_Soft1.pdf", "", "~/RR_Booklet_in_Soft1.pdf", "_blank"));
                mnumenu.ChildItems.Add(new MenuItem("Milestone", "https://epmobile.mahindra.com/sap/bc/ui5_ui5/sap/ZHR_MILESTONES/index.html?sap-client=100&sap-language=EN", "", "https://epmobile.mahindra.com/sap/bc/ui5_ui5/sap/ZHR_MILESTONES/index.html?sap-client=100&sap-language=EN", "_blank"));
                //mnumenu.ChildItems.Add(new MenuItem("IMCR CRE Awards", "~/IMCR_CRE_Award Guidelines.pdf", "", "~/IMCR_CRE_Award Guidelines.pdf", "_blank"));
                mnumenu.ChildItems.Add(new MenuItem("AFS R&R guidelines", "~/AFS_RR_guidelines.pdf", "", "~/AFS_RR_guidelines.pdf", "_blank"));

                Int64 _CashAwardExist_TokenNo = Convert.ToInt64(objCommon.GetCashAwardExist_TokenNumber(Session["TokenNumber"].ToString()));
                if (_CashAwardExist_TokenNo != 0)
                {
                    mnumenu.ChildItems.Add(new MenuItem("Letter of Appreciation", "~/Web_Pages/Rewards/CashAwardPDF.aspx", "", "~/Web_Pages/Rewards/CashAwardPDF.aspx", "_self"));
                }

                mnumenu.Selectable = false;

              
                lstMenuItems.Add(mnumenu);

             

                foreach (MenuItem item in lstMenuItems)
                {
                    homeMenu.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "HomePage.aspx");
            }

        }

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string strPath = ConfigurationManager.AppSettings["DirPath"];

                DirectoryEntry entry = new DirectoryEntry(strPath, txtLoginTokenNumber.Text, txtPassWord.Text);

                if (!string.IsNullOrEmpty(entry.Name))
                {
                    //Means authenticated by LDAP ........... failure will be caught in CATCH BLOCK
                    int Levels = Convert.ToInt16(objCommon.GetUserDetails(txtLoginTokenNumber.Text.Trim()));

                    if (Levels == -1 || Levels <= 0)
                    {
                        ThrowAlertMessage("Please Enter Correct Token Number");
                        btnLogin.OnClientClick += "return btnLogin_ClientClick('" + txtLoginTokenNumber.ClientID + "','" + txtPassWord.ClientID + "')";
                    }
                    else
                    {
                        light.Style.Add("display", "none");
                        fade.Style.Add("display", "none");
                        //lblWelcomeUser.Text = Session["UserName"].ToString();

                        Session["LoginType"] = "l";
                        if (Session["LoginType"].ToString() == "l")
                            btnSignOut.Visible = true;

                        List<BE_Common> lstPageName = objCommon.GetUserMenu(txtLoginTokenNumber.Text.Trim());
                        if (homeMenu.Items.Count < 1)
                        { CreateMenu(lstPageName); }

                    }
                }
                else
                {
                    //LDAP Failed
                    //lblErrorMsgs.Text = "LDAP Failed";
                    light.Style.Add("display", "block");
                    fade.Style.Add("display", "block");

                    btnLogin.OnClientClick += "return btnLogin_ClientClick('" + txtLoginTokenNumber.ClientID + "','" + txtPassWord.ClientID + "')";

                }
            }
            catch (Exception ex)
            {
                //ThrowAlertMessage(MM_Messages.DatabaseError);
                ThrowAlertMessage("Login Failed!");
                //LDAP Failed
                //lblErrorMsgs.Text = "LDAP Failed:"+ex.Message;
                light.Style.Add("display", "block");
                fade.Style.Add("display", "block");
                btnLogin.OnClientClick += "return btnLogin_ClientClick('" + txtLoginTokenNumber.ClientID + "','" + txtPassWord.ClientID + "')";

                txtLoginTokenNumber.Text = "";
                txtPassWord.Text = "";
                objExceptionLogging.LogErrorMessage(ex, "HomePage.aspx");

            }
        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Session["TokenNumber"] = null;
            //lblWelcomeUser.Text = ""; //Rakesh
            //Page_Load(sender, EventArgs.Empty);
            Session.Abandon();
            Response.Redirect("~/Web_Pages/HomePage.aspx");
        }
    }

}
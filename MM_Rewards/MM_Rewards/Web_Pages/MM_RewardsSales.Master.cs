﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using BE_Rewards.BE_Admin;
using System.Security.Principal;

namespace MM_Rewards.Web_Pages
{
    public partial class MM_RewardsSales : System.Web.UI.MasterPage
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        Common objCommon;
        List<MenuItem> lstMenuItems = new List<MenuItem>();

        protected void Page_Load(object sender, EventArgs e)
        {
            string windowsUserName = WindowsIdentity.GetCurrent().Name;
            try
            {              
                if (Session["TokenNumber"] == null)
                {
                    Common.CheckUserLoggedIn();
                }
                else
                {
                    objCommon = new Common();
                    windowsUserName = Session["TokenNumber"].ToString();
                    int IsExist = objCommon.IsUserExist(windowsUserName);
                    if (IsExist == 1)
                    {
                        int Levels = Convert.ToInt16(objCommon.GetSalesUserDetails(windowsUserName));
                    }
                    else if (IsExist == 0)
                    {
                        //tblLogin.Visible = true;
                        //Response.Redirect("~/Web_Pages/HomePage.aspx");
                    }

                    // ======== For Creating The Menu ==================//

                    List<BE_Common> lstPageName = objCommon.GetUserMenu(windowsUserName);
                    CreateMenu(lstPageName);

                    // Completing the welcome note
                    lblWelcomeUser.Text = Session["UserName"].ToString();
                }
            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "HomePage.aspx");
            }
        }

        private void CreateMenu(List<BE_Common> lst)
        {
            MainMenu.Items.Clear();

            int _BudgetExist_TokenNo = Convert.ToInt16(objCommon.GetSalesBudgetExist_TokenNumber(Session["TokenNumber"].ToString()));
            int _Levels = Convert.ToInt16(objCommon.GetUserDetails(Session["TokenNumber"].ToString()));
            lvl.Value = _Levels.ToString();

            MenuItem mnumenu, mnuchild;
            mnumenu = new MenuItem("HOME", "~/Web_Pages/HomePage.aspx", "", "~/Web_Pages/HomePage.aspx");
            lstMenuItems.Add(mnumenu);

            string menu = "";
            mnumenu = new MenuItem();

            foreach (BE_Common objBE_Common in lst)
            {
                if (objBE_Common.ParentMenuName != menu && objBE_Common.ParentMenuName != "ROOT")
                {
                    if (menu != "")
                        lstMenuItems.Add(mnumenu);
                    menu = objBE_Common.ParentMenuName;
                    mnumenu = new MenuItem(objBE_Common.ParentMenuName);
                    mnuchild = new MenuItem(objBE_Common.MenuName, "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                    mnumenu.ChildItems.Add(mnuchild);
                    mnumenu.Selectable = false;
                }
                else if (objBE_Common.ParentMenuName.ToUpper() != "ROOT")
                {

                    mnuchild = new MenuItem(objBE_Common.MenuName, "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                    mnumenu.ChildItems.Add(mnuchild);

                }
                else if (objBE_Common.ParentMenuName.ToUpper() == "ROOT")
                {
                    //Higher levels do not receive awards, hence no need for redemption page
                    if (lvl.Value != "" && objBE_Common.MenuName.ToUpper() == "MY AWARDS")//&& Convert.ToInt32(lvl.Value.ToString()) < 7)
                    {
                        // lock has been removed , now MY Award screen is open to everyone
                        lstMenuItems.Add(mnumenu);
                        mnumenu = new MenuItem(objBE_Common.MenuName, "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);

                    }
                    //9 and Lower levels do not give awards, hence no need for that page
                    else if (lvl.Value != "" && objBE_Common.MenuName.ToUpper() == "GIVE AWARD")// && Convert.ToInt32(lvl.Value.ToString()) > 8)
                    {
                        int Show = Convert.ToInt16(objCommon.CheckEligibility_GiveAwards(Session["TokenNumber"].ToString()));
                        if (Show > 0 && Show < 9)
                        {
                            lstMenuItems.Add(mnumenu);
                            mnumenu = new MenuItem(objBE_Common.MenuName, "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                        }
                    }
                    //No  constraint for other Levels/ users/ menu options
                    else
                    {
                        if (mnumenu.Text != "")
                        {
                            lstMenuItems.Add(mnumenu);
                        }
                        mnumenu = new MenuItem(objBE_Common.MenuName, "~" + objBE_Common.PageName, "", "~" + objBE_Common.PageName);
                    }
                }
            }

            lstMenuItems.Add(mnumenu);

            mnumenu = new MenuItem("Contact US", "~/Web_Pages/ContactUs.aspx", "", "~/Web_Pages/ContactUs.aspx");
            lstMenuItems.Add(mnumenu);

            lstMenuItems.Add(new MenuItem("My R&R Wall", "~/UserProfile.aspx", "", "~/UserProfile.aspx"));

            mnumenu = new MenuItem("Help");
            mnumenu.ChildItems.Add(new MenuItem("FAQ", "~/Web_Pages/FAQ.aspx", "", "~/Web_Pages/FAQ.aspx", "_blank"));
            mnumenu.ChildItems.Add(new MenuItem("User Manual", "~/RnR_UserManual.pdf", "", "~/RnR_UserManual.pdf", "_blank"));
            mnumenu.ChildItems.Add(new MenuItem("Letter Template 1000 BHP/HP", "~/Excellerator Letter 1000 BHP.doc", "", "~/Excellerator Letter 1000 BHP.doc", "_blank"));
            mnumenu.ChildItems.Add(new MenuItem("Letter Template 750 BHP/HP", "~/Excellerator Letter 750 BHP.doc", "", "~/Excellerator Letter 750 BHP.doc", "_blank"));
            mnumenu.ChildItems.Add(new MenuItem("Letter Template 500 BHP/HP", "~/Excellerator Letter 500 BHP.doc", "", "~/Excellerator Letter 500 BHP.doc", "_blank"));
            mnumenu.ChildItems.Add(new MenuItem("Recognition Hand Book", "~/RR_Booklet_in_Soft.pdf", "", "~/RR_Booklet_in_Soft.pdf", "_blank"));
            mnumenu.Selectable = false;
            lstMenuItems.Add(mnumenu);

            foreach (MenuItem item in lstMenuItems)
            {
                MainMenu.Items.Add(item);
            }
        }

        protected void MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            item.Selected = true;
        }       

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Session["TokenNumber"] = null;
            lblWelcomeUser.Text = "";
            Session.Abandon();
            Response.Redirect("~/Web_Pages/HomePage.aspx");

        }
    }
}
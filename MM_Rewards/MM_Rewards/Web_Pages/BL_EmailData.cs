﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.IO;
using System.Text;
using EvoPdf;
using System.Net;
using System.Drawing;
using System.Configuration;
using System.Drawing.Imaging;
using BE_Rewards.BE_Rewards;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;

namespace MM_Rewards.Web_Pages.Rewards
{
    public class BL_EmailData
    {
        public void ProcessAndSendEmail(BE_EmailData obj)
        {
            //BE_EmailData obj = new BE_EmailData();
            //obj.RecipientName = "Ridhima Thakur";
            //obj.ReasonForNomination = "I appreciate & acknowledge the 3D's Discipline, Dedication & Determination you demonstrated during this year,Congratulation for your outstanding work.";
            //obj.GiverName = "Niraj Zanzad";
            //obj.AwardDate = DateTime.Now;
            //obj.AwardType = "SPOT AWARD";
            //obj.SenderDesignation = "HOD";
            //obj.GiverToken = "23188415";
            //obj.RecipientToken = "25001411";
            //obj.AwardAmount = "1000";
            //obj.RecipientEmail = obj.RecipientToken + "@mahindra.com";
            //obj.Subject = "Congratulations!!!";
            //obj.GiverEmail = "rewards_and_recognition@mahindra.com";
            //obj.CCID = "23188415@MAHINDRA.COM,23061948@MAHINDRA.COM,borsra-cont@mahindra.com";
            //BL_EmailData objBL_EmailData = new BL_EmailData();
            //objBL_EmailData.ProcessAndSendEmail(obj);


            string TemplateFileName = "SpotAwardCertificate.txt";
            string BodyPart = "";
            StreamReader sr = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/MailDetails/" + TemplateFileName));
            //StreamReader sr = new StreamReader(ConfigurationManager.AppSettings["AwardTypeTemplatePath"].ToString() + TemplateFileName);            
            StringBuilder sb = new StringBuilder();
            BodyPart = sr.ReadToEnd();

            BodyPart = BodyPart.Replace("#name", obj.RecipientName);
            BodyPart = BodyPart.Replace("#hodmsg", obj.ReasonForNomination);
            BodyPart = BodyPart.Replace("#GiverToken", obj.GiverName);
            BodyPart = BodyPart.Replace("#awardeddate", obj.AwardDate.ToString("dd-MMM-yyyy"));
            //obj.AwardType=obj.AwardType+" AWARD"
            BodyPart = BodyPart.Replace("#awrd", obj.AwardType.ToUpper());
            BodyPart=BodyPart.Replace("#Manager",obj.SenderDesignation);
            // Create a HTML to Image converter object with default settings
            HtmlToImageConverter htmlToImageConverter = new HtmlToImageConverter();
            htmlToImageConverter.HtmlViewerWidth = 900;
            htmlToImageConverter.HtmlViewerHeight = 650;
            htmlToImageConverter.ConversionDelay = 0;
            System.Drawing.Image[] imageTiles = null;
            string baseUrl = "";
            imageTiles = htmlToImageConverter.ConvertHtmlToImageTiles(BodyPart, baseUrl);
            System.Drawing.Image outImage = imageTiles[0];
            Bitmap bmp = new Bitmap(outImage);

            string imageName = obj.RecipientToken + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".jpg";
            //string imageName = ConfigurationSettings.AppSettings["EcardImg_Path"].ToString() + awardName + DateTime.Now.ToString("ddMMyyyyhh")+"_"+recipientToken + ".jpg";
            string path = ConfigurationManager.AppSettings["Certificate_Path"].ToString();
            imageName=(ConfigurationManager.AppSettings["Certificate_Path"].ToString() + imageName);
            
            bmp.Save(imageName);
            bmp.Dispose();

            // crop image ProcessAndSendMails save
            //Image img = Image.FromFile(ConfigurationManager.AppSettings["Certificate_Path"].ToString() + imageName);
            Image img = Image.FromFile(imageName);

            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(new Rectangle(17, 17, 817, 585), bmpImage.PixelFormat);
            string attimagename = obj.AwardType + "_" + obj.RecipientToken + "_" + obj.GiverToken + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".jpg";
            string AttachmentPath=ConfigurationManager.AppSettings["Certificate_Path"].ToString() + attimagename;
            bmpCrop.Save(AttachmentPath);          
            bmpCrop.Dispose();
            // Attachment
            //using (MemoryStream memoryStream = new MemoryStream())
            //{
            //    byte[] bytes = memoryStream.ToArray();
            //    memoryStream.Close();
            //    Attachments = (new Attachment(ConfigurationManager.AppSettings["Certificate_Path"].ToString() + attimagename));
            //}

            // Get body
            TemplateFileName = "SpotAwardCertificateMailAttatchment.txt";
            string BodyPart1 = "";
            //StreamReader sr1 = new StreamReader(ConfigurationManager.AppSettings["AwardTypeTemplatePath"].ToString() + TemplateFileName);
            StreamReader sr1 = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/MailDetails/" + TemplateFileName));
            StringBuilder sb1 = new StringBuilder();
            BodyPart1 = sr1.ReadToEnd();

            BodyPart1 = BodyPart1.Replace("<%RecipientName%>",obj.RecipientName);
            BodyPart1 = BodyPart1.Replace("<%GiverHODName%>", obj.GiverName);
            BodyPart1 = BodyPart1.Replace("<%AwardType%>", obj.AwardType+" Award");
            BodyPart1 = BodyPart1.Replace("<%Amount%>", obj.AwardAmount);
            BodyPart1 = BodyPart1.Replace("#imgname", attimagename);
            if (obj.AwardType == "Quarterly Award")
            {
                string refnumber = "Reference Number = " + obj.ReferenceNumber;
                BodyPart1 = BodyPart1.Replace("<%ReferenceNumber%>", refnumber);
            }
            else
            {
                BodyPart1 = BodyPart1.Replace("<%ReferenceNumber%>", "");

            }
            //mail start
            MailMessage message = new MailMessage();
            obj.RecipientEmail = "borsra-cont@mahindra.com";
            obj.GiverEmail = "rewards_and_recognition@mahindra.com";
            message.To.Add(obj.RecipientEmail);//(to) Email-ID of Receiver  
            message.Subject = obj.Subject;// Subject of Email  
            message.From = new MailAddress(obj.GiverEmail);// Email-ID of Sender 
            message.Body = BodyPart1;
            message.IsBodyHtml = true;
            //<%ReferenceNumber%>
            string strCCID = obj.CCID;
            if (!string.IsNullOrEmpty(strCCID))
            {
                string[] strCC = strCCID.Split(';');
                foreach (string str_CCId in strCC)
                {
                    if (str_CCId.Trim() != string.Empty)
                    {
                        message.CC.Add(str_CCId);
                    }
                }
            }

            Attachment att = new Attachment(AttachmentPath);
            att.ContentDisposition.Inline = true;
            message.Attachments.Add(att);

            SmtpClient SmtpMail = new SmtpClient();
            SmtpMail.Host = System.Configuration.ConfigurationManager.AppSettings["SMTP"];
            //SmtpMail.Port = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Port"]); //25;//Port for sending the mail  
            SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpMail.EnableSsl = false;
            SmtpMail.ServicePoint.MaxIdleTime = 20;
            SmtpMail.ServicePoint.SetTcpKeepAlive(true, 20000, 20000);
            message.BodyEncoding = Encoding.Default;
            message.Priority = MailPriority.High;
            try
            {
                //SmtpMail.Send(message);

               if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings.Get("MailLogs"))))
                    {
                        string LogFileName = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["MailLogs"].ToString()), DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt");
                        if (!Directory.Exists(Path.GetDirectoryName(LogFileName)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(LogFileName));
                        }
                        FileStream logFileStream = null;

                        try
                        {
                            string FilePath = Path.GetDirectoryName(LogFileName);

                            if (File.Exists(LogFileName) == false)
                            {
                                // If file doexs not exists create a new file at the specified path
                                logFileStream = File.Open(LogFileName, FileMode.Create, FileAccess.ReadWrite);
                            }
                            else
                            {
                                logFileStream = File.Open(LogFileName, FileMode.Append, FileAccess.Write);
                            }
                        }
                        finally
                        {
                            if (logFileStream != null)
                            {
                                using (StreamWriter MailLogWriter = new StreamWriter(logFileStream))
                                {
                                    MailLogWriter.WriteLine("-- -- -- -- -- -- -- -- -- -- -- -- ");
                                    MailLogWriter.WriteLine("Subject: " + message.Subject);
                                    MailLogWriter.WriteLine("Date: " + DateTime.Now.ToString() + ", SenderEmailID: " + obj.GiverToken + ", Sender Name: " + obj.GiverName + ", ReceiverEmailID: " + obj.RecipientEmail + " Receiver: " + obj.RecipientName);
                                    MailLogWriter.WriteLine("-- -- -- -- -- -- -- -- -- -- -- -- ");
                                }
                                logFileStream.Close();
                            }
                        }
                        //END
                    }
                }
                catch (Exception GeneralEx)
                {
                    throw new ArgumentException(GeneralEx.Message);
                }
                finally
                {
                    message = null;
                }
                        
        }
       
        public static bool IsValidEmail(string email)
        {
            if (email.Contains('~'))
                return false;
            if (email.Contains('#'))
                return false;
            if (email.Contains('$'))
                return false;
            if (email.Contains('%'))
                return false;
            if (email.Contains('^'))
                return false;
            if (email.Contains('&'))
                return false;
            if (email.Contains('*'))
                return false;
            if (email.Contains('/'))
                return false;
            if (email.Contains('('))
                return false;
            if (email.Contains(')'))
                return false;
            if (email.Contains('?'))
                return false;
            if (email.Contains('\\'))
                return false;
            if (email.Contains('|'))
                return false;
            if (email.Contains('+'))
                return false;
            if (email.Contains('+'))
                return false;
            if (email.Contains(','))
                return false;
            if (email.Contains(';'))
                return false;
            if (email.Contains(':'))
                return false;
            if (email.Contains('\''))
                return false;
            if (email.Contains('"'))
                return false;
            if (email.Contains('<'))
                return false;
            if (email.Contains('>'))
                return false;
            if (email.Contains('`'))
                return false;
            Regex r = new Regex(@"^[^@\s]+@[^@\s]+(\.[^@\s]+)+$");
            return r.IsMatch(email);
        }
    }
}
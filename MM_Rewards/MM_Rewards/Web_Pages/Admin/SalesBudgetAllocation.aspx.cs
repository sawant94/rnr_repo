﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using Telerik.Web.UI;
using System.Data;
using BE_Rewards;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using System.IO;
using System.Text;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class SalesBudgetAllocation : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BE_SalesBudgetAllocation objBE_SalesBudgetAllocation = new BE_SalesBudgetAllocation();
        BE_SalesBudgetAllocationColl objBE_SalesBudgetAllocationColl = new BE_SalesBudgetAllocationColl();
        BL_SalesBudgetAllocation objBL_SalesBudgetAllocation = new BL_SalesBudgetAllocation();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();  // check if the loged in user has access to this page,if not redirect him to home page

                if (!IsPostBack)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn());

                    grdRecipient.DataSource = dt;
                    grdRecipient.DataBind();
                    FillDropDown();
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesBudgetAllocation.aspx");
            }
        }

        private void FillDropDown()
        {
            //Division 
            objBE_SalesBudgetAllocationColl = objBL_SalesBudgetAllocation.GetDivision();
            ddlDivision.DataSource = objBE_SalesBudgetAllocationColl;
            ddlDivision.DataTextField = "DivisionName";
            ddlDivision.DataValueField = "DivisionId";
            ddlDivision.DataBind();
            ddlDivision.Items.Insert(0, "Select");
        }

        /// <summary>
        /// Gets CEOs matching the search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGetCEOs_Click(object sender, EventArgs e)
        {
            try
            {                
                objBE_SalesBudgetAllocation.SearchBy = txtSearchUser.Text;

                objBE_SalesBudgetAllocationColl = objBL_SalesBudgetAllocation.GetData(objBE_SalesBudgetAllocation);
                grdCEO.DataSource = objBE_SalesBudgetAllocationColl;
                grdCEO.DataBind();
                grdCEO.Visible = true;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesBudgetAllocation.aspx");
            }
        }

        /// <summary>
        /// Gets HODs matching the search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGetRecipients_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = grdCEO.Rows[Convert.ToInt32(hdnSelectedRowIndex.Value)];
                row.CssClass = Convert.ToInt32(hdnSelectedRowIndex.Value) % 2 == 0 ? "selectedoddTd" : "selectedevenTd";
                string str = hdnTotal.Value;// lblTotal.Text;
                pnlRecipient.Style.Add("display", "block");

                objBE_SalesBudgetAllocation.DivisionName = ddlDivision.SelectedItem.ToString();
                objBE_SalesBudgetAllocation.SearchBy = txtSearchRecipients.Text;
                objBE_SalesBudgetAllocation.TokenNumber = hdnTokenNumber.Value;

                objBE_SalesBudgetAllocationColl = objBL_SalesBudgetAllocation.GetUserData(objBE_SalesBudgetAllocation);

                grdRecipient.DataSource = objBE_SalesBudgetAllocationColl;
                grdRecipient.DataBind();
                lblTotal.Text = str;

                //foreach (GridViewRow item in grdRecipient.Rows)
                //{
                //    TextBox txtBudget = (TextBox)item.FindControl("txtBudget");
                //    RadDatePicker dpStartDate = (RadDatePicker)item.FindControl("dpStartDate");
                //    RadDatePicker dpEndDate = (RadDatePicker)item.FindControl("dpEndDate");
                //    EnableControls(txtBudget, dpStartDate, dpEndDate, item);
                //}
                pnlGrid.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool checkAtleastOne = false;
                bool checkCredit = true, checkEmpty = true;
                string strAlertMsg = "";

                foreach (GridViewRow item in grdRecipient.Rows)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chkSelect");
                    if (chk.Checked)
                    {
                        string Name = item.Cells[2].Text;
                        TextBox txtBudget = (TextBox)item.FindControl("txtBudget");
                        RadDatePicker dpStartDate = (RadDatePicker)item.FindControl("dpStartDate");
                        RadDatePicker dpEndDate = (RadDatePicker)item.FindControl("dpEndDate");

                        if (lblTotal.Text != "" && Convert.ToDecimal(lblTotal.Text) < 0)
                        {
                            checkCredit = false;
                            strAlertMsg += "Insufficient Credit! ";
                            EnableControls(txtBudget, dpStartDate, dpEndDate, item);
                        }
                        else
                        {
                            checkAtleastOne = true;

                            if (dpStartDate.SelectedDate != null && dpEndDate.SelectedDate != null && txtBudget.Text != "")
                            {
                                if (dpStartDate.SelectedDate < dpEndDate.SelectedDate)
                                {
                                    checkEmpty = false; ;

                                    objBE_SalesBudgetAllocation.TokenNumber = item.Cells[1].Text.ToString();
                                    objBE_SalesBudgetAllocation.Name = item.Cells[2].Text;
                                    objBE_SalesBudgetAllocation.EMailID = grdRecipient.DataKeys[item.RowIndex].Value.ToString();

                                    objBE_SalesBudgetAllocation.AllocationFrom = hdnTokenNumber.Value;
                                    objBE_SalesBudgetAllocation.StartDate = dpStartDate.SelectedDate;
                                    objBE_SalesBudgetAllocation.EndDate = dpEndDate.SelectedDate;
                                    objBE_SalesBudgetAllocation.Budget = Convert.ToDecimal(txtBudget.Text);

                                    if (Session["TokenNumber"] != null)
                                        objBE_SalesBudgetAllocation.CreatedBy = Session["TokenNumber"].ToString();
                                    else // hardcoded
                                        objBE_SalesBudgetAllocation.CreatedBy = "TokenID-1";

                                    objBE_SalesBudgetAllocation.CreatedDate = DateTime.Now;
                                    objBE_SalesBudgetAllocationColl.Add(objBE_SalesBudgetAllocation);
                                    lblTotal.Text = (Convert.ToDecimal(lblTotal.Text) - Convert.ToDecimal(txtBudget.Text)).ToString();
                                    ClearControls(txtBudget, dpStartDate, dpEndDate, chk, item);
                                }
                                else
                                {
                                    strAlertMsg += Name + " : End Date has to greater than start date! \\n";
                                    EnableControls(txtBudget, dpStartDate, dpEndDate, item);
                                    item.Style.Add("class", "selectedoddTd");
                                }
                            }
                            else
                            {
                                strAlertMsg += Name + " : All the fields are mandatory! \\n";
                                string str = "<script language = 'javascript'>alert('" + Name + " : All the fields are mandatory!')</script>";
                                EnableControls(txtBudget, dpStartDate, dpEndDate, item);
                                item.Style.Add("class", "selectedoddTd");
                            }
                        }
                    }

                }

                if (!checkAtleastOne)
                {
                    strAlertMsg += "Please Select atleast one recipient! \\n ";
                }
                else
                {
                    if (checkCredit && (!checkEmpty) && objBE_SalesBudgetAllocationColl.Count > 0)
                    {
                        objBL_SalesBudgetAllocation.SaveData(objBE_SalesBudgetAllocationColl);


                        //=============== Code for sending Mail to the recipient ==============//
                        //string BodyPart = "";
                        //BE_MailDetails objMailDetails = new BE_MailDetails();
                        //Common objCommon = new Common();

                        //StreamReader sr = new StreamReader(Server.MapPath("~/MailDetails/BudgetAllocation.txt"));
                        //StringBuilder sb = new StringBuilder();
                        //BodyPart = sr.ReadToEnd();

                        //foreach (BE_SalesBudgetAllocation objBE_SalesBudgetAllocation in objBE_SalesBudgetAllocationColl)
                        //{
                        //    objMailDetails.FirstName = objBE_SalesBudgetAllocation.Name;
                        //    BodyPart = BodyPart.Replace("<% Amount %>", objBE_SalesBudgetAllocation.Budget.ToString());
                        //}

                        //hdnTotal.Value = lblTotal.Text;
                        //btnGetCEOs_Click(btnGetCEOs, e);
                        //if (strAlertMsg == "")
                        //{
                        //    pnlGrid.Style.Add("display", "none");
                        //}
                    }
                }

                if (strAlertMsg != "")
                {
                    string str = "<script language = 'javascript'>alert('" + strAlertMsg + "');</script>";
                    if (!ClientScript.IsClientScriptBlockRegistered("Script"))
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "Script", str);
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }
        }

        private void EnableControls(TextBox txtBudget, RadDatePicker dpStartDate, RadDatePicker dpEndDate, GridViewRow item)
        {
            txtBudget.Enabled = true;
            dpStartDate.Enabled = true;
            dpEndDate.Enabled = true;
            item.CssClass = "selectedoddTd";
        }

        private void ClearControls(TextBox txtBudget, RadDatePicker dpStartDate, RadDatePicker dpEndDate, CheckBox chk, GridViewRow item)
        {
            txtBudget.Text = "";
            dpStartDate.Clear();
            dpEndDate.Clear();
            chk.Checked = false;
            item.CssClass = item.RowIndex % 2 == 0 ? "" : "row01";
        }

        protected void grdRecipient_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow row = (GridViewRow)e.Row;

                    CheckBox chk = (CheckBox)row.FindControl("chkSelect");
                    TextBox txtBudget = (TextBox)row.FindControl("txtBudget");
                    RadDatePicker dpStartDate = (RadDatePicker)row.FindControl("dpStartDate");
                    RadDatePicker dpEndDate = (RadDatePicker)row.FindControl("dpEndDate");
                    HiddenField hdnBudget = (HiddenField)row.FindControl("hdnBudget");

                    dpStartDate.MinDate = DateTime.Today;
                    dpEndDate.MinDate = DateTime.Today.AddDays(1);

                    chk.Attributes.Add("onclick", "SwitchControls('" + chk.ClientID + "','" + hdnBudget.ClientID + "','" + txtBudget.ClientID + "','" + dpStartDate.ClientID + "','" + dpEndDate.ClientID + "','" + row.RowIndex + "',event);");
                    txtBudget.Attributes.Add("onblur", "txtBudget_onblur('" + hdnBudget.ClientID + "','" + txtBudget.ClientID + "');");
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }

        }

        protected void grdRecipient_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRecipient.PageIndex = e.NewPageIndex;

                btnGetRecipients_Click(btnGetRecipients, e);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }

        }

        #region Exception Handling

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        #endregion


    }
}
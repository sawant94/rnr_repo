﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="HolidayList.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.HolidayList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <script type="text/javascript" >

        function lnkSelect_ClientClick(ID) 
        {
            //alert(ID);
            hdnid = document.getElementById('<%= hdnid.ClientID %>');
            hdnid.value = ID;
        }

        function ValidatePage() {

            var errMsg = '';
            chklLocation = document.getElementById('<%=chklLocation.ClientID %>');
            txtCalendarName = document.getElementById('<%=txtCalendarName.ClientID %>');
            txtHolidayName = document.getElementById('<%=txtHolidayName.ClientID %>');
            dtholidaydate = document.getElementById('<%=dtholidaydate.ClientID %>');

            errMsg += CheckText_Blank(txtCalendarName.value, "Calendar Name");
            errMsg += ValidateName(txtCalendarName.value, "Calendar Name");
            errMsg += CheckText_Blank(txtHolidayName.value, "Holiday Name");
            errMsg += ValidateName(txtHolidayName.value, "Holiday Name");
            errMsg += chkDate_Blank(dtholidaydate.value, "Holiday Date");
            errMsg += RequiredField_CheckBoxList(chklLocation, 'Location');

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }


    
    </script>

    
    <div class="formHold" style="width:100%" >
    <h1> Holiday Management </h1>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <div style="overflow: auto; width: 93%; max-height: 300px;">
                        <asp:GridView ID="grdHolidayList" runat="server" AutoGenerateColumns="False" CellPadding="5" DataKeyNames="ID,LocationCode"
                            BorderColor="#336699" BorderStyle="Solid" BorderWidth="1px" CellSpacing="2" Font-Names="Arial"
                            Font-Size="Small" ForeColor="#003366" Width="100%" OnRowCommand="grdHolidayList_RowCommand"
                            OnRowDeleting="grdHolidayList_RowDeleting">
                            <Columns>
                                <asp:BoundField DataField="CalendarName" HeaderText="Calendar Name" />
                                <asp:BoundField DataField="NoOfLocations" HeaderText="No Of Locations" />
                                <asp:BoundField DataField="HolidayName" HeaderText="Holiday Name" />
                                <asp:BoundField DataField="HolidayDate" HeaderText="Holiday Date" />
                                <asp:BoundField DataField="SatType" HeaderText="SatType" Visible="false" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                    <%--OnClientClick='<%# String.Format("lnkRedeem_ClientClick({0});return false;", Eval("ID")) %>'--%>
                                        <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("ID")  %>' CommandName="Select" 
                                            Width="100px" runat="server" ForeColor="Red" Text="Select"  OnClick = "lnkSelect_Click"
                                            OnClientClick='<%# String.Format("lnkSelect_ClientClick({0});return true;", Eval("ID")) %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" CommandArgument='<%# Eval("ID")  %>' CommandName="Delete"
                                            OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                            Width="100px" runat="server" ForeColor="Red">
                                            Delete</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                                BorderStyle="Solid" Font-Italic="true" />
                            <EmptyDataTemplate>
                                <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display,Please Add Holiday's"></asp:Literal>
                            </EmptyDataTemplate>
                            <HeaderStyle BackColor="#336699" CssClass="textalign" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle BorderColor="#336699" BorderStyle="Solid" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFFFCC" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                
                <td >
                    <asp:Label ID="lblCalName" runat="server" Text="Calendar Name" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span> :
                </td>
                <td><asp:TextBox ID="txtCalendarName" runat="server" TabIndex="1" ></asp:TextBox></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblHolidayName" runat="server" Text="Holiday Name" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span> :
                </td>
                
                <td>
                    <asp:TextBox ID="txtHolidayName" runat="server" Width="141px" MaxLength="100" 
                        TabIndex="2" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 160px; height: 33px;" >
                    <asp:Label ID="lblholidayDate" runat="server" Text="Holiday Date" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span> :
                </td>
                <td style="height: 33px">
                    <telerik:RadDatePicker ID="dtholidaydate" runat="server" Culture="en-US" MinDate="1950-01-01"
                        TabIndex="3">
                        <Calendar ID="Calendar1" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DateInput ID="DateInput1" runat="server" DisplayDateFormat="dd-MMM-yyyy" DateFormat="dd-MMM-yyyy"
                            TabIndex="3">
                        </DateInput>
                        <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="3"></DatePopupButton>
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
               
                <td style="width: 160px; height: 33px;" >
                    <asp:Label ID="lblLocations" runat="server" Text="Locations" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span> :
                </td>
                <td>
                     <%--<a id="addNewAgentGroup" href="#" style="color: #333" onclick="Show(this)" >Select</a>--%>
                    <%--++++++++++++++++++++++++++++++++++++++++++++++++++++++++--%>
                    <%--<div id="Div1" runat="server" class="addChain" style="height:100px; width: 190px; position: absolute; top:275px; left:443px; display:none; z-index:9999; background:#F7F7F7; border:1px solid #CCC; padding:10px; overflow:auto; " >--%>
                    <div id="dvAddChain" runat="server" style="height:100px; width: 190px; top:275px; left:443px; display:block; overflow:auto; border:1px solid #CCC; " >
                        <asp:Label ID="lbErrorPop" Text="" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        <table>
                            <tr>
                                <td>
                                    <asp:CheckBoxList ID="chklLocation" runat="server" Font-Bold="false" 
                                        TabIndex="4" >
                                        
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%--++++++++++++++++++++++++++++++++++++++++++++++++++++++++--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:HiddenField ID="hdnid" runat="server" />
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblsatWeekholiday" runat="server" Text="Saturday Week Holiday" ></asp:Label>&nbsp;<span
                        style="color: Red">*</span> :
                </td>
                <td>
                    <asp:RadioButtonList ID="rdbholidaytype" runat="server" TabIndex="5">
                        <asp:ListItem Selected="True" Value="0">ALL</asp:ListItem>
                        <asp:ListItem Value="1">1 &amp; 3</asp:ListItem>
                        <asp:ListItem Value="2">2 &amp; 4</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td align="left">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnR" OnClick="btnSave_Click"
                        OnClientClick="return ValidatePage() " TabIndex="3" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnR" OnClick="btnCancel_Click"
                        TabIndex="4" />
                </td>
            </tr>
        </table>

      <%--  <table border="0" width="100%" cellspacing="0" cellpadding="0" class="container">
            <tr>
                <td colspan="3">
                    <p>
                        Holiday Management
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblLocation" runat="server" Text="Location" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td style="width: 1px">
                    :
                </td>
                <td >
                    <asp:DropDownList ID="ddlLocation" runat="server" Width="190px" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" onclick="Show(this)"
                        AutoPostBack="True" Visible="false" >
                    </asp:DropDownList>
                    <asp:TextBox ID="txt" runat="server"  onclick="Show(this)" ></asp:TextBox>
                    <asp:LinkButton ID="lnkBindGrid" runat="server" Text="DONE" OnClick="lnkBindGrid_Click"  ></asp:LinkButton>
                   
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:HiddenField ID="hdnid" runat="server" />
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblsatWeekholiday" runat="server" Text="Saturday Week Holiday" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td style="width: 1px" >
                    :
                </td>
                <td>
                    <asp:RadioButtonList ID="rdbholidaytype" runat="server" TabIndex="1">
                        <asp:ListItem Selected="True" Value="0">ALL</asp:ListItem>
                        <asp:ListItem Value="1">1 &amp; 3</asp:ListItem>
                        <asp:ListItem Value="2">2 &amp; 4</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="overflow: auto; width: 93%; max-height: 300px;">
                        <asp:GridView ID="grdHolidayList" runat="server" AutoGenerateColumns="False" CellPadding="5"
                            BorderColor="#336699" BorderStyle="Solid" BorderWidth="1px" CellSpacing="2" Font-Names="Arial"
                            Font-Size="Small" ForeColor="#003366" Width="576px" OnRowCommand="grdHolidayList_RowCommand"
                            OnRowDeleting="grdHolidayList_RowDeleting">
                            <Columns>
                                <asp:BoundField DataField="HolidayName" HeaderText="Holiday Name" />
                                <asp:BoundField DataField="HolidayDate" HeaderText="Holiday Date" />
                                <asp:BoundField DataField="SatType" HeaderText="SatType" Visible="false" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("ID")  %>' CommandName="Select"
                                            Width="100px" runat="server" ForeColor="Red">
                                            Select</asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" CommandArgument='<%# Eval("ID")  %>' CommandName="Delete"
                                            OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                            Width="100px" runat="server" ForeColor="Red">
                                            Delete</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                                BorderStyle="Solid" Font-Italic="true" />
                            <EmptyDataTemplate>
                                <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display,Please Add Holiday's"></asp:Literal>
                            </EmptyDataTemplate>
                            <HeaderStyle BackColor="#336699" CssClass="textalign" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle BorderColor="#336699" BorderStyle="Solid" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFFFCC" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
        <table border="0" width="100%" cellspacing="0" cellpadding="0" class="container"
            id="tblholidayedit" runat="server" visible="false">
            <tr>
                <td >
                    <asp:Label ID="lblHolidayName" runat="server" Text="Holiday Name" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td style="width: 1px" >
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtHolidayName" runat="server" Width="141px" MaxLength="100" TabIndex="1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 160px; height: 33px;" >
                    <asp:Label ID="lblholidayDate" runat="server" Text="Holiday Date" CssClass="labWidth"></asp:Label>&nbsp;<span
                        style="color: Red">*</span>
                </td>
                <td style="width: 1px; height: 33px;" >
                    :
                </td>
                <td style="height: 33px">
                    
                    <telerik:RadDatePicker ID="dtholidaydate" runat="server" Culture="en-US" MinDate="1950-01-01"
                        TabIndex="2">
                        <Calendar ID="Calendar1" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DateInput ID="DateInput1" runat="server" DisplayDateFormat="dd-MMM-yyyy" DateFormat="dd-MMM-yyyy"
                            TabIndex="4">
                        </DateInput>
                        <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="4"></DatePopupButton>
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td align="left">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnR" OnClick="btnSave_Click"
                        OnClientClick="return ValidatePage() " TabIndex="3" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnR" OnClick="btnCancel_Click"
                        TabIndex="4" />
                </td>
            </tr>
        </table>--%>
    </div>
</asp:Content>

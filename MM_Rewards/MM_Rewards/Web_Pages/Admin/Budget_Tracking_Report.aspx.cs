﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MM.DAL;
using BE_Rewards;
using BL_Rewards;
using BL_Rewards.BL_Common;
using System.IO;
namespace MM_Rewards.Web_Pages.Admin
{
    //-----------------------------------------------------------------------------------------------------
    public partial class Budget_Tracking_Report : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BE_Rewards.BE_Admin.BE_BudgetTrackingUtilColl objBE_BudgetTrackingUtilColl = new BE_Rewards.BE_Admin.BE_BudgetTrackingUtilColl();
        BL_Rewards.Admin.BL_BudgetTracking objBL_BudgetTracking = new BL_Rewards.Admin.BL_BudgetTracking();
        int _SaveStatus = 0;
        object frmdate = Convert.ToInt32(System.DateTime.Now.AddYears(-1).Year.ToString());
        object todate = Convert.ToInt32(System.DateTime.Now.Year.ToString());
        //-----------------------------------------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    FillDropdown();
                    ddlfrmdate.Items.Insert(0, "select");
                    ddltodate.Items.Insert(0, "select");
                    for (int i = 1; i <= 1; i++)
                    {
                        ddlfrmdate.Items.Insert(i, (Convert.ToInt32(frmdate)).ToString());
                    }
                    for (int j = 1; j <= 1; j++)
                    {
                        ddltodate.Items.Insert(j, (Convert.ToInt32(todate)).ToString());
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "BudgetTracking.aspx - Page_Load");
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected void btnBudgetTracking_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlfrmdate.SelectedIndex != 0 && ddltodate.SelectedIndex != 0 && ddlAwardCat.SelectedIndex != 0)
                {
                    BE_Rewards.BE_Admin.BE_BudgetTrackingUtil objBE_BudgetTrackingUtil = new BE_Rewards.BE_Admin.BE_BudgetTrackingUtil();
                    objBE_BudgetTrackingUtil.Percentage = hdnpercentage.Value;
                    if (hdnid.Value == "")
                    {
                        objBE_BudgetTrackingUtil.ID = 0;
                        objBE_BudgetTrackingUtil.TokenNumber = Session["TokenNumber"].ToString();
                        objBE_BudgetTrackingUtil.AwardName = ddlAwardCat.SelectedValue;
                        objBE_BudgetTrackingUtil.FromYear = ddlfrmdate.SelectedItem.Text.ToString();
                        objBE_BudgetTrackingUtil.ToYear = ddltodate.SelectedItem.Text.ToString();
                        objBE_BudgetTrackingUtil.totalAmount = Convert.ToDecimal(txtTotalBudget.Text);
                        objBE_BudgetTrackingUtil.Budgetforeachaward = Convert.ToDecimal(txtBudget.Text);
                        objBE_BudgetTrackingUtil.Percentage = hdnpercentage.Value;
                    }
                    else
                    {
                        hdnid.Value = "";
                        objBE_BudgetTrackingUtil.ID = Convert.ToInt16(hdnid.Value.ToString());
                        objBE_BudgetTrackingUtil.TokenNumber = Session["TokenNumber"].ToString();
                        objBE_BudgetTrackingUtil.AwardName = ddlAwardCat.SelectedValue;
                        objBE_BudgetTrackingUtil.FromYear = ddlfrmdate.SelectedItem.Text.ToString();
                        objBE_BudgetTrackingUtil.ToYear = ddltodate.SelectedItem.Text.ToString();
                        objBE_BudgetTrackingUtil.totalAmount = Convert.ToDecimal(txtTotalBudget.Text);
                        objBE_BudgetTrackingUtil.Budgetforeachaward = Convert.ToDecimal(txtBudget.Text);
                        objBE_BudgetTrackingUtil.Percentage = hdnpercentage.Value;
                    }
                    _SaveStatus = objBL_BudgetTracking.SaveData(objBE_BudgetTrackingUtil, out _SaveStatus);
                    if (_SaveStatus == 0)
                    {
                        Session["objBE_BudgetTrackingUtilColl"] = null;
                        ClearControls();
                        ThrowAlertMessage(MM_Messages.DataSaved);
                        bindgrid();
                    }
                    else
                    {
                        ThrowAlertMessage("Please Fill All The Conditions.");
                    }
                }
                else
                {
                    ThrowAlertMessage("Please Fill All The Conditions.");
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "BudgetTrackingReport.aspx - btnsaveAwardname_Click");
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected void grdAwardList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "BudgetTrackingReport.aspx");
            }
        }
        //-----------------------------------------------------------------------------------------------------
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }
        //-----------------------------------------------------------------------------------------------------
        private void bindgrid()
        {
            grdAwardList.SelectedIndex = -1;
            grdAwardList.DataBind();
            grdAwardList.Columns[2].Visible = false;
            grdAwardList.Columns[4].Visible = false;
        }
        //-----------------------------------------------------------------------------------------------------
        public void FillDropdown()
        {
            try
            {
                objBE_BudgetTrackingUtilColl = objBL_BudgetTracking.GetBudgetAwardType();
                ddlAwardCat.DataSource = objBE_BudgetTrackingUtilColl;
                ddlAwardCat.DataTextField = "AwardName";
                ddlAwardCat.DataValueField = "ID";
                ddlAwardCat.DataBind();
                ddlAwardCat.Items.Insert(0, "Select");
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
        //-----------------------------------------------------------------------------------------------------
        private void ClearControls()
        {
            ddlfrmdate.SelectedIndex = 0;
            ddltodate.SelectedIndex = 0;
            txtBudget.Text = "";
            txtPercentage.Text = "";
            hdnAwardTypeid.Value = "";
            hdnDivisionId.Value = "";
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_Rewards.Admin;
using BE_Rewards.BE_Admin;
using BL_Rewards.BL_Common;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class CalendarName : System.Web.UI.Page
    {
        private int CalendarID;
        private BL_Holidays objBL_Holidays;
        private BE_Holidays objBE_Holidays, objBE_H;
        private BE_HolidayManagement objBE_HolidayManagement;
        private BE_HolidayManagementColl objBE_HolidayManagementColl;
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                dpHolidayDate.MinDate = DateTime.Today.Date;

                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                if (!IsPostBack)
                {
                    ClearSessions();
                    AssignDataSource();
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        /// <summary>
        /// All the get methods used on the page
        /// </summary>
        /// <returns></returns>
        #region DataSources

        private BE_HolidayManagementColl DataSource_GetAllLocations()
        {
            try
            {
                if (Session["BE_HolidayManagementColl_Location"] != null)
                {
                    objBE_HolidayManagementColl = (BE_HolidayManagementColl)Session["BE_HolidayManagementColl_Location"];
                }
                else
                {
                    objBL_Holidays = new BL_Holidays();
                    objBE_HolidayManagementColl = objBL_Holidays.Get_AllLocations();
                    Session["BE_HolidayManagementColl_Location"] = objBE_HolidayManagementColl;
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

            return objBE_HolidayManagementColl;
        }

        private BE_HolidayManagementColl DataSource_GetAllCalendars()
        {
            try
            {
                objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                objBL_Holidays = new BL_Holidays();

                objBE_HolidayManagementColl = objBL_Holidays.Get_Mas_Calendar();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
            return objBE_HolidayManagementColl;
        }

        private BE_HolidayManagement DataSource_Get_RecordsByCalendarID(int CalendarID)
        {
            objBE_HolidayManagement = new BE_HolidayManagement();

            try
            {
                BL_Holidays objBL_Holidays = new BL_Holidays();

                if (Session["BE_HolidayManagement"] != null)
                {
                    objBE_HolidayManagement = (BE_HolidayManagement)Session["BE_HolidayManagement"];
                }
                else
                {
                    objBE_HolidayManagement = objBL_Holidays.Get_RecordsByCalendarID(CalendarID);
                    Session["BE_HolidayManagement"] = objBE_HolidayManagement;
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
            return objBE_HolidayManagement;
        }

        #endregion

        #region Assign DataSource at Page Load

        private void AssignDataSource()
        {
            try
            {
                objBL_Holidays = new BL_Holidays();
                objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                objBE_HolidayManagement = new BE_HolidayManagement();

                drpCalender.DataSource = DataSource_GetAllCalendars();
                drpCalender.DataBind();
                drpCalender.Items.Insert(0, new ListItem("---Select---", ""));

                objBE_HolidayManagementColl = DataSource_GetAllLocations();
                chklLocation.DataSource = objBE_HolidayManagementColl;
                chklLocation.DataBind();
                disableLocation(objBE_HolidayManagementColl);

                BindGrid(0, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        #endregion

        #region Calendar

        protected void drpCalender_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearSessions();
                txtHolidayName.Text = "";
                dpHolidayDate.Clear();
                hdnHolidayID.Value = "";

                objBE_HolidayManagement = new BE_HolidayManagement();
                if (drpCalender.SelectedValue != "")
                {
                    CalendarID = Convert.ToInt32(drpCalender.SelectedValue);
                    objBE_HolidayManagement = DataSource_Get_RecordsByCalendarID(CalendarID);
                }

                hdnHolidayID.Value = "";
                disableLocation(DataSource_GetAllLocations());
                FillControlsWithSelectedItem(objBE_HolidayManagement);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        private void FillControlsWithSelectedItem(BE_HolidayManagement objBE_HolidayManagement)
        {
            try
            {
                ////======== Section 0 =============//

                //drpCalender.DataSource = objBE_HolidayManagementColl;
                //drpCalender.DataBind();
                //drpCalender.Items.Insert(0, new ListItem("---Select---", ""));

                //======== Section 1 =============//

                rblWeekend.SelectedValue = objBE_HolidayManagement.Sattype.ToString();

                //======== Section 2 =============//

                grdHolidays.DataSource = objBE_HolidayManagement.HolidayColl;
                grdHolidays.DataBind();

                txtCalendarName.Text = Server.HtmlEncode(drpCalender.SelectedValue == "" ? "" : drpCalender.SelectedItem.Text);

                //======== Section 3 =============//

                int checkedCount = 0; int newlyEnabled = 0;

                foreach (string str in objBE_HolidayManagement.LocationColl)
                {
                    ListItem item = (ListItem)chklLocation.Items.FindByValue(str);
                    if (item != null)
                    {
                        if (item.Enabled == false)
                        {
                            item.Enabled = true;
                            item.Attributes.Add("onclick", "chk_Click(this)");
                            if (!chkAll.Checked)
                                chkAll.Checked = true;
                            if (chkAll.Enabled == false)
                                chkAll.Enabled = true;
                            newlyEnabled++;
                        }
                        item.Selected = true;
                        checkedCount++;
                    }
                }

                hdnNoOfCheckedNodes.Value = checkedCount.ToString();
                hdnTotalLocations.Value = (Convert.ToInt32(hdnTotalLocations.Value) + newlyEnabled).ToString();

                if (Convert.ToInt32(hdnTotalLocations.Value) == Convert.ToInt32(hdnNoOfCheckedNodes.Value) && Convert.ToInt32(hdnTotalLocations.Value) != 0)
                    chkAll.Checked = true;
                else
                    chkAll.Checked = false;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        protected void lnkAddNewCalendar_Click(object sender, EventArgs e)
        {
            try
            {
                divAddCalendar.Style.Add("display", "block");
                divSelectCalender.Style.Add("display", "none");
                drpCalender.SelectedIndex = -1;


                ClearSessions();
                ClearControls();
                btnAddHoliday.Text = "Add";
                BindGrid(0, true);
                disableLocation(DataSource_GetAllLocations());
                //FillControls(objBE_HolidayManagement);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

        }

        protected void lnkSelectCalendar_Click(object sender, EventArgs e)
        {
            try
            {
                divAddCalendar.Style.Add("display", "none");
                divSelectCalender.Style.Add("display", "block");
                btnAddHoliday.Text = "Add";
                ClearSessions();
                ClearControls();
                BindGrid(0, true);
                disableLocation(DataSource_GetAllLocations());
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        #endregion

        #region Holidays

        protected void btnAddHoliday_Click(object sender, EventArgs e)
        {
            try
            {
                //string strHname = txtHolidayName.Text;
                //string strHdate = dpHolidayDate.SelectedDate.ToString();

                bool duplicateRecord = false;
                if (Session["BE_HolidayManagement"] != null)
                {
                    //objBE_HolidayManagement.HolidayColl = (List<BE_Holidays>)Session["BE_HolidayManagement"];
                    objBE_HolidayManagement = (BE_HolidayManagement)Session["BE_HolidayManagement"];
                }
                else
                {
                    objBE_HolidayManagement = new BE_HolidayManagement();
                    Session["BE_HolidayManagement"] = objBE_HolidayManagement;
                }

                objBE_Holidays = new BE_Holidays();
                objBE_H = new BE_Holidays();
                if (hdnHolidayID.Value == "")
                {
                    objBE_H = objBE_HolidayManagement.HolidayColl.Find(delegate(BE_Holidays H) { return H.HolidayDate == Convert.ToDateTime(dpHolidayDate.SelectedDate); });

                    if (objBE_H == null)
                    {
                        objBE_Holidays.HolidayName = txtHolidayName.Text;
                        objBE_Holidays.HolidayDate = Convert.ToDateTime(dpHolidayDate.SelectedDate);

                        objBE_HolidayManagement.HolidayColl.Add(objBE_Holidays);
                        //Session["BE_HolidayManagement"] = objBE_HolidayManagement.HolidayColl;
                        Session["BE_HolidayManagement"] = objBE_HolidayManagement;
                    }
                    else
                    {
                        duplicateRecord = true;
                        ThrowAlertMessage("Holiday for the selected date already exists in this calender!");
                    }
                }
                else
                {
                    objBE_H = objBE_HolidayManagement.HolidayColl.Find(delegate(BE_Holidays H) { return H.HolidayDate == Convert.ToDateTime(dpHolidayDate.SelectedDate) && H.ID != Convert.ToInt32(hdnHolidayID.Value); });
                    if (objBE_H == null)
                    {
                        objBE_Holidays = objBE_HolidayManagement.HolidayColl.Find(delegate(BE_Holidays H) { return H.ID == Convert.ToInt32(hdnHolidayID.Value); });
                        objBE_Holidays.HolidayName = txtHolidayName.Text;
                        objBE_Holidays.HolidayDate = Convert.ToDateTime(dpHolidayDate.SelectedDate);
                    }
                    else
                    {
                        duplicateRecord = true;
                        ThrowAlertMessage("Holiday for the selected date already exists in this calender!");
                    }

                }

                if (!duplicateRecord)
                {
                    txtHolidayName.Text = "";
                    dpHolidayDate.Clear();
                    btnAddHoliday.Text = "Add";
                    grdHolidays.DataSource = objBE_HolidayManagement.HolidayColl;
                    grdHolidays.DataBind();
                }

                foreach (ListItem item in chklLocation.Items)
                {
                    if (item.Enabled == true)
                    {
                        item.Attributes.Add("onclick", "chk_Click(this)");
                    }
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(sender as LinkButton).NamingContainer;
                int ID = Convert.ToInt32(grdHolidays.DataKeys[row.RowIndex][0]);
                hdnHolidayID.Value = ID.ToString();
                txtHolidayName.Text = row.Cells[0].Text;
                string HolidayDate = row.Cells[1].Text;
                //dpHolidayDate.SelectedDate = Convert.ToDateTime(HolidayDate.Substring(3, 2) + '/' + HolidayDate.Substring(0, 2) + '/' + HolidayDate.Substring(6, 4));
                dpHolidayDate.SelectedDate = Convert.ToDateTime(HolidayDate);

                btnAddHoliday.Text = "Update";
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                txtHolidayName.Text = "";
                dpHolidayDate.Clear();
                hdnHolidayID.Value = "";

                objBE_Holidays = new BE_Holidays();
                GridViewRow row = (GridViewRow)(sender as LinkButton).NamingContainer;
                int ID = Convert.ToInt32(grdHolidays.DataKeys[row.RowIndex][0]);

                //if (Session["BE_HolidayManagement"] != null)
                //{
                //    //objBE_HolidayManagement.HolidayColl = (List<BE_Holidays>)Session["BE_HolidayManagement"];
                //    objBE_HolidayManagement = (BE_HolidayManagement)Session["BE_HolidayManagement"];
                //}
                //else
                //{
                //    objBE_HolidayManagement = new BE_HolidayManagement();
                //    Session["BE_HolidayManagement"] = objBE_HolidayManagement;
                //}

                objBE_HolidayManagement = DataSource_Get_RecordsByCalendarID(0);

                objBE_Holidays = objBE_HolidayManagement.HolidayColl.Find(delegate(BE_Holidays H) { return H.ID == ID; });
                // objBE_HolidayManagement.HolidayColl.Find(delegate(BE_Holidays H) { return H.ID == Convert.ToInt32(hdnHolidayID.Value); });

                objBE_HolidayManagement.HolidayColl.Remove(objBE_Holidays);

                Session["BE_HolidayManagement"] = objBE_HolidayManagement;

                BindGrid(0);

                //grdHolidays.DataSource = objBE_HolidayManagement.HolidayColl;
                //grdHolidays.DataBind();

                foreach (ListItem item in chklLocation.Items)
                {
                    if (item.Enabled == true)
                    {
                        item.Attributes.Add("onclick", "chk_Click(this)");
                    }
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        private void BindGrid(int CalendarID, bool empty = false)
        {
            try
            {
                if (empty)
                {
                    objBE_HolidayManagement = new BE_HolidayManagement();
                }
                else
                {
                    if (Session["BE_HolidayManagement"] != null)
                    {
                        objBE_HolidayManagement = (BE_HolidayManagement)Session["BE_HolidayManagement"];
                    }
                    else
                    {
                        objBE_HolidayManagement = DataSource_Get_RecordsByCalendarID(CalendarID);
                    }
                }

                grdHolidays.DataSource = objBE_HolidayManagement.HolidayColl;
                grdHolidays.DataBind();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

        }

        #endregion

        #region Save and Cancel Clicks
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objBE_HolidayManagement = new BE_HolidayManagement();
                objBE_HolidayManagement.CalendarID = drpCalender.SelectedValue != "" ? Convert.ToInt32(drpCalender.SelectedValue) : 0;
                objBE_HolidayManagement.Calendarname = Server.HtmlEncode(txtCalendarName.Text);
                objBE_HolidayManagement.Sattype = Convert.ToInt32(rblWeekend.SelectedValue);
                objBE_HolidayManagement.Createdby = Session["TokenNumber"].ToString();
                objBE_HolidayManagement.Modifiedby = null;

                if (Session["BE_HolidayManagement"] != null)
                {
                    //objBE_HolidayManagement.HolidayColl = (List<BE_Holidays>)Session["BE_HolidayManagement"];
                    BE_HolidayManagement objBE_HolidayManagementS = (BE_HolidayManagement)Session["BE_HolidayManagement"];

                    foreach (BE_Holidays objBE_Holidays in objBE_HolidayManagementS.HolidayColl)
                    {
                        objBE_HolidayManagement.HolidayColl.Add(objBE_Holidays);
                    }
                }

                foreach (ListItem item in chklLocation.Items)
                {
                    if (item.Selected)
                    {
                        objBE_HolidayManagement.LocationColl.Add(item.Value);
                    }
                }
                objBL_Holidays = new BL_Holidays();
                int status = objBL_Holidays.InsertUpdate_Mas_Calendar(objBE_HolidayManagement);
                if (status > 0)
                {
                   ThrowAlertMessage(MM_Messages.DataSaved);
                }
                else
                {
                    ThrowAlertMessage(MM_Messages.DatabaseError);
                }
                ClearSessions();
                ClearControls();
                AssignDataSource();
                // FillControls( new BE_HolidayManagement());

                divAddCalendar.Style.Add("display", "none");
                divSelectCalender.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                divAddCalendar.Style.Add("display", "none");
                divSelectCalender.Style.Add("display", "block");

                ClearSessions();
                ClearControls();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }


        #endregion

        #region Cleanup

        private void ClearSessions()
        {
            Session["BE_HolidayManagement"] = null;
            Session["BE_HolidayManagementColl_Location"] = null;
        }

        private void ClearControls()
        {
            try
            {
                // Clear Calendar region //
                rblWeekend.SelectedValue = "0";
                txtCalendarName.Text = "";
                drpCalender.SelectedItem.Selected = false;

                // Clear Holiday region //
                hdnHolidayID.Value = "";
                btnAddHoliday.Text = "Add";
                txtHolidayName.Text = "";
                dpHolidayDate.Clear();
                BindGrid(0, true);

                // Clear Location Mappings //
                disableLocation(DataSource_GetAllLocations());
                if (Convert.ToInt32(hdnTotalLocations.Value) == Convert.ToInt32(hdnNoOfCheckedNodes.Value) && Convert.ToInt32(hdnTotalLocations.Value) != 0)
                    chkAll.Checked = true;
                else
                    chkAll.Checked = false;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        private void disableLocation(BE_HolidayManagementColl objBE_HolidayManagementColl)
        {
            try
            {
                chklLocation.ClearSelection();
                int count = chklLocation.Items.Count;

                foreach (BE_HolidayManagement objBE_HolidayManagement in objBE_HolidayManagementColl)
                {
                    if (objBE_HolidayManagement.CalendarID != null)
                    {
                        ListItem item = (ListItem)chklLocation.Items.FindByValue(objBE_HolidayManagement.Locationcode);
                        if (item != null)
                        {
                            item.Enabled = false;
                            count--;
                        }
                    }
                }

                foreach (ListItem item in chklLocation.Items)
                {
                    if (item.Enabled == true)
                    {
                        item.Attributes.Add("onclick", "chk_Click(this)");
                    }
                }

                hdnTotalLocations.Value = count.ToString();

                if (count == 0)
                {
                    chkAll.Enabled = false;
                    chkAll.Checked = false;
                }
                else
                    chkAll.Enabled = true;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        #endregion

        #region Exception Handling

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        #endregion


    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="CalendarName.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.CalendarName" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

     <script src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("[id$=dateInput_text]").attr('readonly', 'readonly');
            //$("[id$=dtpEnddate_dateInput_text]").attr('readonly', 'readonly');
        });
    </script>


    <script type="text/javascript">

        function lnkSelectCalendar_ClientClick(sender) {
            divSelectCalender = document.getElementById('<%=divSelectCalender.ClientID %>');
            divAddCalendar = document.getElementById('<%=divAddCalendar.ClientID %>');

            drpCalender = document.getElementById('<%=drpCalender.ClientID %>');
            txtCalendarName = document.getElementById('<%=txtCalendarName.ClientID %>');

            drpCalender.selectedIndex = 0;
            divSelectCalender.style.display = "block";

            txtCalendarName.value = "";
            divAddCalendar.style.display = "none";
        }

        function btnAddHoliday_ClientClick() {
            var errMsg = '';
            txtHolidayName = document.getElementById('<%=txtHolidayName.ClientID %>');
            dpHolidayDate = document.getElementById('<%=dpHolidayDate.ClientID %>');

            errMsg += CheckText_Blank(txtHolidayName.value, 'Holiday Name');
            errMsg += ValidateName(txtHolidayName.value, 'Holiday Name');
            errMsg += chkDate_Blank(dpHolidayDate.value, 'Holiday Date');


            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else
                return true;
        }

        function btnSave_ClientClick() {

            var r = confirm("Do you want to submit record ?");
            //alert(r);
            if (r != true) {
                return false;
            }


            var errMsg = '';
            txtCalendarName = document.getElementById('<%=txtCalendarName.ClientID %>');
            chklLocation = document.getElementById('<%=chklLocation.ClientID %>');

            var grdHolidays = document.getElementById('<%=grdHolidays.ClientID %>');
            var rCount = grdHolidays.rows.length;

            errMsg += CheckText_Blank(txtCalendarName.value, 'Calendar Name');
            errMsg += ValidateName(txtCalendarName.value, 'Calendar Name');
            errMsg += RequiredField_CheckBoxList(chklLocation, 'Location');

            if (rCount < 2) {
                errMsg += "Atleast one holiday is required!";
            }

            drpCalender = document.getElementById("<%= drpCalender.ClientID %>");

            if (txtCalendarName.value != "" && drpCalender.selectedIndex < 1) 
            {
                for (var vLoop = 0; vLoop < drpCalender.options.length; vLoop++) {
                    if (drpCalender.options[vLoop].text.toLowerCase() == txtCalendarName.value.toLowerCase()) {
                        errMsg += "\nCalendar Name already exists.Please select another Calendar Name!";
                        vLoop = drpCalender.options.length;
                    }
                }
            }

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else
                return true;
        }


        function CheckUncheckAllListBox() {
            chkAll = document.getElementById('<%=chkAll.ClientID %>');
            chklLocation = document.getElementById('<%=chklLocation.ClientID %>');

            hdnNoOfCheckedNodes = document.getElementById('<%=hdnNoOfCheckedNodes.ClientID %>');
            hdnTotalLocations = document.getElementById('<%=hdnTotalLocations.ClientID %>');

            var checkbox = chklLocation.getElementsByTagName("input");
            var counter = 0;
            for (var i = 0; i < checkbox.length; i++) {
                if (chkAll.checked) {
                    if (checkbox[i].disabled == false) {
                        checkbox[i].checked = true;
                    }
                    hdnNoOfCheckedNodes.value = hdnTotalLocations.value;
                }
                else {
                    if (checkbox[i].disabled == false) {
                        checkbox[i].checked = false;
                    }
                    hdnNoOfCheckedNodes.value = "0";
                }

            }
            //  alert(hdnTotalLocations.value + " || " + hdnNoOfCheckedNodes.value);
        }


        function chk_Click(sender) {
            //alert(sender.checked);
            chk = sender;
            chkAll = document.getElementById('<%=chkAll.ClientID %>');
            chklLocation = document.getElementById('<%=chklLocation.ClientID %>');

            hdnNoOfCheckedNodes = document.getElementById('<%=hdnNoOfCheckedNodes.ClientID %>');
            hdnTotalLocations = document.getElementById('<%=hdnTotalLocations.ClientID %>');

            // alert(hdnTotalLocations.value + " || " + hdnNoOfCheckedNodes.value);

            if (sender.checked) {
                hdnNoOfCheckedNodes.value++; // = hdnNoOfCheckedNodes.value + 1;
            }
            else {
                hdnNoOfCheckedNodes.value--; // = hdnNoOfCheckedNodes.value - 1;
            }

            // alert(hdnTotalLocations.value + " || " + hdnNoOfCheckedNodes.value);
            if (hdnTotalLocations.value == hdnNoOfCheckedNodes.value && hdnNoOfCheckedNodes.value != "0") {
                chkAll.checked = true;
            }
            else { chkAll.checked = false; }
        }

        function btnCancel_Holidays_ClientClick() {
            hdnHolidayID = document.getElementById('<%=hdnHolidayID.ClientID %>');
            hdnHolidayID.value = "";

            txtHolidayName = document.getElementById('<%=txtHolidayName.ClientID %>');
            dpHolidayDate = document.getElementById('<%=dpHolidayDate.ClientID %>');

            dpDate = $find('<%=dpHolidayDate.ClientID %>');
            dpDate.clear();
            btnAddHoliday = document.getElementById('<%=btnAddHoliday.ClientID %>');

            txtHolidayName.value = "";
            btnAddHoliday.value = "Add";

            grdHolidays = document.getElementById('<%= grdHolidays.ClientID %>');
            UnselectRow(grdHolidays.rows);

        }

        function txtCalendarName_onBlur() {
            var errMsg = '';
            txtCalendarName = document.getElementById('<%=txtCalendarName.ClientID %>');
            errMsg += ValidateName(txtCalendarName.value, 'Calendar Name');
            if (errMsg != '') 
            {
                alert(errMsg);
            }
        }

    </script>
    <div class="form01">
    <h1> Holiday Management </h1>
        <table>
            <tr>
                <td class="lblName">Calendar Name<span style="color:Red">*</span> : 
                </td>
                <td>
                    <div id="divSelectCalender" runat="server">
                        <asp:DropDownList ID="drpCalender" AutoPostBack="true" OnSelectedIndexChanged="drpCalender_SelectedIndexChanged"
                            runat="server" DataValueField="CalendarID" DataTextField="CalendarName" 
                            TabIndex="1">
                        </asp:DropDownList>
                        
                        <asp:LinkButton ID="lnkAddNewCalendar" CssClass="linkBtn" runat="server" OnClick="lnkAddNewCalendar_Click"
                            Text="Add New Calendar" TabIndex="2"></asp:LinkButton>
                    </div>
                    <div id="divAddCalendar" runat="server" style="display: none;">
                        <asp:TextBox ID="txtCalendarName" runat="server" onblur="txtCalendarName_onBlur()"  TabIndex="3"></asp:TextBox>
                        
                        <asp:LinkButton ID="lnkSelectCalendar" runat="server" CssClass="linkBtn"  OnClientClick="lnkSelectCalendar_ClientClick(this);return true;" OnClick="lnkSelectCalendar_Click"
                            Text="Select Calendar"></asp:LinkButton>
                    </div>

                    
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Saturday Holidays :
                </td>
                <td >
                    <asp:RadioButtonList ID="rblWeekend" runat="server" 
                        CssClass="radioBtn" TabIndex="4" RepeatLayout="UnorderedList">
                        <asp:ListItem  Value="0" Text="All" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="1" Text="2nd and 4th"></asp:ListItem>
                        <asp:ListItem Value="2" Text="1st and 3rd"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" >
                        
                     <div class="grid01">
                        <asp:GridView ID="grdHolidays" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    DataKeyNames="ID" AlternatingRowStyle-CssClass="row01" >
                                   
                                    <EmptyDataRowStyle ForeColor="Black" Height="5" BorderColor="Black" BorderWidth="1"
                                        BorderStyle="Solid" Font-Italic="true" />
                                    <EmptyDataTemplate>
                                        <asp:Literal ID="ltrlNorecords"  runat="server" Text="No holidays available to display" ></asp:Literal>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle CssClass="selectedoddTd" />
                                    <Columns>
                                        <asp:BoundField DataField="HolidayName" HeaderText="Holiday Name" />
                                        <asp:BoundField DataField="HolidayDate" HeaderText="Holiday Date" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" CssClass="editLink" runat="server" CommandName="select" Text="Edit" OnClick="lnkEdit_Click" TabIndex="5"></asp:LinkButton>
                                                &nbsp;
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_Click"  class="delLink"
                                                    OnClientClick="return confirm('Are you sure! You want to delete this calendar?')" TabIndex="6"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                               
<PagerStyle CssClass="dataPager" />
                        <SelectedRowStyle CssClass="selectedoddTd" />
                                </asp:GridView>
                     </div>

                     <div class="form01">
                      <table>
                      
                        <tr>
                            <td class="lblName">
                                <asp:HiddenField ID="hdnHolidayID" runat="server" />
                                Holiday Name :<span style="color:Red">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtHolidayName" runat="server" TabIndex="7" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Holiday Date :<span style="color:Red">*</span>
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="dpHolidayDate" runat="server"
                                    DateInput-DateFormat="dd/MM/yyyy" TabIndex="4">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>

<DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="8"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="4"></DatePopupButton>
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="1">
                                <div class="btnRed">
                                    <asp:Button ID="btnAddHoliday" runat="server" Text="Add" OnClientClick="return btnAddHoliday_ClientClick()"
                                        CssClass="btnLeft" OnClick="btnAddHoliday_Click" TabIndex="9" />
                                 </div>
                                <div class="btnRed">
                                    <asp:Button ID="btnCancel_Holidays" runat="server" Text="Cancel" OnClientClick="btnCancel_Holidays_ClientClick();return false;"
                                    CssClass="btnLeft" TabIndex="10" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    </div>
                </td>
            </tr>
           
            <tr>
                <td >
                    <b>Locations :<span style="color:Red">*</span></b>
                </td>
                <td >
                   
                    <asp:CheckBox ID="chkAll" runat="server" CssClass="chkBox" onclick="CheckUncheckAllListBox()" AutoPostBack="false"
                        Text="ALL" TabIndex="7" />
                    <br />
                    <div id="dvAddChain" runat="server" class="chkBoxHold" >
                        <asp:Label ID="lbErrorPop" Text="" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        <table>
                            <tr>
                                <td>
                                    <asp:CheckBoxList ID="chklLocation" runat="server" Font-Bold="false" 
                                        DataTextField="Locationname" CssClass="chkBox" DataValueField="Locationcode" TabIndex="11" RepeatLayout="UnorderedList">
                                    </asp:CheckBoxList>
                                    <asp:HiddenField ID="hdnTotalLocations" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdnNoOfCheckedNodes" runat="server" Value="0" />
                                </td>
                            </tr>
                        </table>
                    </div>
                   
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="
    padding-top: 15px;
    padding-left: 27%;
">
                    <div class="btnRed">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnLeft" OnClientClick="return btnSave_ClientClick()"
                        OnClick="btnSave_Click" TabIndex="12" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" 
                            OnClick="btnCancel_Click" TabIndex="13" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

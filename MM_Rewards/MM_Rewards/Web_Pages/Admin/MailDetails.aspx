﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="MailDetails.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.MailDetails"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <script type="text/javascript">

        function ValidatePage() {

            var r = confirm("Do you want to submit record ?");
            //alert(r);
            if (r != true) {
                return false;
            }


            var errMsg = '';
            var vddl;
            var activity;
            vddl = document.getElementById('<%=ddlLocation.ClientID %>');
            errMsg += CheckDropdown_Blank(vddl.value, " Division");

            vddl = document.getElementById('<%=ddlActivity.ClientID %>');
            errMsg += CheckDropdown_Blank(vddl.value, " Activity");

            vddl = document.getElementById('<%=txtFromName.ClientID %>');
            errMsg += CheckText_Blank(vddl.value, "From Name");
            errMsg += ValidateAlphabet(vddl.value, "From Name");

            vddl = document.getElementById('<%=txtFromId.ClientID %>');
            errMsg += CheckText_Blank(vddl.value, "From ID");
            errMsg += checkMailIds(vddl.value, "From ID");

            activity=document.getElementById('<%=ddlActivity.ClientID %>');
            vddl = document.getElementById('<%=txtTOID.ClientID %>');
            errMsg += CheckTextToId_Blank(vddl.value, "TO ID", activity.value);
            errMsg += checkMailIds(vddl.value, "TO ID");

            vddl = document.getElementById('<%=txtCCID.ClientID %>');
         //   errMsg += CheckText_Blank(vddl.value, "CC ID"); as per uat 19 dec
            errMsg += checkMailIds(vddl.value, "CC ID");

            vddl = document.getElementById('<%=txtSubject.ClientID %>');
            errMsg += CheckText_Blank(vddl.value, " Subject ");
         


            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <div class="form01">
        <table  >
            <tr>
                <td colspan="3">
                    <h1>Mails Details</h1>
                        
                </td>
            </tr>
              <tr>
                <td class="lblName" >
                    <asp:Label ID="lblLcaotion" runat="server" Text="Division" ></asp:Label>&nbsp;<span style="color:Red">*</span>
                </td>
                <td >
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddlLocation" runat="server"  TabIndex="1"  >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblActivity" runat="server" Text="Activity" ></asp:Label>&nbsp;<span style="color:Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddlActivity" runat="server" 
                        AutoPostBack="True" 
                        onselectedindexchanged="ddlActivity_SelectedIndexChanged" TabIndex="2">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblFromName" runat="server" Text="From Name"></asp:Label>&nbsp;<span style="color:Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtFromName" runat="server"  MaxLength="500" TabIndex="3" 
                      ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName" >
                    <asp:Label ID="lblFromID" runat="server" Text="From ID"></asp:Label>&nbsp;<span style="color:Red">*</span>
                </td>
                <td >
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtFromId" runat="server"   MaxLength="500"  Width="300px"
                        TabIndex="4"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName" >
                    <asp:Label ID="lblTOID" runat="server" Text="TOID"></asp:Label>&nbsp;<span style="color:Red;" visible="false" runat="server" id="vtoid">*</span>
                </td>
                <td >
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtTOID" runat="server"  MaxLength="500"  Width="300px"
                        TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName" >
                    <asp:Label ID="lblCCID" runat="server" Text="CCID"></asp:Label>&nbsp;
                </td>
                <td >
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtCCID" runat="server"   MaxLength="1000"  Width="300px"
                        TabIndex="6"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblName" >
                    <asp:Label ID="lblSubject" runat="server" Text="Subject"></asp:Label>&nbsp;<span style="color:Red">*</span>
                </td>
                <td >
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtSubject" runat="server" TextMode="MultiLine"  Width="300px"
                        MaxLength="1000" TabIndex="7"></asp:TextBox>
                </td>
            </tr>
            <%--<tr>
                <td style="width: 98px" >
                    <asp:Label ID="lblMessage" runat="server" Text="Message"></asp:Label>&nbsp;<span style="color:Red">*</span>
                </td>
                <td style="width: 11px">
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Width="350px"  MaxLength="8000"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <tr>
            <td colspan="2">&nbsp;</td>
                <td  align="center" >
                   <div class="btnRed" >
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click"
                        OnClientClick="return ValidatePage() " CssClass="btnLeft" TabIndex="8" />
                        </div>
                           <div class="btnRed">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                        OnClick="btnCancel_Click" TabIndex="9"  CssClass="btnLeft"/>
                        </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

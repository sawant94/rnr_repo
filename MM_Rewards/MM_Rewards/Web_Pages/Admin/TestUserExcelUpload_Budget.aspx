﻿<%@ Page Title="User Excel Upload - Budget" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master"
    AutoEventWireup="true" CodeBehind="TestUserExcelUpload_Budget.aspx.cs" Inherits="MM_Rewards.Web_Pages.TestUserExcelUpload_Budget" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript">
        function checkFile() {
            var validFilesTypes = ["xls", "xlsx"];
            var fileName = document.getElementById("<%= fuExcel.ClientID %>").value;
            var label = document.getElementById("<%=lblError.ClientID%>");
            var ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length).toLowerCase();
            var isValidFile = false;
            for (var i = 0; i < validFilesTypes.length; i++) {
                if (ext == validFilesTypes[i]) {
                    isValidFile = true;
                    break;
                }
            }
            if (!isValidFile) {
                label.innerHTML = "Invalid File. Please upload a File with" +
                " extension:\n\n" + validFilesTypes.join(", ");
                return isValidFile;
            }
            else {
                return isValidFile;
            }

        }
        function chkBlank() {
            var txtboxValue = document.getElementById("<%= txtSearchBox.ClientID %>").value;
            if (txtboxValue == "") {
                var label = document.getElementById("<%=lblError2.ClientID%>");
                label.innerHTML = "Enter the Token Number";
                return false;
            }
            else
                return true;
        }

        function Openpopup() {
            $("#myModal").modal('show');
        }

        function ClosePopup() {
            $('.modal').css('display', 'none');
          
        }
       


        function chkBlanktxt() {
            var txtboxValue = document.getElementById("<%= txtTokenNumber.ClientID %>").value;
            var Calendar1 = $find("<%= telStartDate.ClientID %>");
            startDate = Calendar1.get_selectedDate();
            var Calendar2 = $find("<%= telEndDate.ClientID %>");
            var endDate = Calendar2.get_selectedDate();
            if (txtboxValue == "") {
                var label = document.getElementById("<%=LblErrorSave.ClientID%>");
                label.innerHTML = "Enter the Token Number";
                return false;
            }
            else {
                if (startDate < endDate) {
                    return true;
                }
                else {
                    var label = document.getElementById("<%=LblErrorSave.ClientID%>");
                    label.innerHTML = "Start Date Should be Smaller than End Date";
                    return false;
                }
            }

        }
        function chkNumbers(key) {

            var keycode = (key.which) ? key.which : key.keyCode;
            var budgettxt = document.getElementById('<%=txtBudget.ClientID%>');
            if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else {
                return true;
            }
        }

    </script>    
  <%--changed By Archana K. On 07-06-2016 "SHUBHLABH EMPLOYEES" TO "HOD BUDGET UPLOAD" --%>
      <%-- <h1>
        ShubhLabh Employees Budget Management
    </h1>--%>
    <h1>
        HOD Budget Management
    </h1>
    <br />
    <br />
    <br />
    <div id="MainDiv">
       <%-- <h1>
            UPLOAD SHUBHLABH EMPLOYEES BUDGET</h1>
        --%>
         <h1>
            UPLOAD HOD BUDGET</h1>
        <br />
        <div style="width: 500px;">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="10pt">Please Select Excel File to Upload </asp:Label>
            <br />
            <br />
            <table style="width: 98%;" align="center">
                <tr>
                    <td align="right">
                        <asp:FileUpload ID="fuExcel" runat="server" TabIndex="1" />
                    </td>
                    <td>
                        <div class="btnRed">
                            <asp:Button ID="BtnUpload" runat="server" Font-Bold="True" OnClick="BtnUpload_Click"
                                Text="Upload Excel" CssClass="btnLeft" TabIndex="5" CausesValidation="False" /></div>
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="2"> <br />
                      
                            <b>Note :</b> Please upload excel with following mandatory fields and in same sequence.
                           <h5>  TokenNumber, StartDate, EndDate, Budget.</h5>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblError" runat="server" Font-Italic="True" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <br />
        <%--<h1>
            SEARCH EMPLOYEES BUDGET
        </h1>--%>
        <h1>
            SEARCH HOD BUDGET
        </h1>
        <br />
        <div id="searchBoxDiv" style="width: 799px;">
            <table style="width: 652px">
                <tr>
                    <td class="style2">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="10pt">
              Enter Token Number / Name <span style="color:Red;">*</span> </asp:Label>
                    </td>
                    <td style="width: 160px;">
                        <asp:TextBox ID="txtSearchBox" runat="server" MaxLength="100" TabIndex="10"></asp:TextBox>
                    </td>
                    <td>
                        <div class="btnRed">
                            <asp:Button ID="BtnSearch" runat="server" TabIndex="15" CausesValidation="false"
                                AccessKey="4" Text="Get HOD Details" Font-Bold="True" OnClick="BtnSearch_Click"
                                CssClass="btnLeft" /></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="width: 150px;" colspan="2">
                        <asp:Label ID="lblError2" runat="server" Font-Italic="True" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <asp:Label runat="server" ID="lblDetailsList" Visible="false" Font-Bold="true" Font-Italic="True"
            Text=""></asp:Label>
        <br />
        <br />
        <div id="GridDiv" class="grid01" style="width: 100%;">
            <asp:GridView ID="grdExcelUpload" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                DataKeyNames="TokenNumber" PageSize='<%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdExcelUpload_PageIndexChanging"
                OnRowDataBound="grdExcelUpload_RowDataBound" OnRowCommand="grdExcelUpload_RowCommand">
                <PagerStyle CssClass="dataPager" />
                <SelectedRowStyle CssClass="selectedoddTd" />
                <AlternatingRowStyle CssClass="row01"></AlternatingRowStyle>
                <Columns>
                    <asp:BoundField DataField="TokenNumber" HeaderText="Token Number"></asp:BoundField>
                    <asp:BoundField DataField="EmployeeName" HeaderText="Employee Name"></asp:BoundField>
                    <asp:BoundField DataField="StartDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Start Date"
                        HtmlEncodeFormatString="false" HtmlEncode="false"></asp:BoundField>
                    <asp:BoundField DataField="EndDate" DataFormatString="{0:dd/MM/yyyy}" HtmlEncodeFormatString="false"
                        HtmlEncode="false" HeaderText="End Date"></asp:BoundField>
                    <asp:BoundField DataField="Budget" HeaderText="Budget" DataFormatString="{0:0.00}">
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton CssClass="editLink" ID="btnEdit" runat="server" Text="Edit" CommandName="Select"
                                OnClick="btnEdit_Click" CommandArgument="TokenNumber" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <asp:Panel runat="server" ID="formUpdatePanel" Visible="false">
        <div id="formEditDiv">
            <table class="style1">
                <tr>
                    <td style="width: 150px; height: 30px;">
                        <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtEmpName" runat="server" TabIndex="19" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; height: 30px;">
                        <asp:Label ID="lblTokenNumber" runat="server" Text="Token Number"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtTokenNumber" runat="server" TabIndex="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px;">
                        <asp:Label ID="lblFirstName" runat="server" Text="Start Date"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="telStartDate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="4">
                            <Calendar runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" Height="30px">
                            </Calendar>
                            <DateInput runat="server" DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy"
                                TabIndex="8">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="4"></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px;">
                        <asp:Label ID="lblLastName" runat="server" Text="End Date"></asp:Label>
                    </td>
                    <td colspan="3">
                        <telerik:RadDatePicker ID="telEndDate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="4">
                            <Calendar ID="Calendar1" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" Height="30px">
                            </Calendar>
                            <DateInput ID="DateInput1" runat="server" DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy"
                                TabIndex="8">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="4"></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px;">
                        <asp:Label ID="lblBudget" runat="server" Text="Budget"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtBudget" runat="server" TabIndex="30" onkeypress="return chkNumbers(event)"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <div class="btnRed">
                            <asp:Button ID="BtnSave" runat="server" TabIndex="55" CausesValidation="false" Text="Save"
                                Font-Bold="True" OnClick="BtnSave_Click" CssClass="btnLeft" /></div>
                        &nbsp;
                        <div class="btnRed">
                            <asp:Button ID="BtnCancel" runat="server" TabIndex="60" CausesValidation="false"
                                Text="Cancel" Font-Bold="True" OnClick="BtnCancel_Click" CssClass="btnLeft" /></div>
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <div style="margin-left: 200px;">
        <asp:Label ID="LblErrorSave" runat="server" Font-Italic="True"></asp:Label></div>


   <%-- <div id="modal_dialog1" style="display: none; text-align: center">
    <h4>
        Failed TokenNo:
        <br /><asp:Label ID="lbldamagedesc" runat="server"></asp:Label></h4>
    
    <div class="clr">
    </div>
        </div>--%>


     <div runat="server" id="myModal" class="modal">
            <div class="modal-content">
                <label style="padding: 10px 15px; background-color: #d31621; color: #fff; display: block;">Give Award</label>
                <span onclick="ClosePopup();" class="closemodal">&times;</span>
                <div class="modal_inner">
                    
       <h4>
        Failed TokenNo:
          <br /><asp:Label ID="lbldamagedesc" runat="server"></asp:Label></h4>
    
   </div>
                  
            </div>
        </div>
    <style type="text/css">
        .hideGridColumn
        {
            display: none;
        }
        
        
        .style1
        {
            width: 100%;
        }
        
        
        
        
        .style2
        {
            height: 63px;
            width: 205px;
        }
    </style>

       <style>
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            padding-top: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            border: 1px solid #888;
            width: 80%;
            position: relative;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            box-shadow: 0px 0px 5px 3px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0px 0px 5px 3px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0px 0px 5px 3px rgba(0, 0, 0, 0.3);
            overflow: hidden;
        }

        .modal_inner {
            height: 480px;
            overflow-y: auto;
            padding: 20px;
        }

        /* The Close Button */
        .closemodal {
            color: #fff;
            float: right;
            font-size: 28px;
            font-weight: bold;
            position: absolute;
            right: 15px;
            top: 0;
        }

            .closemodal:hover,
            .closemodal:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .tbl_rec_list {
            width: 100%;
            border-collapse: collapse;
        }

            .tbl_rec_list tr td {
                padding: 3px 7px;
                border: 1px solid #888;
            }

        div.btnRed {
            display: inline-block;
            float: none;
        }
    </style>
</asp:Content>

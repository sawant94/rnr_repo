﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using MM.DAL;
namespace MM_Rewards.Web_Pages.Admin
{
    public partial class User_Feedback : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                if (!Page.IsPostBack)
                {
                 txtTokenNumber.Text = Session["TokenNumber"].ToString();// to populate textbox control with Login user token number.
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserFeedback.aspx");
            }            
        }
        //to save feedbackdetails
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int _SaveStatus = 0;
                BE_UserFeedback objBE_UserFeedback = new BE_UserFeedback();
                BL_UserFeedback objBL_UserFeedback = new BL_UserFeedback();
                objBE_UserFeedback.Tokennumber = txtTokenNumber.Text.Trim();
                objBE_UserFeedback.Createdby = txtTokenNumber.Text.Trim();
                objBE_UserFeedback.Feedabacktype = char.Parse(rdbfeedbacktype.SelectedValue.ToString());
                objBE_UserFeedback.Feedbackdetails = txtFeedbackDetail.Text.Trim();
                _SaveStatus = objBL_UserFeedback.SaveData(objBE_UserFeedback);
                if (_SaveStatus > 0)
                {
                    ThrowAlertMessage("User FeedBack Saved Successfully");
                    txtFeedbackDetail.Text = "";
                    rdbfeedbacktype.SelectedIndex = 0;
                }
                else
                {
                    ThrowAlertMessage(MM_Messages.DatabaseError);
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserFeedback.aspx");
            } 
        }
        //to clear screen
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                txtFeedbackDetail.Text = "";
                rdbfeedbacktype.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserFeedback.aspx");
            } 
        }
        #endregion
        
        #region Function
        // to throw alert message.
        private void ThrowAlertMessage(string strmessage)
        {        
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }
        #endregion     
    }
}
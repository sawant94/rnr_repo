﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="SetUserLevels.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.SetUserLevels" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <script type="text/javascript">

        function EditLevels(lbl, txt, edit, update, cancel, rowIndex) {
            lblLevels = document.getElementById(lbl);
            txtLevels = document.getElementById(txt);
            btnEdit = document.getElementById(edit);
            btnUpdate = document.getElementById(update);
            btnCancel = document.getElementById(cancel);

            txtLevels.value = lblLevels.innerHTML;

            lblLevels.style.display = "none";
            txtLevels.style.display = "block";

            btnEdit.style.display = "none";
            btnUpdate.style.display = "block";
            btnCancel.style.display = "block";

            // for highlighting selected row 
            grdUser = document.getElementById('<%= grdUser.ClientID %>');

            var row = grdUser.rows[parseInt(rowIndex) + 1];
            SelectRow(row);
            //row.style.backgroundColor = '#FFFFCC';

            return false;
        }

        function Cancel(lbl, txt, edit, update, cancel, rowIndex) {
            lblLevels = document.getElementById(lbl);
            txtLevels = document.getElementById(txt);
            btnEdit = document.getElementById(edit);
            btnUpdate = document.getElementById(update);
            btnCancel = document.getElementById(cancel);

            lblLevels.style.display = "block";
            txtLevels.style.display = "none";
            txtLevels.value = "";

            btnEdit.style.display = "block";
            btnUpdate.style.display = "none";
            btnCancel.style.display = "none";

            // for de-selecting the row selected row 
            grdUser = document.getElementById('<%= grdUser.ClientID %>');


            var row = grdUser.rows[parseInt(rowIndex) + 1];
            //row.style.backgroundColor = '';
            UnselectRow(row);

            return false;
        }

        function btnSearch_ClientClick() {
      
            var errMsg = "";
            var txtSearchBy = document.getElementById('<%= txtSearchBy.ClientID %>');
            errMsg += CheckText_Blank(txtSearchBy.value, 'Token Number or Name');
            errMsg += ValidateName(txtSearchBy.value, 'Token Number or Name');

            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                //document.getElementById('lnkShowAssoc').style.diplay = "block";
                return true;
            }
        }

        function validate(sender, key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes
            if ((keycode >= 48 && keycode <= 57)
                 || (keycode >= 96 && keycode <= 105)
                 || keycode == 110
                 || (keycode >= 35 && keycode <= 40)  // 35 end,36 home,37 to 40 are arrow keys
                 || keycode == 8 // backspace
                 || keycode == 9  // tab
                 || keycode == 46  // delete
                ) {
                return true;
            }
            else {
                sender.value = "";
                return false;
            }
        }

        function txtLevels_onblur(txtLevels) {
            //       alert("HI");
            txtLevels = document.getElementById(txtLevels);
            if (parseFloat(txtLevels.value) == 0) {
                txtLevels.value = "";
            }

        }

        function changeDivDisplay(divId) {
            div = document.getElementById(divId);
            if (div.style.display == 'block')
            { div.style.display = 'none' }
            else
            { div.style.display = 'block' }
        }


    </script>

        <div id="divCEO" class="form01">
            <table >
                <tr>
                    <td colspan="3">
                        <h1>
                            Manage User Level
                        </h1>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Enter&nbsp;User's&nbsp;Token&nbsp;Number/Name"></asp:Label> <span style="color: Red">*</span>
                    </td>
                    <td>:</td>
                    <td >
                        <asp:TextBox ID="txtSearchBy" runat="server" MaxLength="100" TabIndex="1"></asp:TextBox>
                        <div class="btnRed"><asp:Button ID="btnSearch" CssClass="btnLeft" runat="server" Text="GET  DETAILS" OnClick="btnGetCEOs_Click"
                            OnClientClick="return btnSearch_ClientClick()"  TabIndex="2" /></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div id="lnkShowAssoc" class="lnkShowAssoc" style="cursor: pointer;display:none;" runat="server"> 
                            <a onclick="changeDivDisplay('lnkBox')">Show association </a><%--</div>--%>
                            <div id="lnkBox" class="lnkBox2" style="cursor: pointer;" onclick="changeDivDisplay('lnkBox')">
                                <asp:Label ID="lblTable" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="grid01">
                             <asp:GridView ID="grdUser" runat="server" AllowPaging="true" AutoGenerateColumns="false" PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                            OnPageIndexChanging="grdUser_PageIndexChanging" AlternatingRowStyle-CssClass="row01" OnRowDataBound="grdUser_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="TokenNumber" HeaderText="TokenNumber"></asp:BoundField>
                                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                                <asp:BoundField DataField="Division" HeaderText="Division"></asp:BoundField>
                                <asp:BoundField DataField="Location" HeaderText="Location"></asp:BoundField>
                                <asp:BoundField DataField="Designation" HeaderText="Designation"></asp:BoundField>
                                <asp:TemplateField HeaderText="Levels">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLevels" runat="server" Text='<%# Eval("Levels") %>'></asp:Label>
                                        <asp:TextBox ID="txtLevels" runat="server" Text='<%# Eval("Levels") %>' MaxLength="2"
                                            Width="50px" Style="display: none" onkeyup="return validate(this,event)"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" CssClass="editLink" runat="server" Text="EDIT" CommandName="select" TabIndex="3"></asp:LinkButton>
                                        <asp:LinkButton ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" class="linkBtn"
                                            Style="display: none" TabIndex="4"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" Style="display: none" class="linkBtn"
                                            TabIndex="5"></asp:LinkButton>
                                        <%--<asp:LinkButton ID="lnkSelect" runat="server" OnClientClick = '<%# String.Format("return SelectUser('{0}','{1}',this)", Eval("TokenNumber"),Eval("Levels")) %>'
                                        Text="Select"></asp:LinkButton>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle  CssClass="dataPager"  />
                            <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                                BorderStyle="Solid" Font-Italic="true" />
                            <EmptyDataTemplate>
                                <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                            </EmptyDataTemplate>
                            <SelectedRowStyle CssClass="selectedoddTd" />
                        </asp:GridView>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
  
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using MM_Rewards;
using BL_Rewards.Admin;
using BE_Rewards;
using BL_Rewards.BL_Common;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class MailDetails : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BE_MailDetails objBE_Maildetails = new BE_MailDetails();
        BL_MailDetails objBL_MailDetails = new BL_MailDetails();
        int _SaveStatus = 0;

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
            Common.CheckPageAccess();// to check user acess.
            if (!Page.IsPostBack)
            {
                try
                {   GetActivityDropdown();    //to fill Activity dropdown
                    GetDivisonDropdown();     //to fill Divison dropdown
                }
                catch (Exception ex)
                {   ThrowAlertMessage(MM_Messages.DatabaseError);
                    objExceptionLogging.LogErrorMessage(ex, "MailDetails.aspx");
                }
            }
        }
        //to clear screen
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {   clearscreen();//clear screen
                ddlLocation.SelectedIndex = 0;
                vtoid.Visible = false;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "MailDetails.aspx");
            }
        }
        //to save maildetails
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                BL_MailDetails objBL_Maildetails = new BL_MailDetails();
                objBE_Maildetails.Divisioncode = ddlLocation.SelectedValue.ToString();
                objBE_Maildetails.ActivityCode = Convert.ToInt16(ddlActivity.SelectedValue.ToString());
                objBE_Maildetails.Fromid = txtFromId.Text.Trim().Replace(" ","");
                objBE_Maildetails.Fromname = txtFromName.Text;
                objBE_Maildetails.Toid = txtTOID.Text;
                objBE_Maildetails.Ccid = txtCCID.Text;
                objBE_Maildetails.Subject = txtSubject.Text;
                //objBE_Maildetails.Message = txtMessage.Text;
                objBE_Maildetails.Createdby = Session["TokenNumber"].ToString();
                objBE_Maildetails.Modifiedby = Session["TokenNumber"].ToString();
                //Session["TokenNumber"].ToString();

                _SaveStatus = objBL_Maildetails.SaveData(objBE_Maildetails);

                if (_SaveStatus > 0)
                {
                    ThrowAlertMessage(MM_Messages.DataSaved);
                }
                else
                {
                    ThrowAlertMessage(MM_Messages.DatabaseError);
                }
                clearscreen();
                ddlLocation.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "MailDetails.aspx");
            }
        }
        // to retrive data for selected activity  if it exist in database . or add new one.
        protected void ddlActivity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlLocation.SelectedValue.ToString() == "-- Select --")
                {
                    ThrowAlertMessage("please select Division");
                    ddlActivity.SelectedIndex = 0;
                }
                else
                {   
                    if (ddlActivity.SelectedValue.ToString() == "-- Select --")
                    {
                        ThrowAlertMessage("Please Select Activity");
                        clearscreen();
                    }
                    else
                    {    // to retrive data for selected activity  if it exist in database . 
                        int activityid = Convert.ToInt16(ddlActivity.SelectedValue.ToString());
                        if (activityid == 3 || activityid == 4)
                        {
                            vtoid.Visible = true;
                        }
                        else
                        {
                            vtoid.Visible = false;
                        }
                        BE_MailDetails objBE_MailDetails = new BE_MailDetails();
                        objBE_MailDetails = objBL_MailDetails.GetData(activityid, ddlLocation.SelectedValue.ToString());
                        if (objBE_MailDetails != null)
                        {
                            txtCCID.Text = objBE_MailDetails.Ccid;
                            txtFromId.Text = objBE_MailDetails.Fromid;
                            txtFromName.Text = objBE_MailDetails.Fromname;
                            //txtMessage.Text = objBE_MailDetails.Message;
                            txtSubject.Text = objBE_MailDetails.Subject;
                            txtTOID.Text = objBE_MailDetails.Toid;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "MailDetails.aspx");
            }
        }
        #endregion

        #region Function
        // to throw alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }
        // to clear screen.
        private void clearscreen()
        {
            ddlActivity.SelectedIndex = 0;
            //ddlLocation.SelectedIndex = 0;
            txtFromId.Text = "";
            txtFromName.Text = "";
            txtCCID.Text = "";
            txtSubject.Text = "";
            txtTOID.Text = "";
            //txtMessage.Text = "";
        }
        //to fill Activity dropdown
        private void GetActivityDropdown()
        {
            BL_MailDetails objBL_Maildetails = new BL_MailDetails();
            BE_MasActivity objBE_MasActivity = new BE_MasActivity();
            BE_MasActivityColl objBE_MasActivitycoll = new BE_MasActivityColl();
            objBE_MasActivitycoll = objBL_Maildetails.Getdropdownitem();
            ddlActivity.DataTextField = "Activity";
            ddlActivity.DataValueField = "Id";
            ddlActivity.DataSource = objBE_MasActivitycoll;
            ddlActivity.DataBind();
            ddlActivity.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
        }
        //to fill Divison dropdown
        private void GetDivisonDropdown()
        {
            BE_MasActivity objBE_MasActivity = new BE_MasActivity();
            BE_MasActivityColl objBE_MasActivitycoll = new BE_MasActivityColl();
            objBE_MasActivitycoll = objBL_MailDetails.GetDivisiondropdownitem();
            ddlLocation.DataTextField = "Divisionname";
            ddlLocation.DataValueField = "Divisioncode";
            ddlLocation.DataSource = objBE_MasActivitycoll;
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
        }
        #endregion
    }
}
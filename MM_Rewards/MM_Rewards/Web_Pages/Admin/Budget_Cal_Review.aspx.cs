﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using BL_Rewards;
using BE_Rewards;
using BL_Rewards.BL_Common;
using System.Text;
using BL_Rewards.BL_Rewards;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;
using BE_Rewards.BE_Rewards;
using System.IO;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class Budget_Cal_Review : System.Web.UI.Page
    {
        string updatedBudget = string.Empty;
        string tokenNumber = string.Empty;
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        //Added by megha for Budget Calculation
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindCompanyCode();
                    BindEmployeeLevelName_ES();

                    Session["ExportToExcelDt"] = null;
                    //Session["Division1"] = "0";
                    //Session["CompanyCode"] = "0";
                    //Session["BusinessUnit"] = "0";
                    //Session["TokenName"] = "";
                    //Session["EmployeeLevelName_ES"] = "0";
                    Session["SortBy"] = string.Empty;

                    //BindDivision();
                    DataSet ds = new DataSet();
                    DAL_Common objDALCommon = new DAL_Common();
                    objDALCommon = new DAL_Common();
                    ds = objDALCommon.DAL_GetData("CheckCurrentFYBudget");
                    FinicialyearStartID.Text = ds.Tables[0].Rows[0]["StartDate"].ToString();
                    FinicialyearEndID.Text = ds.Tables[0].Rows[0]["EndDate"].ToString();
                    FinicialyearStartID1.Text = ds.Tables[0].Rows[0]["StartDate"].ToString();
                    FinicialyearEndID1.Text = ds.Tables[0].Rows[0]["EndDate"].ToString();
                    if (ds.Tables[0].Rows[0]["AvaliableRecord"].ToString() == "1")//"1"
                    {
                        BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

                        //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
                    }
                    else
                    {
                        BudgetUploadedID.Attributes["style"] = "display:block";
                        TblID.Attributes["style"] = "display:none";
                        Detail1.Attributes["style"] = "display:none";
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }


        protected void grdReviewBudget_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName == "Edit")
            //{
            //    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
            //    txtTokenNumber.Text= row.Cells[0].Text;
            //    txtName.Text= row.Cells[1].Text;
            //    txtBudget.Text = row.Cells[9].Text;
            //    myModal.Attributes["style"] = "display:block";


            //}
            // if (e.CommandName == "Edit")
            //{
            //    //int rowIndex = ((GridViewRow)(((Button)e.CommandSource).NamingContainer)).RowIndex;
            //    //grdReviewBudget.EditIndex = rowIndex;
            //    ////BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
            //    //BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

            //}
            //else 
            if (e.CommandName == "Update")
            {

                int row = ((GridViewRow)((Button)e.CommandSource).NamingContainer).RowIndex;
                tokenNumber = Convert.ToString(e.CommandArgument);
                updatedBudget = ((TextBox)(grdReviewBudget.Rows[row].FindControl("txtUpdatedBudgetHOD"))).Text;
                int Budget;
                bool chkIsNumber = Int32.TryParse(updatedBudget, out Budget);              
                if (chkIsNumber == true)
                {
                    grdReviewBudget.EditIndex = -1;
                    NewBudget_Click(this, null);
                }
                else
                {
                    ThrowAlertMessage("Amount Should not in decimal ");

                }

            }

        }

        protected void NewBudget_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@UpdatedBudget", updatedBudget);// Convert.ToInt16(txtNewBudget.Text)
            param[1] = new SqlParameter("@TokenNumber", tokenNumber);//txtTokenNumber.Text
            param[2] = new SqlParameter("@UpdatedBy", Session["TokenNumber"].ToString());

            ds = objCommon.DAL_GetData("usp_UpdateReviewBudget", param);

            ThrowAlertMessage("Budget has been Updated Successfully");
            // grd.CssClass = lblName.CssClass.Replace("selectedoddTd", "");
            //.Attributes["style"] = "display:none";
            myModal.Attributes["style"] = "display:none";
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
        }

        protected void closeemailpopup_Click(object sender, EventArgs e)
        {
              myModal.Attributes["style"] = "display:none";
             
        }

        protected void grdReviewBudget_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdReviewBudget.PageIndex = e.NewPageIndex;
                BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

                //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }   

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            string BudgetFlag = "";
            DAL_Common objCommon = new DAL_Common();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@CreatedBy", Session["TokenNumber"].ToString());
            ds = objCommon.DAL_GetData("usp_Insert_BudgetFromReviewBudgetToMainTbl", param);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                BudgetFlag = dr["BudgetUploadFlag"].ToString();
            }
            if (BudgetFlag == "1")
            {
                BudgetUploadedID.Attributes["style"] = "display:block";
                TblID.Attributes["style"] = "display:none";
                Detail1.Attributes["style"] = "display:none";
            }
        }

        private void BindBudget(string Division, string CompanyCode, string BusinessUnit, string TokenNumber, string EmployeeLevelName_ES)
        {
            try
            {
                if (Division == "") { Division = "0";}
                if (CompanyCode == "") { CompanyCode = "0"; }
                if (BusinessUnit == "") { BusinessUnit = "0"; }
                if (EmployeeLevelName_ES == "") { EmployeeLevelName_ES = "0"; }
                Detail1.Attributes["style"] = "display:block";
                DataSet ds = new DataSet();
                DAL_Common objCommon = new DAL_Common();
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@Division", Division);
                param[1] = new SqlParameter("@CompanyCode", CompanyCode);
                param[2] = new SqlParameter("@BusinessUnit", BusinessUnit);
                param[3] = new SqlParameter("@TokenNumber", TokenNumber);
                param[4] = new SqlParameter("@EmployeeLevelName_ES", EmployeeLevelName_ES);

                ds = objCommon.DAL_GetData("GetBudgetForReview", param);
                DataTable _dt = new DataTable();
                _dt = ds.Tables[1];
                foreach (DataRow _dr in _dt.Rows)
                {
                    CalTotalBudget.Text = _dr["CalTotalBudget"].ToString();
                }
                _dt = ds.Tables[2];
                foreach (DataRow _dr in _dt.Rows)
                {
                    calLastYrBudget.Text = _dr["CalTotalLastYearBudget"].ToString();
                }

                if (Session["SortBy"].ToString() == "Asc")
                {
                    DataTable dtSortedTable = ds.Tables[0].AsEnumerable()
                        .OrderBy(row => row.Field<int>("BudgetDifference"))
                        .CopyToDataTable();

                    grdReviewBudget.DataSource = dtSortedTable;
                    grdReviewBudget.DataBind();
                    Session["ExportToExcelDt"] = dtSortedTable;
                }
                else if (Session["SortBy"].ToString() == "Desc")
                {
                    DataTable dtSortedTableDesc = ds.Tables[0].AsEnumerable()
                        .OrderByDescending(row => row.Field<int>("BudgetDifference"))
                        .CopyToDataTable();

                    grdReviewBudget.DataSource = dtSortedTableDesc;
                    grdReviewBudget.DataBind();
                    Session["ExportToExcelDt"] = dtSortedTableDesc;
                }
                else
                {

                    grdReviewBudget.DataSource = ds;
                    Session["ExportToExcelDt"] = ds.Tables[0];
                    grdReviewBudget.DataBind();

                }
            }
            catch (Exception e)
            { }

        }



        private void BindDivision(string CompanyCode, string BusinessUnit)
        {
            BL_RecipientAward objBLRecipientAward = new BL_RecipientAward();
            BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
            objBE_AwardManagementColl = objBLRecipientAward.GetDivision(CompanyCode, BusinessUnit);
            ddlDivision.DataSource = objBE_AwardManagementColl;
            ddlDivision.DataTextField = "EmployeeDivisionName";
            ddlDivision.DataValueField = "EmployeeDivisionName";
            ddlDivision.DataBind();
            ddlDivision.Items.Insert(0, new ListItem("Select", "0"));
            //ddlDivision.SelectedIndex = 0;

        }

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        protected void btnDisplay_Click(object sender, EventArgs e)
        {
            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            if (ddlDivision.SelectedIndex > 0)
            {
                ddlDivision.SelectedIndex = 0;
            }
            ddlCompanyCode.SelectedIndex = 0;
            if (ddlBusinessUnit.SelectedIndex > 0)
            {
                ddlBusinessUnit.SelectedIndex = 0;
            }
            ddlEmployeeLevelName.SelectedIndex = 0;
            txtToken.Text = string.Empty;
            Session["SortBy"] = string.Empty;
            Session["ExportToExcelDt"] = null;
            //Session["Division"] = "0";
            //Session["CompanyCode"] = "0";
            //Session["BusinessUnit"] = "0";
            //Session["TokenName"] = "";
            //Session["EmployeeLevelName_ES"] = "0";
            ////BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

        }

        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["BusinessUnit"] = ddlBusinessUnit.SelectedValue.ToString();
            BindDivision(ddlCompanyCode.SelectedValue.ToString(), ddlBusinessUnit.SelectedValue.ToString());
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);
            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
        }
        protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["Division"] = ddlDivision.SelectedValue.ToString();
            //ddlDivision.SelectedValue = (string)Session["Division"];
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
        }

        protected void ddlEmployeeLevelName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["EmployeeLevelName_ES"] = ddlEmployeeLevelName.SelectedValue.ToString();
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
        }

        protected void btnSort_Click(object sender, EventArgs e)
        {
            if (Session["SortBy"].ToString() == "Asc")
            {
                Session["SortBy"] = "Desc";
            }
            else
            {
                Session["SortBy"] = "Asc";
            }
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
        }
        protected void btnUpdatedBudget_Click(object sender,EventArgs e)
        {
            BL_RecipientAward objBLRecipientAward = new BL_RecipientAward();
            DataSet ds = objBLRecipientAward.GetBudgetUpdateReport();
            DataTable dt = ds.Tables[0];
            Export(dt);
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["ExportToExcelDt"];
            Export(dt);
        }
        public void Export(DataTable dt)
        {
            try
            {                
                string attachment = "attachment; filename=Data_" + DateTime.Now + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    Response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {

            }

        }
        
        private void BindCompanyCode()
        {
            BL_RecipientAward objBLRecipientAward = new BL_RecipientAward();
            BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
            objBE_AwardManagementColl = objBLRecipientAward.GetCompanyCode();
            ddlCompanyCode.DataSource = objBE_AwardManagementColl;
            ddlCompanyCode.DataTextField = "CompanyCode";
            ddlCompanyCode.DataValueField = "CompanyCode";
            ddlCompanyCode.DataBind();
            ddlCompanyCode.Items.Insert(0, new ListItem("Select", "0"));

        }
        private void BindBusinessUnit(string CompanyCode)
        {
            BL_RecipientAward objBLRecipientAward = new BL_RecipientAward();
            BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
            objBE_AwardManagementColl = objBLRecipientAward.GetBusinessUnit(CompanyCode);
            ddlBusinessUnit.DataSource = objBE_AwardManagementColl;
            ddlBusinessUnit.DataTextField = "BusinessUnit";
            ddlBusinessUnit.DataValueField = "BusinessUnit";
            ddlBusinessUnit.DataBind();
            ddlBusinessUnit.Items.Insert(0, new ListItem("Select", "0"));

        }
        private void BindEmployeeLevelName_ES()
        {
            BL_RecipientAward objBLRecipientAward = new BL_RecipientAward();
            BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
            objBE_AwardManagementColl = objBLRecipientAward.GetEmployeeLevelName_ES();
            ddlEmployeeLevelName.DataSource = objBE_AwardManagementColl;
            ddlEmployeeLevelName.DataTextField = "EmployeeLevelName_ES";
            ddlEmployeeLevelName.DataValueField = "EmployeeLevelName_ES";
            ddlEmployeeLevelName.DataBind();
            ddlEmployeeLevelName.Items.Insert(0, new ListItem("Select", "0"));

        }

        protected void ddlCompanyCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["CompanyCode"] = ddlCompanyCode.SelectedValue.ToString();
            BindBusinessUnit(ddlCompanyCode.SelectedValue.ToString());
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
        }

        protected void txtToken_TextChanged(object sender, EventArgs e)
        {
            //Session["TokenName"] = txtToken.Text;
        }

        protected void grdReviewBudget_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdReviewBudget.EditIndex = e.NewEditIndex;
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
        }

        protected void grdReviewBudget_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdReviewBudget.EditIndex = -1;
            //BindBudget(Session["Division1"].ToString(), Session["CompanyCode"].ToString(), Session["BusinessUnit"].ToString(), Session["TokenName"].ToString(), Session["EmployeeLevelName_ES"].ToString());
            BindBudget(ddlDivision.SelectedValue, ddlCompanyCode.SelectedValue, ddlBusinessUnit.SelectedValue, txtToken.Text, ddlEmployeeLevelName.SelectedValue);

        }

        protected void grdReviewBudget_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        
        //protected void grdReviewBudget_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        HiddenField field = e.Row.FindControl("HiddenField1") as HiddenField;
        //        if (field.Value == "True")
        //        {
        //            e.Row.BackColor = System.Drawing.Color.LightPink;
        //        }

        //    }

        //}
    }
}
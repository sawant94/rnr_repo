﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class SetUserLevels : System.Web.UI.Page
    {
        BE_BudgetAllocation objBE_BudgetAllocation;
        BE_BudgetAllocationColl objBE_BudgetAllocationColl;
        BL_BudgetAllocation objBL_BudgetAllocation;
        BE_AwardManagementColl objBE_AwardManagementColl;
        BL_AwardManagement objBL_AwardManagement;
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                if (!IsPostBack)
                {
                    CreateTableForAssociation();  // Create table for showing the association between Eligibilty and their levels
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        /// <summary>
        /// Create table for showing the association between Eligibilty and their levels
        /// </summary>
        private void CreateTableForAssociation()
        {
            try
            {
                objBE_AwardManagementColl = new BE_AwardManagementColl();
                objBL_AwardManagement = new BL_AwardManagement();
                objBE_AwardManagementColl = objBL_AwardManagement.GetEmployeeLevelsName();
                lblTable.Text = objBL_AwardManagement.CreateTable(objBE_AwardManagementColl).ToString();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }

        #region GetData

        private BE_BudgetAllocationColl GetAllUsers(string SearchBy = "") // search is by default empty string
        {
            //objBE_BudgetAllocation = new BE_BudgetAllocation();
            objBE_BudgetAllocationColl = new BE_BudgetAllocationColl();
            objBL_BudgetAllocation = new BL_BudgetAllocation();
            try
            {

                objBE_BudgetAllocationColl = objBL_BudgetAllocation.GetAllUsers(SearchBy);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
            return objBE_BudgetAllocationColl;
        }

        #endregion

      // Bind the grid
        private void BindGrid()
        {
            objBE_BudgetAllocationColl = GetAllUsers(txtSearchBy.Text);
            grdUser.DataSource = objBE_BudgetAllocationColl;
            grdUser.DataBind();
            if (objBE_BudgetAllocationColl.Count > 0)
                lnkShowAssoc.Style.Add("display", "block");
            else
                lnkShowAssoc.Style.Add("display", "none");
        }

        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow row = (GridViewRow)e.Row;

                    LinkButton btnEdit = (LinkButton)row.FindControl("btnEdit");
                    LinkButton btnUpdate = (LinkButton)row.FindControl("btnUpdate");
                    LinkButton btnCancel = (LinkButton)row.FindControl("btnCancel");

                    Label lblLevels = (Label)row.FindControl("lblLevels");
                    TextBox txtLabels = (TextBox)row.FindControl("txtLevels");

                    // attach javascript event to set the item in edit mode
                    btnEdit.Attributes.Add("onclick", "return EditLevels('" + lblLevels.ClientID + "','" + txtLabels.ClientID + "','" + btnEdit.ClientID + "','" + btnUpdate.ClientID + "','" + btnCancel.ClientID + "','" + row.RowIndex + "' );");

                    // attach javascript event to set the item out of edit mode
                    btnCancel.Attributes.Add("onclick", "return Cancel('" + lblLevels.ClientID + "','" + txtLabels.ClientID + "','" + btnEdit.ClientID + "','" + btnUpdate.ClientID + "','" + btnCancel.ClientID + "','" + row.RowIndex + "' );");

                    // attach javascript event to validate the txtLevel input
                    txtLabels.Attributes.Add("onblur", "txtLevels_onblur('" + txtLabels.ClientID + "')");
                }

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

        }

        protected void btnGetCEOs_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnUpdate = (LinkButton)sender;

                GridViewRow grdSelRow = grdUser.SelectedRow;

                GridViewRow row = (GridViewRow)btnUpdate.NamingContainer;

                TextBox txtLevels = (TextBox)row.FindControl("txtLevels");
                string TokenNumber = row.Cells[0].Text;

                objBE_BudgetAllocation = new BE_BudgetAllocation();
                objBL_BudgetAllocation = new BL_BudgetAllocation();

                objBE_BudgetAllocation.TokenNumber = TokenNumber;
                objBE_BudgetAllocation.Levels = Convert.ToInt32(txtLevels.Text);

                objBL_BudgetAllocation.SaveData_UserLevels(objBE_BudgetAllocation);

                BindGrid();
                row.Style.Add("backgroundColor", "#FFEEDD"); // remove the highlighting on the selected row 

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

        }

        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SetUserLevels.aspx");
            }
        }

        #region Exception Handling

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        #endregion
    }
}
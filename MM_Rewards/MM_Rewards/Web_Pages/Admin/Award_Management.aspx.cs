﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using BL_Rewards;
using BE_Rewards;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using System.Text;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class Award_Management : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BE_AwardManagement objBE_AwardManagement = new BE_AwardManagement();

        BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
        BL_AwardManagement objBL_AwardManagement = new BL_AwardManagement();

        #region comman function
        /// <summary>
        /// To create the table that shows association between the eligibilities and their levels
        /// </summary>
        /// <param name="objBE_AwardManagementColl"></param>
        /// <returns></returns>
        private StringBuilder CreateTable(BE_AwardManagementColl objBE_AwardManagementColl)
        {
            //StringBuilder LevelTable = "<table><tr><td><b>Eligibility Levels</b></td><td><b>Levels</b></td></tr><content></table>";
            StringBuilder LevelTable = new StringBuilder();
            StringBuilder strContent1 = new StringBuilder();
            StringBuilder strContent2 = new StringBuilder();
            int Count = objBE_AwardManagementColl.Count != 0 ? objBE_AwardManagementColl.Count / 2 : 0;
            int i = 0;
            try
            {
                foreach (BE_AwardManagement objBE_AwardManagement in objBE_AwardManagementColl)
                {
                    i++;
                    if (i % 2 == 0)
                    {
                        strContent1.Append("<tr>");
                        strContent1.Append("<td>" + objBE_AwardManagement.EmployeeLevelsName + "</td><td> " + objBE_AwardManagement.StringLevels + " </td>");
                        strContent1.Append("</tr>");
                    }
                    else
                    {
                        strContent2.Append("<tr>");
                        strContent2.Append("<td>" + objBE_AwardManagement.EmployeeLevelsName + "</td><td> " + objBE_AwardManagement.StringLevels + " </td>");
                        strContent2.Append("</tr>");
                    }
                }


                //LevelTable.Append("<div class='clsDIv'><a class='clsBtn' href='#'>X</a></div> ");
                LevelTable.Append("<div class='grid01'><table><tr><th>Eligibility Levels</th><th>Levels</th></tr>");
                LevelTable.Append(strContent1);
                LevelTable.Append("</table></div>");
                LevelTable.Append("<div class='grid01'><table><tr><th>Eligibility Levels</th><th>Levels</th></tr>");
                LevelTable.Append(strContent2);
                LevelTable.Append("</table></div>");

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

            return LevelTable;
        }

        // bind grid to list of award
        private void BindGrid()
        {
            try
            {
                BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
                objBE_AwardManagementColl = Get_BE_AwardManagementColl();
                grdAwards.DataSource = objBE_AwardManagementColl;
                grdAwards.DataBind();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Award_Management.aspx");
            }

        }
        // to clear input controls
        private void ClearData()
        {
            try
            {
                drpAwardType.SelectedIndex = 0;
                txtName.Text = "";
                txtAmount.Text = "";
                foreach (ListItem lstitem in chklEligibility.Items)
                {
                    lstitem.Selected = false;
                }
                foreach (ListItem lstitem in chkRecipientEligiblity.Items)
                {
                    lstitem.Selected = false;
                }
                chkAll.Checked = false;
                chkallrecipient.Checked = false;
                chkActive.Checked = true;
                chkautoredeem.Checked = false;
                hdnMode.Value = string.Empty;
                grdAwards.DataBind();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        // to get list of  
        private void Getdropdownitem()
        {
            try
            {
                objBE_AwardManagementColl = objBL_AwardManagement.Getdropdownitem();

                for (int count = 0; count < objBE_AwardManagementColl.Count; count++)
                {
                    objBE_AwardManagementColl[count].Name = objBE_AwardManagementColl[count].Id + "|" + objBE_AwardManagementColl[count].IsSystem;
                }
                drpAwardType.DataTextField = "AwardType";
                drpAwardType.DataValueField = "Name";
                drpAwardType.DataSource = objBE_AwardManagementColl;
                drpAwardType.DataBind();
                drpAwardType.Items.Insert(0, new ListItem("Select", "Select"));
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Award_Management.aspx");
            }

        }
        // get collection for grid control
        private BE_AwardManagementColl Get_BE_AwardManagementColl()
        {
            BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
            try
            {
                if (Session["objBE_AwardManagementColl"] == null)
                {
                    objBE_AwardManagementColl = objBL_AwardManagement.GetData();
                    Session["objBE_AwardManagementColl"] = objBE_AwardManagementColl;
                }
                else
                    objBE_AwardManagementColl = Session["objBE_AwardManagementColl"] as BE_AwardManagementColl;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

            return objBE_AwardManagementColl;

        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();

                if (!IsPostBack)
                {
                    Session["objBE_AwardManagementColl"] = null;
                    BindGrid();
                    Getdropdownitem();

                    objBE_AwardManagementColl = objBL_AwardManagement.GetEmployeeLevelsName();
                    chklEligibility.DataSource = objBE_AwardManagementColl;
                    chklEligibility.DataTextField = "EmployeeLevelsName";
                    chklEligibility.DataValueField = "StringLevels";
                    chklEligibility.DataBind();
                    hdnTotalLevels.Value = chklEligibility.Items.Count.ToString(); // for the javascript CheckUncheck All Function to work
                    lblTable.Text = objBL_AwardManagement.CreateTable(objBE_AwardManagementColl).ToString();
                    chkRecipientEligiblity.DataSource = objBE_AwardManagementColl;
                    chkRecipientEligiblity.DataTextField = "EmployeeLevelsName";
                    chkRecipientEligiblity.DataValueField = "StringLevels";
                    chkRecipientEligiblity.DataBind();
                    hdnTotalLevelsrecipients.Value = chkRecipientEligiblity.Items.Count.ToString();


                }
                foreach (ListItem item in chklEligibility.Items)
                {
                    if (item.Enabled == true)
                    {
                        item.Attributes.Add("onclick", "chk_Click(this,'g')");
                    }
                }
                foreach (ListItem item in chkRecipientEligiblity.Items)
                {
                    if (item.Enabled == true)
                    {
                        item.Attributes.Add("onclick", "chk_Click(this,'r')");
                    }
                }

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        // edit an award
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                chklEligibility.ClearSelection();
                chkRecipientEligiblity.ClearSelection();
                LinkButton btn = sender as LinkButton;
                GridViewRow item = btn.NamingContainer as GridViewRow;
                int Id = Convert.ToInt32(grdAwards.DataKeys[item.RowIndex].Value);

                hdnMode.Value = Id.ToString();
                hdnSelectedIndex.Value = item.RowIndex.ToString();

                txtName.Text = item.Cells[1].Text;
                txtAmount.Text = item.Cells[2].Text;
                drpAwardType.ClearSelection();
                Label ltrl = item.Cells[0].Controls[1] as Label;
                ListItem SelectedItem = drpAwardType.Items.FindByText(ltrl.Text);
                SelectedItem.Selected = true;
                chkActive.Checked = Convert.ToBoolean(item.Cells[3].Text);
                chkautoredeem.Checked = Convert.ToBoolean(grdAwards.DataKeys[item.RowIndex].Values["Autoredeem"].ToString());
                hdnIsSystem.Value = grdAwards.DataKeys[item.RowIndex].Values["IsSystem"].ToString();
                if (hdnIsSystem.Value == "True")
                {
                    tblCheckListBx.Style.Add("Display", "None");
                }
                else
                {
                    tblCheckListBx.Style.Add("Display", "Block");
                    BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
                    BE_AwardManagement objBE_AwardManagement = new BE_AwardManagement();
                    objBE_AwardManagementColl = Get_BE_AwardManagementColl();

                    objBE_AwardManagement = objBE_AwardManagementColl.Find(delegate(BE_AwardManagement AM) { return AM.Id == Id; });

                    int count = 0;
                    foreach (int i in objBE_AwardManagement.GiverLevel)
                    {
                        ListItem chk = chklEligibility.Items.FindByValue(i.ToString());
                        if (chk != null)
                        {
                            chk.Selected = true;
                            count++;
                        }
                    }
                    hdnNoOfCheckedNodes.Value = count.ToString();
                    if (Convert.ToInt32(hdnTotalLevels.Value) == Convert.ToInt32(hdnNoOfCheckedNodes.Value))
                    {
                        chkAll.Checked = true;
                    }
                    int rcount = 0;
                    foreach (int i in objBE_AwardManagement.RecipientLevel)
                    {
                        ListItem chk = chkRecipientEligiblity.Items.FindByValue(i.ToString());
                        if (chk != null)
                        {
                            chk.Selected = true;
                            rcount++;
                        }
                    }
                    hdnNoOfCheckedNodesRecipient.Value = rcount.ToString();
                    if (Convert.ToInt32(hdnTotalLevelsrecipients.Value) == Convert.ToInt32(hdnNoOfCheckedNodesRecipient.Value))
                    {
                        chkallrecipient.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        // to save an award 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string[] i = drpAwardType.SelectedValue.Split(new char[] { '|' });
                objBE_AwardManagement.AwardId = Convert.ToInt16(i[0]);
                objBE_AwardManagement.Name = txtName.Text;
                objBE_AwardManagement.Amount = Convert.ToDecimal(txtAmount.Text);
                //    objBE_AwardManagement.Amount = Convert.ToInt32(txtAmount.Text);

                foreach (ListItem item in chklEligibility.Items)
                {
                    if (item.Selected)
                    {
                        objBE_AwardManagement.GiverLevel.Add(Convert.ToInt32(item.Value.ToString()));
                    }
                }
                foreach (ListItem item in chkRecipientEligiblity.Items)
                {
                    if (item.Selected)
                    {
                        objBE_AwardManagement.RecipientLevel.Add(Convert.ToInt32(item.Value.ToString()));
                    }
                }

                if (hdnMode.Value == "")
                {
                    objBE_AwardManagement.Id = 0;
                    if (Session["TokenNumber"] != null)
                        objBE_AwardManagement.CreatedBy = Session["TokenNumber"].ToString();
                    else
                        objBE_AwardManagement.CreatedBy = "TokenID-1";

                    objBE_AwardManagement.ModifiedBy = "TokenID-1";
                }
                else
                {
                    objBE_AwardManagement.Id = Convert.ToInt32(hdnMode.Value);
                    if (Session["TokenNumber"] != null)
                        objBE_AwardManagement.ModifiedBy = Session["TokenNumber"].ToString();
                    else
                        objBE_AwardManagement.ModifiedBy = "TokenID-1";

                    objBE_AwardManagement.CreatedBy = "TokenID-1";
                }
                objBE_AwardManagement.Active = chkActive.Checked;
                objBE_AwardManagement.Autoredeem = chkautoredeem.Checked;
                int ID = objBL_AwardManagement.SaveData(objBE_AwardManagement);
                if (ID != -1)
                {
                    Session["objBE_AwardManagementColl"] = null;
                    ClearData();
                    ThrowAlertMessage(MM_Messages.DataSaved);
                    BindGrid();
                }
                else
                {
                    if (!ClientScript.IsClientScriptBlockRegistered("Script"))
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "Script", "<script language = 'javascript'>alert('An award with the same name already exists!Please select another name.')</script>");
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }

        }
        // to clear selection 
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("~/Web_Pages/HomePage.aspx");
                //ClearData();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Award_Management.aspx");
            }
        }

        // to clear selection 
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("~/Web_Pages/HomePage.aspx");
                ClearData();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Award_Management.aspx");
            }
        }
        protected void grdAwards_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAwards.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Award_Management.aspx");
            }
        }
        #endregion

        #region Exception Handling

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        #endregion

        /*Added by Pratk - 02-09-2014 - for Redeem Date*/
        protected void drpAwardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string AwardType = drpAwardType.SelectedItem.ToString();

            if (AwardType == "Spot Gift Hamper" || AwardType == "Spot Vouchers")
            {
                chkautoredeem.Checked = false;
                chkautoredeem.Enabled = false;
            }
            else
            {
                chkautoredeem.Enabled = true;
            }
        }
    }
}
﻿<%@ Page Title="User Excel Upload" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="UserExcelUpload.aspx.cs"
 Inherits="MM_Rewards.Web_Pages.UserExcelUpload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript">
      function checkFile() {
          var validFilesTypes = ["xls", "xlsx"];
          var fileName = document.getElementById("<%= FileUpload1.ClientID %>").value;
          var label = document.getElementById("<%=lblError.ClientID%>");
          var ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length).toLowerCase();
          var isValidFile = false;
          for (var i = 0; i < validFilesTypes.length; i++) {
              if (ext == validFilesTypes[i]) {
                  isValidFile = true;
                  break;
              }
          }
          if (!isValidFile) {              
              label.innerHTML = "Invalid File. Please upload a File with" +
                " extension:\n\n" + validFilesTypes.join(", ");
              return isValidFile;
          }
          else
          {
          return isValidFile;
          }

      }
      function chkBlank() {
          var txtboxValue = document.getElementById("<%= txtSearchBox.ClientID %>").value;
          if (txtboxValue == "") {
              var label = document.getElementById("<%=lblError2.ClientID%>");
              label.innerHTML = "Enter the Token Number / Name";             
              return false;
          }            
              else
                  return true;
          }

          function chkBlanktxt() {
              var txtboxValue = document.getElementById("<%= txtTokenNumber.ClientID %>").value;
              if (txtboxValue == "") {
                  var label = document.getElementById("<%=LblErrorSave.ClientID%>");
                  label.innerHTML = "Enter the Token Number";
                  return false;
              }
              else
                  return true;
          }

    </script>
    <h1>ShubhLabh Employees Management </h1>
    <br />
    <br />
      <br />
    <div id="MainDiv">
    <h1>UPLOAD SHUBHLABH EMPLOYEES </h1>    
    <br />
        <div style="width:500px;">
       <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="10pt">Please Select Excel File to Upload </asp:Label>
    <br />
    <br />
    <table style="width: 98%;" align="center">
        <tr>
            <td align="right">
                <asp:FileUpload ID="FileUpload1" runat="server" TabIndex="1"/>
            </td>
            <td>
            <div class="btnRed"><asp:Button ID="BtnUpload" runat="server" Font-Bold="True" 
                    onclick="BtnUpload_Click" Text="Upload Excel"  CssClass="btnLeft" TabIndex="5"
                    CausesValidation="False" /></div>
            </td>
            
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblError" runat="server" Font-Italic="True" Text=""></asp:Label>                
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    </div>
    <br />
    <br />
      <br />
    <h1>SEARCH EMPLOYEES </h1>    
    <br />
        <div id="searchBoxDiv" style="width:799px;">
          <table style="width: 652px">
              <tr>
              <td class="style2"><asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="10pt">
              Enter Token Number / Name <span style="color:Red;">*</span> </asp:Label></td>                  
              <td style="width:160px;"><asp:TextBox ID="txtSearchBox" runat="server" MaxLength="100" TabIndex="10"></asp:TextBox>
                  </td>
              <td ><div class="btnRed"><asp:Button ID="BtnSearch" runat="server" TabIndex="15"  CausesValidation="false" AccessKey="4" Text="Get Employee Details" Font-Bold="True" 
                      onclick="BtnSearch_Click" CssClass="btnLeft" /></div>                      
              </td>
              </tr>     
              <tr><td></td><td style="width:150px;"  colspan="2"><asp:Label ID="lblError2" runat="server" Font-Italic="True" Text=""></asp:Label>  </td></tr>               
              </table>
        </div>
    <br />
    <br />    
    <asp:Label runat="server" ID="lblDetailsList" Visible="false"  Font-Italic="True" Font-Bold="true" Text=""></asp:Label>
         <br /><br />
         <div id="GridDiv" class="grid01" style="width:100%;">         
           <asp:GridView ID="grdExcelUpload" runat="server" AllowPaging="True" AutoGenerateColumns="False"  DataKeyNames="TokenNumber,EmployeeLevelCode_ES,EmployeeLevelName_ES,Levels"
                            PageSize='<%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                            AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdExcelUpload_PageIndexChanging"
                            OnRowDataBound="grdExcelUpload_RowDataBound" OnRowCommand="grdExcelUpload_RowCommand" >
                            <PagerStyle CssClass="dataPager" />
                            <SelectedRowStyle CssClass="selectedoddTd" />
                            <AlternatingRowStyle CssClass="row01"></AlternatingRowStyle>
                             <Columns>                                                          
                                <asp:BoundField DataField="TokenNumber"  HeaderText="Token Number"></asp:BoundField>
                                <asp:BoundField DataField="EmployeeName"  HeaderText="Employee Name"></asp:BoundField>
                                <asp:BoundField DataField="DivisionName" HeaderText="Division"></asp:BoundField>
                                <asp:BoundField DataField="LocationName" HeaderText="Location"></asp:BoundField>
                                <asp:BoundField DataField="OrgUnitName" HeaderText="Department"></asp:BoundField> 
                                <asp:BoundField DataField="FirstName"  HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn" ></asp:BoundField>                                                               
                                <asp:BoundField DataField="LastName"  HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn"></asp:BoundField>   
                                <asp:BoundField DataField="EmpGradeCode_E"  HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn" ></asp:BoundField> 
                                <asp:BoundField DataField="Division_PA"  HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn" ></asp:BoundField>                                                                                             
                                <asp:BoundField DataField="EmployeeLevelCode_ES" HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn"></asp:BoundField> 
                                <asp:BoundField DataField="EmployeeLevelName_ES" HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn"></asp:BoundField> 
                                <asp:BoundField DataField="Levels" HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn" ></asp:BoundField> 
                                <asp:BoundField DataField="Location_PSA" HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn" ></asp:BoundField> 
                                <asp:BoundField DataField="LocationName_PSA" HeaderStyle-CssClass = "hideGridColumn" ItemStyle-CssClass="hideGridColumn" ></asp:BoundField> 
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton CssClass="editLink" ID="btnEdit" runat="server" Text="Edit" CommandName="Select"
                                            OnClick="btnEdit_Click" CommandArgument="TokenNumber"/>
                                    </ItemTemplate>
                               </asp:TemplateField>

                                </Columns>
           </asp:GridView>
           
         </div>

</div>            
<asp:Panel runat="server" ID="formUpdatePanel" Visible="false">
<div id="formEditDiv">
    <table class="style1">
        <tr>
            <td style="width:150px; height:30px;">
                <asp:Label ID="lblTokenNumber" runat="server" Text="Token Number"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTokenNumber" runat="server" TabIndex="20"></asp:TextBox>  </td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtFirstName" runat="server" TabIndex="25"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="lblLastName" runat="server" Text="Last Name"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLastName" runat="server" TabIndex="30"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="lblEmployeeGroup" runat="server" Text="Employee Group"></asp:Label>
            </td>
            <td>
               <asp:DropDownList ID="DropEmpDetails" runat="server" TabIndex="30">
              </asp:DropDownList>
</td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="lblEmployeeSubGroup" runat="server" Text="Levels"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropEmpSubDetails" runat="server" TabIndex="40">
              </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDivisionDetails" runat="server" TabIndex="45">
              </asp:DropDownList></td>
        </tr>
         <tr>
            <td style="height:30px;">
                <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropLocationDetails" runat="server" TabIndex="50">
              </asp:DropDownList></td>
        </tr>
         <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
         <tr>
            <td colspan=2 align="center">
              <div class="btnRed"><asp:Button ID="BtnSave" runat="server" TabIndex="55"  CausesValidation="false"  Text="Save" Font-Bold="True" 
               onclick="BtnSave_Click" CssClass="btnLeft" /></div>
               &nbsp;
               <div class="btnRed"><asp:Button ID="BtnCancel" runat="server" TabIndex="60"  CausesValidation="false"  Text="Cancel" Font-Bold="True" 
               onclick="BtnCancel_Click" CssClass="btnLeft" /></div>
               &nbsp;&nbsp;
                </td>
        </tr>
        <tr>
           <td></td>
        </tr>
    </table>


</div>  
</asp:Panel>
<div style="margin-left:200px;"><asp:Label ID="LblErrorSave" runat="server" Font-Italic="True"></asp:Label></div>
    <style type="text/css">        

   .hideGridColumn
    {
        display:none;
    }
 
     
        .style1
        {
            width: 100%;
        }

   
 
     
        .style2
        {
            height: 63px;
            width: 205px;
        }
        

   
 
     
    </style>
  
</asp:Content>

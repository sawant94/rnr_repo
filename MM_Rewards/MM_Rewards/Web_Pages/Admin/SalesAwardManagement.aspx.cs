﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using System.Data.SqlClient;
using System.Data;
using MM.DAL;
using BE_Rewards;
using BL_Rewards;
using BL_Rewards.BL_Common;
using System.IO;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class SalesAwardTypeManagement : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BL_SalesAwardTypeManagement objBL_SalesAwardTypeManagement = new BL_SalesAwardTypeManagement();
        BE_SalesAwardTypeManagementColl objBE_SalesAwardTypeManagementColl = new BE_SalesAwardTypeManagementColl();
        int _SaveStatus = 0;

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //to check page acess.
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();
                if (!IsPostBack)
                {
                    bindgrid(); //to populate grdAwardList Control.
                    FillDropdown();
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesAwardType.aspx - Page_Load");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                Response.Redirect(Request.RawUrl);

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Sales_Award_Management.aspx");
            }
        }

        public void FillDropdown()
        {
            //Division 
            objBE_SalesAwardTypeManagementColl = objBL_SalesAwardTypeManagement.GetDivision();
            ddlDivision.DataSource = objBE_SalesAwardTypeManagementColl;
            ddlDivision.DataTextField = "DivisionName";
            ddlDivision.DataValueField = "DivisionId";
            ddlDivision.DataBind();
            ddlDivision.Items.Insert(0, "Select");

            //Award Type
            objBE_SalesAwardTypeManagementColl = objBL_SalesAwardTypeManagement.GetSalesAwardType();
            ddlAwardType.DataSource = objBE_SalesAwardTypeManagementColl;
            ddlAwardType.DataTextField = "AwardTypeName";
            ddlAwardType.DataValueField = "AwardTypeId";
            ddlAwardType.DataBind();
            ddlAwardType.Items.Insert(0, "Select");
        }

        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkSelect = (LinkButton)sender;
                GridViewRow row = (GridViewRow)lnkSelect.NamingContainer;
                int ID = Convert.ToInt32(grdAwardList.DataKeys[row.DataItemIndex].Value);
                hdnid.Value = ID.ToString();
                txtAwardName.Text = row.Cells[1].Text;
                ddlAwardType.SelectedValue = row.Cells[2].Text;
                ddlDivision.SelectedValue = row.Cells[4].Text;
                txtAmount.Text = row.Cells[6].Text;
                chkActive.Checked = Convert.ToBoolean(row.Cells[7].Text);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesAwardTypeManagement.aspx - lnkSelect_Click");
            }
        }

        protected void btnsaveAwardname_Click(object sender, EventArgs e)
        {
            try
            {
                BE_SalesAwardTypeManagement objBE_SalesAwardTypeManagement = new BE_SalesAwardTypeManagement();
                if (hdnid.Value == "")
                {
                    objBE_SalesAwardTypeManagement.Id = 0;
                    objBE_SalesAwardTypeManagement.AwardName = txtAwardName.Text.ToString();
                    objBE_SalesAwardTypeManagement.AwardTypeId = Convert.ToInt32(ddlAwardType.SelectedValue);
                    objBE_SalesAwardTypeManagement.DivisionId = Convert.ToInt32(ddlDivision.SelectedValue);
                    objBE_SalesAwardTypeManagement.Amount = Convert.ToDecimal(txtAmount.Text);
                    objBE_SalesAwardTypeManagement.IsActive = Convert.ToBoolean(chkActive.Checked);
                    objBE_SalesAwardTypeManagement.CreatedBy = Session["TokenNumber"].ToString();
                    objBE_SalesAwardTypeManagement.ModifiedBy = Session["TokenNumber"].ToString();
                }
                else
                {
                    objBE_SalesAwardTypeManagement.Id = Convert.ToInt16(hdnid.Value.ToString());
                    hdnid.Value = "";
                    objBE_SalesAwardTypeManagement.AwardName = txtAwardName.Text.ToString();
                    objBE_SalesAwardTypeManagement.AwardTypeId = Convert.ToInt32(ddlAwardType.SelectedValue);
                    objBE_SalesAwardTypeManagement.DivisionId = Convert.ToInt32(ddlDivision.SelectedValue);
                    objBE_SalesAwardTypeManagement.Amount = Convert.ToDecimal(txtAmount.Text);
                    objBE_SalesAwardTypeManagement.IsActive = Convert.ToBoolean(chkActive.Checked);
                    objBE_SalesAwardTypeManagement.CreatedBy = Session["TokenNumber"].ToString();
                    objBE_SalesAwardTypeManagement.ModifiedBy = Session["TokenNumber"].ToString();
                }
                _SaveStatus = objBL_SalesAwardTypeManagement.SaveData(objBE_SalesAwardTypeManagement, out _SaveStatus);

                if (_SaveStatus == 0)
                {
                    Session["objBE_SalesAwardTypeManagementColl"] = null;
                    ClearControls();
                    ThrowAlertMessage(MM_Messages.DataSaved);
                    bindgrid();
                    //grdAwardList.SelectedIndex = -1;
                }
                else
                {
                    ThrowAlertMessage("Award Name already existing.");
                }

                // added to refresh page
                // Response.Redirect("~/Web_Pages/Admin/SalesAwardManagement.aspx", false);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesAwardTypeManagement.aspx - btnsaveAwardname_Click");
            }

        }

        private void ClearControls()
        {
            ddlDivision.SelectedIndex = 0;
            ddlAwardType.SelectedIndex = 0;
            txtAmount.Text = "";
            txtAwardName.Text = "";
            chkActive.Checked = true;
            hdnAwardTypeid.Value = "";
            hdnDivisionId.Value = "";
        }

        protected void grdAwardList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow row = (GridViewRow)e.Row;
                    LinkButton lnkSelect = (LinkButton)row.FindControl("lnkSelect");
                    int ID = Convert.ToInt32(grdAwardList.DataKeys[row.DataItemIndex].Value);
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesAwardTypeManagement.aspx");
            }
        }

        #endregion

        #region CommonFunction

        //throw an alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        //to bind girdview control
        private void bindgrid()
        {
            objBE_SalesAwardTypeManagementColl = objBL_SalesAwardTypeManagement.GetData();
            grdAwardList.SelectedIndex = -1;
            grdAwardList.DataSource = objBE_SalesAwardTypeManagementColl;
            grdAwardList.DataBind();
            grdAwardList.Columns[2].Visible = false;
            grdAwardList.Columns[4].Visible = false;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BE_Rewards;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using MM.DAL;
using System.Text;


namespace MM_Rewards.Web_Pages.Admin
{
    public partial class HolidayList : System.Web.UI.Page
    {
        int id = 0;
        int _SaveStatus = 0;
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BL_HolidayManagement objBL_HolidayManagement = new BL_HolidayManagement();
        BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
        BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();

        protected void Page_Load(object sender, EventArgs e)
        {
            BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
            if (!Page.IsPostBack)
            {

                string sourcePath = Server.MapPath(@"~/Uploads/");
                string targetPath = Server.MapPath(@"~/UploadsTest/");

                string fileName = "1130201163246PM_EmpData.xlsx";
               // string sourcePath = @"C:\Users\Public\TestFolder";
               // string targetPath = @"C:\Users\Public\TestFolder\SubDir";

                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                string destFile = System.IO.Path.Combine(targetPath, fileName);

                // To copy a folder's contents to a new location:
                // Create a new target folder, if necessary.
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, destFile, true);

                // To copy all the files in one directory to another directory.
                // Get the files in the source folder. (To recursively iterate through
                // all subfolders under the current directory, see
                // "How to: Iterate Through a Directory Tree.")
                // Note: Check for target path was performed previously
                //       in this code example.
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] files = System.IO.Directory.GetFiles(sourcePath);

                    // Copy the files and overwrite destination files if they already exist.
                    foreach (string s in files)
                    {
                        // Use static Path methods to extract only the file name from the path.
                        fileName = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + "_" + System.IO.Path.GetFileName(s);
                        destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }
                else
                {
                   // Console.WriteLine("Source path does not exist!");
                }

                //Common.CheckPageAccess();
                //try
                //{
                //    getdropdown();
                //    objBE_HolidayManagementColl = objBL_HolidayManagement.GetHolidayList("");
                //    bindgrid();
                //}
                //catch (Exception ex)
                //{
                //    ThrowAlertMessage(MM_Messages.DatabaseError);
                //    objExceptionLogging.LogErrorMessage(ex, "HolidayList.aspx");
                //}
            }

        }

        //to save data 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                if (hdnid.Value == "")
                    objBE_HolidayManagement.ID = 0;

                else
                {
                    objBE_HolidayManagement.ID = Convert.ToInt16(hdnid.Value.ToString());
                    hdnid.Value = "";
                }

                StringBuilder strLocations = new StringBuilder();
                int LocCount = 0;
                foreach (ListItem item in chklLocation.Items)
                {
                    if (item.Selected)
                    {
                        strLocations.Append(item.Value + ",");
                        LocCount++;
                    }
                }

                if (strLocations.Length > 0)
                {
                    strLocations.Remove(strLocations.Length - 1, 1);
                }

                objBE_HolidayManagement.Locationcode = strLocations.ToString();
                objBE_HolidayManagement.Calendarname = txtCalendarName.Text.Trim();
                objBE_HolidayManagement.NoOfLocations = LocCount;
                objBE_HolidayManagement.Sattype = Convert.ToInt16(rdbholidaytype.SelectedValue.ToString());
                objBE_HolidayManagement.Holidayname = txtHolidayName.Text.Trim();
                objBE_HolidayManagement.Holidaydate = dtholidaydate.SelectedDate.Value.ToString("dd-MMM-yyyy hh:mm tt");
                objBE_HolidayManagement.Createdby = Session["TokenNumber"].ToString();
                objBE_HolidayManagement.Modifiedby = Session["TokenNumber"].ToString();

                //_SaveStatus= objBL_HolidayManagement.saveData(objBE_HolidayManagement,out _SaveStatus);
                _SaveStatus = objBL_HolidayManagement.SaveHoliday(objBE_HolidayManagement, out _SaveStatus);


                if (_SaveStatus > 1)
                {
                    ThrowAlertMessage(MM_Messages.DataSaved);
                    //  objBE_HolidayManagementColl = objBL_HolidayManagement.getHolidayList(ddlLocation.SelectedValue.ToString());
                    bindgrid();
                    clearSceen();
                }
                else
                {
                    ThrowAlertMessage("Holiday Name already exist for this Location code.");
                }
                //  ddlLocation.SelectedItem.Selected =true;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "HolidayList.aspx");
            }
        }

        //to clear screen
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                clearSceen();
                clearseletion();
                grdHolidayList.Visible = false;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "HolidayList.aspx");
            }
        }

        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = sender as LinkButton;
                GridViewRow item = btn.NamingContainer as GridViewRow;
                int ID = Convert.ToInt32(grdHolidayList.DataKeys[item.DataItemIndex].Values[0]);
                string LocationCode = Convert.ToString(grdHolidayList.DataKeys[item.DataItemIndex].Values[1]);

                hdnid.Value = ID.ToString();
                chklLocation.ClearSelection();

                objBE_HolidayManagementColl = objBL_HolidayManagement.GetHolidayList("");
                objBE_HolidayManagement = objBE_HolidayManagementColl.Find(delegate(BE_HolidayManagement HM) { return HM.ID == ID; });

                txtCalendarName.Text = objBE_HolidayManagement.Calendarname;
                txtHolidayName.Text = objBE_HolidayManagement.Holidayname;
                dtholidaydate.SelectedDate = Convert.ToDateTime(objBE_HolidayManagement.Holidaydate.ToString());
                rdbholidaytype.SelectedValue = objBE_HolidayManagement.Sattype.ToString();
                foreach (string str in objBE_HolidayManagement.LocationCodeColl)
                {
                    ListItem ListItem = (ListItem)chklLocation.Items.FindByValue(str);
                    if (ListItem != null)
                        ListItem.Selected = true;
                }
            }
            catch (Exception ex)
            {

            }

        }

        //to populate Gridview
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //   objBE_HolidayManagementColl = objBL_HolidayManagement.getHolidayList(ddlLocation.SelectedValue.ToString());


                if (objBE_HolidayManagementColl.Count == 0)
                {
                    clearSceen();
                    clearseletion();
                    grdHolidayList.Visible = true;
                    bindgrid();
                    //     tblholidayedit.Visible = true;
                }
                else
                {
                    clearSceen();
                    clearseletion();
                    grdHolidayList.Visible = true;
                    bindgrid();
                    rdbholidaytype.SelectedValue = objBE_HolidayManagementColl[0].Sattype.ToString();
                    //     tblholidayedit.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "HolidayList.aspx");
            }
        }

        //to get dropdown item
        public void getdropdown()
        {

            BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
            BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
            objBE_HolidayManagementColl = objBL_HolidayManagement.Getdropdownitem();
            for (int i = 0; i < objBE_HolidayManagementColl.Count; i++)
            {
               // objBE_HolidayManagementColl[i].Locationname = objBE_HolidayManagementColl[i].Locationname + "  " + "(" + objBE_HolidayManagementColl[i].Locationcode + ")";

                chklLocation.DataTextField = "Locationname";
                chklLocation.DataValueField = "Locationcode";
                chklLocation.DataSource = objBE_HolidayManagementColl;
                chklLocation.DataBind();
            }

        }

        //to select Holiday
        protected void grdHolidayList_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            try
            {
                //if (e.CommandName == "Select")
                //{

                //    BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                //    hdnid.Value = e.CommandArgument.ToString();
                //    id = Convert.ToInt16(e.CommandArgument.ToString());
                //    BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                //    // objBE_HolidayManagementColl = objBL_HolidayManagement.getHolidayList(ddlLocation.SelectedValue.ToString());
                //    objBE_HolidayManagement = objBE_HolidayManagementColl.Find(delegate(BE_HolidayManagement HM) { return HM.Id == id; });
                //    txtHolidayName.Text = objBE_HolidayManagement.Holidayname;
                //    dtholidaydate.SelectedDate = Convert.ToDateTime(objBE_HolidayManagement.Holidaydate.ToString());
                //    rdbholidaytype.SelectedValue = objBE_HolidayManagement.Sattype.ToString();
                //}
                if (e.CommandName == "Delete")
                {
                    id = Convert.ToInt16(e.CommandArgument.ToString());
                    _SaveStatus = objBL_HolidayManagement.DeleteData(id);

                    if (_SaveStatus > 0)
                    {

                        ThrowAlertMessage("Data Deleted Sucessfully");
                        //    objBE_HolidayManagementColl = objBL_HolidayManagement.getHolidayList(ddlLocation.SelectedValue.ToString());
                        grdHolidayList.DataSource = objBE_HolidayManagementColl;
                        grdHolidayList.DataBind();

                    }
                    else
                    {
                        ThrowAlertMessage(MM_Messages.DatabaseError);
                    }

                }
            }

            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "HolidayList.aspx");
            }
        }

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        //to clear screen
        private void clearSceen()
        {
            txtHolidayName.Text = "";
            txtCalendarName.Text = "";
            dtholidaydate.Clear();
        }

        //to bind datagrid
        private void bindgrid()
        {
            grdHolidayList.SelectedIndex = -1;
            grdHolidayList.DataSource = objBE_HolidayManagementColl;
            grdHolidayList.DataBind();


        }

        //to delete holiday
        protected void grdHolidayList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            clearSceen();
            // clearseletion();

        }

        //to clear radiobutton selection
        private void clearseletion()
        {
            rdbholidaytype.ClearSelection();
            rdbholidaytype.Items[0].Selected = true;
            chklLocation.ClearSelection();
        }

        /// <summary>
        /// To  Bind the grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder strLocations = new StringBuilder();
                foreach (ListItem item in chklLocation.Items)
                {
                    if (item.Selected)
                    {
                        strLocations.Append(item.Value + ",");
                    }
                }

                if (strLocations.Length > 0)
                {
                    strLocations.Remove(strLocations.Length - 1, 1);
                }
                objBE_HolidayManagementColl = objBL_HolidayManagement.GetHolidayList(strLocations.ToString());

                if (objBE_HolidayManagementColl.Count == 0)
                {
                    clearSceen();
                    clearseletion();
                    grdHolidayList.Visible = true;
                    bindgrid();
                }
                else
                {
                    clearSceen();
                    clearseletion();
                    grdHolidayList.Visible = true;
                    bindgrid();
                    rdbholidaytype.SelectedValue = objBE_HolidayManagementColl[0].Sattype.ToString();
                }

            }
            catch (Exception ex)
            {

            }
        }

       
    }

}
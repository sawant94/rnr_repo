﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using System.Collections;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class RnrMangement : System.Web.UI.Page
    {
        BE_RNRUserManagement objBE_RNRUserManagement = new BE_RNRUserManagement();
        BE_RNRUserManagementColl objBE_RNRUserManagementColl = new BE_RNRUserManagementColl();
        BL_RNRUserManagement objBL_RNRUserManagement = new BL_RNRUserManagement();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        Common objCommon = new Common();
        int _SaveStatus;

        #region Methods

        // to clear selected checkbox
        private void ClearCheckBox(Repeater objrepater, string id)
        {
            foreach (RepeaterItem item in objrepater.Controls)
            {
                CheckBox chkselected = item.FindControl(id) as CheckBox;
                if (chkselected != null && chkselected.Checked)
                {
                    chkselected.Checked = false;
                }
            }
        }
        //throw a  java script alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }
        // to set visiblity of panel
        private void PanelVisible(bool businessUnit, bool division, bool location,bool save=true)
        {
            pnlBusinessUnit.Visible = businessUnit;
            pnldivision.Visible = division;
            pnllocation.Visible = location;
          
            //pnldepartment.Visible = department;
            pnlsave.Visible = save;
        }
        //Dyanamic css apply to selected button
        private void ApplyActiveCss(HtmlGenericControl div, Button btn, HtmlGenericControl div1, Button btn1, HtmlGenericControl div2, Button btn2,bool css=true)
        {
            if (css)
            {
                div.Attributes.Add("Class", "btnActive");
                btn.CssClass = "btnActiveLeft";
            }
            else
            {
                div.Attributes.Add("Class", "btnRed");
                btn.CssClass = "btnLeft";
            }
            div1.Attributes.Add("Class", "btnRed");
            btn1.CssClass = "btnLeft";
            div2.Attributes.Add("Class", "btnRed");
            btn2.CssClass = "btnLeft";

        }
   
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
          
            try
            {
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();//to check page acess for user.
             
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
      
        }
        //to get a rnr user details .
        protected void btnGetRNR_Click(object sender, EventArgs e)
        {
            try
            {
                objBE_RNRUserManagementColl = objBL_RNRUserManagement.GetDetailsByTokenNumber(txtSearchBy.Text);
                grdRnrUser.DataSource = objBE_RNRUserManagementColl;
                grdRnrUser.DataBind();
                if (objBE_RNRUserManagementColl.Count > 0)
                {
                    pnldetails.Visible = true;
                    PanelVisible(false, false, false,false);
                    //ApplyActiveCss(divbtndivision, btngetdivision, divbtnlocation, btngetlocation, divbtnDepartment, btngetdepartment, false);
                    ApplyActiveCss(divbtnBusinessUnit, btngetBusinessUnit, divbtndivision, btngetdivision, divbtnlocation, btngetlocation, false);
                }
                else
                {
                    ThrowAlertMessage("Invalid Token number");
                    pnldetails.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
      
        }        
        protected void grdRnrUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
        }
        protected void grdRnrUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
        }

        // to get BusinessUnit for rnr user by token number
        protected void btngetBusinessUnit_Click(object sender, EventArgs e)
        {

            try
            {
                //display visbile panel
                PanelVisible(true, false, false);
                //apply highlighting css to button. 
                //ApplyActiveCss(divbtndivision, btngetdivision, divbtnlocation, btngetlocation, divbtnDepartment, btngetdepartment);
                ApplyActiveCss(divbtnBusinessUnit, btngetBusinessUnit, divbtndivision, btngetdivision, divbtnlocation, btngetlocation);
                objBE_RNRUserManagementColl = objBL_RNRUserManagement.GetBusinessUnit(txtSearchBy.Text);
                lstBusinessUnit.DataSource = objBE_RNRUserManagementColl;
                lstBusinessUnit.DataBind();
                //used to comapre while saving Division
                hdnselectedpanel.Value = "BU";
                // pnldivision.Visible = true;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
        }
        // to get division for rnr user by token number
        protected void btngetdivision_Click(object sender, EventArgs e)
        {
            try
            {   
                //display visbile panel
                PanelVisible( false,true, false);
                //apply highlighting css to button. 
                //ApplyActiveCss(divbtndivision, btngetdivision, divbtnlocation, btngetlocation, divbtnDepartment, btngetdepartment);
                ApplyActiveCss(divbtndivision, btngetdivision, divbtnlocation, btngetlocation,divbtnBusinessUnit, btngetBusinessUnit);
                objBE_RNRUserManagementColl = objBL_RNRUserManagement.GetDivision(txtSearchBy.Text);
                lstDivision.DataSource = objBE_RNRUserManagementColl;
                lstDivision.DataBind();
                //used to comapre while saving Division
                hdnselectedpanel.Value = "DIV";
               // pnldivision.Visible = true;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
        }
        // to get location as per division
        protected void btngetlocation_Click(object sender, EventArgs e)
        {
            try
            {   
                //display visbile panel
                PanelVisible(false, false, true);
                //apply highlighting css to button. 
               // ApplyActiveCss(divbtnlocation, btngetlocation, divbtnDepartment, btngetdepartment, divbtndivision, btngetdivision);
                ApplyActiveCss(divbtnlocation, btngetlocation, divbtndivision, btngetdivision,divbtnBusinessUnit, btngetBusinessUnit);
                objBE_RNRUserManagementColl = objBL_RNRUserManagement.GetLocation(txtSearchBy.Text);
                lstlocation.DataSource = objBE_RNRUserManagementColl;
                lstlocation.DataBind();
                //used to comapre while saving locataion
                hdnselectedpanel.Value = "LOC";
               // pnldivision.Visible = true;

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
        }
        //to get department as per division and location
        //protected void btngetdepartment_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //display visbile panel
        //        PanelVisible(false, false, true);
        //        //apply highlighting css to button. 
        //        //ApplyActiveCss(divbtnDepartment, btngetdepartment, divbtnlocation,btngetlocation,divbtndivision, btngetdivision);
        //        ApplyActiveCss(divbtnBusinessUnit, btngetBusinessUnit, divbtnlocation, btngetlocation, divbtndivision, btngetdivision);
        //        objBE_RNRUserManagementColl = objBL_RNRUserManagement.GetDepartment(txtSearchBy.Text);
        //        lstdepartment.DataSource = objBE_RNRUserManagementColl;
        //        lstdepartment.DataBind();
        //        //used to comapre while saving Department
        //        hdnselectedpanel.Value = "DPT";
        //        //pnldivision.Visible = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        ThrowAlertMessage(MM_Messages.DatabaseError);
        //        objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
        //    }
        //}
        // to save division,location and department by comparing hdnselectedpanel
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try

            {
                string currentuser = Session["TokenNumber"].ToString();
                //vanda
                if (hdnselectedpanel.Value == "BU")//to save BusinessUnit
                {
                    objBE_RNRUserManagement.TokenNumber = txtSearchBy.Text;
                    objBE_RNRUserManagement.CreatedBy = currentuser;
                    foreach (RepeaterItem item in lstBusinessUnit.Controls)
                    {
                        CheckBox chkselected = item.FindControl("chkBusinessUnit") as CheckBox;
                    //    Label lblBusinessUnit = item.FindControl("lblBusinessUnit") as Label;

                        if (chkselected != null && chkselected.Checked)
                        {
                            objBE_RNRUserManagement.BUnit.Add(chkselected.Text.ToString());
                        }
                    }

                    _SaveStatus = objBL_RNRUserManagement.SaveBusinessUnit(objBE_RNRUserManagement);

                    if (_SaveStatus == 0)
                    {
                        ThrowAlertMessage(MM_Messages.DataSaved);
                        ClearCheckBox(lstBusinessUnit, "chkBusinessUnit");//clear division
                    }
                    else
                    {
                        ThrowAlertMessage(MM_Messages.DataSaved);
                    }

                }
                // vanda

                if (hdnselectedpanel.Value == "DIV")//to save division
                {
                    objBE_RNRUserManagement.TokenNumber = txtSearchBy.Text;
                    objBE_RNRUserManagement.CreatedBy = currentuser;
                    foreach (RepeaterItem item in lstDivision.Controls)
                    {
                        CheckBox chkselected = item.FindControl("chkDivision") as CheckBox;
                        Label lbldivisioncode = item.FindControl("lbldivisioncode") as Label;

                        if (chkselected != null && chkselected.Checked)
                        {
                            objBE_RNRUserManagement.Division.Add(lbldivisioncode.Text);
                        }
                    }

                    _SaveStatus = objBL_RNRUserManagement.SaveDivision(objBE_RNRUserManagement);

                    if (_SaveStatus == 0)
                    {
                        ThrowAlertMessage(MM_Messages.DataSaved);
                        ClearCheckBox(lstDivision, "chkDivision");//clear division
                    }
                    else
                    {
                        ThrowAlertMessage(MM_Messages.DataSaved);
                    }

                }
                else if (hdnselectedpanel.Value == "LOC")//To save Location
                {
                    objBE_RNRUserManagement.TokenNumber = txtSearchBy.Text;
                    objBE_RNRUserManagement.CreatedBy = currentuser;
                    foreach (RepeaterItem item in lstlocation.Controls)
                    {
                        CheckBox chkselected = item.FindControl("chklocation") as CheckBox;
                        Label lbllocationcode = item.FindControl("lbllocationcode") as Label;

                        if (chkselected != null && chkselected.Checked)
                        {
                            objBE_RNRUserManagement.Location.Add(lbllocationcode.Text);
                        }
                    }

                    _SaveStatus = objBL_RNRUserManagement.SaveLocation(objBE_RNRUserManagement);

                    if (_SaveStatus == 0)
                    {
                        ThrowAlertMessage(MM_Messages.DataSaved);
                        ClearCheckBox(lstlocation, "chklocation");//clear location
                    }
                    else
                    {
                        ThrowAlertMessage(MM_Messages.DataSaved);
                    }
                }
                //else if (hdnselectedpanel.Value == "DPT") //to save Department
                //{
                //    objBE_RNRUserManagement.TokenNumber = txtSearchBy.Text;
                //    objBE_RNRUserManagement.CreatedBy = currentuser;
                //    foreach (RepeaterItem item in lstdepartment.Controls)
                //    {
                //        CheckBox chkselected = item.FindControl("chkdepartment") as CheckBox;
                //        Label lbldepartmentcode = item.FindControl("lbldepartmentcode") as Label;
                        
                //        if (chkselected != null && chkselected.Checked)
                //        {
                //            objBE_RNRUserManagement.Orgunit.Add(lbldepartmentcode.Text);
                //        }
                //    }

                //    _SaveStatus = objBL_RNRUserManagement.SaveDepartment(objBE_RNRUserManagement);

                //    if (_SaveStatus == 0)
                //    {
                //        ThrowAlertMessage(MM_Messages.DataSaved);
                //        ClearCheckBox(lstdepartment, "chkdepartment");//clear department
                //    }
                //    else
                //    {
                //        ThrowAlertMessage(MM_Messages.DataSaved);
                //    }
                //}
               // ApplyActiveCss(divbtndivision, btngetdivision, divbtnlocation, btngetlocation, divbtnDepartment, btngetdepartment, false);
                PanelVisible(false, false, false, false);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RnrMangement.aspx");
            }
        }
        #endregion
 
     

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using Telerik.Web.UI;
using System.Data;
using BE_Rewards;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using System.IO;
using System.Text;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class Budget_Allocation : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BE_BudgetAllocation objBE_BudgetAllocation;
        BE_BudgetAllocationColl objBE_BudgetAllocationColl;
        BL_BudgetAllocation objBL_BudgetAllocation;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();  // check if the loged in user has access to this page,if not redirect him to home page

                if (!IsPostBack)
                {

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn());

                    grdRecipient.DataSource = dt;
                    grdRecipient.DataBind();

                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }
        }

        /// <summary>
        /// Gets CEOs matching the search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGetCEOs_Click(object sender, EventArgs e)
        {
            try
            {
                objBL_BudgetAllocation = new BL_BudgetAllocation();
                objBE_BudgetAllocation = new BE_BudgetAllocation();
                objBE_BudgetAllocationColl = new BE_BudgetAllocationColl();

                objBE_BudgetAllocation.SearchBy = txtSearchCEOs.Text;
                objBE_BudgetAllocation.CheckButtonClick = 1;

                if (rdCEHOD.SelectedValue == "1")
                {
                    objBE_BudgetAllocation.IsCEO = true;
                }
                else if (rdCEHOD.SelectedValue == "0")
                {
                    objBE_BudgetAllocation.IsCEO = false;
                }

                objBE_BudgetAllocationColl = objBL_BudgetAllocation.GetData(objBE_BudgetAllocation);

                grdCEO.DataSource = objBE_BudgetAllocationColl;
                grdCEO.DataBind();
                lblCEO.Visible = true;
                grdCEO.Visible = true;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }
        }

        /// <summary>
        /// Gets HODs matching the search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGetRecipients_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = grdCEO.Rows[Convert.ToInt32(hdnSelectedRowIndex.Value)];
                row.CssClass = Convert.ToInt32(hdnSelectedRowIndex.Value) % 2 == 0 ? "selectedoddTd" : "selectedevenTd";
                string str = hdnTotal.Value;// lblTotal.Text;
                pnlRecipient.Style.Add("display", "block");

                objBL_BudgetAllocation = new BL_BudgetAllocation();
                objBE_BudgetAllocation = new BE_BudgetAllocation();
                objBE_BudgetAllocationColl = new BE_BudgetAllocationColl();

                objBE_BudgetAllocation.SearchBy = txtSearchRecipients.Text;
                objBE_BudgetAllocation.TokenNumber = hdnTokenNumber.Value;
                objBE_BudgetAllocation.IsCEO = false;
                objBE_BudgetAllocation.CheckButtonClick = 2;

                objBE_BudgetAllocationColl = objBL_BudgetAllocation.GetData(objBE_BudgetAllocation);

                grdRecipient.DataSource = objBE_BudgetAllocationColl;
                grdRecipient.DataBind();
                lblTotal.Text = str;

                pnlGrid.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objBL_BudgetAllocation = new BL_BudgetAllocation();

                objBE_BudgetAllocationColl = new BE_BudgetAllocationColl();
                bool checkAtleastOne = false;
                bool checkCredit = true, checkEmpty = true;
                string strAlertMsg = "";

                foreach (GridViewRow item in grdRecipient.Rows)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chkSelect");
                    if (chk.Checked)
                    {
                        string Name = item.Cells[2].Text;
                        TextBox txtBudget = (TextBox)item.FindControl("txtBudget");
                        RadDatePicker dpStartDate = (RadDatePicker)item.FindControl("dpStartDate");
                        RadDatePicker dpEndDate = (RadDatePicker)item.FindControl("dpEndDate");

                        if (lblTotal.Text != "" && Convert.ToDecimal(lblTotal.Text) < 0)
                        {
                            checkCredit = false;
                            strAlertMsg += "Insufficient Credit! ";
                            EnableControls(txtBudget, dpStartDate, dpEndDate, item);
                            //item.Style.Add("backgroundColor","#FFFFCC");
                            //item.Style.Add("class", "selectedoddTd");

                            //item.CssClass = "selectedoddTd";
                        }
                        else
                        {
                            checkAtleastOne = true;
                            //string Name = item.Cells[2].Text;
                            //TextBox txtBudget = (TextBox)item.FindControl("txtBudget");
                            //RadDatePicker dpStartDate = (RadDatePicker)item.FindControl("dpStartDate");
                            //RadDatePicker dpEndDate = (RadDatePicker)item.FindControl("dpEndDate");

                            if (dpStartDate.SelectedDate != null && dpEndDate.SelectedDate != null && txtBudget.Text != "")
                            {
                                if (dpStartDate.SelectedDate < dpEndDate.SelectedDate)
                                {
                                    checkEmpty = false; ;
                                    objBE_BudgetAllocation = new BE_BudgetAllocation();
                                    objBE_BudgetAllocation.TokenNumber = item.Cells[1].Text.ToString();
                                    objBE_BudgetAllocation.Name = item.Cells[2].Text;
                                    objBE_BudgetAllocation.EMailID = grdRecipient.DataKeys[item.RowIndex].Value.ToString();
                                    objBE_BudgetAllocation.AllocationType = rdCEHOD.SelectedValue == "1" ? "C" : "H";
                                    objBE_BudgetAllocation.AllocationFrom = hdnTokenNumber.Value;
                                    objBE_BudgetAllocation.StartDate = dpStartDate.SelectedDate;
                                    objBE_BudgetAllocation.EndDate = dpEndDate.SelectedDate;
                                    objBE_BudgetAllocation.Budget = Convert.ToDecimal(txtBudget.Text);

                                    if (Session["TokenNumber"] != null)
                                        objBE_BudgetAllocation.CreatedBy = Session["TokenNumber"].ToString();
                                    else // hardcoded
                                        objBE_BudgetAllocation.CreatedBy = "TokenID-1";

                                    objBE_BudgetAllocation.CreatedDate = DateTime.Now;
                                    objBE_BudgetAllocationColl.Add(objBE_BudgetAllocation);
                                    lblTotal.Text = (Convert.ToDecimal(lblTotal.Text) - Convert.ToDecimal(txtBudget.Text)).ToString();
                                    ClearControls(txtBudget, dpStartDate, dpEndDate, chk, item);
                                }
                                else
                                {
                                    strAlertMsg += Name + " : End Date has to greater than start date! \\n";
                                    EnableControls(txtBudget, dpStartDate, dpEndDate, item);
                                    item.Style.Add("class", "selectedoddTd");
                                    //item.Style.Add("backgroundColor", "#FFFFCC");
                                }
                            }
                            else
                            {
                                //checkEmpty = false;
                                strAlertMsg += Name + " : All the fields are mandatory! \\n";
                                string str = "<script language = 'javascript'>alert('" + Name + " : All the fields are mandatory!')</script>";
                                EnableControls(txtBudget, dpStartDate, dpEndDate, item);
                                item.Style.Add("class", "selectedoddTd");
                            }
                        }
                    }

                }

                if (!checkAtleastOne)
                {
                    strAlertMsg += "Please Select atleast one recipient! \\n ";
                }
                else
                {
                    if (checkCredit && (!checkEmpty) && objBE_BudgetAllocationColl.Count > 0)
                    {
                        //objBE_BudgetAllocation = new BE_BudgetAllocation();
                        objBL_BudgetAllocation.SaveData(objBE_BudgetAllocationColl);


                        //=============== Code for sending Mail to the recipient ==============//
                        string BodyPart = "";
                        BE_MailDetails objMailDetails = new BE_MailDetails();
                        Common objCommon = new Common();

                        StreamReader sr = new StreamReader(Server.MapPath("~/MailDetails/BudgetAllocation.txt"));
                        StringBuilder sb = new StringBuilder();
                        BodyPart = sr.ReadToEnd();

                        foreach (BE_BudgetAllocation objBE_BudgetAllocation in objBE_BudgetAllocationColl)
                        {
                            objMailDetails.FirstName = objBE_BudgetAllocation.Name;
                            BodyPart = BodyPart.Replace("<% Amount %>", objBE_BudgetAllocation.Budget.ToString());

                            //objCommon.sendMail(
                            //                    "R&R@Mahindra.com",//hdnCEOEMailID.Value,                     //objMailDetails.Fromid,
                            //                    "",                     //objMailDetails.Fromname,
                            //                    objBE_BudgetAllocation.EMailID,
                            //                    "",
                            //                    "",                     //objMailDetails.Ccid,
                            //                    "Budget Allocated!",
                            //                    BodyPart);
                        }

                        //====================================================================//

                        hdnTotal.Value = lblTotal.Text;
                        btnGetCEOs_Click(btnGetCEOs, e);
                        if (strAlertMsg == "")
                        {
                            pnlGrid.Style.Add("display", "none");
                        }
                    }
                }

                if (strAlertMsg != "")
                {
                    string str = "<script language = 'javascript'>alert('" + strAlertMsg + "');</script>";
                    if (!ClientScript.IsClientScriptBlockRegistered("Script"))
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "Script", str);
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }
        }

        private void EnableControls(TextBox txtBudget, RadDatePicker dpStartDate, RadDatePicker dpEndDate, GridViewRow item)
        {
            txtBudget.Enabled = true;
            dpStartDate.Enabled = true;
            dpEndDate.Enabled = true;
            //item.Style.Add("background-color", "#FFFFCC");
            //item.Style.Add("class", "selectedoddTd");
            item.CssClass = "selectedoddTd";
        }

        private void ClearControls(TextBox txtBudget, RadDatePicker dpStartDate, RadDatePicker dpEndDate, CheckBox chk, GridViewRow item)
        {
            txtBudget.Text = "";
            dpStartDate.Clear();
            dpEndDate.Clear();
            chk.Checked = false;
            item.CssClass = item.RowIndex % 2 == 0 ? "" : "row01";
        }

        protected void grdRecipient_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow row = (GridViewRow)e.Row;

                    CheckBox chk = (CheckBox)row.FindControl("chkSelect");
                    TextBox txtBudget = (TextBox)row.FindControl("txtBudget");
                    RadDatePicker dpStartDate = (RadDatePicker)row.FindControl("dpStartDate");
                    RadDatePicker dpEndDate = (RadDatePicker)row.FindControl("dpEndDate");
                    HiddenField hdnBudget = (HiddenField)row.FindControl("hdnBudget");

                    dpStartDate.MinDate = DateTime.Today;
                    dpEndDate.MinDate = DateTime.Today.AddDays(1);

                    chk.Attributes.Add("onclick", "SwitchControls('" + chk.ClientID + "','" + hdnBudget.ClientID + "','" + txtBudget.ClientID + "','" + dpStartDate.ClientID + "','" + dpEndDate.ClientID + "','" + row.RowIndex + "',event);");
                    txtBudget.Attributes.Add("onblur", "txtBudget_onblur('" + hdnBudget.ClientID + "','" + txtBudget.ClientID + "');");
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }

        }

        protected void grdRecipient_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRecipient.PageIndex = e.NewPageIndex;

                btnGetRecipients_Click(btnGetRecipients, e);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Budget_Allocation.aspx");
            }

        }

        #region Exception Handling

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        #endregion

        protected void rdCEHOD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdCEHOD.SelectedValue == "0")
            {
                lblSearchCEOs.Text = "Enter HOD TokenNumber/Name";
                lblCEO.Text = "For HOD";
                lblCEO.Visible = false;
                grdCEO.Visible = false;
            }
            else if (rdCEHOD.SelectedValue == "1")
            {
                lblSearchCEOs.Text = "Enter CE TokenNumber/Name";
                lblCEO.Text = "For CE";
                lblCEO.Visible = false;
                grdCEO.Visible = false;
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Rewards;
namespace MM_Rewards.Web_Pages.Admin
{
    public partial class QuarterAwardBudget : System.Web.UI.Page
    {
        BL_Nomination objBL_Nomination = new BL_Nomination();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BL_QuarterAwardBudget objBL_QuarterAwardBudget = new BL_QuarterAwardBudget();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAFLCMember();
                CheckFYData();
            }
        }
        public void CheckFYData()
        {
            YearData obj = objBL_Nomination.GetFYData();
            txtStartDate.Text = obj.StartDate;
            txtEndDate.Text = obj.EndDate;
            ddlQuarter.SelectedValue = obj.Quarter.ToString();
        }
        public void BindAFLCMember()
        {
            DataSet ds = objBL_QuarterAwardBudget.GetAFLCMember();
            ddlALFCMember.DataSource = ds.Tables[0];
            ddlALFCMember.DataValueField = "AFLCTokenNumber";
            ddlALFCMember.DataTextField = "AFLCName";
            ddlALFCMember.DataBind();
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            BE_QuarterAwardBudget objBE_QuarterAwardBudget = new BE_QuarterAwardBudget();
            objBE_QuarterAwardBudget.TokenNumber = ddlALFCMember.SelectedValue;
            objBE_QuarterAwardBudget.AllocatedBudget = Convert.ToInt32(txtAllocatedBudget.Text);
            objBE_QuarterAwardBudget.StartDate = Convert.ToDateTime(txtStartDate.Text);
            objBE_QuarterAwardBudget.EndDate=Convert.ToDateTime(txtEndDate.Text);
            objBE_QuarterAwardBudget.UtilizedBudget = 0;
            objBE_QuarterAwardBudget.RemaningBudget = Convert.ToInt32(txtAllocatedBudget.Text);
            objBE_QuarterAwardBudget.BudgetType = txtBudgetType.Text;
            objBE_QuarterAwardBudget.Quarter = Convert.ToInt32(ddlQuarter.SelectedValue);
            objBE_QuarterAwardBudget.CreatedBy=Session["TokenNumber"].ToString();
            objBE_QuarterAwardBudget.IsActive = 1;
            int res=objBL_QuarterAwardBudget.SaveData(objBE_QuarterAwardBudget);
            if (res > 0)
            {
                objBE_QuarterAwardBudget = null;
                Clear();
                ThrowAlertMessage("Budget Allocated Successfully");
            }
            else
            {
                ThrowAlertMessage("Budget Not Saved");
            }
        }

        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Year = DateTime.Now.Year;
            int Month = DateTime.Now.Month;

            txtStartDate.Text = "";
            txtEndDate.Text = "";

            int Quarter= Convert.ToInt32(ddlQuarter.SelectedValue);

            switch (Quarter)
            {
                case 1:
                    {
                        txtStartDate.Text = Year + "-04-01";
                        txtEndDate.Text = Year + "-07-10";
                        break;
                    }
                case 2:
                    {
                        txtStartDate.Text = Year + "-07-11";
                        txtEndDate.Text = Year + "-10-10";
                        break;
                    }
                case 3:
                    {
                        txtStartDate.Text = Year + "-10-11";
                        if (Month <=3)
                        {
                            txtEndDate.Text = Year + "-01-10";
                        }
                        else
                        {
                            txtEndDate.Text = (Year + 1) + "-01-10";
                        }
                            break;
                    }
                case 4:
                    {
                        txtStartDate.Text = Year + "-01-11";
                        txtEndDate.Text = Year + "-03-10";
                        break;
                    }
                default:
                    {
                        txtStartDate.Text = "";
                        txtEndDate.Text = "";
                        break;
                    }
            }           

        }
        public void Clear()
        {
            ddlALFCMember.SelectedIndex = -1;
            ddlQuarter.SelectedIndex = -1;
            txtStartDate.Text = "";
            txtEndDate.Text = "";
            txtAllocatedBudget.Text = "";
            CheckFYData();
        }
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

    }
}
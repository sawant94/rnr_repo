﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Rewards;
using BL_Rewards.Admin;
using System.Data;
using BE_Rewards.BE_Admin;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class QuarterAwardApprover : System.Web.UI.Page
    {
        //BL_QuarterAwardBudget objBL_QuarterAwardBudget = new BL_QuarterAwardBudget();
        BL_QuarterAwardApprover objBL_QuarterAwardApprover = new BL_QuarterAwardApprover();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAFLCMember();

            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        public void BindAFLCMember()
        {
            DataSet ds = objBL_QuarterAwardApprover.GetAFLCMember();
            ddlSubAFLCMember.DataSource = ds.Tables[0];
            ddlSubAFLCMember.DataValueField = "TokenNumber";
            ddlSubAFLCMember.DataTextField = "Name";
            ddlSubAFLCMember.DataBind();

            ddlAFLCMember.DataSource = ds.Tables[0]; 
            ddlAFLCMember.DataValueField = "TokenNumber";
            ddlAFLCMember.DataTextField = "Name";
            ddlAFLCMember.DataBind();

            ddlSubAFLCMember.Items.Insert(0, "Select");
            ddlSubAFLCMember.SelectedIndex = -1;

            ddlAFLCMember.Items.Insert(0, "Select");
            ddlAFLCMember.SelectedIndex = -1;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            BE_QuarterAwardApprover objBE_QuarterAwardApprover = new BE_QuarterAwardApprover();
            objBE_QuarterAwardApprover.SubAFLCTokenNumber = ddlSubAFLCMember.SelectedValue;
            objBE_QuarterAwardApprover.SubAFLCName = ddlSubAFLCMember.SelectedItem.Text;

            objBE_QuarterAwardApprover.AFLCTokenNumber = ddlAFLCMember.SelectedValue;
            objBE_QuarterAwardApprover.AFLCName = ddlAFLCMember.SelectedItem.Text;

            objBE_QuarterAwardApprover.Role = ddlRole.SelectedValue;
            objBE_QuarterAwardApprover.CreatedBy = Session["TokenNumber"].ToString();
            objBE_QuarterAwardApprover.IsActive = 1;
            int res= objBL_QuarterAwardApprover.SaveData(objBE_QuarterAwardApprover);
            if (res > 0)
            {
                objBE_QuarterAwardApprover = null;
                Clear();
                ThrowAlertMessage("AFLC Member added Successfully");
            }
            else
            {
                ThrowAlertMessage("Record not Saved..");
            }
        }


        public void Clear()
        {
            ddlAFLCMember.SelectedIndex = 0;
            ddlRole.SelectedIndex = -1;
            ddlSubAFLCMember.SelectedIndex = 0;
        }
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

    }
}
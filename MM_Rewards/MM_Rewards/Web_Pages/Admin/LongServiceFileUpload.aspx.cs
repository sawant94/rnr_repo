﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_Rewards.Admin;
using System.Text;
using BE_Rewards.BE_Rewards;
using BE_Rewards.BE_Admin;
using BL_Rewards.BL_Common;
using System.Configuration;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class LongServiceFileUpload : System.Web.UI.Page
    {
        BL_ExcelUtility objBL_ExcelUtility = new BL_ExcelUtility();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        protected void Page_Load(object sender, EventArgs e)
        {
            BL_Rewards.BL_Common.Common.CheckUserLoggedIn(true); // Check that the usr is logged in, if not redirect him to homepage
        }

        //upload an excel file
        protected void btnupload_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Server.MapPath(@"~/Uploads/");
                if (flupload.HasFile)
                {
                    // generate a unique file name
                    string filename = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + "_" + flupload.FileName;
                    path += filename;
                    flupload.SaveAs(path);
                    string awardtypename = ConfigurationManager.AppSettings["LongServiceAwardName"];
                    string awardreason = ConfigurationManager.AppSettings["LongServiceReason"];
                    int awardamount = Convert.ToInt32(ConfigurationManager.AppSettings["LongServiceAmount"]);
                    int i=objBL_ExcelUtility.SaveLongService(path, awardtypename, awardreason,awardamount);
                    if (i == 1)
                    {
                        errlbl.Text = "Error in File Upload";
                        errlbl.Visible = true;
                    }
                    else
                    {
                        errlbl.Text = "File Uploaded Successfully";
                        errlbl.Visible = true;

                    }


                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "LongServiceFileUpload.aspx");
            }
        }


        private void ThrowAlertMessage(string strmessage)
        {
            try
            {
                if (strmessage.Contains("'"))
                {
                    strmessage = strmessage.Replace("'", "\\'");
                }
                string script = @"alert('" + strmessage + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "LongServiceFileUpload.aspx");
            }
        }

      
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using System.IO;
using System.Text;
using BL_Rewards.BL_Common;
using BL_Rewards.BL_Common;
namespace MM_Rewards.Web_Pages
{
    public partial class TestimonialApproval : System.Web.UI.Page
    {
        BE_Testimonial objBE_Testimonial = new BE_Testimonial();
        BE_TestimonialColl objBE_Testimonialcoll ;
        BL_Testimonial objBL_Testimonial = new BL_Testimonial();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        Common objCommon = new Common();
      
    
        #region method
        // to clear input controls
        private void clear()
        {
            txtRemark.Text = "";
            txttestimonial.Text = "";
            imguploadedimage.ImageUrl = "";
            rdbstatus.ClearSelection();
            divedit.Visible = false; //hide Editable div that appear after grid view selection
        }
        // function will bind grid to collection of testimonial
        private void BindData()
        {
            objBE_Testimonialcoll = new BE_TestimonialColl();
            objBE_Testimonialcoll = objBL_Testimonial.GetData();
            grdTestimonialList.DataSource = objBE_Testimonialcoll;
            grdTestimonialList.DataBind();
        }
        //function will used for throwing an alert message.
        private void ThrowAlertMessage(string strmessage)
        {
            try
            {
                if (strmessage.Contains("'"))
                {
                    strmessage = strmessage.Replace("'", "\\'");
                }
                string script = @"alert('" + strmessage + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Common.CheckUserLoggedIn();
                Common.CheckPageAccess();
                if (!Page.IsPostBack)
                {
                    BindData();
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "TestimonialApproval.aspx");
            }
        }
        //bind grid while paging
        protected void grdTestimonialList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTestimonialList.PageIndex = e.NewPageIndex;
                BindData();
                grdTestimonialList.SelectedIndex = -1;// to hide selected index while paging
                divedit.Visible = false;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "TestimonialApproval.aspx");
            }
        }
        // to Approve or reject testiomonial
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string currentuser = (Session["TokenNumber"] == null ? "" : Session["TokenNumber"].ToString());
                BE_MailDetails objMailDetails = new BE_MailDetails();
                int _SaveStatus = 0;
                string im = string.Empty;
                objBE_Testimonial.ID = Convert.ToInt16(hdnid.Value);
                objBE_Testimonial.TestimonoialText = txttestimonial.Text;
                objBE_Testimonial.Status = Convert.ToBoolean(rdbstatus.SelectedValue.ToString());
                objBE_Testimonial.ApproverRemarks = txtRemark.Text;

                objBE_Testimonial.CreatedBy = currentuser;
                objBE_Testimonial.ApproveBy = currentuser;
                _SaveStatus = objBL_Testimonial.SaveData(objBE_Testimonial, out im);
                if (_SaveStatus > 0)
                {
                    StreamReader sr = new StreamReader(Server.MapPath("~/MailDetails/Testimonial.txt"));
                    StringBuilder sb = new StringBuilder();
                    string BodyPart = sr.ReadToEnd();
                    objCommon.GetUserDetails(currentuser);
                    objMailDetails = objCommon.GetUserDetailsByTokenNumber(hdnTokennumber.Value);
                    if (objBE_Testimonial.Status)
                    {
                        //if a status is approve
                        BodyPart = BodyPart.Replace("<%RecipientName%>", objMailDetails.RecipientName).Replace("<%message%>", "This is to inform you that your testimonial has been approved sucessfully. ").Replace("<%fromname%>", Session["UserName"].ToString());

                    }
                    else
                    {  // if a status is reject
                        BodyPart = BodyPart.Replace("<%RecipientName%>", objMailDetails.RecipientName).Replace("<%message%>", "This is to inform you that for some reason your testimonial was not approved.").Replace("<%fromname%>", Session["UserName"].ToString());
                    }

                    //sr.Close();

                    objCommon.sendMail(Session["EmailID"].ToString(), Session["UserName"].ToString(), objMailDetails.Toid, objMailDetails.RecipientName, "", "About Testimoinal upload", BodyPart);
                    ThrowAlertMessage(MM_Messages.DataSaved);
                    grdTestimonialList.SelectedIndex = -1;
                    BindData();

                }
                else
                {
                    ThrowAlertMessage(MM_Messages.DatabaseError);
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "TestimonialApproval.aspx");
            }
            finally {
                clear();
            }
        }
        // to select particular testimonial on grid.
        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            try
            {
                string path = "~/TestimoinalImage/";
                divedit.Visible = true;
                LinkButton btn = sender as LinkButton;

                GridViewRow item = btn.NamingContainer as GridViewRow;
                int Id = Convert.ToInt32(grdTestimonialList.DataKeys[item.RowIndex].Value);
                Label lblimageurl = (Label)grdTestimonialList.Rows[item.RowIndex].FindControl("ImageName");
                Label TestimonoialText = (Label)grdTestimonialList.Rows[item.RowIndex].FindControl("lblFulltext");
                imguploadedimage.ImageUrl = path + lblimageurl.Text; // show an image 
                txttestimonial.Text = TestimonoialText.Text;
                hdnSelectedRowIndex.Value = item.RowIndex.ToString();
                hdnid.Value = Id.ToString();
                hdnTokennumber.Value = grdTestimonialList.Rows[item.RowIndex].Cells[0].Text.ToString();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Testimonial.aspx");
            }
            // hdnMode.Value = Id.ToString();

        }
        protected void grdTestimonialList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        #endregion
    }
}
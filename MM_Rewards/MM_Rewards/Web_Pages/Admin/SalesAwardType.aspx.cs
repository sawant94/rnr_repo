﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using System.Data.SqlClient;
using System.Data;
using MM.DAL;
using BE_Rewards;
using BL_Rewards;
using BL_Rewards.BL_Common;
using System.IO;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class SalesAwardType : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BL_SalesAwardType objBL_SalesAwardType = new BL_SalesAwardType();
        int _SaveStatus = 0;
        BE_SalesAwardTypeColl objBE_SalesAwardTypeColl = new BE_SalesAwardTypeColl();

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {   //to check page acess.
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();
                if (!IsPostBack)
                {
                    bindgrid(); //to populate grdAwardList Control.
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesAwardType.aspx - Page_Load");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                bindgrid();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Sales_Award_Management.aspx");
            }
        }

        private void ClearControls()
        {

            txtAwardName.Text = "";
            chkActive.Checked = true;

        }

        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkSelect = (LinkButton)sender;
                GridViewRow row = (GridViewRow)lnkSelect.NamingContainer;
                int ID = Convert.ToInt32(grdAwardList.DataKeys[row.DataItemIndex].Value);
                hdnid.Value = ID.ToString();
                txtAwardName.Text = row.Cells[1].Text;
                chkActive.Checked = Convert.ToBoolean(row.Cells[2].Text);
                chkbugdetaward.Checked = Convert.ToBoolean(row.Cells[3].Text);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesAwardType.aspx - lnkSelect_Click");
            }
        }

        protected void btnsaveAwardname_Click(object sender, EventArgs e)
        {
            try
            {
                BE_SalesAwardType objBE_SalesAwardType = new BE_SalesAwardType();
                if (hdnid.Value == "")
                    objBE_SalesAwardType.Id = 0;
                else
                {
                    objBE_SalesAwardType.Id = Convert.ToInt16(hdnid.Value.ToString());
                    hdnid.Value = "";
                }
                objBE_SalesAwardType.AwardName = txtAwardName.Text.Trim();
                objBE_SalesAwardType.Isactive = Convert.ToBoolean(chkActive.Checked);
                objBE_SalesAwardType.Createdby = Session["TokenNumber"].ToString();
                objBE_SalesAwardType.ModifiedBy = Session["TokenNumber"].ToString();
                objBE_SalesAwardType.IsbudgetTracking = Convert.ToBoolean(chkbugdetaward.Checked);


                _SaveStatus = objBL_SalesAwardType.SaveData(objBE_SalesAwardType, out _SaveStatus);

                if (_SaveStatus == 0)
                {
                    ThrowAlertMessage(MM_Messages.DataSaved);
                    bindgrid();
                    grdAwardList.SelectedIndex = -1;
                    txtAwardName.Text = "";
                }
                else
                {
                    ThrowAlertMessage("Award Name already existing.");
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "SalesAwardType.aspx - btnsaveAwardname_Click");
            }
        }

        private void Save_Templates(string fileName, string template)
        {
            string path = System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath");
            FileStream fs = File.Create(path + fileName);
            StreamWriter sw = new StreamWriter(fs);

            string body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
            body += "<html xmlns=\"http://www.w3.org/1999/xhtml \">";
            body += "<head><title>Untitled Page</title></head>";
            body += "<body><table width=\"598\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-top:20px solid #E92226;border-bottom:10px solid #E92226;border-left:1px solid #E92226;border-right:1px solid #E92226;\">";
            body += "<tr><td style=\"border-top:5px double #E92226;padding:10px 20px; color:#4E504F; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:20px;\">";
            body += template;
            body += "</td></tr></table></body></html>";
            sw.Write(body);
            sw.Close();
            fs.Close();
        }

        protected void grdAwardList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow row = (GridViewRow)e.Row;
                    LinkButton lnkSelect = (LinkButton)row.FindControl("lnkSelect");
                    int ID = Convert.ToInt32(grdAwardList.DataKeys[row.DataItemIndex].Value);
                    Label lblTemplate = (Label)row.FindControl("lblTemplate");
                    //   string temp = lblTemplate.Text.Substring(lblTemplate.Text.First('<body>');
                    //    lnkSelect.OnClientClick += "return onGridViewRowSelected(" + e.Row.RowIndex + ",'" + ID + "','" + row.Cells[1].Text + "','" + Convert.ToBoolean(row.Cells[2].Text) + "','" + lblTemplate.Text + "')";
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardTypeManagement.aspx");
            }
        }

        #endregion

        #region CommonFunction

        //throw an alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        //to bind girdview control
        private void bindgrid()
        {
            objBE_SalesAwardTypeColl = objBL_SalesAwardType.GetData();
            grdAwardList.SelectedIndex = -1;
            grdAwardList.DataSource = objBE_SalesAwardTypeColl;
            grdAwardList.DataBind();
        }

        #endregion


    }
}
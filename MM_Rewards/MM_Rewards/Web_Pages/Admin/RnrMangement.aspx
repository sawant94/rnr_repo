﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="RnrMangement.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.RnrMangement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <script type="text/javascript">


        function btnSearch_ClientClick() {

            var errMsg = '';
            var txtSearchBy = document.getElementById('<%=txtSearchBy.ClientID %>');

            errMsg += CheckText_Blank(txtSearchBy.value, 'Token Number');
           // errMsg += ValidateInteger(txtSearchBy.value, 'Token Number');
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
        function ClearFields() {
            var grdRnrUser = document.getElementById('<%=grdRnrUser.ClientID %>');
            var txtSearchBy = document.getElementById('<%=txtSearchBy.ClientID %>');
            var pnldetails = document.getElementById('<%=pnldetails.ClientID %>');
            var pnldivision = document.getElementById('<%=pnldivision.ClientID %>');
            var pnllocation = document.getElementById('<%=pnllocation.ClientID %>');
            var pnlBusinessUnit = document.getElementById('<%=pnlBusinessUnit.ClientID %>');
          
            if (pnldivision != null)
                pnldivision.style.display = "none";
            else if (pnllocation != null)
                pnllocation.style.display = "none";
            else if (pnlBusinessUnit != null)
                pnlBusinessUnit.style.display = "none";

            pnldetails.style.display = "none";
            grdRnrUser.style.display = "none";
            txtSearchBy.value = "";
            return false;
        }

        function CheckUncheckAllListBox(chk,obj) {
         //   debugger;
           
            var collection = document.getElementById(obj).getElementsByTagName('INPUT');         

            for (var x = 0; x < collection.length; x++) {
                if (collection[x].type.toUpperCase() == 'CHECKBOX')
                    if (chk.checked==true) {
                        collection[x].checked = true;
                    } else {
                        collection[x].checked = false;
                    }
            }
        }
        function ValidatePage() {

            var message;
            var BusinessUnitbox;
            var divisionbox;
            var locationbox;
            var departmentbox
            var errMsg = '';
            var lblmsg;

            BusinessUnitbox = document.getElementById('BusinessUnitbox');

            if (BusinessUnitbox != null) {
                message = "Business Unit";
                var checkbox = BusinessUnitbox.getElementsByTagName("input");
                if (checkbox.length > 0) {
                    errMsg += RequiredField_CheckBoxListMore(BusinessUnitbox, message);
                }
                else {
                    errMsg += 'No record to save';
                }
            }


            divisionbox = document.getElementById('divisionbox');

            if (divisionbox != null) {
                message = "Division";
                var checkbox = divisionbox.getElementsByTagName("input");
                if (checkbox.length > 0) {
                    errMsg += RequiredField_CheckBoxListMore(divisionbox, message);
                }
                else {
                    errMsg += 'No record to save';
                }
            }

            locationbox = document.getElementById('locationbox');

            if (locationbox != null) {
                message = "Location";
                var checkbox = locationbox.getElementsByTagName("input");
                if (checkbox.length > 0) {
                    errMsg += RequiredField_CheckBoxListMore(locationbox, message);
                }
                else {
                    errMsg += 'No record to save';
                }

            }

            departmentbox = document.getElementById('departmentbox');
            if (departmentbox != null) {
                message = "Department";
                var checkbox = departmentbox.getElementsByTagName("input");
                if (checkbox.length > 0) {

                    errMsg += RequiredField_CheckBoxListMore(departmentbox, message);
                }
                else {
                    errMsg += 'No record to Save';
                }
            }


            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }

        }

    </script>
    <div id="divRNR" class="form01">
        <table>
            <tr>
                <td colspan="3">
                    <h1>
                        Manage RNR Employee
                    </h1>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                   Enter&nbsp;Token&nbsp;Number<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtSearchBy" runat="server" MaxLength="100" TabIndex="1"></asp:TextBox>
                    <div class="btnRed">
                        <asp:Button ID="btnSearch" CssClass="btnLeft" runat="server" Text="GET  DETAILS"
                            OnClick="btnGetRNR_Click" OnClientClick="return btnSearch_ClientClick()" TabIndex="2" /></div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="grid01">
                        <asp:GridView ID="grdRnrUser" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                            PageSize=' <%# Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) %>'
                            AlternatingRowStyle-CssClass="row01" OnPageIndexChanging="grdRnrUser_PageIndexChanging"
                            OnRowDataBound="grdRnrUser_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="TokenNumber" HeaderText="Token Number"></asp:BoundField>
                                <asp:BoundField DataField="EmployeeName" HeaderText="Employee Name"></asp:BoundField>
                                <asp:BoundField DataField="BusinessUnit" HeaderText="BusinessUnit"></asp:BoundField>
                                <asp:BoundField DataField="DivisionName" HeaderText="Division"></asp:BoundField>
                                <asp:BoundField DataField="LocationName" HeaderText="Location"></asp:BoundField>
                          <%--      <asp:BoundField DataField="OrgUnitName" HeaderText="Department"></asp:BoundField>--%>
                                <%--<asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("TokenNumber")  %>' CommandName="Select"
                                            runat="server" CssClass="selLink" TabIndex="3">
                                            Select</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                            <PagerStyle CssClass="dataPager" />
                           <%-- <EmptyDataRowStyle ForeColor="Red" Height="5" BorderColor="Black" BorderWidth="1"
                                BorderStyle="Solid" Font-Italic="true" />
                            <EmptyDataTemplate>
                                <asp:Literal ID="ltrlNorecords" runat="server" Text="No records to display"></asp:Literal>
                            </EmptyDataTemplate>--%>
                            <SelectedRowStyle CssClass="selectedoddTd" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <asp:Panel ID="pnldetails" runat="server" Visible="false">
        <div class="form01">
            <table>
                <tr>
                <td>
                        <div class="btnRed" runat="server" id="divbtnBusinessUnit">
                            <asp:Button ID="btngetBusinessUnit" CssClass="btnLeft"  runat="server" Text="Get  Business Unit"
                                TabIndex="4" OnClick="btngetBusinessUnit_Click" />
                        </div>
                    </td>
                    <td>
                        <div class="btnRed" runat="server" id="divbtndivision">
                            <asp:Button ID="btngetdivision" CssClass="btnLeft"  runat="server" Text="Get  Division"
                                TabIndex="4" OnClick="btngetdivision_Click" />
                        </div>
                    </td>
                    <td>
                        <div class="btnRed" runat="server" id="divbtnlocation">
                            <asp:Button ID="btngetlocation" CssClass="btnLeft" runat="server" Text="Get  Location"
                                TabIndex="5" OnClick="btngetlocation_Click" />
                        </div>
                    </td>
                    <td>
                        <%--<div class="btnRed" runat="server" id="divbtnDepartment">
                            <asp:Button ID="btngetdepartment" CssClass="btnLeft" runat="server" Text="Get  Department"
                                TabIndex="6" OnClick="btngetdepartment_Click" />
                        </div>--%>
                    </td>
                </tr>
             <%--vanda--%>

             <tr>
                    <td colspan="3" class="lblName">
                        <asp:Panel ID="pnlBusinessUnit" runat="server" Visible="false" >
                            <asp:Label CssClass="lblName" ID="lblBusinessUnit" runat="server" Text="<h1>BusinessUnit</h1>"></asp:Label>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <asp:CheckBox ID="chkBusinessUnit" CssClass="chkBox" runat="server" onclick="CheckUncheckAllListBox(this,'BusinessUnitbox')"
                             AutoPostBack="false" Text="ALL"  />
                            <div class="chkBoxHold">                            
                                <asp:Repeater ID="lstBusinessUnit" runat="server" >
                                    <HeaderTemplate>
                                        <ul class="chkBox" id="BusinessUnitbox">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                
                                            <asp:CheckBox ID="chkBusinessUnit" runat="server" Text='<%# Eval("BusinessUnit") %>' 
                                                Checked='<%# Eval("IsSelected") %>'   />
                                                
                                            <%-- <input type="checkbox" id="chkDivision"  Checked='<%# Eval("IsSelected") %>' value='<%# Eval("DivisionCode") %>' runat="server" /> --%> 
                                                       </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                        <asp:Label ID="lblBusinessUnitMessage" runat="server" Text="<h1>No records found</h1>" Visible='<%# ((ICollection)lstBusinessUnit.DataSource).Count==0?true:false %>'></asp:Label>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
              <%--vanda--%>

                <tr>
                    <td colspan="3" class="lblName">
                        <asp:Panel ID="pnldivision" runat="server" Visible="false" >
                            <asp:Label CssClass="lblName" ID="lbldivision" runat="server" Text="<h1>Division</h1>"></asp:Label>
                            <asp:HiddenField ID="hdnselectedpanel" runat="server" />
                            <asp:CheckBox ID="chkdivAll" CssClass="chkBox" runat="server" onclick="CheckUncheckAllListBox(this,'divisionbox')"
                             AutoPostBack="false" Text="ALL"  />
                            <div class="chkBoxHold">                            
                                <asp:Repeater ID="lstDivision" runat="server" >
                                    <HeaderTemplate>
                                        <ul class="chkBox" id="divisionbox">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                        <asp:Label ID="lbldivisioncode" runat="server" Text='<%# Eval("DivisionCode")%>' Visible="false"></asp:Label>
                                            <asp:CheckBox ID="chkDivision" runat="server" Text='<%# Eval("DivisionName") %>' 
                                                Checked='<%# Eval("IsSelected") %>'   />
                                                
                                            <%-- <input type="checkbox" id="chkDivision"  Checked='<%# Eval("IsSelected") %>' value='<%# Eval("DivisionCode") %>' runat="server" />   
                                    --%>            </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                        <asp:Label ID="lblDivisionMessage" runat="server" Text="<h1>No records found</h1>" Visible='<%# ((ICollection)lstDivision.DataSource).Count==0?true:false %>'></asp:Label>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="lblName">
                        <asp:Panel ID="pnllocation" runat="server" Visible="false">
                            <asp:Label CssClass="lblName" ID="lbllocation" runat="server" Text="<h1>Location</h1>"></asp:Label>
                             <asp:CheckBox ID="chklocall" CssClass="chkBox" runat="server" onclick="CheckUncheckAllListBox(this,'locationbox')"
                             AutoPostBack="false" Text="ALL"  />
                            <div class="chkBoxHold">
                                <asp:Repeater ID="lstlocation" runat="server">
                                    <HeaderTemplate>
                                        <ul class="chkBox" id="locationbox">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                        <asp:Label ID="lbllocationcode" runat="server" Text='<%# Eval("LocationCode")%>' Visible="false"></asp:Label>
                                 
                                            <asp:CheckBox ID="chklocation" runat="server" Text='<%# Eval("LocationName") %>'
                                                Checked='<%# Eval("IsSelected") %>'  /></li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                        <asp:Label ID="lblLocationMessage" runat="server" Text="<h1>No records found</h1>" Visible='<%# ((ICollection)lstlocation.DataSource).Count==0?true:false %>'></asp:Label>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="3" class="lblName">
                        <asp:Panel ID="pnldepartment" runat="server" Visible="false">
                            <asp:Label CssClass="lblName" ID="lbldepartment" runat="server" Text="<h1>Department</h1>"></asp:Label>
                           <asp:CheckBox ID="chkdeall" CssClass="chkBox" runat="server" onclick="CheckUncheckAllListBox(this,'departmentbox')"
                             AutoPostBack="false" Text="ALL"  />
                            <div class="chkBoxHold">
                                <asp:Repeater ID="lstdepartment" runat="server">
                                    <HeaderTemplate>
                                        <ul class="chkBox" id="departmentbox" >
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                          <asp:Label ID="lbldepartmentcode" runat="server" Text='<%# Eval("OrgUnitCode")%>' Visible="false"></asp:Label>
                                 
                                            <asp:CheckBox ID="chkdepartment" runat="server" Text='<%# Eval("OrgUnitName") %>'
                                                Checked='<%# Eval("IsSelected") %>'  /></li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                        <asp:Label ID="lblepartmentMessage"  runat="server" Text="<h1>No records found</h1>" Visible='<%# ((ICollection)lstdepartment.DataSource).Count==0?true:false %>'></asp:Label>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>--%>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlsave" runat="server" Visible="false">
                            <div class="btnRed">
                                <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btnLeft" OnClientClick="return ValidatePage()"
                                    TabIndex="4" OnClick="btnsave_Click" />
                            </div>
                            <div class="btnRed">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" OnClientClick="return ClearFields()"
                                    TabIndex="5" />
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_RewardsSales.Master"
    AutoEventWireup="true" CodeBehind="SalesAwardManagement.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.SalesAwardTypeManagement"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function validateSave() {
            var errMsg = '';
            var ddlDivision = document.getElementById('<%=ddlDivision.ClientID %>');
            var ddlAwardType = document.getElementById('<%=ddlAwardType.ClientID %>');
            var txtAmount = document.getElementById('<%=txtAmount.ClientID %>');
            debugger;
            errMsg += CheckDropdown_Blank2(ddlDivision.value, 'Division');
            errMsg += CheckDropdown_Blank2(ddlAwardType.value, 'Award Type');
            errMsg += CheckText_Blank(txtAmount.value, 'Amount');
            //wether to validate CheckListBox or Not, Acc to condition

            errMsg += ValidateDecimal(txtAmount.value, 'Amount');
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        } 
    </script>
    <h1>
        Sales Award Management</h1>
    <div class="grid01">
        <asp:HiddenField ID="hdnSelectedRowIndex" runat="server" />
        <asp:GridView ID="grdAwardList" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdAwardList_RowDataBound"
            DataKeyNames="ID" AlternatingRowStyle-CssClass="row01" ViewStateMode="Enabled">
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                <asp:BoundField DataField="AwardName" HeaderText="Award Name" Visible="false" />
                <asp:BoundField DataField="AwardTypeId" HeaderText="Award Type Id" />
                <asp:BoundField DataField="AwardType" HeaderText="Award Type" />
                <asp:BoundField DataField="DivisionId" HeaderText="Division Id" />
                <asp:BoundField DataField="DivisionName" HeaderText="Division" />
                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                <asp:BoundField DataField="IsActive" HeaderText="Active" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("ID")  %>' CommandName="Select"
                            OnClick="lnkSelect_Click" runat="server" CssClass="selLink" TabIndex="1">
                                            Select</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="dataPager" />
            <SelectedRowStyle CssClass="selectedoddTd" />
        </asp:GridView>
    </div>
    <div class="form01">
        <table>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label><span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddlDivision" runat="server">
                    </asp:DropDownList>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblAwardType" runat="server" Text="Award Type"></asp:Label><span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddlAwardType" runat="server" onchange="validateDropDown();">
                    </asp:DropDownList>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label><span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtAmount" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr runat="server" visible="false">
                <td class="lblName">
                    <asp:Label ID="lblAwardName" runat="server" Text="Award Name"></asp:Label><span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtAwardName" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblactive" runat="server" Text="Is Active" CssClass="lblName"></asp:Label><span
                        style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:CheckBox ID="chkActive" CssClass="chkBox" runat="server" Checked="true" TabIndex="3">
                    </asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnsaveAwardname" runat="server" Text="Save" CssClass="btnLeft" OnClick="btnsaveAwardname_Click"
                            OnClientClick="return validateSave()" TabIndex="7" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLeft" OnClientClick="return ClearFields()"
                            OnClick="btnCancel_Click" TabIndex="8" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hdnid" runat="server" />
                    <asp:HiddenField ID="hdnDivisionId" runat="server" />
                    <asp:HiddenField ID="hdnAwardTypeid" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

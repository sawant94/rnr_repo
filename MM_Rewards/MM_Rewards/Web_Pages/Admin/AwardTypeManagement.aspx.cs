﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using System.Data.SqlClient;
using System.Data;
using MM.DAL;
using BE_Rewards;
using BL_Rewards;
using BL_Rewards.BL_Common;
using System.IO;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class AwardTypeManagement : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BL_AwardTypeMangement objBLAwardTypeMangement = new BL_AwardTypeMangement();
        int _SaveStatus = 0;
        BE_AwardTypeManagementColl objBEAwardTypeManagementColl = new BE_AwardTypeManagementColl();   

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {   //to check page acess.
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();
                if (!IsPostBack)
                {
                    bindgrid(); //to populate grdAwardList Control.
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardTypeManagement.aspx");
            }
        }
        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkSelect = (LinkButton)sender;
                GridViewRow row = (GridViewRow) lnkSelect.NamingContainer;
                int ID = Convert.ToInt32(grdAwardList.DataKeys[row.DataItemIndex].Value);
                hdnid.Value = ID.ToString();
                txtAwardName.Text = row.Cells[1].Text;
                chkActive.Checked = Convert.ToBoolean(row.Cells[2].Text);

                string fileName = txtAwardName.Text + "_Redeem.txt"; //+ "_" + ID
                txtMailTemplate_Redeem.Text = Fill_TemplatesFields(fileName);

                fileName = txtAwardName.Text + "_Auto.txt"; //+ "_" + ID
                txtMailTemplate_Auto.Text = Fill_TemplatesFields(fileName);

                fileName = txtAwardName.Text + "_Confirmation.txt"; //+ "_" + ID
                txtMailTemplate_Confirmation.Text = Fill_TemplatesFields(fileName);                
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardTypeManagement.aspx");
            } 
        }

        private string Fill_TemplatesFields(string fileName)
        {
            string path = System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath");            

            string strTemplate = "";
            if (File.Exists(path + fileName))
            {
                // create reader & open file
                StreamReader sr = new StreamReader(path + fileName);

                strTemplate = sr.ReadToEnd();
                sr.Close();
            }

            string body = strTemplate.Replace("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">", "");
            body = body.Replace("<html xmlns=\"http://www.w3.org/1999/xhtml \">", "");
            body = body.Replace("<head><title>Untitled Page</title></head>", "");
            body = body.Replace("<body><table width=\"598\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-top:20px solid #E92226;border-bottom:10px solid #E92226;border-left:1px solid #E92226;border-right:1px solid #E92226;\">", "");
            body = body.Replace("<tr><td style=\"border-top:5px double #E92226;padding:10px 20px; color:#4E504F; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:20px;\">", "");
            body = body.Replace("</td></tr></table></body></html>", "");

            return body;
        }
        //to save award type 
        protected void btnsaveAwardname_Click(object sender, EventArgs e)
        {
            try
            {
                BE_AwardTypeMangement objBEAwardTypeMangement = new BE_AwardTypeMangement();
                if (hdnid.Value == "")
                    objBEAwardTypeMangement.Id = 0;
                else
                {   objBEAwardTypeMangement.Id = Convert.ToInt16(hdnid.Value.ToString());
                    hdnid.Value = "";
                }
                objBEAwardTypeMangement.AwardName = txtAwardName.Text.Trim();
                objBEAwardTypeMangement.Isactive = Convert.ToBoolean(chkActive.Checked);
                objBEAwardTypeMangement.Createdby = Session["TokenNumber"].ToString();
                objBEAwardTypeMangement.ModifiedBy = Session["TokenNumber"].ToString();

                
                _SaveStatus = objBLAwardTypeMangement.SaveData(objBEAwardTypeMangement, out _SaveStatus);
                
                if (_SaveStatus ==0)
                {                 

                    string fileName = txtAwardName.Text + "_Redeem.txt"; //+ "_" + ID
                    Save_Templates(fileName,txtMailTemplate_Redeem.Text);
                    fileName = txtAwardName.Text + "_Auto.txt"; //+ "_" + ID
                    Save_Templates(fileName,txtMailTemplate_Auto.Text);
                    fileName = txtAwardName.Text + "_Confirmation.txt"; //+ "_" + ID
                    Save_Templates(fileName, txtMailTemplate_Confirmation.Text);

                    bindgrid();
                    grdAwardList.SelectedIndex = -1;
                    txtAwardName.Text = "";
                    txtMailTemplate_Redeem.Text = "";
                    txtMailTemplate_Auto.Text = "";
                    txtMailTemplate_Confirmation.Text = "";
                    ThrowAlertMessage("Award Details Saved Successfully");
                }
                else
                {
                    ThrowAlertMessage("Award Name already exists.");
                }            
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardTypeManagement.aspx");
            } 
        }

        private void Save_Templates(string fileName,string template)
        {
            string path = System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath");
            FileStream fs = File.Create(path + fileName);
            StreamWriter sw = new StreamWriter(fs);

            string body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
            body += "<html xmlns=\"http://www.w3.org/1999/xhtml \">";
            body += "<head><title>Untitled Page</title></head>";
            body += "<body><table width=\"598\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-top:20px solid #E92226;border-bottom:10px solid #E92226;border-left:1px solid #E92226;border-right:1px solid #E92226;\">";
            body += "<tr><td style=\"border-top:5px double #E92226;padding:10px 20px; color:#4E504F; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:20px;\">";
            body += template;
            body += "</td></tr></table></body></html>";
            sw.Write(body);
            sw.Close();
            fs.Close();
        }
        protected void grdAwardList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow row = (GridViewRow)e.Row;
                    LinkButton lnkSelect = (LinkButton)row.FindControl("lnkSelect");
                    int ID = Convert.ToInt32(grdAwardList.DataKeys[row.DataItemIndex].Value);
                    Label lblTemplate = (Label)row.FindControl("lblTemplate");
                    //   string temp = lblTemplate.Text.Substring(lblTemplate.Text.First('<body>');
                    //    lnkSelect.OnClientClick += "return onGridViewRowSelected(" + e.Row.RowIndex + ",'" + ID + "','" + row.Cells[1].Text + "','" + Convert.ToBoolean(row.Cells[2].Text) + "','" + lblTemplate.Text + "')";
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardTypeManagement.aspx");
            }
        }           
        #endregion

        #region CommonFunction
        //throw an alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }
        //to bind girdview control
        private void bindgrid()
        {
            objBEAwardTypeManagementColl = objBLAwardTypeMangement.GetData();
            grdAwardList.SelectedIndex = -1;
            grdAwardList.DataSource = objBEAwardTypeManagementColl;
            grdAwardList.DataBind();
        }
        #endregion
    
    
    }
}


/*    //protected void grdAwardList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    txtAwardName.Text = "";
        //    grdAwardList.SelectedIndex = -1;
        //    hdnid.Value = "";
        //}
        
        //to retrive data for selected item in gridview -- NO MORE IN USE(Using Javascript to select the item)
        //protected void lnkSelect_Click(object sender,EventArgs e)
        //{
        //    try
        //    {
        //        LinkButton btnSelect = (LinkButton)sender;
        //        GridViewRow row = btnSelect.NamingContainer as GridViewRow ;

        //        int ID = Convert.ToInt32(grdAwardList.DataKeys[row.DataItemIndex].Value);
        //        string AwardName = row.Cells[1].Text;
        //        bool Active = Convert.ToBoolean(row.Cells[2].Text);

        //        hdnid.Value = ID.ToString();
        //        txtAwardName.Text = AwardName;
        //        chkActive.Checked = Active;

        //    }
        //    catch(Exception ex)
        //    {
        //        ThrowAlertMessage(MM_Messages.DatabaseError);
        //    }
        //}

        // to clear screen -- NO MORE IN USE(Using Javascript to cancel and clear the item)
         protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //txtAwardName.Text = "";
                //grdAwardList.SelectedIndex = -1;
                //hdnid.Value = "";
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardTypeManagement.aspx");
            }
        }*/
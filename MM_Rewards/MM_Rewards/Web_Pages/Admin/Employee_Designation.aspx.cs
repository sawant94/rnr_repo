﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using System.Data.SqlClient;
using System.Data;
using MM.DAL;
using BE_Rewards;
using BL_Rewards;
using BL_Rewards.BL_Common;

namespace MM_Rewards.Web_Pages.Admin
{
    public partial class Employee_Designation : System.Web.UI.Page
    {
       ExceptionLogging objExceptionLogging = new ExceptionLogging();
        BL_EmployeeDesignation objBLEmpDesign = new BL_EmployeeDesignation();
        int _SaveStatus = 0;

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
                Common.CheckPageAccess();//to check page acess for user.
                if (!IsPostBack)
                {
                    getchecklist();
                }
                tblcheck.Visible = false;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Employee_Designation.aspx");
            }
        }     
         // to get EmmployeeList By Name orTokenNumber      
        protected void btnget_Click(object sender, EventArgs e)
        {
            try
            {
                grdEmployeeList.SelectedIndex = -1;
                List<BE_EmployeeDesignation> ObjEmpDesignColl = new List<BE_EmployeeDesignation>();
                //get data function to retrive record.
                ObjEmpDesignColl = objBLEmpDesign.GetData(txtTokenNumber.Text);
                if (ObjEmpDesignColl.Count == 0)
                {
                    ThrowAlertMessage("There is no Record for this TokenNumber / Name.Please Enter Correct Token Number / Name");
                    grdEmployeeList.Visible = false;
                }
                else
                {
                    grdEmployeeList.DataSource = ObjEmpDesignColl;//.Distinct(new Comparer());//compare class to retrive distinct record.
                    Session["ObjEmpDesignColl"] = ObjEmpDesignColl;
                    grdEmployeeList.DataBind();
                    grdEmployeeList.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Employee_Designation.aspx");
            }
        }
         //to set designation of employee
        protected void btnsetdesignation_Click(object sender, EventArgs e)
        {
            try
            {
                BE_EmployeeDesignation objBEEmpDesign = new BE_EmployeeDesignation();
                foreach (ListItem item in chkdesignation.Items)
                {
                    if (item.Selected)
                    {
                        objBEEmpDesign.Roles.Add(Convert.ToInt32(item.Value));
                    }
                }
                objBEEmpDesign.TokenNumber = hdnRecipientTokenNumber.Value.ToString();
                objBEEmpDesign.Modifiedby = Session["TokenNumber"].ToString();
                _SaveStatus = objBLEmpDesign.SaveData(objBEEmpDesign);

                if (_SaveStatus > 0)
                {
                    ThrowAlertMessage(MM_Messages.DatabaseError);
                }
                else
                {
                    ThrowAlertMessage(MM_Messages.DataSaved);
                }
                chkdesignation.ClearSelection();
                tblcheck.Visible = false;
                grdEmployeeList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Employee_Designation.aspx");
            }
        }
        //TO clear Screen data
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {   txtTokenNumber.Text = "";
                grdEmployeeList.SelectedIndex = -1;
                grdEmployeeList.Visible = false;
                chkdesignation.ClearSelection();
                tblcheck.Visible = false;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Employee_Designation.aspx");
            }
        }
        #endregion

        #region Grid for Employee List
        //To Display seleceted Employee designation
        protected void grdEmployeeList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string RecipientTokenNumber = "";
            chkdesignation.ClearSelection();
            if (e.CommandName == "Select")
            {
                RecipientTokenNumber = e.CommandArgument.ToString();
                hdnRecipientTokenNumber.Value = Convert.ToString(RecipientTokenNumber);
                BL_EmployeeDesignation objBLEmpDesign = new BL_EmployeeDesignation();
                BE_EmpDesignationColl ObjBEEmpdesigncoll = new BE_EmpDesignationColl();
                BE_EmployeeDesignation objBEEmpDesign = new BE_EmployeeDesignation();
                ObjBEEmpdesigncoll = objBLEmpDesign.GetRolesbyId(hdnRecipientTokenNumber.Value);
                tblcheck.Visible = true;

                foreach (BE_EmployeeDesignation empDesig in ObjBEEmpdesigncoll.EmpDesignColl)//to populate ROLES for selected employee .
                {
                    for (int i = 0; i < chkdesignation.Items.Count; i++ )
                    {
                        if (Convert.ToInt32(chkdesignation.Items[i].Value.ToString()) == empDesig.Roleid)
                        {
                            chkdesignation.Items[i].Selected = true;
                        }
                    }
                }
            }
            
        }
        #endregion

        #region Common Function
        //throw an alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }
       //to bind checkboxlist 'ROLES'
        private void getchecklist()
        {
            BE_EmpDesignationColl objBE_EmpDesignationColl = new BE_EmpDesignationColl();
            objBE_EmpDesignationColl = objBLEmpDesign.getRoles();
            chkdesignation.DataSource = objBE_EmpDesignationColl.EmpDesignColl;
            chkdesignation.DataTextField = "Rolename";
            chkdesignation.DataValueField = "Roleid";
            chkdesignation.DataBind();
         }
        #endregion

        protected void grdEmployeeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEmployeeList.PageIndex = e.NewPageIndex;
             btnget_Click(sender, e);            
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Employee_Designation.aspx");
            }
        }
    }
    // use for retriving distinct record
    //public class Comparer : IEqualityComparer<BE_EmployeeDesignation>{
    //    public bool Equals(BE_EmployeeDesignation x, BE_EmployeeDesignation y)
    //    {
    //        return x.EmpName.Equals(y.EmpName);
    //    }
    //    public int GetHashCode(BE_EmployeeDesignation obj)
    //    {
    //        return obj.EmpName.GetHashCode();
    //    }
    //}
}




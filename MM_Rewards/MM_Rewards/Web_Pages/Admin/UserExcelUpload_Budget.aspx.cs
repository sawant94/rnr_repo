﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using System.Collections;
using System.Globalization;

namespace MM_Rewards.Web_Pages
{
    public partial class UserExcelUpload_Budget : System.Web.UI.Page
    {
        #region Variable Declaration
        BE_UserExcelUpload objBE_UserExcelUpload = new BE_UserExcelUpload();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl2 = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl3 = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl4 = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadCollAll = new BE_UserExcelUploadColl();
        BL_UserExcelUpload objBL_UserExcelUpload = new BL_UserExcelUpload();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        Common objCommon = new Common();
        int errCounter = 0, successCounter = 0, excelSheetNo = 0;
        static string tokenNumber = "";
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            BtnUpload.Attributes.Add("onClick", "return checkFile()");
            BtnSearch.Attributes.Add("onClick", "return chkBlank()");
            BtnSave.Attributes.Add("onClick", "return chkBlanktxt()");
            lblError.Text = "";
            lblError2.Text = "";
            LblErrorSave.Text = "";
            FileUpload1.Focus();
            // clearFields();

        }
        #endregion

        #region button Upload click
        protected void BtnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (Path.GetExtension(FileUpload1.PostedFile.FileName) == ".xls" || Path.GetExtension(FileUpload1.PostedFile.FileName) == ".xlsx")
                {
                    lblError.Text = "";
                    string fileName = "", rowInserted = "";
                    string dirPath = "", errMsg = "", fileLocation;                    
                    //string BodyPart = "";                               

                    fileName = "Budgetlog.txt";
                  //  dirPath = "D:/Projects/MM_Rewards/MM_Rewards.root/MM_Rewards/MM_Rewards/Logs";
                    dirPath = Server.MapPath(ConfigurationManager.AppSettings["FolderDirPath"].ToString());

                    fileLocation = UploadFile();
                    OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Excel 12.0 Xml;HDR=YES;", fileLocation));
                    con.Open();
                    var sheets = from DataRow dr in con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows
                                 select new { SheetName = dr["TABLE_NAME"].ToString() };
                    con.Close();

                    DateTime now = DateTime.Now;
                    LogData(dirPath, fileName, " \t", "------------------------------" + now.ToString("F") + "-----------------------------------------\t");
                    LogData(dirPath, fileName, " \t", " ");

                    BE_UserExcelUpload objBEUserDataUpLoad = new BE_UserExcelUpload();
                    BE_UserExcelUploadColl objBEUserDataUpLoadColl = new BE_UserExcelUploadColl();

                    foreach (var s in sheets)
                    {
                        if (!s.SheetName.Contains("_xlnm#_FilterDatabase"))
                        {
                            OleDbDataAdapter da = new OleDbDataAdapter(string.Format("select * from [{0}]", s.SheetName), con);
                            DataTable dtMain = new DataTable();
                            da.Fill(dtMain);

                            DataTable MasUserTable;
                            DataTable ExcelTable;                          

                            if (CheckExcelFormat(dtMain) == "valid")
                            {
                                MasUserTable = objBL_UserExcelUpload.GetMasUserTable();
                                ExcelTable = dtMain;

                                var query = from MasUserTable2 in MasUserTable.AsEnumerable()
                                            join ExcelTable2 in ExcelTable.AsEnumerable()
                                            on MasUserTable2.Field<string>("TokenNumber") equals ExcelTable2.Field<string>("TokenNumber")
                                            where MasUserTable2.Field<string>("TokenNumber") == ExcelTable2.Field<string>("TokenNumber")
                                            select new
                                            {
                                                TokenNumber = MasUserTable2.Field<string>("TokenNumber")

                                            };

                                List<string> ExistTokenNumbers = new List<string>();
                                foreach (var token in query)
                                {
                                    ExistTokenNumbers.Add(token.TokenNumber);
                                }


                                foreach (DataRow _dr in dtMain.Rows)
                                {
                                    if (CheckBlank(_dr))
                                    {
                                        objBEUserDataUpLoad = new BE_UserExcelUpload();
                                        errMsg = ValidateRow(_dr, dirPath, fileName);                                        
                                      
                                        //Data fetching from Row
                                     //   if (errMsg != "Token Number is missing and Start Date and End Date is Missing" && errMsg != "Token Number is missing" && errMsg != "Budget is missing!" && errMsg != "Dates are Missing! and Budget Missing!")
                                        if (errMsg == "" && ExistTokenNumbers.Contains(_dr[0].ToString()))
                                        {
                                            CultureInfo UsEn = new CultureInfo("de-DE");
                                            DateTime StartDate = new DateTime();
                                            DateTime EndDate = new DateTime();
                                            if (_dr[1].ToString() != "")
                                            {
                                                StartDate = Convert.ToDateTime(_dr[1].ToString(), UsEn);
                                                StartDate.ToString("dd/MM/yyyy");
                                            }
                                            if (_dr[2].ToString() != "")
                                            {
                                                EndDate = Convert.ToDateTime(_dr[2].ToString(), UsEn);
                                                EndDate.ToString("dd/MM/yyyy");
                                            }

                                            objBEUserDataUpLoad.EmployeeNumber = _dr[0].ToString();

                                            objBEUserDataUpLoad.StartDate = StartDate.ToString("dd/MM/yyyy");
                                            objBEUserDataUpLoad.EndDate = EndDate.ToString("dd/MM/yyyy");
                                            objBEUserDataUpLoad.Budget = Convert.ToDecimal(_dr[3].ToString());
                                            objBEUserDataUpLoad.Status = "SUCCESS";
                                            objBEUserDataUpLoad.Remarks = "Record saved successfully!";

                                        }
                                        else 
                                        {
                                            if (!ExistTokenNumbers.Contains(_dr[0].ToString()))
                                            {
                                                objBEUserDataUpLoad.TokenNumber = _dr[0].ToString();
                                                objBEUserDataUpLoad.Status = "FAIL";
                                                if (_dr[0].ToString() == "")
                                                {
                                                    if (errMsg != "")
                                                        LogData(dirPath, fileName, " \t  FAIL Record : " + errMsg,
                                                            " FAIL Record :" + errMsg + "                 " + now);
                                                    else
                                                        LogData(dirPath, fileName, " \t  FAIL Record : " + "Token Number Missing!",
                                                            " FAIL Record :" + "Token Number Missing!" + "                 " + now);

                                                    errCounter = errCounter + 1;
                                                }
                                                else
                                                {
                                                    if (errMsg != "")
                                                        LogData(dirPath, fileName, " \t  FAIL Record :  " + objBEUserDataUpLoad.TokenNumber + ":" + "InvalidToken Number and" + errMsg,
                                                            " FAIL Record :" + objBEUserDataUpLoad.TokenNumber + ": " + "InvalidToken Number and " + errMsg + "                 " + now);
                                                    else
                                                        LogData(dirPath, fileName, " \t  FAIL Record :  " + objBEUserDataUpLoad.TokenNumber + ":" + "InvalidToken Number",
                                                           " FAIL Record :" + objBEUserDataUpLoad.TokenNumber + ": " + "InvalidToken Number" + "                 " + now);

                                                    errCounter = errCounter + 1;
                                                }

                                            }
                                            
                                        }
                                        if (errMsg != "" && ExistTokenNumbers.Contains(_dr[0].ToString()))
                                        {
                                            objBEUserDataUpLoad.TokenNumber = _dr[0].ToString();                                                                                     
                                            objBEUserDataUpLoad.Status = "FAIL";
                                            objBEUserDataUpLoad.Remarks = errMsg;

                                            LogData(dirPath, fileName, " \t  FAIL Record :  " + objBEUserDataUpLoad.TokenNumber + ":" + errMsg, " FAIL Record :" + objBEUserDataUpLoad.TokenNumber + ": " + errMsg + "                 " + now);
                                            errCounter = errCounter + 1;

                                        }
                                        //if (errMsg != "Token Number is missing and Start Date and End Date is Missing" && errMsg != "Token Number is missing" 
                                          //  && errMsg != "Budget is missing!" && errMsg != "Dates are Missing! and Budget Missing!"
                                           // && errMsg != "Start Date is missing!" && errMsg != "End Date is missing!" && errMsg != "Budget is missing!")
                                        if(errMsg  =="" && ExistTokenNumbers.Contains(_dr[0].ToString()))
                                        {
                                            objBEUserDataUpLoadColl.Add(objBEUserDataUpLoad);
                                            successCounter = successCounter + 1;
                                            
                                        }
                                        //else
                                        {
                                            
                                        }
                                    }
                                }
                                //Call to function and it will return the no. of row inserted.
                                rowInserted = UpLoadExcel(objBEUserDataUpLoadColl, dirPath, fileName, dtMain);
                                if (rowInserted != "")
                                {
                                    // if (lblError.Text != "Please Upload Correct Excel File" && excelSheetNo != 1)
                                    lblError.Text = "" + rowInserted + "";
                                    txtSearchBox.Text = "";
                                    ShowGrid();
                                }
                                else
                                {
                                    lblError.Text = "Failure in Process";
                                    lblError.CssClass = "redClass";
                                }
                            }
                            else
                            {
                                if (excelSheetNo == 1)
                                {
                                    lblError.Text = "Please Upload Correct Excel File";
                                    lblError.CssClass = "redClass";
                                }
                            }
                        }

                    }
                }
                else
                {
                    lblError.Text = "Please Select Only Excel File";
                    lblError.CssClass = "redClass";
                }

            }
            catch (Exception ex)
            {
                ThrowAlertMessage("Please Check the Excel Sheet And Excel Format");
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region function Upload Excel
        //In this function we are creating datatavble same fields like in Excel for importing then one by one row will create and data will be stored to table.
        public string UpLoadExcel(BE_UserExcelUploadColl objBEUserDataUpLoadColl, string dirpath, string FileName, DataTable dtMain)
        {
            try
            {
                object _stat;
                string messageforInsert = "", messageforUpdate = "";
                int _status = 0;
                DAL_Common objDALCommon = new DAL_Common();

                DataTable MasUserTable;
                DataTable ExcelTable;
                MasUserTable = objBL_UserExcelUpload.GetMasBudgetTable();
                ExcelTable = dtMain;

                var query = from MasUserTable2 in MasUserTable.AsEnumerable()
                            join ExcelTable2 in ExcelTable.AsEnumerable()
                            on MasUserTable2.Field<string>("TokenNumber") equals ExcelTable2.Field<string>("TokenNumber")
                            where MasUserTable2.Field<string>("TokenNumber") == ExcelTable2.Field<string>("TokenNumber")
                            select new
                            {
                                TokenNumber = MasUserTable2.Field<string>("TokenNumber")

                            };

                List<string> ExistTokenNumbers = new List<string>();
                foreach (var token in query)
                {
                    ExistTokenNumbers.Add(token.TokenNumber);
                }

                DataTable dtforInsert = new DataTable();                
                dtforInsert.Columns.Add("TokenNumber", typeof(string));
                dtforInsert.Columns.Add("StartDate", typeof(DateTime));
                dtforInsert.Columns.Add("EndDate", typeof(DateTime));
                dtforInsert.Columns.Add("Budget", typeof(string));
                dtforInsert.Columns.Add("Status", typeof(string));
                dtforInsert.Columns.Add("Remarks", typeof(string));

                DataTable dtforUpdate = new DataTable();
                dtforUpdate.Columns.Add("TokenNumber", typeof(string));
                dtforUpdate.Columns.Add("StartDate", typeof(DateTime));
                dtforUpdate.Columns.Add("EndDate", typeof(DateTime));
                dtforUpdate.Columns.Add("Budget", typeof(string));
                dtforUpdate.Columns.Add("Status", typeof(string));
                dtforUpdate.Columns.Add("Remarks", typeof(string));


                foreach (BE_UserExcelUpload objUserDataUpLoad in objBEUserDataUpLoadColl)
                {
                    CultureInfo UsEn = new CultureInfo("de-DE");                            
                    if (ExistTokenNumbers.Count == 0)
                    {
                        DataRow _dr = dtforInsert.NewRow();
                        
                        _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                        _dr["Budget"] = objUserDataUpLoad.Budget;
                        _dr["Status"] = objUserDataUpLoad.Status;
                        _dr["Remarks"] = objUserDataUpLoad.Remarks;                       

                        if (objUserDataUpLoad.StartDate != null)
                        {
                            _dr["StartDate"] = Convert.ToDateTime(objUserDataUpLoad.StartDate,UsEn);
                        }
                        else
                            _dr["StartDate"] = DBNull.Value;                       

                        if (objUserDataUpLoad.EndDate != null)
                        {
                            _dr["EndDate"] = Convert.ToDateTime(objUserDataUpLoad.EndDate,UsEn);
                        }
                        else
                            _dr["EndDate"] = DBNull.Value;

                        dtforInsert.Rows.Add(_dr);
                    }
                    else
                    {

                        if (ExistTokenNumbers.Contains(objUserDataUpLoad.EmployeeNumber))
                        {
                            // Update
                            
                            DataRow _dr = dtforUpdate.NewRow();

                            _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                            _dr["Budget"] = objUserDataUpLoad.Budget;
                            _dr["Status"] = objUserDataUpLoad.Status;
                            _dr["Remarks"] = objUserDataUpLoad.Remarks;

                            if (objUserDataUpLoad.StartDate != null)
                            {
                                _dr["StartDate"] = Convert.ToDateTime(objUserDataUpLoad.StartDate,UsEn);
                            }
                            else
                                _dr["StartDate"] = DBNull.Value;

                            if (objUserDataUpLoad.EndDate != null)
                            {
                                _dr["EndDate"] = Convert.ToDateTime(objUserDataUpLoad.EndDate,UsEn);
                            }
                            else
                                _dr["EndDate"] = DBNull.Value;

                            dtforUpdate.Rows.Add(_dr);
                        }
                        else
                        {
                            //Insert

                            DataRow _dr = dtforInsert.NewRow();

                            _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                            _dr["Budget"] = objUserDataUpLoad.Budget;
                            _dr["Status"] = objUserDataUpLoad.Status;
                            _dr["Remarks"] = objUserDataUpLoad.Remarks;

                            if (objUserDataUpLoad.StartDate != null)
                            {
                                _dr["StartDate"] = Convert.ToDateTime(objUserDataUpLoad.StartDate,UsEn);
                            }
                            else
                                _dr["StartDate"] = DBNull.Value;

                            if (objUserDataUpLoad.EndDate != null)
                            {
                                _dr["EndDate"] = Convert.ToDateTime(objUserDataUpLoad.EndDate,UsEn);
                            }
                            else
                                _dr["EndDate"] = DBNull.Value;

                            dtforInsert.Rows.Add(_dr);
                        }

                    }

                }
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@Table_Mas_UserBudget", dtforInsert);

                //function call Save data which will send table and stored procedure name it will return no. of row inserted.

                messageforInsert = objBL_UserExcelUpload.SaveBudgetExcel(objSqlParam[0], 0);    //// 0 for Insert and 1 for Update Records
                objSqlParam[0] = new SqlParameter("@Table_Mas_UserBudget", dtforUpdate);
                messageforUpdate = objBL_UserExcelUpload.SaveBudgetExcel(objSqlParam[0], 1);
                LogData(dirpath, FileName, " \t", " ");  //for New Line
                LogData(dirpath, FileName, "\n", "Number of Failed Records  : " + errCounter);
                LogData(dirpath, FileName, "\n", "Number of Success Records : " + successCounter);
                //LogData(dirpath, FileName, "UpLoaded RowCount : " + _status, "UpLoaded RowCount : " + _status);
                LogData(dirpath, FileName, " " + messageforInsert, "" + messageforInsert);
                LogData(dirpath, FileName, " " + messageforUpdate, "" + messageforUpdate);
                string message = messageforInsert + "<br /> " + messageforUpdate;
                return message;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage("Please Check the Excel Sheet And Excel Format");
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region function Check Blank()
        private Boolean CheckBlank(DataRow dr)
        {
            bool b = false;
            foreach (var item in dr.ItemArray)
            {
                if (item.ToString() != "")
                {
                    b = true;
                    break;
                }
            }
            return b;
        }
        #endregion

        #region function Check Excel Format()
        private string CheckExcelFormat(DataTable dt)
        {
            excelSheetNo = excelSheetNo + 1;
            string excel = "blank";
            foreach (var item in dt.Columns)
            {
                if (dt.Columns[0].ToString() == "TokenNumber" && dt.Columns[1].ToString() == "StartDate" && dt.Columns[2].ToString() == "EndDate")
                {
                    excel = "valid";
                    break;
                }

            }
            return excel;
        }
        #endregion

        #region LogData Function
        private static void LogData(string dirpath, string FileName, string strMessage, string strDisplayMessage = "")
        {
            StreamWriter logFile;
        A:
            try
            {
                string FilePath = dirpath + "\\" + FileName;

                if (!File.Exists(FilePath.ToString()))               //("Log.txt"))
                {
                    logFile = new StreamWriter(FilePath);
                }
                else
                {
                    logFile = File.AppendText(FilePath.ToString());
                }
                logFile.WriteLine(strDisplayMessage);



                logFile.Close();
            }
            catch (Exception ex)
            {
                goto A;
            }
        }
        #endregion

        #region Upload File
        public String UploadFile()
        {
            if (FileUpload1.HasFile)
            {
                string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string fileLocation = Server.MapPath("~/Uploads/" + fileName);
                FileUpload1.SaveAs(fileLocation);
                return (fileLocation);
            }
            else
            {
                return "";
            }

        }
        #endregion

        #region Validate Row
        private string ValidateRow(DataRow _dr, string dirpath, string FileName)
        {
            string errMsg = "";
            try
            {
                if (_dr[0].ToString() == "" && _dr[1].ToString() =="" && _dr[2].ToString() == "")
                    errMsg = "Token Number is missing and Start Date and End Date is Missing";
                else if (_dr[0].ToString() == "")
                    errMsg = "Token Number is missing";
                else if (_dr[1].ToString() == "" && _dr[2].ToString() == "" && _dr[3].ToString() == "")
                    errMsg = "Dates are Missing! and Budget Missing!";
                else if (_dr[1].ToString() == ""  && _dr[3].ToString() == "")
                    errMsg = "Dates are Missing! and Budget Missing!";
                else if (_dr[2].ToString() == "" && _dr[3].ToString() == "")
                    errMsg = "Dates are Missing! and Budget Missing!";
                else if (_dr[1].ToString() == "" && _dr[2].ToString() == "")
                    errMsg = "Start Date is missing! and End Date is Missing!";
                else if (_dr[1].ToString() == "")
                    errMsg = "Start Date is missing!";
                else if (_dr[2].ToString() == "")
                    errMsg = "End Date is missing!";
                else if (_dr[3].ToString() == "")
                    errMsg = "Budget is missing!";
                else
                {
                    //DateTime stdate = DateTime.Now, enddate = DateTime.Now;
                    string stdate = "", enddate = "";
                    for (int i = 1; i < 3; i++)
                    {
                     //   string date = _dr[i].ToString().ToDate_SwapDayMonth();
                        CultureInfo UsEn = new CultureInfo("de-DE");
                        DateTime date = Convert.ToDateTime(_dr[i].ToString(), UsEn);
                        date.ToString("dd/MM/yyyy");
                        try
                        {
                            if (i == 1)
                            {
                                 stdate = date.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                 enddate = date.ToString("dd/MM/yyyy");
                            }
                        }
                        catch (Exception ex)
                        {
                            errMsg = (i == 1 ? "Start" : "End") + " Date is invalid! Valid date format is dd/MM/yyyy";
                        }
                    }

                    //if (errMsg == "")
                    //{
                    //    if (Convert.ToInt32(stdate.ToString("yyyyMMdd")) > Convert.ToInt32(enddate.ToString("yyyyMMdd")))
                    //    {
                    //        errMsg = "End Date should be greater than Start Date!";
                    //    }
                    //}

                    if (errMsg == "")
                    {
                        try
                        {
                            Convert.ToDecimal(_dr["Budget"]);
                        }
                        catch (Exception ex)
                        {
                            errMsg = "Budget Invalid!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return errMsg;
        }
        #endregion

        #region Email Validation
        private bool isEmailValid(string emailID)
        {
            bool isValid = false;
            string pattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            try
            {
                Regex myRegex = new Regex(pattern);
                isValid = myRegex.IsMatch(emailID);
            }
            catch (Exception exp)
            {
            }
            return isValid;
        }
        #endregion

        #region Search Button Click
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblError2.Text = "";
                clearFields();
                lblDetailsList.Visible = false;
                objBE_UserExcelUploadColl = objBL_UserExcelUpload.GetBudgetDetailsByTokenNumber(txtSearchBox.Text);
                grdExcelUpload.DataSource = objBE_UserExcelUploadColl;
                Session["dataset"] = objBE_UserExcelUploadColl;
                grdExcelUpload.DataBind();
                if (objBE_UserExcelUploadColl.Count <= 0)
                {
                    lblError2.Text = "Please Enter the Valid Token Number!";
                    lblError2.CssClass = "redClass";
                    formUpdatePanel.Visible = false;
                }
                else
                {
                    lblDetailsList.Visible = true;
                    lblDetailsList.Text = "Details of Employee ";
                    formUpdatePanel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region Grid events
        protected void grdExcelUpload_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }
        }
        protected void grdExcelUpload_EditRow(object sender, GridViewEditEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }
        }
        protected void grdExcelUpload_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdExcelUpload.PageIndex = e.NewPageIndex;
                if (Session["dataset"] != null)
                {
                    grdExcelUpload.DataSource = Session["dataset"];
                    grdExcelUpload.DataBind();

                }
                else
                {
                    ShowGrid();
                }

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }
        }
        protected void grdExcelUpload_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {



            }
        }
        #endregion

        #region   throw a  java script alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }
        #endregion

        #region ShowGrid With Latest Upload
        private void ShowGrid()
        {
            try
            {
                lblError2.Text = "";
                objBE_UserExcelUploadColl = objBL_UserExcelUpload.GetBudgetDetailsAfterUpload();
                grdExcelUpload.DataSource = objBE_UserExcelUploadColl;
                grdExcelUpload.DataBind();
                Session["dataset"] = objBE_UserExcelUploadColl;
                if (objBE_UserExcelUploadColl.Count <= 0)
                {
                    lblError2.Text = "No Records Modified Today";
                }
                else
                {
                    lblDetailsList.Visible = true;
                    lblDetailsList.Text = "Records Modified Today";
                }

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion    

        #region Edit Button Click

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                formUpdatePanel.Visible = true;
                clearFields();
                DateTime dt = new DateTime();
                LinkButton btn = sender as LinkButton;
                GridViewRow item = btn.NamingContainer as GridViewRow;
                string Id = grdExcelUpload.DataKeys[item.RowIndex].Value.ToString();                
                txtTokenNumber.Text = item.Cells[0].Text;
                tokenNumber = item.Cells[0].Text;
                CultureInfo UsEn = new CultureInfo("de-DE");
                dt = Convert.ToDateTime(item.Cells[2].Text, UsEn);
                telStartDate.DateInput.DisplayDateFormat = "dd/MM/yyyy";
                telStartDate.SelectedDate = dt;
                dt = Convert.ToDateTime(item.Cells[3].Text, UsEn);
                telEndDate.DateInput.DisplayDateFormat = "dd/MM/yyyy";                
                telEndDate.SelectedDate = dt;
                txtBudget.Text = item.Cells[4].Text;
                txtEmpName.Text = item.Cells[1].Text;
               
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Button Save Record
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string tokenNumberPrevious = "", TokenNumberNext = "",
                    startDate="", endDate="",  message = "";
                decimal Budget = 0;
                
                tokenNumberPrevious = tokenNumber;
                TokenNumberNext = txtTokenNumber.Text;
                startDate = telStartDate.SelectedDate.ToString();
                endDate = telEndDate.SelectedDate.ToString();
                Budget = Convert.ToDecimal(txtBudget.Text);

                message = objBL_UserExcelUpload.UpdateBudgetRecord(TokenNumberNext, tokenNumberPrevious, startDate, endDate, Budget);

                if (message != null && message != "")
                {
                    if (message == "2")
                        LblErrorSave.Text = "Token Number is Already Exist!";
                    else if(message == "Null")
                    {
                        LblErrorSave.Text = "Please Enter Valid Details!";
                    }
                    else
                    {
                        LblErrorSave.Text = "Record Updated Successfully!";
                        formUpdatePanel.Visible = false;
                    }
                    txtSearchBox.Text = "";
                    ShowGrid();
                }
                else
                {

                    LblErrorSave.Text = "Failure in Process! Please Try Again ";
                    txtSearchBox.Text = "";
                    ShowGrid();
                }
            }

            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }

        }
        #endregion

        #region Button Cancel
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            clearFields();
            formUpdatePanel.Visible = false;
        }
        #endregion

        #region Separation of Table into 2 tables one for Exist tokens and non Exist

        //  public bool SeparateTables(DataTable dtMain)
        // {
        //DataSet ds= null;
        //DataTable MasUserTable;
        //DataTable ExcelTable1;
        //MasUserTable = objBL_UserExcelUpload.GetMasUserTable();
        //ExcelTable1 = dtMain;            

        //    var query = from MasUserTable2 in MasUserTable.AsEnumerable()
        //                join ExcelTable2 in ExcelTable1.AsEnumerable()
        //                on MasUserTable2.Field<string>("TokenNumber") equals ExcelTable2.Field<string>("Employee Number")
        //                where MasUserTable2.Field<string>("TokenNumber") == ExcelTable2.Field<string>("Employee Number")
        //                select new
        //                {
        //                    TokenNumber = MasUserTable2.Field<string>("TokenNumber")

        //                };

        //    //var query2 = from ExcelTable2 in ExcelTable .AsEnumerable()
        //    //             join MasUserTable2 in MasUserTable.AsEnumerable()
        //    //             on ExcelTable2.Field<string>("Employee Number") equals MasUserTable2.Field<string>("TokenNumber")
        //    //             where MasUserTable2.Field<string>("TokenNumber") != ExcelTable2.Field<string>("Employee Number")
        //    //            select new
        //    //            {
        //    //                TokenNumber = ExcelTable2.Field<string>("Employee Number")

        //    //            };
        //    //var query3 = from  ExcelTable2 in ExcelTable.AsEnumerable()
        //    //             join  MasUserTable2 in MasUserTable.AsEnumerable()
        //    //             on    ExcelTable2.Field<string>("Employee Number")  equals MasUserTable2.Field<string>("TokenNumber")
        //    //             where !ExcelTable2.Field<string>("Employee Number").Contains(MasUserTable2.Field<string>("TokenNumber")) 
        //    //             select new
        //    //             {
        //    //                 TokenNumber = ExcelTable2.Field<string>("Employee Number")

        //    //             };

        //    var qry1 = MasUserTable.AsEnumerable().Select(a => new { TokenNumber = a["TokenNumber"].ToString() });
        //    List<string> ExistTokenNumbers = new List<string>();
        //   // ExistTokenNumbers = query.ToList<string>();
        // foreach (var grade in query)
        //{
        //    ExistTokenNumbers.Add(grade.TokenNumber);
        //}


        //   // List<> query2 = qry1.AsEnumerable().Select(b => new { TokenNumber2 = b.TokenNumber.ToString() });
        //    //var qry2 = ExcelTable.AsEnumerable().Select(b => new { TokenNumber = b["Employee Number"].ToString() });

        //    //var exceptAB = qry2.Except(qry1);

        //    //var dtMisMatch = (from a in ExcelTable.AsEnumerable()
        //    //                        join ab in exceptAB on a["Employee Number"].ToString() equals ab.TokenNumber
        //    //                        select a);




        //return true;
        // }

        #endregion

        #region Clear all fields
        public void clearFields()
        {          
            txtTokenNumber.Text = "";
            txtBudget.Text = "";
            telStartDate.Clear();
            telEndDate.Clear();
            txtEmpName.Text = "";
        }
        #endregion

      
}
}
    

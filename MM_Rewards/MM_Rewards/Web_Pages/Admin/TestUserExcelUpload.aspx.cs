﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using System.Collections;
using System.Globalization;
using Excel;



namespace MM_Rewards.Web_Pages
{

    public partial class TestUserExcelUpload : System.Web.UI.Page
    {
        #region Variable Declaration
        BE_UserExcelUpload objBE_UserExcelUpload = new BE_UserExcelUpload();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl2 = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl3 = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadColl4 = new BE_UserExcelUploadColl();
        BE_UserExcelUploadColl objBE_UserExcelUploadCollAll = new BE_UserExcelUploadColl();
        BL_UserExcelUpload objBL_UserExcelUpload = new BL_UserExcelUpload();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        Common objCommon = new Common();
        int errCounter = 0, successCounter = 0, excelSheetNo = 0;
        static string tokenNumber = "";
        public int intTotalProductCount = 0;
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            BtnUpload.Attributes.Add("onClick", "return checkFile()");
            BtnSearch.Attributes.Add("onClick", "return chkBlank()");
            BtnSave.Attributes.Add("onClick", "return chkBlanktxt()");
            lblError.Text = "";
            lblError2.Text = "";
            LblErrorSave.Text = "";
            FileUpload1.Focus();
            // clearFields();

        }
        #endregion

        #region button Upload click
        protected void BtnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (Path.GetExtension(FileUpload1.PostedFile.FileName) == ".xls" || Path.GetExtension(FileUpload1.PostedFile.FileName) == ".xlsx")
                {
                    DataSet dsGetImportedData = new DataSet();
                    string strpath = FileUpload1.PostedFile.FileName.Substring(FileUpload1.PostedFile.FileName.LastIndexOf("\\") + 1);
                    if (File.Exists(Server.MapPath("~/Include/Excel/" + strpath)))
                        File.Delete(Server.MapPath("~/Include/Excel/" + strpath));
                    FileUpload1.SaveAs(Server.MapPath("~/Include/Excel/" + strpath));
                    dsGetImportedData = ImportExcelXLS(Server.MapPath("~/Include/Excel/" + strpath), true);
                    if (dsGetImportedData != null)
                    {
                        if (dsGetImportedData.Tables.Count > 0)
                        {
                            BtnUpload.Enabled = false;
                            intTotalProductCount = dsGetImportedData.Tables[0].Rows.Count;

                            lblError.Text = "";
                            string fileName = "", rowInserted = "";
                            string dirPath = "", errMsg = "";
                            bool IsValidEmail = true;
                            //string BodyPart = "";                               

                            fileName = "log.txt";
                            // dirPath = "D:/Projects/MM_Rewards/MM_Rewards.root/MM_Rewards/MM_Rewards/Logs";
                            dirPath = Server.MapPath(ConfigurationManager.AppSettings["FolderDirPath"].ToString());

                            //fileLocation = UploadFile();
                            //OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Excel 12.0 Xml;HDR=YES;", fileLocation));
                            //con.Open();
                            //var sheets = from DataRow dr in con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows
                            //             select new { SheetName = dr["TABLE_NAME"].ToString() };
                            //con.Close();

                            DateTime now = DateTime.Now;
                            LogData(dirPath, fileName, " \t", "------------------------------" + now.ToString("F") + "-----------------------------------------\t");
                            LogData(dirPath, fileName, " \t", " ");

                            BE_UserExcelUpload objBEUserDataUpLoad = new BE_UserExcelUpload();
                            BE_UserExcelUploadColl objBEUserDataUpLoadColl = new BE_UserExcelUploadColl();


                            //OleDbDataAdapter da = new OleDbDataAdapter(string.Format("select * from [{0}]", s.SheetName), con);
                            DataTable dtMain = new DataTable();
                            dtMain = dsGetImportedData.Tables[0];

                            if (CheckExcelFormat(dtMain) == "valid")
                            {
                                foreach (DataRow _dr in dtMain.Rows)
                                {
                                    if (CheckBlank(_dr))
                                    {
                                        objBEUserDataUpLoad = new BE_UserExcelUpload();
                                        errMsg = ValidateRow(_dr, dirPath, fileName);
                                        IsValidEmail = isEmailValid(_dr[13].ToString());
                                        if (errMsg != "Token Number and EmailID Missing!")
                                        {
                                            if (!IsValidEmail && errMsg == "Token Number is missing!")
                                                errMsg = "Token Number is missing! and Invalid EmailID!";
                                            else if (!IsValidEmail && errMsg == "First Name is missing! and Required Field is Missing!")
                                            {
                                                errMsg = "First Name is missing! and Required Field is Missing!";

                                            }
                                            else if (!IsValidEmail && errMsg == "Names are missing! and Required Field is Missing!")
                                            {
                                                errMsg = "Names are missing! and Required Field is Missing!";
                                            }
                                            else if (!IsValidEmail)
                                                errMsg = "Invalid EmailID!";
                                        }
                                        //Data fetching from Row
                                        if (errMsg != "Token Number is missing!" && errMsg != "Token Number is missing! and Invalid EmailID!")
                                        {
                                            // DateTime DOB = DateTime.ParseExact(_dr[25].ToString(), "dd/MM/yyyy", null);  //Convert.ToDateTime(_dr[25].ToString());  // DateTime DOJ = DateTime.ParseExact(_dr[24].ToString(), "dd/MM/yyyy", null); //Convert.ToDateTime(_dr[24].ToString());                                          
                                            // UsEn.DateTimeFormat = DateTimeFormatInfo.// DateTime DOB = DateTime.Parse(_dr[25].ToString());
                                            CultureInfo UsEn = new CultureInfo("de-DE");
                                            DateTime DOB = new DateTime();
                                            DateTime DOJ = new DateTime();
                                            if (_dr[25].ToString() != "")
                                            {
                                                DOB = Convert.ToDateTime(_dr[25].ToString(), UsEn);
                                                DOB.ToString("dd/MM/yyyy");
                                            }
                                            if (_dr[24].ToString() != "")
                                            {
                                                DOJ = Convert.ToDateTime(_dr[24].ToString(), UsEn);
                                                DOJ.ToString("dd/MM/yyyy");
                                            }

                                            objBEUserDataUpLoad.EmployeeNumber = _dr[0].ToString();
                                            objBEUserDataUpLoad.EmployeeName = _dr[1].ToString();
                                            objBEUserDataUpLoad.FirstName = _dr[2].ToString();
                                            objBEUserDataUpLoad.LastName = _dr[3].ToString();
                                            objBEUserDataUpLoad.EmployeeGroup = _dr[4].ToString();
                                            objBEUserDataUpLoad.EG = _dr[5].ToString();
                                            objBEUserDataUpLoad.EmployeeSubgroup = _dr[6].ToString();
                                            objBEUserDataUpLoad.Counter = Convert.ToInt32(_dr[7].ToString());
                                            objBEUserDataUpLoad.ESG = _dr[8].ToString();
                                            objBEUserDataUpLoad.PersonnelArea = _dr[9].ToString();
                                            objBEUserDataUpLoad.PA = _dr[10].ToString();
                                            objBEUserDataUpLoad.PersonnelSubarea = _dr[11].ToString();
                                            objBEUserDataUpLoad.PSA = _dr[12].ToString();
                                            objBEUserDataUpLoad.EmailID = _dr[13].ToString();
                                            objBEUserDataUpLoad.EmployeeOrgUnitNumber = _dr[14].ToString();
                                            objBEUserDataUpLoad.EmployeeOrgUnitName = _dr[15].ToString();
                                            objBEUserDataUpLoad.BusinessUnit = _dr[16].ToString();
                                            objBEUserDataUpLoad.BUNo = _dr[17].ToString();
                                            objBEUserDataUpLoad.EmployeeCostCenterNumber = _dr[18].ToString();
                                            objBEUserDataUpLoad.EmployeeCostCenterName = _dr[19].ToString();
                                            objBEUserDataUpLoad.EmployeePositionNumber = _dr[20].ToString();
                                            objBEUserDataUpLoad.EmployeePositionName = _dr[21].ToString();
                                            objBEUserDataUpLoad.CompanyCode = _dr[22].ToString();
                                            objBEUserDataUpLoad.Gender = _dr[23].ToString();
                                            objBEUserDataUpLoad.DateofJoining = DOJ.ToString();
                                            objBEUserDataUpLoad.DateofBirth = DOB.ToString();
                                            objBEUserDataUpLoad.PayrollAreaText = _dr[26].ToString();
                                            objBEUserDataUpLoad.PayrollAreaCode = _dr[27].ToString();
                                            objBEUserDataUpLoad.MangerNumber = _dr[28].ToString();
                                            objBEUserDataUpLoad.MangerName = _dr[29].ToString();
                                            objBEUserDataUpLoad.ManagerPositionNumber = _dr[30].ToString();
                                            objBEUserDataUpLoad.ManagerPositionName = _dr[31].ToString();
                                            objBEUserDataUpLoad.EmploymentStatus = _dr[32].ToString();
                                            //        objBEUserDataUpLoad.Status = "SUCCESS";
                                            //        objBEUserDataUpLoad.Remarks = "Record saved successfully!";                                    
                                        }
                                        if (errMsg != "")
                                        {
                                            objBEUserDataUpLoad.EmployeeNumber = _dr[0].ToString();
                                            objBEUserDataUpLoad.FirstName = _dr[2].ToString();
                                            objBEUserDataUpLoad.LastName = _dr[3].ToString();
                                            //           objBEUserDataUpLoad.Status = "FAIL";
                                            //   objBEUserDataUpLoad.Remarks = errMsg;

                                            string Name = "";

                                            if (_dr[2].ToString() == "" & _dr[3].ToString() == "")
                                                Name = "Name Missing! ";
                                            else
                                                Name = _dr[2].ToString() + " " + _dr[3].ToString();


                                            LogData(dirPath, fileName, " \t  FAIL Record :  " + Name + ":" + errMsg, " FAIL Record :" + Name + ": " + errMsg + "                 " + now);


                                        }
                                        if (errMsg != "Token Number is missing!" && errMsg != "Invalid EmailID!" && errMsg != "Token Number and EmailID Missing!"
                                            && errMsg != "Token Number is missing! and Invalid EmailID!" && errMsg != "First Name is missing! and Required Field is Missing!"
                                            && errMsg != "First Name is missing! and Required Field is Missing!" && errMsg != "Names are missing! and Required Field is Missing!")
                                        {
                                            objBEUserDataUpLoadColl.Add(objBEUserDataUpLoad);
                                            successCounter = successCounter + 1;
                                        }
                                        else
                                        {
                                            errCounter = errCounter + 1;
                                        }
                                    }
                                }
                                //Call to function and it will return the no. of row inserted.
                                rowInserted = UpLoadExcel(objBEUserDataUpLoadColl, dirPath, fileName, dtMain);
                                if (rowInserted != "")
                                {
                                    // if (lblError.Text != "Please Upload Correct Excel File" && excelSheetNo != 1)
                                    lblError.Text = "" + rowInserted + "";
                                    txtSearchBox.Text = "";
                                    ShowGrid();
                                }
                                else
                                {
                                    lblError.Text = "Failure in Process";
                                    lblError.CssClass = "redClass";
                                }
                            }
                            else
                            {
                                ThrowAlertMessage("No Valid Data Found");
                                return;
                            }
                        }
                        else
                        {
                            ThrowAlertMessage("No Data Found in Correct Format");
                            return;
                        }
                    }
                    else
                    {
                        ThrowAlertMessage("No Data Found in Correct Format");
                        return;
                    }
                }
                else
                {
                    lblError.Text = "Please Select Only Excel File";
                    lblError.CssClass = "redClass";
                }
                //BulkUpload_CUD objbulk = new BulkUpload_CUD();
                //lblError.Text = "Plese do not press Refresh or Back button of your Browser...";
                //DataSet ds = objbulk.UploadProduct(dsGetImportedData, this.UserId);
                //ViewState["dsFailProducts"] = ds;
                //dsFailureProducts = ds.Tables[0];
                //BindData();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage("Please Check the Excel Sheet And Format"+ex.Message);
                // throw new ArgumentException(ex.Message);
                ex = null;
                return;
            }
            return;
        }

        private DataSet ImportExcelXLS(string FileName, bool hasHeaders)
        {
            DataSet output = new DataSet();
            try
            {
                FileStream stream = File.Open(FileName, FileMode.Open, FileAccess.Read);

                IExcelDataReader excelReader = null;
                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                if (Path.GetExtension(FileUpload1.PostedFile.FileName) == ".xls")
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                //else if (Path.GetExtension(FileUpload1.PostedFile.FileName) == ".xlsx")
                //{
                //    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                //}
                //...
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                //IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                //...
                //3. DataSet - The result of each spreadsheet will be created in the result.Tables
                //DataSet result = excelReader.AsDataSet();
                //...
                //4. DataSet - Create column names from first row
                excelReader.IsFirstRowAsColumnNames = true;
                output = excelReader.AsDataSet();
                output.Tables[0].TableName = "Sheet1$";
                excelReader.Close();
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return output;
        }
        #endregion

        #region function Upload Excel
        //In this function we are creating datatavble same fields like in Excel for importing then one by one row will create and data will be stored to table.
        public string UpLoadExcel(BE_UserExcelUploadColl objBEUserDataUpLoadColl, string dirpath, string FileName, DataTable dtMain)
        {
            try
            {
                object _stat;
                string messageforInsert = "", messageforUpdate = "";
                int _status = 0;
                DAL_Common objDALCommon = new DAL_Common();

                DataTable MasUserTable;
                DataTable ExcelTable;
                MasUserTable = objBL_UserExcelUpload.GetMasUserTable();
                ExcelTable = dtMain;

                var query = from MasUserTable2 in MasUserTable.AsEnumerable()
                            join ExcelTable2 in ExcelTable.AsEnumerable()
                            on MasUserTable2.Field<string>("TokenNumber") equals ExcelTable2.Field<string>("Employee Number")
                            where MasUserTable2.Field<string>("TokenNumber") == ExcelTable2.Field<string>("Employee Number")
                            select new
                            {
                                TokenNumber = MasUserTable2.Field<string>("TokenNumber")

                            };

                List<string> ExistTokenNumbers = new List<string>();
                foreach (var token in query)
                {
                    ExistTokenNumbers.Add(token.TokenNumber);
                }

                DataTable dtforInsert = new DataTable();
                dtforInsert.Columns.Add("FirstName", typeof(string));
                dtforInsert.Columns.Add("LastName", typeof(string));
                dtforInsert.Columns.Add("TokenNumber", typeof(string));
                dtforInsert.Columns.Add("ProcessCode", typeof(string));
                dtforInsert.Columns.Add("ProcessN", typeof(string));
                dtforInsert.Columns.Add("Division_PA", typeof(string));
                dtforInsert.Columns.Add("DivisionName_PA", typeof(string));
                dtforInsert.Columns.Add("Location_PSA", typeof(string));
                dtforInsert.Columns.Add("LocationName_PSA", typeof(string));
                dtforInsert.Columns.Add("DesigCode", typeof(string));
                dtforInsert.Columns.Add("Designation", typeof(string));
                dtforInsert.Columns.Add("EmailID", typeof(string));
                dtforInsert.Columns.Add("DOB", typeof(DateTime));
                dtforInsert.Columns.Add("Gender", typeof(string));
                dtforInsert.Columns.Add("DOJ", typeof(DateTime));
                dtforInsert.Columns.Add("EmpGradeCode_E", typeof(string));
                dtforInsert.Columns.Add("EmpGradeName_E", typeof(string));
                dtforInsert.Columns.Add("EmployeeLevelCode_ES", typeof(string));
                dtforInsert.Columns.Add("EmployeeLevelName_ES", typeof(string));
                dtforInsert.Columns.Add("OrgUnitCode", typeof(string));
                dtforInsert.Columns.Add("OrgUnitName", typeof(string));
                dtforInsert.Columns.Add("Levels", typeof(int));
                dtforInsert.Columns.Add("EmployeeName", typeof(string));
                dtforInsert.Columns.Add("EmployeeCostCenterNumber", typeof(string));
                dtforInsert.Columns.Add("EmployeeCostCenterName", typeof(string));
                dtforInsert.Columns.Add("CompanyCode", typeof(string));
                dtforInsert.Columns.Add("PayrollAreaText", typeof(string));
                dtforInsert.Columns.Add("PayrollAreaText1", typeof(string));
                dtforInsert.Columns.Add("MangerNumber", typeof(string));
                dtforInsert.Columns.Add("MangerName", typeof(string));
                dtforInsert.Columns.Add("ManagerPositionNumber", typeof(string));
                dtforInsert.Columns.Add("ManagerPositionName", typeof(string));
                dtforInsert.Columns.Add("EmploymentStatus", typeof(string));

                DataTable dtforUpdate = new DataTable();
                dtforUpdate.Columns.Add("FirstName", typeof(string));
                dtforUpdate.Columns.Add("LastName", typeof(string));
                dtforUpdate.Columns.Add("TokenNumber", typeof(string));
                dtforUpdate.Columns.Add("ProcessCode", typeof(string));
                dtforUpdate.Columns.Add("ProcessN", typeof(string));
                dtforUpdate.Columns.Add("Division_PA", typeof(string));
                dtforUpdate.Columns.Add("DivisionName_PA", typeof(string));
                dtforUpdate.Columns.Add("Location_PSA", typeof(string));
                dtforUpdate.Columns.Add("LocationName_PSA", typeof(string));
                dtforUpdate.Columns.Add("DesigCode", typeof(string));
                dtforUpdate.Columns.Add("Designation", typeof(string));
                dtforUpdate.Columns.Add("EmailID", typeof(string));
                dtforUpdate.Columns.Add("DOB", typeof(DateTime));
                dtforUpdate.Columns.Add("Gender", typeof(string));
                dtforUpdate.Columns.Add("DOJ", typeof(DateTime));
                dtforUpdate.Columns.Add("EmpGradeCode_E", typeof(string));
                dtforUpdate.Columns.Add("EmpGradeName_E", typeof(string));
                dtforUpdate.Columns.Add("EmployeeLevelCode_ES", typeof(string));
                dtforUpdate.Columns.Add("EmployeeLevelName_ES", typeof(string));
                dtforUpdate.Columns.Add("OrgUnitCode", typeof(string));
                dtforUpdate.Columns.Add("OrgUnitName", typeof(string));
                dtforUpdate.Columns.Add("Levels", typeof(int));
                dtforUpdate.Columns.Add("EmployeeName", typeof(string));
                dtforUpdate.Columns.Add("EmployeeCostCenterNumber", typeof(string));
                dtforUpdate.Columns.Add("EmployeeCostCenterName", typeof(string));
                dtforUpdate.Columns.Add("CompanyCode", typeof(string));
                dtforUpdate.Columns.Add("PayrollAreaText", typeof(string));
                dtforUpdate.Columns.Add("PayrollAreaText1", typeof(string));
                dtforUpdate.Columns.Add("MangerNumber", typeof(string));
                dtforUpdate.Columns.Add("MangerName", typeof(string));
                dtforUpdate.Columns.Add("ManagerPositionNumber", typeof(string));
                dtforUpdate.Columns.Add("ManagerPositionName", typeof(string));
                dtforUpdate.Columns.Add("EmploymentStatus", typeof(string));

                foreach (BE_UserExcelUpload objUserDataUpLoad in objBEUserDataUpLoadColl)
                {
                    if (ExistTokenNumbers.Count == 0)
                    {
                        DataRow _dr = dtforInsert.NewRow();

                        _dr["FirstName"] = objUserDataUpLoad.FirstName;
                        _dr["LastName"] = objUserDataUpLoad.LastName;
                        _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                        _dr["ProcessCode"] = objUserDataUpLoad.BUNo;
                        _dr["ProcessN"] = objUserDataUpLoad.BusinessUnit;
                        _dr["Division_PA"] = objUserDataUpLoad.PA;
                        _dr["DivisionName_PA"] = objUserDataUpLoad.PersonnelArea;
                        _dr["Location_PSA"] = objUserDataUpLoad.PSA;
                        _dr["LocationName_PSA"] = objUserDataUpLoad.PersonnelSubarea;
                        _dr["DesigCode"] = objUserDataUpLoad.EmployeePositionNumber;
                        _dr["Designation"] = objUserDataUpLoad.EmployeePositionName;
                        _dr["EmailID"] = objUserDataUpLoad.EmailID;

                        if (objUserDataUpLoad.DateofBirth != null)
                        {
                            _dr["DOB"] = Convert.ToDateTime(objUserDataUpLoad.DateofBirth);
                        }
                        else
                            _dr["DOB"] = DBNull.Value;
                        _dr["Gender"] = objUserDataUpLoad.Gender;

                        if (objUserDataUpLoad.DateofJoining != null)
                        {
                            _dr["DOJ"] = Convert.ToDateTime(objUserDataUpLoad.DateofJoining);
                        }
                        else
                            _dr["DOJ"] = DBNull.Value;


                        _dr["EmpGradeCode_E"] = objUserDataUpLoad.EG;
                        _dr["EmpGradeName_E"] = objUserDataUpLoad.EmployeeGroup;
                        _dr["EmployeeLevelCode_ES"] = objUserDataUpLoad.ESG;
                        _dr["EmployeeLevelName_ES"] = objUserDataUpLoad.EmployeeSubgroup;
                        _dr["OrgUnitCode"] = objUserDataUpLoad.EmployeeOrgUnitNumber;
                        _dr["OrgUnitName"] = objUserDataUpLoad.EmployeeOrgUnitName;
                        _dr["Levels"] = objUserDataUpLoad.Counter != null ? objUserDataUpLoad.Counter : 0;
                        _dr["EmployeeName"] = objUserDataUpLoad.EmployeeName;
                        _dr["EmployeeCostCenterNumber"] = objUserDataUpLoad.EmployeeCostCenterNumber;
                        _dr["EmployeeCostCenterName"] = objUserDataUpLoad.EmployeeCostCenterName;
                        _dr["CompanyCode"] = objUserDataUpLoad.CompanyCode;
                        _dr["PayrollAreaText"] = objUserDataUpLoad.PayrollAreaCode;
                        _dr["PayrollAreaText1"] = objUserDataUpLoad.PayrollAreaText;
                        _dr["MangerNumber"] = objUserDataUpLoad.MangerNumber;
                        _dr["MangerName"] = objUserDataUpLoad.MangerName;
                        _dr["ManagerPositionNumber"] = objUserDataUpLoad.ManagerPositionNumber;
                        _dr["ManagerPositionName"] = objUserDataUpLoad.ManagerPositionName;
                        _dr["EmploymentStatus"] = objUserDataUpLoad.EmploymentStatus;
                        //      _dr["Status"] = objUserDataUpLoad.Status;
                        // _dr["Remark"] = objUserDataUpLoad.Remarks;
                        dtforInsert.Rows.Add(_dr);
                    }
                    else
                    {

                        if (ExistTokenNumbers.Contains(objUserDataUpLoad.EmployeeNumber))
                        {
                            // Update

                            DataRow _dr = dtforUpdate.NewRow();

                            _dr["FirstName"] = objUserDataUpLoad.FirstName;
                            _dr["LastName"] = objUserDataUpLoad.LastName;
                            _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                            _dr["ProcessCode"] = objUserDataUpLoad.BUNo;
                            _dr["ProcessN"] = objUserDataUpLoad.BusinessUnit;
                            _dr["Division_PA"] = objUserDataUpLoad.PA;
                            _dr["DivisionName_PA"] = objUserDataUpLoad.PersonnelArea;
                            _dr["Location_PSA"] = objUserDataUpLoad.PSA;
                            _dr["LocationName_PSA"] = objUserDataUpLoad.PersonnelSubarea;
                            _dr["DesigCode"] = objUserDataUpLoad.EmployeePositionNumber;
                            _dr["Designation"] = objUserDataUpLoad.EmployeePositionName;
                            _dr["EmailID"] = objUserDataUpLoad.EmailID;

                            if (objUserDataUpLoad.DateofBirth != null)
                            {
                                _dr["DOB"] = Convert.ToDateTime(objUserDataUpLoad.DateofBirth);
                            }
                            else
                                _dr["DOB"] = DBNull.Value;
                            _dr["Gender"] = objUserDataUpLoad.Gender;

                            if (objUserDataUpLoad.DateofJoining != null)
                            {
                                _dr["DOJ"] = Convert.ToDateTime(objUserDataUpLoad.DateofJoining);
                            }
                            else
                                _dr["DOJ"] = DBNull.Value;


                            _dr["EmpGradeCode_E"] = objUserDataUpLoad.EG;
                            _dr["EmpGradeName_E"] = objUserDataUpLoad.EmployeeGroup;
                            _dr["EmployeeLevelCode_ES"] = objUserDataUpLoad.ESG;
                            _dr["EmployeeLevelName_ES"] = objUserDataUpLoad.EmployeeSubgroup;
                            _dr["OrgUnitCode"] = objUserDataUpLoad.EmployeeOrgUnitNumber;
                            _dr["OrgUnitName"] = objUserDataUpLoad.EmployeeOrgUnitName;
                            _dr["Levels"] = objUserDataUpLoad.Counter != null ? objUserDataUpLoad.Counter : 0;
                            _dr["EmployeeName"] = objUserDataUpLoad.EmployeeName;
                            _dr["EmployeeCostCenterNumber"] = objUserDataUpLoad.EmployeeCostCenterNumber;
                            _dr["EmployeeCostCenterName"] = objUserDataUpLoad.EmployeeCostCenterName;
                            _dr["CompanyCode"] = objUserDataUpLoad.CompanyCode;
                            _dr["PayrollAreaText"] = objUserDataUpLoad.PayrollAreaCode;
                            _dr["PayrollAreaText1"] = objUserDataUpLoad.PayrollAreaText;
                            _dr["MangerNumber"] = objUserDataUpLoad.MangerNumber;
                            _dr["MangerName"] = objUserDataUpLoad.MangerName;
                            _dr["ManagerPositionNumber"] = objUserDataUpLoad.ManagerPositionNumber;
                            _dr["ManagerPositionName"] = objUserDataUpLoad.ManagerPositionName;
                            _dr["EmploymentStatus"] = objUserDataUpLoad.EmploymentStatus;
                            //      _dr["Status"] = objUserDataUpLoad.Status;
                            // _dr["Remark"] = objUserDataUpLoad.Remarks;
                            dtforUpdate.Rows.Add(_dr);
                        }
                        else
                        {
                            //Insert

                            DataRow _dr = dtforInsert.NewRow();

                            _dr["FirstName"] = objUserDataUpLoad.FirstName;
                            _dr["LastName"] = objUserDataUpLoad.LastName;
                            _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                            _dr["ProcessCode"] = objUserDataUpLoad.BUNo;
                            _dr["ProcessN"] = objUserDataUpLoad.BusinessUnit;
                            _dr["Division_PA"] = objUserDataUpLoad.PA;
                            _dr["DivisionName_PA"] = objUserDataUpLoad.PersonnelArea;
                            _dr["Location_PSA"] = objUserDataUpLoad.PSA;
                            _dr["LocationName_PSA"] = objUserDataUpLoad.PersonnelSubarea;
                            _dr["DesigCode"] = objUserDataUpLoad.EmployeePositionNumber;
                            _dr["Designation"] = objUserDataUpLoad.EmployeePositionName;
                            _dr["EmailID"] = objUserDataUpLoad.EmailID;

                            if (objUserDataUpLoad.DateofBirth != null)
                            {
                                _dr["DOB"] = Convert.ToDateTime(objUserDataUpLoad.DateofBirth);
                            }
                            else
                                _dr["DOB"] = DBNull.Value;
                            _dr["Gender"] = objUserDataUpLoad.Gender;

                            if (objUserDataUpLoad.DateofJoining != null)
                            {
                                _dr["DOJ"] = Convert.ToDateTime(objUserDataUpLoad.DateofJoining);
                            }
                            else
                                _dr["DOJ"] = DBNull.Value;


                            _dr["EmpGradeCode_E"] = objUserDataUpLoad.EG;
                            _dr["EmpGradeName_E"] = objUserDataUpLoad.EmployeeGroup;
                            _dr["EmployeeLevelCode_ES"] = objUserDataUpLoad.ESG;
                            _dr["EmployeeLevelName_ES"] = objUserDataUpLoad.EmployeeSubgroup;
                            _dr["OrgUnitCode"] = objUserDataUpLoad.EmployeeOrgUnitNumber;
                            _dr["OrgUnitName"] = objUserDataUpLoad.EmployeeOrgUnitName;
                            _dr["Levels"] = objUserDataUpLoad.Counter != null ? objUserDataUpLoad.Counter : 0;
                            _dr["EmployeeName"] = objUserDataUpLoad.EmployeeName;
                            _dr["EmployeeCostCenterNumber"] = objUserDataUpLoad.EmployeeCostCenterNumber;
                            _dr["EmployeeCostCenterName"] = objUserDataUpLoad.EmployeeCostCenterName;
                            _dr["CompanyCode"] = objUserDataUpLoad.CompanyCode;
                            _dr["PayrollAreaText"] = objUserDataUpLoad.PayrollAreaCode;
                            _dr["PayrollAreaText1"] = objUserDataUpLoad.PayrollAreaText;
                            _dr["MangerNumber"] = objUserDataUpLoad.MangerNumber;
                            _dr["MangerName"] = objUserDataUpLoad.MangerName;
                            _dr["ManagerPositionNumber"] = objUserDataUpLoad.ManagerPositionNumber;
                            _dr["ManagerPositionName"] = objUserDataUpLoad.ManagerPositionName;
                            _dr["EmploymentStatus"] = objUserDataUpLoad.EmploymentStatus;
                            //      _dr["Status"] = objUserDataUpLoad.Status;
                            // _dr["Remark"] = objUserDataUpLoad.Remarks;
                            dtforInsert.Rows.Add(_dr);
                        }

                    }

                }
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@MasUser_Upload", dtforInsert);

                //function call Save data which will send table and stored procedure name it will return no. of row inserted.

                messageforInsert = objBL_UserExcelUpload.SaveExcel(objSqlParam[0], 0);    //// 0 for Insert and 1 for Update Records
                objSqlParam[0] = new SqlParameter("@MasUser_Upload", dtforUpdate);
                messageforUpdate = objBL_UserExcelUpload.SaveExcel(objSqlParam[0], 1);
                LogData(dirpath, FileName, " \t", " ");  //for New Line
                LogData(dirpath, FileName, "\n", "Number of Failed Records  : " + errCounter);
                LogData(dirpath, FileName, "\n", "Number of Success Records : " + successCounter);
                //LogData(dirpath, FileName, "UpLoaded RowCount : " + _status, "UpLoaded RowCount : " + _status);
                LogData(dirpath, FileName, " " + messageforInsert, "" + messageforInsert);
                LogData(dirpath, FileName, " " + messageforUpdate, "" + messageforUpdate);
                string message = messageforInsert + "<br /> " + messageforUpdate;
                return message;
            }
            catch (Exception ex)
            {
                ThrowAlertMessage("Please Check the Excel Sheet And Format" + ex.Message);
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region function Check Blank()
        private Boolean CheckBlank(DataRow dr)
        {
            bool b = false;
            foreach (var item in dr.ItemArray)
            {
                if (item.ToString() != "")
                {
                    b = true;
                    break;
                }
            }
            return b;
        }
        #endregion

        #region function Check Excel Format()
        private string CheckExcelFormat(DataTable dt)
        {
            excelSheetNo = excelSheetNo + 1;
            string excel = "blank";
            foreach (var item in dt.Columns)
            {
                if (dt.Columns[0].ToString() == "Employee Number" && dt.Columns[1].ToString() == "Employee Name" && dt.Columns[2].ToString() == "First Name")
                {
                    excel = "valid";
                    break;
                }

            }
            return excel;
        }
        #endregion

        #region LogData Function
        private static void LogData(string dirpath, string FileName, string strMessage, string strDisplayMessage = "")
        {
            StreamWriter logFile;
        A:
            try
            {
                string FilePath = dirpath + "\\" + FileName;

                if (!File.Exists(FilePath.ToString()))               //("Log.txt"))
                {
                    logFile = new StreamWriter(FilePath);
                }
                else
                {
                    logFile = File.AppendText(FilePath.ToString());
                }
                logFile.WriteLine(strDisplayMessage);



                logFile.Close();
            }
            catch (Exception ex)
            {
                goto A;
            }
        }
        #endregion

        #region Upload File
        public String UploadFile()
        {
            if (FileUpload1.HasFile)
            {
                string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string fileLocation = Server.MapPath("~/Uploads/" + fileName);
                FileUpload1.SaveAs(fileLocation);
                return (fileLocation);
            }
            else
            {
                return "";
            }

        }
        #endregion

        #region Validate Row
        private string ValidateRow(DataRow _dr, string dirpath, string FileName)
        {
            string errMsg = "";
            try
            {
                if (_dr[2].ToString() == "" && _dr[3].ToString() == "")
                {
                    if (_dr[0].ToString() == "" || _dr[13].ToString() == "")
                    {
                        errMsg = "Names are missing! and Required Field is Missing!";
                    }
                    else
                    {
                        errMsg = "Names are Missing!";
                    }

                }
                else if (_dr[2].ToString() == "")
                {
                    if (_dr[0].ToString() == "" || _dr[13].ToString() == "")
                    {
                        errMsg = "First Name is missing! and Required Field is Missing!";
                    }
                    else
                    {
                        errMsg = "First Name is missing!";
                    }

                }
                else if (_dr[3].ToString() == "")
                    errMsg = "Last Name is missing!";
                else if (_dr[0].ToString() == "")
                {
                    if (_dr[13].ToString() == "")
                    { errMsg = "Token Number and EmailID Missing!"; }
                    else
                    { errMsg = "Token Number is missing!"; }
                }
                else if (_dr[9].ToString() == "" || _dr[10].ToString() == "")
                    errMsg = "Division field is missing!";
                else if (_dr[11].ToString() == "" || _dr[11].ToString() == "")
                    errMsg = "Location field is missing!";
                else if (_dr[8].ToString() == "" || _dr[6].ToString() == "")
                    errMsg = "Employee Level field is missing!";
                else
                {
                    string DOB = _dr[25].ToString();
                    try
                    {
                        CultureInfo UsEn = new CultureInfo("de-DE");
                        DateTime dt = Convert.ToDateTime(_dr[25].ToString(), UsEn);
                        //DateTime dt = DateTime.ParseExact(DOB, "dd.MM.yyyy",null);
                        DOB = dt.ToString("dd/MM/yyyy");
                    }
                    catch (Exception ex)
                    {
                        errMsg = "Date of Birth is invalid! Valid date format is dd/MM/yyyy";
                    }

                    string DOJ = _dr[24].ToString();
                    try
                    {
                        CultureInfo UsEn = new CultureInfo("de-DE");
                        DateTime dt = Convert.ToDateTime(_dr[24].ToString(), UsEn);
                        DOJ = dt.ToString("dd/MM/yyyy");
                    }
                    catch (Exception ex)
                    {
                        errMsg = "Date of Joining is invalid! Valid date format is dd/MM/yyyy";
                    }
                }


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return errMsg;
        }
        #endregion

        #region Email Validation
        private bool isEmailValid(string emailID)
        {
            bool isValid = false;
            string pattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            try
            {
                Regex myRegex = new Regex(pattern);
                isValid = myRegex.IsMatch(emailID);
            }
            catch (Exception exp)
            {
            }
            return isValid;
        }
        #endregion

        #region Search Button Click
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblError2.Text = "";
                clearFields();
                lblDetailsList.Visible = false;
                objBE_UserExcelUploadColl = objBL_UserExcelUpload.GetDetailsByTokenNumber(txtSearchBox.Text);
                grdExcelUpload.DataSource = objBE_UserExcelUploadColl;
                Session["dataset"] = objBE_UserExcelUploadColl;
                grdExcelUpload.DataBind();
                if (objBE_UserExcelUploadColl.Count <= 0)
                {
                    lblError2.Text = "Please Enter the Valid Token Number / Name !";
                    lblError2.CssClass = "redClass";
                    formUpdatePanel.Visible = false;
                }
                else
                {
                    lblDetailsList.Visible = true;
                    lblDetailsList.Text = "Details of Employee ";
                    formUpdatePanel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region Grid events
        protected void grdExcelUpload_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }
        }
        protected void grdExcelUpload_EditRow(object sender, GridViewEditEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }
        }
        protected void grdExcelUpload_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdExcelUpload.PageIndex = e.NewPageIndex;
                if (Session["dataset"] != null)
                {
                    grdExcelUpload.DataSource = Session["dataset"];
                    grdExcelUpload.DataBind();

                }
                else
                {
                    ShowGrid();
                }

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }
        }
        protected void grdExcelUpload_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {



            }
        }
        #endregion

        #region   throw a  java script alert message
        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            return;
        }
        #endregion

        #region ShowGrid With Latest Upload
        private void ShowGrid()
        {
            try
            {
                lblError2.Text = "";
                objBE_UserExcelUploadColl = objBL_UserExcelUpload.GetDetailsAfterUpload();
                grdExcelUpload.DataSource = objBE_UserExcelUploadColl;
                grdExcelUpload.DataBind();
                Session["dataset"] = objBE_UserExcelUploadColl;
                if (objBE_UserExcelUploadColl.Count <= 0)
                {
                    lblError2.Text = "No Records Modified Today";
                }
                else
                {
                    lblDetailsList.Visible = true;
                    lblDetailsList.Text = "Records Modified Today";
                }

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region SaveData function for actual data insertion in Table.
        ////      private string SaveData(string StoredProcName, DataTable InputTable)
        //      {
        //          int rowAffect = 0;
        //          string Message = "";
        //          SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MM_Connection"].ToString());
        //          SqlCommand cmd = new SqlCommand();
        //          SqlDataReader dr;
        //          con.Open();
        //          cmd.Connection = con;
        //          cmd.CommandText = StoredProcName;
        //          cmd.CommandType = CommandType.StoredProcedure;
        //          cmd.Parameters.AddWithValue("@MasUser_Upload", InputTable);
        //          SqlParameter InsertRow = cmd.Parameters.Add("@InserRowCount", SqlDbType.Int);
        //          InsertRow.Direction = ParameterDirection.Output;
        //          //SqlParameter UpdateRow = cmd.Parameters.Add("@UpdateRowCount", SqlDbType.Int);
        //          //UpdateRow.Direction = ParameterDirection.Output;
        //          rowAffect = cmd.ExecuteNonQuery();
        //          //dr = cmd.ExecuteReader();  
        //          if (InsertRow.Value.ToString() == "")
        //              InsertRow.Value = 0;
        //          //if (UpdateRow.Value.ToString() == "")
        //          //    UpdateRow.Value = 0;

        //          if (rowAffect >= 0)
        //          {
        //              Message = "Total Number of Inserted Records are " + InsertRow.Value.ToString();
        //          }
        //          else
        //          {
        //              Message = "Failure in Process Check the Excel Sheet and Try Again";
        //          }
        //          con.Close();
        //          return Message;
        //      }
        #endregion

        #region Edit Button Click

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                formUpdatePanel.Visible = true;
                clearFields();
                LinkButton btn = sender as LinkButton;
                GridViewRow item = btn.NamingContainer as GridViewRow;
                string Id = grdExcelUpload.DataKeys[item.RowIndex].Value.ToString();
                fillDetails(Id);
                txtTokenNumber.Text = item.Cells[0].Text;
                tokenNumber = item.Cells[0].Text;
                txtFirstName.Text = item.Cells[5].Text;
                txtLastName.Text = item.Cells[6].Text;
                DropEmpDetails.SelectedValue = item.Cells[7].Text;
                //  DropEmpSubDetails.SelectedValue = item.Cells[9].Text;
                // Label ltrl = item.Cells[9].Controls[1] as Label;
                ListItem SelectedItem = DropEmpSubDetails.Items.FindByText(item.Cells[10].Text);
                SelectedItem.Selected = true;
                DropDivisionDetails.SelectedValue = item.Cells[8].Text;
                DropLocationDetails.SelectedValue = item.Cells[12].Text;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Fill the DropdownList with pre details.

        public void fillDetails(string TokenNumber)
        {
            try
            {
                objBE_UserExcelUploadColl = objBL_UserExcelUpload.GetEmployeeGroup();
                objBE_UserExcelUploadColl2 = objBL_UserExcelUpload.GetDivisionPA();
                objBE_UserExcelUploadColl3 = objBL_UserExcelUpload.GetEmployeeSubGroup();
                objBE_UserExcelUploadColl4 = objBL_UserExcelUpload.GetLocationPSA();
                lblError2.Text = "";
                DropEmpDetails.DataSource = objBE_UserExcelUploadColl;
                DropEmpDetails.DataTextField = "EmpGradeName_E";
                DropEmpDetails.DataValueField = "EmpGradeCode_E";
                DropEmpDetails.DataBind();
                DropDivisionDetails.DataSource = objBE_UserExcelUploadColl2;
                DropDivisionDetails.DataTextField = "DivisionName_PA";
                DropDivisionDetails.DataValueField = "Division_PA";
                DropDivisionDetails.DataBind();

                DropEmpSubDetails.DataSource = objBE_UserExcelUploadColl3;
                for (int count = 0; count < objBE_UserExcelUploadColl3.Count; count++)
                {
                    objBE_UserExcelUploadColl3[count].Levels = objBE_UserExcelUploadColl3[count].EmployeeLevelCode_ES + "|" + objBE_UserExcelUploadColl3[count].Levels;
                }

                DropEmpSubDetails.DataTextField = "EmployeeLevelName_ES";
                DropEmpSubDetails.DataValueField = "Levels";
                DropEmpSubDetails.DataBind();
                DropLocationDetails.DataSource = objBE_UserExcelUploadColl4;
                DropLocationDetails.DataTextField = "LocationName_PSA";
                DropLocationDetails.DataValueField = "Location_PSA";
                DropLocationDetails.DataBind();
            }

            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region Button Save Record
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string tokenNumberPrevious = "", TokenNumberNext = "", FirstName = "", LastName = "", EmpGradeName_E = "", EmployeeLevelCode_ES = "",
                EmployeeLevelName_ES = "", Levels = "", Division_PA = "", DivisionName_PA = "", LocationName_PSA = "", Location_PSA = "", message = "";

                string EmpGradeCode_E = "";
                tokenNumberPrevious = tokenNumber;
                TokenNumberNext = txtTokenNumber.Text;
                FirstName = txtFirstName.Text;
                LastName = txtLastName.Text;
                EmpGradeCode_E = DropEmpDetails.SelectedValue.ToString();
                EmpGradeName_E = DropEmpDetails.SelectedItem.Text;
                //EmployeeLevelCode_ES = DropEmpSubDetails.SelectedValue.ToString();
                EmployeeLevelName_ES = DropEmpSubDetails.SelectedItem.Text;

                string[] i = DropEmpSubDetails.SelectedValue.Split(new char[] { '|' });
                EmployeeLevelCode_ES = (i[0]);
                Levels = i[1];
                //Levels = DropEmpSubDetails.SelectedValue.ToString();
                Division_PA = DropDivisionDetails.SelectedValue.ToString();
                DivisionName_PA = DropDivisionDetails.SelectedItem.Text;
                Location_PSA = DropLocationDetails.SelectedValue.ToString();
                LocationName_PSA = DropLocationDetails.SelectedItem.Text;

                message = objBL_UserExcelUpload.UpdateRecord(TokenNumberNext, tokenNumberPrevious, FirstName, LastName, EmpGradeCode_E, EmpGradeName_E,
                    EmployeeLevelCode_ES, EmployeeLevelName_ES, Levels, Division_PA, DivisionName_PA, LocationName_PSA, Location_PSA);
                if (message != null && message != "")
                {
                    if (message == "2")
                        LblErrorSave.Text = "Token Number is Already Exist!";
                    else
                    {
                        LblErrorSave.Text = "Record Updated Successfully!";
                        formUpdatePanel.Visible = false;
                    }
                    txtSearchBox.Text = "";
                    ShowGrid();


                }
                else
                {

                    LblErrorSave.Text = "Failure in Process! Please Try Again ";
                    ShowGrid();
                }
            }

            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "UserExcelUpload.aspx");
            }

        }
        #endregion

        #region Button Cancel
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            clearFields();
            formUpdatePanel.Visible = false;
        }
        #endregion

        #region Separation of Table into 2 tables one for Exist tokens and non Exist

        //  public bool SeparateTables(DataTable dtMain)
        // {
        //DataSet ds= null;
        //DataTable MasUserTable;
        //DataTable ExcelTable1;
        //MasUserTable = objBL_UserExcelUpload.GetMasUserTable();
        //ExcelTable1 = dtMain;            

        //    var query = from MasUserTable2 in MasUserTable.AsEnumerable()
        //                join ExcelTable2 in ExcelTable1.AsEnumerable()
        //                on MasUserTable2.Field<string>("TokenNumber") equals ExcelTable2.Field<string>("Employee Number")
        //                where MasUserTable2.Field<string>("TokenNumber") == ExcelTable2.Field<string>("Employee Number")
        //                select new
        //                {
        //                    TokenNumber = MasUserTable2.Field<string>("TokenNumber")

        //                };

        //    //var query2 = from ExcelTable2 in ExcelTable .AsEnumerable()
        //    //             join MasUserTable2 in MasUserTable.AsEnumerable()
        //    //             on ExcelTable2.Field<string>("Employee Number") equals MasUserTable2.Field<string>("TokenNumber")
        //    //             where MasUserTable2.Field<string>("TokenNumber") != ExcelTable2.Field<string>("Employee Number")
        //    //            select new
        //    //            {
        //    //                TokenNumber = ExcelTable2.Field<string>("Employee Number")

        //    //            };
        //    //var query3 = from  ExcelTable2 in ExcelTable.AsEnumerable()
        //    //             join  MasUserTable2 in MasUserTable.AsEnumerable()
        //    //             on    ExcelTable2.Field<string>("Employee Number")  equals MasUserTable2.Field<string>("TokenNumber")
        //    //             where !ExcelTable2.Field<string>("Employee Number").Contains(MasUserTable2.Field<string>("TokenNumber")) 
        //    //             select new
        //    //             {
        //    //                 TokenNumber = ExcelTable2.Field<string>("Employee Number")

        //    //             };

        //    var qry1 = MasUserTable.AsEnumerable().Select(a => new { TokenNumber = a["TokenNumber"].ToString() });
        //    List<string> ExistTokenNumbers = new List<string>();
        //   // ExistTokenNumbers = query.ToList<string>();
        // foreach (var grade in query)
        //{
        //    ExistTokenNumbers.Add(grade.TokenNumber);
        //}


        //   // List<> query2 = qry1.AsEnumerable().Select(b => new { TokenNumber2 = b.TokenNumber.ToString() });
        //    //var qry2 = ExcelTable.AsEnumerable().Select(b => new { TokenNumber = b["Employee Number"].ToString() });

        //    //var exceptAB = qry2.Except(qry1);

        //    //var dtMisMatch = (from a in ExcelTable.AsEnumerable()
        //    //                        join ab in exceptAB on a["Employee Number"].ToString() equals ab.TokenNumber
        //    //                        select a);




        //return true;
        // }

        #endregion

        #region Clear all fields
        public void clearFields()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtTokenNumber.Text = "";
            DropLocationDetails.Items.Clear();
            DropDivisionDetails.Items.Clear();
            DropEmpDetails.Items.Clear();
            DropEmpSubDetails.Items.Clear();
        }
        #endregion
    }
}
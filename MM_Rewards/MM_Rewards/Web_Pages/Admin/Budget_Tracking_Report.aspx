﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Budget_Tracking_Report.aspx.cs" Inherits="MM_Rewards.Web_Pages.Admin.Budget_Tracking_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <style type="text/css">
        .style1
        {
            font-size: large;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function onCalendarShown(sender, args) {
            sender._switchMode("years", true);
        }

        function validateQty(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        };

    </script>
    <script type="text/javascript">
        function cal() {
            var a = document.getElementById('<%= txtBudget.ClientID %>').value;
            var b = document.getElementById('<%= txtTotalBudget.ClientID %>').value;
            if (b != '' && a != '' && parseInt(b) != 0) {
                var total = (parseFloat(a) / parseFloat(b)) * 100
                document.getElementById('<%= txtPercentage.ClientID %>').value = parseFloat(total).toFixed(4);
                //alert(document.getElementById('<%= txtPercentage.ClientID %>').value = parseFloat(total).toFixed(2));
                document.getElementById('<%= hdnpercentage.ClientID %>').value = document.getElementById('<%= txtPercentage.ClientID %>').value;
                alert(document.getElementById('<%= hdnpercentage.ClientID %>').value);
            }
        }
    </script>
    <div align="center">
        <h1>
            Budget Tracking Utilization</h1>
        <div class="grid01">
            <asp:HiddenField ID="hdnSelectedRowIndex" runat="server" />
            <asp:GridView ID="grdAwardList" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdAwardList_RowDataBound"
                DataKeyNames="ID" AlternatingRowStyle-CssClass="row01" ViewStateMode="Enabled">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                    <%--  <itemtemplate>
                            <asp:LinkButton ID="lnkSelect" CommandArgument='<%# Eval("ID")  %>' CommandName="Select"
                                OnClick="lnkSelect_Click" runat="server" CssClass="selLink" TabIndex="1">
                                            Select</asp:LinkButton>
                        </itemtemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <PagerStyle CssClass="dataPager" />
                <SelectedRowStyle CssClass="selectedoddTd" />
            </asp:GridView>
        </div>
        <table style="width: 70%" cellpadding="5" cellspacing="10">
            <tr>
                <td align="right">
                    From
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlfrmdate" runat="server" Width="150px">
                    </asp:DropDownList>
                </td>
                <td rowspan="1">
                    To
                </td>
                <td>
                    <asp:DropDownList ID="ddltodate" runat="server" Width="150px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Total Budget
                </td>
                <td align="left">
                    <asp:TextBox ID="txtTotalBudget" runat="server" Text="129114950" ReadOnly="true"
                        ClientIDMode="Static"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Award Category
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlAwardCat" runat="server" Width="150px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Budget For Each Award
                </td>
                <td align="left">
                    <asp:TextBox ID="txtBudget" runat="server" onblur="javascript: cal();" onkeypress='return validateQty(event);'
                        ClientIDMode="Static"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Percentage
                </td>
                <td align="left">
                    <asp:TextBox ID="txtPercentage" runat="server" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                </td>
                <td align="left">
                    <div class="btnRed" align="center">
                        <asp:Button ID="btnBudgetTracking" CssClass="btnLeft" runat="server" Text="Submit"
                            TabIndex="3" OnClick="btnBudgetTracking_Click" /></div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hdnid" runat="server" />
                    <asp:HiddenField ID="hdnDivisionId" runat="server" />
                    <asp:HiddenField ID="hdnAwardTypeid" runat="server" />
                    <asp:HiddenField ID="hdnpercentage" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

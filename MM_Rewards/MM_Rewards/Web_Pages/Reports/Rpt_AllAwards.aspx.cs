﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MM.DAL;
using Microsoft.Reporting.WebForms;
using BL_Rewards.BL_Common;
using BE_Rewards.BE_Rewards;

namespace MM_Rewards.Web_Pages.Reports
{
    public partial class Rpt_AllAwards : System.Web.UI.Page
    {
        bool Isrnr;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               BL_Rewards.BL_Common.Common objCommon = new Common();    
               LoadDivisionDropDown();
               
               if (Session["TokenNumber"] == null)
               {
                   Response.Redirect("/Web_Pages/HomePage.aspx");
               }
               else
               {
                   //if (objCommon.GetRoles(Session["TokenNumber"].ToString()) == 1)
                   //{
                   //    ddl_division.SelectedValue = Session["Division"].ToString();
                   //    ddl_division.Enabled = false;
                   //}
               }
               //LoadLocationDropDown();

            }
        }

        private void LoadDivisionDropDown()
        {

            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();
            ddl_division.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, Session["TokenNumber"].ToString());
            ddl_division.DataValueField = "DivisionCode_PA";
            ddl_division.DataTextField = "DivisionName_PA";
            ddl_division.DataBind();
            hndIsrnr.Value = Isrnr.ToString();
            ddl_division.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
            if (!Isrnr)
            {
                ddl_division.Items.Insert(1, new ListItem("ALL", "ALL"));
                //  ddl_division.SelectedIndex = 0;
            }
            ddl_division.SelectedIndex = 0;
        }

        private void LoadLocationDropDown()
        {
            if (ddl_division.SelectedIndex > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                ddl_Location.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), ddl_division.SelectedValue.ToString());
                ddl_Location.DataValueField = "LocationCode_PSA";
                ddl_Location.DataTextField = "LocationName_PSA";
                ddl_Location.DataBind();
                ddl_Location.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
                if (!(Convert.ToBoolean(hndIsrnr.Value)))
                {
                    ddl_Location.Items.Insert(1, new ListItem("ALL", "ALL"));
                }
                ddl_Location.SelectedIndex = 0;
            }
            else
            {
                ddl_Location.Items.Clear();
            }
        } 

        private void LoadDepartmentDropDown()
        {
            if
                (ddl_division.SelectedIndex != 0 && ddl_Location.SelectedIndex != 0)
            {

                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();

                ddl_Department.DataSource = objCommon.LoadDepartmentDropDown(ddl_Location.SelectedValue, ddl_division.SelectedValue,Session["TokenNumber"].ToString());
                ddl_Department.DataValueField = "DivisionCode_PA";
                ddl_Department.DataTextField = "DivisionName_PA";
                ddl_Department.DataBind();
                ddl_Department.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
                if (!(Convert.ToBoolean(hndIsrnr.Value)))
                {
                    ddl_Department.Items.Insert(1, new ListItem("ALL", "ALL"));
                }
                    ddl_Department.SelectedIndex = 0;
            }
            else 
            {
                ddl_Department.Items.Clear();
            }
        }

        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            try
            {
                AllAwardrptviewer.LocalReport.Refresh();
               rpthold.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void ddl_division_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                LoadLocationDropDown();
                LoadDepartmentDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void ddl_Location_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadDepartmentDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Web.UI;
using System.Web.UI.WebControls;
using MM.DAL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BL_Rewards.BL_Common;

namespace MM_Rewards
{
    public partial class Rpt_AwardDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BL_Rewards.BL_Common.Common.CheckUserLoggedIn();
            Common.CheckPageAccess();

            if(!IsPostBack)
            {
                LoadLocationDropDown();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ReportViewer1.LocalReport.Refresh();
            MyReport.Visible = true;
            ReportViewer1.Visible = true;

        }

        private void LoadLocationDropDown()
        {
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            _ds = objDAL.DAL_GetData("usp_Mas_Division_View");
            ddlDivision.DataSource = _ds;
            ddlDivision.DataValueField = "Division_PA";
            ddlDivision.DataTextField = "DivisionName_PA";
            ddlDivision.DataBind();
            ddlDivision.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
            ddlDivision.SelectedIndex = 0;
        }

        protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@Location", ddlDivision.SelectedItem.Value);
            _ds = objDAL.DAL_GetData("usp_GetPlantFromDivision", param);
            ddlLocation.DataSource = _ds;
            ddlLocation.DataValueField = "PlantName_PSA";
            ddlLocation.DataTextField = "PlantName_PSA";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("-- Select --", "0"));
            ddlLocation.SelectedIndex = 0;
        }

      
    }
}
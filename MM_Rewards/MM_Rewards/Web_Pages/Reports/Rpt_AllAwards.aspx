﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="Rpt_AllAwards.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_AllAwards" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
    <script type="text/javascript">

        function ValidatePage() {

            var errMsg = '';
            var vddl;
            var dpStartDate;
            var dpEndDate;
            vddl = document.getElementById('<%=ddl_division.ClientID %>');
            errMsg += CheckDropdown_Blank(vddl.value, " Division ");
            vddl = document.getElementById('<%=ddl_Location.ClientID %>');
            errMsg += CheckDropdown_Blank(vddl.value, " Location");
            vddl = document.getElementById('<%=ddl_Department.ClientID %>');
            errMsg += CheckDropdown_Blank(vddl.value, " Department");
         
            dpStartDate = $find('<%=dtpStartdate.ClientID %>');
            dpEndDate = $find('<%=dtpEnddate.ClientID %>');

            errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");



            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }
    </script> 
<div class="form01" >
     <table >
      <tr>
                <td colspan="3">
                    <h1>ALL Awards</h1></td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label> <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddl_division" runat="server" TabIndex="1" 
                        AutoPostBack="True" onselectedindexchanged="ddl_division_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblLoaction" runat="server" Text="Location"></asp:Label>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddl_Location" runat="server"  TabIndex="2" AutoPostBack="True" onselectedindexchanged="ddl_Location_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:DropDownList ID="ddl_Department" runat="server"  TabIndex="3">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    Start Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="4">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    End Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="5">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
            <td></td>
                <td></td>
                    <td>
                    <div class="btnRed">
                               <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" OnClick="btnGetDetails_Click"
                        Text="Generate Report" TabIndex="6" OnClientClick="return ValidatePage()" />
                        </div>
                </td>
            </tr>
            <tr>
            <td colspan="3"><asp:HiddenField ID="hndIsrnr" runat="server" /></td>
            </tr>
           </table>
    
           </div>
           <div class="reportHold" runat="server" id="rpthold">
               <rsweb:ReportViewer ID="AllAwardrptviewer" runat="server" InteractiveDeviceInfos="(Collection)" Width="100%" >
                   <LocalReport ReportPath="Web_Pages\Reports\AllAwards.rdlc">
                       <DataSources>
                           <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                               Name="ds_AllAwardsrppt" />
                       </DataSources>
                   </LocalReport>
               </rsweb:ReportViewer>

               <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                   SelectMethod="GetAllAward" TypeName="BL_Rewards.BL_Reports.Reports" 
                   OldValuesParameterFormatString="original_{0}">
                   <SelectParameters>
                       <asp:ControlParameter ControlID="ddl_division" Name="Division" 
                           PropertyName="SelectedValue" Type="String" />
                       <asp:ControlParameter ControlID="ddl_Location" DefaultValue="-- Select --" 
                           Name="Location" PropertyName="SelectedValue" Type="String" />
                       <asp:ControlParameter ControlID="ddl_Department" Name="OrgUnitCode" DefaultValue="-- Select --" 
                           PropertyName="SelectedValue" Type="String" />
                       <asp:ControlParameter ControlID="dtpStartdate" DefaultValue="" Name="start" 
                           PropertyName="SelectedDate" Type="String" />
                       <asp:ControlParameter ControlID="dtpEnddate" Name="end" 
                           PropertyName="SelectedDate" Type="String" />
                   </SelectParameters>
               </asp:ObjectDataSource>
           </div>
                
</asp:Content>

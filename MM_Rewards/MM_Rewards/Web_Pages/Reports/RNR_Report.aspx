﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" CodeBehind="RNR_Report.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.RNR_Report" %>
<%@ Register Src="~/UserControls/RNRReport.ascx" TagPrefix="uc1" TagName="RNRReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
<style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:RNRReport runat="server" id="ucRNRReport" />
</asp:Content>

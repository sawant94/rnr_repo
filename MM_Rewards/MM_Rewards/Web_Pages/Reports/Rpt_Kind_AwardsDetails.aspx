﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Rpt_Kind_AwardsDetails.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_Kind_AwardsDetails" %>

<%@ Register Src="../../UserControl/ReportControl.ascx" TagName="ReportControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
<h1>
                        Kind Awards Details</h1>
    <uc1:ReportControl ID="RptKindAward" runat="server" />
</asp:Content>

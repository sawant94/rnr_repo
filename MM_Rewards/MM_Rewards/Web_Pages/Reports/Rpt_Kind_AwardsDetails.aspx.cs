﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_Rewards.BL_Common;

namespace MM_Rewards.Web_Pages.Reports
{
    public partial class Rpt_Kind_AwardsDetails : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        Common objCommon = new Common();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["rid"] != null)
                    {
                        RptKindAward.Rid = Request.QueryString["rid"];
                    }
                }
            }
            catch (Exception ex)
            {
                //  ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_Home.aspx");
            }
        }
    }
}
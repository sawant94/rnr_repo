﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;

namespace MM_Rewards.Web_Pages.Reports
{
    public partial class RNR_Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                User thisPageUser;
                string tokennumber = Request.QueryString["tokennumber"];
                if (string.IsNullOrWhiteSpace(tokennumber))
                {
                    thisPageUser = WebContext.Current.CurrentUser;
                }
                else
                {
                    thisPageUser = WebContext.Current.GetUserByTokenNumber(tokennumber);
                }
            }
            catch (Exception ex)
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Success", "<script type='text/javascript'>alert('Vehicle Model is not mapped against VIN No, Please map Vehicle Model according to material assigned to Vehicle');</script>'");
            }
        }
    }
}
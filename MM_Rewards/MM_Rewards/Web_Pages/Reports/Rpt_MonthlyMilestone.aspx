﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true" 
    CodeBehind="Rpt_MonthlyMilestone.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_MonthlyMilestone" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
    <style type="text/css">
        .btndownloadexcel {
            padding: 5px;
            color: #FFF;
            background-color: brown;
            border: 1px solid brown;
            cursor: pointer;
            font-weight: bold;
            margin-right: 10px;
        }

        .ddlmonth {
            width: 60px;
            margin-right: 10px;
            height: 30px;
        }

        .lblmessage {
            font-weight: bold;
        }
    </style>

     <script type="text/javascript">

         function btnGet_OnClientClick() {
             var dtpStartdate = $find('<%#dtpStartdate.ClientID %>');
            var dtpEnddate = $find('<%#dtpEnddate.ClientID %>');

            var err = '';
            err += Check_StartEndDate(dtpStartdate.get_selectedDate() == null ? '' : dtpStartdate.get_selectedDate(), dtpEnddate.get_selectedDate() == null ? '' : dtpEnddate.get_selectedDate(), "Start", "End");

            if (err != "") {
                alert(err);
                return false;
            }
            return true;
        }

        function Check_StartEndDate(SDate, EDate, strStart, strEnd) {
            var endDate = new Date(EDate);
            var startDate = new Date(SDate);

            var err = '';

            err += chkDate_Blank(SDate, strStart + " Date");
            err += chkDate_Blank(EDate, strEnd + " Date");

            if (SDate != '' && EDate != '' && startDate > endDate) {
                err += "Please ensure that the " + strEnd + " Date is greater than or equal to the " + strStart + " Date.";
            }

            return err;
        }

        function chkDate_Blank(strValue, strSuffix) {
            if (strValue == '') {
                return 'Please Select ' + strSuffix + ' \n';
            }
            else {
                return '';
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

     <%-- <asp:DropDownList ID="ddlyear" runat="server" CssClass="ddlmonth"></asp:DropDownList>
    <asp:DropDownList ID="ddlmonth" runat="server" CssClass="ddlmonth">
        <asp:ListItem Value="1">Jan</asp:ListItem>
        <asp:ListItem Value="2">Feb</asp:ListItem>
        <asp:ListItem Value="3">Mar</asp:ListItem>
        <asp:ListItem Value="4">Apr</asp:ListItem>
        <asp:ListItem Value="5">May</asp:ListItem>
        <asp:ListItem Value="6">Jun</asp:ListItem>
        <asp:ListItem Value="7">Jul</asp:ListItem>
        <asp:ListItem Value="8">Aug</asp:ListItem>
        <asp:ListItem Value="9">Spet</asp:ListItem>
        <asp:ListItem Value="10">Oct</asp:ListItem>
        <asp:ListItem Value="11">Nov</asp:ListItem>
        <asp:ListItem Value="12">Dec</asp:ListItem>
    </asp:DropDownList>--%>



    <div class="form01" style="width: 100%; overflow-x: auto; padding: 15px 0;">
            <table style="width: 80%; margin: 0 auto;" class="tbl_awdme">
                <tr>
                    <td class="lblName">
                        Start Date
                    </td>
                    <td class="hidden-xs"> :</td>
                    <td>
                        <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="1">
                        </telerik:RadDatePicker>
                    </td>

                    <td class="lblName">
                        End Date 
                    </td>
                     <td class="hidden-xs"> :</td>
                    <td>
                        <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                            TabIndex="2">
                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DateInput DisplayDateFormat="dd/MM/yyyy" DateFormat="dd/MM/yyyy" TabIndex="2">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="2"></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>

                   
                    <td >
                        <div class="btnRed">
                            <asp:Button ID="btnExportToExcel" runat="server" Text="GET Detail" CssClass="btnLeft" OnClick="btnExportToExcel_Click"
                                OnClientClick="return btnGet_OnClientClick()" TabIndex="3" />
                        </div>
                    </td>
                </tr>
               
            </table>
            </div>

    <div>

   <%-- <asp:Button ID="btnExportToExcel" runat="server" Text="Download Excel" OnClick="btnExportToExcel_Click" CssClass="btndownloadexcel" />--%>
    <asp:Label ID="lblmessage" runat="server" Text="" CssClass="lblmessage"></asp:Label>

        </div>
</asp:Content>

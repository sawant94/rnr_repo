﻿using BL_Rewards.BL_Common;
using Microsoft.Reporting.WebForms;
using MM.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MM_Rewards.Web_Pages.Reports
{
    public partial class RptUserBudgetQuarterly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BL_Rewards.BL_Common.Common objCommon = new Common();

                if (Session["TokenNumber"] == null)
                {
                    Response.Redirect("/Web_Pages/HomePage.aspx");
                }
                else
                {

                }

            }
        }
        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            try
            {
                BudgetQuarterly.Visible = true;
                BudgetQuarterly.ProcessingMode = ProcessingMode.Local;
                BudgetQuarterly.LocalReport.ReportPath = Server.MapPath("~/Web_Pages/Reports/RptUserBudgetQuarterly.rdlc");
                DataSet ds = new DataSet();

                string start = Convert.ToString(dtpStartdate.SelectedDate);
                string end = Convert.ToString(dtpEnddate.SelectedDate);
                ds = GetUserBudgetQuarterlyReports(start, end);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ReportDataSource rds = new ReportDataSource("dsUserBudgetQuarterly", ds.Tables[0]);
                    BudgetQuarterly.LocalReport.DataSources.Clear();
                    BudgetQuarterly.LocalReport.DataSources.Add(rds);
                    rpthold.Style.Add("display", "block");

                }

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public DataSet GetUserBudgetQuarterlyReports(string start, string end)
        {
            SqlParameter[] objsqlparam = new SqlParameter[2];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            objsqlparam[0] = new SqlParameter("@StartDate", start);
            objsqlparam[1] = new SqlParameter("@EndDate", end);
            _ds = objDAL.DAL_GetData("Rpt_GetUserBudgetQuarterly", objsqlparam);
            return _ds;

        }

    }
}
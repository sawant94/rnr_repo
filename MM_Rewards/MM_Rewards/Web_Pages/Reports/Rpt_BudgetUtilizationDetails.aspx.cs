﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Common;
using BL_Rewards.BL_Reports;
using System.Text;
using Telerik.Web.UI;
using System.Data;
using System.Data.SqlClient;
using BE_Rewards.BE_Admin;
using Microsoft.Reporting.WebForms;

namespace MM_Rewards.Web_Pages.Reports
{
    public partial class Rpt_BudgetUtilizationDetails : System.Web.UI.Page
    {
        List<BE_MailDetails> lstExcel = new List<BE_MailDetails>();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        //BL_Rewards.BL_Reports.Reports objre = new BL_Rewards.BL_Reports.Reports();
        //List<> liMylist = objre.GetBudgetutilisationDetail("","","","","","","");

        Common objCommon = new Common();
        bool Isrnr;
        StringBuilder sbDiv = new StringBuilder();//, sbDiv,
        StringBuilder sb, sbLoc, sbDept, sbBusibess;

        #region function
        //Get item for Business unit drop down for current user
        private void LoadBusinessDropDown()
        {
            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();

            rlbBusiness.DataSource = objCommon.LoadReportBusinessDropDown();
            rlbBusiness.DataTextField = "BusinessUnit";
            rlbBusiness.DataBind();
            rlbBusiness.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
        }

        //Get item for Division drop down for current user
        private void LoadDivisionDropDown()
        {
            sbBusibess = new StringBuilder();

            if (rlbBusiness.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();


                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }

                rlbdivision.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, sbBusibess.ToString().Trim(','), Session["TokenNumber"].ToString());
                rlbdivision.DataValueField = "DivisionCode_PA";
                rlbdivision.DataTextField = "DivisionName_PA";
                rlbdivision.DataBind();
                rlbdivision.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }

            else
            {
                rlbdivision.Items.Clear();
                rlbLocation.Items.Clear();

            }
            #region backup
            //ddl_division.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, Session["TokenNumber"].ToString());
            //ddl_division.DataValueField = "DivisionCode_PA";
            //ddl_division.DataTextField = "DivisionName_PA";
            //hndIsrnr.Value = Isrnr.ToString();
            //ddl_division.DataBind();
            //ddl_division.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
            //// if (!Isrnr) // if current user is not an rnr 
            //{
            //    ddl_division.Items.Insert(1, new ListItem("ALL", "ALL"));
            //}
            //ddl_division.SelectedIndex = 0;
            #endregion backup
        }

        /*Inserted by kiran*/
        private void LoadLocationDropDown()
        {
            sb = new StringBuilder();
            if (rlbdivision.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sb.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                rlbLocation.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), sb.ToString().Trim(','));
                rlbLocation.DataValueField = "LocationCode_PSA";
                rlbLocation.DataTextField = "LocationName_PSA";
                rlbLocation.DataBind();
                rlbLocation.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }
            else
            {
                /*Inserted by Kiran*/
                rlbLocation.Items.Clear();
                //   rlbDepartment.Items.Clear();
            }
        }

        private void LoadDepartmentDropDown()
        {
            if (rlbdivision.CheckedItems.Count > 0 && rlbLocation.CheckedItems.Count > 0 && rlbBusiness.CheckedItems.Count > 0)
            {
                //sbDiv = new StringBuilder();
                sbLoc = new StringBuilder();
                sbBusibess = new StringBuilder();
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();

                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbBusibess.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }


                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sbDiv.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                if (rlbLocation.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbLoc.Append(rlbLocation.CheckedItems[0].Value);
                }
                else
                {
                    for (int j = 0; j < rlbLocation.CheckedItems.Count; j++)
                    {
                        sbLoc.Append(rlbLocation.CheckedItems[j].Value + ",");
                    }
                }
                //rlbDepartment.DataSource = objCommon.LoadDepartmentDropDown(sbLoc.ToString().Trim(','), sbDiv.ToString().Trim(','), Session["TokenNumber"].ToString());
                //rlbDepartment.DataValueField = "DivisionCode_PA";
                //rlbDepartment.DataTextField = "DivisionName_PA";
                //rlbDepartment.DataBind();
                //rlbDepartment.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));

                ViewState["sbBusibess"] = sbBusibess.ToString().Trim(',');
                ViewState["sbDiv"] = sbDiv.ToString().Trim(',');
                ViewState["sbLoc"] = sbLoc.ToString().Trim(',');
            }
            //else
            //{
            //    rlbDepartment.Items.Clear();
            //}
        }
        /*End*/

        #region backup

        // Get location drop down item as per selected value for Division for current user

        //private void LoadLocationDropDown()
        //{
        //    if (ddl_division.SelectedIndex > 0)
        //    {
        //        BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
        //        BL_Rewards.BL_Common.Common objCommon = new Common();
        //        ddl_Location.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), ddl_division.SelectedValue.ToString());
        //        ddl_Location.DataValueField = "LocationCode_PSA";
        //        ddl_Location.DataTextField = "LocationName_PSA";
        //        ddl_Location.DataBind();
        //        ddl_Location.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
        //       // if (!(Convert.ToBoolean(hndIsrnr.Value)))// if current user is not an rnr 
        //        {
        //            ddl_Location.Items.Insert(1, new ListItem("ALL", "ALL"));
        //        }
        //        ddl_Location.SelectedIndex = 0;
        //    }
        //    else
        //    {
        //        ddl_Location.Items.Clear();
        //    }
        //}

        // Get department drop down item  as per selected value for Division and location for current user

        /*
        private void LoadDepartmentDropDown()
        {
            if
                (ddl_division.SelectedIndex != 0 && ddl_Location.SelectedIndex != 0)
            {

                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                ddl_Department.DataSource = objCommon.LoadDepartmentDropDown(ddl_Location.SelectedValue, ddl_division.SelectedValue, Session["TokenNumber"].ToString());
                ddl_Department.DataValueField = "DivisionCode_PA";
                ddl_Department.DataTextField = "DivisionName_PA";
                ddl_Department.DataBind();
                ddl_Department.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
              //  if (!(Convert.ToBoolean(hndIsrnr.Value)))// if current user is not an rnr 
                {
                    ddl_Department.Items.Insert(1, new ListItem("ALL", "ALL"));
                }
                ddl_Department.SelectedIndex = 0;
            }
            else
            {
                ddl_Department.Items.Clear();
            }
        }
         */

        #endregion backup
        //function will used for throwing an alert message.
        private void ThrowAlertMessage(string strmessage)
        {
            try
            {
                if (strmessage.Contains("'"))
                {
                    strmessage = strmessage.Replace("'", "\\'");
                }
                string script = @"alert('" + strmessage + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // get the current month and year
                int month = DateTime.Now.Month;
                int year = DateTime.Now.Year;

                if (!IsPostBack)
                {
                    // dvBudgetUtilization.Visible = false;
                    Common objCommon = new Common();
                    //  LoadDivisionDropDown();
                    LoadBusinessDropDown();
                    if (Session["TokenNumber"] == null)
                    {
                        Response.Redirect("/Web_Pages/HomePage.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }
        // get report For selecetd division ,location,Department and token number 
        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            try
            { //dvBudgetUtilization.Visible = true;r

                BL_Rewards.BL_Reports.Reports objRewards = new BL_Rewards.BL_Reports.Reports();
                List<BE_MailDetails> lstReports = new List<BE_MailDetails>();
                
                //lstReports = objRewards.GetBudgetutilisationDetail(ViewState["sbDiv"].ToString(), ViewState["sbLoc"].ToString(), dtpStartdate.SelectedDate.ToString(), dtpEnddate.SelectedDate.ToString(), txtHODTokenNumber.Text.Trim(), ViewState["sbDept"].ToString(), Session["TokenNumber"].ToString());
                ObjectDataSource obj = new ObjectDataSource("BL_Rewards.BL_Reports.Reports", "GetBudgetutilisationDetail");

                obj.SelectParameters.Add(new Parameter("Business", DbType.String, ViewState["sbBusibess"].ToString()));
                obj.SelectParameters.Add(new Parameter("Division", DbType.String, ViewState["sbDiv"].ToString()));
                obj.SelectParameters.Add(new Parameter("Location", DbType.String, ViewState["sbLoc"].ToString()));
                obj.SelectParameters.Add(new Parameter("start", DbType.String, Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("end", DbType.String, Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("HODTokenNumber", DbType.String, txtHODTokenNumber.Text.Trim()));
                //   obj.SelectParameters.Add(new Parameter("OrgUnitCode", DbType.String, ViewState["sbDept"].ToString()));
                obj.SelectParameters.Add(new Parameter("TokenNumber", DbType.String, Session["TokenNumber"].ToString()));
                //  obj.SelectParameters.Add(new Parameter("AddedBalance", DbType.String, Session["AddedBalance"].ToString()));
                //obj.OldValuesParameterFormatString = "original_{0}";
                obj.DataBind();

                ReportDataSource RptObj = new ReportDataSource("BudgetUtilization", obj);
                rpvBudgetUtilization.LocalReport.DataSources.Clear();
                rpvBudgetUtilization.LocalReport.DataSources.Add(RptObj);

                rpvBudgetUtilization.LocalReport.Refresh();
               // rpvBudgetUtilization.ReportRefresh();
                rpthold.Style.Add("display", "block");

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }

        protected void rlbBusiness_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadDivisionDropDown();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }

        protected void rlbdivision_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                /*Inserted by Kiran on 25-07-2013*/
                LoadLocationDropDown();
                /*End*/
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }

        protected void rlbLocation_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                /*Inserted by Kiran on 25-07-2013*/
                LoadDepartmentDropDown();
                /*End*/
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }

        //protected void rlbDepartment_ItemCheck(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        /*Inserted by Kiran on 25-07-2013*/
        //        sbDept = new StringBuilder();
        //        if (rlbDepartment.CheckedItems.Count > 0)
        //        {
        //            if (rlbDepartment.CheckedItems[0].Value.ToString() == "ALL")
        //            {
        //                sbDept.Append(rlbDepartment.CheckedItems[0].Value);
        //            }
        //            else
        //            {
        //                for (int k = 0; k < rlbDepartment.CheckedItems.Count; k++)
        //                {
        //                    sbDept.Append(rlbDepartment.CheckedItems[k].Value + ",");
        //                }
        //            }
        //        }
        //        ViewState["sbDept"] = sbDept.ToString().Trim(',');
        //        /*End*/
        //    }
        //    catch (Exception ex)
        //    {
        //        ThrowAlertMessage(MM_Messages.DatabaseError);
        //        objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
        //    }
        //}

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Reports.Reports objRewards = new BL_Rewards.BL_Reports.Reports();

                GridView obj_grd = new GridView();
                TemplateField TempField = new TemplateField();
                TempField.ItemStyle.BackColor = System.Drawing.Color.BurlyWood;

                obj_grd.HeaderStyle.Font.Bold = true;

                obj_grd.GridLines = GridLines.Both;
                obj_grd.RowStyle.HorizontalAlign = HorizontalAlign.Center;

                lstExcel = objRewards.GetBudgetutilisationDetail(ViewState["sbBusibess"].ToString(), ViewState["sbDiv"].ToString(), ViewState["sbLoc"].ToString(), Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat), Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat), txtHODTokenNumber.Text.Trim(), Session["TokenNumber"].ToString()); // ViewState["sbDept"].ToString(),

                // New table.
                DataTable table = new DataTable();
                // add four columns
                table.Columns.Add("Giver Token Number");
                table.Columns.Add("Giver Name");
                table.Columns.Add("BusinessUnit");
                table.Columns.Add("Division");
                table.Columns.Add("Location");
                table.Columns.Add("Department");
                table.Columns.Add("Budget Allocated (RS)");
                table.Columns.Add("Budget Balance (RS)");
                table.Columns.Add("Balance(%)");
                table.Columns.Add("Types of Awards Given");
                table.Columns.Add("Number of Awards Given");
                table.Columns.Add("Budget Utlized(Rs)");
                table.Columns.Add("Budget Utilized Till Date( Rs)");
                table.Columns.Add("Budget Utilization in the FY(%)");

                //add row record
                foreach (var myobject in lstExcel)
                {
                    //exist check if the Name exist in table
                    Boolean exist = false;
                    foreach (DataRow row in table.Rows)
                    {
                        //if exist, add new columns value to the row
                        if (myobject.GiverTokenNUmber == row[0])
                        {
                            exist = true;
                        }

                    }
                    //not exist, add new row
                    if (!exist)
                    {
                        string Budgetpercent = Math.Round(Convert.ToDecimal(myobject.ClosingAmountPercentile * 100), 0).ToString() + " %";
                        string AwardCount = myobject.AwardCount == 0 ? "No Award" : myobject.AwardCount.ToString();
                        string PeriodicExpenditure = myobject.PeriodicExpenditure == 0 ? "No Award" : myobject.PeriodicExpenditure.ToString();
                        string Expenditure = myobject.Expenditure == 0 ? "No Award" : myobject.Expenditure.ToString();
                        string AwardAmount = myobject.AwardAmount == 0 ? "No Award" : Math.Round(Convert.ToDecimal(myobject.AwardAmount * 100), 2).ToString() + " %";

                        table.Rows.Add(myobject.GiverTokenNUmber, myobject.GiverName, myobject.BusinessUnit, myobject.Divisioncode, myobject.LocationName, myobject.Department, myobject.Budget, myobject.ClosingAmount, Budgetpercent, myobject.AwardType, AwardCount, PeriodicExpenditure, Expenditure, AwardAmount);
                    }
                }

                obj_grd.DataSource = table;
                obj_grd.DataBind();

                FunExportToExcel(obj_grd);

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }

        private void FunExportToExcel(GridView GrdView)
        {
            try
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=BudgetUtilization.xls");
                Response.Charset = "";
                // If you want the option to open the Excel file without saving than
                // comment out the line below
                // Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                GrdView.RenderControl(htmlWrite);
                Response.Write(stringWrite.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        public override void VerifyRenderingInServerForm(Control control) { }

        public void ConfigureExport(RadGrid radGrid, string strFileName)
        {
            radGrid.ExportSettings.IgnorePaging = true;
            radGrid.ExportSettings.FileName = strFileName;
        }

        #region backup
        // on select index change of division drop down  populate location and Department drop down
        //uncommented below event by Rakesh
        /*protected void ddl_division_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadLocationDropDown();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }
        */
        //on select index change of location drop down  populate Department drop down
        
            //uncommented below event by Rakesh
        /*protected void ddl_Location_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadDepartmentDropDown();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationDetails.aspx");
            }
        }
        */
        #endregion backup

        #endregion
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Hifi_Reports.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Hifi_Reports" %>

<%@ Register Src="../../UserControls/HifiReport.ascx" TagName="HifiReport" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceholderAddtionalPageHead" runat="server">
 <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:HifiReport ID="HifiReport1" runat="server">
    </uc1:HifiReport>
</asp:Content>

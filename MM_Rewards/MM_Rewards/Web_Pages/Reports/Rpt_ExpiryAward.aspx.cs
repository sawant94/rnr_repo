﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Common;

namespace MM_Rewards.Web_Pages.Reports
{
    public partial class Rpt_ExpiryAward : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // Common objCommon = new Common();
            if (!IsPostBack)
            {
                BL_Rewards.BL_Common.Common objCommon = new Common();
                LoadDivisionDropDown();
                if (Session["TokenNumber"] == null)
                {
                    Response.Redirect("/Web_Pages/HomePage.aspx");
                }
                else
                {
                    if (objCommon.GetRoles(Session["TokenNumber"].ToString()) == 1)
                    {
                        ddl_division.SelectedValue = Session["DivisionCode"].ToString();
                        ddl_division.Enabled = false;
                    }
                }
                LoadLocationDropDown();

                //int IsRR = objCommon.IsRRExist(Session["TokenNumber"].ToString());
                //if(IsRR == 1)
                //{
                //    LoadLocationDropDown();
                //    LoadDivisionDropDown();
                //    ddl_Location.SelectedItem.Text = Session["Location"].ToString();
                //    ddl_Location.Enabled = false;
                //    ddl_division.SelectedItem.Text = Session["Division"].ToString();
                //    ddl_division.Enabled = false;
                //}
                //else
                //{
                         
                //}
            }
        }

        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            rvpExpiryAward.LocalReport.Refresh();
            rpthold.Style.Add("display", "block");
        }
        private void LoadLocationDropDown()
        {
            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();
            ddl_Location.DataSource = objCommon.LoadLocationDropDown(ddl_division.SelectedValue.ToString());
            ddl_Location.DataValueField = "LocationCode_PSA";
            ddl_Location.DataTextField = "LocationName_PSA";
            ddl_Location.DataBind();
            ddl_Location.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddl_Location.SelectedIndex = 0;
        }


        private void LoadDivisionDropDown()
        {
            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();
            ddl_division.DataSource = objCommon.LoadDivisionDropDown();
            ddl_division.DataValueField = "DivisionCode_PA";
            ddl_division.DataTextField = "DivisionName_PA";
            ddl_division.DataBind();
            ddl_division.Items.Insert(0, new ListItem("-- Select --", "-- Select --"));
            ddl_division.Items.Insert(1, new ListItem("ALL", "ALL"));
            ddl_division.SelectedIndex = 0;
        }

        protected void ddl_division_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadLocationDropDown();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
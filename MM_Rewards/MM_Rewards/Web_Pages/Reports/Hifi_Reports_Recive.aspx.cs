﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MM_Rewards.Web_Pages.Reports
{
    public partial class Hifi_Reports_recd : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["rid"] != null)
                    {
                        HifiReport1.Rid = Request.QueryString["rid"];
                        // .Rid = Request.QueryString["rid"];
                    }
                }
            }
            catch (Exception ex)
            {
                //  ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Bu.aspx");
            }
        }
    }
}
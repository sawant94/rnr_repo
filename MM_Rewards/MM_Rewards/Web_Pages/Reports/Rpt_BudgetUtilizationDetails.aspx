﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Rpt_BudgetUtilizationDetails.aspx.cs" Inherits="MM_Rewards.Web_Pages.Reports.Rpt_BudgetUtilizationDetails" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

      <script src="../../Scripts/jquery-1.4.1.min.js"></script>
    <script  type="text/javascript">
        $(document).ready(function () {
            $("[id$=dateInput_text]").attr('readonly', 'readonly');
            //$("[id$=dtpEnddate_dateInput_text]").attr('readonly', 'readonly');
        });
    </script>

    <script type="text/javascript">
        function ValidatePage() {
            var errMsg = '';
            var vddl;


            vddl = document.getElementById('<%=rlbBusiness.ClientID %>');

            if (vddl != null) {
                var chkList = vddl.getElementsByTagName("input");
                var CountStatement = 0;

                for (i = 0; i < chkList.length; i++) {
                    if (chkList[i].checked) {
                        CountStatement = CountStatement + 1;
                    }
                }

                if (CountStatement == 0) {
                    errMsg += "Please Select Business Unit.\n";
                }
            }


            vddl = document.getElementById('<%=rlbdivision.ClientID %>');

            if (vddl != null) {
                var chkList = vddl.getElementsByTagName("input");
                var CountStatement = 0;

                for (i = 0; i < chkList.length; i++) {
                    if (chkList[i].checked) {
                        CountStatement = CountStatement + 1;
                    }
                }

                if (CountStatement == 0) {
                    errMsg += "Please Select Division.\n";
                }
            }

            vddl = document.getElementById('<%=rlbLocation.ClientID %>');

            if (vddl != null) {
                var chkList = vddl.getElementsByTagName("input");
                var CountStatement = 0;

                for (i = 0; i < chkList.length; i++) {
                    if (chkList[i].checked) {
                        CountStatement = CountStatement + 1;
                    }
                }

                if (CountStatement == 0) {
                    errMsg += "Please Select Location.\n";
                }
            }


            dpStartDate = $find('<%=dtpStartdate.ClientID %>');
            dpEndDate = $find('<%=dtpEnddate.ClientID %>');
            errMsg += Check_StartEndDate(dpStartDate.get_selectedDate() == null ? '' : dpStartDate.get_selectedDate(), dpEndDate.get_selectedDate() == null ? '' : dpEndDate.get_selectedDate(), "Start", "End");
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }
        }

        //function onItemChecked(sender, e) {
        //    debugger;
        //    var item = e.get_item();
        //    var items = sender.get_items();
        //    var checked = item.get_checked();
        //    var firstItem = sender.getItem(0);
        //    if (item.get_text() == "ALL") {
        //        items.forEach(function (itm) { itm.set_checked(checked); });
        //    }
        //    else {
        //        if (sender.get_checkedItems().length == items.get_count() - 1) {
        //            firstItem.set_checked(!firstItem.get_checked());
        //        }
        //    }
        //}
        function onItemChecked(sender, e) {
          
            var item = e.get_item();
            var items = sender.get_items();
            var checked = item.get_checked();
            var firstItem = sender.getItem(0);
            if (item.get_text() == "ALL") {
                items.forEach(function (itm) { itm.set_checked(checked); });
            }
            else {
                if (sender.get_checkedItems().length == items.get_count() - 1) {
                    firstItem.set_checked(!firstItem.get_checked());
                }
            }
        }
    </script>
    <div class="form01">
        <table class="budgetReport">
            <tr>
                <td colspan="3">
                    <h1>
                        Budget Utilization Details</h1>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    <asp:Label ID="lblDepartment" runat="server" Text="Business Unit"></asp:Label>
                    &nbsp;<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadListBox ID="rlbBusiness" runat="server" CheckBoxes="true" Width="300px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnClientItemChecked="onItemChecked"
                        OnItemCheck="rlbBusiness_ItemCheck">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <%--<asp:DropDownList ID="ddl_division" runat="server" TabIndex="1" AutoPostBack="True"
                        OnSelectedIndexChanged="ddl_division_SelectedIndexChanged">
                    </asp:DropDownList>--%>
                    <telerik:RadListBox ID="rlbdivision" runat="server" CheckBoxes="true" Width="300px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnItemCheck="rlbdivision_ItemCheck"
                        OnClientItemChecked="onItemChecked">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                    <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <%--<asp:DropDownList ID="ddl_Location" runat="server" TabIndex="2" AutoPostBack="True">
                    </asp:DropDownList>--%><%-- OnSelectedIndexChanged="ddl_Location_SelectedIndexChanged"--%>
                    <telerik:RadListBox ID="rlbLocation" runat="server" CheckBoxes="true" Width="300px"
                        AutoPostBack="true" Height="105px" ViewStateMode="Enabled" OnClientItemChecked="onItemChecked"
                        OnItemCheck="rlbLocation_ItemCheck">
                    </telerik:RadListBox>
                </td>
            </tr>
            <%--  <tr>
                <td class="lblName">
                    <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                    &nbsp;<span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>--%>
            <%--<asp:DropDownList ID="ddl_Department" runat="server" TabIndex="3">
                    </asp:DropDownList>--%>
            <%--       <telerik:RadListBox ID="rlbDepartment" runat="server" CheckBoxes="true" Width="200px" AutoPostBack="true"  
                        Height="105px" ViewStateMode="Enabled" OnItemCheck="rlbDepartment_ItemCheck" OnClientItemChecked="onItemChecked" >
                    </telerik:RadListBox>
                </td>
            </tr>--%>
            <tr>
                <td class="lblName">
                    Start Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpStartdate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="3">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    End Date <span style="color: Red">*</span>
                </td>
                <td>
                    :
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtpEnddate" runat="server" DateInput-DateFormat="dd/MM/yyyy"
                        TabIndex="4">
                    </telerik:RadDatePicker>
                      <asp:CompareValidator ID="dateCompareValidator" runat="server" 
                        ControlToValidate="dtpEnddate" ControlToCompare="dtpStartdate" 
                        Operator="GreaterThan" Type="date" 
                        ErrorMessage="The end date must be greater than the start date."
                        CssClass="errorMessage">
                    </asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="lblName">
                    HOD Token Number&nbsp;
                </td>
                <td>
                    :
                </td>
                <td>
                    <asp:TextBox ID="txtHODTokenNumber" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hndIsrnr" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <div class="btnRed">
                        <asp:Button ID="btnGetDetails" CssClass="btnLeft" runat="server" Text="Generate Report"
                            TabIndex="3" OnClick="btnGetDetails_Click" OnClientClick="return ValidatePage()" />
                    </div>
                    <div class="btnRed">
                        <asp:Button ID="btnExportToExcel" CssClass="btnLeft" runat="server" Text="Export To Excel"
                            TabIndex="3" OnClick="btnExportToExcel_Click" OnClientClick="return ValidatePage()" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="reportHold" runat="server" id="rpthold">
        <rsweb:ReportViewer ID="rpvBudgetUtilization" runat="server" InteractiveDeviceInfos="(Collection)"
            CssClass="rpTab">
 <%--           \Web_Pages\Reports\BudgetUtilizationDetails.rdlc--%>
            <LocalReport ReportPath="Web_Pages\Reports\BudgetUtilizationDetails.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource Name="BudgetUtilization" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <%--<asp:ObjectDataSource ID="ObjectDataSource" runat="server" SelectMethod="GetBudgetutilisationDetail"
            TypeName="BL_Rewards.BL_Reports.Reports" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:ControlParameter ControlID="rlbdivision" Name="Division" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="rlbLocation" Name="Location" PropertyName="SelectedValue"
                    Type="String" DefaultValue="-- Select --" />
                <asp:ControlParameter ControlID="dtpStartdate" Name="start" PropertyName="SelectedDate"
                    Type="String" />
                <asp:ControlParameter ControlID="dtpEnddate" Name="end" PropertyName="SelectedDate"
                    Type="String" />
                <asp:ControlParameter ControlID="txtHODTokenNumber" DefaultValue=" " Name="HODTokenNumber"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="rlbDepartment" Name="OrgUnitCode" PropertyName="SelectedValue"
                    Type="String" DefaultValue="-- Select --" />
                <asp:SessionParameter Name="TokenNumber" SessionField="TokenNumber" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>--%>
    </div>
</asp:Content>

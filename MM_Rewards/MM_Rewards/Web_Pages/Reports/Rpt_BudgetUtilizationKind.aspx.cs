﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE_Rewards.BE_Rewards;
using BL_Rewards.BL_Common;
using BL_Rewards.BL_Reports;
using System.Text;
using Telerik.Web.UI;
using System.Data;
using System.Data.SqlClient;
using BE_Rewards.BE_Admin;
using Microsoft.Reporting.WebForms;


namespace MM_Rewards.Web_Pages.Reports
{
    public partial class Rpt_BudgetUtilizationKind : System.Web.UI.Page
    {
        List<BE_MailDetails> lstExcel = new List<BE_MailDetails>();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        Common objCommon = new Common();
        bool Isrnr;
        StringBuilder sbDiv = new StringBuilder();
        StringBuilder sb, sbLoc, sbDept, sbBusibess;

        #region function
        //Get item for Business unit drop down for current user
        private void LoadBusinessDropDown()
        {
            BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
            BL_Rewards.BL_Common.Common objCommon = new Common();

            rlbBusiness.DataSource = objCommon.LoadReportBusinessDropDown();
            rlbBusiness.DataTextField = "BusinessUnit";
            rlbBusiness.DataBind();
            rlbBusiness.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
        }

        //Get item for Division drop down for current user
        private void LoadDivisionDropDown()
        {
            sbBusibess = new StringBuilder();

            if (rlbBusiness.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();


                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }

                rlbdivision.DataSource = objCommon.LoadReportDivisionDropDown(out Isrnr, sbBusibess.ToString().Trim(','), Session["TokenNumber"].ToString());
                rlbdivision.DataValueField = "DivisionCode_PA";
                rlbdivision.DataTextField = "DivisionName_PA";
                rlbdivision.DataBind();
                rlbdivision.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }

            else
            {
                rlbdivision.Items.Clear();
                rlbLocation.Items.Clear();

            }

        }

        private void LoadLocationDropDown()
        {
            sb = new StringBuilder();
            if (rlbdivision.CheckedItems.Count > 0)
            {
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();
                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sb.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                rlbLocation.DataSource = objCommon.LoadLocationDropDown(Session["TokenNumber"].ToString(), sb.ToString().Trim(','));
                rlbLocation.DataValueField = "LocationCode_PSA";
                rlbLocation.DataTextField = "LocationName_PSA";
                rlbLocation.DataBind();
                rlbLocation.Items.Insert(0, new Telerik.Web.UI.RadListBoxItem("ALL", "ALL"));
            }
            else
            {
                rlbLocation.Items.Clear();
            }
        }

        private void LoadDepartmentDropDown()
        {
            if (rlbdivision.CheckedItems.Count > 0 && rlbLocation.CheckedItems.Count > 0 && rlbBusiness.CheckedItems.Count > 0)
            {
                sbLoc = new StringBuilder();
                sbBusibess = new StringBuilder();
                BE_RecipientAward objBERecipientAwrad = new BE_RecipientAward();
                BL_Rewards.BL_Common.Common objCommon = new Common();

                if (rlbBusiness.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbBusibess.Append(rlbBusiness.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbBusiness.CheckedItems.Count; i++)
                    {
                        sbBusibess.Append(rlbBusiness.CheckedItems[i].Value + ",");
                    }
                }


                if (rlbdivision.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbDiv.Append(rlbdivision.CheckedItems[0].Value);
                }
                else
                {
                    for (int i = 0; i < rlbdivision.CheckedItems.Count; i++)
                    {
                        sbDiv.Append(rlbdivision.CheckedItems[i].Value + ",");
                    }
                }
                if (rlbLocation.CheckedItems[0].Value.ToString() == "ALL")
                {
                    sbLoc.Append(rlbLocation.CheckedItems[0].Value);
                }
                else
                {
                    for (int j = 0; j < rlbLocation.CheckedItems.Count; j++)
                    {
                        sbLoc.Append(rlbLocation.CheckedItems[j].Value + ",");
                    }
                }
                ViewState["sbBusibess"] = sbBusibess.ToString().Trim(',');
                ViewState["sbDiv"] = sbDiv.ToString().Trim(',');
                ViewState["sbLoc"] = sbLoc.ToString().Trim(',');
            }

        }
        /*End*/


        //function will used for throwing an alert message.
        private void ThrowAlertMessage(string strmessage)
        {
            try
            {
                if (strmessage.Contains("'"))
                {
                    strmessage = strmessage.Replace("'", "\\'");
                }
                string script = @"alert('" + strmessage + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int month = DateTime.Now.Month;
                int year = DateTime.Now.Year;

                if (!IsPostBack)
                {
                    Common objCommon = new Common();
                    LoadBusinessDropDown();
                    if (Session["TokenNumber"] == null)
                    {
                        Response.Redirect("/Web_Pages/HomePage.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationKind.aspx");
            }
        }

        // get report For selecetd division ,location,Department and token number 
        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Reports.Reports objRewards = new BL_Rewards.BL_Reports.Reports();
                List<BE_MailDetails> lstReports = new List<BE_MailDetails>();
                ObjectDataSource obj = new ObjectDataSource("BL_Rewards.BL_Reports.Reports", "GetBudgetutilisationDetail");
                obj.SelectParameters.Add(new Parameter("Business", DbType.String, ViewState["sbBusibess"].ToString()));
                obj.SelectParameters.Add(new Parameter("Division", DbType.String, ViewState["sbDiv"].ToString()));
                obj.SelectParameters.Add(new Parameter("Location", DbType.String, ViewState["sbLoc"].ToString()));
                obj.SelectParameters.Add(new Parameter("start", DbType.String, Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("end", DbType.String, Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)));
                obj.SelectParameters.Add(new Parameter("HODTokenNumber", DbType.String, txtHODTokenNumber.Text.Trim()));
                obj.SelectParameters.Add(new Parameter("TokenNumber", DbType.String, Session["TokenNumber"].ToString()));
                obj.DataBind();
                ReportDataSource RptObj = new ReportDataSource("BudgetUtilization", obj);
                rpvBudgetUtilization.LocalReport.DataSources.Clear();
                rpvBudgetUtilization.LocalReport.DataSources.Add(RptObj);
                rpvBudgetUtilization.LocalReport.Refresh();
                rpthold.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationKind.aspx");
            }
        }

        protected void rlbBusiness_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadDivisionDropDown();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationKind.aspx");
            }
        }

        protected void rlbdivision_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadLocationDropDown();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationKind.aspx");
            }
        }

        protected void rlbLocation_ItemCheck(object sender, EventArgs e)
        {
            try
            {
                LoadDepartmentDropDown();
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationKind.aspx");
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                BL_Rewards.BL_Reports.Reports objRewards = new BL_Rewards.BL_Reports.Reports();
                GridView obj_grd = new GridView();
                TemplateField TempField = new TemplateField();
                TempField.ItemStyle.BackColor = System.Drawing.Color.BurlyWood;
                obj_grd.HeaderStyle.Font.Bold = true;
                obj_grd.GridLines = GridLines.Both;
                obj_grd.RowStyle.HorizontalAlign = HorizontalAlign.Center;
                lstExcel = objRewards.GetBudgetutilisationDetail(ViewState["sbBusibess"].ToString(), ViewState["sbDiv"].ToString(), ViewState["sbLoc"].ToString(), Convert.ToDateTime(dtpStartdate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat), Convert.ToDateTime(dtpEnddate.SelectedDate).ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat), txtHODTokenNumber.Text.Trim(), Session["TokenNumber"].ToString()); // ViewState["sbDept"].ToString(),
                DataTable table = new DataTable();
                table.Columns.Add("Giver Token Number");
                table.Columns.Add("Giver Name");
                table.Columns.Add("BusinessUnit");
                table.Columns.Add("Division");
                table.Columns.Add("Location");
                table.Columns.Add("Department");
                table.Columns.Add("Budget Allocated (RS)");
                table.Columns.Add("Budget Balance (RS)");
                table.Columns.Add("Balance(%)");
                table.Columns.Add("Types of Awards Given");
                table.Columns.Add("Number of Awards Given");
                table.Columns.Add("Budget Utlized(Rs)");
                table.Columns.Add("Budget Utilized Till Date( Rs)");
                table.Columns.Add("Budget Utilization in the FY(%)");
                foreach (var myobject in lstExcel)
                {
                    Boolean exist = false;
                    foreach (DataRow row in table.Rows)
                    {
                        if (myobject.GiverTokenNUmber == row[0])
                        {
                            exist = true;
                        }
                    }
                    if (!exist)
                    {
                        string Budgetpercent = Math.Round(Convert.ToDecimal(myobject.ClosingAmountPercentile * 100), 0).ToString() + " %";
                        string AwardCount = myobject.AwardCount == 0 ? "No Award" : myobject.AwardCount.ToString();
                        string PeriodicExpenditure = myobject.PeriodicExpenditure == 0 ? "No Award" : myobject.PeriodicExpenditure.ToString();
                        string Expenditure = myobject.Expenditure == 0 ? "No Award" : myobject.Expenditure.ToString();
                        string AwardAmount = myobject.AwardAmount == 0 ? "No Award" : Math.Round(Convert.ToDecimal(myobject.AwardAmount * 100), 2).ToString() + " %";

                        table.Rows.Add(myobject.GiverTokenNUmber, myobject.GiverName, myobject.BusinessUnit, myobject.Divisioncode, myobject.LocationName, myobject.Department, myobject.Budget, myobject.ClosingAmount, Budgetpercent, myobject.AwardType, AwardCount, PeriodicExpenditure, Expenditure, AwardAmount);
                    }
                }

                obj_grd.DataSource = table;
                obj_grd.DataBind();
                FunExportToExcel(obj_grd);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Rpt_BudgetUtilizationKind.aspx");
            }
        }

        private void FunExportToExcel(GridView GrdView)
        {
            try
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=BudgetUtilizationKind.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.xls";
                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                GrdView.RenderControl(htmlWrite);
                Response.Write(stringWrite.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        public override void VerifyRenderingInServerForm(Control control) { }

        public void ConfigureExport(RadGrid radGrid, string strFileName)
        {
            radGrid.ExportSettings.IgnorePaging = true;
            radGrid.ExportSettings.FileName = strFileName;
        }

        #endregion
    }
}
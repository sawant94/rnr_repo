﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Rpt_AwardDetails.aspx.cs"
    Inherits="MM_Rewards.Rpt_AwardDetails"  MasterPageFile="~/Web_Pages/MM_Rewards.Master"  %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">       
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

<script type="text/javascript">

     function ValidatePage() {

         var errMsg = '';
         var vddl;
 
         vddl = document.getElementById('<%=ddlDivision.ClientID %>');
         errMsg += CheckDropdown_Blank(vddl.value, " Division ");
         vddl = document.getElementById('<%=ddlredeemType.ClientID %>');
         errMsg += CheckDropdown_Blank(vddl.value, " Redeeem Type ");



         if (errMsg != '') {
             alert(errMsg);
             return false;
         }
         else {
             return true;
         }
     }
    </script> 
<div class="form01">
                <table border="0"  width="100%" cellspacing="0" cellpadding="0"  >
                    <tr>
                        <td colspan="3">
                            <asp:Label runat="server" ID="lblMessage" Font-Bold="True" Font-Size="Large"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Division</td>
                        <td >
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddlDivision" runat="server" Width="200px" TabIndex="1">
                                <asp:ListItem>-- Select --</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Redeeem Type
                        </td>
                        <td >
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlredeemType" runat="server" Width="137px" TabIndex="2" 
                               >
                                <asp:ListItem>-- Select --</asp:ListItem>
                                <asp:ListItem Value="C" Selected="True">Cash</asp:ListItem>
                                <asp:ListItem Value="K">Kind</asp:ListItem>
                                <asp:ListItem Value="N">Not Redeemed</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr >
                        <td colspan="3" style="padding-left:270px;">
                            <asp:Button ID="Button1" Width="150px" CssClass="btnR" runat="server" 
                                OnClick="Button1_Click"  OnClientClick="return ValidatePage()" 
                                Text="Generate Report" TabIndex="3" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:DropDownList ID="ddlLocation" runat="server" Width="250px" 
                                Visible="False" TabIndex="4" >
                                <asp:ListItem Value="0">-- Select --</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                    </tr>
                    <tr  >
                    <td colspan="3" class="rptTbl">
                      <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="MM_Rewards.AwardDetailsDataSetTableAdapters.usp_Rpt_AwardDetailsTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="N" Name="Location" Type="String" />
                        <asp:ControlParameter ControlID="ddlDivision" Name="Division" 
                            PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="ddlredeemType" Name="RedeemType" 
                            PropertyName="SelectedValue" Type="String" />
                        <asp:Parameter DefaultValue="N" Name="PeriodWise" Type="String" />
                        <asp:Parameter Name="FromDate" Type="String" />
                        <asp:Parameter Name="ToDate" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>            
                      <div id="MyReport" runat="server"  visible="true"  width="100%"  >
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
                    Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                    WaitMessageFont-Names="Verdana"  
                    WaitMessageFont-Size="10pt" Width="100%"   SizeToReportContent="True"
                    PageCountMode="Actual" style="padding-left:0px;">
                    <LocalReport ReportPath="Web_Pages\Reports\Rpt_AwardDetails.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="AwardDetailsDataSource" 
                                Name="ds_AwardDetails" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="AwardDetailsDataSource" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="MM_Rewards.AwardDetailsDataSetTableAdapters.usp_Rpt_AwardDetailsTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="N" Name="Location" Type="String" />
                        <asp:ControlParameter ControlID="ddlDivision" Name="Division" 
                            PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="ddlredeemType" Name="RedeemType" 
                            PropertyName="SelectedValue" Type="String" />
                        <asp:Parameter DefaultValue="N" Name="PeriodWise" Type="String" />
                        <asp:Parameter Name="FromDate" Type="String" />
                        <asp:Parameter Name="ToDate" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
                    </td>
                    
                    </tr>
                </table>
        </div> 
</asp:Content>       

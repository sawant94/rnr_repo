﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Idealake.Mahindra.RewardsSocial;
using Idealake.Web.MMRewardsSocial.Core;

namespace MM_Rewards
{
    public partial class BulkUploadFriends : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User thisPageUser;
            string tokennumber = Request.QueryString["tokennumber"];
            if (string.IsNullOrWhiteSpace(tokennumber))
            {
                thisPageUser = WebContext.Current.CurrentUser;
            }
            else
            {
                thisPageUser = WebContext.Current.GetUserByTokenNumber(tokennumber);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MM.DAL;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MM_Rewards.Web_Pages
{
    public partial class CustomLogin : System.Web.UI.Page
    {

        ////////production code
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    int num = ((IEnumerable<string>)this.Request.QueryString.AllKeys).Count<string>();
        //    string authority = HttpContext.Current.Request.Url.Authority;
        //    if (num <= 0)
        //    {
        //        if (authority == "www.afsrecognitionportal.com:8081")
        //            this.Response.Redirect("https://www.mahindrarise.com/get_auth_external_internet.php");
        //        else
        //            this.Response.Redirect("https://www.mahindrarise.com/get_auth_external_intranet.php");
        //    }
        //    else
        //    {
        //        string[] strArray = Encoding.UTF8.GetString(Convert.FromBase64String(this.Request.QueryString["login"].ToString())).Split('|');
        //        HttpContext.Current.Session["userName"] = (object)strArray[1];
        //        HttpContext.Current.Session["mailid"] = (object)strArray[2];
        //        HttpContext.Current.Session["token"] = (object)strArray[0];
        //        this.Session["windowsUserName"] = (object)strArray[0];
        //        HttpContext.Current.Session["type"] = (object)"Emp";

        //        HttpContext.Current.Response.Redirect("HomePage.aspx");


        //    }
        //}


        // testing code
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //    //int num = ((IEnumerable<string>)this.Request.QueryString.AllKeys).Count<string>();
        //    ////if (CustomLogin.blSessionisActive)
        //    ////    return;
        //    //string authority = HttpContext.Current.Request.Url.Authority;
        //    //if (num <= 0)
        //    //{
        //    //    if (authority == "www.afsrecognitionportal.com:8081")
        //    //        this.Response.Redirect("https://www.mahindrarise.com/get_auth_external_internet.php");
        //    //    else
        //    //        this.Response.Redirect("https://www.mahindrarise.com/get_auth_external_intranet.php");
        //    //}
        //    //else
        //    //{
        //    //    string[] strArray = Encoding.UTF8.GetString(Convert.FromBase64String(this.Request.QueryString["login"].ToString())).Split('|');
        //    //    string[] strArray = new string[3];
        //    //    strArray[0] = "214717";
        //    //    strArray[1] = "Anant";
        //    //    strArray[2] = "ghode.anant@mahindra.com";
        //    //    HttpContext.Current.Session["userName"] = (object)strArray[1];
        //    //    HttpContext.Current.Session["mailid"] = (object)strArray[2];
        //    //    HttpContext.Current.Session["token"] = (object)strArray[0];

        //    //    this.Session["windowsUserName"] = (object)strArray[0];
        //    //    HttpContext.Current.Session["type"] = (object)"Emp";

        //    //    HttpContext.Current.Response.Redirect("HomePage.aspx");


        //    //}
        //}

        protected void Button1_Click(object sender, EventArgs e)
        {
            this.Session["windowsUserName"] = TextBox1.Text; //"12724"; //"23107134";
            HttpContext.Current.Session["type"] = (object)"Emp";

            HttpContext.Current.Session["userName"] = TextBox1.Text;
            ///HttpContext.Current.Session["mailid"] = (object)strArray[2];
            HttpContext.Current.Session["TokenNumber"] = TextBox1.Text;
            HttpContext.Current.Response.Redirect("HomePage.aspx");
            string token = Session["TokenNumber"].ToString();

            if (TextBox1.Text != "")
            {
                if (ValidateUser())
                {
                }
                else
                {
                    Response.Write("<script>alert('Invalid UserName or Password');</script>");

                }
            }
            else
            {
                Response.Write("<script>alert('Add token No');</script>");
            }

        }

        private bool ValidateUser()
        {
            SqlParameter[] objsqlparam = new SqlParameter[2];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            objsqlparam[0] = new SqlParameter("@username", TextBox1.Text);
            objsqlparam[1] = new SqlParameter("@password", TextBox2.Text);
            _ds = objDAL.DAL_GetData("usp_Login", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="ContactUs.aspx.cs" Inherits="MM_Rewards.Web_Pages.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 105px;
        }
        div.pgData {
            padding: 0 20px;
        }
    </style>

    <div class="grid01">
        <table border="1" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="4">
                    <b><span>In case of any R&R issues, please write an email to hrsupport@mahindrampeople.com , marking your HRBP in CC.
<%--If the issue is still not resolved, please write to your respective R&R co-ordinator mentioned below.--%> </span></b>
                </td>
            </tr>
        <%--    <tr>
                <td valign="top" width="162">
                    <b><span>Division </span></b>
                </td>
                <td valign="top" width="162">
                    <b><span>R&amp;R co-ordinator<o:p></o:p></span></b>
                </td>
                <td valign="top" width="156">
                    <b><span>Contact No<o:p></o:p></span></b>
                </td>
                <td valign="top" width="258">
                    <b><span>Email Id<o:p></o:p></span></b>
                </td>
            </tr>
            <tr>
                <td valign="top" width="162">
                    <span>Auto Division<o:p></o:p></span>
                </td>
                <td valign="top" width="162">
                    <span>Niraja Mulye<o:p></o:p></span>
                </td>
                <td valign="top" width="156">
                    <span>022- 28849138<o:p></o:p></span>
                </td>
                <td valign="top" width="258">
                    <span>mulye.niraja@mahindra.com<o:p></o:p></span>
                </td>
            </tr>
            <tr>
                <td valign="top" width="162">
                    <span>Common Functions<o:p></o:p></span>
                </td>
                <td valign="top" width="162">
                    <span>Christine D’souza<o:p></o:p></span>
                </td>
                <td valign="top" width="156">
                    <span>022-28849737<o:p></o:p></span>
                </td>
                <td valign="top" width="258">
                    <span>d’souza.christine@mahindra.com<o:p></o:p></span>
                </td>
            </tr>
            <tr>
                <td valign="top" width="162">
                    <span>Farm Division<o:p></o:p></span>
                </td>
                <td valign="top" width="162">
                    <span>Ruchi Rawat<o:p></o:p></span>
                </td>
                <td valign="top" width="156">
                    <span>022- 66483711<o:p></o:p></span>
                </td>
                <td valign="top" width="258">
                    <span>rawat.ruchi@mahindra.com<o:p></o:p></span>
                </td>
            </tr>--%>
            <%-- <tr>
            <td valign="top" width="162">
                
                    <span>International Operations<o:p></o:p></span>
            </td>
            <td valign="top" width="162">
                
                    <span>Nicolette Noronha<o:p></o:p></span>
            </td>
            <td valign="top" width="156">
                
                    <span>022 - 24916615<o:p></o:p></span>
            </td>
            <td valign="top" width="258">
                
                    <span>noronha.nicolette@mahindra.com<o:p></o:p></span>
            </td>
        </tr>--%>
           <%-- <tr>
                <td valign="top" width="162">
                    <span>MNAL<o:p></o:p></span>
                </td>
                <td valign="top" width="162">
                    <span>Piyush Agarwal<o:p></o:p></span>
                </td>
                <td valign="top" width="156">
                    <span>020-27501357<o:p></o:p></span>
                </td>
                <td valign="top" width="258">
                    <span>agarwal.piyush@mahindranavistar.com<o:p></o:p></span>
                </td>
            </tr>
            <tr>
                <td valign="top" width="162">
                    <span>MNEPL<o:p></o:p></span>
                </td>
                <td valign="top" width="162">
                    <span>Reetu Jain<o:p></o:p></span>
                </td>
                <td valign="top" width="156">
                    <span>0213-5612915<o:p></o:p></span>
                </td>
                <td valign="top" width="258">
                    <span>jain.reetu@mnel.com<o:p></o:p></span>
                </td>
            </tr>--%>
            <%--   <tr>
            <td valign="top" width="162">
                
                    <span>MVML<o:p></o:p></span>
            </td>
            <td valign="top" width="162">
                
                    <span>Manabendra Majumdar<o:p></o:p></span>
            </td>
            <td valign="top" width="156">
                
                    <span>0213- 5617109<o:p></o:p></span>
            </td>
            <td valign="top" width="258">
                
                    <span>majumdar.manabendra@mahindra.com<o:p></o:p></span>
            </td>
        </tr>--%>
          <%--  <tr>
                <td valign="top" width="162">
                    <span>Swaraj Division<o:p></o:p></span>
                </td>
                <td valign="top" width="162">
                    <span>Monika Garg </span>
                </td>
                <td valign="top" width="156">
                    <span>0172-2271620</span>
                </td>
                <td valign="top" width="258">
                    <span>garg.monika@mahindraswaraj.com</span>
                </td>
            </tr>
            <tr>
                <td valign="top" width="162">
                    <span>TPDS<o:p></o:p></span>
                </td>
                <td valign="top" width="162">
                    <span>Harikumar Nair<o:p></o:p></span>
                </td>
                <td valign="top" width="156">
                    <span>20536608388<o:p></o:p></span>
                </td>
                <td valign="top" width="258">
                    <span>nair.harikumar@mahindra.com<o:p></o:p></span>
                </td>
                </tr>
                <tr>
                    <td valign="top" width="162">
                        <span>MVML<o:p></o:p></span>
                    </td>
                    <td valign="top" width="162">
                        <span>Shrikumar Nair<o:p></o:p></span>
                    </td>
                    <td valign="top" width="156">
                        <span>02135-617121<o:p></o:p></span>
                    </td>
                    <td valign="top" width="258">
                        <span>nair.shrikumar@mahindra.com<o:p></o:p></span>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="162">
                        <span>International Operations<o:p></o:p></span>
                    </td>
                    <td valign="top" width="162">
                        <span>Sandra Miranda<o:p></o:p></span>
                    </td>
                    <td valign="top" width="156">
                        <span>24931668<o:p></o:p></span>
                    </td>
                    <td valign="top" width="258">
                        <span>miranda.sandra@mahindra.co.in<o:p></o:p></span>
                    </td>
                </tr>--%>
        </table>
    </div>
</asp:Content>

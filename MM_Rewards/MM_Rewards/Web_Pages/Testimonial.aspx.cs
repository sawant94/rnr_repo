﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BE_Rewards.BE_Admin;
using BL_Rewards.Admin;
using BL_Rewards.BL_Common;
using System.Configuration;

namespace MM_Rewards.Web_Pages
{
    public partial class Testimonial : System.Web.UI.Page
    {
        BE_Testimonial objBE_Testimonial = new BE_Testimonial();
        BE_TestimonialColl objBE_Testimonialcoll = new BE_TestimonialColl();
        BL_Testimonial objBL_Testimonial = new BL_Testimonial();
        ExceptionLogging objExceptionLogging = new ExceptionLogging();

        #region method
        //to clear screen
        private void Clear()
        {
            txttestimonial.Text = "";
        }
        // to generate javascript alert message
        private void ThrowAlertMessage(string strmessage)
        {
            try
            {
                if (strmessage.Contains("'"))
                {
                    strmessage = strmessage.Replace("'", "\\'");
                }
                string script = @"alert('" + strmessage + "');";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "AwardsByMe.aspx");
            }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnsavetestimonial_Click(object sender, EventArgs e)
        {
            try
            {  
                 int _SaveStatus = 0;
                string extension = string.Empty;
                string imagename =string.Empty;
                // retrive a file size in Kb from web config and convert it into byte.
                int maxFilesize = Convert.ToInt32(ConfigurationManager.AppSettings["FileSizeInKb"].ToString()) * 1024;
                //path to save image.
                string path = Server.MapPath(@"~/TestimoinalImage/");
                if (flupload.HasFile &&((flupload.PostedFile.ContentLength) < (maxFilesize)))
                {
                    extension = flupload.FileName.Substring(flupload.FileName.LastIndexOf('.'));
                    objBE_Testimonial.TokenNumber = Session["TokenNumber"].ToString();
                    objBE_Testimonial.TestimonoialText = txttestimonial.Text;
                    objBE_Testimonial.ImageName = extension;
                    objBE_Testimonial.CreatedBy = Session["TokenNumber"].ToString();
                    
                    //to save data into database.
                    _SaveStatus = objBL_Testimonial.SaveData(objBE_Testimonial, out imagename);
                    if (_SaveStatus > 0)
                    {
                        string filename = imagename.ToString();
                        path += filename;
                        flupload.SaveAs(path);
                        ThrowAlertMessage(MM_Messages.DataSaved);
                        Clear();
                    }
                    else
                    {
                        ThrowAlertMessage(MM_Messages.DatabaseError);
                    }
                }
                else
                {
                    ThrowAlertMessage("File Size Exceeds the max limit " + ConfigurationManager.AppSettings["FileSizeInKb"].ToString() + " Kb");
                }

            }
            catch (Exception ex)
            {
                ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "Testimonial.aspx");
            }
        
           
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        #endregion



    }
}
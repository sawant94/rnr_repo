﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Pages/MM_Rewards.Master" AutoEventWireup="true"
    CodeBehind="Testimonial.aspx.cs" Inherits="MM_Rewards.Web_Pages.Testimonial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        body {
            padding-top: 0;
        }
        div.pgData {
            padding: 0;
        }
    </style>

    <style>
        div.form01 table td input {
            margin-right: 0;
        }

        input.btnReset.testi_cancel {
            color: #333;
            background: transparent!important;
        }

        .mahindra_drop_down_div.rnr-dd label.fl_label {
            position: relative;
            top: 0;
            left: 0;
        }
    </style>
    <script src="../assets/js/all.js"></script>

    <script type="text/javascript">
        function flupload_onchange() {
            var flupload = document.getElementById('<%=flupload.ClientID%>');


            var rExp = "^.+(.jpg|.JPG|.gif|.GIF|.png|.PNG|.bmp|.BMP)$";
            if (flupload.value == "") {
                alert("Please upload a file first!");
                return false;
            }
            else {
                if (!flupload.value.match(rExp)) {
                    alert("Only Image file is allowed!");
                    flupload.value = "";
                    return false;
                }


            }

        }

        function ValidatePage() {

            var r = confirm("Do you want to submit record ?");
            if (r != true) {
                return false;
            }


            var errMsg = '';
            var flupload = document.getElementById('<%=flupload.ClientID%>');
            if (flupload.value == "") {
                errMsg = 'Please upload a Image first! \n';

            }
            var txtTestimonialtext = document.getElementById('<%=txttestimonial.ClientID %>');
            errMsg += CheckText_Blank(txtTestimonialtext.value, 'Testimonial ');
            if (errMsg != '') {
                alert(errMsg);
                return false;
            }
            else {
                return true;
            }

        }
        function ClearFields() {
            var txtTestimonialtext = document.getElementById('<%=txttestimonial.ClientID %>');
            var uploadcontrol = document.getElementById('<%=flupload.ClientID%>');
            txtTestimonialtext.value = '';
            uploadcontrol.value = '';
            return false;
        }
    </script>

    <script type="text/javascript">
        function ValidateTextLength(source, args) {


            var length = document.getElementById('<%=txttestimonial.ClientID %>').value.length;


            //this would make it valid unless the length is greater than 250
            if (length <= 250) {


                args.IsValid = true;
            }
            else {
                alert('You may only enter 250 characters. You have entered ' + length + ' characters.');
                args.IsValid = false;
            }


        }
    </script>


    <section class="main-rnr">
        <div class="rnr-inner-top">
            <div class="rnr-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href='/Web_Pages/HomePage.aspx'>HOME</a></li>

                    <li>TESTIMONIAL</li>
                </ul>

            </div>
        </div>

        <div class="rnr-tabs">
            <h3 class="rnr-h3-top">Testimonial</h3>


            <div class="rnr-tab-pane tab-content Feedback-wrap">
                <div class="tab-pane Feedback-inn">
                    <div class="container">
                        <div class="row rnr-custom-row">
                            <div class="col-md-12">
                                <div class="mahindra_drop_down_div rnr-dd award-top-dd file-attch">
                                    <label for="">Upload Profile Picture</label>
                                    <!--    <input type="file" id="real-file" hidden="hidden">
        <button type="button" id="custom-button">Upload Document Type</button> -->
                                    <div class="file-upload">
                                       <%-- <asp:FileUpload ID="flupload" runat="server" onchange="return flupload_onchange()" CssClass="file-upload__input hide" TabIndex="1" />
                                        <label class="file-upload__button fl_label" for="">Choose File</label>
                                        <span class="file-upload__label" id="file-upload-filename"></span>--%>
                                         <asp:FileUpload ID="flupload" runat="server" onchange="return flupload_onchange()" 
                                             CssClass="file-upload__input hide" BackColor="DodgerBlue" ForeColor="AliceBlue" /> 

                                        <br /><br /> <asp:Image ID="Image1" runat="server" />
                                        <label class="file-upload__button fl_label" for="">Choose File</label>
                                        <span class="file-upload__label" id="file-upload-filename"></span>
                                    </div>


                                </div>

                                <div class="nominee-name">

                                    <div class="rnr-input">
                                        <label for="">Testimonial Details</label>
                                        <asp:TextBox ID="txttestimonial" runat="server" MaxLength="255" TabIndex="3"
                                            TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        <asp:CustomValidator ID="cvTest" runat="server" ControlToValidate="txttestimonial"
                                            ClientValidationFunction="ValidateTextLength" ErrorMessage="Text is too long" />
                                        <%--<textarea name="name" rows="3" placeholder="Please enter the Testimonial" ></textarea>--%>
                                    </div>



                                    <div class="send-awards">


                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnReset testi_cancel close" OnClientClick="return ClearFields()"
                                            TabIndex="5" OnClick="btnCancel_Click" />
                                        <asp:Button ID="btnsavetestimonial" runat="server" Text="send testimonial" CssClass="add-emp rnr-btn"
                                            OnClientClick="return ValidatePage()" TabIndex="4" OnClick="btnsavetestimonial_Click" />
                                        <%--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">CANCEL</span>
              </button>
              <button type="button" name="button" class="add-emp rnr-btn">send Testimonial</button>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            //var var_id = $('.file-upload > input').attr('id');
            //alert(var_id);
            //$("label.file-upload__button").attr("for", var_id);

            $('body').removeClass().addClass('user-feedback Chrome').attr('id', 'user-feedback');

        })
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Web.UI;
using System.Web.UI.WebControls;
using MM.DAL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MM_Rewards;
using BL_Rewards.BL_Rewards;
using BL_Rewards.BL_Common;
using BE_Rewards.BE_Admin;
using BE_Rewards.BE_Rewards;
using BE_Rewards;
using MM_Rewards.Web_Pages.Admin;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Net;
using System.Text.RegularExpressions;
namespace MM_Rewards
{
    //-----------------------------------------------------------------------------------------------------
    public partial class RecipientAward : System.Web.UI.Page
    {
        ExceptionLogging objExceptionLogging = new ExceptionLogging();
        DAL_Common objDALCommon = new DAL_Common();
        BL_RecipientAward objBLRecipientAward = new BL_RecipientAward();
        BE_RecipientAward ObjBERecipientAward = new BE_RecipientAward();
        BE_GiftHampers ObjBEGiftHamper = new BE_GiftHampers();
        BE_GiftVouchers ObjBEGiftVoucher = new BE_GiftVouchers();
        BE_SpotKind ObjBESpotKind = new BE_SpotKind();
        MahindraAwardService objGiftHamper1 = new MahindraAwardService();
        Common objCommon = new Common();
        object _stat;
        int _Status;
        string TokenNumber;
        string AwardIDOut = "", Request_Id = "";
        int Levels = 0;
        public string scroll1 = "0";
        string strCC = string.Empty;
        Boolean blnMailTrigger = false;
        //-----------------------------------------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["DIVCHECK"] != null)
                {
                    ptpgrey.Attributes.Add("style", "display:none");
                    popupContainer.Attributes.Add("style", "display:none");
                    Session["DIVCHECK"] = null;
                    //ddlAwardType.SelectedItem.Text="Spot Award";
                }
                //Display Note
                string strNote = ConfigurationManager.AppSettings["note"].ToString();

                if (!String.IsNullOrEmpty(strNote))
                {
                    lblNote.Text = strNote + "<br /><br />";
                    lblNote.Visible = true;
                }

                if (!IsPostBack)
                {
                    string windowsUserName = WindowsIdentity.GetCurrent().Name;
                    windowsUserName = windowsUserName.Substring(windowsUserName.LastIndexOf("\\") + 1);
                    if (Session["TokenNumber"] == null)
                    {
                        Response.Redirect("/Web_Pages/HomePage.aspx");
                    }
                    else
                    {
                        //Levels = Convert.ToInt16(objCommon.GetUserDetails(Session["TokenNumber"].ToString()));

                        Levels = Convert.ToInt32(Session["Levels"].ToString());
                        Session["AwardCollection"] = null;
                        LoadAwardsDropDown(Levels);
                        //txtMailID.Text = objBLRecipientAward.GetEmailCCId(Session["TokenNumber"].ToString());
                        //if (txtMailID.Text.Trim() != "")
                        //{
                        //    txtMailID.Text = txtMailID.Text.Trim() + ";";
                        //}
                    }
                    getUserDetails();
                    ////tblRecipientList.Attributes["style"] = "display:none";
                    btnSpotAward_Click(sender ,e);
                }
            }
            catch (Exception ex)
            {
                // ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx");
            }
        }

        private void LoadHodReportEmployee()
        {

            string TokennNumber = (Session["TokenNumber"]).ToString();
            DataSet ds = new DataSet();
            //  ds = objBLRecipientAward.GetDirectEmployeeDetail(TokenNumber);

            ds = new DataSet();
            objDALCommon = new DAL_Common();
            BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokennNumber);
            ds = objDALCommon.DAL_GetData("GetHodReportDirctEmp", objsqlparam);

            ddlHodReportEmployee.DataSource = ds;
            //ddlAwardType.DataTextField = "Name";
            ddlHodReportEmployee.DataTextField = "EmployeeName";
            ddlHodReportEmployee.DataValueField = "TokenNumber";
            ddlHodReportEmployee.DataBind();
            ddlHodReportEmployee.Items.Insert(0, new ListItem("Select", "Select"));
            //s ddlHodReportEmployee.Items.Add(new ListItem("Other", "10"));

        }
        private void LoadPillars(string type)
        {
            DataSet ds = new DataSet();
            ds = objBLRecipientAward.GetPillars(type);

            ddlPillars.DataSource = ds.Tables[0];
            
            ddlPillars.DataValueField = "Id";
            ddlPillars.DataTextField = "CardName";
            ddlPillars.DataBind();
            ddlPillars.Items.Insert(0, new ListItem("Select", "Select"));
        }

        #region Common Function

        //This Function is used to Authenticate Login User and to Dsiplay Details (Means HOD or Heads)

        public void getUserDetails()
        {
            try
            {
                string windowsUserName = WindowsIdentity.GetCurrent().Name;
                windowsUserName = windowsUserName.Substring(windowsUserName.LastIndexOf("\\") + 1);
                txtHODName.Text = (string)Session["UserName"];
                txtHODProcess.Text = (string)Session["Division"];
                txtHODLocation.Text = (string)Session["Location"];
                lblBalance.Text = Session["Balance"].ToString() != "" ? Convert.ToDecimal(Session["Balance"].ToString()).ToString("00.00") : "0";
                lblExpenses.Text = Session["Expenses"].ToString() != "" ? Convert.ToDecimal(Session["Expenses"].ToString()).ToString("00.00") : "0";
                lblDeducted.Text = Session["RollUpBalance"].ToString() != "" ? Convert.ToDecimal(Session["RollUpBalance"].ToString()).ToString("00.00") : "0";
                //    lblBudget.Text = (Convert.ToDecimal(lblBalance.Text) + Convert.ToDecimal(lblExpenses.Text)).ToString();
                lblBudget.Text = Session["TotalYearlyBudget"].ToString() != "" ? Convert.ToDecimal(Session["TotalYearlyBudget"].ToString()).ToString("00.00") : "0";
                lblAdded.Text = Session["AmountAdded"].ToString() != "" ? Convert.ToDecimal(Session["AmountAdded"].ToString()).ToString("00.00") : "0";
                HiddenBalance.Value = lblBalance.Text;
            }
            catch
            { }
        }
        //Used on Reset Button Click

        private void ClearScreen(bool IsReset = true)
        {
            if (IsReset == true)
            {
                ddlAwardType.SelectedIndex = 0;
                ddlHodReportEmployee.SelectedIndex = 0;
                txtTokenNumberTop.Text = "";
                txtEmpReason.Text = "";
                ////lblGetEmpResponse.Text = "";
                grdRecipientList.DataSource = null;
                grdRecipientList.DataBind();
                divEmpList.Attributes["style"] = "display:none";
                ////tblRecipientList.Attributes["style"] = "display:none";
                Date.Clear();
                lblFromDate.Attributes.Add("display", "none");
                lblFromDate.Attributes["style"] = "display : none";
                Date.Attributes.Add("display", "none");
                Date.Attributes["style"] = "display : none";
                FromTime.Attributes.Add("display", "none");
                FromTime.Attributes["style"] = "display : none";
                spnTime.Attributes.Add("display", "none");
                spnTime.Attributes["style"] = "display : none";
                ddlGifts.Attributes["style"] = "display : none";
                //s btnGetEmpDetails1.Attributes["style"] = "display:none";

                txtName.Text = "";
                txtMailID.Text = "";
                txtEmpReason.Text = "";
            }
            else if (IsReset == false)
            {
                txtEmpReason.Text = "";
                ////lblGetEmpResponse.Text = "";
                /// txtMailID.Text = "";
                //s btnGetEmpDetails1.Attributes["style"] = "display.none";
            }
        }
        //To get List of Recipient By Entering FirstName or lastName  or Token Number

        private void GetRecipientList()
        {
            var a = Convert.ToInt32(ddlAwardType.SelectedValue.Split('|')[0].ToString());//Rakesh
            BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();
            objBERecipientAwardColl = objBLRecipientAward.GetRecipientList(txtTokenNumberTop.Text.Trim(), Session["TokenNumber"].ToString(), Convert.ToInt32(ddlAwardType.SelectedValue.Split('|')[0].ToString()));
            if (objBERecipientAwardColl.Count == 0)
            {
                ThrowAlertMessage("No data matched your search criteria.");
                divEmpList.Attributes["style"] = "display:none";
                // divEmpList.Visible = false;
            }
            else
            {
                grdRecipientList.DataSource = objBERecipientAwardColl;
                grdRecipientList.DataBind();
                //divEmpList.Visible = true;
                divEmpList.Attributes["style"] = "display:block";
                //searchempmodal.Visible = true;
                // hdnRecipient_EmailId.Value = ObjBERecipientAward.EmailID;
            }
        }
        //Top Load Drop Down        
        private void LoadAwardsDropDown(int Levels)
        {
            BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
            BE_AwardManagement objBEAwardMangement = new BE_AwardManagement();
            objAwardManagementColl = Get_BE_AwardManagementColl(Levels);
            BE_AwardManagement objBEAwardMangement1 = new BE_AwardManagement();
            //Session["objAwardColl"] = objAwardManagementColl;
            string strRecptAward = string.Empty;
            // by mahesh start
            for (int count = objAwardManagementColl.Count - 1; count >= 0; count--)
            {
                if (strRecptAward.Trim() == objAwardManagementColl[count].AwardType.ToString().Trim())
                {
                    objAwardManagementColl.Remove(objAwardManagementColl[count]);
                }
                strRecptAward = objAwardManagementColl[count].AwardType.ToString().Trim();
                if (strRecptAward.Contains("Spot Award") == true)
                {
                    btnSpotAward.Visible = true;
                }
                else if (strRecptAward.Contains("Excellerator") == true)
                {
                    btnExcelleratorAward.Visible = true;
                }
                //objAwardManagementColl[count].Name = objAwardManagementColl[count].Name;// +" [RS. " + objAwardManagementColl[count].Amount + "]";
                //objAwardManagementColl[count].AwardType = objAwardManagementColl[count].AwardType;// +" [RS. " + objAwardManagementColl[count].Amount + "]";
            }
            // by mahesh end
            Session["objAwardColl"] = objAwardManagementColl;
            ddlAwardType.DataSource = objAwardManagementColl;
            //ddlAwardType.DataTextField = "Name";
            ddlAwardType.DataTextField = "AwardType";
            ddlAwardType.DataValueField = "ID_IsAuto";
            ddlAwardType.DataBind();
            ddlAwardType.Items.Insert(0, new ListItem("Select", "Select"));
            //  ddlAwardType.Items[0].Attributes.Add("disabled", "disabled");
        }


        private BE_AwardManagementColl Get_BE_AwardManagementColl(int Levels)
        {
            BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
            if (Session["AwardCollection"] == null)
            {
                objBE_AwardManagementColl = objBLRecipientAward.GetAwardList(Levels);
                Session["AwardCollection"] = objBE_AwardManagementColl;
            }
            else
                objBE_AwardManagementColl = Session["AwardCollection"] as BE_AwardManagementColl;
            return objBE_AwardManagementColl;
        }

        private BE_AwardManagementColl Get_BE_AwardManagementColl_amt(int Levels, string awardtype)
        {
            BE_AwardManagementColl objBE_AwardManagementColl_amt = new BE_AwardManagementColl();
            objBE_AwardManagementColl_amt = objBLRecipientAward.GetAwardList_amt(Levels, awardtype);
            return objBE_AwardManagementColl_amt;
        }

        private void ThrowAlertMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        private void ThrowAlertCCEmailMessage(string strmessage)
        {
            if (strmessage.Contains("'"))
            {
                strmessage = strmessage.Replace("'", "\\'");
            }
            string script = @"alert('" + strmessage + "');";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IDName", script, true);
        }

        #endregion

        #region Grid For Recipient List

        protected void grdRecipientList_RowCommand(object sender, GridViewCommandEventArgs e) //xxxxx
        {
            try
            {
                string RecipientTokenNumber = "";
                if (e.CommandName == "Select")
                {
                    ObjBERecipientAward = new BE_RecipientAward();
                    RecipientTokenNumber = e.CommandArgument.ToString();
                    hdnRecipientTokenNumber.Value = Convert.ToString(RecipientTokenNumber);
                    ClearScreen(false);
                    ObjBERecipientAward = objBLRecipientAward.GetRecipientDetails(hdnRecipientTokenNumber.Value);
                    if (ddlAwardType.SelectedItem.Text.ToString().ToUpper().Contains("COFFEE"))
                    {
                        int giver_level = 0;
                        if (Session["Levels"] != null)
                        {
                            giver_level = Convert.ToInt16(Session["Levels"]);
                        }
                        int recipient_level = Convert.ToInt16(ObjBERecipientAward.Level);
                        if (!(giver_level < recipient_level && (giver_level + 1) != recipient_level))
                        {
                            ThrowAlertMessage(MM_Messages.NotEligible);
                            return;
                        }
                    }

                    ////tblRecipientList.Attributes.Add("style", "display:table");
                    DataSet ds = new DataSet();
                    objDALCommon = new DAL_Common();
                    SqlParameter[] objsqlparam = new SqlParameter[1];
                    objsqlparam[0] = new SqlParameter("@TokenNumber", RecipientTokenNumber);
                    ds = objDALCommon.DAL_GetDataN("usp_RecipientAwards_View", objsqlparam);
                    hdnEmpName.Value = ds.Tables[0].Rows[0]["UserName"].ToString();
                    lblEmpDivision.Text = ds.Tables[0].Rows[0]["DivisionName_PA"].ToString();
                    hdnEmpDesignation.Value = ds.Tables[0].Rows[0]["Designation"].ToString();
                    lblEmpLevel.Text = ds.Tables[0].Rows[0]["EmployeeLevelName_ES"].ToString();
                    hdnLoactionCode.Value = ds.Tables[0].Rows[0]["LocationName_PSA"].ToString();
                    hdnEmpDepartmentName.Value = ds.Tables[0].Rows[0]["OrgUnitName"].ToString();
                    HdnDivisionCode.Value = ds.Tables[0].Rows[0]["Division_PA"].ToString();
                    hdnEmailId.Value = ds.Tables[0].Rows[0]["EmailID"].ToString();
                    lblTokenno.Text = ds.Tables[0].Rows[0]["TokenNumber"].ToString();
                    lblfirstname.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                    lblLastName.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
                    Session["FirstName"] = lblfirstname.Text.ToString();
                    Session["LastName"] = lblLastName.Text.ToString();
                    //To Display on PreView by Rakesh
                    lblfirstnamePreview.Text = lblfirstname.Text;
                    lblLastNamePreview.Text = lblLastName.Text;
                    lblTokennoPreview.Text = lblTokenno.Text;
                    lblEmpDivisionPreview.Text = lblEmpDivision.Text;
                    lblEmpLevelPreview.Text = lblEmpLevel.Text;
                    //lblDepartmentPreview.Text = ds.Tables[0].Rows[0]["EmailID"].ToString();
                    //lblBusinessUnitPreview.Text = ds.Tables[0].Rows[0]["EmailID"].ToString();

                    //lblTokenno.Text = ObjBERecipientAward.TokenNumber;// RecipientTokenNumber;//ObjBERecipientAward.UserName;
                    //lblEmpDivision.Text = ObjBERecipientAward.DivisionName_PA;
                    //lblfirstname.Text = ObjBERecipientAward.FirstName;
                    //lblLastName.Text = ObjBERecipientAward.LastName;
                    //HdnDivisionCode.Value = ObjBERecipientAward.DivisionCode_PA;
                    //  hdnEmpDepartmentName.Value = ObjBERecipientAward.OrgUnitName;
                    //hdnEmpDesignation.Value = ObjBERecipientAward.Designation;
                    //lblEmpLevel.Text = ObjBERecipientAward.EmployeeLevelName_ES;                
                    //hdnLoactionCode.Value = ObjBERecipientAward.LocationCode_PSA;
                    //hdnEmailId.Value = ObjBERecipientAward.EmailID; 
                    HidePopUp();
                    txtMailID.Text = objBLRecipientAward.GetEmailCCId(Session["TokenNumber"].ToString());
                    if (txtMailID.Text.Trim() != "")
                    {
                        txtMailID.Text = txtMailID.Text.Trim() + ";";
                    }

                    txtName.Text = "";
                    divEmpList.Attributes["style"] = "display:block";
                }
            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "grdRecipientList_RowCommand" + ex.StackTrace);
            }
        }

        #endregion

        #region Grid For Emp List added by gaurav


        protected void grdEmpList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Select")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    //Store Email ID value in string
                    string strEmailID = row.Cells[3].Text;
                    if (txtMailID.Text.ToString().ToUpper().Contains(strEmailID.ToString().ToUpper().Trim()) == false)
                    {
                        txtMailID.Text = txtMailID.Text + strEmailID + ";";
                    }
                    // btnSubmit.Focus();
                    ////myModal.Attributes["style"] = "display:none";
                    grdRecipientList.Attributes["style"] = "display:none";
                }
            }
            catch (Exception ex)
            {
                //ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "grdEmpList_RowCommand" + ex.StackTrace);
            }
        }

        #endregion


        #region Events

        //To Get Details of Employee to Whom Award is going to get....

        protected void btnGetEmpDetails_Click(object sender, EventArgs e)
        {
            try
            {

                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                objAwardManagementColl = Session["objAwardColl"] as BE_AwardManagementColl;
                //  BE_AwardManagement objBEAwardMangement = new BE_AwardManagement();
                //objBEAwardMangement = objAwardManagementColl.Find(delegate(BE_AwardManagement AwardMange) { return AwardMange.Id == Convert.ToInt32(ddlAwardType.SelectedItem.Value); });
                //  objBEAwardMangement = objAwardManagementColl.Find(AwardManage => AwardManage.Id == Convert.ToInt32(ddlAwardType.SelectedItem.Value.Split('|')[0]));

                if (ddlAwardType.SelectedItem.Text.ToString().ToUpper().Contains("COFFEE"))
                {
                    if (Date.IsEmpty)
                    {
                        ThrowAlertMessage(MM_Messages.EmptyDate);
                        return;
                    }
                    if (FromTime.IsEmpty)
                    {
                        ThrowAlertMessage(MM_Messages.EmptyTime);
                        return;
                    }
                }

                if (ddlAwardType.SelectedIndex <= 0)
                {
                    grdRecipientList.DataSource = null;
                    //  grdRecipientList.Attributes["style"] = "display : none";
                    divEmpList.Attributes["style"] = "display:none";
                    ThrowAlertMessage("Please select Award Type");
                }

                //else if (Convert.ToDouble(lblBalance.Text) < Convert.ToDouble(objBEAwardMangement.Amount))
                else if (Convert.ToDouble(lblBalance.Text) < Convert.ToDouble(ddlGifts.SelectedItem.ToString()))
                {
                    ThrowAlertMessage("You do not have budget for the selected Award. Please select another award.");
                    grdRecipientList.DataSource = null;
                    //  grdRecipientList.Attributes["style"] = "display : none";                  
                    divEmpList.Attributes["style"] = "display:none";

                }
                else
                {

                    GetRecipientList();
                    divEmpList.Attributes["style"] = "display:block";
                    grdRecipientList.Attributes["style"] = "display:table";

                    searchnomineemodal.Attributes.Add("class", "modal fade search-emp-modal in show");
                    searchnomineemodal.Attributes.Add("style", "display:block");
                    searchnomineemodal.Attributes.Remove("aria-hidden");
                    searchnomineemodal.Attributes.Add("aria-modal", "true");

                    ////tblRecipientList.Attributes["style"] = "display : none";

                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "scrollWin()", true);
                }


                BE_AwardManagement objBEAwardMangement = new BE_AwardManagement();
                //objBEAwardMangement = objAwardManagementColl.Find(delegate(BE_AwardManagement AwardMange) { return AwardMange.Id == Convert.ToInt32(ddlAwardType.SelectedItem.Value); });
                objBEAwardMangement = objAwardManagementColl.Find(AwardManage => AwardManage.Id == Convert.ToInt32(ddlAwardType.SelectedItem.Value.Split('|')[0]));
            }
            catch (Exception ex)
            {
                //ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "btnGetEmpDetails_Click" + ex.StackTrace);
            }
        }
        //To Save Details of Employee who Gets Award and Also Seneding Confirmation MAil to HOD

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                objAwardManagementColl = Session["objAwardColl"] as BE_AwardManagementColl;
                BE_AwardManagement objBEAwardMangement = new BE_AwardManagement();
                //objBEAwardMangement = objAwardManagementColl.Find(delegate(BE_AwardManagement AwardMange) { return AwardMange.Id == Convert.ToInt32(ddlAwardType.SelectedItem.Value); });
                objBEAwardMangement = objAwardManagementColl.Find(delegate(BE_AwardManagement AwardMange) { return AwardMange.Id == Convert.ToInt32(ddlAwardType.SelectedValue.Split('|')[0]); });
                //if (Convert.ToDouble(lblBalance.Text) < Convert.ToDouble(objBEAwardMangement.Amount))
                if (Convert.ToDouble(lblBalance.Text) < Convert.ToDouble(ddlGifts.SelectedItem.ToString()))
                {
                    ThrowAlertMessage("You do not have budget for the selected Award. Please select another award.");
                }
                else
                {
                    //objBEAwardMangement = objAwardManagementColl.Find(delegate(BE_AwardManagement AwardMange) { return AwardMange.Id == Convert.ToInt32(ddlAwardType.SelectedItem.Value); });
                    objBEAwardMangement = objAwardManagementColl.Find(delegate(BE_AwardManagement AwardMange) { return AwardMange.Id == Convert.ToInt32(ddlAwardType.SelectedValue.Split('|')[0]); });
                    int Id = Convert.ToInt32(ddlAwardType.SelectedValue.Split('|')[0]);
                    // int Id1 = Convert.ToInt32(ddlGifts.SelectedValue.Split('|')[0]); // by vandana
                    BE_MailDetails objMailDetails = new BE_MailDetails();
                    BE_AwardManagement objBE_AwardManagement = new BE_AwardManagement();
                    //Gettin ID On Selection of User from GRID, using Delegate
                    objBE_AwardManagement = ((BE_AwardManagementColl)Session["AwardCollection"]).Find(delegate(BE_AwardManagement AM) { return AM.Id == Id; });
                    //objBE_AwardManagement = ((BE_AwardManagementColl)Session["AwardCollection"]).Find(AM => AM.Id == Id);
                    TokenNumber = (string)Session["TokenNumber"];
                    ObjBERecipientAward.TokenNumber = hdnRecipientTokenNumber.Value;
                    ObjBERecipientAward.LocationCode_PSA = hdnLoactionCode.Value;
                    ObjBERecipientAward.OrgUnitName = hdnEmpDepartmentName.Value.Trim();
                    ObjBERecipientAward.Designation = hdnEmpDesignation.Value.Trim();
                    // ObjBERecipientAward.MasAwardID = Convert.ToInt32(ddlAwardType.SelectedValue.Split('|')[0]);
                    ObjBERecipientAward.MasAwardID = Convert.ToInt32(ddlGifts.SelectedValue.Split('|')[0]); // by vandana
                    ObjBERecipientAward.AwardType = objBE_AwardManagement.AwardType;
                    // by vandana start
                    if (objBE_AwardManagement.AwardType.ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                    {
                        ObjBERecipientAward.AwardAmount = decimal.Parse("0");
                    }
                    else
                    {
                        ObjBERecipientAward.AwardAmount = decimal.Parse(ddlGifts.SelectedItem.ToString()); // by vandana
                    }
                    // by vandana end
                    ObjBERecipientAward.DivisionCode_PA = HdnDivisionCode.Value;
                    //ObjBERecipientAward.ReasonForNomination = txtEmpReason.Text.Trim();
                    ObjBERecipientAward.ReasonForNomination = txtCertificateText.Text.Trim();//by satyam shukla ask by rakesh
                    ObjBERecipientAward.GiverTokenNumber = TokenNumber;
                    // by gaurav gaurav
                    if (Date.SelectedDate != null)
                    {
                        ObjBERecipientAward.CoffeeDate = Date.SelectedDate.Value.Day.ToString() + "-" + Date.SelectedDate.Value.Month.ToString() + "-" + Date.SelectedDate.Value.Year.ToString();
                    }
                    else
                    {
                        ObjBERecipientAward.CoffeeDate = string.Empty;
                    }
                    Boolean flag = false;
                    StringBuilder builder = new StringBuilder();
                    string EmID = txtMailID.Text.Replace(";", ",");
                    if (EmID == "")
                    {
                        flag = true;
                    }
                    else
                    {
                        string[] Emails = Regex.Split(EmID.TrimEnd(','), ",");
                        foreach (string Email in Emails)
                        {
                            Boolean check = IsValidEmail(Email.Trim());
                            if (check == true)
                            {
                                builder.Append(Email).Append(",");
                                flag = true;
                            }
                            else
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (flag == false)
                    {
                        ThrowAlertCCEmailMessage("Please enter correct cc email id.");
                    }
                    else
                    {
                        ObjBERecipientAward.CCEmailID = builder.ToString().Trim(',');
                        //To Save
                        int _SaveStatus = objBLRecipientAward.SaveData(ObjBERecipientAward, out AwardIDOut);
                        Hodlbl.Text = Session["UserName"].ToString();
                        lblFirst.Text = Session["FirstName"].ToString();
                        lbllast.Text = Session["LastName"].ToString();
                        // ThrowAlertMessage("");
                        SuccessModal.Attributes["style"] = "display:block";
                        SuccessModal.Attributes.Add("class", "modal fade search-emp-modal show in");
                        //// myModal.Attributes["style"] = "display:none";

                        string awardidout = AwardIDOut;
                        string award_id = awardidout.Substring(awardidout.LastIndexOf(':') + 1);
                        ClearScreen();
                        Levels = Convert.ToInt16(objCommon.GetUserDetails(Session["TokenNumber"].ToString()));
                        getUserDetails();
                        // ddlHodReportEmployee.SelectedIndex = 0;
                        //   tblRecipientList.Attributes["style"] = "display:none";

                        #region  commented code by anat to remove mail sending code

                        if (false)
                        {
                            #region Gift_Hamper
                            //-----------------------------------------------------------------------------------------------------
                            // insert data in Gift Hamper table
                            if (Session["TokenNumber"] != null)
                            {
                                //if (objBE_AwardManagement.AwardType == "Gift Hamper")
                                if (objBE_AwardManagement.AwardType.ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                                {
                                    ObjBEGiftHamper.NomName = txtHODName.Text.Trim();
                                    ObjBEGiftHamper.NomEmpId = (string)Session["TokenNumber"];
                                    if (Session["EmailID"] != null)
                                    {
                                        ObjBEGiftHamper.NomEmailID = (string)Session["EmailID"];
                                    }
                                    //added
                                    ObjBEGiftHamper.RequestId = award_id;
                                    ObjBEGiftHamper.RecName = hdnEmpName.Value.Trim();
                                    ObjBEGiftHamper.RecEmpId = hdnRecipientTokenNumber.Value;
                                    ObjBEGiftHamper.RecEmailID = hdnEmailId.Value;
                                    ObjBEGiftHamper.DeliveryAddress = string.Empty;
                                    ObjBEGiftHamper.GiftAmount = ddlGifts.SelectedItem.ToString();
                                    ObjBEGiftHamper.RequestStatus = "Pending";
                                    ObjBEGiftHamper.RequestId = "";
                                    ObjBEGiftHamper.RequestDate = DateTime.Today.ToString("dd-MMM-yyyy");
                                    ObjBEGiftHamper.Pincode = string.Empty;
                                    ObjBEGiftHamper.AwardId = award_id; // by vandana
                                    ObjBEGiftHamper.AwardBusinessUnit = hdnEmpDepartmentName.Value.Trim();
                                    ObjBEGiftHamper.AwardDivision = lblEmpDivision.Text.Trim();
                                    ObjBEGiftHamper.AwardLocation = hdnEmpLocation.Value.Trim();
                                    // int insertStatus = objBLRecipientAward.SaveGiftHamperData(ObjBEGiftHamper, out Request_Id);
                                    ObjBEGiftHamper.RequestId = Request_Id;
                                    try
                                    {
                                        string strHamper = "SUCCESS"; // benefit plus do not want value from web service it will work from catalogue
                                        //string strHamper = objGiftHamper1.SendRequestDetails(ObjBEGiftHamper);
                                        if (strHamper.ToString().ToUpper().Contains("SUCCESS") == true || strHamper.ToString().ToUpper().Contains("DUPLICATE ENTRY") == true)
                                        {
                                            blnMailTrigger = false;
                                        }
                                        else
                                        {
                                            blnMailTrigger = true;
                                        }
                                    }
                                    catch (Exception objException)
                                    {
                                        blnMailTrigger = true;
                                        Console.WriteLine("hamper not inserted" + objException.StackTrace);
                                        Console.WriteLine(objException.Message + " " + "Gift_Hamper");
                                    }
                                    lblGiftHamper.Visible = false;
                                }
                            }
                            //-----------------------------------------------------------------------------------------------------
                            #endregion Gift_Hamper
                            //-----------------------------------------------------------------------------------------------------
                            // Gift Hamper code ends here
                            //-----------------------------------------------------------------------------------------------------
                            #region Vouchers
                            //-----------------------------------------------------------------------------------------------------
                            // insert data in Gift Voucher table
                            if (Session["TokenNumber"] != null)
                            {
                                if (objBE_AwardManagement.AwardType.ToString().ToUpper() == ConfigurationManager.AppSettings["Vouchers"].ToString().ToUpper())
                                {
                                    ObjBEGiftVoucher.RequestId = award_id;// "";
                                    ObjBEGiftVoucher.RequestDate = DateTime.Today.ToString("dd-MMM-yyyy");
                                    ObjBEGiftVoucher.NomName = txtHODName.Text.Trim();
                                    ObjBEGiftVoucher.NomEmpId = (string)Session["TokenNumber"];
                                    ObjBEGiftVoucher.NomEmail = Session["EmailID"].ToString();
                                    ObjBEGiftVoucher.RecName = hdnEmpName.Value.Trim();
                                    ObjBEGiftVoucher.RecEmpId = hdnRecipientTokenNumber.Value;
                                    ObjBEGiftVoucher.RecEmail = hdnEmailId.Value;
                                    ObjBEGiftVoucher.GiftAmount = ddlGifts.SelectedItem.ToString();
                                    ObjBEGiftVoucher.RequestStatus = "Pending";
                                    ObjBEGiftVoucher.AwardBusinessUnit = hdnEmpDepartmentName.Value.Trim();
                                    ObjBEGiftVoucher.AwardDivision = lblEmpDivision.Text.Trim();
                                    ObjBEGiftVoucher.AwardLocation = hdnEmpLocation.Value.Trim();
                                    ObjBEGiftVoucher.AwardID = award_id; //by vandana
                                    //int insertStatus1 = objBLRecipientAward.SaveGiftVoucherData(ObjBEGiftVoucher, out Request_Id);
                                    ObjBEGiftVoucher.RequestId = Request_Id;
                                    try
                                    {
                                        string strVoucher = objGiftHamper1.SendVoucherRequestDetails(ObjBEGiftVoucher);
                                        Exception ex = new Exception();
                                        objExceptionLogging.LogErrorMessage(ex, "Voucher Id - " + ObjBEGiftVoucher.RequestId + ", Status - " + strVoucher);
                                        if (strVoucher.ToString().ToUpper().Contains("SUCCESS") == true || strVoucher.ToString().ToUpper().Contains("DUPLICATE ENTRY") == true)
                                        {
                                            blnMailTrigger = false;
                                        }
                                        else
                                        {
                                            blnMailTrigger = true;
                                        }
                                    }
                                    catch (Exception objException)
                                    {
                                        blnMailTrigger = true;
                                        Console.WriteLine("Vouchers not inserted" + objException.StackTrace);
                                        Console.WriteLine(objException.Message);
                                    }
                                }
                            }
                            //-----------------------------------------------------------------------------------------------------
                            #endregion Vouchers
                            //-----------------------------------------------------------------------------------------------------
                            //-----------------------------------------------------------------------------------------------------
                            #region mailer
                            //-----------------------------------------------------------------------------------------------------
                            if (blnMailTrigger == true)
                            {
                                objCommon = new Common();
                                objMailDetails = objCommon.GetMailDetails(2, AwardIDOut);//To get Details for sending Confirmation MAil
                                string path = System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath");
                                if (objBE_AwardManagement.AwardType.ToString().ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                                {
                                    //string fileName = "Webservice_GiftHamper.txt";
                                    string fileName = "Webservice_GiftHamper.txt";
                                    StreamReader sr = new StreamReader(path + fileName);
                                    //StreamReader sr = new StreamReader(@"D:\VSS\MMRewards_27Mar14\MM_Rewards.root\MM_Rewards\MM_Rewards\Templates\Webservice_GiftHamper.txt");
                                    //StreamReader sr = new StreamReader(@"D:\VSS\MMRewards_27Mar14\MM_Rewards.root\MM_Rewards\MM_Rewards\Templates\Webservice_GiftHamper.txt");
                                    StringBuilder sb = new StringBuilder();
                                    string BodyPart = sr.ReadToEnd();
                                    StringBuilder strBodyPart = new StringBuilder(BodyPart);
                                    // B
                                    // Replace the first word
                                    // The result doesn't need assignment
                                    strBodyPart.Replace("<%RequestId%>", ObjBEGiftHamper.RequestId.ToString());
                                    strBodyPart.Replace("<%RequestDate%>", ObjBEGiftHamper.RequestDate.ToString());
                                    strBodyPart.Replace("<%NominatorName%>", ObjBEGiftHamper.NomName.ToString());
                                    strBodyPart.Replace("<%NominatorEmpID%>", ObjBEGiftHamper.NomEmpId.ToString());
                                    strBodyPart.Replace("<%RecipientName%>", ObjBEGiftHamper.RecName.ToString());
                                    strBodyPart.Replace("<%RecipientEmpId%>", ObjBEGiftHamper.RecEmpId.ToString());
                                    //  strBodyPart.Replace("<%DeliveryAddress%>", ObjBEGiftHamper.DeliveryAddress.ToString());
                                    //  strBodyPart.Replace("<%Pincode%>", ObjBEGiftHamper.Pincode.ToString());
                                    strBodyPart.Replace("<%GiftAmount%>", ObjBEGiftHamper.GiftAmount.ToString());
                                    strBodyPart.Replace("<%RequestStatus%>", ObjBEGiftHamper.RequestStatus.ToString());
                                    sr.Close();
                                    ObjBERecipientAward.CCEmailID = txtMailID.Text;
                                    try
                                    {
                                        //    objCommon.sendMail(
                                        //    objMailDetails.Fromid,
                                        //    objMailDetails.Fromname,
                                        //    ConfigurationManager.AppSettings["BP_ToEmailID"].ToString(),
                                        //        //"vandana_pandey@idealake.com", // testing
                                        //    txtEmpName.Text,
                                        //    "",
                                        //        // ObjBERecipientAward.CCEmailID,  // objMailDetails.Ccid,    by gaurav
                                        //    objMailDetails.Subject,
                                        //    strBodyPart.ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        //ThrowAlertMessage(MM_Messages.DatabaseError);
                                        objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx: Voucher mail not send to Benifit Plus");
                                    }
                                    //ThrowAlertMessage(MM_Messages.DataSaved);
                                }
                                else if (objBE_AwardManagement.AwardType.ToString().ToUpper() == ConfigurationManager.AppSettings["Vouchers"].ToString().ToUpper())
                                {
                                    //string fileName = "Webservice_GiftHamper.txt";
                                    //string path = System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath");
                                    string fileName = "Webservice_GiftVoucher.txt";
                                    StreamReader sr = new StreamReader(path + fileName);
                                    //StreamReader sr = new StreamReader(@"D:\VSS\MMRewards_27Mar14\MM_Rewards.root\MM_Rewards\MM_Rewards\Templates\Webservice_GiftHamper.txt");
                                    //StreamReader sr = new StreamReader(@"D:\VSS\MMRewards_27Mar14\MM_Rewards.root\MM_Rewards\MM_Rewards\Templates\Webservice_GiftVoucher.txt");
                                    StringBuilder sb = new StringBuilder();
                                    string BodyPart = sr.ReadToEnd();
                                    StringBuilder strBodyPart = new StringBuilder(BodyPart);
                                    // B
                                    // Replace the first word
                                    // The result doesn't need assignment
                                    strBodyPart.Replace("<%RequestId%>", ObjBEGiftVoucher.RequestId.ToString());
                                    strBodyPart.Replace("<%RequestDate%>", ObjBEGiftVoucher.RequestDate.ToString());
                                    strBodyPart.Replace("<%NominatorName%>", ObjBEGiftVoucher.NomName.ToString());
                                    strBodyPart.Replace("<%NominatorEmpID%>", ObjBEGiftVoucher.NomEmpId.ToString());
                                    strBodyPart.Replace("<%NominatorEmailAddress%>", ObjBEGiftVoucher.NomEmail.ToString());
                                    strBodyPart.Replace("<%RecipientName%>", ObjBEGiftVoucher.RecName.ToString());
                                    strBodyPart.Replace("<%RecipientEmailAddress%>", ObjBEGiftVoucher.RecEmail.ToString());
                                    strBodyPart.Replace("<%RecipientEmpId%>", ObjBEGiftVoucher.RecEmpId.ToString());
                                    strBodyPart.Replace("<%GiftAmount%>", ObjBEGiftVoucher.GiftAmount.ToString());
                                    strBodyPart.Replace("<%AwardBussinessUnit%>", ObjBEGiftVoucher.AwardBusinessUnit.ToString());
                                    strBodyPart.Replace("<%AwardDivision%>", ObjBEGiftVoucher.AwardDivision.ToString());
                                    strBodyPart.Replace("<%AwardLocation%>", ObjBEGiftVoucher.AwardLocation.ToString());
                                    strBodyPart.Replace("<%RequestStatus%>", ObjBEGiftVoucher.RequestStatus.ToString());
                                    sr.Close();
                                    objCommon.sendMail(
                                    objMailDetails.Fromid,
                                    objMailDetails.Fromname,
                                    ConfigurationManager.AppSettings["BP_ToEmailID"].ToString(),
                                        //"vandana_pandey@idealake.com", // testing
                                    hdnEmpName.Value,
                                    "",
                                        //ObjBERecipientAward.CCEmailID,  //objMailDetails.Ccid,   by gaurav
                                    objMailDetails.Subject,
                                    strBodyPart.ToString());
                                    ThrowAlertMessage(MM_Messages.DataSaved);
                                }
                            }
                            //-----------------------------------------------------------------------------------------------------
                            #endregion mailer
                            //-----------------------------------------------------------------------------------------------------
                            // Gift Voucher code ends here
                            if (_SaveStatus > 0)
                            {
                                //ThrowAlertMessage(MM_Messages.DatabaseError);
                                FromTime.Attributes.Add("display", "none");
                                FromTime.Attributes["style"] = "display : none";
                            }
                            else
                            {
                                string windowsUserName = WindowsIdentity.GetCurrent().Name;
                                windowsUserName = windowsUserName.Substring(windowsUserName.LastIndexOf("\\") + 1);
                                if (Session["TokenNumber"] != null)
                                {
                                    Levels = Convert.ToInt16(objCommon.GetUserDetails(Session["TokenNumber"].ToString()));
                                }
                                else
                                {
                                    Levels = Convert.ToInt16(objCommon.GetUserDetails(windowsUserName));
                                }
                                getUserDetails();
                                objCommon = new Common();
                                objMailDetails = objCommon.GetMailDetails(2, AwardIDOut);//To get Details for sending Confirmation MAil
                                string path = System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath");
                                string fileName = objBE_AwardManagement.AwardType + "_Confirmation.txt";
                                StreamReader sr = new StreamReader(path + fileName);
                                StringBuilder sb = new StringBuilder();
                                string BodyPart = sr.ReadToEnd();
                                double BAL = Convert.ToDouble(lblBalance.Text.ToString());
                                // BodyPart = BodyPart.Replace("<%RecipientName%>", txtEmpName.Text).Replace("<%AwardName%>", objMailDetails.AwardName.ToString()).Replace("<%AwardAmount%>", objMailDetails.AwardAmount.ToString()).Replace("<%AwardType%>", objMailDetails.AwardType.ToString());
                                // BodyPart = BodyPart.Replace("<%HODName%>", txtHODName.Text).Replace("<%RecipientName%>", txtEmpName.Text).Replace("<%AwardName%>", objMailDetails.AwardName.ToString()).Replace("<%BalanceAmount%>", BAL.ToString()).Replace("<%AwardAmount%>", objMailDetails.AwardAmount.ToString()).Replace("<%AwardType%>", objMailDetails.AwardType.ToString());// by vandana
                                //-----------------------------------------------------------------------------------------------------
                                #region COFFEE
                                //-----------------------------------------------------------------------------------------------------
                                if (ddlAwardType.SelectedItem != null)
                                {
                                    if (ddlAwardType.SelectedItem.Text.ToUpper().Contains("COFFEE") == true)
                                    {
                                        if (Date.SelectedDate != null)
                                        {
                                            if (Request.QueryString["bDate"] != null && Request.QueryString["fromTime"] != null && Request.QueryString["toTime"] != null)
                                            {
                                                DateTime BDate = DateTime.Parse(Request.QueryString["bDate"].ToString());
                                                TimeSpan AfromTime = TimeSpan.Parse(Request.QueryString["fromTime"].ToString());
                                            }
                                            string mailerDate = Date.SelectedDate.Value.ToString("dd/MM/yyyy");
                                            BodyPart = BodyPart.Replace("<%HODName%>", txtHODName.Text).Replace("<%RecipientName%>", hdnEmpName.Value).Replace("<%AwardName%>", objMailDetails.AwardName.ToString()).Replace("<%BalanceAmount%>", BAL.ToString()).Replace("<%AwardAmount%>", objMailDetails.AwardAmount.ToString()).Replace("<%AwardType%>", objMailDetails.AwardType.ToString()).Replace("<%CoffeeDate%>", mailerDate);
                                            //objCommon.SendCalendar("You are invited for coffee with boss 111111111111", objMailDetails.Fromid, Session["EmailID"].ToString(), txtHODName.Text,hdnEmpName.Value, mailerDate, "3:00", "3:00" + "30");
                                            if (FromTime.SelectedDate != null)
                                            {
                                                DateTime dtFromTime = Convert.ToDateTime(Date.SelectedDate.Value.Date.Add(FromTime.SelectedDate.Value.TimeOfDay)).ToUniversalTime();
                                                DateTime dtToTime = Convert.ToDateTime(Date.SelectedDate.Value.Date.Add(FromTime.SelectedDate.Value.TimeOfDay)).AddMinutes(30).ToUniversalTime();
                                                //  objCommon.SendCalendar("",  objMailDetails.Fromid, Session["EmailID"].ToString(), txtHODName.Text, hdnEmpName.Value, mailerDate, dtFromTime, dtToTime);
                                                //objCommon.SendCalendar("", hdnEmailId.Value, objMailDetails.Fromid, Session["EmailID"].ToString(), txtHODName.Text, hdnEmpName.Value, mailerDate, dtFromTime, dtToTime);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        {
                                            BodyPart = BodyPart.Replace("<%HODName%>", txtHODName.Text).Replace("<%RecipientName%>", hdnEmpName.Value).Replace("<%AwardName%>", objMailDetails.AwardName.ToString()).Replace("<%BalanceAmount%>", BAL.ToString()).Replace("<%AwardAmount%>", objMailDetails.AwardAmount.ToString()).Replace("<%AwardType%>", objMailDetails.AwardType.ToString());
                                        }
                                    }
                                }
                                //-----------------------------------------------------------------------------------------------------
                                #endregion COFFEE
                                //-----------------------------------------------------------------------------------------------------
                                try
                                {
                                    objCommon.sendMail(
                                    objMailDetails.Fromid,
                                    objMailDetails.Fromname,
                                    Session["EmailID"].ToString(),
                                        //"vandana_pandey@idealake.com", // testing
                                    hdnEmpName.Value,
                                    "",
                                        // txtMailID.Text,  //  by gaurav
                                    objMailDetails.Subject,
                                    BodyPart);
                                }
                                catch (Exception ex)
                                {
                                    //ThrowAlertMessage(MM_Messages.DatabaseError);
                                    objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "grdEmpList_RowCommand" + ex.StackTrace);
                                }
                                //added by vandana start
                                ddlGifts.ClearSelection();
                                ddlGifts.Attributes.Add("display", "none");
                                ddlGifts.Attributes["style"] = "display : none";
                                // added by vandana end
                                if (objBE_AwardManagement.AwardType.ToString().ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                                {
                                    ThrowAlertMessage(MM_Messages.Redirection);
                                }
                                ThrowAlertMessage(MM_Messages.DataSaved);
                            }
                            ClearScreen();
                            if (objBE_AwardManagement.AwardType.ToString().ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                            {
                                using (System.IO.StreamReader Reader = new System.IO.StreamReader(ConfigurationManager.AppSettings["AwardTypeTemplatePath"].ToString() + "Catalog.htm"))
                                {
                                    StringBuilder Sb = new StringBuilder();
                                    string fileContent = Reader.ReadToEnd();
                                    fileContent = fileContent.Replace("[REQUESTID]", ObjBEGiftHamper.RequestId);
                                    fileContent = fileContent.Replace("[NOMEMPLOYEEID]", ObjBEGiftHamper.NomEmpId);
                                    fileContent = fileContent.Replace("[NOMNAME]", ObjBEGiftHamper.NomName);
                                    fileContent = fileContent.Replace("[NOMEMAIL]", ObjBEGiftHamper.NomEmailID);
                                    fileContent = fileContent.Replace("[RECEMPLOYEEID]", ObjBEGiftHamper.RecEmpId);
                                    fileContent = fileContent.Replace("[RECNAME]", ObjBEGiftHamper.RecName);
                                    fileContent = fileContent.Replace("[RECEMAIL]", ObjBEGiftHamper.RecEmailID);
                                    fileContent = fileContent.Replace("[AMOUNT]", "1000");
                                    RedirectAndPOST(this.Page, fileContent.ToString());
                                }
                                ThrowAlertMessage(MM_Messages.DataSaved);
                            }
                            ThrowAlertMessage(MM_Messages.DataSaved);
                        }
                        #endregion
                    }

                }

            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "btnSubmit_Click" + ex.StackTrace);
            }
        }

        public void RedirectAndPOST(System.Web.UI.Page page, string strForm)
        {
            page.Controls.Add(new LiteralControl(strForm));
        }

        private void SaveGiftHamper(BE_AwardManagement objAwardManagement)
        {
            try
            {
                BE_GiftHampers objGiftHampers = new BE_GiftHampers();
                //objGiftHampers.RequestId = "";
                objGiftHampers.RequestDate = DateTime.Now.ToString();
                objGiftHampers.NomName = hdnEmpName.Value;
                if (Session["TokenNumber"] != null)
                {
                    objGiftHampers.NomEmpId = (string)Session["TokenNumber"];
                }
                objGiftHampers.RecName = hdnEmpName.Value;
                objGiftHampers.RecEmpId = "";
                //objGiftHampers.DeliveryAddress = txtEmpDeliveryAddress.Text;
                //objGiftHampers.Pincode = txtEmpPinCode.Text;
                //objGiftHampers.GiftHamperCode = "";
                objGiftHampers.RequestStatus = "Pending";
            }
            catch
            {
            }
        }


        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {

                ddlHodReportEmployee.SelectedIndex = 0;
                ////tblRecipientList.Attributes["style"] = "display:none";
                txtEmpReason.Text = "";
                txtName.Text = "";
                // txtMailID.Text = "";
                //ClearScreen();                          
                //divEmpList.Attributes["style"] = "display:none";
                //myModal.Attributes["style"] = "display:none";
                //btnGetEmpDetails1.Attributes.Add("style", "display:none");
            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "btnReset_Click" + ex.StackTrace);
            }
        }


        protected void btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                int Id = Convert.ToInt32(ddlAwardType.SelectedValue.Split('|')[0]);
                BE_AwardManagement objBE_AwardManagement = new BE_AwardManagement();
                BE_MailDetails objMailDetails = new BE_MailDetails();
                DataSet dsAward = new DataSet();
                objCommon = new Common();
                dsAward = objCommon.GetAwardAmount(ddlAwardType.SelectedItem.ToString() + " " + ddlGifts.SelectedItem.ToString().Replace(".00", ""));
                objBE_AwardManagement = ((BE_AwardManagementColl)Session["AwardCollection"]).Find(delegate(BE_AwardManagement AM) { return AM.Id == Id; });
                string path1 = System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath");
                string fileName1 = objBE_AwardManagement.AwardType + "_Auto.txt";
                StreamReader sr = new StreamReader(path1 + fileName1);
                StringBuilder sb = new StringBuilder();
                string BodyPart = sr.ReadToEnd();
                double BAL = Convert.ToDouble(lblBalance.Text.ToString());
                if (ddlAwardType.SelectedItem != null)
                {
                    if (ddlAwardType.SelectedItem.Text.ToUpper().Contains("COFFEE") == true)
                    {
                        //  BodyPart = BodyPart.Replace("<%RecipientName%>", hdnEmpName.Value).Replace("<%AwardType%>", ddlAwardType.SelectedItem.ToString().Split(' ')[0]).Replace("<%AwardAmount%>", dsAward.Tables[0].Rows[0]["Amount"].ToString().Split('.')[0]).Replace("<%GiverHODName%>", txtHODName.Text).Replace("<%reason%>", txtEmpReason.Text).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString()).Replace("<%CoffeeDate%>", Date.SelectedDate.Value.ToString("dd/MM/yyyy"));
                        BodyPart = BodyPart.Replace("<%RecipientName%>", hdnEmpName.Value).Replace("<%AwardType%>", ddlAwardType.SelectedItem.ToString().Split(' ')[0]).Replace("<%AwardAmount%>", ddlGifts.SelectedItem.ToString().Split('.')[0]).Replace("<%GiverHODName%>", txtHODName.Text).Replace("<%reason%>", txtCertificateText.Text).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString()).Replace("<%CoffeeDate%>", Date.SelectedDate.Value.ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        //BodyPart = BodyPart.Replace("<%RecipientName%>", hdnEmpName.Value).Replace("<%AwardType%>", ddlAwardType.SelectedItem.ToString().Split(' ')[0]).Replace("<%AwardAmount%>", dsAward.Tables[0].Rows[0]["Amount"].ToString().Split('.')[0]).Replace("<%GiverHODName%>", txtHODName.Text).Replace("<%reason%>", txtEmpReason.Text).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                        BodyPart = BodyPart.Replace("<%RecipientName%>", hdnEmpName.Value).Replace("<%AwardType%>", ddlAwardType.SelectedItem.ToString().Split(' ')[0]).Replace("<%AwardAmount%>", ddlGifts.SelectedItem.ToString().Split('.')[0]).Replace("<%GiverHODName%>", txtHODName.Text).Replace("<%reason%>", txtCertificateText.Text).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                    }
                }
                string strbodyP = BodyPart;
                ltPMessage.Text = Server.HtmlDecode(BodyPart);
                ptpgrey.Attributes.Add("style", "display:block");
                popupContainer.Attributes.Add("style", "display:block");
                Session["DIVCHECK"] = "true";
                scroll1 = "1";
                //  tblRecipientList.Attributes.Add("style", "display:none");
                //   myModal.Attributes["style"] = "display:none"; 



                //  var win = window.open("http://localhost:8000/player/index.html", 'newwin', 'height=200px,width=200px');
            }
            catch (Exception ex)
            {
                ThrowAlertMessage("There was some problem.");
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "btnPreview_Click" + ex.StackTrace);
            }
        }


        #endregion


        //protected void btnClose_Click(object sender, EventArgs e)
        //{
        //    try
        //    {              
        //       // myModal.Attributes["style"] = "display:none";
        //       // Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "scrollWin()", true);
        //        txtEmpReason.Text = "";
        //        txtMailID.Text = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        ThrowAlertMessage("There was some problem.");
        //        objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "btnClose_Click" + ex.StackTrace);
        //    }
        //}


        protected void btnGetDetails_Click(object sender, EventArgs e) //bbb
        {
            try
            {

                GetCCEmailID();
                searchempmodal.Attributes.Add("class", "modal fade search-emp-modal in show");
                searchempmodal.Attributes.Add("style", "display:block");
                searchempmodal.Attributes.Remove("aria-hidden");
                searchempmodal.Attributes.Add("aria-modal", "true");

                //ClientScript.RegisterStartupScript(this.GetType(), "ShowPopup", "$('#search-emp-modal').modal('show');", true);
                //  btnSubmit.Focus();
                //myModal.Attributes["style"] = "display:block";
            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "btnGetDetails_Click" + ex.StackTrace);
            }
        }

        protected void GetCCEmailID()
        {
            try
            {
                string SearchNum = txtName.Text.Trim().ToString();
                txtMailID.Enabled = true;
                BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();
                ObjBERecipientAward = new BE_RecipientAward();
                objBERecipientAwardColl = objBLRecipientAward.GetEmpDetails(SearchNum);



                if (objBERecipientAwardColl.Count == 0)
                {
                    ThrowAlertMessage("No data matched your search criteria.");
                }
                else
                {
                    grdEmpList.DataSource = objBERecipientAwardColl;
                    grdEmpList.DataBind();

                    //hdfield.Value = "1";
                    ////myModal.Attributes["style"] = "display:block";                
                }
            }
            catch (Exception ex)
            {

                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "btnGetDetails_Click" + ex.StackTrace);
            }
        }

        protected void grdRecipientList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRecipientList.PageIndex = e.NewPageIndex;
                GetRecipientList();
            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "grdRecipientList_PageIndexChanging" + ex.StackTrace);
            }
        }

        protected void ddlAwardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                //    PromoteScreen.Style["display"] = "block";
                ddlGifts.Visible = true;
                lblGiftHamper.Visible = false;
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                BE_AwardManagement objBEAwardMangement = new BE_AwardManagement();
                string awardtype = ddlAwardType.SelectedItem.Text;
                //  Levels = Convert.ToInt16(objCommon.GetUserDetails(Session["TokenNumber"].ToString()));
                Levels = Convert.ToInt32(Session["Levels"].ToString());
                objAwardManagementColl = Get_BE_AwardManagementColl_amt(Levels, awardtype);
                // Session["objAwardColl"] = objAwardManagementColl;
                if (ddlAwardType.SelectedItem.Text == "Select")
                {
                    ddlGifts.Attributes.Add("style", "display:none");
                    lblFromDate.Attributes.Add("style", "display:none");
                    Date.Attributes.Add("style", "display:none");
                    FromTime.Attributes.Add("style", "display:none");
                    spnTime.Attributes.Add("style", "display:none");
                    ////tblRecipientList.Attributes.Add("style", "display:none");
                    ddlHodReportEmployee.Items.Clear();
                    ddlHodReportEmployee.Items.Insert(0, new ListItem("Select", "Select"));

                }
                else
                {
                    ddlGifts.Attributes.Add("style", "display:block");

                    if (ddlAwardType.SelectedItem.Text == "Coffee With You")
                    {
                        lblFromDate.Text = "";
                        FromTime.Clear();
                        Date.MinDate = DateTime.Today.Date;
                        lblFromDate.Attributes.Add("style", "display:block");
                        spnTime.Attributes.Add("style", "display:none");
                        Date.Attributes.Add("style", "display:block");
                        FromTime.Attributes.Add("style", "display:none");
                        lblFromDate.Text = "";
                        Date.Clear();
                        FromTime.Clear();


                    }
                    else
                    {
                        lblFromDate.Attributes.Add("style", "display:none");
                        Date.Attributes.Add("style", "display:none");
                        spnTime.Attributes.Add("style", "display:none");
                        FromTime.Attributes.Add("style", "display:none");

                    }
                    ddlGifts.DataSource = objAwardManagementColl;
                    ddlGifts.DataTextField = "Amount";
                    ddlGifts.DataValueField = "ID_IsAuto";
                    ddlGifts.DataBind();
                    ////tblRecipientList.Attributes.Add("style", "display:none"); 
                    LoadHodReportEmployee();

                }

                if (ddlAwardType.SelectedItem.Text.ToString().ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                {
                    ddlGifts.Visible = false;
                    lblGiftHamper.Visible = true;
                }
            }
            catch (Exception ex)
            {
                // ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "ddlAwardType_SelectedIndexChanged" + ex.StackTrace);
            }
        }

        protected void grdEmpList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEmpList.PageIndex = e.NewPageIndex;
                //BindGrid();
                GetCCEmailID();

            }
            catch (Exception ex)
            {
                //ThrowAlertMessage(MM_Messages.DatabaseError);
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "grdEmpList_PageIndexChanging" + ex.StackTrace);
            }
        }

        protected void Date_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            if (Date.SelectedDate != null)
            {
                FromTime.Attributes.Add("style", "display:block");
                spnTime.Attributes.Add("style", "display:block");
                if (Date.SelectedDate.Value.Date == DateTime.Now.Date)
                {
                    FromTime.TimeView.StartTime = new TimeSpan(DateTime.Now.Hour + 1, 0, 0);
                }
                else
                {
                    FromTime.TimeView.StartTime = new TimeSpan();
                }
            }
        }

        public static bool IsValidEmail(string email)
        {
            if (email.Contains('~'))
                return false;
            if (email.Contains('#'))
                return false;
            if (email.Contains('$'))
                return false;
            if (email.Contains('%'))
                return false;
            if (email.Contains('^'))
                return false;
            if (email.Contains('&'))
                return false;
            if (email.Contains('*'))
                return false;
            if (email.Contains('/'))
                return false;
            if (email.Contains('('))
                return false;
            if (email.Contains(')'))
                return false;
            if (email.Contains('?'))
                return false;
            if (email.Contains('\\'))
                return false;
            if (email.Contains('|'))
                return false;
            if (email.Contains('+'))
                return false;
            if (email.Contains('+'))
                return false;
            if (email.Contains(','))
                return false;
            if (email.Contains(';'))
                return false;
            if (email.Contains(':'))
                return false;
            if (email.Contains('\''))
                return false;
            if (email.Contains('"'))
                return false;
            if (email.Contains('<'))
                return false;
            if (email.Contains('>'))
                return false;
            if (email.Contains('`'))
                return false;
            Regex r = new Regex(@"^[^@\s]+@[^@\s]+(\.[^@\s]+)+$");
            return r.IsMatch(email);
        }

        protected void ddlHodReportEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            string hodid = Convert.ToString(ddlHodReportEmployee.SelectedValue);

            if (ddlAwardType.SelectedItem.Text.ToString().ToUpper().Contains("COFFEE"))
            {
                // ObjBERecipientAward = new BE_RecipientAward();
                ObjBERecipientAward = objBLRecipientAward.GetRecipientDetails(hodid);
                int giver_level = 0;
                if (Session["Levels"] != null)
                {
                    giver_level = Convert.ToInt16(Session["Levels"]);
                }
                int recipient_level = Convert.ToInt16(ObjBERecipientAward.Level);
                if (!(giver_level < recipient_level && (giver_level + 1) != recipient_level))
                {
                    ThrowAlertMessage(MM_Messages.NotEligible);
                    ////tblRecipientList.Attributes["style"] = "display:none";
                    return;
                }

                if (Date.IsEmpty)
                {
                    ThrowAlertMessage(MM_Messages.EmptyDate);
                    return;
                }
                if (FromTime.IsEmpty)
                {
                    ThrowAlertMessage(MM_Messages.EmptyTime);
                    return;
                }
            }

            if (ddlAwardType.SelectedIndex <= 0)
            {

                //s btnGetEmpDetails1.Attributes.Add("style", "display:none");
                ThrowAlertMessage("Please select Award Type");
                ////tblRecipientList.Attributes["style"] = "display:none";
            }
            else if (Convert.ToDouble(lblBalance.Text) < Convert.ToDouble(ddlGifts.SelectedItem.ToString()))
            {
                ThrowAlertMessage("You do not have budget for the selected Award. Please select another award.");
                ////tblRecipientList.Attributes["style"] = "display:none";
            }

            //else if (ddlAwardType.SelectedIndex == 0)
            //{

            //}
            else
            {
                string TokennNumber = (Session["TokenNumber"]).ToString();
                // DataSet ds = new DataSet();
                //s btnGetEmpDetails1.Attributes.Add("style", "display:none");
                divEmpList.Attributes.Add("style", "display:none");
                DataSet ds = new DataSet();
                objDALCommon = new DAL_Common();
                //  BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", hodid);
                ds = objDALCommon.DAL_GetDataN("GetHodReportDirctEmpForBindingData", objsqlparam);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    hdnEmpName.Value = ds.Tables[0].Rows[0]["EmployeeName"].ToString();
                    // txtEmpDivision.Text = ds.Tables[0].Rows[0][2].ToString();
                    lblEmpDivision.Text = ds.Tables[0].Rows[0]["DivisionName_PA"].ToString();
                    hdnEmpDesignation.Value = ds.Tables[0].Rows[0]["Designation"].ToString();
                    lblEmpLevel.Text = ds.Tables[0].Rows[0]["EmployeeLevelName_ES"].ToString();
                    hdnLoactionCode.Value = ds.Tables[0].Rows[0]["LocationName_PSA"].ToString();

                    HdnDivisionCode.Value = ds.Tables[0].Rows[0]["Division_PA"].ToString();
                    hdnLoactionCode.Value = ds.Tables[0].Rows[0][8].ToString();
                    hdnEmailId.Value = ds.Tables[0].Rows[0]["EmailID"].ToString();
                    lblTokenno.Text = ds.Tables[0].Rows[0]["TokenNumber"].ToString();
                    lblfirstname.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                    lblLastName.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
                    Session["FirstName"] = lblfirstname.Text.ToString();
                    Session["LastName"] = lblLastName.Text;

                    hdnRecipientTokenNumber.Value = hodid;
                    ////tblRecipientList.Attributes["style"] = "display:table";
                    txtEmpReason.Text = "";
                    txtName.Text = "";

                    //Adding Code Show Ecard Receiver details by satyam
                    lblfirstnamePreview.Text = lblfirstname.Text;
                    lblLastNamePreview.Text = lblLastName.Text;
                    lblTokennoPreview.Text = lblTokenno.Text;
                    lblEmpDivisionPreview.Text = lblEmpDivision.Text;
                    lblEmpLevelPreview.Text = lblEmpLevel.Text;

                    //End

                    txtMailID.Text = objBLRecipientAward.GetEmailCCId(Session["TokenNumber"].ToString());
                    if (txtMailID.Text.Trim() != "")
                    {
                        txtMailID.Text = txtMailID.Text.Trim() + ";";
                    }
                    // txtMailID.Text = "";
                }
                else
                {
                    /////tblRecipientList.Attributes["style"] = "display:none";
                }

            }
            // }



        }



        protected void SelectNomineeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectNomineeName.SelectedItem.Value == "Direct Reportees")
            {
                ////DRDetails.Attributes["style"] = "display:table";
                ////btnGetEmpDetails1.Attributes["style"] = "display:none";
                ddlHodReportEmployee.SelectedIndex = 0;
                sel_drp.Attributes["style"] = "display:block";
                sel_nom.Attributes["style"] = "display:none";
            }
            else
            {
                ////DRDetails.Attributes["style"] = "display:none";
                ////btnGetEmpDetails1.Attributes["style"] = "display:table";
                txtTokenNumberTop.Text = "";
                sel_drp.Attributes["style"] = "display:none";
                sel_nom.Attributes["style"] = "display:block";
            }
            divEmpList.Attributes["style"] = "display:none";
            ////tblRecipientList.Attributes["style"] = "display:none";
            // ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + message + "');", true);

        }

        protected void closeemailpopup_Click(object sender, EventArgs e)
        {
            ////myModal.Attributes["style"] = "display:none";
            SuccessModal.Attributes["style"] = "display:none";
        }
        protected void btnSpotAward_Click(object sender, EventArgs e)
        {
            ddlAwardType.SelectedValue = "4|0";
            ddlAwardType_SelectedIndexChanged(sender, e);
            LoadPillars("spot");
            
            HidePopUp();
        }
        protected void btnExcelleratorAward_Click(object sender, EventArgs e)
        {
            //Session["AwardCollection"] = null;
            //LoadAwardsDropDown(Levels);
            //getUserDetails();

            ddlAwardType.SelectedValue = "8|1";
            ddlAwardType_SelectedIndexChanged(sender, e);
            LoadPillars("excellator");
            HidePopUp();
        }

        protected void txtEmpReason_TextChanged(object sender, EventArgs e)
        {
            //txtReasonPreview.Text = txtEmpReason.Text;
            txtReasonPreview.Text = txtCertificateText.Text;
        }

        protected void HidePopUp()
        {
            searchempmodal.Attributes.Add("class", "modal fade search-emp-modal");
            searchempmodal.Attributes.Add("style", "display:none");
            searchnomineemodal.Attributes.Add("class", "modal fade search-emp-modal");
            searchnomineemodal.Attributes.Add("style", "display:none");
        }

        protected void ddlPillars_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtCertificateText.Text = objBLRecipientAward.GetCertificateTextById(Convert.ToInt32(ddlPillars.SelectedValue));
                txtEmpReason.Text = txtCertificateText.Text;
                txtReasonPreview.Text = txtCertificateText.Text;
                HidePopUp();
            }
            catch (Exception ex)
            {
                objExceptionLogging.LogErrorMessage(ex, "RecipientAward.aspx" + "ddlPillars_SelectedIndexChanged" + ex.StackTrace);
            }

        }

        

    }
}

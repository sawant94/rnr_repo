﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AwardWinnersDetailsTransfer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Program program = new Program();
                program.ErrorLog("AwardWinnersDetailsTransfer Utility Starts", "");

                string SPdate = DateTime.Today.ToString("yyyy-MM-dd");
                program.TransferAwardWinners(SPdate);

                program.ErrorLog("AwardWinnersDetailsTransfer Utility Ends", "");
            }
            catch (Exception ex)
            {
                Program program = new Program();
                program.ErrorLog("Main Method :", ex.ToString());
            }
        }

        public void TransferAwardWinners(string date)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MM_Connection"].ConnectionString))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("USP_Get_AwardWinnerList", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@CurrentDate", date);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string Id;
                        string userID;
                        string description;
                        string name;
                        string institution;
                        string APITransferStatus;

                        while (reader.Read())
                        {
                            Id = reader["Id"].ToString();
                            userID = reader["TokenNumber"].ToString();
                            description = reader["AwardAmount"].ToString();
                            name = reader["AwardName"].ToString();
                            institution = reader["Description"].ToString();
                            APITransferStatus = reader["APITransferStatus"].ToString();

                            if (APITransferStatus == "")
                            {
                                Program program = new Program();
                                program.ErrorLog("", "");

                                program.ErrorLog("API Transfer for", "userID : " + userID + "  description : " + description + "  name : " + name + "  institution : " + institution);
                                //program.ErrorLog("", "");

                                string responce = TransferAwardDetails(userID, description, name, institution);
                                program.ErrorLog("API Transfer for responce", responce);

                                if (responce!="")
                                {
                                    UpdateAwardTransferStatus(Id, responce);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void UpdateAwardTransferStatus(string Id, string responce)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MM_Connection"].ConnectionString))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("USP_Update_APITransferStatus ", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Id", Id);
                    command.Parameters.AddWithValue("@APITransferStatus", responce);
                    int reader = command.ExecuteNonQuery();                   
                }
            }
        }

        private string TransferAwardDetails(string userId, string description, string name, string institution)
        {
            string responce = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)
  WebRequest.Create(Convert.ToString(ConfigurationManager.AppSettings["BaseURL"]));
                //request.KeepAlive = false;
                //request.ProtocolVersion = HttpVersion.Version10;

                //request.Timeout = 1000 * 40; //10 seconds - I've also tried leaving it as default
                //request.KeepAlive = false; ///

                //request.Proxy = new System.Net.WebProxy("10.2.152.4:80", true);

                request.Method = "POST";
                //request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes("mconnect@MMTest:Welcome1")));
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(ConfigurationManager.AppSettings["Username"] + ":" + ConfigurationManager.AppSettings["Password"])));

                string json = new JavaScriptSerializer().Serialize(new
                {
                    userId = userId,
                    description = description,
                    name = name,
                    institution = institution
                });

                // this is important - make sure you specify type this way
                request.ContentType = "application/json; charset=UTF-8";
                request.Accept = "application/json";

                try
                {
                    var streamWriter = new StreamWriter(request.GetRequestStream());
                    streamWriter.Write(json);
                }
                catch (WebException ex)
                {
                    ErrorLog("request.GetRequestStream() Method :", ex.ToString());
                }

                //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                //{
                //    streamWriter.Write(json);
                //}
                // grab te response and print it out to the console along with the status code

                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        responce = streamReader.ReadToEnd();
                    }
                }
                catch (WebException ex)
                {
                    ErrorLog("request.GetResponse() Method :", ex.ToString());
                }

            }
            catch (WebException ex)
            {
                string pageContent = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd().ToString();
                ErrorLog("TransferAwardDetails Method :", ex.ToString());
            }
            return responce;
        }

  //      private string TransferAwardDetails(string userId, string description, string name, string institution)
  //      {
  //          string responce = "";
  //          try
  //          {
  //              HttpWebRequest request = (HttpWebRequest)
  //WebRequest.Create(Convert.ToString(ConfigurationManager.AppSettings["BaseURL"]));
  //              //request.KeepAlive = false;
  //              //request.ProtocolVersion = HttpVersion.Version10;

  //              request.Timeout = 1000 * 40; //10 seconds - I've also tried leaving it as default
  //              request.KeepAlive = false; ///

  //              request.Proxy = new System.Net.WebProxy("10.2.152.4:80", true);

  //              request.Method = "POST";
  //              //request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes("mconnect@MMTest:Welcome1")));
  //              request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(ConfigurationManager.AppSettings["Username"] + ":" + ConfigurationManager.AppSettings["Password"])));
               
  //              string json = new JavaScriptSerializer().Serialize(new
  //              {
  //                  userId = userId,
  //                  description = description,
  //                  name = name,
  //                  institution = institution
  //              });
      
  //              // this is important - make sure you specify type this way
  //              request.ContentType = "application/json; charset=UTF-8";
  //              request.Accept = "application/json";

  //              Console.WriteLine("URL " + ConfigurationManager.AppSettings["BaseURL"]);
  //              Console.WriteLine("Username : Pass " + ConfigurationManager.AppSettings["Username"] + ":" + ConfigurationManager.AppSettings["Password"]);
  //              Console.WriteLine("request.Accept " + request.Accept);

               
  //               ErrorLog("TransferAwardDetails Method :", " Start ---------");
  //               Console.WriteLine("request.Connection " + request.Connection);
  //               Console.WriteLine("json" + json);
  //               Console.ReadLine();

  //               try
  //               {
  //                   var streamWriter1 = new StreamWriter(request.GetRequestStream());
  //               }
  //               catch (WebException ex)
  //               {
  //                   ErrorLog("request.GetRequestStream() Method :", ex.ToString());
  //               }

  //              using (var streamWriter = new StreamWriter(request.GetRequestStream()))
  //              {
  //                  Console.WriteLine("Start 1 ---------");
  //                  Console.ReadLine();

  //                  ErrorLog("TransferAwardDetails Method :", " Start 1 ---------");

  //                  streamWriter.Write(json);
  //              }
  //              ErrorLog("TransferAwardDetails Method :", "End ---------");
  //              // grab te response and print it out to the console along with the status code

  //              ErrorLog("TransferAwardDetails Method :", " Start 1---------");
  //              try
  //              {
  //                  HttpWebResponse response1 = (HttpWebResponse)request.GetResponse();
  //              }
  //              catch (WebException ex)
  //              {
  //                  ErrorLog("request.GetResponse() Method :", ex.ToString());
  //              }
             

  //              ErrorLog("TransferAwardDetails Method :", "End 1---------");
  //              HttpWebResponse response = (HttpWebResponse)request.GetResponse();

  //              ErrorLog("TransferAwardDetails Method :", " Start 2---------");
  //              using (var streamReader = new StreamReader(response.GetResponseStream()))
  //              {
  //                  responce = streamReader.ReadToEnd();
  //              }
  //              ErrorLog("TransferAwardDetails Method :", "End 2---------");
  //          }
  //          catch (WebException ex)
  //          {
  //              string pageContent = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd().ToString();
  //              ErrorLog("TransferAwardDetails Method :", ex.ToString());
  //          }
  //          return responce;
  //      }

        public void ErrorLog(string methodName, string message)
        {
            StreamWriter sw = null;

            try
            {
                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                //string sPathName = @"C:\\MM_Rewards\\ReddempedLog\\";

                //string sPathName = @"E:\\MM_Rewards_Folders\\MM_Rewards\\PushPointKwench\\";
                string sPathName = ConfigurationManager.AppSettings["LogFolderPath"];

                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string sErrorTime = sDay + "-" + sMonth + "-" + sYear;

                if (!Directory.Exists(sPathName))
                    Directory.CreateDirectory(sPathName);

                sw = new StreamWriter(sPathName + "ErrorLog_" + sErrorTime + ".txt", true);

                sw.WriteLine(sLogFormat + methodName + "," + message);
                sw.Flush();

            }
            catch (Exception ex)
            {
                ErrorLog("ErrorLog Method :", ex.ToString());
            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }
        }
    }
}

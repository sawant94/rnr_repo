﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using MM.DAL;
//using BL_Rewards.Admin;
using System.IO;
using System.Reflection;
//////using BL_Rewards.Admin;
namespace HODBudgetAllocation
{
    public sealed class ExcelUpload_HODBudget
    {
        DAL_Common objDALCommon = new DAL_Common();
        //After gettin the file Path,Process Excel and add value in Collection
        public void ProcessExcel(string strExcelFilePath, string dirpath, string FileName)
        {
            try
            {
                //pick up the file
                string errMsg = "";
                object _stat;
                int _status = 0;
                int errCounter = 0;
                int successCounter = 0;
                OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Excel 12.0 Xml;HDR=YES;", strExcelFilePath));
                con.Open();
                var sheets = from DataRow dr in con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows
                             select new { SheetName = dr["TABLE_NAME"].ToString() };
                con.Close();
                BE_UserDataUpLoad objBEUserDataUpLoad = new BE_UserDataUpLoad();
                BE_UserDataUpLoadColl objBEUserDataUpLoadColl = new BE_UserDataUpLoadColl();
                foreach (var s in sheets)
                {
                    if (s.SheetName != "_xlnm#_FilterDatabase")
                    {
                        OleDbDataAdapter da = new OleDbDataAdapter(string.Format("select * from [{0}]", s.SheetName), con);
                        DataTable dtMain = new DataTable();
                        dtMain.Columns.Add("TokenNumber", typeof(string));
                        dtMain.Columns.Add("StartDate", typeof(DateTime));
                        dtMain.Columns.Add("EndDate", typeof(DateTime));
                        dtMain.Columns.Add("Budget", typeof(decimal));
                        dtMain.Columns.Add("Status");
                        dtMain.Columns.Add("Remarks");
                        da.Fill(dtMain);
                        //DataTable dtLog = dtMain.Clone();
                        DataTable dtNew = dtMain.Clone();
                        foreach (DataRow _dr in dtMain.Rows)
                        {
                            if (CheckBlank(_dr))
                            {
                                DataRow drNew = dtNew.NewRow();
                                errMsg = ValidateRow(_dr);
                                if (errMsg == "")
                                {
                                    string date = "";
                                    for (int i = 0; i < dtMain.Columns.Count; i++)
                                    {
                                        if (i == 1 || i == 2)
                                        {
                                            date = _dr[i].ToString();
                                            //date = date == "" ? "" : date.ToDate_SwapDayMonth();
                                            drNew[i] = Convert.ToDateTime(date);// date;
                                        }
                                        else
                                        {
                                            drNew[i] = _dr[i].ToString() == "" ? DBNull.Value : _dr[i];
                                        }
                                    }
                                    drNew["Status"] = "SUCCESS";
                                    drNew["Remarks"] = "Record saved successfully!";
                                    successCounter += 1;
                                }
                                else
                                {
                                    //drNew["TokenNumber"] = _dr[0].ToString();
                                    drNew["Status"] = "FAIL";
                                    drNew["Remarks"] = errMsg;
                                    errCounter += 1;
                                    string TokenNumber = "";
                                    TokenNumber = _dr[0].ToString() == "" ? "Token Number Missing! " : _dr[0].ToString();
                                    LogData(dirpath, FileName, " \t" + TokenNumber + ":" + errMsg, TokenNumber + " : " + errMsg);
                                }
                                dtNew.Rows.Add(drNew);
                            }
                        }
                        SqlParameter[] objSqlParam = new SqlParameter[1];
                        objSqlParam[0] = new SqlParameter("@Table_Mas_UserBudget", dtNew);
                        _stat = objDALCommon.DAL_SaveData("usp_Insert_Mas_UserBudget_ExcelUpload1", objSqlParam, 'S');
                        _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                        if (_status == 0)
                        {
                            LogData(dirpath, FileName, "Number of rows updated successfully : " + successCounter, "Number of rows updated successfully : " + successCounter);
                            LogData(dirpath, FileName, "Number of rows updation failed : " + errCounter, "Number of rows updated successfully : " + errCounter);
                        }
                        else
                        {
                            LogData(dirpath, FileName, "There was some error in SQL : SQL Error Code is  " + _status, "There was some error in SQL : SQL Error Code is  " + _status);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }
        private Boolean CheckBlank(DataRow dr)
        {
            bool b = false;
            foreach (var item in dr.ItemArray)
            {
                if (item.ToString() != "")
                {
                    b = true;
                    break;
                }
            }
            return b;
        }
        private string ValidateRow(DataRow _dr)
        {
            string errMsg = "";
            try
            {
                if (_dr[0].ToString() == "")
                    errMsg = "Token Number is missing";
                else if (_dr[1].ToString() == "")
                    errMsg = "Start Date is missing!";
                else if (_dr[2].ToString() == "")
                    errMsg = "End Date is missing!";
                else if (_dr[3].ToString() == "")
                    errMsg = "Budget is missing";
                else
                {
                    DateTime stdate = DateTime.Now, enddate = DateTime.Now;
                    for (int i = 1; i < 3; i++)
                    {
                        string date = _dr[i].ToString().ToDate_SwapDayMonth();
                        try
                        {
                            if (i == 1)
                            {
                                stdate = Convert.ToDateTime(date.ToDate_SwapDayMonth());
                            }
                            else
                            {
                                enddate = Convert.ToDateTime(date.ToDate_SwapDayMonth());
                            }
                        }
                        catch (Exception ex)
                        {
                            errMsg = ex.ToString();
                            //errMsg = (i == 1 ? "Start" : "End") + " Date is invalid! Valid date format is dd/MM/yyyy";
                        }
                    }
                    if (errMsg == "")
                    {
                        if (Convert.ToInt32(stdate.ToString("yyyyMMdd")) > Convert.ToInt32(enddate.ToString("yyyyMMdd")))
                        {
                            errMsg = "End Date should be greater than Start Date!";
                        }
                    }
                    if (errMsg == "")
                    {
                        try
                        {
                            Convert.ToDecimal(_dr["Budget"]);
                        }
                        catch (Exception ex)
                        {
                            errMsg = "Budget Invalid!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            return errMsg;
        }
        #region ExceptionHandling
        private static void LogException(Exception ex)
        {
            string folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            // string p2 = Directory.GetCurrentDirectory();
            string dirpath = Directory.GetParent(Directory.GetParent(folder).ToString()).ToString();
            string FileName = DateTime.Now.Date.ToString("MMM")
            + "_" + DateTime.Now.Date.Year + ".txt";
            dirpath = dirpath + "\\Logs";
            if (!System.IO.Directory.Exists(dirpath))
            {
                System.IO.Directory.CreateDirectory(dirpath);
            }
            LogData(dirpath, FileName, "Exception : " + ex.Message, "Exception : " + ex.Message);
        }
        #endregion
        #region Log
        private static void LogData(string dirpath, string FileName, string strMessage, string strDisplayMessage = "")
        {
            StreamWriter logFile;
        A:
            try
            {
                string FilePath = dirpath + "\\" + FileName;
                if (!File.Exists(FilePath.ToString()))               //("Log.txt"))
                {
                    logFile = new StreamWriter(FilePath);
                }
                else
                {
                    logFile = File.AppendText(FilePath.ToString());
                }
                logFile.WriteLine(strMessage);
                if (strDisplayMessage != "")
                    Console.WriteLine(strDisplayMessage);
                logFile.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                goto A;
            }
        }
        #endregion
        #region ExceptionHandling
        private static void LogException(Exception ex, string dirpath)
        {
            string FileName = DateTime.Now.Date.ToString("MMM")
            + "_" + DateTime.Now.Date.Year + ".txt";
            if (!System.IO.Directory.Exists(dirpath))
            {
                System.IO.Directory.CreateDirectory(dirpath);
            }
            LogData(dirpath, FileName, "Exception : " + ex.Message, "Exception : " + ex.Message);
        }
        #endregion
    }
}

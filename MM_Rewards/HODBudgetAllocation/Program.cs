﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.IO.Compression;

namespace HODBudgetAllocation
{
    class Program
    {
        static void Main(string[] args)
        {
            //AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;
            //throw new Exception("Exception");

            string folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            // string p2 = Directory.GetCurrentDirectory();
            string RootDirpath = Directory.GetParent(Directory.GetParent(folder).ToString()).ToString();
            string FileName = DateTime.Now.Date.ToString("MMM")
                + "_" + DateTime.Now.Date.Year + ".txt";
            string dirpath = folder + "\\Logs";
            if (!System.IO.Directory.Exists(dirpath))
            {
                System.IO.Directory.CreateDirectory(dirpath);
            }
            try
            {
                #region Log Startup

                LogData(dirpath, FileName, "=============================================================================================================", "Execution Started");
                LogData(dirpath, FileName, DateTime.Now.Date.ToString("dd MMM yyyy"));

                //  LogData(dirpath, FileName, "<html><body><div style='border:1px solid black;height:100px;width:100px;color:blue;'>111111</div><div style='border:1px solid black;height:100px;width:100px;color:red;'>2222</div></body></html>");
                LogData(dirpath, FileName, "Start Time : " + DateTime.Now.ToString("hh:mm:ss tt"), "UserDetailsUploadUtility.exe execution started!");

                #endregion
                #region Cleaning

                if ((args.Length > 0 && args[0].ToString().ToLower() == "c") || ConfigurationSettings.AppSettings["AllowZip"].ToString() == "1")
                {
                    LogData(dirpath, FileName, "Cleaning process started : " + DateTime.Now.ToString("hh:mm:ss tt"), "Cleaning process started!");
                    HouseCleaning(dirpath);
                }
                #endregion

                string sourcepath = string.Empty;
                string targetPath = string.Empty;
                string fileName;
                ExcelUpload_HODBudget objExcelUpload = new ExcelUpload_HODBudget();
                sourcepath = ConfigurationSettings.AppSettings["SrcExcelFilePath"].ToString();
                targetPath = ConfigurationSettings.AppSettings["TargetExcelFilePath"].ToString();

                if (targetPath == "")
                {
                    targetPath = RootDirpath + "\\UploadedExcels";
                }

                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                if (System.IO.Directory.Exists(sourcepath))
                {
                    string[] files = System.IO.Directory.GetFiles(sourcepath);

                    // Copy the files and overwrite destination files if they already exist.
                    foreach (string s in files)
                    {
                        // Use static Path methods to extract only the file name from the path.
                        LogData(dirpath, FileName, "Uploading File " + s, "Uploading File " + s + " ...");
                        fileName = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + "_" + System.IO.Path.GetFileName(s);
                        string destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(s, destFile, true);
                        objExcelUpload.ProcessExcel(destFile, dirpath, FileName);
                    }
                }
                else
                {
                    LogData(dirpath, FileName, "Source Path ( " + sourcepath + " ) not found!", "Source Path ( " + sourcepath + " ) not found!");
                }

                #region Log End
                LogData(dirpath, FileName, "End Time : " + DateTime.Now.ToString("hh:mm:ss tt"), "HODBudgetAllocation.exe execution end!");
                LogData(dirpath, FileName, "");
                #endregion

                //objExcelUpload.ProcessExcel(_SrcExcelFilePath,_TargetFilePath);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }


        /* static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
         {            
             Console.WriteLine(e.ExceptionObject.ToString());
             Console.WriteLine("Press Enter to continue");
             Console.ReadLine();
             Environment.Exit(1);
         }    */

        #region Log

        private static void LogData(string dirpath, string FileName, string strMessage, string strDisplayMessage = "")
        {
            StreamWriter logFile;
        A:
            try
            {
                string FilePath = dirpath + "\\" + FileName;

                if (!File.Exists(FilePath.ToString()))
                {
                    logFile = new StreamWriter(FilePath);
                }
                else
                {
                    logFile = File.AppendText(FilePath.ToString());
                }

                logFile.WriteLine(strMessage);

                if (strDisplayMessage != "")
                    Console.WriteLine(strDisplayMessage);

                logFile.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                goto A;
            }
        }

        #endregion

        #region ExceptionHandling
        private static void LogException(Exception ex)
        {
            string folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            // string p2 = Directory.GetCurrentDirectory();
            string dirpath = Directory.GetParent(Directory.GetParent(folder).ToString()).ToString();
            string FileName = DateTime.Now.Date.ToString("MMM")
                + "_" + DateTime.Now.Date.Year + ".txt";

            dirpath = dirpath + "\\Logs";
            if (!System.IO.Directory.Exists(dirpath))
            {
                System.IO.Directory.CreateDirectory(dirpath);
            }
            LogData(dirpath, FileName, "Exception : " + ex.Message, "Exception : " + ex.Message);

        }
        #endregion

        #region HouseCleaning
        /// <summary>
        /// Zips the earlier .txt Files(Log Files in case) at the given path and dumps them into a folder
        /// </summary>
        /// <param name="dirpath"></param> Path to directory of files to compress and decompress.
        private static void HouseCleaning(string dirpath)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(dirpath);
                string Log_Zip = dirpath + "\\Log_Zip";

                bool compressedFileExists = false;

                // Compress the directory's .txt files.
                foreach (FileInfo fi in di.GetFiles("*.txt"))
                {
                    string FileName = DateTime.Now.Date.ToString("MMM")
                    + "_" + DateTime.Now.Date.Year + ".txt";

                    // exclude the current file
                    if (fi.Name != FileName)
                    {
                        //Compress file
                        Compress(fi);
                        compressedFileExists = true;
                        // delete the original file
                        File.Delete(fi.FullName);
                    }
                }

                //if zip files exists create a folder to dump them if it is already not existing
                if (!System.IO.Directory.Exists(Log_Zip) && compressedFileExists)
                {
                    System.IO.Directory.CreateDirectory(Log_Zip);
                }

                // Move all zip files(excluding the current file) into the Zip Folder
                foreach (FileInfo fi in di.GetFiles("*.gz"))
                {

                    if (!File.Exists(Log_Zip + "\\" + fi.Name))
                    {
                        File.Move(fi.FullName, Log_Zip + "\\" + fi.Name);
                    }
                    else  // if a file with the same name already exists in the zip folder change the name
                    {
                        int count = 2;
                        while (File.Exists(Log_Zip + "\\" + fi.Name.Replace(".gz", "__[Part" + count + "].gz")))
                        {
                            count++;
                        }
                        File.Move(fi.FullName, Log_Zip + "\\" + fi.Name.Replace(".gz", "__[Part" + count + "].gz"));
                    }
                }

                if (ConfigurationSettings.AppSettings["AllowReadLine"].ToString() == "1")
                    Console.ReadLine();

                //// Decompress all *.gz files in the directory.
                //foreach (FileInfo fi in di.GetFiles("*.gz"))
                //{
                //    Decompress(fi);
                //}
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }

        public static void Compress(FileInfo fi)
        {
            //FileStream outFile = new FileStream();
            // Get the stream of the source file.
            using (FileStream inFile = fi.OpenRead())
            {
                // Prevent compressing hidden and 
                // already compressed files.
                if ((File.GetAttributes(fi.FullName)
                    & FileAttributes.Hidden)
                    != FileAttributes.Hidden & fi.Extension != ".gz")
                {
                    // Create the compressed file.
                    using (FileStream outFile = File.Create(fi.FullName + ".gz"))
                    {
                        using (GZipStream Compress =
                            new GZipStream(outFile,
                            CompressionMode.Compress))
                        {
                            // Copy the source file into 
                            // the compression stream.
                            inFile.CopyTo(Compress);

                            Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                                fi.Name, fi.Length.ToString(), outFile.Length.ToString());
                        }
                    }
                }
            }
        }

        public static void Decompress(FileInfo fi)
        {
            // Get the stream of the source file.
            using (FileStream inFile = fi.OpenRead())
            {
                // Get original file extension, for example
                // "doc" from report.doc.gz.
                string curFile = fi.FullName;
                string origName = curFile.Remove(curFile.Length -
                        fi.Extension.Length);

                //Create the decompressed file.
                using (FileStream outFile = File.Create(origName))
                {
                    using (GZipStream Decompress = new GZipStream(inFile,
                            CompressionMode.Decompress))
                    {
                        // Copy the decompression stream 
                        // into the output file.
                        Decompress.CopyTo(outFile);

                        Console.WriteLine("Decompressed: {0}", fi.Name);

                    }
                }
            }
        }
        #endregion

    }
}

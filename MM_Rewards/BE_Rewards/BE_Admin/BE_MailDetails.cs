﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_MailDetails
    {
        #region fields

        private int activityCode;
        private string fromid;
        private string fromname;
        private string toid;
        private string ccid;
        private string createdby;
        private string createdDate;
        private string modifiedby;
        private DateTime? modifieddate;
        private string subject;
        private string message;
        private string divisioncode;
        private string firstName;
        private string lastName;
        private string emailID;
        private string awardType;
        private decimal awardAmount;
        private string startDate;
        private string endDate;
        private string rewardReason;
        private string giverName;
        private string giverEmailID;
        private string giverTokenNUmber;
        private string redeemedDate;
        private string closureDate;
        private string expiryDate;
        private string recipientTokenNumber;
        private string emailedDate;
        private string orgUnitName;
        private decimal budget;
        private decimal expenditure;
        private decimal closingAmount;
        private decimal closingAmountPercentile;
        private string awardName;
        private string allocationFrom;
        private string allocationTokenNumber;
        private int awardcount;
        private string recipientname;
        private string status;
        private string department;
        private string locationname;
        private string hODTokenNumber;
        private string hODName;
        //vandana
        private string divisionName;
        private string location_PSA;
        private string businessUnit;
        private string awardDate;
        private string awardedDate;
        private string salesDivision;
        private string addedBalance;
        private string deductedBalance;

        public string MileStoneDate { get; set; }
        public int MileStoneYears { get; set; }

        public string countHifi { get; set; }

        //By Neil
        public string RequestId
        {
            get;
            set;
        }

        public string HODName
        {
            get { return hODName; }
            set { hODName = value; }
        }

        public string HODTokenNumber
        {
            get { return hODTokenNumber; }
            set { hODTokenNumber = value; }
        }
        #endregion

        #region Properties

        public int ActivityCode
        {
            get { return activityCode; }
            set { activityCode = value; }
        }

        public string Fromid
        {
            get { return fromid; }
            set { fromid = value; }
        }

        public string Fromname
        {
            get { return fromname; }
            set { fromname = value; }
        }

        public string Toid
        {
            get { return toid; }
            set { toid = value; }
        }

        public string Ccid
        {
            get { return ccid; }
            set { ccid = value; }
        }

        public string Createdby
        {
            get { return createdby; }
            set { createdby = value; }
        }

        public string CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public string Modifiedby
        {
            get { return modifiedby; }
            set { modifiedby = value; }
        }

        public DateTime? Modifieddate
        {
            get { return modifieddate; }
            set { modifieddate = value; }
        }

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public string Divisioncode
        {
            get { return divisioncode; }
            set { divisioncode = value; }
        }

        public string ClosureDate
        {
            get { return closureDate; }
            set { closureDate = value; }
        }

        public string AllocationTokenNumber
        {
            get { return allocationTokenNumber; }
            set { allocationTokenNumber = value; }
        }

        public string LocationName
        {
            get { return locationname; }
            set { locationname = value; }
        }

        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        public string AwardStatus
        {
            get { return status; }
            set { status = value; }
        }

        public string RecipientName
        {
            get { return recipientname; }
            set { recipientname = value; }
        }

        public int AwardCount
        {
            get { return awardcount; }
            set { awardcount = value; }
        }

        public string AllocationFrom
        {
            get { return allocationFrom; }
            set { allocationFrom = value; }
        }

        public string AwardName
        {
            get { return awardName; }
            set { awardName = value; }
        }

        public decimal ClosingAmountPercentile
        {
            get { return closingAmountPercentile; }
            set { closingAmountPercentile = value; }
        }

        public decimal ClosingAmount
        {
            get { return closingAmount; }
            set { closingAmount = value; }
        }

        public decimal Expenditure
        {
            get { return expenditure; }
            set { expenditure = value; }
        }

        public decimal PeriodicExpenditure
        {
            get;
            set;
        }

        public decimal Budget
        {
            get { return budget; }
            set { budget = value; }
        }

        public string OrgUnitName
        {
            get { return orgUnitName; }
            set { orgUnitName = value; }
        }

        public string EmailedDate
        {
            get { return emailedDate; }
            set { emailedDate = value; }
        }

        public string RecipientTokenNumber
        {
            get { return recipientTokenNumber; }
            set { recipientTokenNumber = value; }
        }

        public string ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public string RedeemedDate
        {
            get { return redeemedDate; }
            set { redeemedDate = value; }
        }

        private string redeemType;

        public string RedeemType
        {
            get { return redeemType; }
            set { redeemType = value; }
        }

        private string redeemAmount;

        public string RedeemAmount
        {
            get { return redeemAmount; }
            set { redeemAmount = value; }
        }

        public string AwardedDate
        {
            get { return awardedDate; }
            set { awardedDate = value; }
        }


        public string AwardDate
        {
            get { return awardDate; }
            set { awardDate = value; }
        }

        public string GiverName
        {
            get { return giverName; }
            set { giverName = value; }
        }

        public string GiverTokenNUmber
        {
            get { return giverTokenNUmber; }
            set { giverTokenNUmber = value; }
        }

        public string GiverEmailID
        {
            get { return giverEmailID; }
            set { giverEmailID = value; }
        }

        public string RewardReason
        {
            get { return rewardReason; }
            set { rewardReason = value; }
        }

        public string StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public string EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public string AwardType
        {
            get { return awardType; }
            set { awardType = value; }
        }

        public decimal AwardAmount
        {
            get { return awardAmount; }
            set { awardAmount = value; }
        }

        public string EmailID
        {
            get { return emailID; }
            set { emailID = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LongServiceDate { get; set; }

        public string ProcessName { get; set; }

        public string RecipientLocation { get; set; }


        public string RecipientDivision { get; set; }

        public string RecipientDepartment { get; set; }

        public string DivisionName_PA
        {
            get { return divisionName; }
            set { divisionName = value; }
        }


        public string SalesDivision
        {
            get { return salesDivision; }
            set { salesDivision = value; }
        }


        public string Location_PSA
        {
            get { return location_PSA; }
            set { location_PSA = value; }
        }
        public string BusinessUnit
        {
            get { return businessUnit; }
            set { businessUnit = value; }
        }

        public string AddedBalance
        {
            get { return addedBalance; }
            set { addedBalance = value; }
        }
        public string DeductedBalance
        {
            get { return deductedBalance; }
            set { deductedBalance = value; }
        }
        #endregion
    }
}

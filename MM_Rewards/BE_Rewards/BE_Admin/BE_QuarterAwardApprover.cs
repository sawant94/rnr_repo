﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_QuarterAwardApprover
    {
        private string subAFLCTokenNumber;
        private string subAFLCName;
        private string aFLCTokenNumber;
        private string aFLCName;
        private string role;
        private string createdBy;
        private int isActive;

        public string SubAFLCTokenNumber
        {
            get { return subAFLCTokenNumber; }
            set { subAFLCTokenNumber = value; }
        }
        public string SubAFLCName
        {
            get { return subAFLCName; }
            set { subAFLCName = value; }
        }
        public string AFLCTokenNumber
        {
            get { return aFLCTokenNumber; }
            set { aFLCTokenNumber = value; }
        }
        public string AFLCName
        {
            get { return aFLCName; }
            set { aFLCName = value; }
        }
        public string Role
        {
            get { return role; }
            set { role = value; }

        }
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_AwardManagement
    {

        #region fields

        private int id;
        private string awardType;
        private string awardTypeText;
        private string name;
        private decimal amount;
        private string createdBy;
        private DateTime createdDate;
        private string modifiedBy;
        private bool active;
        private bool typeIsActive;
        private bool autoredeem;
        private string employeeLevelsName;
        private string employeeLevelsCode;
        private string stringLevels;
        private bool isSystem;
        private string companyCode;
        private string businessUnit;
        private string tokenName;
        private string employeeLevelName_ES;
        private string employeeDivisionName;

        private List<int> Level = new List<int>();

        private List<int> recipientLevel = new List<int>();

        public List<int> RecipientLevel
        {
            get { return recipientLevel; }
            set { recipientLevel = value; }
        }
        private List<string> employeeLevelCodeColl = new List<string>();

        private int awardId;

        #endregion

        #region properties

        public List<string> EmployeeLevelCodeColl
        {
            get { return employeeLevelCodeColl; }
            set { employeeLevelCodeColl = value; }
        }
        public bool IsSystem
        {
            get { return isSystem; }
            set { isSystem = value; }
        }
        public string AwardTypeText
        {
            get { return awardTypeText; }
            set { awardTypeText = value; }
        }
        public bool TypeIsActive
        {
            get { return typeIsActive; }
            set { typeIsActive = value; }
        }

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public DateTime? ModifiedDate { get; set; }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        public string AwardType
        {
            get { return awardType; }
            set { awardType = value; }
        }

        public int AwardId
        {
            get { return awardId; }
            set { awardId = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }


        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }


        public string ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }

        public List<int> GiverLevel
        {
            get { return Level; }
            set { Level = value; }
        }

        public string StringLevels
        {
            get { return stringLevels; }
            set { stringLevels = value; }
        }

        public string EmployeeLevelsCode
        {
            get { return employeeLevelsCode; }
            set { employeeLevelsCode = value; }
        }

        public string EmployeeLevelsName
        {
            get { return employeeLevelsName; }
            set { employeeLevelsName = value; }
        }
        public bool Autoredeem
        {
            get { return autoredeem; }
            set { autoredeem = value; }
        }
        public string CompanyCode
        {
            get { return companyCode; }
            set { companyCode = value; }
        }
        public string BusinessUnit
        {
            get { return businessUnit; }
            set { businessUnit = value; }
        }
        public string TokenName
        {
            get { return tokenName; }
            set { tokenName = value; }
        }
        public string EmployeeLevelName_ES
        {
            get { return employeeLevelName_ES; }
            set { employeeLevelName_ES = value; }
        }
        public string EmployeeDivisionName
        {
            get { return employeeDivisionName; }
            set { employeeDivisionName = value; }
        }
        #endregion

        public string ID_IsAuto { get; set; }


    }
}

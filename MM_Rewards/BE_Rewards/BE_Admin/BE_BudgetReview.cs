﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_BudgetReview
    {
        public string TokenNumber {get ; set ;}
        public string Name{get ; set ;}
        public int EX500BHP{get ; set ;}
        public int EX750BHP{get ; set ;}
        public int EX1000BHP{get ; set ;}
        public int SpotAward{get ; set ;}
        public int TotalBudget{get ; set ;}
        public int FinalBudget{get ; set ;}

        //private int eX500BHP;
        //private int eX750BHP;
        //private int eX1000BHP;
        //private int spotAward;
        //private int totalBudget;
        //private int finalBudget;
        //private string tokenNumber;
        //private string name;

        //public string TokenNumber
        //{
        //    get { return tokenNumber; }
        //    set { tokenNumber = value; }
        //}
        //public string Name
        //{
        //    get { return name; }
        //    set { name = value; }
        //}
        //public int EX500BHP
        //{
        //    get { return eX500BHP; }
        //    set { eX500BHP = value; }
        //}
        //public int EX750BHP
        //{
        //    get { return eX750BHP; }
        //    set { eX750BHP = value; }
        //}
        //public int EX1000BHP
        //{
        //    get { return eX1000BHP; }
        //    set { eX1000BHP = value; }
        //}
        //public int SpotAward
        //{
        //    get { return spotAward; }
        //    set { spotAward = value; }
        //}
        //public int TotalBudget
        //{
        //    get { return totalBudget; }
        //    set { totalBudget = value; }
        //}
        //public int FinalBudget
        //{
        //    get { return finalBudget; }
        //    set { finalBudget = value; }

        //}

    }
}

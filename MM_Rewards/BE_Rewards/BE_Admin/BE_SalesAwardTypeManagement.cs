﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_SalesAwardTypeManagement
    {
        #region fields

        private int id;
        private string awardname;
        private string awardtype;
        private int division;
        private decimal amount;
        private string createdby;
        private string modifiedby;
        private bool isactive;
        public string TokenNumber;
        private string location;
        private string department;
        private string designation;
        private string reason;
        private string redeemType;
        private DateTime redeemDate;
        private DateTime createdDate;
        private int divisionId;
        public string divisionName;
        private int awardTypeId;
        public string awardTypeName;

        #endregion

        #region properties

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string AwardName
        {
            get { return awardname; }
            set { awardname = value; }
        }
        public string AwardType
        {
            get { return awardtype; }
            set { awardtype = value; }
        }
        public int Division
        {
            get { return division; }
            set { division = value; }
        }
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public string CreatedBy
        {
            get { return createdby; }
            set { createdby = value; }
        }
        public string ModifiedBy
        {
            get { return modifiedby; }
            set { modifiedby = value; }
        }
        public bool IsActive
        {
            get { return isactive; }
            set { isactive = value; }
        }

        public int DivisionId
        {
            get { return divisionId; }
            set { divisionId = value; }
        }
        public string DivisionName
        {
            get { return divisionName; }
            set { divisionName = value; }
        }

        public int AwardTypeId
        {
            get { return awardTypeId; }
            set { awardTypeId = value; }
        }
        public string AwardTypeName
        {
            get { return awardTypeName; }
            set { awardTypeName = value; }
        }
        public string Location
        {
            get { return location; }
            set { location = value; }
        }
        public string Department
        {
            get { return department; }
            set { department = value; }
        }
        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }
        public string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public string RedeemType
        {
            get { return redeemType; }
            set { redeemType = value; }
        }

        public DateTime RedeemDate
        {
            get { return redeemDate; }
            set { redeemDate = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }
        #endregion
    }
}

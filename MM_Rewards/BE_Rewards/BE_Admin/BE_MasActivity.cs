﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
 public sealed class BE_MasActivity
    {   
     #region fields;
        private int id;
        private string activity;
        private string divisionname;
        private string divisioncode;
     #endregion;

    #region Properties;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Activity
        {
            get { return activity; }
            set { activity = value; }
        }
        public string Divisionname
        {
            get { return divisionname; }
            set { divisionname = value; }
        }
        public string Divisioncode
        {
            get { return divisioncode; }
            set { divisioncode = value; }
        }
        #endregion;

    }
}

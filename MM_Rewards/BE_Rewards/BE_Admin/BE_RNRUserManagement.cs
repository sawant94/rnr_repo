﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
  public sealed class BE_RNRUserManagement
  {
      #region fields
      private List<string> division = new List<string>();
      private List<string> location = new List<string>();
      private List<string> orgunit = new List<string>();
      private List<string> bunit = new List<string>();
    #endregion
     
      #region Properties

        public string TokenNumber { get; set; }
        public string EmployeeName { get; set; }
        public string DivisionName { get; set; }
        public string LocationName { get; set; }
        public string BusinessUnit { get; set; }
        public string OrgUnitName { get; set; }
        public bool IsSelected { get; set; }
        public string CreatedBy { get; set; }
        public string DivisionCode { get; set; }
        public string LocationCode { get; set; }
        public String OrgUnitCode { get; set; }
        public bool IsEnable { get; set; }
        public List<string> Division
        {
            get { return division; }
            set { division = value; }
        }   
        public List<string> Location
        {
            get { return location; }
            set { location = value; }
        }
        public List<string> Orgunit
        {
            get { return orgunit; }
            set { orgunit = value; }
        }

        public List<string> BUnit
        {
            get { return bunit; }
            set { bunit = value; }
        }   
      #endregion
  }
}

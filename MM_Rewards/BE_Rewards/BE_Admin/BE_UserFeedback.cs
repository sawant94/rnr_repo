﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
   public sealed class BE_UserFeedback
   {
       #region fields;
       private string tokennumber;
        private char feedabacktype;
        private string feedbackdetails;
        private string createdby;       
       #endregion;

        //for reports
       #region Report  
        private string employeename;
        private string feedtype;
        private string division;
        private string location;
        private string createdDate;

        public string Employeename
        {
            get { return employeename; }
            set { employeename = value; }
        }
        public string Feedtype
        {
            get { return feedtype; }
            set { feedtype = value; }
        }
        public string Division
        {
            get { return division; }
            set { division = value; }
        }
        public string Location
        {
            get { return location; }
            set { location = value; }
        }
        public string CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }
           #endregion
      
       #region Properties;
        public string Createdby
        {
            get { return createdby; }
            set { createdby = value; }
        }
        public string Feedbackdetails
        {
            get { return feedbackdetails; }
            set { feedbackdetails = value; }
        }
        public char Feedabacktype
        {
            get { return feedabacktype; }
            set { feedabacktype = value; }
        }
        public string Tokennumber
        {
            get { return tokennumber; }
            set { tokennumber = value; }
        }
     
     
        #endregion;

   }
}

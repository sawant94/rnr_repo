﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_UserDataUpLoad
    {
        public string EmployeeNumber { get; set; }
        public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeGroup { get; set; }
        public string EG { get; set; }
        public string EmployeeSubgroup { get; set; }
        public int Counter { get; set; }
        public string ESG { get; set; }
        public string PersonnelArea { get; set; }
        public string PA { get; set; }
        public string PersonnelSubarea { get; set; }
        public string PSA { get; set; }
        public string EmailID { get; set; }
        public string EmployeeOrgUnitNumber { get; set; }
        public string EmployeeOrgUnitName { get; set; }
        public string BusinessUnit { get; set; }
        public string BUNo { get; set; }
        public string EmployeeCostCenterNumber { get; set; }
        public string EmployeeCostCenterName { get; set; }
        public string EmployeePositionNumber { get; set; }
        public string EmployeePositionName { get; set; }
        public string CompanyCode { get; set; }
        public string Gender { get; set; }
        public string DateofJoining { get; set; }
        public string DateofBirth { get; set; }
        public string PayrollAreaText { get; set; }
        public string PayrollAreaCode { get; set; }
        public string MangerNumber { get; set; }
        public string MangerName { get; set; }
        public string ManagerPositionNumber { get; set; }
        public string ManagerPositionName { get; set; }
        public string EmploymentStatus { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
    }
}

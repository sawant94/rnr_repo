﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_HolidayManagement
    {   
        #region Fields.
        private int id;
        private int noOfLocations;
        private string locationcode;        
        private string calendarname;
        private int? calendarID;

        public int? CalendarID
        {
            get { return calendarID; }
            set { calendarID = value; }
        }
        private string locationname;
        private string holidayname;
        private string holidaydate;
        private string createdby;
        private int sattype;
        private string modifiedby;
        private List<string> locationcodeColl = new List<string>();
        private List<BE_Holidays> holidayColl = new List<BE_Holidays>();
        private List<string> locationColl = new List<string>();

        #endregion

        #region Properties.


        public List<BE_Holidays> HolidayColl
        {
            get { return holidayColl; }
            set { holidayColl = value; }
        }

        public List<string> LocationColl
        {
            get { return locationColl; }
            set { locationColl = value; }
        }


        public string Locationcode
        {
            get { return locationcode; }
            set { locationcode = value; }
        }
        

        public List<string> LocationCodeColl
        {
            get { return locationcodeColl; }
            set { locationcodeColl = value; }
        }

       

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public int NoOfLocations
        {
            get { return noOfLocations; }
            set { noOfLocations = value; }
        }   
        public string Calendarname
        {
            get { return calendarname; }
            set { calendarname = value; }
        }
        public string Holidayname
        {
            get { return holidayname; }
            set { holidayname = value; }
        }
        public string Holidaydate
        {
            get { return holidaydate; }
            set { holidaydate = value; }
        }       
        public string Createdby
        {
            get { return createdby; }
            set { createdby = value; }
        }      
        public int Sattype
        {
            get { return sattype; }
            set { sattype = value; }
        }       
        public string Modifiedby
        {
            get { return modifiedby; }
            set { modifiedby = value; }
        }
        public string Locationname
        {
            get { return locationname; }
            set { locationname = value; }
        }
       
        #endregion
    }
}

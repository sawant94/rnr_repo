﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_AwardTypeMangement
    {  
        #region Fields.
        private int id;
        private string awardname;
        private string createdby;
        private string modifiedBy;
        private bool isactive;

      
        #endregion

        #region Properties

        public string Template { get; set; }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string AwardName
        {
            get { return awardname; }
            set { awardname = value; }
        }      
        public string Createdby
        {
            get { return createdby; }
            set { createdby = value; }
        }
        public string ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        public bool Isactive
        {
            get { return isactive; }
            set { isactive = value; }
        }
        #endregion
        
    }
}

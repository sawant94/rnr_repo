﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_Holidays
    {
        public string HolidayName { get; set; }
        public int CalendarID { get; set; }
        public DateTime HolidayDate { get; set; }
        public int ID { get; set; }
    }
}

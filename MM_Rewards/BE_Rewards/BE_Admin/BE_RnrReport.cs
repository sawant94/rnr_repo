﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_RnrReport
    {
        #region Fields
        private string TokenNumber;
        private string Number_of_friend_requests_sent;
        private string Number_of_friend_requests_received;
        private string Number_of_friends;
        private string No_of_Hi5_given;
        private string No_of_Hi5_received;
        private string No_Of_Likes_Made;
        private string No_Of_Comment_Made;
        #endregion

        public string TokenNumber_Property
        {
            get { return TokenNumber; }
            set { TokenNumber = value; }
        }
        public string Number_of_friend_requests_sent_Property
        {
            get { return Number_of_friend_requests_sent; }
            set { Number_of_friend_requests_sent = value; }
        }
        public string Number_of_friend_requests_received_property
        {
            get { return Number_of_friend_requests_received; }
            set { Number_of_friend_requests_received = value; }
        }
        public string Number_of_friends_Property
        {
            get { return Number_of_friends; }
            set { Number_of_friends = value; }
        }
        public string No_of_Hi5_given_property
        {
            get { return No_of_Hi5_given; }
            set { No_of_Hi5_given = value; }
        }
        public string No_of_Hi5_received_property
        {
            get { return No_of_Hi5_received; }
            set { No_of_Hi5_received = value; }
        }
        public string No_Of_Likes_Made_property
        {
            get { return No_Of_Likes_Made; }
            set { No_Of_Likes_Made = value; }
        }
        public string No_Of_Comment_Made_Property
        {
            get { return No_Of_Comment_Made; }
            set { No_Of_Comment_Made = value; }
        }

    }
}

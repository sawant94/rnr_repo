﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_QuarterAwardBudget
    {
        private int id;
        private string tokenNumber;
        private DateTime startDate;
        private DateTime endDate;
        private int allocatedBudget;
        private int utilizedBudget;
        private int remaningBudget;
        private string budgetType;
        private int quarter;
        private string createdBy;
        private string updatedBy;
        private int isActive;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string TokenNumber
        {
            get { return tokenNumber; }
            set { tokenNumber = value; }
        }
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        public int AllocatedBudget
        {
            get { return allocatedBudget; }
            set { allocatedBudget = value; }
        }
        public int UtilizedBudget
        {
            get { return utilizedBudget; }
            set { utilizedBudget = value; }
        }
        public int RemaningBudget
        {
            get { return remaningBudget; }
            set { remaningBudget = value; }
        }
        public string BudgetType
        {
            get { return budgetType; }
            set { budgetType = value; }
        }
        public int Quarter
        {
            get { return quarter; }
            set { quarter = value; }
        }
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public string UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }
        public int IsActive
        {
            get { return isActive; }
            set { isActive = value; }

        }
    }
}

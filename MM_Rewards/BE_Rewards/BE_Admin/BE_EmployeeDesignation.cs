﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_EmployeeDesignation
    {
        private int id;
        private string tokenNumber;       
        private string empName;
        private string processName;
        private string divisionName;
        private string locationName;
        private string modifiedby;
        private string createdby;
        private string rolename;
        private int roleid;

        public int Roleid
        {
            get { return roleid; }
            set { roleid = value; }
        }
        public string Rolename
        {
            get { return rolename; }
            set { rolename = value; }
        }
      
        private List<int> roles = new List<int>();
        
        public string Modifiedby
        {
            get { return createdby; }
            set { createdby = value; }
        }
        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }
        
        public string DivisionName
        {
            get { return divisionName; }
            set { divisionName = value; }
        }

        public string LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public string EmpName
        {
            get { return empName; }
            set { empName = value; }
        }

       
        public string TokenNumber
        {
            get { return tokenNumber; }
            set { tokenNumber = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public List<int> Roles
        {
            get { return roles; }
            set { roles = value; }
        }
        public string Createdby
        {
            get { return createdby; }
            set { createdby = value; }
        }
    }
}

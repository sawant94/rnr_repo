﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_Testimonial
    {   
        public int ID { get; set; }
        public string  TokenNumber { get; set; }
        public string TestimonoialText { get; set; }
        public string ImageName { get; set; }
        public string ApproverRemarks { get; set; }
        public bool Status { get; set; }
        public string ApproveBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string EmployeeName { get; set; }
        public string ShortTestimonial { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_BudgetTrackingUtil
    {
        #region Fields
        private int id;
        public string TokenNumber;
        private string FromDate;
        private string Todate;
        private decimal TotalAmount;
        private string awardName;
        private decimal budgetforeachaward;
        private string percentage;
        private DateTime createdOn;
        private DateTime modifiedOn;
        #endregion

        #region properties

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public decimal totalAmount
        {
            get { return TotalAmount; }
            set { TotalAmount = value; }
        }

        public decimal Budgetforeachaward
        {
            get { return budgetforeachaward; }
            set { budgetforeachaward = value; }
        }

        public string FromYear
        {
            get { return FromDate; }
            set { FromDate = value; }
        }
        public string ToYear
        {
            get { return Todate; }
            set { Todate = value; }
        }
        public string AwardName
        {
            get { return awardName; }
            set { awardName = value; }
        }
        public string Percentage
        {
            get { return percentage; }
            set { percentage = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }


        #endregion
    }
}

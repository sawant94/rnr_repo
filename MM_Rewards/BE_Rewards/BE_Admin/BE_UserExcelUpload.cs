﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_UserExcelUpload
    {
        #region fields
        private List<string> division = new List<string>();
        private List<string> location = new List<string>();
        private List<string> orgunit = new List<string>();
        #endregion

        #region Properties

        public string TokenNumber { get; set; }
        public string EmailID { get; set; }
        public string EmployeeName { get; set; }
        public string DivisionName { get; set; }
        public string LocationName { get; set; }
        public string OrgUnitName { get; set; }
        public bool IsSelected { get; set; }
        public string CreatedBy { get; set; }
        public string DivisionCode { get; set; }
        public string LocationCode { get; set; }
        public String OrgUnitCode { get; set; }
        public bool IsEnable { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmpGradeCode_E { get; set; }
        public string EmpGradeName_E { get; set; }
        public string EmployeeLevelCode_ES { get; set; }
        public string EmployeeLevelName_ES { get; set; }
        public string Levels { get; set; }
        public string Division_PA { get; set; }
        public string DivisionName_PA { get; set; }
        public string Location_PSA { get; set; }
        public string LocationName_PSA { get; set; }

        public string EmployeeNumber { get; set; }        
        public string EmployeeGroup { get; set; }
        public string EG { get; set; }
        public string EmployeeSubgroup { get; set; }
        public int Counter { get; set; }
        public string ESG { get; set; }
        public string PersonnelArea { get; set; }
        public string PA { get; set; }
        public string PersonnelSubarea { get; set; }
        public string PSA { get; set; }      
        public string EmployeeOrgUnitNumber { get; set; }
        public string EmployeeOrgUnitName { get; set; }
        public string BusinessUnit { get; set; }
        public string BUNo { get; set; }
        public string EmployeeCostCenterNumber { get; set; }
        public string EmployeeCostCenterName { get; set; }
        public string EmployeePositionNumber { get; set; }
        public string EmployeePositionName { get; set; }
        public string CompanyCode { get; set; }
        public string Gender { get; set; }
        public string DateofJoining { get; set; }
        public string DateofBirth { get; set; }
        public string PayrollAreaText { get; set; }
        public string PayrollAreaCode { get; set; }
        public string MangerNumber { get; set; }
        public string MangerName { get; set; }
        public string ManagerPositionNumber { get; set; }
        public string ManagerPositionName { get; set; }
        public string EmploymentStatus { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal Budget { get; set; }

        public List<string> Division
        {
            get { return division; }
            set { division = value; }
        }
        public List<string> Location
        {
            get { return location; }
            set { location = value; }
        }
        public List<string> Orgunit
        {
            get { return orgunit; }
            set { orgunit = value; }
        }     
        #endregion
    }
}

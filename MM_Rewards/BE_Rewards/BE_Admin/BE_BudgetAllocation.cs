﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
  public sealed  class BE_BudgetAllocation
    {
        public string TokenNumber { get; set; }
        public string Name { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public int? Levels { get; set; }
        public string Designation { get; set; }

        public Decimal Budget { get; set; }
        public Decimal? Balance { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AllocationFrom { get; set; }
        public string AllocationType { get; set; }
        public Boolean IsCEO { get; set; }
        public string SearchBy { get; set; }
        public string EMailID { get; set; }
        public int CheckButtonClick { get; set; }
    }
}

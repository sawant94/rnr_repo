﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_LongServiceUpload : List<BE_LongServiceUpload>
    {
        
        public string RecepientTokenNumber{get; set;}
        public string Division { get; set; }
        public string Location { get; set; }       
        public string AwardTypeName { get; set; }
        public int AwardAmount { get; set; }
        public string LongServiceReason { get; set; }
      
    }
}

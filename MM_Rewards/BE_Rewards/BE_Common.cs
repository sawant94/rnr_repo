﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards
{
   public sealed class BE_Common
    {
        private string tokenNumber;
        private string userName;
        private string division;
        private string location;
        private string levels;
        private string expenses;
        private string balance;
        private string emailID;
        private string processName;
        private string pageName;
        private string menuName;
        private string parentMenuName;
        private int roleID;

        public string MenuName
        {
            get { return menuName; }
            set { menuName = value; }
        }

        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }

        public string ParentMenuName
        {
            get { return parentMenuName; }
            set { parentMenuName = value; }
        }

        public int RoleID
        {
            get { return roleID; }
            set { roleID = value; }
        }

        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }

        public string EmailID
        {
            get { return emailID; }
            set { emailID = value; }
        }

        public string Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        public string Expenses
        {
            get { return expenses; }
            set { expenses = value; }
        }

        public string Levels
        {
            get { return levels; }
            set { levels = value; }
        }

        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        public string Division
        {
            get { return division; }
            set { division = value; }
        }   

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string TokenNumber
        {
            get { return tokenNumber; }
            set { tokenNumber = value; }
        }
    }
}

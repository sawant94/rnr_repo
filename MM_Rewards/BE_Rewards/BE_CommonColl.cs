﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards
{
   public sealed class BE_CommonColl
    {
       public List<BE_Common> CommonCollection = new List<BE_Common>();

       public void Add(BE_Common objBECommon)
       {
           CommonCollection.Add(objBECommon);
       }
    }
}

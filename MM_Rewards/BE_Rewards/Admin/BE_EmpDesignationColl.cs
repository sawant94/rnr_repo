﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.Admin
{
   public sealed class BE_EmpDesignationColl
    {
       public List<BE_EmployeeDesignation> EmpDesignColl = new List<BE_EmployeeDesignation>();

       public void Add(BE_EmployeeDesignation objBEEmpDesign)
       {
           EmpDesignColl.Add(objBEEmpDesign);
       }
    }
}

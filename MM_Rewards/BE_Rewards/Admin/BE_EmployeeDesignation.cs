﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards;

namespace BE_Rewards.Admin
{
    public sealed class BE_EmployeeDesignation
    {
        private int _ID;
        private string _TokenNumber;
        private int _IsCEO;
        private int _IsRR;
        private int _IsAdmin;
        private string _EmpName;
        private string _ProcessName;
        private string _DivisionName;
        private string _LocationName;

        public string ProcessName
        {
            get { return _ProcessName; }
            set { _ProcessName = value; }
        }
        
        public string DivisionName
        {
            get { return _DivisionName; }
            set { _DivisionName = value; }
        }

        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        public string EmpName
        {
            get { return _EmpName; }
            set { _EmpName = value; }
        }

        public int IsAdmin
        {
            get { return _IsAdmin; }
            set { _IsAdmin = value; }
        }
        public int IsRR
        {
            get { return _IsRR; }
            set { _IsRR = value; }
        }
        public int IsCEO
        {
            get { return _IsCEO; }
            set { _IsCEO = value; }
        }

        public string TokenNumber
        {
            get { return _TokenNumber; }
            set { _TokenNumber = value; }
        }

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
    }
}

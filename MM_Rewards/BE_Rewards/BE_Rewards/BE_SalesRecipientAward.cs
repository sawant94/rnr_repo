﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
    public sealed class BE_SalesRecipientAward
    {
        private int id;
        private string tokenNumber;
        private string firstName;
        private string lastName;
        private string divisionName_PA;
        private string divisionCode_PA;
        private string employeeLevelName_ES;
        private string designation;
        private string emailID;
        private string processName;
        private string userName;
        private string locationName_PSA;
        private string locationCode_PSA;
        private string orgUnitName;
        private string awardType;
        private decimal awardAmount;
        private string reasonForNomination;
        private string giverTokenNumber;
        private int masAwardID;
        private string awardName;
        // added by swapneel
        private string Request_Id;
        private string Request_Date;
        private string Nom_Name;
        private string Nom_EmpId;
        private string Rec_Name;
        private string Rec_EmpId;
        private string Delivery_Address;
        private string PinCode;
        private string GiftHamper_Code;
        private string Request_Status;
        // added by gaurav
        private string businessUnit;
        private string coffeeDate;
        private string ccEmailID;
        private string redeemType;
        private DateTime redeemDate;
        private DateTime createdDate;

        // by vandana
        private string _level;


        public string Level
        {
            get { return _level; }
            set { _level = value; }
        }
                
        public string RequestId
        {
            get { return Request_Id; }
            set { Request_Id = value; }
        }

        public string RequestDate
        {
            get { return Request_Date; }
            set { Request_Date = value; }
        }

        public string NomName
        {
            get { return Nom_Name; }
            set { Nom_Name = value; }
        }

        public string NomEmpId
        {
            get { return Nom_EmpId; }
            set { Nom_EmpId = value; }
        }

        public string RecName
        {
            get { return Rec_Name; }
            set { Rec_Name = value; }
        }

        public string RecEmpId
        {
            get { return Rec_EmpId; }
            set { Rec_EmpId = value; }
        }

        public string DeliveryAddress
        {
            get { return Delivery_Address; }
            set { Delivery_Address = value; }
        }

        public string Pincode
        {
            get { return PinCode; }
            set { PinCode = value; }
        }

        public string GiftHamperCode
        {
            get { return GiftHamper_Code; }
            set { GiftHamper_Code = value; }
        }

        public string RequestStatus
        {
            get { return Request_Status; }
            set { Request_Status = value; }
        }

        // code ends here

        public string AwardName
        {
            get { return awardName; }
            set { awardName = value; }
        }

        public string DivisionCode_PA
        {
            get { return divisionCode_PA; }
            set { divisionCode_PA = value; }
        }
        

        public string LocationCode_PSA
        {
            get { return locationCode_PSA; }
            set { locationCode_PSA = value; }
        }
        

        private int isExpired;

        public int IsExpired
        {
            get { return isExpired; }
            set { isExpired = value; }
        }

        public int MasAwardID
        {
            get { return masAwardID; }
            set { masAwardID = value; }
        }
        public string GiverTokenNumber
        {
            get { return giverTokenNumber; }
            set { giverTokenNumber = value; }
        }

        public string ReasonForNomination
        {
            get { return reasonForNomination; }
            set { reasonForNomination = value; }
        }

        public decimal AwardAmount
        {
            get { return awardAmount; }
            set { awardAmount = value; }
        }

        public string AwardType
        {
            get { return awardType; }
            set { awardType = value; }
        }

        public string OrgUnitName
        {
            get { return orgUnitName; }
            set { orgUnitName = value; }
        }

        public string LocationName_PSA
        {
            get { return locationName_PSA; }
            set { locationName_PSA = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }

        public string EmailID
        {
            get { return emailID; }
            set { emailID = value; }
        }

        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }

        public string EmployeeLevelName_ES
        {
            get { return employeeLevelName_ES; }
            set { employeeLevelName_ES = value; }
        }

        public string DivisionName_PA
        {
            get { return divisionName_PA; }
            set { divisionName_PA = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string TokenNumber
        {
            get { return tokenNumber; }
            set { tokenNumber = value; }
        }
        
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string CoffeeDate    // added by gaurav
        {
            get { return coffeeDate; }
            set { coffeeDate = value; }
        }

        public string CCEmailID    // added by gaurav
        {
            get { return ccEmailID; }
            set { ccEmailID = value; }
        }

        public string BusinessUnit  // added by gaurav
        {
            get { return businessUnit; }
            set { businessUnit = value; }
        }

        public string RedeemType
        {
            get { return redeemType; }
            set { redeemType = value; }
        }

        public DateTime RedeemDate
        {
            get { return redeemDate; }
            set { redeemDate = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

    }
}

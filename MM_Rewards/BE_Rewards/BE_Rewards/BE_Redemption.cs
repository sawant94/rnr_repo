﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_Redemption
    {
        public int ID { get; set; }
        public string RecipientTokenNumber { get; set; }
        public string RecipientName { get; set; }
        public string GiverTokenNumber { get; set; }
        public string GiverName { get; set; }
        public string AwardID { get; set; }
        public string AwardType { get; set; }
        public Decimal AwardAmount { get; set; }
        public DateTime AwardedDate { get; set; }
        public DateTime RevertDate { get; set; }
        public string ReasonForNomination { get; set; }
//        public string ReasonOnCertificate { get; set; }

        public DateTime? EmailedDate { get; set; }
        public string RedeemType { get; set; }
        public DateTime? RedeemDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? ClosureDate { get; set; }
        public Decimal? ClosingAmount { get; set; }
        public string VendorInvoiceValue { get; set; }
        public int? Status { get; set; }

        public bool IsAuto { get; set; }
        public bool Giver_IsAdmin { get; set; }



        public string Feedback { get; set; }   // by vandana
    }
}

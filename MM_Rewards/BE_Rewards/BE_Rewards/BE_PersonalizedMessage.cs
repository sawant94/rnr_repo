﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
   public sealed class BE_PersonalizedMessage
   {
       
       #region Properties
       public int ID { get; set; }
        public string RecipientTokenNumber { get; set; }
        public string HodTokenNumber { get; set; }
        public string RecipientName { get; set; }
        public string DivisionName { get; set; }
        public string LocationName { get; set; }
        public int Years { get; set; }
        public string MilestoneDate { get; set; }
        public string PersonalizedMessage { get; set; }
        public string RNREmailID { get; set; }
        #endregion
   }
}

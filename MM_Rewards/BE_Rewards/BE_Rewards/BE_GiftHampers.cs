﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
    public sealed class BE_GiftHampers
    {
        // added by swapneel
        private string Request_Id;
        private string Request_Date;
        private string Nom_Name;
        private string Nom_EmpId;
        private string Nom_EmailID;
        private string Rec_Name;
        private string Rec_EmpId;
        private string Rec_EmailID;
        private string Delivery_Address;
        private string PinCode;
        private string Gift_Amount;
        private string Request_Status;
        private string Points_Assigned;
        private string Points_Used;
        private string Bal;
        private string Award_Id;

        
        private string Awrad_BusinessUnit;
        private string Award_Division;
        private string Award_Location;

        public string RequestId
        {
            get { return Request_Id; }
            set { Request_Id = value; }
        }

        public string RequestDate
        {
            get { return Request_Date; }
            set { Request_Date = value; }
        }

        public string NomName
        {
            get { return Nom_Name; }
            set { Nom_Name = value; }
        }

        public string NomEmpId
        {
            get { return Nom_EmpId; }
            set { Nom_EmpId = value; }
        }

        public string RecName
        {
            get { return Rec_Name; }
            set { Rec_Name = value; }
        }

        public string RecEmpId
        {
            get { return Rec_EmpId; }
            set { Rec_EmpId = value; }
        }

        public string DeliveryAddress
        {
            get { return Delivery_Address; }
            set { Delivery_Address = value; }
        }

        public string Pincode
        {
            get { return PinCode; }
            set { PinCode = value; }
        }

        public string GiftAmount
        {
            get { return Gift_Amount; }
            set { Gift_Amount = value; }
        }

        public string RequestStatus
        {
            get { return Request_Status; }
            set { Request_Status = value; }
        }

        public string NomEmailID
        {
            get { return Nom_EmailID; }
            set { Nom_EmailID = value; }
        }

        public string RecEmailID
        {
            get { return Rec_EmailID; }
            set { Rec_EmailID = value; }
        }


        public string PointsAssigned
        {
            get { return Points_Assigned; }
            set { Points_Assigned = value; }
        }

        public string PointsUsed
        {
            get { return Points_Used; }
            set { Points_Used = value; }
        }

        public string Balance
        {
            get { return Bal; }
            set { Bal = value; }
        }
        public string AwardId
        {
            get { return Award_Id; }
            set { Award_Id = value; }
        }
        public string AwardBusinessUnit
        {
            get { return Awrad_BusinessUnit; }
            set { Awrad_BusinessUnit = value; }
        }

        public string AwardDivision
        {
            get { return Award_Division; }
            set { Award_Division = value; }
        }

        public string AwardLocation
        {
            get { return Award_Location; }
            set { Award_Location = value; }
        }
        //public string AwardID
        //{
        //    get { return Award_ID; }
        //    set { Award_ID = value; }
        //}
    }
}

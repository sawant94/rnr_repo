﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
    public sealed class BE_AwardClosure
    {
        public int ID { get; set; }
        public DateTime? ClosureDate { get; set; }
        public string AwardID { get; set; }
        public string FileName { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string RecipientTokenNumber { get; set; }
        public string RecipientName { get; set; }
        public Decimal? ClosingAmount { get; set; }
        public string AwardType { get; set; } 
    }
}

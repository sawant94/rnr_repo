﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
    public class BE_BudgetRequest
    {
        public int ID { get; set; }
        public string RequestorTokenNumber { get; set; }
        public string RequesteeTokenNumber { get; set; }
        public string FullName { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }
        public char IsCE { get; set; }
        public string  SearchBy { get; set; }
        public decimal RequestedAmount { get; set; }
        public string RequestReason { get; set; }
        public string EmailID { get; set; }
        public string RequestedDate { get; set; }
        public string RequesteeName { get; set; }
        public string IsCEHOD { get; set; }
        public int IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedReason { get; set; }
        public decimal ApprovedAmount { get; set; }
        public string ShortText { get; set; }
        public decimal Budget { get; set; }

    }
}

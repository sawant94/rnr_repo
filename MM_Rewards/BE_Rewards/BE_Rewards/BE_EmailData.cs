﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
    public class BE_EmailData
    {
        public int Id { get; set; }
        public string GiverName { get; set; }
        public string RecipientName { get; set; }
        public string GiverEmail { get; set; }
        public string RecipientEmail { get; set; }
        public string GiverToken { get; set; }
        public string RecipientToken { get; set; }
        public string AwardType { get; set; }
        public string CardType { get; set; }
        public DateTime AwardDate { get; set; }
        public string AwardAmount { get; set; }
        public string SenderDesignation { get; set; }
        public string AttachmentName { get; set; }
        public string Subject { get; set; }
        public string CCID { get; set; }
        public string ReasonForNomination { get; set; }
        public bool IsSendEmail { get; set; }
        public bool IsDownloadOnLocal { get; set; }
        public bool IsSendAPI { get; set; }
        public string ReferenceNumber { get; set; } 

    }
}

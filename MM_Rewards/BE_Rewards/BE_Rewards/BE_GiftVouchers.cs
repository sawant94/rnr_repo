﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
    public sealed class BE_GiftVouchers
    {
        // added by swapneel

        private string Request_Id;
        private string Request_Date;
        private string Nom_EmpId;
        private string Nom_Name;
        private string Rec_EmpId;
        private string Rec_Name;
        private string Nom_Email;
        private string Rec_Email;
        private string Gift_Amount;
        private string Request_Status;
        private string Awrad_BusinessUnit;
        private string Award_Division;
        private string Award_Location;
        private string Award_ID;

        public string RequestId
        {
            get { return Request_Id; }
            set { Request_Id = value; }
        }

        public string RequestDate
        {
            get { return Request_Date; }
            set { Request_Date = value; }
        }

        public string NomName
        {
            get { return Nom_Name; }
            set { Nom_Name = value; }
        }

        public string NomEmpId
        {
            get { return Nom_EmpId; }
            set { Nom_EmpId = value; }
        }

        public string RecName
        {
            get { return Rec_Name; }
            set { Rec_Name = value; }
        }

        public string RecEmpId
        {
            get { return Rec_EmpId; }
            set { Rec_EmpId = value; }
        }

        public string NomEmail
        {
            get { return Nom_Email; }
            set { Nom_Email = value; }
        }

        public string RecEmail
        {
            get { return Rec_Email; }
            set { Rec_Email = value; }
        }

        public string GiftAmount
        {
            get { return Gift_Amount; }
            set { Gift_Amount = value; }
        }

        public string RequestStatus
        {
            get { return Request_Status; }
            set { Request_Status = value; }
        }

        public string AwardBusinessUnit
        {
            get { return Awrad_BusinessUnit; }
            set { Awrad_BusinessUnit = value; }
        }

        public string AwardDivision
        {
            get { return Award_Division; }
            set { Award_Division = value; }
        }

        public string AwardLocation
        {
            get { return Award_Location; }
            set { Award_Location = value; }
        }
        public string AwardID
        {
            get { return Award_ID; }
            set { Award_ID = value; }
        }
    }
}

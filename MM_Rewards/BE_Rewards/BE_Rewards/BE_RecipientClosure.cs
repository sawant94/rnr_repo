﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Admin
{
    public sealed class BE_RecipientClosure
    { 
        #region feilds
        private int id;
        private string awardid;
        private int amount;
        private DateTime closingdate;
        private string tokennumber;
        private string recipientName;
        private string awardedDate;
        private string redeemedDate;
        private string redeemedType;        
        private decimal closingamount;
        private string reasonForNomination;
        #endregion

        #region Properties
        public string AwardType { get; set; }        
        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public string Awardid
        {
            get { return awardid; }
            set { awardid = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }    

        public string Tokennumber
        {
            get { return tokennumber; }
            set { tokennumber = value; }
        }
     
        public string RecipientName
        {
            get { return recipientName; }
            set { recipientName = value; }
        }
          
        public string AwardedDate
        {
            get { return awardedDate; }
            set { awardedDate = value; }
        }
       

        public string ReasonForNomination
        {
            get { return reasonForNomination; }
            set { reasonForNomination = value; }
        }
      

        public decimal closingAmount
        {
            get { return closingamount; }
            set { closingamount = value; }
        }
                   
         public DateTime ClosingDate
        {
            get { return closingdate; }
            set { closingdate = value; }
        }

         public string RedeemedDate
         {
             get { return redeemedDate; }
             set { redeemedDate = value; }
         }


         public string RedeemedType
         {
             get { return redeemedType; }
             set { redeemedType = value; }
         }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text;
using System.Collections.ObjectModel;

namespace BE_Rewards.BE_Rewards
{
    public sealed class BE_Ecard
    {
        private int _ID;
        private string _RecipientTokenNumber;
        public string UserName { get; set; }
        private string _GiverTokenNUmber;
        private string _Message;
        private string _CardType;
        private string _CCEmailID;
        public DateTime Date { get; set; }
        private string _ImageURL;


        public string CardType
        {
            get { return _CardType; }
            set { _CardType = value; }
        }

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string RecipientTokenNumber
        {
            get { return _RecipientTokenNumber; }
            set { _RecipientTokenNumber = value; }
        }

        public string GiverTokenNUmber
        {
            get { return _GiverTokenNUmber; }
            set { _GiverTokenNUmber = value; }
        }

        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string CCEmailID
        {
            get { return _CCEmailID; }
            set { _CCEmailID = value; }
        }
        public string ImageURL
        {
            get { return _ImageURL; }
            set { _ImageURL = value; }
        }
    }
    public class BE_EcardCollection : Collection<BE_Ecard>
    {

    }
}

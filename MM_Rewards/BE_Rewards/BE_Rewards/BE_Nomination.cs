﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BE_Rewards.BE_Rewards
{
    public sealed class BE_Nomination
    {
        private int id;
        private string aflcToken;
        private string aflcName;
        private string recipientToken;
        private string recipientName;
        private string businessUnit;
        private string story;
        private string synopsis;
        private string businessImpact;
        private int nominatedYear;
        private int nominatedQuarter;
        private string createdBy;
        private int awardAmount;
        private string awardDecision;
        private string actionBy;
        private string referenceNumber;
        private string remark;

        private int employeeCount;
        private int totalAmount;
        private string createdByName;

        //---------------------
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string AFLCToken
        {
            get { return aflcToken; }
            set { aflcToken = value; }
        }
        public string AFLCName
        {
            get { return aflcName; }
            set { aflcName = value; }
        }

        public string RecipientToken
        {
            get { return recipientToken; }
            set { recipientToken = value; }
        }
        public string RecipientName
        {
            get { return recipientName; }
            set { recipientName = value; }
        }
        public string BusinessUnit
        {
            get { return businessUnit; }
            set { businessUnit = value; }
        }
        public string Story
        {
            get { return story; }
            set { story = value; }
        }
        public string Synopsis
        {
            get { return synopsis; }
            set { synopsis = value; }
        }
        public string BusinessImpact
        {
            get { return businessImpact; }
            set { businessImpact = value; }
        }
        public int NominatedYear
        {
            get { return nominatedYear; }
            set { nominatedYear = value; }
        }
        public int NominatedQuarter
        {
            get { return nominatedQuarter; }
            set { nominatedQuarter = value; }
        }
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int AwardAmount
        {
            get { return awardAmount; }
            set { awardAmount = value; }
        }
        public string AwardDecision
        {
            get { return awardDecision; }
            set { awardDecision = value; }
        }
        public string ActionBy
        {
            get { return actionBy; }
            set { actionBy = value; }
        }
        public string ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }
        public int EmployeeCount
        {
            get { return employeeCount; }
            set { employeeCount = value; }
        }
        public int TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }
        public string CreatedByName
        {
            get { return createdByName; }
            set { createdByName = value; }
        }
        public BE_RecipientColl objBERecipientList { get; set; }
    }

    public class BE_Recipient
    {
        public string RecipientToken { get; set; }
        public string RecipientName { get; set; }
    }
    public class BE_RecipientColl : Collection<BE_Recipient>
    {
    }
    public class BE_NominationColl : Collection<BE_Nomination>
    {
    }
    public class YearData
    {
        public int Quarter { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int Year { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE_Rewards.BE_Rewards
{
    public class BE_OnlineRecommendation
    {
        public int ID { get; set; }
        public string RequestorTokenNumber { get; set; }
        public string RequesteeTokenNumber { get; set; }
        public string FullName { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }
        public string Levels { get; set; }
        public char IsCE { get; set; }
        public string SearchBy { get; set; }
        public decimal RequestedAmount { get; set; }
        public string RequestReason { get; set; }
        public string EmailID { get; set; }
        public string RequestedDate { get; set; }
        public string RequesteeName { get; set; }
        public string IsCEHOD { get; set; }
        public int IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedReason { get; set; }
        public decimal ApprovedAmount { get; set; }
        public string ShortText { get; set; }
        public decimal Budget { get; set; }
        private string tokenNumber;
        private string firstName;
        private string lastName;
        private string divisionName_PA;
        private string divisionCode_PA;
        private string employeeLevelName_ES;
        private string designation;
        private string emailID;
        private string processName;
        private string userName;
        private string locationName_PSA;
        private string locationCode_PSA;
        private string orgUnitName;
        private string awardType;
        private decimal awardAmount;
        private string reasonForNomination;
        private string giverTokenNumber;
        private int masAwardID;
        private string awardName;
        private string RecommendedEmp;
        public string AwardedDate { get; set; }
        public string RecHODEMAIL { get; set; }

        public string AwardName
        {
            get { return awardName; }
            set { awardName = value; }
        }

        public string DivisionCode_PA
        {
            get { return divisionCode_PA; }
            set { divisionCode_PA = value; }
        }


        public string LocationCode_PSA
        {
            get { return locationCode_PSA; }
            set { locationCode_PSA = value; }
        }


        private int isExpired;

        public int IsExpired
        {
            get { return isExpired; }
            set { isExpired = value; }
        }

        public int MasAwardID
        {
            get { return masAwardID; }
            set { masAwardID = value; }
        }
        public string GiverTokenNumber
        {
            get { return giverTokenNumber; }
            set { giverTokenNumber = value; }
        }

        public string ReasonForNomination
        {
            get { return reasonForNomination; }
            set { reasonForNomination = value; }
        }

        public decimal AwardAmount
        {
            get { return awardAmount; }
            set { awardAmount = value; }
        }

        public string AwardType
        {
            get { return awardType; }
            set { awardType = value; }
        }

        public string OrgUnitName
        {
            get { return orgUnitName; }
            set { orgUnitName = value; }
        }

        public string LocationName_PSA
        {
            get { return locationName_PSA; }
            set { locationName_PSA = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }


        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }

        public string EmployeeLevelName_ES
        {
            get { return employeeLevelName_ES; }
            set { employeeLevelName_ES = value; }
        }

        public string DivisionName_PA
        {
            get { return divisionName_PA; }
            set { divisionName_PA = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string TokenNumber
        {
            get { return tokenNumber; }
            set { tokenNumber = value; }
        }

        public string RecommendedEmployee
        {
            get { return RecommendedEmp; }
            set { RecommendedEmp = value; }
        }
        
    }
}

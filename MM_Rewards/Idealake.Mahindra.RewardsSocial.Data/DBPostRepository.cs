﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Idealake.Mahindra.RewardsSocial;

namespace Idealake.Mahindra.RewardsSocial.Data
{
    public class DBPostRepository : IPostRepository
    {
        private string connectionstring;

        public DTO.Post GetPostById(int ID)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return Mapper.Map((from p in db.Posts
                                   where p.Id == ID
                                   select p
                        ).FirstOrDefault());
            }
        }

        public DTO.Post UpdatePost(DTO.Post post)
        {
            DTO.Post result = null;
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Post posttoupdate = (from p in db.Posts
                                     where p.Id == post.ID
                                     select p).FirstOrDefault();
                if (posttoupdate != null)
                {
                    Mapper.Map(posttoupdate, post);
                    db.SaveChanges();
                    result = post;
                }
            }
            return result;
        }

        public DTO.Post CreateNewPost(DTO.Post innerObject)
        {


            Post newPost = new Post();
            Mapper.Map(newPost, innerObject);
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.AddToPosts(newPost);
                try
               {
                    db.SaveChanges();
                }

                catch (Exception exx)
                {
                    Console.WriteLine(exx.Message);
                    throw exx;
                }
                innerObject.ID = newPost.Id;
            }
            return innerObject;
        }

        public void LikePost(DTO.Post post, DTO.User user)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.LikePost(post.ID, user.ID);
            }
        }

        public DTO.Comment AddCommentToPost(string message, DTO.Post innerObject, DTO.User user)
        {
            Comment newComment = new Comment();
            DTO.Comment comment = new DTO.Comment();
            comment.ID = -1;
            comment.PostId = innerObject.ID;
            comment.SourceId = user.ID;
            comment.Content = message;
            comment.CreatedOn = DateTime.Now;
            Mapper.Map(newComment, comment);
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.AddToComments(newComment);
                db.SaveChanges();
                comment.ID = newComment.Id;
            }
            return comment;
        }

        public IEnumerable<DTO.Comment> GetCommentsForPost(DTO.Post post, int limit = 1000)
        {

            //using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            //{
            //    return (from comment in db.Comments
            //            where comment.PostId == post.ID
            //            orderby comment.CreatedOn
            //            select new DTO.Comment()
            //            {
            //                ID = comment.Id,
            //                PostId = comment.PostId,
            //                SourceId = comment.SourceId,
            //                Content = comment.Content,
            //                CreatedOn = comment.CreatedOn
            //            }).ToList();
            //}

            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                IEnumerable<DTO.Comment> comments = (from p in db.GetCommentsForPost(post.ID, limit)
                                                     select new DTO.Comment()
                                                     {
                                                         ID = p.ID,
                                                         PostId = p.PostId,
                                                         SourceId = p.SourceId,
                                                         Content = p.Content,
                                                         CreatedOn = p.CreatedOn,
                                                         LikeCount = p.LikeCount
                                                     }).ToList();

                return comments;
            }
        }

        public DTO.Comment GetCommentById(int ID)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return Mapper.Map((from comment in db.Comments
                                   where comment.Id == ID
                                   select comment).FirstOrDefault());

            }
        }

        public bool DeleteComment(DTO.Comment innerObject)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                try
                {
                    Comment comment = (from c in db.Comments
                                       where c.Id == innerObject.ID
                                       select c).FirstOrDefault();

                    db.Comments.DeleteObject(comment);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public void SharePostWithGroups(DTO.Post post, IEnumerable<RewardsSocial.Group> groups)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Post p = db.Posts.Where(i => i.Id == post.ID).First();
                p.IsSharedWithAllFriends = false;
                p.Groups.Clear();
                foreach (var g in groups)
                {
                    Group lg = db.Groups.Where(i => i.Id == g.ID).First();
                    p.Groups.Add(lg);
                }
                db.SaveChanges();
            }

        }

        public IEnumerable<DTO.Group> GetSharedGroupsForPost(DTO.Post post)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Post p = db.Posts.Where(i => i.Id == post.ID).First();
                return (from g in p.Groups
                        select new DTO.Group()
                        {
                            ID = g.Id,
                            Title = g.Title,
                            OwnerId = g.OwnerId,
                            Photo = g.Photo
                        }).ToList();
            }
        }

        public void DeleteSharedGroups(DTO.Post post)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Post p = db.Posts.Where(i => i.Id == post.ID).First();
                p.Groups.Clear();
                db.SaveChanges();
            }
        }



        public IEnumerable<DTO.Like> GetLikesByPostId(int postID)
        {
            IEnumerable<DTO.Like> likes = null;
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {

                likes = (from item in db.GetLikesbyPostId(postID)
                         select new DTO.Like()
                         {
                             OwnerName = item.OwnerName,
                             PostID = item.PostId,
                             UserId = item.LikeUser
                         }).ToList();

            }
            return likes;
        }

        public void LikeComment(DTO.Comment comment, DTO.User user)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.InsertLikeComment(comment.ID, user.ID);
            }
        }

        public IEnumerable<DTO.CommentLike> GetLikesByCommentId(int commentID)
        {
            IEnumerable<DTO.CommentLike> likes = null;
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {

                likes = (from item in db.GetLikesbyCommentId(commentID)
                         select new DTO.CommentLike()
                         {
                             OwnerName = item.OwnerName,
                             CommentID = item.CommentId,
                             UserId = item.LikeUser
                         }).ToList();

            }
            return likes;
        }

        internal DBPostRepository(string connectionString)
        {
            connectionstring = connectionString;
        }

    }
}

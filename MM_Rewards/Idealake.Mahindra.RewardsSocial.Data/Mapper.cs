﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.Data
{
    public class Mapper
    {
        public static DTO.User Map(User u)
        {
            return u == null ? null : new DTO.User()
            {
                ID = u.Id,
                FullName = u.FullName,
                Photo = string.IsNullOrWhiteSpace(u.Photo) ? "userimage.jpg" : u.Photo,
                Department = u.Department,
                Designation = u.Designation,
                TokenNumber = u.TokenNumber,
                Education = u.Education,
                Interest = u.Interest,
                 hifimail = (bool)u.hifimail
            };
        }

        public static void Map(User u, DTO.User user)
        {
            u.FullName = user.FullName;
            u.Photo = user.Photo;
            u.Department = user.Department;
            u.Designation = user.Designation;
            u.TokenNumber = user.TokenNumber;
            u.Education = user.Education;
            u.Interest = user.Interest;
            u.hifimail = user.hifimail;
        }

        internal static DTO.Post Map(Post p)
        {
            return p == null ? null : new DTO.Post()
            {
                ID = p.Id,
                PostTypeId = p.PostType,
                SourceId = p.SourceId,
                TargetId = p.TargetId,
                Content = p.Content,
                IsSharedWithAllFriends = p.IsSharedWithAllFriends,
                CreatedOn = p.CreatedOn,
                ishifiMailSent = p.hifiMailSent == null ? false : (bool)p.hifiMailSent

            };
        }

        internal static void Map(Post p, DTO.Post post)
        {
            p.PostType = post.PostTypeId;
            p.SourceId = post.SourceId;
            p.TargetId = post.TargetId;
            p.Content = post.Content;
            p.IsSharedWithAllFriends = post.IsSharedWithAllFriends;
            p.CreatedOn = post.CreatedOn;
            p.Title = post.Title;
            p.hifiMailSent = post.ishifiMailSent;
        }

        internal static DTO.Comment Map(Comment c)
        {
            return c == null ? null : new DTO.Comment()
            {
                ID = c.Id,
                PostId = c.PostId,
                SourceId = c.SourceId,
                Content = c.Content,
                CreatedOn = c.CreatedOn
            };
        }

        internal static void Map(Comment c, DTO.Comment comment)
        {
            c.Id = comment.ID;
            c.PostId = comment.PostId;
            c.SourceId = comment.SourceId;
            c.Content = comment.Content;
            c.CreatedOn = comment.CreatedOn;
        }

        internal static DTO.Notification Map(Notification n)
        {
            return n == null ? null : new DTO.Notification()
            {
                ID = n.Id,
                Message = n.Message,
                DetailURL = n.DetailURL,
                NotificationType = n.NotificationTypeId,
                RelateItemId = n.RelatedItemId,
                CreatedOn = n.CreatedOn,
                TargetId = n.TargetId,
                Active = n.Active
            };
        }

        internal static void Map(Notification n, DTO.Notification notification)
        {
            n.Id = notification.ID;
            n.Message = notification.Message;
            n.DetailURL = notification.DetailURL;
            n.NotificationTypeId = notification.NotificationType;
            n.RelatedItemId = notification.RelateItemId;
            n.TargetId = notification.TargetId;
            n.CreatedOn = notification.CreatedOn;
            n.Active = notification.Active;
            n.Unread = notification.UnRead;
        }

        internal static DTO.ConnectionRequest Map(ConnectionRequest c)
        {
            return c == null ? null : new DTO.ConnectionRequest()
            {
                ID = c.Id,
                SourceId = c.SourceId,
                TargetId = c.TargetId,
                RequestedDate = c.RequestDate,
                Status = c.Status
            };
        }

        internal static void Map(ConnectionRequest c, DTO.ConnectionRequest conreq)
        {
            c.Id = conreq.ID;
            c.SourceId = conreq.SourceId;
            c.TargetId = conreq.TargetId;
            c.RequestDate = conreq.RequestedDate;
            c.Status = conreq.Status;
        }

        internal static DTO.Group Map(Group g)
        {
            return g == null ? null : new DTO.Group()
            {
                ID = g.Id,
                OwnerId = g.OwnerId,
                Title = g.Title,
                Photo = string.IsNullOrWhiteSpace(g.Photo) ? "usergroup.jpg" : g.Photo
            };
        }

        internal static void Map(Group c, DTO.Group conreq)
        {
            c.Id = conreq.ID;
            c.OwnerId = conreq.OwnerId;
            c.Title = conreq.Title;
            c.Photo = conreq.Photo;
        }


    }
}

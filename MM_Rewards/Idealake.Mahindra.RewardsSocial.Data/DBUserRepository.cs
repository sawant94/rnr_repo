﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Idealake.Mahindra.RewardsSocial;
using System.Data.Objects;

namespace Idealake.Mahindra.RewardsSocial.Data
{
    public class DBUserRepository : IUserRepository
    {
        private string connectionstring;

        public DTO.User GetUserById(int ID)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return Mapper.Map((from u in db.Users
                                   where u.Id == ID
                                   select u
                        ).FirstOrDefault());
            }
        }

        public DTO.User UpdateUser(DTO.User user)
        {
            DTO.User result = null;
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                User usertoupdate = (from u in db.Users
                                     where u.Id == user.ID
                                     select u).FirstOrDefault();
                if (usertoupdate != null)
                {
                    Mapper.Map(usertoupdate, user);
                    db.SaveChanges();
                    result = user;
                }
            }
            return result;
        }

        public IEnumerable<DTO.User> GetFriends(DTO.User user)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                var result = (from u in db.Users
                              where u.Id == user.ID
                              select u
                        ).FirstOrDefault();

                return (from item in result.Friends
                        select new DTO.User
                        {
                            ID = item.Id,
                            FullName = item.FullName,
                            Photo = string.IsNullOrWhiteSpace(item.Photo) ? "userimage.jpg" : item.Photo,
                            Department = item.Department,
                            Designation = item.Designation,
                            TokenNumber = item.TokenNumber
                        }).ToList();



            }
        }

        public DTO.User GetUserByTokenNumber(string tokenNumber)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                var result = (from u in db.Users
                              where u.TokenNumber == tokenNumber
                              select u.Id
                        ).FirstOrDefault();
                return new DTO.User() { ID = result };
            }
        }


        public DTO.ConnectionRequest ConnectUsers(RewardsSocial.User sourceUser, RewardsSocial.User targetUser)
        {
            ConnectionRequest conreq = new ConnectionRequest();
            DTO.ConnectionRequest connectionrequest = new DTO.ConnectionRequest();
            connectionrequest.SourceId = sourceUser.ID;
            connectionrequest.TargetId = targetUser.ID;
            connectionrequest.RequestedDate = DateTime.Now;
            connectionrequest.Status = 0;
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Mapper.Map(conreq, connectionrequest);
                db.AddToConnectionRequests(conreq);
                db.SaveChanges();
                connectionrequest.ID = conreq.Id;
            }
            return connectionrequest;
        }

        public void ConnectUsers_BulkUpload(RewardsSocial.User sourceUser, RewardsSocial.User targetUser)
        {

            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                //var result = (from u in db.Users
                //              where u.Users == sourceUser.ID && u.TargetId == currentUser.ID && u.Status == 0
                //              select u
                //        ).FirstOrDefault();

                db.AddFriendBulkUpload(sourceUser.ID, targetUser.ID);
                db.SaveChanges();
                //connectionrequest.ID = conreq.Id;
            }
        }

        public IEnumerable<DTO.ConnectionRequest> GetConnectionRequests(DTO.User user)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                var result = (from u in db.Users
                              where u.Id == user.ID
                              select u
                        ).FirstOrDefault();

                return (from item in result.ReceivedConnectionRequests
                        where item.Status == 0
                        select new DTO.ConnectionRequest
                        {
                            ID = item.Id,
                            SourceId = item.SourceId,
                            TargetId = item.TargetId,
                            RequestedDate = item.RequestDate,
                            Status = item.Status
                        }).ToList();
            }
        }

        public bool AcceptConnectionRequest(DTO.ConnectionRequest connectionRequest)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                if (connectionRequest != null)
                {
                    db.AcceptConnectionRequest(connectionRequest.ID);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool DeclineConnectionRequest(DTO.ConnectionRequest connectionRequest)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                if (connectionRequest != null)
                {
                    db.DeclineConnectionRequest(connectionRequest.ID);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool CheckPendingRequest(DTO.User user, DTO.User currentUser)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                var result = (from u in db.ConnectionRequests
                              where u.SourceId == user.ID && u.TargetId == currentUser.ID && u.Status == 0
                              select u
                        ).FirstOrDefault();
                if (result == null)
                {
                    return false;

                }
                else
                {
                    return true;
                }
            }
        }





        //public bool CreateNewConnection(DTO.ConnectionRequest connectionRequest)
        //{
        //    DTO.ConnectionRequest result = null;
        //    using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
        //    {
        //        if (connectionRequest != null)
        //        {
        //            ConnectionRequest cr = new ConnectionRequest();
        //            Mapper.MapConnectionRequest(cr, connectionRequest);
        //            db.AddToConnectionRequests(cr);
        //            db.SaveChanges();
        //            result = Mapper.MapConnectionRequest(cr); 
        //        }
        //        if (result != null)
        //            return true;
        //        else
        //            return false;
        //    }

        //}



        public IEnumerable<DTO.Post> GetPostsForCurrentUser(DTO.User currentUser, int limit = 1000)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return (from p in db.GetPostsForCurrentUser(currentUser.ID, limit)
                        select new DTO.Post()
                        {
                            ID = p.ID,
                            PostTypeId = p.PostType,
                            SourceId = p.SourceId,
                            TargetId = p.TargetId,
                            Content = p.Content,
                            CreatedOn = p.CreatedOn,
                            IsSharedWithAllFriends = p.IsSharedWithAllFriends,
                            LikeCount = p.LikeCount,
                            CommentCount = p.CommentCount,
                            Title = p.Title
                        }).ToList();

            }
        }



        public IEnumerable<DTO.Post> GetAllPostsForCurrentUser(DTO.User currentUser, int limit = 1000)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return (from p in db.GetAllPostForCurrentUser(currentUser.ID, limit)
                        select new DTO.Post()
                        {
                            ID = p.ID,
                            PostTypeId = p.PostType,
                            SourceId = p.SourceId,
                            TargetId = p.TargetId,
                            Content = p.Content,
                            CreatedOn = p.CreatedOn,
                            IsSharedWithAllFriends = p.IsSharedWithAllFriends,
                            LikeCount = p.LikeCount,
                            CommentCount = p.CommentCount,
                            Title = p.Title
                        }).ToList();

            }
        }


        public IEnumerable<DTO.Post> GetPostsForTargetUser(DTO.User currentUser, DTO.User targetUser, int limit = 1000)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return (from p in db.GetPostsForTargetUser(currentUser.ID, targetUser.ID, limit)
                        select new DTO.Post()
                        {
                            ID = p.ID,
                            PostTypeId = p.PostType,
                            SourceId = p.SourceId,
                            TargetId = p.TargetId,
                            Content = p.Content,
                            CreatedOn = p.CreatedOn,
                            IsSharedWithAllFriends = p.IsSharedWithAllFriends,
                            LikeCount = p.LikeCount,
                            CommentCount = p.CommentCount,
                            Title = p.Title
                        }).ToList();
            }
        }


        //public IEnumerable<DTO.User> SearchForUsers(string searchString)
        //{
        //    using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
        //    {
        //        return (from item in db.SearchForUsers(searchString)
        //                select new DTO.User()
        //                {
        //                    ID = item.ID,
        //                    FullName = item.FullName,
        //                    Photo = string.IsNullOrWhiteSpace(item.Photo) ? "userimage.jpg" : item.Photo,
        //                    TokenNumber = item.TokenNumber,
        //                    Department = item.Department,
        //                    Designation = item.Designation
        //                }).ToList();

        //    }
        //}


        public IEnumerable<DTO.User> SearchForUsers(string searchString)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {

                string[] str = searchString.Split(' ', ',');
                var x = db.Users.AsQueryable();
                foreach (string i in str)
                {
                    x = x.Where(u => u.FullName.Contains(i));
                }
                //return (from item in db.SearchForUsers(searchString)
                var temp = x.ToList();
                return (from item in temp
                        select new DTO.User()
                        {
                            ID = item.Id,
                            FullName = item.FullName,
                            Photo = string.IsNullOrWhiteSpace(item.Photo) ? "userimage.jpg" : item.Photo,
                            TokenNumber = item.TokenNumber,
                            Department = item.Department,
                            Designation = item.Designation
                        }).ToList();

            }
        }

        public IEnumerable<DTO.Notification> GetNotificationsForCurrentUser(DTO.User user)
        {

            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                //var x = from n in db.Notifications
                //        where n.TargetId == user.ID
                //        select new
                //        {
                //            From = n.CreatedOn,
                //            To = DateTime.Now,
                //            Diff = EntityFunctions.DiffDays(n.CreatedOn, DateTime.Now)
                //        };

                var result = (from n in db.Notifications
                              where n.TargetId == user.ID
                              && EntityFunctions.DiffDays(n.CreatedOn, DateTime.Now) <= 15
                              && n.Active == true
                              orderby n.CreatedOn descending
                              select new DTO.Notification()
                              {
                                  ID = n.Id,
                                  Message = n.Message,
                                  NotificationType = n.NotificationTypeId,
                                  RelateItemId = n.RelatedItemId,
                                  TargetId = n.TargetId,
                                  UnRead = n.Unread,
                                  DetailURL = n.DetailURL,
                                  CreatedOn = n.CreatedOn
                              }).ToList();

                return result;
            }
        }
        public DTO.Notification UpdateNotification(DTO.Notification notification)
        {
            DTO.Notification result = null;
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Notification notificationToUpdate = (from u in db.Notifications
                                                     where u.Id == notification.ID
                                                     select u).FirstOrDefault();
                if (notificationToUpdate != null)
                {
                    Mapper.Map(notificationToUpdate, notification);
                    db.SaveChanges();
                    result = notification;
                }
            }
            return result;
        }
        public DTO.Notification CreateNewNotification(DTO.Notification notification)
        {
            Notification newNotification = new Notification();
            Mapper.Map(newNotification, notification);
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.AddToNotifications(newNotification);
                db.SaveChanges();
                notification.ID = newNotification.Id;
            }
            return notification;
        }
        public DTO.Notification GetNotificationById(int notificationID)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return Mapper.Map((from n in db.Notifications
                                   where n.Id == notificationID
                                   select n
                                   ).FirstOrDefault());
            }
        }

        public DTO.Group GetGroupById(int groupID)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return Mapper.Map((from g in db.Groups
                                   where g.Id == groupID
                                   select g
                        ).FirstOrDefault());
            }
        }

        public DTO.Group UpdateGroup(DTO.Group groupToUpdate)
        {
            DTO.Group result = null;
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Group usertoupdate = (from g in db.Groups
                                      where g.Id == groupToUpdate.ID
                                      select g).FirstOrDefault();
                if (usertoupdate != null)
                {
                    Mapper.Map(usertoupdate, groupToUpdate);
                    db.SaveChanges();
                    result = groupToUpdate;
                }
            }
            return result;
        }

        public DTO.Group CreateNewGroup(DTO.Group group)
        {
            Group newGroup = new Group();
            Mapper.Map(newGroup, group);
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.AddToGroups(newGroup);
                db.SaveChanges();
                group.ID = newGroup.Id;
            }
            return group;
        }

        public IEnumerable<DTO.Group> GetGroupsForCurrentUser(DTO.User currentUser)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                var result = (from u in db.Users
                              where u.Id == currentUser.ID
                              select u
                        ).FirstOrDefault();

                return (from item in result.Groups
                        select new DTO.Group
                        {
                            ID = item.Id,
                            OwnerId = item.OwnerId,
                            Title = item.Title,
                            Photo = string.IsNullOrWhiteSpace(item.Photo) ? "groupimage.jpg" : item.Photo
                        }).ToList();
            }
        }

        public IEnumerable<DTO.User> GetMembersForGroup(DTO.Group currentGroup)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                return (from item in db.Groups.Where(g => (g.Id == currentGroup.ID)).FirstOrDefault().Members
                        select new DTO.User()
                        {
                            ID = item.Id,
                            FullName = item.FullName,
                            Photo = string.IsNullOrWhiteSpace(item.Photo) ? "userimage.gif" : item.Photo
                        }).ToList();
            }
        }

        public DTO.User AddMemberToGroup(DTO.Group groupForMember, RewardsSocial.User newMember)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Group g = db.Groups.Where(item => item.Id == groupForMember.ID).First();
                User u = db.Users.Where(item => item.Id == newMember.ID).First();
                if (!g.Members.Contains(u))
                {
                    g.Members.Add(u);
                    db.SaveChanges();
                }
                return Mapper.Map(u);
            }
        }

        public void RemoveMemberFromGroup(DTO.Group groupForMember, RewardsSocial.User oldMember)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                Group g = db.Groups.Where(item => item.Id == groupForMember.ID).First();
                User u = db.Users.Where(item => item.Id == oldMember.ID).First();
                if (g.Members.Contains(u))
                {
                    g.Members.Remove(u);
                    db.SaveChanges();
                }
            }
        }
        public int GetUnreadNotificationCount(DTO.User user)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                var result = (from n in db.Notifications
                              where n.TargetId == user.ID
                              && EntityFunctions.DiffDays(n.CreatedOn, DateTime.Now) <= 15
                              && n.Active == true
                              && n.Unread == true
                              select n);

                return result.Count();
            }

        }

        public void DeleteFriend(DTO.User currentUser, DTO.User user)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.DeleteFriend(currentUser.ID, user.ID);
                db.SaveChanges();
            }
        }

        public void DeleteGroup(DTO.Group group)
        {
            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                db.DeleteGroup(group.ID);
                db.SaveChanges();
            }
        }

        public IEnumerable<DTO.RNRReport> GetRNRReport(DateTime fromDate, DateTime toDate)
        {

            using (MMRewardsSocialDB db = new MMRewardsSocialDB(connectionstring))
            {
                var result = (from rpt in db.GetRNRReports(fromDate, toDate)
                              select new DTO.RNRReport()
                              {
                                  //Hits = rpt.HIT,
                                  BusinessUnit = rpt.BusinessUnit,
                                  CommentsReceived = rpt.CommentsReceived,
                                  FriendRequestsMade = rpt.FriendRequestsMade,
                                  LikesReceived = rpt.LikesReceived,
                                  HighFivesReceived = rpt.HighFivesReceived,
                                  SpotVouchersReceived = rpt.SpotVouchersReceived,
                                  GiftHampersReceived = rpt.GiftHampersReceived,
                                  CoffeeHampersReceived = rpt.CoffeeHampersReceived
                              }).ToList();
                return result;
            }
        }

        internal DBUserRepository(string connectionString)
        {
            connectionstring = connectionString;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Idealake.Mahindra.RewardsSocial;

namespace Idealake.Mahindra.RewardsSocial.Data
{
    public abstract class DBContext : Context
    {
        private string connectionstring;

        protected DBContext(string connectionString):base(new DBUserRepository(connectionString), new DBPostRepository(connectionString))
        {
            connectionstring = connectionString;
        }
    }
}

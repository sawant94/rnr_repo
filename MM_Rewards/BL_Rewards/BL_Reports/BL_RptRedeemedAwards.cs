﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using MM.DAL;
using System.Net;
using System.Net.Mail;
using BE_Rewards;
using BE_Rewards.BE_Admin;

namespace BL_Rewards.BL_Reports
{
    //-----------------------------------------------------------------------------------------------------
    public class Reports
    {
        //Function to get ALL Details for Not Redeemed Award Reports
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetRedeemedAwardsReport(string Business, string Division, string start, string end, string Location, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //  objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetAllAward_Redeemed", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.FirstName = dr["RecipientName"].ToString();
                    bem.AwardType = dr["AwardName"].ToString();
                    bem.AwardAmount = Convert.ToDecimal(dr["AwardAmount"].ToString());
                    bem.RewardReason = dr["ReasonForNomination"].ToString();
                    bem.AwardDate = Convert.ToDateTime(dr["AwardedDate"]).ToString("dd-MMM-yyyy");
                    bem.RedeemType = dr["RedeemType"].ToString();
                    bem.RedeemedDate = Convert.ToDateTime(dr["RedeemDate"]).ToString("dd-MMM-yyyy");
                    bem.ExpiryDate = String.IsNullOrEmpty(dr["ExpiryDate"].ToString()) ? "" : Convert.ToDateTime(dr["ExpiryDate"]).ToString("dd-MMM-yyyy");//Convert.ToDateTime(dr["ExpiryDate"]).ToString("dd-MMM-yyyy");
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.LocationName = dr["LocationName_PSA"].ToString();
                    bem.Divisioncode = dr["DivisionName_PA"].ToString();
                    bem.Department = dr["OrgUnitName"].ToString();

                    mailDetailsColl.Add(bem);
                }

            }

            return mailDetailsColl;
        }

        //Function to get ALL Details for Not Redeemed Award Reports
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetNotRedeemedAwardsReport(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //      objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_Rpt_GetAllAward_NotRedeemed", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.FirstName = dr["RecipientName"].ToString();
                    bem.AwardType = dr["AwardName"].ToString();
                    bem.AwardAmount = Convert.ToDecimal(dr["AwardAmount"].ToString());
                    bem.RewardReason = dr["ReasonForNomination"].ToString();
                    bem.AwardDate = Convert.ToDateTime(dr["AwardedDate"]).ToString("dd-MMM-yyyy");
                    bem.EmailedDate = Convert.ToDateTime(dr["EmailedDate"]).ToString("dd-MMM-yyyy");
                    bem.ExpiryDate = String.IsNullOrEmpty(dr["ExpiryDate"].ToString()) ? "" : Convert.ToDateTime(dr["ExpiryDate"]).ToString("dd-MMM-yyyy");
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.LocationName = dr["LocationName_PSA"].ToString();
                    bem.Divisioncode = dr["DivisionName_PA"].ToString();
                    bem.Department = dr["OrgUnitName"].ToString();

                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        //Function to get ALL Details Of Transferd Budget
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetBudgetTransferDetails(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();
            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //   objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetBudgetTransferedDetails", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["CEOTokenNumber"].ToString();
                    bem.GiverName = dr["CEOName"].ToString();
                    bem.AllocationFrom = dr["AllocationType"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.FirstName = dr["RecipientName"].ToString();
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.OrgUnitName = dr["OrgUnitName"].ToString();
                    bem.Budget = Convert.ToDecimal(dr["Budget"].ToString());
                    bem.CreatedDate = Convert.ToDateTime(dr["AllocationDate"]).ToString("dd-MMM-yyyy");
                    bem.Createdby = dr["CreatedBy"].ToString();
                    bem.LastName = dr["GivenBy"].ToString();
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        //Function to get ALL Details of Budget Utilization without department
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetBudgetutilisationDetail(string Business, string Division, string Location, string start, string end, string HODTokenNumber, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[7];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            try
            {

                objsqlparam[0] = new SqlParameter("@Business", Business);
                objsqlparam[1] = new SqlParameter("@Division_PA", Division);
                objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
                //     objsq3param[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
                objsqlparam[3] = new SqlParameter("@StartDate", start);
                objsqlparam[4] = new SqlParameter("@EndDate", end);
                objsqlparam[5] = new SqlParameter("@HOD", HODTokenNumber);
                objsqlparam[6] = new SqlParameter("@Tokennumber", TokenNumber);

                //_ds = objDAL.DAL_GetData("usp_Rpt_GetBudgetExpenditureDetail_HOD_test", objsqlparam);
                _ds = objDAL.DAL_GetDataN("usp_Rpt_GetBudgetExpenditureDetail_HOD_test", objsqlparam);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in _ds.Tables[0].Rows)
                    {
                        BE_MailDetails bem = new BE_MailDetails();
                        bem.GiverTokenNUmber = dr["TokenNumber"].ToString();
                        bem.GiverName = dr["HOD_Name"].ToString();
                        bem.AwardType = dr["AwardNAme"].ToString();
                        bem.BusinessUnit = dr["BusinessUnit"].ToString();
                        bem.Divisioncode = dr["Division"].ToString();
                        bem.Department = dr["Department"].ToString();
                        bem.LocationName = dr["LocationName_PSA"].ToString();
                        string PeriodicExpenditure = dr["PeriodicExpenditure"].ToString();
                        bem.Expenditure = Convert.ToDecimal(dr["Expenditure"].ToString().Split(':')[0]);
                        bem.AddedBalance = dr["AddedBalance"].ToString();
                        bem.DeductedBalance = dr["DeductedBalance"].ToString();

                        string[] countarry = PeriodicExpenditure.Split(':');
                        if (countarry.Length > 1)
                        {
                            bem.PeriodicExpenditure = Convert.ToDecimal(countarry[0].ToString());
                            bem.AwardCount = Convert.ToInt32(countarry[1].ToString());
                        }
                        else
                        {
                            bem.PeriodicExpenditure = Convert.ToDecimal(countarry[0].ToString());
                            bem.AwardCount = 0;
                        }
                        bem.Budget = Convert.ToDecimal(dr["OpeningBalance"].ToString());
                        if (dr["ClosingBalance"].ToString() != "")
                        {
                            bem.ClosingAmount = Convert.ToDecimal(dr["ClosingBalance"].ToString());
                        }
                        else
                        {
                            bem.ClosingAmount = 0;
                        }

                        // String.IsNullOrEmpty(dr["ExpiryDate"].ToString()) ? "" : Convert.ToDateTime(dr["ExpiryDate"]).ToString("dd-MMM-yyyy");
                        if (bem.Expenditure > 0)
                        {
                            if (bem.Budget > 0)
                            {
                                bem.AwardAmount = Convert.ToDecimal(bem.Expenditure / bem.Budget);
                            }
                            else
                            {
                                bem.AwardAmount = Convert.ToDecimal(bem.Budget);
                            }
                        }
                        else
                        {
                            bem.AwardAmount = Convert.ToDecimal(bem.Expenditure);
                        }
                        if (bem.ClosingAmount > 0)
                        {
                            if (bem.Budget > 0)
                            {
                                bem.ClosingAmountPercentile = Convert.ToDecimal(bem.ClosingAmount / bem.Budget);
                            }
                            else
                            {
                                bem.ClosingAmountPercentile = Convert.ToDecimal(bem.Budget);
                            }
                        }
                        else
                        {
                            bem.ClosingAmountPercentile = Convert.ToDecimal(bem.ClosingAmount);
                        }
                        //bem.ClosingAmountPercentile =Convert.ToDecimal (bem.ClosingAmount / bem.Budget);
                        mailDetailsColl.Add(bem);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return mailDetailsColl;
        }

        //Function to get ALL Details for Expiry Date
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetExpiryAward(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //    objsqlpara[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetAllAward_ExpiryDate", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.FirstName = dr["RecipientName"].ToString();
                    bem.AwardName = dr["AwardName"].ToString();
                    bem.AwardAmount = Convert.ToDecimal(dr["AwardAmount"].ToString());
                    bem.AwardDate = Convert.ToDateTime(dr["AwardedDate"]).ToString("dd-MMM-yyyy");
                    bem.ExpiryDate = Convert.ToDateTime(dr["ExpiryDate"]).ToString("dd-MMM-yyyy");

                    mailDetailsColl.Add(bem);
                }

            }

            return mailDetailsColl;
        }

        //Function to get ALL Details for Budget Log
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetBudgetLog(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();
            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //  objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetBudgetLog", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    //BE_MailDetails bem = new BE_MailDetails();
                    //bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    //bem.FirstName = dr["RecipientName"].ToString();
                    //bem.GiverTokenNUmber = dr["Giver_TokenNumber"].ToString();
                    //bem.GiverName = dr["Giver_Name"].ToString();
                    //bem.Budget = Convert.ToDecimal(dr["Budget"].ToString());
                    //bem.AllocationTokenNumber = dr["ModifiedBy_TokenNumber"].ToString();
                    //bem.AllocationFrom = dr["ModifierName"].ToString();
                    //bem.CreatedDate = dr["CreatedDate"].ToString();
                    //mailDetailsColl.Add(bem);

                    BE_MailDetails bem = new BE_MailDetails();
                    bem.RecipientTokenNumber = dr["TokenNumber"].ToString();
                    bem.FirstName = dr["HOD_Name"].ToString();
                    bem.GiverTokenNUmber = dr["Modified_TokenNumber"].ToString();
                    bem.GiverName = dr["Modified_Name"].ToString();
                    bem.Budget = Convert.ToDecimal(dr["Budget"].ToString());
                    bem.AllocationTokenNumber = dr["AllocatedFrom"].ToString();
                    bem.AllocationFrom = dr["CreatedBy"].ToString();
                    bem.CreatedDate = dr["CreatedDate"].ToString();
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        //Function to get ALL Details for Sales Awards Log
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetSalesAwardLog(string Division, string start, string end, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();
            SqlParameter[] objsqlparam = new SqlParameter[4];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Division_PA", Division);

            objsqlparam[1] = new SqlParameter("@StartDate", start);
            objsqlparam[2] = new SqlParameter("@EndDate", end);
            objsqlparam[3] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetSalesAwardLog", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {

                    BE_MailDetails bem = new BE_MailDetails();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    // bem.GiverTokenNUmber = dr["RecipientTokenNumber"].ToString();
                    bem.AwardType = dr["AwardType"].ToString();
                    bem.AwardAmount = Convert.ToInt16(dr["AwardAmount"].ToString());
                    bem.AwardDate = dr["AwardedDate"].ToString();
                    bem.AwardedDate = dr["AwardedDate"].ToString();
                    bem.CreatedDate = dr["CreatedDate"].ToString();
                    bem.SalesDivision = dr["SalesDivision"].ToString();

                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        // Get Number of all Hits
        //-----------------------------------------------------------------------------------------------------
        public List<BE_UserFeedback> GetNumber_of_Hits(string start, string end)
        {
            List<BE_UserFeedback> userfeedbackColl = new List<BE_UserFeedback>();

            SqlParameter[] objsqlparam = new SqlParameter[2];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            objsqlparam[0] = new SqlParameter("@StartDate", start);
            objsqlparam[1] = new SqlParameter("@EndDate", end);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetFeedback", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_UserFeedback buf = new BE_UserFeedback();
                    buf.Tokennumber = dr["TokenNumber"].ToString();
                    buf.Employeename = dr["EmployeeName"].ToString();
                    buf.Division = dr["Division"].ToString();
                    buf.Location = dr["Location"].ToString();
                    buf.Feedtype = dr["FeedBackType"].ToString();
                    buf.Feedbackdetails = dr["FeedBackDetails"].ToString();
                    buf.CreatedDate = dr["CreatedDate"].ToString();
                    userfeedbackColl.Add(buf);
                }
            }
            return userfeedbackColl;
        }

        //Function to get All FeedBack given by user
        //-----------------------------------------------------------------------------------------------------
        public List<BE_UserFeedback> GetUserFeedback(string start, string end)
        {
            List<BE_UserFeedback> userfeedbackColl = new List<BE_UserFeedback>();

            SqlParameter[] objsqlparam = new SqlParameter[2];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            objsqlparam[0] = new SqlParameter("@StartDate", start);
            objsqlparam[1] = new SqlParameter("@EndDate", end);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetFeedback", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_UserFeedback buf = new BE_UserFeedback();
                    buf.Tokennumber = dr["TokenNumber"].ToString();
                    buf.Employeename = dr["EmployeeName"].ToString();
                    buf.Division = dr["Division"].ToString();
                    buf.Location = dr["Location"].ToString();
                    buf.Feedtype = dr["FeedBackType"].ToString();
                    buf.Feedbackdetails = dr["FeedBackDetails"].ToString();
                    buf.CreatedDate = dr["CreatedDate"].ToString();
                    userfeedbackColl.Add(buf);
                }
            }
            return userfeedbackColl;
        }

        //Function to get All Award Redeemed and Non reedeemed
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetAllAward(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //    objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetAllAward", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();

                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.RecipientName = dr["RecipientName"].ToString();
                    bem.AwardName = dr["AwardName"].ToString();
                    bem.AwardAmount = Convert.ToDecimal(dr["AwardAmount"].ToString());
                    bem.AwardDate = dr["AwardedDate"].ToString();
                    bem.AwardStatus = dr["Awardstatus"].ToString();
                    bem.LocationName = dr["GiverLocation"].ToString();
                    bem.Divisioncode = dr["GiverDivision"].ToString();
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.Department = dr["OrgUnitName"].ToString();
                    bem.RecipientLocation = dr["RecipientLocation"].ToString();
                    bem.RecipientDivision = dr["RecipientDivision"].ToString();
                    bem.RecipientDepartment = dr["RecipientDepartment"].ToString();
                    bem.RewardReason = dr["ReasonForNomination"].ToString();

                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        //Function to get ALL Details for Not Redeemed Award Reports
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetClosedAwardsReport(string Business, string Division, string start, string end, string Location, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //     objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetAllAward_Closure", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.FirstName = dr["RecipientName"].ToString();
                    bem.AwardType = dr["AwardName"].ToString();
                    bem.AwardAmount = Convert.ToDecimal(dr["AwardAmount"].ToString());
                    bem.RewardReason = dr["ReasonForNomination"].ToString();
                    bem.AwardDate = Convert.ToDateTime(dr["AwardedDate"]).ToString("dd-MMM-yyyy");
                    bem.RedeemType = dr["RedeemType"].ToString();
                    bem.RedeemedDate = Convert.ToDateTime(dr["RedeemDate"]).ToString("dd-MMM-yyyy");
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.LocationName = dr["LocationName_PSA"].ToString();
                    bem.Divisioncode = dr["DivisionName_PA"].ToString();
                    bem.ClosureDate = dr["ClosureDate"].ToString();
                    bem.ClosingAmount = Convert.ToDecimal(dr["ClosingAmount"].ToString());
                    mailDetailsColl.Add(bem);
                }

            }

            return mailDetailsColl;
        }

        //Function to get ALL Details for MileStones
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetAllMileStoneAwards(string Business, string Division, string start, string end, string Location, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            ////objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetAllAward_MileStones", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.RecipientName = dr["RecipientName"].ToString();
                    bem.LocationName = dr["LocationName_PSA"].ToString();
                    bem.Divisioncode = dr["DivisionName_PA"].ToString();
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.MileStoneYears = Convert.ToInt16(dr["MileStoneYears"]);
                    bem.MileStoneDate = Convert.ToDateTime(dr["MileStoneDate"]).ToString("dd-MMM-yyyy");
                    bem.RewardReason = dr["ReasonForNomination"].ToString();
                    bem.RedeemType = dr["Status"].ToString();
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        //Function to get ALL Details for Long Service
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetLongServiceAwards(string Business, string Division, string start, string end, string Location, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            //       objsqlparam[2] = new SqlParameter("@OrgUnitCode", OrgUnitCode);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_Rpt_GetLongServiceAward", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    //bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.RecipientName = dr["RecipientName"].ToString();
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.LocationName = dr["LocationName_PSA"].ToString();
                    bem.Divisioncode = dr["DivisionName_PA"].ToString();
                    bem.ProcessName = dr["ProcessName"].ToString();
                    //bem.MileStoneYears = Convert.ToInt16(dr["MileStoneYears"]);
                    //bem.MileStoneDate = Convert.ToDateTime(dr["MileStoneDate"]).ToString("dd-MMM-yyyy");
                    bem.AwardAmount = Convert.ToDecimal(dr["AwardAmount"].ToString());
                    bem.AwardDate = dr["AwardedDate"].ToString();
                    bem.AwardStatus = dr["Awardstatus"].ToString();
                    bem.LongServiceDate = dr["LongServiceDate"].ToString();
                    bem.AwardType = dr["AwardType"].ToString();
                    bem.RewardReason = dr["ReasonForNomination"].ToString();
                    bem.RedeemedDate = dr["RedeemDate"].ToString();
                    //  bem.Status = dr["Status"].ToString();
                    bem.RedeemType = dr["RedeemType"].ToString();
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        //Function to get Kind Award Redeemed and Non reedeemed
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetKindAward(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {
            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);

            objsqlparam[5] = new SqlParameter("@Tokennumber", "ALL");

            _ds = objDAL.DAL_GetData("usp_Rpt_GetAward_Kind", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.RequestId = dr["ID"].ToString();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.RecipientName = dr["RecipientName"].ToString();
                    bem.AwardName = dr["AwardName"].ToString();
                    bem.AwardAmount = Convert.ToDecimal(dr["AwardAmount"].ToString());
                    bem.AwardedDate = dr["AwardedDate"].ToString();
                    bem.AwardStatus = dr["Request_Status"].ToString();
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.Location_PSA = dr["GiverLocation"].ToString(); //Location_PSA
                    bem.DivisionName_PA = dr["GiverDivision"].ToString(); //DivisionName_PA
                    bem.OrgUnitName = dr["OrgUnitName"].ToString();
                    bem.RecipientLocation = dr["RecipientLocation"].ToString();
                    bem.RecipientDivision = dr["RecipientDivision"].ToString();
                    bem.RecipientDepartment = dr["RecipientDepartment"].ToString();
                    bem.RewardReason = dr["ReasonForNomination"].ToString();
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        //Function to get count of hifi
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetHifiReport(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {

            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();
            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", "");

            _ds = objDAL.DAL_GetData("Rpt_HifiCount", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.GiverEmailID = dr["GiverEmail"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.RecipientName = dr["RecipientName"].ToString();
                    bem.EmailID = dr["EmailId"].ToString();
                    bem.countHifi = dr["Count_Hifi"].ToString();
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.LocationName = dr["GiverLocation"].ToString(); //Location_PSA
                    bem.DivisionName_PA = dr["GiverDivision"].ToString(); //DivisionName_PA
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }
        //-----------------------------------------------------------------------------------------------------
        public List<BE_MailDetails> GetHifiReport_rec(string Business, string Division, string Location, string start, string end, string TokenNumber)
        {

            List<BE_MailDetails> mailDetailsColl = new List<BE_MailDetails>();
            SqlParameter[] objsqlparam = new SqlParameter[6];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            objsqlparam[5] = new SqlParameter("@Tokennumber", "");

            _ds = objDAL.DAL_GetData("Rpt_HifiCount_recd", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_MailDetails bem = new BE_MailDetails();
                    bem.GiverTokenNUmber = dr["GiverTokenNumber"].ToString();
                    bem.GiverName = dr["GiverName"].ToString();
                    bem.GiverEmailID = dr["GiverEmail"].ToString();
                    bem.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                    bem.RecipientName = dr["RecipientName"].ToString();
                    bem.EmailID = dr["EmailId"].ToString();
                    bem.countHifi = dr["Count_Hifi"].ToString();
                    bem.BusinessUnit = dr["BusinessUnit"].ToString();
                    bem.LocationName = dr["GiverLocation"].ToString(); //Location_PSA
                    bem.DivisionName_PA = dr["GiverDivision"].ToString(); //DivisionName_PA
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

        public List<BE_RnrReport> GetRnr_Report(string Business, string Division, string Location, string start, string end)
        {

            List<BE_RnrReport> mailDetailsColl = new List<BE_RnrReport>();
            SqlParameter[] objsqlparam = new SqlParameter[5];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            objsqlparam[0] = new SqlParameter("@Business", Business);
            objsqlparam[1] = new SqlParameter("@Division_PA", Division);
            objsqlparam[2] = new SqlParameter("@Location_PSA", Location);
            objsqlparam[3] = new SqlParameter("@StartDate", start);
            objsqlparam[4] = new SqlParameter("@EndDate", end);
            _ds = objDAL.DAL_GetData("Usp_Report_Rnr", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_RnrReport bem = new BE_RnrReport();
                    bem.TokenNumber_Property = dr["TokenNumber"].ToString();
                    bem.Number_of_friend_requests_sent_Property = dr["Number of friend requests sent"].ToString();
                    bem.Number_of_friend_requests_received_property = dr["Number of friend requests received"].ToString();
                    bem.Number_of_friends_Property = dr["Number of friends"].ToString();
                    bem.No_of_Hi5_given_property = dr["No. of Hi5 given"].ToString();
                    bem.No_of_Hi5_received_property = dr["No. of Hi5s received"].ToString();
                    bem.No_Of_Likes_Made_property = dr["No Of Likes Made"].ToString();
                    bem.No_Of_Comment_Made_Property = dr["No Of_Comment Made"].ToString();
                    mailDetailsColl.Add(bem);
                }
            }
            return mailDetailsColl;
        }

    }
}



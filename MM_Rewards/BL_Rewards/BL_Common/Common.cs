﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using MM.DAL;
using System.Net;
using System.Net.Mail;
using BE_Rewards;
using BE_Rewards.BE_Admin;
using BE_Rewards.BE_Rewards;
using System.Security.Principal;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Specialized;
using System.Web.UI;
using EvoPdf;

namespace BL_Rewards.BL_Common
{
    public class Common : System.Web.UI.Page
    {
        public string GetUserDetails(string TokenNumber)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            string Levels = "";

            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_GetUserDetailsByTokenNumber", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {

                HttpContext.Current.Session["TokenNumber"] = _ds.Tables[0].Rows[0]["TokenNumber"].ToString();
                HttpContext.Current.Session["RollUpBalance"] = _ds.Tables[0].Rows[0]["RollUpBalance"].ToString();
                HttpContext.Current.Session["TotalYearlyBudget"] = _ds.Tables[0].Rows[0]["TotalYearlyBudget"].ToString();
                HttpContext.Current.Session["AmountAdded"] = _ds.Tables[0].Rows[0]["AmountAdded"].ToString();
                HttpContext.Current.Session["UserName"] = _ds.Tables[0].Rows[0]["UserName"].ToString();
                HttpContext.Current.Session["ProcessName"] = _ds.Tables[0].Rows[0]["ProcessName"].ToString();
                HttpContext.Current.Session["DivisionCode"] = _ds.Tables[0].Rows[0]["Division_PA"].ToString();
                HttpContext.Current.Session["Division"] = _ds.Tables[0].Rows[0]["DivisionName_PA"].ToString();
                HttpContext.Current.Session["Location"] = _ds.Tables[0].Rows[0]["LocationName_PSA"].ToString();
                HttpContext.Current.Session["Levels"] = _ds.Tables[0].Rows[0]["Levels"].ToString();
                HttpContext.Current.Session["Expenses"] = _ds.Tables[0].Rows[0]["Expenses"].ToString();
                HttpContext.Current.Session["Balance"] = _ds.Tables[0].Rows[0]["Balance"].ToString();
                HttpContext.Current.Session["EmailID"] = _ds.Tables[0].Rows[0]["EmailID"].ToString();
                HttpContext.Current.Session["DateOfResign"] = _ds.Tables[0].Rows[0]["DateOfResign"].ToString();

                if (HttpContext.Current.Session["Levels"] != null)
                {
                    Levels = (string)HttpContext.Current.Session["Levels"];
                }
            }
            else
            {
                HttpContext.Current.Session["TokenNumber"] = TokenNumber;
                Levels = "-1";
            }
            return Levels;
        }



        public void SendCalendar(string strBodyPart, string strRecipient_EmailId, string strGiverEmail, string strToEmail, string strGiverName, string strReceiverName, string strCoffeeDate, DateTime dtStartDate, DateTime dtendTime)
        {
            try
            {


                int identfr = 1;
                SmtpClient sc = new SmtpClient(System.Configuration.ConfigurationSettings.AppSettings.Get("SMTP"));
                MailMessage msg = new MailMessage();
                MailAddress objGiverEmail = new MailAddress(strGiverEmail);
                MailAddress objToEmail = new MailAddress(strToEmail);
                MailAddress objToEmailReceiver = new MailAddress(strRecipient_EmailId);


                msg.From = objGiverEmail;
                msg.To.Add(objToEmail);
                msg.To.Add(objToEmailReceiver);


                msg.IsBodyHtml = true;
                msg.Subject = "Coffee With You";
                msg.Body = strBodyPart;
                string strMessage = " You are invited to have Coffee with " + strGiverName + " which is scheduled on " + strCoffeeDate + ".";

                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//MM Rewards");
                // str.AppendLine("PRODID: -//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN");

                str.AppendLine("VERSION:2.0");
                str.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");

                str.AppendLine("CALSCALE:GREGORIAN");
                str.AppendLine("METHOD:REQUEST");


                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", dtStartDate));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.Now));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", dtendTime));
                str.AppendLine(string.Format("UID:{0}", identfr));

                str.AppendLine(string.Format("DESCRIPTION:{0}", strMessage));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", strMessage));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", strGiverEmail));


                //    str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", strReceiverName, "hiiiiiiiiii"));
                //str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", strReceiverName, "212501@mahindra.com"));
                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", strReceiverName, "mahesh_dhumal@idealake.com"));

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                //str.AppendLine("Key: HKEY_CURRENT_USER\\Software\\Microsoft\\Office\\<version>\\Options\\Calendar");
                //str.AppendLine("Value name: ExtractOrganizedMeetings");
                //str.AppendLine("Value type: REG_DWORD");
                //str.AppendLine("Value: 1");

                System.Net.Mime.ContentType ct = null;




                ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");

                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);

                sc.Send(msg);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool sendMail(string strFromID, string strSenderName, string strToID, string strRecipientName, string strCCID, string strSubject, string strMessage)
        {
            bool blnSuccess = false;
            //THIS TRY CATCH IS FOR DEV SERVER UAT SINCE THERE ARE NO SMTP THERE
            try
            {
                MailMessage mailMsg = new MailMessage();
                MailAddress fromAddr = new MailAddress(strFromID, strSenderName);

                mailMsg.IsBodyHtml = true;
                mailMsg.From = fromAddr;
                mailMsg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                strToID = strToID.Replace(';', ','); //commented by megha.
               
               
                //strToID = "saurab@idealake.com"; //for testing 
                //strToID = "kiran_mahale@idealake.com";
                mailMsg.To.Add(strToID);
                mailMsg.Subject = strSubject;
                mailMsg.Body = strMessage;

                //strCCID = "saurab@idealake.com"; //for testing 
                //strCCID = "aaarti_gaikwad@idealake.com";

                // by mahesh 

                //if (!string.IsNullOrEmpty(strCCID))
                //{
                //    string[] strCC = strCCID.Split(';');
                //    foreach (string str_CCId in strCC)
                //    {
                //        if (str_CCId.Trim() != string.Empty)
                //        {
                //            mailMsg.CC.Add(str_CCId);
                //        }
                //    }
                //}

                string SMTPServer = ConfigurationSettings.AppSettings.Get("SMTP");
                SmtpClient objSmtpClient = new SmtpClient(SMTPServer);
                try
                {
                   objSmtpClient.Send(mailMsg);

                    //To Log sent mails and there Details
                    if (!string.IsNullOrEmpty(Server.MapPath(ConfigurationSettings.AppSettings.Get("MailLogs"))))
                    {
                        string LogFileName = Path.Combine(Server.MapPath(ConfigurationSettings.AppSettings["MailLogs"].ToString()), DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt");
                        if (!Directory.Exists(Path.GetDirectoryName(LogFileName)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(LogFileName));
                        }
                        FileStream logFileStream = null;

                        try
                        {
                            string FilePath = Path.GetDirectoryName(LogFileName);

                            if (File.Exists(LogFileName) == false)
                            {
                                // If file doexs not exists create a new file at the specified path
                                logFileStream = File.Open(LogFileName, FileMode.Create, FileAccess.ReadWrite);
                            }
                            else
                            {
                                logFileStream = File.Open(LogFileName, FileMode.Append, FileAccess.Write);
                            }
                        }
                        finally
                        {
                            if (logFileStream != null)
                            {
                                using (StreamWriter MailLogWriter = new StreamWriter(logFileStream))
                                {
                                    MailLogWriter.WriteLine("-- -- -- -- -- -- -- -- -- -- -- -- ");
                                    MailLogWriter.WriteLine("Subject: " + strSubject);
                                    MailLogWriter.WriteLine("Date: " + DateTime.Now.ToString() + ", SenderEmailID: " + strFromID + ", Sender Name: " + strSenderName + ", ReceiverEmalID: " + strToID + " Receiver: " + strRecipientName);
                                    MailLogWriter.WriteLine("-- -- -- -- -- -- -- -- -- -- -- -- ");
                                }
                                logFileStream.Close();
                            }
                        }
                        //END
                    }
                    blnSuccess = true;
                }
                catch (Exception GeneralEx)
                {
                    throw new ArgumentException(GeneralEx.Message);
                }
                finally
                {
                    objSmtpClient = null;
                    mailMsg = null;
                    fromAddr = null;
                }
            }
            catch (Exception ex)
            {
                //THIS TRY CATCH IS FOR DEV SERVER UAT SINCE THERE ARE NO SMTP THERE
            }
            return blnSuccess;

        }

        internal DataSet GetMailDetails(string ActivityType, string Location)
        {
            SqlParameter[] objsqlparam = new SqlParameter[2];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@ActivityType", ActivityType);
            objsqlparam[1] = new SqlParameter("@Location", Location);
            _ds = objDAL.DAL_GetData("usp_GetMailDetailsFromActivity", objsqlparam);

            return _ds;
        }

        //public DateTime GetUserDetails(string windowsUserName)
        //{
        //    throw new NotImplementedException();
        //}

        public int IsUserExist(string TokenNumber)
        {
            try
            {
                object _stat;
                int IsExist = 0;
                DAL_Common objDAL = new DAL_Common();
                DataSet _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDAL.DAL_GetData("IsUserExist", objsqlparam);
                IsExist = Convert.ToInt32(_ds.Tables[0].Rows[0][0].ToString());
                return IsExist;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //To get PageName from TokenNumber ,
        //its to  give Access to Pages according to Roles

        public List<BE_Common> GetPageNameFromTokenNumber(string TokenNumber)
        {
            BE_Common objBECommon = new BE_Common();
            BE_CommonColl objBECommonColl = new BE_CommonColl();
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_GetPageURLFromRoleID", objsqlparam);
            foreach (DataRow _dr in _ds.Tables[0].Rows)
            {
                objBECommon = new BE_Common();
                objBECommon.PageName = _dr["PageName"].ToString();
                objBECommon.MenuName = _dr["MenuName"].ToString();
                objBECommon.ParentMenuName = _dr["ParentName"].ToString();
                objBECommonColl.Add(objBECommon);
            }
            return objBECommonColl.CommonCollection;
        }

        public List<BE_Common> GetUserMenu(string TokenNumber)
        {
            BE_Common objBECommon = new BE_Common();
            BE_CommonColl objBECommonColl = new BE_CommonColl();
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);

            _ds = objDAL.DAL_GetData("usp_GetUserMenu", objsqlparam);
            foreach (DataRow _dr in _ds.Tables[0].Rows)
            {
                objBECommon = new BE_Common();
                objBECommon.PageName = _dr["PageName"].ToString();
                objBECommon.MenuName = _dr["MenuName"].ToString();
                objBECommon.ParentMenuName = _dr["ParentName"].ToString();
                objBECommonColl.Add(objBECommon);
            }
            return objBECommonColl.CommonCollection;
        }

        public static void CheckPageAccess()
        {
            try
            {
                BE_Common objBECommonForPageAccess = new BE_Common();
                List<BE_Common> objBECommonForPageAccessColl = new List<BE_Common>();
                Common objBLCommonForPageAccess = new Common();
                objBECommonForPageAccessColl = objBLCommonForPageAccess.GetPageNameFromTokenNumber((string)HttpContext.Current.Session["TokenNumber"]);

                if (objBECommonForPageAccessColl.Exists(obj => obj.PageName.ToUpper() == HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"].ToString().ToUpper()))
                {
                    //Do Nothing

                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Web_Pages/HomePage.aspx");
                }

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Web_Pages/HomePage.aspx");
            }
        }

        public BE_MailDetails GetMailDetails(int ActivityID, string AwardID)
        {
            try
            {
                DataSet _ds = new DataSet();
                DAL_Common objDAL = new DAL_Common();
                BE_MailDetails objBEMailDetails = new BE_MailDetails();
                SqlParameter[] objsqlparam = new SqlParameter[2];
                objsqlparam[0] = new SqlParameter("@AwardID", AwardID);
                objsqlparam[1] = new SqlParameter("@ActivityID", ActivityID);
                _ds = objDAL.DAL_GetData("usp_MailDetailsByActivity_View", objsqlparam);

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    objBEMailDetails = new BE_MailDetails();
                    objBEMailDetails.FirstName = _dr["FirstName"].ToString();
                    objBEMailDetails.LastName = _dr["LastName"].ToString();
                    objBEMailDetails.AwardType = _dr["AwardType"].ToString();
                    objBEMailDetails.AwardAmount = Convert.ToDecimal(_dr["AwardAmount"].ToString());
                    objBEMailDetails.AwardName = _dr["Name"].ToString();
                    objBEMailDetails.EmailID = _dr["EmailID"].ToString();
                    objBEMailDetails.Fromid = _dr["FromID"].ToString();
                    objBEMailDetails.Fromname = _dr["FromName"].ToString();
                    objBEMailDetails.Toid = _dr["ToID"].ToString();
                    objBEMailDetails.Ccid = _dr["CCID"].ToString();
                    objBEMailDetails.Subject = _dr["Subject"].ToString();
                    objBEMailDetails.StartDate = _dr["EmailedDate"].ToString();
                    objBEMailDetails.EndDate = _dr["ExpiryDate"].ToString();
                    objBEMailDetails.RewardReason = _dr["ReasonForNomination"].ToString();

                    objBEMailDetails.GiverEmailID = _dr["GiverEmailID"].ToString();
                    objBEMailDetails.GiverName = _dr["GiverName"].ToString();
                    objBEMailDetails.GiverTokenNUmber = _dr["GiverTokenNumber"].ToString();
                    objBEMailDetails.Location_PSA = _dr["Location_PSA"].ToString();
                    objBEMailDetails.BusinessUnit = _dr["BusinessUnit"].ToString();
                    objBEMailDetails.DivisionName_PA = _dr["BusinessUnit"].ToString();

                }
                return objBEMailDetails;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static void CheckUserLoggedIn(bool popup = false)
        {
            if (HttpContext.Current.Session["TokenNumber"] == null)
            {
                if (!popup)
                {
                    CheckLoginType();
                    //HttpContext.Current.Server.Transfer("~/Web_Pages/HomePage.aspx");
                }
                else
                {
                    HttpContext.Current.Response.Write("Your Session has expired. Kindly refresh the Redemption Page and click on the Upload Link again. <br/><br/><a href='JavaScript:window.close();'> Close This Window </a>");
                    HttpContext.Current.Response.End();
                }
            }
        }

        private static Boolean CheckLoginType()
        {
            string windowsUserName = WindowsIdentity.GetCurrent().Name;

            //if (Session["windowsUserName"]!=null)
            //{
                
            //}   

            if (HttpContext.Current.Session["windowsUserName"] != null)
            {
                windowsUserName = Convert.ToString(HttpContext.Current.Session["windowsUserName"]);
            } 

            if (!string.IsNullOrEmpty(windowsUserName))
            {
                //Single Sign on
                windowsUserName = windowsUserName.Substring(windowsUserName.LastIndexOf("\\") + 1);

                int IsExist = new Common().IsUserExist(windowsUserName);
                if (IsExist == 0)
                {
                    //Single Sign On Failed
                    HttpContext.Current.Server.Transfer("~/Web_Pages/HomePage.aspx");

                    //light.Style.Add("display", "block");
                    //fade.Style.Add("display", "block");
                    //btnLogin.OnClientClick += "return btnLogin_ClientClick('" + txtLoginTokenNumber.ClientID + "','" + txtPassWord.ClientID + "')";
                }
                else
                {
                    HttpContext.Current.Session["TokenNumber"] = windowsUserName;
                }


            }


            return true;
        }

        public int CheckEligibility_GiveAwards(string TokenNumber)
        {
            try
            {
                object _stat;
                int IsExist = 0;
                DAL_Common objDAL = new DAL_Common();
                DataSet _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDAL.DAL_GetData("usp_CheckEligibility_GiveAwards", objsqlparam);
                IsExist = Convert.ToInt32(_ds.Tables[0].Rows[0][0].ToString());
                return IsExist;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //To Load Division DropDown For Report pages
        public List<BE_RecipientAward> LoadReportDivisionDropDown(out bool IsRnr, string business, string TokenNumber = "")
        {
            DAL_Common objDAL = new DAL_Common();
            BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();
            DataSet _ds = new DataSet();
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            objsqlparam[1] = new SqlParameter("@Business", business);
            objsqlparam[2] = new SqlParameter("@rnr", SqlDbType.Bit, 1, ParameterDirection.Output, false, 10, 10, "", DataRowVersion.Default, 0);

            _ds = objDAL.DAL_GetData("usp_Get_DivForReport", objsqlparam);
            foreach (DataRow dr in _ds.Tables[0].Rows)
            {
                BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                objBERecipientAward.DivisionCode_PA = dr["Division_PA"].ToString();
                objBERecipientAward.DivisionName_PA = dr["DivisionName_PA"].ToString();
                objBERecipientAwardColl.Add(objBERecipientAward);
            }
            IsRnr = Convert.ToBoolean(objsqlparam[2].Value);
            return objBERecipientAwardColl;
        }

        //To Load Location DropDown for report page But According to Division ,Location get Loaded 
        public List<BE_RecipientAward> LoadLocationDropDown(string TokenNumber = "", string Division = "")
        {
            DAL_Common objDAL = new DAL_Common();
            BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();
            DataSet _ds = new DataSet();

            SqlParameter[] objsqlparam = new SqlParameter[2];
            objsqlparam[0] = new SqlParameter("@Division", Division);
            objsqlparam[1] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_Get_LocForDiv", objsqlparam);

            foreach (DataRow dr in _ds.Tables[0].Rows)
            {
                BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                objBERecipientAward.LocationCode_PSA = dr["Location_PSA"].ToString();
                objBERecipientAward.LocationName_PSA = dr["LocationName_PSA"].ToString();
                objBERecipientAwardColl.Add(objBERecipientAward);
            }
            return objBERecipientAwardColl;
        }

        //To Load Department DropDown for report pages
        public List<BE_RecipientAward> LoadDepartmentDropDown(string Loc, string Div, string TokenNumber = "")
        {
            DAL_Common objDAL = new DAL_Common();
            BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();
            DataSet _ds = new DataSet();
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@Location_PSA", Loc);
            objsqlparam[1] = new SqlParameter("@DivCode", Div);
            objsqlparam[2] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_Get_OrgUnitNamesForLocAndDiv", objsqlparam);
            foreach (DataRow dr in _ds.Tables[0].Rows)
            {
                BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                objBERecipientAward.DivisionCode_PA = dr["OrgUnitCode"].ToString();
                objBERecipientAward.DivisionName_PA = dr["OrgUnitName"].ToString(); // here DivisionName_PA is used for holding Department NAMe
                objBERecipientAwardColl.Add(objBERecipientAward);
            }
            return objBERecipientAwardColl;
        }

        public List<BE_RecipientAward> LoadDivisionDropDown()
        {
            DAL_Common objDAL = new DAL_Common();
            BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();
            DataSet _ds = new DataSet();

            _ds = objDAL.DAL_GetData("usp_Mas_Division_View");
            foreach (DataRow dr in _ds.Tables[0].Rows)
            {
                BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                objBERecipientAward.DivisionCode_PA = dr["Division_PA"].ToString();
                objBERecipientAward.DivisionName_PA = dr["DivisionName_PA"].ToString();
                objBERecipientAwardColl.Add(objBERecipientAward);
            }

            return objBERecipientAwardColl;
        }

        //This Function is used to find whether User has a right to Administrator and C&B
        //If Yes then return 0 else 1, acc to tht dropdown of Report is feeled...
        public int GetRoles(string TokenNumber)
        {
            try
            {
                object _stat;
                int IsExist = 0;
                DAL_Common objDAL = new DAL_Common();
                DataSet _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDAL.DAL_GetData("usp_GetRRfromTokenNumber", objsqlparam);
                IsExist = Convert.ToInt32(_ds.Tables[0].Rows[0][0].ToString());
                return IsExist;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int GetBudgetExist_TokenNumber(string TokenNumber)
        {
            try
            {
                int IsExist = 0;
                DAL_Common objDAL = new DAL_Common();
                DataSet _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDAL.DAL_GetData("usp_ISBudgetExist_TokenNumber", objsqlparam);
                IsExist = Convert.ToInt32(_ds.Tables[0].Rows[0][0].ToString());
                return IsExist;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //Added By Archana K . On 26-08-2016
        public Int64 GetCashAwardExist_TokenNumber(string TokenNumber)
        {
            try
            {
                Int64 IsExist = 0;
                DAL_Common objDAL = new DAL_Common();
                DataSet _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDAL.DAL_GetData("usp_ISCashAwardExist", objsqlparam);
                IsExist = Convert.ToInt64(_ds.Tables[0].Rows[0][0].ToString());
                return IsExist;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //Added By Archana K . On 27-08-2016
        public void UpdateData_CashAwardUser(string TokenNumber)
        {
            try
            {
                DAL_Common objDAL = new DAL_Common();
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                objDAL.DAL_SaveData("usp_Update_CashAwardUser_IsDwn", objSqlParam, 'N');
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public BE_MailDetails GetUserDetailsByTokenNumber(string TokenNumber)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            BE_MailDetails objBEMailDetails = new BE_MailDetails();


            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_GetMailDetail_TokenNumber", objsqlparam);
            foreach (DataRow _dr in _ds.Tables[0].Rows)
            {
                objBEMailDetails = new BE_MailDetails();
                objBEMailDetails.RecipientName = _dr["FullName"].ToString();
                objBEMailDetails.Toid = _dr["EmailID"].ToString();


            }
            return objBEMailDetails;


        }

        /*Added by Kiran on 30 july 2013*/
        public DataSet GetAwardAmount(string AwardType)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@AwardType", AwardType);
            _ds = objDAL.DAL_GetData("usp_Get_AwardAmount", objsqlparam);

            return _ds;
        }

        //adding Post Method

        public void RedirectAndPOST(System.Web.UI.Page page, string destinationUrl, System.Collections.Specialized.NameValueCollection data)
        {
            //Prepare the Posting form
            string strForm = PreparePOSTForm(destinationUrl, data);
            //Add a literal control the specified page holding 
            //the Post Form, this is to submit the Posting form with the request.
            page.Controls.Add(new LiteralControl(strForm));
        }

        private String PreparePOSTForm(string url, NameValueCollection data)
        {
            //Set a name for the form
            string formID = "PostForm";
            //Build the form using the specified data to be posted.
            StringBuilder strForm = new StringBuilder();
            strForm.Append("<form id=\"" + formID + "\" name=\"" +
                           formID + "\" action=\"" + url +
                           "\" method=\"POST\">");

            foreach (string key in data)
            {
                strForm.Append("<input type=\"hidden\" name=\"" + key +
                               "\" value=\"" + data[key] + "\">");
            }

            strForm.Append("</form>");
            //Build the JavaScript which will do the Posting operation.
            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language='javascript'>");
            strScript.Append("var v" + formID + " = document." +
                             formID + ";");
            strScript.Append("v" + formID + ".submit();");
            strScript.Append("</script>");
            //Return the form and the script concatenated.
            //(The order is important, Form then JavaScript)
            return strForm.ToString() + strScript.ToString();
        }

        #region Sales Award

        public string GetSalesUserDetails(string TokenNumber)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            string Levels = "";

            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_GetSalesUserDetailsByTokenNumber", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {

                HttpContext.Current.Session["TokenNumber"] = _ds.Tables[0].Rows[0]["TokenNumber"].ToString();
                HttpContext.Current.Session["UserName"] = _ds.Tables[0].Rows[0]["UserName"].ToString();
                HttpContext.Current.Session["ProcessName"] = _ds.Tables[0].Rows[0]["ProcessName"].ToString();
                HttpContext.Current.Session["DivisionCode"] = _ds.Tables[0].Rows[0]["Division_PA"].ToString();
                HttpContext.Current.Session["Division"] = _ds.Tables[0].Rows[0]["DivisionName_PA"].ToString();
                HttpContext.Current.Session["Location"] = _ds.Tables[0].Rows[0]["LocationName_PSA"].ToString();
                HttpContext.Current.Session["Levels"] = _ds.Tables[0].Rows[0]["Levels"].ToString();
                HttpContext.Current.Session["Expenses"] = _ds.Tables[0].Rows[0]["Expenses"].ToString();
                HttpContext.Current.Session["Balance"] = _ds.Tables[0].Rows[0]["Balance"].ToString();
                HttpContext.Current.Session["EmailID"] = _ds.Tables[0].Rows[0]["EmailID"].ToString();

                if (HttpContext.Current.Session["Levels"] != null)
                {
                    Levels = (string)HttpContext.Current.Session["Levels"];
                }
            }
            else
            {
                HttpContext.Current.Session["TokenNumber"] = TokenNumber;
                Levels = "-1";
            }
            return Levels;
        }

        public int GetSalesBudgetExist_TokenNumber(string TokenNumber)
        {
            try
            {
                int IsExist = 0;
                DAL_Common objDAL = new DAL_Common();
                DataSet _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDAL.DAL_GetData("usp_ISSalesBudgetExist_TokenNumber", objsqlparam);
                IsExist = Convert.ToInt32(_ds.Tables[0].Rows[0][0].ToString());
                return IsExist;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        public List<BE_SalesRecipientAward> LoadSalesReportDivisionDropDown()
        {

            DAL_Common objDAL = new DAL_Common();

            BE_SalesRecipientAwardColl objBESalesRecipientAwardColl = new BE_SalesRecipientAwardColl();

            DataSet _ds = new DataSet();

            _ds = objDAL.DAL_GetData("usp_Get_DivisionForSalesReport");

            foreach (DataRow dr in _ds.Tables[0].Rows)
            {

                BE_SalesRecipientAward objBESalesRecipientAward = new BE_SalesRecipientAward();

                // objBERecipientAward.DivisionCode_PA = dr["ID"].ToString();

                objBESalesRecipientAward.DivisionName_PA = dr["SALES_DIVISION"].ToString();

                objBESalesRecipientAwardColl.Add(objBESalesRecipientAward);

            }

            return objBESalesRecipientAwardColl;

        }

        #endregion

        //To Load Business DropDown For Report pages

        public List<BE_RecipientAward> LoadReportBusinessDropDown()
        {

            DAL_Common objDAL = new DAL_Common();

            BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();

            DataSet _ds = new DataSet();

            _ds = objDAL.DAL_GetData("usp_Get_BusinessUnitForReport");

            foreach (DataRow dr in _ds.Tables[0].Rows)
            {

                BE_RecipientAward objBERecipientAward = new BE_RecipientAward();

                // objBERecipientAward.DivisionCode_PA = dr["ID"].ToString();

                objBERecipientAward.BusinessUnit = dr["BusinessUnit"].ToString();

                objBERecipientAwardColl.Add(objBERecipientAward);

            }

            return objBERecipientAwardColl;

        }

        public string GetUserEmailId(string TokenNumber)
        {
            string EmaildId = "";
            SqlParameter[] objsqlparam = new SqlParameter[1];
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();

            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_GetUserEmailIdByTokenNumber", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                HttpContext.Current.Session["KTokenNumber"] = _ds.Tables[0].Rows[0]["TokenNumber"].ToString();
                HttpContext.Current.Session["KEmailID"] = _ds.Tables[0].Rows[0]["EmailID"].ToString();
                HttpContext.Current.Session["KFirstName"] = _ds.Tables[0].Rows[0]["FirstName"].ToString();
                HttpContext.Current.Session["KLastName"] = _ds.Tables[0].Rows[0]["LastName"].ToString();
            }
            return EmaildId;
        }
        public DataSet GetLeaderDetails(string TokenNumber)
        {
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDAL.DAL_GetData("usp_Leadername", objsqlparam);
            return _ds;
        }
        public DataSet LoadFYear()
        {
            DataSet _ds = new DataSet();
            DAL_Common objDAL = new DAL_Common();
            _ds = objDAL.DAL_GetData("uspLoadFYear");
            return _ds;
        }

        //public void CreateCertificate(int id, string awardType, string cardtype, string giverToken,string recipientToken, string giverName, string recipientName,string message, string awardedon) 
        //{
        //    try
        //    {
        //        //string giverName = GetNameByToken(giverToken);
        //        //string recipientName = GetNameByToken(recipientToken);
                
        //        string strFileName = string.Empty;
        //        if (cardtype.Contains("TC1"))
        //        { strFileName = "card1.txt"; }
        //        if (cardtype.Contains("TC2"))
        //        { strFileName = "card2.txt"; }
        //        if (cardtype.Contains("TC3"))
        //        { strFileName = "card3.txt"; }
        //        if ((cardtype.Contains("ANL")) || (cardtype.Contains("AT")) || (cardtype.Contains("DPC")))
        //        { strFileName = "EcardDesign.txt"; }
        //        if ((cardtype.Contains("Spot")) || (cardtype.Contains("SPOT")) || (cardtype.Contains("Excellerator")))
        //        {
        //            strFileName = "SpotAwardCertificate.txt";
        //        }

        //        StreamReader sr = new StreamReader(Server.MapPath("~/MailDetails/" + strFileName));
        //        StringBuilder sb = new StringBuilder();

        //        string BodyPart = sr.ReadToEnd();
        //        sr.Close();
        //        if (cardtype.Contains("ANL"))
        //        {
        //            BodyPart = BodyPart.Replace("#fist", "ACCEPTING");
        //            BodyPart = BodyPart.Replace("#second", "NO");
        //            BodyPart = BodyPart.Replace("#third", "LIMITS");
        //        }
        //        if (cardtype.Contains("AT"))
        //        {
        //            BodyPart = BodyPart.Replace("#fist", "ALTERNATIVE");
        //            BodyPart = BodyPart.Replace("#second", "");
        //            BodyPart = BodyPart.Replace("#third", "THINKING");
        //        }

        //        if (cardtype.Contains("DPC"))
        //        {
        //            BodyPart = BodyPart.Replace("#fist", "DRIVING");
        //            BodyPart = BodyPart.Replace("#second", "POSITIVE");
        //            BodyPart = BodyPart.Replace("#third", "CHANGE");
        //        }
        //        BodyPart = BodyPart.Replace("#Name", recipientName);
        //        BodyPart = BodyPart.Replace("#Message", message);

        //        if ((awardType.Contains("Spot")) || (awardType.Contains("SPOT")) || (awardType.Contains("Excellerator")) || (awardType.Contains("Coffee With You")))
        //        {
        //            strFileName = "SpotAwardCertificate.txt";
        //            StreamReader reader = new StreamReader(Server.MapPath("~/MailDetails/" + strFileName));
        //            //sr = new StreamReader(path + strFileName);

        //            sb = new StringBuilder();

        //            BodyPart = reader.ReadToEnd();
        //            reader.Close();
        //            BodyPart = BodyPart.Replace("#name", recipientName);
        //            BodyPart = BodyPart.Replace("#hodmsg", message);
        //            BodyPart = BodyPart.Replace("#awardeddate", awardedon);
        //            BodyPart = BodyPart.Replace("#GiverToken", giverName);
        //            if (cardtype.Contains("SPOT") || cardtype.Contains("Spot"))
        //            {
        //                BodyPart = BodyPart.Replace("#awrd", awardType.ToUpper()+" AWARD");//"SPOT AWARD");                       
        //            }
        //            else if (cardtype.Contains("EXCELLERATOR") || cardtype.Contains("Excellerator"))
        //            {
        //                BodyPart = BodyPart.Replace("#awrd", awardType.ToUpper() + " AWARD");

        //            }
        //            else if (cardtype.Contains("Coffee With You"))
        //            {
        //                AwardType = cardtype + " AWARD";
        //                BodyPart = BodyPart.Replace("#awrd", AwardType.ToUpper());

        //            }
        //        }


        //        HtmlToImageConverter htmlToImageConverter = new HtmlToImageConverter();
        //        htmlToImageConverter.HtmlViewerWidth = 870;
        //        htmlToImageConverter.HtmlViewerHeight = 700;
        //        htmlToImageConverter.ConversionDelay = 0;
        //        System.Drawing.Image[] imageTiles = null;
        //        string baseUrl = "";

        //        imageTiles = htmlToImageConverter.ConvertHtmlToImageTiles(BodyPart, baseUrl);
        //        System.Drawing.Image outImage = imageTiles[0];

        //        Bitmap bmp = new Bitmap(outImage);
        //        string imageName = ConfigurationSettings.AppSettings["EcardImg_Path"].ToString() + "Spot" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".jpg";

        //        //                string imageName = ConfigurationSettings.AppSettings["EcardImg_Path"].ToString() + awardName + DateTime.Now.ToString("ddMMyyyyhh")+"_"+recipientToken + ".jpg";
        //        bmp.Save(imageName);
        //        bmp.Dispose();

        //        // crop image ProcessAndSendMails save
        //        System.Drawing.Image img = System.Drawing.Image.FromFile(imageName);
        //        Bitmap bmpImage = new Bitmap(outImage);
        //        Bitmap bmpCrop = bmpImage.Clone(new Rectangle(17, 17, 817, 660), bmpImage.PixelFormat);
        //        //string imageNameBody =awardName + DateTime.Now.ToString("ddMMyyyyhh") + "_" + recipientToken + ".jpg"; ;
        //        string imageNameBody = "SpotCertificate" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".jpg";

        //        string EcardCertificate = ConfigurationSettings.AppSettings["EcardImg_Path"].ToString() + imageNameBody;
        //        bmpCrop.Save(EcardCertificate);
        //        bmpCrop.Dispose();


        //        //code added by suraj to send document and data to SAP on 7th April 2020

        //        //apicall(imageNameBody, id, AwardType, recipientToken, giverToken, giverName, message, awardName, awardedonAPI);

        //        //ThrowAlertMessage("E-Card send Successfully");

        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //}
        /*public void ExportExcelFromGrid(GridView grd)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string filename = "attachment; filename=DataN-1_" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwritter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", filename);
            grd.GridLines = GridLines.Both;
            grd.HeaderStyle.Font.Bold = true;
            grd.RenderControl(htmltextwritter);
            Response.Write(strwritter.ToString());
            Response.End();
        }*/
        public void ExportExcelFromDatatable(DataTable dt)
        {
            try
            {
                string attachment = "attachment; filename=Data_" + DateTime.Now + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    Response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {

            }

        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }

    }
}
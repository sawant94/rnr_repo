﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;

namespace BL_Rewards.BL_Rewards
{
    public sealed class BL_MonthlyMilestone
    {
        DAL_Common objDALCommon = new DAL_Common();
        DataSet ds;
        DataTable dt;
        public DataTable mmilestonedt(string startdate, string enddate)
        {
            ds = new DataSet();
            dt = new DataTable();
            SqlParameter[] objSqlParam;
            try
            {
                if (startdate != null)
                {
                    objSqlParam = new SqlParameter[2];
                    //objSqlParam[0] = new SqlParameter("@month", month);
                    //objSqlParam[1] = new SqlParameter("@year", year);
                    objSqlParam[0] = new SqlParameter("@StartDate", startdate);
                    objSqlParam[1] = new SqlParameter("@EndDate", enddate);
                    ds = objDALCommon.DAL_GetData("Rpt_MonthMileStone", objSqlParam);
                }
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

            return dt;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Rewards;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Data;

namespace BL_Rewards.BL_Rewards
{
    public sealed class BL_SalesRecipientAward
    {
        DataSet _ds;
        BE_SalesRecipientAwardColl objBESalesRecipientAwardColl;
        DAL_Common objDALCommon;

        //To Get  RecipientData List , against TokenNumber or Name
        //public BE_SalesRecipientAwardColl GetRecipientList(string RecipientName, string HODTokenNumber, int AwardId)
        //{
        //    try
        //    {
        //        _ds = new DataSet();
        //        objDALCommon = new DAL_Common();
        //        objBESalesRecipientAwardColl = new BE_SalesRecipientAwardColl();

        //        SqlParameter[] objsqlparam = new SqlParameter[3];
        //        objsqlparam[0] = new SqlParameter("@RecipientName", RecipientName);
        //        objsqlparam[1] = new SqlParameter("@HODTokenNumber", HODTokenNumber);
        //        if (AwardId != 0)
        //        {
        //            objsqlparam[2] = new SqlParameter("@AwardID", AwardId);
        //            _ds = objDALCommon.DAL_GetData("usp_GetRecipentDetailsByName", objsqlparam);
        //        }
        //        else
        //        {
        //            _ds = objDALCommon.DAL_GetData("usp_GetRecipentDetailsByNameForEcard", objsqlparam);

        //        }

        //        foreach (DataRow _dr in _ds.Tables[0].Rows)
        //        {
        //            BE_SalesRecipientAward objBESalesRecipientAward = new BE_SalesRecipientAward();
        //            objBESalesRecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
        //            objBESalesRecipientAward.FirstName = _dr["FirstName"].ToString();
        //            objBESalesRecipientAward.LastName = _dr["LastName"].ToString();
        //            objBESalesRecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
        //            objBESalesRecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
        //            objBESalesRecipientAward.Designation = _dr["Designation"].ToString();
        //            objBESalesRecipientAward.EmailID = _dr["EmailID"].ToString();
        //            objBESalesRecipientAward.LocationName_PSA = _dr["LocationName_PSA"].ToString();
        //            objBESalesRecipientAward.OrgUnitName = _dr["OrgUnitName"].ToString();
        //            objBESalesRecipientAward.ProcessName = _dr["ProcessName"].ToString();
        //            objBESalesRecipientAward.Level = _dr["Levels"].ToString();

        //            objBESalesRecipientAwardColl.Add(objBESalesRecipientAward);
        //        }
        //        return objBESalesRecipientAwardColl;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }
        //}
        public BE_SalesRecipientAwardColl GetRecipientList(string RecipientName, string HODTokenNumber)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                objBESalesRecipientAwardColl = new BE_SalesRecipientAwardColl();

                SqlParameter[] objsqlparam = new SqlParameter[2];
                objsqlparam[0] = new SqlParameter("@RecipientName", RecipientName);
                objsqlparam[1] = new SqlParameter("@HODTokenNumber", HODTokenNumber);
                //if (AwardId != 0)
                //{
                //    objsqlparam[2] = new SqlParameter("@AwardID", AwardId);
                //    _ds = objDALCommon.DAL_GetData("usp_GetRecipentDetailsByName", objsqlparam);
                //}
                //else
                //{
                _ds = objDALCommon.DAL_GetData("usp_GetSalesRecipentDetailsByNameForEcard", objsqlparam);

                //}

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_SalesRecipientAward objBESalesRecipientAward = new BE_SalesRecipientAward();
                    objBESalesRecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
                    objBESalesRecipientAward.FirstName = _dr["FirstName"].ToString();
                    objBESalesRecipientAward.LastName = _dr["LastName"].ToString();
                    objBESalesRecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                    objBESalesRecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBESalesRecipientAward.Designation = _dr["Designation"].ToString();
                    objBESalesRecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBESalesRecipientAward.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBESalesRecipientAward.OrgUnitName = _dr["OrgUnitName"].ToString();
                    objBESalesRecipientAward.ProcessName = _dr["ProcessName"].ToString();
                    objBESalesRecipientAward.Level = _dr["Levels"].ToString();

                    objBESalesRecipientAwardColl.Add(objBESalesRecipientAward);
                }
                return objBESalesRecipientAwardColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //To Get Recipient Data, AfterSelecting specific Value
        public BE_SalesRecipientAward GetRecipientDetails(string TokenNumber)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_SalesRecipientAward objBESalesRecipientAward = new BE_SalesRecipientAward();
                DataTable _dt = new DataTable();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_SalesRecipientAwards_View", objsqlparam);
                _dt = _ds.Tables[0];
                foreach (DataRow _dr in _dt.Rows)
                {
                    objBESalesRecipientAward.UserName = _dr["UserName"].ToString();
                    objBESalesRecipientAward.ProcessName = _dr["ProcessName"].ToString();
                    objBESalesRecipientAward.LocationCode_PSA = _dr["Location_PSA"].ToString();
                    objBESalesRecipientAward.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBESalesRecipientAward.OrgUnitName = _dr["OrgUnitName"].ToString();
                    objBESalesRecipientAward.Designation = _dr["Designation"].ToString();
                    objBESalesRecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBESalesRecipientAward.DivisionCode_PA = _dr["Division_PA"].ToString();
                    objBESalesRecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                    objBESalesRecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBESalesRecipientAward.Level = _dr["Levels"].ToString();
                }
                return objBESalesRecipientAward;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        // Added by gaurav
        public BE_SalesRecipientAwardColl GetEmpDetails(String TokenNumber)
        {
            _ds = new DataSet();
            objDALCommon = new DAL_Common();
            BE_SalesRecipientAward objBESalesRecipientAward = new BE_SalesRecipientAward();
            objBESalesRecipientAwardColl = new BE_SalesRecipientAwardColl();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            try
            {

                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_RecipientDetails_View", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    objBESalesRecipientAward.FirstName = _dr["FirstName"].ToString();
                    objBESalesRecipientAward.LastName = _dr["LastName"].ToString();
                    objBESalesRecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
                    objBESalesRecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBESalesRecipientAwardColl.Add(objBESalesRecipientAward);
                    objBESalesRecipientAward = new BE_SalesRecipientAward();
                }
                return objBESalesRecipientAwardColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            finally
            {
                if (objsqlparam != null)
                {
                    objsqlparam = null;
                }
                if (_ds != null)
                {
                    _ds = null;
                }
                if (objDALCommon != null)
                {
                    objDALCommon = null;
                }

                if (objBESalesRecipientAward != null)
                {
                    objBESalesRecipientAward = null;
                }
            }
        }


        //public int SaveData(BE_SalesRecipientAward objBESalesRecipientAward, out string AwardIDR)
        //{
        //    try
        //    {
        //        int _status = 0;
        //        AwardIDR = "";
        //        object _stat;
        //        SqlParameter[] objsqlparam = new SqlParameter[13];
        //        objsqlparam[0] = new SqlParameter("@TokenNumber", objBESalesRecipientAward.TokenNumber);
        //        objsqlparam[1] = new SqlParameter("@Location", objBESalesRecipientAward.LocationCode_PSA);
        //        objsqlparam[2] = new SqlParameter("@DepartmentName", objBESalesRecipientAward.OrgUnitName);
        //        objsqlparam[3] = new SqlParameter("@Designation", objBESalesRecipientAward.Designation);
        //        objsqlparam[4] = new SqlParameter("@MasAwardID", objBESalesRecipientAward.MasAwardID);
        //        objsqlparam[5] = new SqlParameter("@AwardType", objBESalesRecipientAward.AwardType);
        //        objsqlparam[6] = new SqlParameter("@AwardAmount", objBESalesRecipientAward.AwardAmount);
        //        objsqlparam[7] = new SqlParameter("@Division", objBESalesRecipientAward.DivisionCode_PA);
        //        objsqlparam[8] = new SqlParameter("@ReasonForNomination", objBESalesRecipientAward.ReasonForNomination);
        //        objsqlparam[9] = new SqlParameter("@LoginID", objBESalesRecipientAward.GiverTokenNumber);
        //        objsqlparam[10] = new SqlParameter("@AwardIDAuto", SqlDbType.VarChar, 100, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Default, "");

        //        //added by gaurav

        //        objsqlparam[11] = new SqlParameter("@CoffeeDate", objBESalesRecipientAward.CoffeeDate);
        //        objsqlparam[12] = new SqlParameter("@CCEmailID", objBESalesRecipientAward.CCEmailID);


        //        objDALCommon = new DAL_Common();
        //        _stat = objDALCommon.DAL_SaveData("usp_SalesRecipientAwards_Write", objsqlparam, 'S');
        //        _status = _stat == null ? 0 : Convert.ToInt32(_stat);

        //        AwardIDR = (string)objsqlparam[10].Value;
        //        return _status;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }
        //}

        //public int SaveData(BE_SalesAwardTypeManagement objBESalesRecipientAward, out string AwardIDR)
        public int SaveData(BE_SalesRecipientAward objBESalesRecipientAward, out string AwardIDR)
        {
            try
            {
                int _status = 0;
                AwardIDR = "";
                object _stat;
                SqlParameter[] objsqlparam = new SqlParameter[13];
                objsqlparam[0] = new SqlParameter("@TokenNumber", objBESalesRecipientAward.TokenNumber);
                objsqlparam[1] = new SqlParameter("@Location", objBESalesRecipientAward.LocationCode_PSA);
                objsqlparam[2] = new SqlParameter("@DepartmentName", objBESalesRecipientAward.OrgUnitName);
                objsqlparam[3] = new SqlParameter("@Designation", objBESalesRecipientAward.Designation);
                objsqlparam[4] = new SqlParameter("@MasAwardID", objBESalesRecipientAward.MasAwardID);
                objsqlparam[5] = new SqlParameter("@AwardType", objBESalesRecipientAward.AwardType);
                objsqlparam[6] = new SqlParameter("@AwardAmount", objBESalesRecipientAward.AwardAmount);
                objsqlparam[7] = new SqlParameter("@Division", objBESalesRecipientAward.DivisionCode_PA);
                objsqlparam[8] = new SqlParameter("@ReasonForNomination", objBESalesRecipientAward.ReasonForNomination);
                objsqlparam[9] = new SqlParameter("@LoginID", objBESalesRecipientAward.GiverTokenNumber);
                objsqlparam[10] = new SqlParameter("@CreatedBy", objBESalesRecipientAward.GiverTokenNumber);
                objsqlparam[11] = new SqlParameter("@ModifiedBy", objBESalesRecipientAward.GiverTokenNumber);
                objsqlparam[12] = new SqlParameter("@AwardIDAuto", SqlDbType.VarChar, 100, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Default, "");
                
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_SalesRecipientAwards_Write", objsqlparam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);

                AwardIDR = (string)objsqlparam[12].Value;
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        public BE_SalesAwardTypeManagementColl GetAwardList()
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_SalesAwardTypeManagementColl objBE_SalesAwardTypeManagementColl = new BE_SalesAwardTypeManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[0];
                _ds = objDALCommon.DAL_GetData("GetAwardsforSalesAward", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_SalesAwardTypeManagement objBE_SalesAwardTypeManagement = new BE_SalesAwardTypeManagement();
                    objBE_SalesAwardTypeManagement.Id = Convert.ToInt32(_dr["ID"].ToString());
                    objBE_SalesAwardTypeManagement.AwardType = _dr["AwardType"].ToString();
                    objBE_SalesAwardTypeManagement.AwardName = _dr["AwardName"].ToString();
                    objBE_SalesAwardTypeManagement.Amount = Convert.ToDecimal(_dr["Amount"].ToString());

                    objBE_SalesAwardTypeManagementColl.Add(objBE_SalesAwardTypeManagement);
                }
                return objBE_SalesAwardTypeManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_SalesAwardTypeManagementColl GetSalesAwardList_amt(int Levels, string awardtype)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_SalesAwardTypeManagementColl objBE_SalesAwardTypeManagementColl = new BE_SalesAwardTypeManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@AwardName", awardtype);
                _ds = objDALCommon.DAL_GetData("GetAmountOfSalesAwards", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_SalesAwardTypeManagement objBE_SalesAwardTypeManagement = new BE_SalesAwardTypeManagement();
                    // change by swapneel to add ID
                    objBE_SalesAwardTypeManagement.AwardTypeId = Convert.ToInt16(_dr["ID"]);
                    // code ends
                    objBE_SalesAwardTypeManagement.AwardType = _dr["AwardType"].ToString();
                    objBE_SalesAwardTypeManagement.Amount = Convert.ToDecimal(_dr["Amount"].ToString());
                    objBE_SalesAwardTypeManagementColl.Add(objBE_SalesAwardTypeManagement);
                }
                return objBE_SalesAwardTypeManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public string GetEmailCCId(String TokenNumber)
        {
            DataSet dsgetEmails = null;
            DAL_Common objDALCommon = null;
            SqlParameter[] objsqlparam1 = null;
            try
            {
                dsgetEmails = new DataSet();
                objDALCommon = new DAL_Common();
                objsqlparam1 = new SqlParameter[1];
                objsqlparam1[0] = new SqlParameter("@RTokenNumber", TokenNumber);
                dsgetEmails = objDALCommon.DAL_GetData("usp_Get_CC_EmailIDs_byToken", objsqlparam1);
                foreach (DataRow _dr in dsgetEmails.Tables[0].Rows)
                {
                    return _dr["Emails"].ToString().Trim(',');
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            finally
            {
                if (dsgetEmails != null)
                {
                    dsgetEmails = null;
                }
                if (objDALCommon != null)
                {
                    objDALCommon = null;
                }
                if (objsqlparam1 != null)
                {
                    objsqlparam1 = null;
                }
            }
        }


    }
}

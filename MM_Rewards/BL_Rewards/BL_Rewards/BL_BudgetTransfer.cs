﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Rewards;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;

namespace BL_Rewards.BL_Rewards
{
    public sealed class BL_BudgetTransfer
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        public BE_BudgetTransferColl GetData(BE_BudgetTransfer objBE_BudgetTransfer)
        {
            try
            {
                ds = new DataSet();

                //BE_BudgetAllocation objBE_BudgetAllocation;
                BE_BudgetTransferColl objBE_BudgetTransferColl = new BE_BudgetTransferColl();
                bool IsCEO = false;
                IsCEO = objBE_BudgetTransfer.IsCEO;
                if (objBE_BudgetTransfer.CheckButtonClick == 1)
                {
                    SqlParameter[] objSqlParam = new SqlParameter[1];
                    objSqlParam[0] = new SqlParameter("@SearchBy", objBE_BudgetTransfer.SearchBy);
                    ds = objDALCommon.DAL_GetData("usp_Get_HOD_ForBudget", objSqlParam);

                }
                else if (objBE_BudgetTransfer.CheckButtonClick == 2)
                {
                    SqlParameter[] objSqlParam = new SqlParameter[2];
                    objSqlParam[0] = new SqlParameter("@HODTokenNumber", objBE_BudgetTransfer.TokenNumber);
                    objSqlParam[1] = new SqlParameter("@SearchBy", objBE_BudgetTransfer.SearchBy);
                    ds = objDALCommon.DAL_GetData("usp_Get_HOD_User", objSqlParam);

                }

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_BudgetTransfer = new BE_BudgetTransfer();

                    objBE_BudgetTransfer.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_BudgetTransfer.Name = dr["Name"].ToString();
                    objBE_BudgetTransfer.Division = dr["Division"].ToString();
                    objBE_BudgetTransfer.Location = dr["Location"].ToString();
                    objBE_BudgetTransfer.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBE_BudgetTransfer.Balance = dr["Balance"].ToString() == "" ? (Decimal?)null : Convert.ToDecimal(dr["Balance"]);
                    objBE_BudgetTransfer.IsCEO = objBE_BudgetTransfer.IsCEO;
                    //if (!IsCEO)
                    //{
                    objBE_BudgetTransfer.EMailID = dr["EmailID"].ToString();
                    //}
                    //objBE_BudgetAllocation.CreatedBy = dr["CreatedBy"].ToString();
                    //if (dr["CreatedDate"].ToString() != "")
                    //    objBE_BudgetAllocation.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    //objBE_BudgetAllocation.ModifiedBy = dr["ModifiedBy"].ToString();
                    //if (dr["ModifiedDate"].ToString() != "")
                    //    objBE_BudgetAllocation.ModifiedDate = (Convert.ToString(dr["ModifiedDate"]) == "") ? (DateTime?)null : Convert.ToDateTime(dr["ModifiedDate"]);


                    objBE_BudgetTransferColl.Add(objBE_BudgetTransfer);
                }
                return objBE_BudgetTransferColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_BudgetTransferColl GetAllUsers(string SearchBy)
        {
            try
            {
                ds = new DataSet();

                BE_BudgetTransfer objBE_BudgetTransfer;
                BE_BudgetTransferColl objBE_BudgetTransferColl = new BE_BudgetTransferColl();


                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[1] = new SqlParameter("@SearchBy", SearchBy);
                ds = objDALCommon.DAL_GetData("usp_Get_Mas_User", objSqlParam);


                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_BudgetTransfer = new BE_BudgetTransfer();

                    objBE_BudgetTransfer.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_BudgetTransfer.Name = dr["Name"].ToString();
                    objBE_BudgetTransfer.Division = dr["Division"].ToString();
                    objBE_BudgetTransfer.Location = dr["Location"].ToString();
                    objBE_BudgetTransfer.Levels = dr["Levels"].ToString() == "" ? (int?)null : Convert.ToInt32(dr["Levels"]);
                    objBE_BudgetTransfer.Designation = dr["Designation"].ToString();
                    objBE_BudgetTransferColl.Add(objBE_BudgetTransfer);
                }

                return objBE_BudgetTransferColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }


        }

        public int SaveData(BE_BudgetTransferColl objBE_BudgetTransferColl)
        {
            int Id = 0;
            try
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("TokenNumber", typeof(string));
                dt.Columns.Add("StartDate", typeof(DateTime));
                dt.Columns.Add("EndDate", typeof(DateTime));
                dt.Columns.Add("Budget", typeof(decimal));
                dt.Columns.Add("AllocationTpe", typeof(string));
                dt.Columns.Add("AllocationFrom", typeof(string));
                dt.Columns.Add("CreatedBy", typeof(string));
                dt.Columns.Add("CreatedDate", typeof(DateTime));
                dt.Columns.Add("ModifiedBy", typeof(string));
                dt.Columns.Add("ModifiedDate", typeof(DateTime));

                foreach (BE_BudgetTransfer objBE_BudgetTransfer in objBE_BudgetTransferColl)
                {
                    DataRow dr = dt.NewRow();
                    dr["TokenNumber"] = objBE_BudgetTransfer.TokenNumber;
                    dr["StartDate"] = objBE_BudgetTransfer.StartDate;
                    dr["EndDate"] = objBE_BudgetTransfer.EndDate;
                    dr["Budget"] = objBE_BudgetTransfer.Budget;
                    //dr["AllocationTpe"] = "C";
                    dr["AllocationTpe"] = objBE_BudgetTransfer.AllocationType;
                    dr["AllocationFrom"] = objBE_BudgetTransfer.AllocationFrom;
                    dr["CreatedBy"] = objBE_BudgetTransfer.CreatedBy;
                    dr["CreatedDate"] = objBE_BudgetTransfer.CreatedDate;
                    dr["ModifiedBy"] = DBNull.Value;
                    dr["ModifiedDate"] = DBNull.Value;

                    // dr["EligibilityLevel"] = i;
                    dt.Rows.Add(dr);
                }


                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@Table_Mas_UserBudget", dt);
                objDALCommon.DAL_SaveData("usp_Insert_Mas_UserBudget_BudgetTransfer", objSqlParam, 'N');

                //foreach (int i in objBE_BudgetAllocation.Levels)
                //{
                //    SqlParameter[] objSqlParamLevels = new SqlParameter[2];
                //    objSqlParamLevels[0] = new SqlParameter("@AwardID",objBE_BudgetAllocation.Id != 0?objBE_BudgetAllocation.Id: Id);
                //    objSqlParamLevels[1] = new SqlParameter("@Level", i);
                //    objDALCommon = new DAL_Common();
                //    objDALCommon.DAL_SaveData("usp_Insert_Award_Levels_Mapping", objSqlParamLevels, 'N');
                //}
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return Id;
        }

        public void SaveData_UserLevels(BE_BudgetTransfer objBE_BudgetTransfer)
        {
            try
            {
                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_BudgetTransfer.TokenNumber);
                objSqlParam[1] = new SqlParameter("@Levels", objBE_BudgetTransfer.Levels);
                objDALCommon.DAL_SaveData("usp_Update_Mas_User_Levels", objSqlParam, 'N');
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

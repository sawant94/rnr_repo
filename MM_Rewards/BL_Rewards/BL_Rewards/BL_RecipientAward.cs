﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Rewards;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Data;

namespace BL_Rewards.BL_Rewards
{
    public  class BL_RecipientAward
    {
        DataSet _ds;
        BE_RecipientAwardColl objBERecipientAwardColl;
        DAL_Common objDALCommon;

        //To Get  RecipientData List , against TokenNumber or Name
        public BE_RecipientAwardColl GetRecipientList(string RecipientName, string HODTokenNumber, int AwardId)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                objBERecipientAwardColl = new BE_RecipientAwardColl();

                SqlParameter[] objsqlparam = new SqlParameter[3];
                objsqlparam[0] = new SqlParameter("@RecipientName", RecipientName);
                objsqlparam[1] = new SqlParameter("@HODTokenNumber", HODTokenNumber);
                if (AwardId != 0)
                {
                    objsqlparam[2] = new SqlParameter("@AwardID", AwardId);
                    _ds = objDALCommon.DAL_GetData("usp_GetRecipentDetailsByName", objsqlparam);
                }
                else
                {
                    _ds = objDALCommon.DAL_GetData("usp_GetRecipentDetailsByNameForEcard", objsqlparam);

                }

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                    objBERecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
                    objBERecipientAward.FirstName = _dr["FirstName"].ToString();
                    objBERecipientAward.LastName = _dr["LastName"].ToString();
                    objBERecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                    objBERecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBERecipientAward.Designation = _dr["Designation"].ToString();
                    objBERecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBERecipientAward.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBERecipientAward.OrgUnitName = _dr["OrgUnitName"].ToString();
                    objBERecipientAward.ProcessName = _dr["ProcessName"].ToString();
                    objBERecipientAward.Level = _dr["Levels"].ToString();

                    objBERecipientAwardColl.Add(objBERecipientAward);
                }
                return objBERecipientAwardColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //To Get Recipient Data, AfterSelecting specific Value
        public BE_RecipientAward GetRecipientDetails(string TokenNumber)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                DataTable _dt = new DataTable();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_RecipientAwards_View", objsqlparam);
                _dt = _ds.Tables[0];
                foreach (DataRow _dr in _dt.Rows)
                {
                    objBERecipientAward.UserName = _dr["UserName"].ToString();
                    objBERecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
                    objBERecipientAward.FirstName = _dr["FirstName"].ToString();
                    objBERecipientAward.LastName = _dr["LastName"].ToString();
                    objBERecipientAward.ProcessName = _dr["ProcessName"].ToString();
                    objBERecipientAward.LocationCode_PSA = _dr["Location_PSA"].ToString();
                    objBERecipientAward.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBERecipientAward.OrgUnitName = _dr["OrgUnitName"].ToString();
                    objBERecipientAward.Designation = _dr["Designation"].ToString();
                    objBERecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBERecipientAward.DivisionCode_PA = _dr["Division_PA"].ToString();
                    objBERecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                    objBERecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBERecipientAward.Level = _dr["Levels"].ToString();
                }
                return objBERecipientAward;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        //Added by Megha

        public DataSet GetReportiesDetail(string TokenNumber)
        {
            try
            {

                _ds = new DataSet();
                objDALCommon = new DAL_Common();
               BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);              
                _ds = objDALCommon.DAL_GetData("GetHodReportDirctEmp", objsqlparam);
               
                return _ds;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        public DataSet GetDirectEmployeeDetail(string tokenno)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", tokenno);
                _ds = objDALCommon.DAL_GetData("GetHodReportDirctEmp", objsqlparam);
                return _ds;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public DataSet GetPillars(string type="")
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@cardtype",type);
                _ds = objDALCommon.DAL_GetData("GetCardByType", objsqlparam);
                return _ds;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }




        // Added by gaurav
        public BE_RecipientAwardColl GetEmpDetails(String TokenNumber)
        {
            _ds = new DataSet();
            objDALCommon = new DAL_Common();
            BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
            objBERecipientAwardColl = new BE_RecipientAwardColl();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            try
            {

                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_RecipientDetails_View", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    objBERecipientAward.FirstName = _dr["FirstName"].ToString();
                    objBERecipientAward.LastName = _dr["LastName"].ToString();
                    objBERecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
                    objBERecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBERecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                   objBERecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBERecipientAwardColl.Add(objBERecipientAward);
                    objBERecipientAward = new BE_RecipientAward();
                }
                return objBERecipientAwardColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            finally
            {
                if (objsqlparam != null)
                {
                    objsqlparam = null;
                }
                if (_ds != null)
                {
                    _ds = null;
                }
                if (objDALCommon != null)
                {
                    objDALCommon = null;
                }

                if (objBERecipientAward != null)
                {
                    objBERecipientAward = null;
                }
            }
        }

        public int SaveData(BE_RecipientAward objBERecipientAward, out string AwardIDR)
        {
            try
            {
                int _status = 0;
                AwardIDR = "";
                object _stat;
                SqlParameter[] objsqlparam = new SqlParameter[15];
                objsqlparam[0] = new SqlParameter("@TokenNumber", objBERecipientAward.TokenNumber);
                objsqlparam[1] = new SqlParameter("@Location", objBERecipientAward.LocationCode_PSA);
                objsqlparam[2] = new SqlParameter("@DepartmentName", objBERecipientAward.OrgUnitName);
                objsqlparam[3] = new SqlParameter("@Designation", objBERecipientAward.Designation);
                objsqlparam[4] = new SqlParameter("@MasAwardID", objBERecipientAward.MasAwardID);
                objsqlparam[5] = new SqlParameter("@AwardType", objBERecipientAward.AwardType);
                objsqlparam[6] = new SqlParameter("@AwardAmount", objBERecipientAward.AwardAmount);
                objsqlparam[7] = new SqlParameter("@Division", objBERecipientAward.DivisionCode_PA);
                objsqlparam[8] = new SqlParameter("@ReasonForNomination", objBERecipientAward.ReasonForNomination);
                objsqlparam[9] = new SqlParameter("@LoginID", objBERecipientAward.GiverTokenNumber);
                //objsqlparam[10] = new SqlParameter("@AwardIDAuto", SqlDbType.VarChar, 100, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Default, "");
                objsqlparam[10] = new SqlParameter("@AwardIDAuto", SqlDbType.VarChar, 1000, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Default, "");

                //added by gaurav

                objsqlparam[11] = new SqlParameter("@CoffeeDate", objBERecipientAward.CoffeeDate);
                objsqlparam[12] = new SqlParameter("@CCEmailID", objBERecipientAward.CCEmailID);
                //objsqlparam[13] = new SqlParameter("@ReasonOnCertificate", objBERecipientAward.ReasonForNomination);
                objsqlparam[13] = new SqlParameter("@Pillar", objBERecipientAward.Pillar);
                objsqlparam[14] = new SqlParameter("@ImageURL", objBERecipientAward.ImageURL);
               // objsqlparam[16] = new SqlParameter("@ReturnResponse", objBERecipientAward.ReturnResponse);
                // Added by Swapneel on 2nd April for web service

                //objsqlparam[12] = new SqlParameter("@RequestId","10");
                //objsqlparam[13] = new SqlParameter("@RequestDate","10");
                //objsqlparam[14] = new SqlParameter("@NominatorName","Swapneel");
                //objsqlparam[15] = new SqlParameter("@NominatorId","10");
                //objsqlparam[16] = new SqlParameter("@RecipientName","Priyanka");
                //objsqlparam[17] = new SqlParameter("@RecipientId","10");
                //objsqlparam[18] = new SqlParameter("@DeliveryAddress","Borivali");
                //objsqlparam[19] = new SqlParameter("@PinCode","400103");
                //objsqlparam[20] = new SqlParameter("@GiftHamperCode","10");
                //objsqlparam[21] = new SqlParameter("@Status","Pending");

                // code ends here
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_RecipientAwards_Write", objsqlparam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                if (objsqlparam[10].Value != null || objsqlparam[10].Value != string.Empty || objsqlparam[10].Value != "")
                {
                    AwardIDR = (string)objsqlparam[10].Value;
                    //  AwardIDR =DBNull.Value.ToString();
                }

                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        // Gift Hamper code

        public int SaveGiftHamperData(BE_GiftHampers objBE_GiftHampers, out string Request_Id)
        {
            try
            {
                int _status = 0;
                Request_Id = "";
                object _stat;
                SqlParameter[] objsqlparam = new SqlParameter[13];

                objsqlparam[0] = new SqlParameter("@Request_Id", SqlDbType.NVarChar, 255, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Default, "");
                objsqlparam[1] = new SqlParameter("@RequestDate", objBE_GiftHampers.RequestDate);
                objsqlparam[2] = new SqlParameter("@NominatorName", objBE_GiftHampers.NomName);
                objsqlparam[3] = new SqlParameter("@NominatorId", objBE_GiftHampers.NomEmpId);
                objsqlparam[4] = new SqlParameter("@RecipientName", objBE_GiftHampers.RecName);
                objsqlparam[5] = new SqlParameter("@RecipientId", objBE_GiftHampers.RecEmpId);
                objsqlparam[6] = new SqlParameter("@DeliveryAddress", objBE_GiftHampers.DeliveryAddress);
                objsqlparam[7] = new SqlParameter("@PinCode", objBE_GiftHampers.Pincode);
                objsqlparam[8] = new SqlParameter("@GiftAmount", objBE_GiftHampers.GiftAmount);
                objsqlparam[9] = new SqlParameter("@Status", objBE_GiftHampers.RequestStatus);
                objsqlparam[10] = new SqlParameter("@Award_Id", objBE_GiftHampers.AwardId);
                objsqlparam[11] = new SqlParameter("@NomEmailID", objBE_GiftHampers.NomEmailID);
                objsqlparam[12] = new SqlParameter("@RecEmailID", objBE_GiftHampers.RecEmailID);


                // code ends here
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Insert_GiftHamper", objsqlparam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                Request_Id = (string)objsqlparam[0].Value;
                // AwardIDR = (string)objsqlparam[10].Value;
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        // gift Hamper code ends here

        // Gift Voucher code

        public int SaveGiftVoucherData(BE_GiftVouchers objBE_GiftVouchers, out string Request_Id)
        {
            try
            {
                int _status = 0;
                Request_Id = "";
                object _stat;
                SqlParameter[] objsqlparam = new SqlParameter[14];

                //objsqlparam[0] = new SqlParameter("@Request_Id", objBE_GiftVouchers.RequestId);
                objsqlparam[0] = new SqlParameter("@Request_Id", objBE_GiftVouchers.RequestId);//SqlDbType.NVarChar, 255, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Default, "");
                objsqlparam[1] = new SqlParameter("@Request_Date", objBE_GiftVouchers.RequestDate);
                objsqlparam[2] = new SqlParameter("@Nom_Name", objBE_GiftVouchers.NomName);
                objsqlparam[3] = new SqlParameter("@Nom_EmpId", objBE_GiftVouchers.NomEmpId);
                objsqlparam[4] = new SqlParameter("@Rec_Name", objBE_GiftVouchers.RecName);
                objsqlparam[5] = new SqlParameter("@Rec_EmpId", objBE_GiftVouchers.RecEmpId);
                objsqlparam[6] = new SqlParameter("@Nom_Email", objBE_GiftVouchers.NomEmail);
                objsqlparam[7] = new SqlParameter("@Rec_Email", objBE_GiftVouchers.RecEmail);
                objsqlparam[8] = new SqlParameter("@Gift_Amount", objBE_GiftVouchers.GiftAmount);
                objsqlparam[9] = new SqlParameter("@Request_Status", objBE_GiftVouchers.RequestStatus);
                objsqlparam[10] = new SqlParameter("@Award_BusinessUnit", objBE_GiftVouchers.AwardBusinessUnit);
                objsqlparam[11] = new SqlParameter("@Award_Division", objBE_GiftVouchers.AwardDivision);
                objsqlparam[12] = new SqlParameter("@Award_Location ", objBE_GiftVouchers.AwardLocation);
                objsqlparam[13] = new SqlParameter("@Award_Id", objBE_GiftVouchers.AwardID);// by vandana
                // code ends here
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Insert_GiftVoucher", objsqlparam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                Request_Id = (string)objsqlparam[0].Value;
                // AwardIDR = (string)objsqlparam[10].Value;
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_AwardManagementColl GetAwardList(int EligibilityLevels)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@EligibilityLevel", EligibilityLevels);
                //_ds = objDALCommon.DAL_GetData("GetAwardsByEligibilityLevel", objsqlparam);
                _ds = objDALCommon.DAL_GetData("GetAwardsByEligibilityLevel", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_AwardManagement objBEAwardManagement = new BE_AwardManagement();
                    objBEAwardManagement.Id = Convert.ToInt32(_dr["ID"].ToString());
                    objBEAwardManagement.AwardType = _dr["AwardType"].ToString();
                    objBEAwardManagement.Name = _dr["Name"].ToString();
                    objBEAwardManagement.Amount = Convert.ToDecimal(_dr["Amount"].ToString());
                    objBEAwardManagement.Autoredeem = Convert.ToBoolean(_dr["IsAuto"]);
                    objBEAwardManagement.ID_IsAuto = Convert.ToString(_dr["ID_IsAuto"]);

                    objAwardManagementColl.Add(objBEAwardManagement);
                }
                return objAwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //megha

        public BE_AwardManagementColl GetDivision()
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();

                _ds = objDALCommon.DAL_GetData("GetDivision");
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_AwardManagement objBEAwardManagement = new BE_AwardManagement();
                    objBEAwardManagement.EmployeeLevelsName = Convert.ToString(_dr["Division_PA"].ToString());
                    objAwardManagementColl.Add(objBEAwardManagement);
                }
                return objAwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //megha
        public DataSet GetBudget()
        {
            BE_BudgetReviewColl objAwardManagementColl = new BE_BudgetReviewColl();
            try
            {
                DataSet dsreportiesbhp = new DataSet();
                DAL_Common objCommon = new DAL_Common();
                SqlParameter[] param = new SqlParameter[2];

                dsreportiesbhp = objCommon.DAL_GetData("GetBudgetForReview");
                //if (dsreportiesbhp.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow _dr in dsreportiesbhp.Tables[0].Rows)
                //    {
                //        BE_BudgetReview objBEAwardManagement = new BE_BudgetReview();
                //        objBEAwardManagement.TokenNumber = Convert.ToString(_dr["TokenNumber"].ToString());
                //        objBEAwardManagement.Name = Convert.ToString(_dr["Name"].ToString());
                //        objBEAwardManagement.EX500BHP = Convert.ToInt32(_dr["EX500BHP"].ToString());
                //        objBEAwardManagement.EX750BHP = Convert.ToInt32(_dr["EX750BHP"].ToString());
                //        objBEAwardManagement.EX1000BHP = Convert.ToInt32(_dr["EX1000BHP"].ToString());
                //        objBEAwardManagement.SpotAward = Convert.ToInt32(_dr["SpotAward"].ToString());
                //        objBEAwardManagement.TotalBudget = Convert.ToInt32(_dr["TotalBudget"].ToString());
                //        objBEAwardManagement.FinalBudget = Convert.ToInt32(_dr["FinalBudget"].ToString());
                //        objAwardManagementColl.Add(objBEAwardManagement);
                //    }
                //}
                return dsreportiesbhp;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_Ecard GetEcardByID(int ID)
        {
            _ds = new DataSet();
            BE_Ecard objBeEcard = new BE_Ecard();

            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@ID", ID);


            objDALCommon = new DAL_Common();
            _ds = objDALCommon.DAL_GetData("usp_GetEcardByID", objsqlparam);

            if (_ds.Tables[0].Rows.Count > 0)
            {
                objBeEcard.ID = (int)_ds.Tables[0].Rows[0]["ID"];
                objBeEcard.RecipientTokenNumber = (string)_ds.Tables[0].Rows[0]["TokenNumber"];
                objBeEcard.GiverTokenNUmber = (string)_ds.Tables[0].Rows[0]["GiverTokenNumber"];
                objBeEcard.Message = (string)_ds.Tables[0].Rows[0]["Message"];
                objBeEcard.CardType = (string)_ds.Tables[0].Rows[0]["CardType"];
            }

            return objBeEcard;
        }

        public int Check_RequestExist(string RequestID)
        {
            _ds = new DataSet();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@RequestId", RequestID);

            objDALCommon = new DAL_Common();
            _ds = objDALCommon.DAL_GetData("usp_CheckRecordExist", objsqlparam);

            if (_ds.Tables[0].Rows.Count > 0)
            {
                return 1;
            }
            return 0;
        }

        public int Check_VoucherRequestExist(string RequestID)
        {
            _ds = new DataSet();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@RequestId", RequestID);

            objDALCommon = new DAL_Common();
            _ds = objDALCommon.DAL_GetData("usp_CheckVoucherRecordExist", objsqlparam);

            if (_ds.Tables[0].Rows.Count > 0)
            {
                return 1;
            }
            return 0;
        }

        public int StoreEcard(BE_Ecard ecard)
        {
            int _status = 0;
            object _stat;

            DataSet _ds = new DataSet();

            SqlParameter[] objsqlparam = new SqlParameter[6];
            objsqlparam[0] = new SqlParameter("@RecipientTokenNumber", ecard.RecipientTokenNumber);
            objsqlparam[1] = new SqlParameter("@GiverTokenNumber", ecard.GiverTokenNUmber);
            objsqlparam[2] = new SqlParameter("@Message", ecard.Message);
            objsqlparam[3] = new SqlParameter("@CardType", ecard.CardType);
            objsqlparam[4] = new SqlParameter("@CCEmailID", ecard.CCEmailID);
            objsqlparam[5] = new SqlParameter("@ImageUrl", ecard.ImageURL);

            objDALCommon = new DAL_Common();
            _stat = objDALCommon.DAL_SaveData("usp_StoreEcardForRecipient", objsqlparam, 'S');
            _status = _stat == null ? 0 : Convert.ToInt32(_stat);

            return _status;
        }

        public BE_AwardManagementColl GetAwardList_amt(int Levels, string awardtype)
        {

            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[2];
                objsqlparam[0] = new SqlParameter("@EligibilityLevel", Levels);
                objsqlparam[1] = new SqlParameter("@AwardName", awardtype);
                _ds = objDALCommon.DAL_GetData("GetAmountOfAwardsByEligibilityLevel", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_AwardManagement objBEAwardManagement = new BE_AwardManagement();
                    // objBEAwardManagement.Id =Convert.ToInt32( _dr["ID"].ToString());
                    objBEAwardManagement.AwardType = _dr["AwardType"].ToString();
                    //   objBEAwardManagement.Name = _dr["Name"].ToString();
                    objBEAwardManagement.Amount = Convert.ToDecimal(_dr["Amount"].ToString()); // by vandana
                    //  objBEAwardManagement.Autoredeem = Convert.ToBoolean(_dr["IsAuto"]);
                    objBEAwardManagement.ID_IsAuto = Convert.ToString(_dr["ID_IsAuto"]);

                    objAwardManagementColl.Add(objBEAwardManagement);
                }
                return objAwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int UpdateRequestPoint(BE_GiftHampers objBE_GiftHampers)
        {
            int Id = 0;
            try
            {
                objDALCommon = new DAL_Common();
                SqlParameter[] objSqlParam = new SqlParameter[5];
                objSqlParam[0] = new SqlParameter("@RequestId", objBE_GiftHampers.RequestId);
                objSqlParam[1] = new SqlParameter("@Points_Assigned", objBE_GiftHampers.PointsAssigned);
                objSqlParam[2] = new SqlParameter("@Points_Used", objBE_GiftHampers.PointsUsed);
                objSqlParam[3] = new SqlParameter("@Balance", objBE_GiftHampers.Balance);
                objSqlParam[4] = new SqlParameter("@Award_Id", objBE_GiftHampers.AwardId);
                Id = (int)objDALCommon.DAL_SaveData("usp_Update_GiftHamperPoints", objSqlParam, 'N');
                return 0;
            }
            catch (Exception exx)
            {
                throw new ArgumentException(exx.Message);
            }
            // return Id;
        }

        //to save data to Request Status
        public int UpdateRequestStatus(BE_GiftHampers objBE_GiftHampers)
        {
            int Id = 0;
            try
            {
                objDALCommon = new DAL_Common();
                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[0] = new SqlParameter("@RequestId", objBE_GiftHampers.RequestId);
                objSqlParam[1] = new SqlParameter("@Status", objBE_GiftHampers.RequestStatus);
                Id = (int)objDALCommon.DAL_SaveData("usp_Update_GiftHamperStatus", objSqlParam, 'N');
                return 0;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            //  return Id;
        }

        //to save data to request status - voucher
        public int UpdateRequestStatusVoucher(BE_GiftVouchers objBE_GiftVouchers)
        {
            int Id = 0;
            try
            {
                objDALCommon = new DAL_Common();
                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[0] = new SqlParameter("@RequestId", objBE_GiftVouchers.RequestId);
                objSqlParam[1] = new SqlParameter("@Status", objBE_GiftVouchers.RequestStatus);

                Id = (int)objDALCommon.DAL_SaveData("usp_Update_GiftVoucherStatus", objSqlParam, 'N');
                return 0;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            // return Id;
        }

        public string GetEmailCCId(String TokenNumber)
        {
            DataSet dsgetEmails = null;
            DAL_Common objDALCommon = null;
            SqlParameter[] objsqlparam1 = null;
            try
            {
                dsgetEmails = new DataSet();
                objDALCommon = new DAL_Common();
                objsqlparam1 = new SqlParameter[1];
                objsqlparam1[0] = new SqlParameter("@RTokenNumber", TokenNumber);
                dsgetEmails = objDALCommon.DAL_GetData("usp_Get_CC_EmailIDs_byToken", objsqlparam1);
                foreach (DataRow _dr in dsgetEmails.Tables[0].Rows)
                {
                    return _dr["Emails"].ToString().Trim(',');
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            finally
            {
                if (dsgetEmails != null)
                {
                    dsgetEmails = null;
                }
                if (objDALCommon != null)
                {
                    objDALCommon = null;
                }
                if (objsqlparam1 != null)
                {
                    objsqlparam1 = null;
                }
            }
        }

        //Code added by Nilesh J 22-12-2014
        public int SavespotKindData(BE_SpotKind objBE_SpotKind, out string Request_Id)
        {
            try
            {
                int _status = 0;
                Request_Id = "";
                object _stat;
                SqlParameter[] objsqlparam = new SqlParameter[14];
                //objsqlparam[0] = new SqlParameter("@Request_Id", objBE_GiftVouchers.RequestId); //objBE_SpotKind.RequestId);   //)
                objsqlparam[0] = new SqlParameter("@Request_Id", objBE_SpotKind.RequestId);//SqlDbType.NVarChar, 255, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Default, "");
                objsqlparam[1] = new SqlParameter("@Request_Date", objBE_SpotKind.RequestDate);
                objsqlparam[2] = new SqlParameter("@Nom_Name", objBE_SpotKind.NomName);
                objsqlparam[3] = new SqlParameter("@Nom_EmpId", objBE_SpotKind.NomEmpId);
                objsqlparam[4] = new SqlParameter("@Rec_Name", objBE_SpotKind.RecName);
                objsqlparam[5] = new SqlParameter("@Rec_EmpId", objBE_SpotKind.RecEmpId);
                objsqlparam[6] = new SqlParameter("@Nom_Email", objBE_SpotKind.NomEmailID); //NomEmail);
                objsqlparam[7] = new SqlParameter("@Rec_Email", objBE_SpotKind.RecEmailID);  //.RecEmail);
                objsqlparam[8] = new SqlParameter("@Gift_Amount", objBE_SpotKind.GiftAmount);
                objsqlparam[9] = new SqlParameter("@Request_Status", objBE_SpotKind.RequestStatus);
                objsqlparam[10] = new SqlParameter("@Award_BusinessUnit", objBE_SpotKind.AwardBusinessUnit);
                objsqlparam[11] = new SqlParameter("@Award_Division", objBE_SpotKind.AwardDivision);
                objsqlparam[12] = new SqlParameter("@Award_Location ", objBE_SpotKind.AwardLocation);
                objsqlparam[13] = new SqlParameter("@Award_Id", objBE_SpotKind.AwardId);// by vandana
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Insert_Webservice_Kind", objsqlparam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                Request_Id = (string)objsqlparam[0].Value;
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //to save data to request status - Kind      code added By Nilesh J  22-12-2014
        public int UpdateRequestStatusKind(BE_SpotKind objBE_SpotKind)
        {
            int Id = 0;
            try
            {
                objDALCommon = new DAL_Common();
                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[0] = new SqlParameter("@RequestId", objBE_SpotKind.RequestId);
                objSqlParam[1] = new SqlParameter("@Status", objBE_SpotKind.RequestStatus);

                Id = (int)objDALCommon.DAL_SaveData("usp_Update_SpotKindStatus", objSqlParam, 'N');
                return 0;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            // return Id;
        }

        //Added By Nilesh J 07-01-2015
        public int Check_KindRequestExist(string RequestID)
        {
            _ds = new DataSet();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@RequestId", RequestID);

            objDALCommon = new DAL_Common();
            _ds = objDALCommon.DAL_GetData("usp_KindCheckRecordExist", objsqlparam);

            if (_ds.Tables[0].Rows.Count > 0)
            {
                return 1;
            }
            return 0;
        }


        public BE_AwardManagementColl GetDivision(string CompanyCode, string BusinessUnit)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[2];
                objsqlparam[0] = new SqlParameter("@CompanyCode", CompanyCode);
                objsqlparam[1] = new SqlParameter("@BusinessUnit", BusinessUnit);
                _ds = objDALCommon.DAL_GetData("GetDivisionName", objsqlparam);

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_AwardManagement objBEAwardManagement = new BE_AwardManagement();

                    objBEAwardManagement.EmployeeDivisionName= Convert.ToString(_dr["DivisionName_PA"].ToString());
                    objAwardManagementColl.Add(objBEAwardManagement);
                }
                return objAwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public BE_AwardManagementColl GetCompanyCode()
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();

                _ds = objDALCommon.DAL_GetData("GetCompanyCode");
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_AwardManagement objBEAwardManagement = new BE_AwardManagement();
                    objBEAwardManagement.CompanyCode = Convert.ToString(_dr["CompanyCode"].ToString());
                    objAwardManagementColl.Add(objBEAwardManagement);
                }
                return objAwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public BE_AwardManagementColl GetBusinessUnit(string CompanyCode)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@CompanyCode", CompanyCode);

                _ds = objDALCommon.DAL_GetData("GetBusinessUnit", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_AwardManagement objBEAwardManagement = new BE_AwardManagement();
                    objBEAwardManagement.BusinessUnit = Convert.ToString(_dr["BusinessUnit"].ToString());
                    objAwardManagementColl.Add(objBEAwardManagement);
                }
                return objAwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public BE_AwardManagementColl GetEmployeeLevelName_ES()
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();

                _ds = objDALCommon.DAL_GetData("GetEmployeeLevelName_ES");
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_AwardManagement objBEAwardManagement = new BE_AwardManagement();
                    objBEAwardManagement.EmployeeLevelName_ES = Convert.ToString(_dr["EmployeeLevelName_ES"].ToString());
                    objAwardManagementColl.Add(objBEAwardManagement);
                }
                return objAwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public DataSet GetBudgetUpdateReport()
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();

                _ds = objDALCommon.DAL_GetData("spGetBudgetUpdateReport");
                
                return _ds;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //Search for Transfer Budget by token number or name according to level
        public BE_RecipientAwardColl GetRecipientListForBudgetTransfer(string RecipientName, string HODTokenNumber)
        {
            //[usp_GetRecipentDetailsByNameForBudgetTransfer]
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                objBERecipientAwardColl = new BE_RecipientAwardColl();

                SqlParameter[] objsqlparam = new SqlParameter[3];
                objsqlparam[0] = new SqlParameter("@RecipientName", RecipientName);
                objsqlparam[1] = new SqlParameter("@HODTokenNumber", HODTokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_GetRecipentDetailsByNameForBudgetTransfer", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                    objBERecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
                    objBERecipientAward.FirstName = _dr["FirstName"].ToString();
                    objBERecipientAward.LastName = _dr["LastName"].ToString();
                    objBERecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                    objBERecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBERecipientAward.Designation = _dr["Designation"].ToString();
                    objBERecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBERecipientAward.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBERecipientAward.OrgUnitName = _dr["OrgUnitName"].ToString();
                    objBERecipientAward.ProcessName = _dr["ProcessName"].ToString();
                    objBERecipientAward.Level = _dr["Levels"].ToString();

                    objBERecipientAwardColl.Add(objBERecipientAward);
                }
                return objBERecipientAwardColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public string GetCertificateTextById(int id)
        {
            try
            {
                objDALCommon = new DAL_Common();
                BE_AwardManagementColl objAwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@id", id);
                var _text = objDALCommon.DAL_SaveData("GetCertificateTextById", objsqlparam, 'S');
                string _certificateText = Convert.ToString(_text);
                return _certificateText;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

    }
}

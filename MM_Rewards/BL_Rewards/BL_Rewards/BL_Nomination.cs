﻿using System;
using System.Data;
using System.Data.SqlClient;
using MM.DAL;
using BE_Rewards.BE_Rewards;


namespace BL_Rewards.BL_Rewards
{
    public class BL_Nomination
    {
        //For RNR 3.0
        DAL_Common objDALCommon = new DAL_Common();
        DataSet _ds = new DataSet();
        public YearData GetFYData()
        {
            YearData obj = new YearData();

            int CurrentYear = DateTime.Now.Year;
            int CurrentMonth = DateTime.Now.Month;

            string StartYear = CurrentYear.ToString();

            if (CurrentMonth >= 4 && CurrentMonth <= 7)
            {//Correct
                obj.Quarter = 1;
                obj.Year = CurrentYear+1;
                obj.StartDate = StartYear + "-04-01";
                obj.EndDate = StartYear + "-07-30";
            }
            else if (CurrentMonth > 7 && CurrentMonth <= 9)
            {
                obj.Quarter = 2;
                obj.Year = CurrentYear+1;
                obj.StartDate = StartYear + "-08-01";
                obj.EndDate = StartYear + "-10-10";
            }
            else if (CurrentMonth >= 10 && CurrentMonth <= 12)
            {
                obj.Quarter = 3;
                obj.Year = CurrentYear+1;
                obj.StartDate = StartYear + "-10-11";
                obj.EndDate = StartYear + 1 + "-01-10";
            }
            else if (CurrentMonth >= 1 && CurrentMonth <= 3)
            {
                obj.Quarter = 4;
                obj.Year = CurrentYear;
                obj.StartDate = StartYear + "-01-11";
                obj.EndDate = StartYear + "-03-31";
            }
            return obj;
        }
        //Rakesh
        public DataSet NominationAccess(string Token)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@Token", Token);
            _ds = objDALCommon.DAL_GetData("usp_NominationAccess", objsqlparam);
            return _ds;
        }
        public DataSet NominationApproverAccess(string Token)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@Token", Token);
            _ds = objDALCommon.DAL_GetData("usp_NominationApproverAccess", objsqlparam);
            return _ds;
        }
        public DataSet NominationApproverAccessN_1(string Token)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@Token", Token);
            _ds = objDALCommon.DAL_GetData("usp_NominationApproverAccessN_1", objsqlparam);
            return _ds;
        }
        public int GetAFLCRemaingBudget(string AFLCToken, string StartDate, string EndDate)
        {
            int budget = 0;
            object _stat = null;
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@Token", AFLCToken);
            objsqlparam[1] = new SqlParameter("@StartDate", StartDate);
            objsqlparam[2] = new SqlParameter("@EndDate", EndDate);
            _stat = objDALCommon.DAL_SaveData("usp_GetAFLCRemaingBudget", objsqlparam, 'S');
            budget = _stat == null ? 0 : Convert.ToInt32(_stat);
            return budget;
        }
        public int UpdateAFLCRemaingBudget(string AFLCToken, string StartDate, string EndDate, int UsedBudget)
        {
            object _stat = null;
            int _status = 0;
            SqlParameter[] objsqlparam = new SqlParameter[4];
            objsqlparam[0] = new SqlParameter("@Token", AFLCToken);
            objsqlparam[1] = new SqlParameter("@StartDate", StartDate);
            objsqlparam[2] = new SqlParameter("@EndDate", EndDate);
            objsqlparam[3] = new SqlParameter("@UsedBudget", UsedBudget);
            _stat = objDALCommon.DAL_SaveData("usp_UpdateAFLCRemaingBudget", objsqlparam, 'N');
            _status = _stat == null ? 0 : Convert.ToInt32(_stat);
            return _status;
        }
        public DataSet GetAFLCMember()
        {
            //SqlParameter[] sqlparameter = new SqlParameter[1];
            //sqlparameter[0] = new SqlParameter("@SubAFLCTokenNumber", SubAFLCTokenNumber);
            _ds = objDALCommon.DAL_GetData("spGetAFLCMember");
            return _ds;
        }
        public DataSet GetNominees()
        {
            _ds = objDALCommon.DAL_GetData("spGetNominees");
            return _ds;
        }
        public DataSet GetNomineesByToken(string TokenNumber)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDALCommon.DAL_GetData("spSearchNominees", objsqlparam);
            return _ds;
        }
        public DataSet GetEmpDetailsByToken(string TokenNumber)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
            _ds = objDALCommon.DAL_GetData("spGetEmpDetailsByToken", objsqlparam);
            return _ds;
        }
        public BE_NominationColl BindNomineeGrid(string MasterReferenceNo)
        {
            BE_NominationColl objBE_NominationColl = new BE_NominationColl();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@MasterReferenceNo", @MasterReferenceNo);
            _ds = objDALCommon.DAL_GetData("spBindNomineesByHOD", objsqlparam);
            if (_ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    BE_Nomination objBE_Nomination = new BE_Nomination();
                    objBE_Nomination.Id = Convert.ToInt32(dr["ID"]);
                    objBE_Nomination.AFLCToken = dr["AFLCToken"].ToString();
                    objBE_Nomination.AFLCName = dr["AFLCName"].ToString();
                    objBE_Nomination.RecipientToken = dr["RecipientToken"].ToString();
                    objBE_Nomination.RecipientName = dr["RecipientName"].ToString();
                    objBE_Nomination.BusinessUnit = dr["BusinessUnit"].ToString();
                    objBE_Nomination.Story = dr["Story"].ToString();
                    objBE_Nomination.Synopsis = dr["Synopsis"].ToString();
                    objBE_Nomination.BusinessImpact = dr["BusinessImpact"].ToString();
                    objBE_Nomination.NominatedYear = Convert.ToInt32(dr["NominatedYear"]);
                    objBE_Nomination.NominatedQuarter = Convert.ToInt32(dr["NominatedQuarter"]);
                    objBE_Nomination.CreatedBy = dr["CreatedBy"].ToString();
                    objBE_Nomination.AwardAmount = Convert.ToInt32(dr["AwardAmount"]);
                    objBE_Nomination.AwardDecision = dr["AwardDecision"].ToString();
                    objBE_Nomination.ReferenceNumber = dr["MasterReferenceNo"].ToString();
                    objBE_NominationColl.Add(objBE_Nomination);
                }
            }
            return objBE_NominationColl;

        }
        public DataSet spGetNomineebyId(int Id)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@Id", Id);
            _ds = objDALCommon.DAL_GetData("spGetNomineebyId", objsqlparam);
            return _ds;
        }
        //GetMaxNomineeId awardee formateed id+1
        public DataSet GetMaxNomineeId()
        {
            _ds = objDALCommon.DAL_GetData("spGetMaxNomineeId");
            return _ds;
        }
        //insert awardee name and data
        public int SaveNominee(BE_Nomination objBENomination)
        {
            try
            {
                int _status = 0;
                int Count = 0;
                object _stat;
                foreach (BE_Recipient emp in objBENomination.objBERecipientList)
                {
                    objBENomination.RecipientToken = emp.RecipientToken;
                    objBENomination.RecipientName = emp.RecipientName;

                    SqlParameter[] objsqlparam = new SqlParameter[12];
                    objsqlparam[0] = new SqlParameter("@AFLCToken", objBENomination.AFLCToken);
                    objsqlparam[1] = new SqlParameter("@AFLCName", objBENomination.AFLCName);
                    objsqlparam[2] = new SqlParameter("@RecipientToken", objBENomination.RecipientToken);
                    objsqlparam[3] = new SqlParameter("@RecipientName", objBENomination.RecipientName);
                    objsqlparam[4] = new SqlParameter("@BusinessUnit", objBENomination.BusinessUnit);
                    objsqlparam[5] = new SqlParameter("@Story", objBENomination.Story);
                    objsqlparam[6] = new SqlParameter("@Synopsis", objBENomination.Synopsis);
                    objsqlparam[7] = new SqlParameter("@BusinessImpact", objBENomination.BusinessImpact);
                    objsqlparam[8] = new SqlParameter("@NominatedYear", objBENomination.NominatedYear);
                    objsqlparam[9] = new SqlParameter("@NominatedQuarter", objBENomination.NominatedQuarter);
                    objsqlparam[10] = new SqlParameter("@CreatedBy", objBENomination.CreatedBy);
                    //objsqlparam[11] = new SqlParameter("@AwardAmount", objBENomination.AwardAmount);
                    objsqlparam[11] = new SqlParameter("@MasterReferenceNo", objBENomination.ReferenceNumber);

                    _stat = objDALCommon.DAL_SaveData("spInsertNominees", objsqlparam, 'N');
                    _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                    Count++;

                }
                return Count;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //Update all awardee data
        public int UpdateNomineeData(BE_Nomination objBENomination)
        {
            try
            {
                int _status = 0;
                object _stat;

                SqlParameter[] objsqlparam = new SqlParameter[10];
                objsqlparam[0] = new SqlParameter("@ID", objBENomination.Id);
                objsqlparam[1] = new SqlParameter("@RecipientToken", objBENomination.RecipientToken);
                objsqlparam[2] = new SqlParameter("@AwardAmount", objBENomination.AwardAmount);
                objsqlparam[3] = new SqlParameter("@AwardDecision", objBENomination.AwardDecision);
                objsqlparam[4] = new SqlParameter("@BusinessUnit", objBENomination.BusinessUnit);
                objsqlparam[5] = new SqlParameter("@Story", objBENomination.Story);
                objsqlparam[6] = new SqlParameter("@Synopsis", objBENomination.Synopsis);
                objsqlparam[7] = new SqlParameter("@BusinessImpact", objBENomination.BusinessImpact);
                objsqlparam[8] = new SqlParameter("@ActionBy", objBENomination.ActionBy);
                objsqlparam[9] = new SqlParameter("@Remark", objBENomination.Remark);

                _stat = objDALCommon.DAL_SaveData("spUpdateNomineesData", objsqlparam, 'N');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //Update awardee name and data
        //public int UpdateNominee(BE_Nomination objBENomination)
        //{
        //    try
        //    {
        //        int _status = 0;
        //        object _stat;

        //        SqlParameter[] objsqlparam = new SqlParameter[6];
        //        objsqlparam[0] = new SqlParameter("@ID", objBENomination.Id);
        //        objsqlparam[1] = new SqlParameter("@RecipientToken", objBENomination.RecipientToken);
        //        objsqlparam[2] = new SqlParameter("@AwardAmount", objBENomination.AwardAmount);
        //        objsqlparam[3] = new SqlParameter("@AwardDecision", objBENomination.AwardDecision);
        //        objsqlparam[4] = new SqlParameter("@ActionBy", objBENomination.ActionBy);
        //        objsqlparam[5] = new SqlParameter("@Remark", objBENomination.Remark);

        //        _stat = objDALCommon.DAL_SaveData("spUpdateNominees", objsqlparam, 'N');
        //        _status = _stat == null ? 0 : Convert.ToInt32(_stat);
        //        return _status;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }
        //}
        //Insert Nominee Bulk Data

        public string SaveNomineeMasterData(BE_Nomination objBENomination)
        {
            try
            {
                //string _status = "";
                object _stat;

                SqlParameter[] objsqlparam = new SqlParameter[13];
                objsqlparam[0] = new SqlParameter("@AFLCToken", objBENomination.AFLCToken);
                objsqlparam[1] = new SqlParameter("@AFLCName", objBENomination.AFLCName);
                objsqlparam[2] = new SqlParameter("@BusinessUnit", objBENomination.BusinessUnit);
                objsqlparam[3] = new SqlParameter("@Story", objBENomination.Story);
                objsqlparam[4] = new SqlParameter("@Synopsis", objBENomination.Synopsis);
                objsqlparam[5] = new SqlParameter("@BusinessImpact", objBENomination.BusinessImpact);
                objsqlparam[6] = new SqlParameter("@NominatedYear", objBENomination.NominatedYear);
                objsqlparam[7] = new SqlParameter("@NominatedQuarter", objBENomination.NominatedQuarter);
                //objsqlparam[10] = new SqlParameter("@AwardAmountIndividual", objBENomination.AwardAmount);
                objsqlparam[8] = new SqlParameter("@EmployeeCount", objBENomination.EmployeeCount);
                objsqlparam[9] = new SqlParameter("@TotalAmount", objBENomination.TotalAmount);
                objsqlparam[10] = new SqlParameter("@CreatedByToken", objBENomination.CreatedBy);
                objsqlparam[11] = new SqlParameter("@CreatedByName", objBENomination.CreatedByName);
                objsqlparam[12] = new SqlParameter("@MasterReferenceNo", SqlDbType.VarChar, 50, ParameterDirection.Output, false, 0, 20, "MasterReferenceNo", DataRowVersion.Default, null);

                _stat = objDALCommon.DAL_SaveData("uspInsertNominationMaster", objsqlparam, 'N');
                string _status = _stat == null ? "0" : Convert.ToString(_stat);
                if (_status != "0")
                {
                    _status = (string)objsqlparam[12].Value;
                }
                return _status;
                //MasterReferenceNumber
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        //Get AFLC Budget data
        public DataSet GetAFLCQuarterBudgetData(string AFLCToken, string StartDate, string EndDate)
        {
            DataSet dsbudget = new DataSet();
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@Token", AFLCToken);
            objsqlparam[1] = new SqlParameter("@StartDate", StartDate);
            objsqlparam[2] = new SqlParameter("@EndDate", EndDate);
            _ds = objDALCommon.DAL_GetData("usp_GetAFLCQuarterBudgetData", objsqlparam);
            return _ds;
        }
        public DataSet GetProjectData(string AFLCToken, int NominatedYear, int NominatedQuarter)
        {
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@AFLCToken", AFLCToken);
            objsqlparam[1] = new SqlParameter("@NominatedYear", NominatedYear);
            objsqlparam[2] = new SqlParameter("@NominatedQuarter", NominatedQuarter);
            _ds = objDALCommon.DAL_GetData("usp_GetProjectData", objsqlparam);
            return _ds;
        }
        public DataSet GetProjectDataDetailedForN1(string AFLCToken, int NominatedYear, int NominatedQuarter)
        {
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@AFLCToken", AFLCToken);
            objsqlparam[1] = new SqlParameter("@NominatedYear", NominatedYear);
            objsqlparam[2] = new SqlParameter("@NominatedQuarter", NominatedQuarter);
            _ds = objDALCommon.DAL_GetData("GetProjectDataDetailedForN1", objsqlparam);
            return _ds;
        }
        public DataSet GetProjectDataDetailedForN(string AFLCToken, int NominatedYear, int NominatedQuarter)
        {
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@AFLCToken", AFLCToken);
            objsqlparam[1] = new SqlParameter("@NominatedYear", NominatedYear);
            objsqlparam[2] = new SqlParameter("@NominatedQuarter", NominatedQuarter);
            _ds = objDALCommon.DAL_GetData("GetProjectDataDetailedForN", objsqlparam);
            return _ds;
        }
        public DataSet GetProjectDataForN(string AFLCToken, int NominatedYear, int NominatedQuarter)
        {
            SqlParameter[] objsqlparam = new SqlParameter[3];
            objsqlparam[0] = new SqlParameter("@AFLCToken", AFLCToken);
            objsqlparam[1] = new SqlParameter("@NominatedYear", NominatedYear);
            objsqlparam[2] = new SqlParameter("@NominatedQuarter", NominatedQuarter);
            _ds = objDALCommon.DAL_GetData("usp_GetProjectDataForN", objsqlparam);
            return _ds;
        }
        
        //Update Project Grid Data
        public int UpdateProjectAndNomineeData(BE_NominationColl objBE_NominationColl)
        {
            try
            { 
                int _status = 0;
                object _stat;
                foreach (BE_Nomination objBENomination in objBE_NominationColl)
                {
                    SqlParameter[] objsqlparam = new SqlParameter[8];
                    objsqlparam[0] = new SqlParameter("@MasterRefernceNumber", objBENomination.ReferenceNumber);
                    //objsqlparam[1] = new SqlParameter("@AwardAmount", objBENomination.AwardAmount);
                    if (objBENomination.AwardDecision == "Approve")
                    {
                        objBENomination.AwardDecision = "ApproveByN";
                    }
                    else
                        if (objBENomination.AwardDecision == "Reject")
                        {
                            objBENomination.AwardDecision = "RejectByN";
                        }
                    objsqlparam[1] = new SqlParameter("@AwardDecision", objBENomination.AwardDecision);
                    objsqlparam[2] = new SqlParameter("@BusinessUnit", objBENomination.BusinessUnit);
                    objsqlparam[3] = new SqlParameter("@Story", objBENomination.Story);
                    objsqlparam[4] = new SqlParameter("@ActionBy", objBENomination.ActionBy);
                    //objBENomination.TotalAmount = (Convert.ToInt32(objBENomination.AwardAmount) * Convert.ToInt32(objBENomination.EmployeeCount));
                    //objsqlparam[6] = new SqlParameter("@TotalAmount", objBENomination.TotalAmount);
                    objsqlparam[5] = new SqlParameter("@Remark", objBENomination.Remark);
                    _stat = objDALCommon.DAL_SaveData("usp_UpdateProjectAndNomineesData", objsqlparam, 'N');
                    _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                }
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public int UpdateProjectAndNomineeDataForN(BE_NominationColl objBE_NominationColl)
        {
            try
            {
                int _status = 0;
                object _stat;
                foreach (BE_Nomination objBENomination in objBE_NominationColl)
                {
                    SqlParameter[] objsqlparam = new SqlParameter[8];
                    objsqlparam[0] = new SqlParameter("@MasterRefernceNumber", objBENomination.ReferenceNumber);
                    objsqlparam[1] = new SqlParameter("@AwardAmount", objBENomination.AwardAmount);
                    objsqlparam[2] = new SqlParameter("@AwardDecision", objBENomination.AwardDecision);
                    objsqlparam[3] = new SqlParameter("@BusinessUnit", objBENomination.BusinessUnit);
                    objsqlparam[4] = new SqlParameter("@Story", objBENomination.Story);
                    objsqlparam[5] = new SqlParameter("@ActionBy", objBENomination.ActionBy);
                    objBENomination.TotalAmount = (Convert.ToInt32(objBENomination.AwardAmount) * Convert.ToInt32(objBENomination.EmployeeCount));
                    objsqlparam[6] = new SqlParameter("@TotalAmount", objBENomination.TotalAmount);
                    objsqlparam[7] = new SqlParameter("@Remark", objBENomination.Remark);
                    _stat = objDALCommon.DAL_SaveData("usp_UpdateProjectAndNomineesDataForN", objsqlparam, 'N');
                    _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                }
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //Update Nominee Data single
        public int UpdateNominee(BE_NominationColl objBE_NominationColl,int total,string decision)
        {
            try
            {
                int _status = 0;
                object _stat;
                foreach (BE_Nomination objBENomination in objBE_NominationColl)
                {
                    SqlParameter[] objsqlparam = new SqlParameter[10];
                    objsqlparam[0] = new SqlParameter("@Id", objBENomination.Id);
                    objsqlparam[1] = new SqlParameter("@MasterRefernceNumber", objBENomination.ReferenceNumber);
                    objsqlparam[2] = new SqlParameter("@RecipientToken", objBENomination.RecipientToken);
                    objsqlparam[3] = new SqlParameter("@AwardAmount", objBENomination.AwardAmount);
                    objsqlparam[4] = new SqlParameter("@AwardDecision", decision);
                    objsqlparam[5] = new SqlParameter("@BusinessUnit", objBENomination.BusinessUnit);
                    objsqlparam[6] = new SqlParameter("@Story", objBENomination.Story);
                    objsqlparam[7] = new SqlParameter("@ActionBy", objBENomination.ActionBy);
                    objsqlparam[8] = new SqlParameter("@TotalAmount", total);
                    objsqlparam[9] = new SqlParameter("@Remark", objBENomination.Remark);
                    _stat = objDALCommon.DAL_SaveData("spUpdateNominees", objsqlparam, 'N');
                    _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                }
                    return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        //Update Nominee Data single
        public int UpdateNomineeMaster(BE_Nomination objBENomination, int total, string decision)
        {
            try
            {
                int _status = 0;
                object _stat;
                SqlParameter[] objsqlparam = new SqlParameter[8];
                objsqlparam[0] = new SqlParameter("@MasterRefernceNumber", objBENomination.ReferenceNumber);
                objsqlparam[1] = new SqlParameter("@AwardAmount", objBENomination.AwardAmount);
                objsqlparam[2] = new SqlParameter("@AwardDecision", decision);
                objsqlparam[3] = new SqlParameter("@BusinessUnit", objBENomination.BusinessUnit);
                objsqlparam[4] = new SqlParameter("@Story", objBENomination.Story);
                objsqlparam[5] = new SqlParameter("@ActionBy", objBENomination.ActionBy);
                objsqlparam[6] = new SqlParameter("@TotalAmount", total);
                objsqlparam[7] = new SqlParameter("@Remark", objBENomination.Remark);
                _stat = objDALCommon.DAL_SaveData("usp_UpdateNomineeMaster", objsqlparam, 'N');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat); 
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

    }
}

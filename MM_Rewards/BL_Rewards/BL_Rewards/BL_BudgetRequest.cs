﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Rewards;
using MM.DAL;
using System.Data;
using System.Data.SqlClient;

namespace BL_Rewards.BL_Rewards
{
    public class BL_BudgetRequest
    {
        BE_BudgetRequestColl objBEBudgetRequestColl;
        DataSet ds;
        DAL_Common objDALCommon;
        // On selection of CE/HOD, Get the employee Details 
        public BE_BudgetRequestColl GetEmployee_ForBudgetRequest(BE_BudgetRequest objBEBudgetReq)
        {
            try
            {
                objBEBudgetRequestColl = new BE_BudgetRequestColl();
                ds = new DataSet();
                objDALCommon = new DAL_Common();
                
                SqlParameter[] objSQLParam = new SqlParameter[3];
                objSQLParam[0] = new SqlParameter("@SearchBy", objBEBudgetReq.SearchBy);
                objSQLParam[1] = new SqlParameter("@RequestorTokenNumber", objBEBudgetReq.RequestorTokenNumber);
                objSQLParam[2] = new SqlParameter("@IsCE", objBEBudgetReq.IsCE);
                ds = objDALCommon.DAL_GetData("usp_GetEmployee_ForBudgetRequest", objSQLParam);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_BudgetRequest objBEBudgetRequest = new BE_BudgetRequest();
                    objBEBudgetRequest.RequesteeTokenNumber = dr["TokenNumber"].ToString();
                    objBEBudgetRequest.FullName = dr["FullName"].ToString();
                    objBEBudgetRequest.Division = dr["DivisionName_PA"].ToString();
                    objBEBudgetRequest.Location = dr["LocationName_PSA"].ToString();
                    objBEBudgetRequest.Department = dr["OrgUnitName"].ToString();
                    objBEBudgetRequestColl.Add(objBEBudgetRequest);
                }
                return objBEBudgetRequestColl;  
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //To save the data in Budget_Request Table
        public int SaveBudgetRequest(BE_BudgetRequest objBEBudgetReq)
        {
            ds = new DataSet();
            objBEBudgetRequestColl = new BE_BudgetRequestColl();
            objDALCommon = new DAL_Common();

            SqlParameter[] objSQLparam = new SqlParameter[5];
            objSQLparam[0] = new SqlParameter("@RequestorTokenNumber", objBEBudgetReq.RequestorTokenNumber);
            objSQLparam[1] = new SqlParameter("@RequesteeTokenNumber", objBEBudgetReq.RequesteeTokenNumber);
            objSQLparam[2] = new SqlParameter("@IsCEHOD", objBEBudgetReq.IsCE);
            objSQLparam[3] = new SqlParameter("@RequestedAmount", objBEBudgetReq.RequestedAmount);
            objSQLparam[4] = new SqlParameter("@RequestReason", objBEBudgetReq.RequestReason);

            object _stat = objDALCommon.DAL_SaveData("usp_Insert_BudgetRequest", objSQLparam,'S');
            int _status = _stat == null ? 0 : Convert.ToInt32(_stat);

            return _status;
        
        }
        //To Get Mail details which is to send after Requesting
        public BE_BudgetRequest GetMailDetails(string TokenNumber)
        {
            try
            {
                BE_BudgetRequest objBEBudgetRequest = new BE_BudgetRequest();
                objBEBudgetRequestColl = new BE_BudgetRequestColl();
                ds = new DataSet();
                objDALCommon = new DAL_Common();

                SqlParameter[] objSQLParam = new SqlParameter[1];
                objSQLParam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                ds = objDALCommon.DAL_GetData("usp_GetMailDetail_TokenNumber", objSQLParam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBEBudgetRequest = new BE_BudgetRequest();
                    objBEBudgetRequest.RequesteeTokenNumber =  TokenNumber;
                    objBEBudgetRequest.FullName = dr["FullName"].ToString();
                    objBEBudgetRequest.EmailID = dr["EmailID"].ToString();
                    
                }
                return objBEBudgetRequest;
            }
            catch (Exception ex)
            { 
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

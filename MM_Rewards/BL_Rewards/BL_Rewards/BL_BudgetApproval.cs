﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Rewards;
using MM.DAL;
using System.Data;
using System.Data.SqlClient;

namespace BL_Rewards.BL_Rewards
{
    public class BL_BudgetApproval
    {
        DataSet ds;
        BE_BudgetRequestColl objBudgetReqColl;
        DAL_Common objDALCommon;

        public BE_BudgetRequestColl GetBudgetApprovalData(string RequestorTokenNumber)
        {
            try
            {
                 objBudgetReqColl = new BE_BudgetRequestColl();
                 ds = new DataSet();
                 objDALCommon = new DAL_Common();

                SqlParameter[] objSQLParam = new SqlParameter[1];
                objSQLParam[0] = new SqlParameter("@RequesteeTokenNumber", RequestorTokenNumber);
                ds = objDALCommon.DAL_GetData("usp_GetBudgetRequest_ForApproval", objSQLParam);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_BudgetRequest objBEBudgetRequest = new BE_BudgetRequest();
                    objBEBudgetRequest.ID = Convert.ToInt16(dr["ID"]);
                    objBEBudgetRequest.RequestedDate = dr["RequestedDate"].ToString();
                    objBEBudgetRequest.RequesteeTokenNumber = dr["RequesteeTokenNumber"].ToString();
                    objBEBudgetRequest.RequesteeName = dr["RequesteeName"].ToString();
                    objBEBudgetRequest.RequestorTokenNumber = dr["RequestorTokenNumber"].ToString();
                    objBEBudgetRequest.FullName = dr["RequestorName"].ToString();
                    objBEBudgetRequest.RequestedAmount = Convert.ToDecimal( dr["RequestedAmount"].ToString());
                    objBEBudgetRequest.RequestReason = dr["RequestReasons"].ToString();
                    int len = objBEBudgetRequest.RequestReason.Length;
                    objBEBudgetRequest.ShortText = (objBEBudgetRequest.RequestReason.Substring(0, len > 25 ? 25 : len)) + (len > 25 ? " ..." : "");
                    objBEBudgetRequest.IsCEHOD = dr["IsCEHOD"].ToString();
                    objBEBudgetRequest.Budget = Convert.ToDecimal(dr["BudgetBal"].ToString());

                    objBudgetReqColl.Add(objBEBudgetRequest);
                }
                return objBudgetReqColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int SaveBudgetApproval(BE_BudgetRequest objBEBudgetReq)
        {
            ds = new DataSet();
            objBudgetReqColl = new BE_BudgetRequestColl();
            objDALCommon = new DAL_Common();

            SqlParameter[] objSQLparam = new SqlParameter[5];
            objSQLparam[0] = new SqlParameter("@ID", objBEBudgetReq.ID);
            objSQLparam[1] = new SqlParameter("@IsApproved", objBEBudgetReq.IsApproved);
            objSQLparam[2] = new SqlParameter("@ApprovedBy", objBEBudgetReq.ApprovedBy);
            objSQLparam[3] = new SqlParameter("@ApprovedAmount", objBEBudgetReq.ApprovedAmount);
            objSQLparam[4] = new SqlParameter("@ApprovedReasons", objBEBudgetReq.ApprovedReason);

            object _stat = objDALCommon.DAL_SaveData("usp_Insert_BudgetApproval", objSQLparam, 'S');
            int _status = _stat == null ? 0 : Convert.ToInt32(_stat);

            return _status;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BE_Rewards.BE_Rewards;
using MM.DAL;
using System.Data.SqlClient;


namespace BL_Rewards.BL_Rewards
{
   public class BL_PersonalizedMessage
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        BE_PersonalizedMessage objBE_PersonalizedMessage;
        BE_PersonalizedMessageColl objBE_PersonalizedMessageColl;
       
        // Get Data for HOD
        public BE_PersonalizedMessageColl GetData(string TokenNumber)
        {
            objBE_PersonalizedMessageColl = new BE_PersonalizedMessageColl();
            SqlParameter[] objSqlParam = new SqlParameter[1];
            try
            {
            objSqlParam[0]=new SqlParameter("@HODTokenNumber",TokenNumber);
            ds = objDALCommon.DAL_GetData("usp_GetMileStone_PersonalizeMessage", objSqlParam);
            if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objBE_PersonalizedMessage = new BE_PersonalizedMessage();

                        objBE_PersonalizedMessage.ID = Convert.ToInt32(dr["ID"]);
                        objBE_PersonalizedMessage.RecipientTokenNumber=dr["RecipientTokenNumber"].ToString();
                        objBE_PersonalizedMessage.RecipientName=dr["RecipientName"].ToString();
                        objBE_PersonalizedMessage.Years=Convert.ToInt32(dr["Years"]);
                        objBE_PersonalizedMessage.MilestoneDate=dr["MileStoneDate"].ToString();
                        objBE_PersonalizedMessage.RNREmailID = dr["RNREmailID"].ToString();
                        objBE_PersonalizedMessageColl.Add(objBE_PersonalizedMessage);
                    }
                }
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return objBE_PersonalizedMessageColl;
        }
        //save Personalized message
        public int save(BE_PersonalizedMessage objBE_PersonalizedMessage)
        {
            try
            {
                object _stat;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[3];

                objSqlParam[0] = new SqlParameter("@ID", objBE_PersonalizedMessage.ID);
                objSqlParam[1] = new SqlParameter("@HODTokenNumber", objBE_PersonalizedMessage.HodTokenNumber);
                objSqlParam[2] = new SqlParameter("@PersonalizedMessage", objBE_PersonalizedMessage.PersonalizedMessage);
                _stat = objDALCommon.DAL_SaveData("usp_Insert_PersonalizeMessage", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

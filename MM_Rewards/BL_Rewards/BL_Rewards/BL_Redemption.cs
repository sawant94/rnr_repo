﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using BE_Rewards.BE_Rewards;

namespace BL_Rewards.Admin
{
    public sealed class BL_Redemption
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        BE_Redemption objBE_Redemption;
        BE_RedemptionColl objBE_RedemptionColl;

        public BE_RedemptionColl GetData(string strStartDate, string strEndDate, string RecipientTokenNumber = null)
        {

            objBE_RedemptionColl = new BE_RedemptionColl();
            SqlParameter[] objSqlParam;
            try
            {
                ds = new DataSet();

                if (RecipientTokenNumber != null)
                {
                    objSqlParam = new SqlParameter[3];

                    objSqlParam[0] = new SqlParameter("@StartDate", strStartDate);
                    objSqlParam[1] = new SqlParameter("@EndDate", strEndDate);
                    objSqlParam[2] = new SqlParameter("@RecipientTokenNumber", RecipientTokenNumber);

                }
                else
                {
                    objSqlParam = new SqlParameter[2];

                    objSqlParam[0] = new SqlParameter("@StartDate", strStartDate);
                    objSqlParam[1] = new SqlParameter("@EndDate", strEndDate);

                }

                ds = objDALCommon.DAL_GetData("usp_Get_RecipientAwards", objSqlParam);

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objBE_Redemption = new BE_Redemption();

                        objBE_Redemption.ID = Convert.ToInt32(dr["ID"]);
                        objBE_Redemption.AwardID = Convert.ToString(dr["AwardID"]);
                        objBE_Redemption.AwardedDate = Convert.ToDateTime(dr["AwardedDate"]);
                        objBE_Redemption.GiverName = dr["GiverName"].ToString();
                        objBE_Redemption.RecipientName = dr["RecipientName"].ToString();
                        objBE_Redemption.AwardType = dr["AwardName"].ToString();
                        objBE_Redemption.AwardAmount = Convert.ToDecimal(dr["AwardAmount"]);
                        objBE_Redemption.ReasonForNomination = dr["ReasonForNomination"].ToString();
                        objBE_Redemption.RedeemType = dr["RedeemType"].ToString() != "" ? (dr["RedeemType"].ToString() == "C" ? "Salary" : "Kwench") : "";
                        objBE_Redemption.RedeemDate = dr["RedeemDate"].ToString() == "" ? (DateTime?)null : Convert.ToDateTime(dr["RedeemDate"]);
                        objBE_Redemption.ExpiryDate = dr["ExpiryDate"].ToString() == "" ? (DateTime?)null : Convert.ToDateTime(dr["ExpiryDate"]);
                        objBE_Redemption.Status = dr["Status"].ToString() == "" ? (Int32?)null : Convert.ToInt32(dr["Status"]);


                        objBE_Redemption.Feedback = dr["Feedback"].ToString(); //by vandana

                        objBE_RedemptionColl.Add(objBE_Redemption);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return objBE_RedemptionColl;
        }



        //megha 

        public void GetEmployeeResignStatus(string TokenNumber)
        {

            objBE_RedemptionColl = new BE_RedemptionColl();
            SqlParameter[] objSqlParam;

            try
            {
                ds = new DataSet();
                objSqlParam = new SqlParameter[1];

                objSqlParam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                ds = objDALCommon.DAL_GetData("usp_Get_ResignStatus", objSqlParam);
                int ResignStatus = 0;
                // if (ds.Tables[0].Rows[0].ToString() == "1")
                // {
                // return 1;
                // }
                //  else
                // {
                //  return 0;
                //}
                // return ResignStatus;
            }

            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
                //return 9;
            }

        }

        public BE_RedemptionColl GetDataForHOD(string strStartDate, string strEndDate, string HODTokenNumber = null, bool Revert = false)
        {

            objBE_RedemptionColl = new BE_RedemptionColl();
            SqlParameter[] objSqlParam;
            try
            {
                ds = new DataSet();

                if (HODTokenNumber != null)
                {
                    objSqlParam = new SqlParameter[4];

                    objSqlParam[0] = new SqlParameter("@StartDate", strStartDate);
                    objSqlParam[1] = new SqlParameter("@EndDate", strEndDate);
                    objSqlParam[2] = new SqlParameter("@HODTokenNumber", HODTokenNumber);
                    objSqlParam[3] = new SqlParameter("@Revert", Revert);

                }
                else
                {
                    objSqlParam = new SqlParameter[3];

                    objSqlParam[0] = new SqlParameter("@StartDate", strStartDate);
                    objSqlParam[1] = new SqlParameter("@EndDate", strEndDate);
                    objSqlParam[2] = new SqlParameter("@Revert", Revert);

                }

                // ds = objDALCommon.DAL_GetData("usp_Get_AwardsByMe", objSqlParam);
                ds = objDALCommon.DAL_GetData("usp_Get_AwardsByMeForRepoter", objSqlParam);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objBE_Redemption = new BE_Redemption();

                        objBE_Redemption.ID = Convert.ToInt32(dr["ID"]);
                        objBE_Redemption.AwardedDate = Convert.ToDateTime(dr["AwardedDate"]);
                        objBE_Redemption.GiverTokenNumber = dr["GiverTokenNumber"].ToString();
                        objBE_Redemption.GiverName = dr["GiverName"].ToString();
                        objBE_Redemption.RecipientTokenNumber = dr["RecipientTokenNumber"].ToString();
                        objBE_Redemption.RecipientName = dr["RecipientName"].ToString();
                        objBE_Redemption.AwardType = dr["Awardtype"].ToString();
                        objBE_Redemption.AwardAmount = Convert.ToDecimal(dr["AwardAmount"]);
                        objBE_Redemption.ReasonForNomination = dr["ReasonForNomination"].ToString();
                        //objBE_Redemption.ReasonOnCertificate = dr["ReasonOnCertificate"].ToString();                 
                        objBE_Redemption.RedeemDate = dr["RedeemDate"].ToString() == "" ? (DateTime?)null : Convert.ToDateTime(dr["RedeemDate"]);
                        objBE_Redemption.EmailedDate = dr["EmailedDate"].ToString() == "" ? (DateTime?)null : Convert.ToDateTime(dr["EmailedDate"]);
                        objBE_Redemption.IsAuto = Convert.ToBoolean(dr["IsAuto"]);
                        objBE_Redemption.Giver_IsAdmin = Convert.ToBoolean(dr["Giver_IsAdmin"]);

                        objBE_RedemptionColl.Add(objBE_Redemption);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return objBE_RedemptionColl;
        }



        public void RevertAward(int ID)
        {
            SqlParameter[] objSqlParams = new SqlParameter[1];
            objSqlParams[0] = new SqlParameter("@ID", ID);
            objDALCommon.DAL_SaveData("usp_Delete_RecipientAwards", objSqlParams, 'N');
        }

        public void SaveData(BE_Redemption objBE_Redemption)
        {
            try
            {
                SqlParameter[] objSqlParams = new SqlParameter[3];
                objSqlParams[0] = new SqlParameter("@ID", objBE_Redemption.ID);
                objSqlParams[1] = new SqlParameter("@RedeemType", objBE_Redemption.RedeemType);
                objSqlParams[2] = new SqlParameter("@Feedback", objBE_Redemption.Feedback);

                objDALCommon.DAL_SaveData("usp_Insert_RecipientAwards_Redeem", objSqlParams, 'N');
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void SaveDataClosure(BE_AwardClosure objBE_AwardClosure, BE_Redemption objBE_Redemption = null)
        {
            try
            {

                SqlParameter[] objSqlParams = new SqlParameter[6];
                if (objBE_Redemption != null)
                {
                    objSqlParams[4] = new SqlParameter("@ClosingAmount", objBE_Redemption.ClosingAmount);
                    objSqlParams[5] = new SqlParameter("@ClosingDate", objBE_Redemption.ClosureDate);
                }
                else
                {
                    objSqlParams[4] = new SqlParameter("@ClosingAmount", DBNull.Value);
                    objSqlParams[5] = new SqlParameter("@ClosingDate", DBNull.Value);
                }

                objSqlParams[0] = new SqlParameter("@AwardID", objBE_AwardClosure.AwardID);
                objSqlParams[2] = new SqlParameter("@Name", objBE_AwardClosure.FileName);
                objSqlParams[3] = new SqlParameter("@Status", objBE_AwardClosure.Status);
                objSqlParams[1] = new SqlParameter("@Remarks", objBE_AwardClosure.Remarks);

                objDALCommon.DAL_SaveData("usp_Update_Log_RecipientAwards_Closure", objSqlParams, 'N');
            }
            catch (Exception ex)
            {

                throw new ArgumentException(ex.Message);
            }
        }

        public BE_AwardClosureColl GetDataClosure()
        {
            BE_AwardClosureColl objBE_AwardClosureColl = new BE_AwardClosureColl();
            BE_AwardClosure objBE_AwardClosure;
            try
            {
                ds = new DataSet();
                ds = objDALCommon.DAL_GetData("usp_Get_Log_RecipientAwards_Closure");

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objBE_AwardClosure = new BE_AwardClosure();

                        //objBE_Redemption.ID = Convert.ToInt32(dr["ID"]);
                        objBE_AwardClosure.AwardID = Convert.ToString(dr["AwardID"]);
                        objBE_AwardClosure.FileName = dr["Name"].ToString();
                        objBE_AwardClosure.Status = dr["Status"].ToString();
                        objBE_AwardClosure.Remarks = dr["Remarks"].ToString();
                        objBE_AwardClosure.RecipientTokenNumber = Convert.ToString(dr["RecipientTokenNumber"]);
                        objBE_AwardClosure.RecipientName = dr["RecipientName"].ToString();
                        objBE_AwardClosureColl.Add(objBE_AwardClosure);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return objBE_AwardClosureColl;
        }

        public DataSet GetDataDownloadExcel(string strStartDate, string strEndDate, string HODTokenNumber, string AwardType, bool Revert = false)
        {
            SqlParameter[] objSqlParam;
            try
            {
                ds = new DataSet();
                if (HODTokenNumber != null)
                {
                    objSqlParam = new SqlParameter[4];
                    objSqlParam[0] = new SqlParameter("@StartDate", strStartDate);
                    objSqlParam[1] = new SqlParameter("@EndDate", strEndDate);
                    objSqlParam[2] = new SqlParameter("@HODTokenNumber", HODTokenNumber);
                    objSqlParam[3] = new SqlParameter("@Revert", Revert);
                }
                else
                {
                    objSqlParam = new SqlParameter[3];
                    objSqlParam[0] = new SqlParameter("@StartDate", strStartDate);
                    objSqlParam[1] = new SqlParameter("@EndDate", strEndDate);
                    objSqlParam[2] = new SqlParameter("@Revert", Revert);
                }
                if (AwardType == "")
                    ds = objDALCommon.DAL_GetData("usp_Get_AwardsByMeForRepoter", objSqlParam);
                else if (AwardType == "")
                    ds = objDALCommon.DAL_GetData("usp_Get_AwardsByMeForOtherRecipient", objSqlParam);
                else if (AwardType == "")
                    ds = objDALCommon.DAL_GetData("usp_Get_AwardsByMeForRevertAward", objSqlParam);
                else
                    ds = objDALCommon.DAL_GetData("usp_Get_AwardsByMeForRepoterEcard", objSqlParam);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return ds;
        }

    }
}

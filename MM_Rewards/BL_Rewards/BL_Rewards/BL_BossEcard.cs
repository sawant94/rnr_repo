﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MM.DAL;
using BE_Rewards.BE_Rewards;

namespace BL_Rewards.BL_Rewards
{
    public class BL_BossEcard
    {
        DataSet _ds;
        DAL_Common objDALCommon;
        BE_Ecard objBeEcard;

        public int GetBossEcardCount(string TokenNumber)
        {
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);

            objDALCommon = new DAL_Common();
            _ds = objDALCommon.DAL_GetData("usp_GetBossEcardCountByTokenNumber", objsqlparam);
            int Count = Convert.ToInt16(_ds.Tables[0].Rows[0][0].ToString());

            return Count;
        }

        public BE_EcardCollection GetBossEcardByTokenNumber(string TokenNumber)
        {
            _ds = new DataSet();
            BE_EcardCollection EcardColl = new BE_EcardCollection();
            DataTable _dt = new DataTable();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);


            objDALCommon = new DAL_Common();
            _ds = objDALCommon.DAL_GetData("usp_GetBossEcardByTokenNumber", objsqlparam);
            _dt = _ds.Tables[0];
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow _dr in _dt.Rows)
                {
                    objBeEcard = new BE_Ecard();
                    objBeEcard.ID = Convert.ToInt16(_dr["ID"]);
                    objBeEcard.RecipientTokenNumber = _dr["TokenNumber"].ToString();
                    objBeEcard.UserName = _dr["Name"].ToString();
                    objBeEcard.Message = _dr["Message"].ToString();
                    objBeEcard.CardType = _dr["CardType"].ToString();
                    objBeEcard.Date = Convert.ToDateTime(_dr["CardSentDate"].ToString());
                    EcardColl.Add(objBeEcard);
                }
            }
            return EcardColl;
        }
        
        public int DeleteData(int id)
        {
            try
            {
                object _stat;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@ID", id);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_DeleteBossEcard", objSqlParam, 'N');

                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        public BE_RecipientAwardColl GetRecipientList(string RecipientName, string HODTokenNumber)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_RecipientAwardColl objBERecipientAwardColl = new BE_RecipientAwardColl();

                SqlParameter[] objsqlparam = new SqlParameter[3];
                objsqlparam[0] = new SqlParameter("@RecipientName", RecipientName);
                objsqlparam[1] = new SqlParameter("@TokenNumber", HODTokenNumber);

                _ds = objDALCommon.DAL_GetData("usp_GetRecipientDetailsforBossEcard", objsqlparam);


                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_RecipientAward objBERecipientAward = new BE_RecipientAward();
                    objBERecipientAward.TokenNumber = _dr["TokenNumber"].ToString();
                    objBERecipientAward.FirstName = _dr["FirstName"].ToString();
                    objBERecipientAward.LastName = _dr["LastName"].ToString();
                    objBERecipientAward.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                    objBERecipientAward.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBERecipientAward.Designation = _dr["Designation"].ToString();
                    objBERecipientAward.EmailID = _dr["EmailID"].ToString();
                    objBERecipientAward.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBERecipientAward.OrgUnitName = _dr["OrgUnitName"].ToString();
                    objBERecipientAward.ProcessName = _dr["ProcessName"].ToString();
                    objBERecipientAwardColl.Add(objBERecipientAward);
                }
                return objBERecipientAwardColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        
        public int SaveData(BE_Ecard ecard)
        {
            try
            {
                int _status = 0;
                object _stat;

                _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[4];
                objsqlparam[0] = new SqlParameter("@RecipientTokenNumber", ecard.RecipientTokenNumber);
                objsqlparam[1] = new SqlParameter("@GiverTokenNumber", ecard.GiverTokenNUmber);
                objsqlparam[2] = new SqlParameter("@Message", ecard.Message);
                objsqlparam[3] = new SqlParameter("@CardType", ecard.CardType);

                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_InsertBossEcard", objsqlparam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);

                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_Ecard GetBossDayEcardByID(int ID)
        {
            _ds = new DataSet();
            BE_Ecard objBeEcard = new BE_Ecard();

            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@ID", ID);


            objDALCommon = new DAL_Common();
            _ds = objDALCommon.DAL_GetData("usp_GetBossDayEcardByID", objsqlparam);

            if (_ds.Tables[0].Rows.Count > 0)
            {
                objBeEcard.ID = (int)_ds.Tables[0].Rows[0]["ID"];
                objBeEcard.RecipientTokenNumber = (string)_ds.Tables[0].Rows[0]["TokenNumber"];
                objBeEcard.GiverTokenNUmber = (string)_ds.Tables[0].Rows[0]["GiverTokenNumber"];
                objBeEcard.Message = (string)_ds.Tables[0].Rows[0]["Message"];
                objBeEcard.CardType = (string)_ds.Tables[0].Rows[0]["CardType"];
                objBeEcard.UserName = (string)_ds.Tables[0].Rows[0]["UserName"];
            }
            return objBeEcard;
        }
    }
}

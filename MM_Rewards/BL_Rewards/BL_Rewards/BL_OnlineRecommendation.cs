﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Rewards;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;

namespace BL_Rewards.BL_Rewards
{
    public class BL_OnlineRecommendation
    {
        BE_OnlineRecommendationColl objBEOnlineRecommendationColl;
        DataSet ds, _ds;
        DAL_Common objDALCommon;
        // On selection of CE/HOD, Get the employee Details 
        public BE_OnlineRecommendationColl GetEmployee_ForOnlineRecommendation(BE_OnlineRecommendation objBEOnlineRecom)
        {
            try
            {
                objBEOnlineRecommendationColl = new BE_OnlineRecommendationColl();
                ds = new DataSet();
                objDALCommon = new DAL_Common();

                SqlParameter[] objSQLParam = new SqlParameter[3];
                objSQLParam[0] = new SqlParameter("@SearchBy", objBEOnlineRecom.SearchBy);
                objSQLParam[1] = new SqlParameter("@RequestorTokenNumber", objBEOnlineRecom.RequestorTokenNumber);
                objSQLParam[2] = new SqlParameter("@IsCE", objBEOnlineRecom.IsCE);
                ds = objDALCommon.DAL_GetData("usp_GetEmployee_ForOnlineRecommendation", objSQLParam);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_OnlineRecommendation objBEOnlineRecommendation = new BE_OnlineRecommendation();
                    objBEOnlineRecommendation.RequesteeTokenNumber = dr["TokenNumber"].ToString();
                    objBEOnlineRecommendation.FullName = dr["FullName"].ToString();
                    objBEOnlineRecommendation.Division = dr["DivisionName_PA"].ToString();
                    objBEOnlineRecommendation.Location = dr["LocationName_PSA"].ToString();
                    objBEOnlineRecommendation.Department = dr["OrgUnitName"].ToString();
                    objBEOnlineRecommendation.Levels = dr["Levels"].ToString();
                    objBEOnlineRecommendationColl.Add(objBEOnlineRecommendation);
                }
                return objBEOnlineRecommendationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        // On selection of one HOd employee, Get the other employee Details 
        public BE_OnlineRecommendationColl GetEmployee_ForOnlineRecommendation_other(BE_OnlineRecommendation objBEOnlineRecom)
        {
            try
            {
                objBEOnlineRecommendationColl = new BE_OnlineRecommendationColl();
                ds = new DataSet();
                objDALCommon = new DAL_Common();

                SqlParameter[] objSQLParam = new SqlParameter[3];
                objSQLParam[0] = new SqlParameter("@SearchBy", objBEOnlineRecom.SearchBy);
                objSQLParam[1] = new SqlParameter("@RequestorTokenNumber", objBEOnlineRecom.RequestorTokenNumber);
                objSQLParam[2] = new SqlParameter("@IsCE", objBEOnlineRecom.IsCE);
                ds = objDALCommon.DAL_GetData("usp_GetEmployee_ForOnlineRecommendation_other", objSQLParam);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_OnlineRecommendation objBEOnlineRecommendation = new BE_OnlineRecommendation();
                    objBEOnlineRecommendation.RequesteeTokenNumber = dr["TokenNumber"].ToString();
                    objBEOnlineRecommendation.FullName = dr["FullName"].ToString();
                    objBEOnlineRecommendation.Division = dr["DivisionName_PA"].ToString();
                    objBEOnlineRecommendation.Location = dr["LocationName_PSA"].ToString();
                    objBEOnlineRecommendation.Department = dr["OrgUnitName"].ToString();
                    objBEOnlineRecommendationColl.Add(objBEOnlineRecommendation);
                }
                return objBEOnlineRecommendationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //To Get Recipient Data, AfterSelecting specific Value
        public BE_OnlineRecommendation GetRecipientDetails(string TokenNumber)
        {
            try
            {
                _ds = new DataSet();
                objDALCommon = new DAL_Common();
                BE_OnlineRecommendation objBEOnlineRecommendation = new BE_OnlineRecommendation();
                DataTable _dt = new DataTable();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_RecipientAwards_View", objsqlparam);
                _dt = _ds.Tables[0];
                foreach (DataRow _dr in _dt.Rows)
                {
                    objBEOnlineRecommendation.UserName = _dr["UserName"].ToString();
                    objBEOnlineRecommendation.ProcessName = _dr["ProcessName"].ToString();
                    objBEOnlineRecommendation.LocationCode_PSA = _dr["Location_PSA"].ToString();
                    objBEOnlineRecommendation.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBEOnlineRecommendation.OrgUnitName = _dr["OrgUnitName"].ToString();
                    objBEOnlineRecommendation.Designation = _dr["Designation"].ToString();
                    objBEOnlineRecommendation.EmployeeLevelName_ES = _dr["EmployeeLevelName_ES"].ToString();
                    objBEOnlineRecommendation.DivisionCode_PA = _dr["Division_PA"].ToString();
                    objBEOnlineRecommendation.DivisionName_PA = _dr["DivisionName_PA"].ToString();
                    objBEOnlineRecommendation.Levels = _dr["Levels"].ToString();
                }
                return objBEOnlineRecommendation;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int SaveData(BE_OnlineRecommendation objBEOnlineRecommendation)
        {
            try
            {
                int _status = 0;
                object _stat;
                SqlParameter[] objsqlparam = new SqlParameter[8];
                objsqlparam[0] = new SqlParameter("@HOD", objBEOnlineRecommendation.SearchBy);
                objsqlparam[1] = new SqlParameter("@RecommendHOD", objBEOnlineRecommendation.RequesteeTokenNumber);
                objsqlparam[2] = new SqlParameter("@ReceiverEmpToken", objBEOnlineRecommendation.TokenNumber);
                objsqlparam[3] = new SqlParameter("@IsCEHOD", objBEOnlineRecommendation.IsCEHOD);
                objsqlparam[4] = new SqlParameter("@AwardType", objBEOnlineRecommendation.AwardType);
                objsqlparam[5] = new SqlParameter("@AwardAmt", objBEOnlineRecommendation.AwardAmount);
                objsqlparam[6] = new SqlParameter("@ReasonOfNomination", objBEOnlineRecommendation.ReasonForNomination);
                objsqlparam[7] = new SqlParameter("@LoginID", objBEOnlineRecommendation.GiverTokenNumber);

                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Insert_OnlineRecom", objsqlparam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);

                //AwardIDR = (string)objsqlparam[10].Value;
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public DataSet GetMailDetails(string HODTokenNumber, string RecommendedHODToken)
        {
            try
            {
                BE_OnlineRecommendation objBEOnlineRecommendation = new BE_OnlineRecommendation();
                objBEOnlineRecommendationColl = new BE_OnlineRecommendationColl();
                ds = new DataSet();
                objDALCommon = new DAL_Common();

                SqlParameter[] objSQLParam = new SqlParameter[2];
                objSQLParam[0] = new SqlParameter("@HODTokenNumber", HODTokenNumber);
                objSQLParam[1] = new SqlParameter("@RecommendedTokenNumber", RecommendedHODToken);
                ds = objDALCommon.DAL_GetData("usp_GetMailDetails_Recommend", objSQLParam);
                //foreach (DataRow dr in ds.Tables[0].Rows)
                //{
                //    objBEOnlineRecommendation = new BE_OnlineRecommendation();
                //    objBEOnlineRecommendation.FullName = dr["FullName"].ToString();
                //    objBEOnlineRecommendation.EmailID = dr["EmailID"].ToString();
                //}
                return ds;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_OnlineRecommendationColl GetEmployeeApprovalData(string RecHODTokenNumber)
        {
            try
            {
                objBEOnlineRecommendationColl = new BE_OnlineRecommendationColl();
                ds = new DataSet();
                objDALCommon = new DAL_Common();

                SqlParameter[] objSQLParam = new SqlParameter[1];
                objSQLParam[0] = new SqlParameter("@RecomHODToken", RecHODTokenNumber);
                ds = objDALCommon.DAL_GetData("usp_GetOnlineRecom_ForApproval", objSQLParam);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_OnlineRecommendation objBEOnlineRecommendation = new BE_OnlineRecommendation();
                    objBEOnlineRecommendation.ID = Convert.ToInt16(dr["ID"]);
                    objBEOnlineRecommendation.RecHODEMAIL = dr["RecommendHODEmail"].ToString();
                    objBEOnlineRecommendation.RequesteeName = dr["RequesteeName"].ToString();
                    //objBEOnlineRecommendation.RequestedDate = dr["RequestedDate"].ToString();
                    objBEOnlineRecommendation.RequestorTokenNumber = dr["RequestorTokenNumber"].ToString();
                    objBEOnlineRecommendation.FullName = dr["RequestorName"].ToString();
                    objBEOnlineRecommendation.EmailID = dr["RequestorHODEmail"].ToString();
                    objBEOnlineRecommendation.TokenNumber = dr["ReceiverEmpToken"].ToString();
                    objBEOnlineRecommendation.RecommendedEmployee = dr["RecommendedName"].ToString();
                    objBEOnlineRecommendation.AwardType = dr["AwardType"].ToString();
                    objBEOnlineRecommendation.AwardAmount = Convert.ToDecimal(dr["AwardAmt"].ToString());
                    objBEOnlineRecommendation.AwardedDate = dr["AwardedDate"].ToString();
                    objBEOnlineRecommendation.RequestReason = dr["ResonOfNomination"].ToString();
                    int len = objBEOnlineRecommendation.RequestReason.Length;
                    objBEOnlineRecommendation.RequestReason = (objBEOnlineRecommendation.RequestReason.Substring(0, len > 25 ? 25 : len)) + (len > 25 ? " ..." : "");
                    objBEOnlineRecommendation.IsCEHOD = dr["IsCEHOD"].ToString();

                    objBEOnlineRecommendationColl.Add(objBEOnlineRecommendation);
                }
                return objBEOnlineRecommendationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int SaveRecommendationApproval(BE_OnlineRecommendation objBEOnlineRecommendation)
        {
            ds = new DataSet();
            objBEOnlineRecommendationColl = new BE_OnlineRecommendationColl();
            objDALCommon = new DAL_Common();

            SqlParameter[] objSQLparam = new SqlParameter[5];
            objSQLparam[0] = new SqlParameter("@ID", objBEOnlineRecommendation.ID);
            objSQLparam[1] = new SqlParameter("@IsApproved", objBEOnlineRecommendation.IsApproved);
            objSQLparam[2] = new SqlParameter("@ApprovedBy", objBEOnlineRecommendation.ApprovedBy);
            objSQLparam[4] = new SqlParameter("@ApprovedReason", objBEOnlineRecommendation.ApprovedReason);

            object _stat = objDALCommon.DAL_SaveData("usp_Insert_EmpRecommendationApproval", objSQLparam, 'S');
            int _status = _stat == null ? 0 : Convert.ToInt32(_stat);

            return _status;

        }
    }
}

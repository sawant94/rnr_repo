﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;
using System.Web;
namespace BL_Rewards.Admin
{
    public sealed class BL_RecipientClosure
    {
        DataSet _ds;
        DAL_Common objDALCommon = new DAL_Common();
        // to save data 
        public int savedata(BE_RecipientClosureColl objBEClosureColl)
        {
            int Id = 0;
            object _stat;
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID", typeof(int));
                dt.Columns.Add("closingDate", typeof(DateTime));
                dt.Columns.Add("amount", typeof(decimal));

                foreach (BE_RecipientClosure objBEClosure in objBEClosureColl)
                {
                    DataRow dr = dt.NewRow();
                    dr["ID"] = objBEClosure.Id;
                    dr["closingdate"] = objBEClosure.ClosingDate;
                    dr["amount"] = objBEClosure.closingAmount;
                    dt.Rows.Add(dr);
                }


                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@RecipientAwards_Closer", dt);
                _stat = objDALCommon.DAL_SaveData("usp_RecipientAwards_Closer_Write", objSqlParam, 'S');
                Id = _stat == null ? 0 : Convert.ToInt32(_stat);
                return Id;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
                   
                
        }
        //to get data for Gridview
        public BE_RecipientClosureColl GetCloserData(string strStartDate, string strEndDate)
        {
            try
            {
                _ds = new DataSet();
                SqlParameter[] objSqlParam = new SqlParameter[2];

                objSqlParam[0] = new SqlParameter("@StartDate", strStartDate);
                objSqlParam[1] = new SqlParameter("@EndDate", strEndDate);
                _ds = objDALCommon.DAL_GetData("usp_RecipientAwards_Closer_View",objSqlParam);
                BE_RecipientClosureColl objBEClosureColl = new BE_RecipientClosureColl();
            
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_RecipientClosure objBEClosure = new BE_RecipientClosure();
                    objBEClosure.Id = Convert.ToInt32(_dr["ID"].ToString());
                    objBEClosure.Awardid = _dr["AwardID"].ToString();
                    objBEClosure.Tokennumber = _dr["RecipientTokenNumber"].ToString();
                    objBEClosure.RecipientName = _dr["RecipientName"].ToString();
                    objBEClosure.AwardedDate = _dr["AwardedDate"].ToString();
                    objBEClosure.AwardType = _dr["AwardType"].ToString();
                    objBEClosure.ReasonForNomination = _dr["ReasonForNomination"].ToString();
                    objBEClosure.Amount = Convert.ToInt32(_dr["AwardAmount"].ToString());
                    objBEClosure.RedeemedDate = _dr["RedeemDate"].ToString() == "" ? "" : Convert.ToDateTime(_dr["RedeemDate"]).ToString("dd MMM yyyy");
                    objBEClosure.RedeemedType = _dr["RedeemType"].ToString();

                    objBEClosureColl.Add(objBEClosure);

                }
                return objBEClosureColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
                        
        }

        //to get data for Gridview
        public DataSet GetCloserDataSet()
        {
            try
            {
                _ds = new DataSet();
                _ds = objDALCommon.DAL_GetData("usp_RecipientAwards_Closer_View");
                //BE_RecipientClosureColl objBEClosureColl = new BE_RecipientClosureColl();

                //foreach (DataRow _dr in _ds.Tables[0].Rows)
                //{
                //    BE_RecipientClosure objBEClosure = new BE_RecipientClosure();
                //    objBEClosure.Id = Convert.ToInt32(_dr["ID"].ToString());
                //    objBEClosure.Awardid = _dr["AwardID"].ToString();
                //    objBEClosure.Tokennumber = _dr["RecipientTokenNumber"].ToString();
                //    objBEClosure.RecipientName = _dr["RecipientName"].ToString();
                //    objBEClosure.AwardedDate = _dr["AwardedDate"].ToString();
                //    objBEClosure.AwardType = _dr["AwardType"].ToString();
                //    objBEClosure.ReasonForNomination = _dr["ReasonForNomination"].ToString();
                //    objBEClosure.Amount = Convert.ToInt32(_dr["AwardAmount"].ToString());
                //    objBEClosure.RedeemedDate = _dr["RedeemDate"].ToString() == "" ? "" : Convert.ToDateTime(_dr["RedeemDate"]).ToString("dd MMM yyyy");
                //    objBEClosure.RedeemedType = _dr["RedeemType"].ToString();

                //    objBEClosureColl.Add(objBEClosure);

                //}
                return _ds;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        public List<BE_RecipientClosure> GetCloserDataForExcel()
        {
            string StartDate;
            string EndDate;
            try
            {
                _ds = new DataSet();
                StartDate = HttpContext.Current.Session["StartDate"].ToString();
                EndDate = HttpContext.Current.Session["EndDate"].ToString();
                SqlParameter[] objSqlParam = new SqlParameter[2];

                objSqlParam[0] = new SqlParameter("@StartDate", StartDate);
                objSqlParam[1] = new SqlParameter("@EndDate", EndDate);
                _ds = objDALCommon.DAL_GetData("usp_RecipientAwards_Closer_View",objSqlParam);
                List<BE_RecipientClosure> objBEClosureColl = new List<BE_RecipientClosure>();


                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_RecipientClosure objBEClosure = new BE_RecipientClosure();
                    objBEClosure.Id = Convert.ToInt32(_dr["ID"].ToString());
                    objBEClosure.Awardid = _dr["AwardID"].ToString();
                    objBEClosure.Tokennumber = _dr["RecipientTokenNumber"].ToString();
                    objBEClosure.RecipientName = _dr["RecipientName"].ToString();
                    objBEClosure.AwardedDate = _dr["AwardedDate"].ToString();
                    objBEClosure.AwardType = _dr["AwardType"].ToString();
                    objBEClosure.ReasonForNomination = _dr["ReasonForNomination"].ToString();
                    objBEClosure.Amount = Convert.ToInt32(_dr["AwardAmount"].ToString());
                    objBEClosure.RedeemedDate = _dr["RedeemDate"].ToString() == "" ? "" : Convert.ToDateTime(_dr["RedeemDate"]).ToString("dd MMM yyyy");
                    objBEClosure.RedeemedType = _dr["RedeemType"].ToString();

                    objBEClosureColl.Add(objBEClosure);

                }
              
                return objBEClosureColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }


        }
            
    }
}

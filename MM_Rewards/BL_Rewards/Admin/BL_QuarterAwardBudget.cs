﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MM.DAL;
using System.Data.SqlClient;
using BE_Rewards.BE_Admin;

namespace BL_Rewards.Admin
{
    public class BL_QuarterAwardBudget
    {
        DAL_Common objDALCommon = new DAL_Common();

        public DataSet GetAFLCMember()
        {
            DataSet _ds = new DataSet();
            _ds = objDALCommon.DAL_GetData("spGetAFLCMemberForBudget");
            return _ds;
        }

        public int SaveData(BE_QuarterAwardBudget objBE_QuarterAwardBudget)
        {
            try
            {
                int Id = 0;
                SqlParameter[] objSqlParam = new SqlParameter[10];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_QuarterAwardBudget.TokenNumber);
                objSqlParam[1] = new SqlParameter("@StartDate", objBE_QuarterAwardBudget.StartDate);
                objSqlParam[2] = new SqlParameter("@EndDate", objBE_QuarterAwardBudget.EndDate);
                objSqlParam[3] = new SqlParameter("@AllocatedBudget", objBE_QuarterAwardBudget.AllocatedBudget);
                objSqlParam[4] = new SqlParameter("@UtilizedBudget", objBE_QuarterAwardBudget.UtilizedBudget);
                objSqlParam[5] = new SqlParameter("@RemaningBudget", objBE_QuarterAwardBudget.RemaningBudget);
                objSqlParam[6] = new SqlParameter("@BudgetType", objBE_QuarterAwardBudget.BudgetType);
                objSqlParam[7] = new SqlParameter("@Quarter", objBE_QuarterAwardBudget.Quarter);
                objSqlParam[8] = new SqlParameter("@CreatedBy", objBE_QuarterAwardBudget.CreatedBy);
                objSqlParam[9] = new SqlParameter("@IsActive", objBE_QuarterAwardBudget.IsActive);

                Id = (int)objDALCommon.DAL_SaveData("usp_InsertQuarterAwardBudget", objSqlParam, 'N');

                return Id;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


    }
}

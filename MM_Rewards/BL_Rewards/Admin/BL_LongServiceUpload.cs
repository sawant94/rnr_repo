﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace BL_Rewards.Admin
{
    public sealed class BL_LongServiceUpload
    {
        DataSet ds;
        BE_LongServiceUpload objBE_LongServiceData;
        DAL_Common objDALCommon = new DAL_Common();

        public void SaveLongServiceData(BE_LongServiceUpload objBE_LongServiceData)
        {
            try
            {
                 SqlParameter[] objSqlParams = new SqlParameter[6];
                
                   //  objSqlParams[0] = new SqlParameter("@RecipientTokenNumber", DBNull.Value);               
                     objSqlParams[0] = new SqlParameter("@RecipientTokenNumber", objBE_LongServiceData.RecepientTokenNumber);
                     objSqlParams[1] = new SqlParameter("@Division", objBE_LongServiceData.Division);                                                  
                     objSqlParams[2] = new SqlParameter("@Location", objBE_LongServiceData.Location);      
                     objSqlParams[3]=new SqlParameter("@AwardTypeName",objBE_LongServiceData.AwardTypeName);
                     objSqlParams[4]=new SqlParameter("@AwardAmount",objBE_LongServiceData.AwardAmount);
                     objSqlParams[5]=new SqlParameter("@Longservicereason",objBE_LongServiceData.LongServiceReason);
                     //objSqlParams[6] = new SqlParameter("@Name", objBE_LongServiceData.FileName);
                     //objSqlParams[7] = new SqlParameter("@Status", objBE_LongServiceData.Status);
                     //objSqlParams[8] = new SqlParameter("@Remarks", objBE_LongServiceData.Remarks);

                 objDALCommon.DAL_SaveData("InsertLongServiceUsers", objSqlParams, 'N');

                }
                catch (Exception ex)
                {

                    throw new ArgumentException(ex.Message);
                }
            }

        }
    }


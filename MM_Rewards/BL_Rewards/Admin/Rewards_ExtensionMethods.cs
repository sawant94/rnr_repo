﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Rewards.Admin
{
    public static class Rewards_ExtensionMethods
    {
        public static string ToDate_SwapDayMonth(this String str)
        {
            string date = "";// DateTime dtDate;
            string[] splits = str.Split(new char[] { '/', '.', '-', ' ' },
                             StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < splits.Length - 1; i++)
            {
                if (splits[i].Length < 2)
                {
                    splits[i] = "0" + splits[i];
                }
            }

            if (splits.Length > 1)
            {
                date = splits[1] + "/" + splits[0] + "/" + splits[2];
            }
            else
            {
                date= str;
            }
            return date;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;
using BE_Rewards.BE_Admin;

namespace BL_Rewards.Admin
{
  public sealed  class BL_Testimonial
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();

        // retrive a dataset to bind grid
        public BE_TestimonialColl GetData()
        {
            try
            {
                ds = new DataSet();
                BE_TestimonialColl objBE_TestimonialColl = new BE_TestimonialColl();

                ds = objDALCommon.DAL_GetData("usp_GetTestimonial_ForAprroval");
                foreach (DataRow _dr in ds.Tables[0].Rows)
                {

                    BE_Testimonial objBE_Testimonial = new BE_Testimonial();

                    objBE_Testimonial.ID = Convert.ToInt32(_dr["ID"].ToString());
                    objBE_Testimonial.TokenNumber = _dr["TokenNumber"].ToString();
                    objBE_Testimonial.EmployeeName = _dr["EmployeeName"].ToString();
                    objBE_Testimonial.TestimonoialText = _dr["TestimonialText"].ToString();
                    int len = objBE_Testimonial.TestimonoialText.Length;
                    objBE_Testimonial.ShortTestimonial = (objBE_Testimonial.TestimonoialText.Substring(0, len >25? 25  : len)) + (len > 25 ? " ...":"");

                  //  objBE_Testimonial.ShortTestimonial = objBE_Testimonial.TestimonoialText.Substring(0,25) + " ...";
                    objBE_Testimonial.ImageName=_dr["ImageName"].ToString();
                    objBE_Testimonial.ApproverRemarks = _dr["Status"].ToString();
                    objBE_Testimonial.Status = Convert.ToBoolean(_dr["showlink"]);
                    objBE_TestimonialColl.Add(objBE_Testimonial);
                }
                return objBE_TestimonialColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
         // to save testimonal and it will return image name from database
        public int SaveData(BE_Testimonial objBE_Testimonial,out  string Imagename)
        {
            try
            {
                object _stat;
                Imagename = string.Empty;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[10];
                objSqlParam[0] = new SqlParameter("@ID", objBE_Testimonial.ID);
                objSqlParam[1] = new SqlParameter("@TokenNumber", objBE_Testimonial.TokenNumber);
                objSqlParam[2] = new SqlParameter("@TestimonialText", objBE_Testimonial.TestimonoialText);
                objSqlParam[3] = new SqlParameter("@Status", objBE_Testimonial.Status);
                objSqlParam[4] = new SqlParameter("@ImageName", objBE_Testimonial.ImageName);
                objSqlParam[5] = new SqlParameter("@ApproverRemarks", objBE_Testimonial.ApproverRemarks);
                objSqlParam[6] = new SqlParameter("@CreatedBy", objBE_Testimonial.CreatedBy);
                //objSqlParam[7] = new SqlParameter("@ModifiedBy", objBE_Testimonial.ModifiedBy);
                objSqlParam[7] = new SqlParameter("@ApproveBy", objBE_Testimonial.ApproveBy);
              
                objSqlParam[8] = new SqlParameter("@NewImageName", SqlDbType.VarChar,25, ParameterDirection.Output, false, 10, 10, "", DataRowVersion.Default, 0);

                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_InsertUpdate_Testimonial", objSqlParam, 'N');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                Imagename = objSqlParam[8].Value.ToString();
                
                return _status;
            
                              
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
         // function used to retive data set for Repeater control on home page
        public BE_TestimonialColl GetDataForSlider()
        {
            try
            {
                ds = new DataSet();
                BE_TestimonialColl objBE_TestimonialColl = new BE_TestimonialColl();

                ds = objDALCommon.DAL_GetData("usp_GetTestimonial_ForSlider");
                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    BE_Testimonial objBE_Testimonial = new BE_Testimonial();

                    objBE_Testimonial.TestimonoialText = _dr["TestimonialText"].ToString();               
                    objBE_Testimonial.ImageName = _dr["ImageName"].ToString();
                    objBE_Testimonial.EmployeeName = _dr["EmpName"].ToString();
                    objBE_TestimonialColl.Add(objBE_Testimonial);
                }
                return objBE_TestimonialColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

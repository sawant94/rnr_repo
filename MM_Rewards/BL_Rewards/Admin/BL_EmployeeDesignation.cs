﻿using System;
using System.Collections.Generic;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Data;

namespace BL_Rewards.Admin
{
    public sealed class BL_EmployeeDesignation
    {
        DataSet _ds;
        DAL_Common objDALCommon = new DAL_Common();
        //to retrive desgination of given token number
        public List<BE_EmployeeDesignation> GetData(string TokenNumber)
        {
            try
            {
                _ds = new DataSet();
                BE_EmpDesignationColl objEmpDesignColl = new BE_EmpDesignationColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@RecipientName", TokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_GetEmployee_view", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {

                    BE_EmployeeDesignation objBEEmpDesign = new BE_EmployeeDesignation();
              

                        //objBEEmpDesign = new BE_EmployeeDesignation();
                        objBEEmpDesign.TokenNumber = _dr["TokenNumber"].ToString();
                        objBEEmpDesign.EmpName = _dr["EmpName"].ToString();
                        objBEEmpDesign.ProcessName = _dr["ProcessName"].ToString();
                        objBEEmpDesign.DivisionName = _dr["DivisionName"].ToString();
                        objBEEmpDesign.LocationName = _dr["LocationName"].ToString();
                        //if (_dr["RoleID"].ToString() == "")
                        //{
                        //    objBEEmpDesign.Roleid = 0;
                        //}
                        //else
                        //{
                        //    objBEEmpDesign.Roleid = Convert.ToInt16(_dr["RoleID"].ToString());
                        //}
                        objEmpDesignColl.Add(objBEEmpDesign);
                  

                }
                return objEmpDesignColl.EmpDesignColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //to save Employee designation
        public int SaveData(BE_EmployeeDesignation objBEEmpDesign)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("RoleID");

                foreach (int i in objBEEmpDesign.Roles)
                {
                    DataRow dr = dt.NewRow();
                    dr["RoleID"] = i;
                    dt.Rows.Add(dr);
                }



                object _stat;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[4];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBEEmpDesign.TokenNumber);
                objSqlParam[1] = new SqlParameter("@CreatedBy", objBEEmpDesign.Createdby);
                objSqlParam[2] = new SqlParameter("@ModifiedBy", objBEEmpDesign.Modifiedby);
                objSqlParam[3] = new SqlParameter("@RoleID", dt);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_EmployeeDesignation_write", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        // to retrive  roles .
        public BE_EmpDesignationColl getRoles()
        {

            try
            {
                _ds = new DataSet();
                BE_EmpDesignationColl objBE_EmpDesignationColl = new BE_EmpDesignationColl();
                _ds = objDALCommon.DAL_GetData("usp_Mas_Roles_view");

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_EmployeeDesignation objBE_EmployeeDesignation = new BE_EmployeeDesignation();

                    objBE_EmployeeDesignation.Roleid = Convert.ToInt16(_dr["ID"].ToString());
                    objBE_EmployeeDesignation.Rolename = _dr["Roles"].ToString();
                    objBE_EmpDesignationColl.Add(objBE_EmployeeDesignation);
                }
                return objBE_EmpDesignationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }




        }
        // to retrive Roles of Employee by tokennumber.
        public BE_EmpDesignationColl GetRolesbyId(string TokenNumber)
        {
            try
            {
                _ds = new DataSet();
                BE_EmpDesignationColl objEmpDesignColl = new BE_EmpDesignationColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", TokenNumber);
                _ds = objDALCommon.DAL_GetData("usp_GetRolesByTokenNumber_view", objsqlparam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_EmployeeDesignation objBEEmpDesign = new BE_EmployeeDesignation();
                    //objBEEmpDesign.Roles.Add(Convert.ToInt16(_dr["RoleID"].ToString()));
                    objBEEmpDesign.Roleid = Convert.ToInt16(_dr["RoleID"].ToString());
                    objEmpDesignColl.Add(objBEEmpDesign);
                }
                return objEmpDesignColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
namespace BL_Rewards.Admin
{
    public sealed class BL_MailDetails
    {
        DataSet _ds;
        DAL_Common objDALCommon = new DAL_Common();
        //TO save data into Mas_MailDetails
        public int SaveData(BE_MailDetails objBE_MailDetail)
        {
            try
            {
                int Id = 0;
                SqlParameter[] objSqlParam = new SqlParameter[9];
                objSqlParam[0] = new SqlParameter("@ActivityID", objBE_MailDetail.ActivityCode);
                objSqlParam[1] = new SqlParameter("@DivisionCode", objBE_MailDetail.Divisioncode);
                objSqlParam[2] = new SqlParameter("@Subject", objBE_MailDetail.Subject);
                objSqlParam[3] = new SqlParameter("@FromID", objBE_MailDetail.Fromid);
                objSqlParam[4] = new SqlParameter("@FromName", objBE_MailDetail.Fromname);
                objSqlParam[5] = new SqlParameter("@TOID", objBE_MailDetail.Toid);
                objSqlParam[6] = new SqlParameter("@CcID", objBE_MailDetail.Ccid);
                objSqlParam[7] = new SqlParameter("@CreatedBy", objBE_MailDetail.Createdby);
                objSqlParam[8] = new SqlParameter("@ModifiedBy", objBE_MailDetail.Modifiedby);
              
                 Id=(int)objDALCommon.DAL_SaveData("usp_Insert_Mas_MailDetails_Write", objSqlParam, 'N');

                 return Id; 
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //To retrive Dropdowndata for Activity control
        public BE_MasActivityColl Getdropdownitem()
        {
            try
            {
                _ds = new DataSet();
                BE_MasActivityColl objBE_Maildetailscoll = new BE_MasActivityColl();
                _ds = objDALCommon.DAL_GetData("usp_Mas_Activity_View");

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_MasActivity objBE_Masactivity = new BE_MasActivity();
                    objBE_Masactivity.Id = Convert.ToInt16(_dr["ID"]);
                    objBE_Masactivity.Activity = _dr["Activity"].ToString();
                    objBE_Maildetailscoll.Add(objBE_Masactivity);
                }
                return objBE_Maildetailscoll;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //To retrive Dropdowndata for Location control
        public BE_MasActivityColl GetDivisiondropdownitem()
        {
            try
            {
                _ds = new DataSet(); 
                BE_MasActivityColl objBE_Maildetailscoll = new BE_MasActivityColl();
                _ds = objDALCommon.DAL_GetData("usp_Mas_Division_View");

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_MasActivity objBE_Masactivity = new BE_MasActivity();
                    objBE_Masactivity.Divisioncode = _dr["Division_PA"].ToString();
                    objBE_Masactivity.Divisionname = _dr["DivisionName_PA"].ToString();
                    objBE_Maildetailscoll.Add(objBE_Masactivity);
                }
                return objBE_Maildetailscoll;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        //to retrive MailDetails for given activityid and divisionname.
        public BE_MailDetails GetData(int Activityid, string divisionname)
        {
            try
            {
                BE_MailDetails objBE_MailDetails = new BE_MailDetails();
                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[0] = new SqlParameter("@ActivityID", Activityid);
                objSqlParam[1] = new SqlParameter("@DivisionCode", divisionname);
                _ds = objDALCommon.DAL_GetData("usp_GetMaildetails_view", objSqlParam);
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    objBE_MailDetails.Subject = _dr["Subject"].ToString();
                    objBE_MailDetails.Toid = _dr["TOID"].ToString();
                    objBE_MailDetails.Fromname = _dr["FromName"].ToString();
                    objBE_MailDetails.Fromid = _dr["FromID"].ToString();
                    objBE_MailDetails.Ccid = _dr["CcID"].ToString();  
                }
                return objBE_MailDetails;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        
        
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;
using BE_Rewards.BE_Admin;

namespace BL_Rewards.Admin
{
    public sealed class BL_SalesBudgetAllocation
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        BE_SalesBudgetAllocationColl objBE_SalesBudgetAllocationColl;
        BE_SalesBudgetAllocation objBE_SalesBudgetAllocation;

        public BE_SalesBudgetAllocationColl GetDivision()
        {
            try
            {
                ds = new DataSet();
                objBE_SalesBudgetAllocationColl = new BE_SalesBudgetAllocationColl();
                ds = objDALCommon.DAL_GetData("usp_Get_Division");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_SalesBudgetAllocation = new BE_SalesBudgetAllocation();
                    objBE_SalesBudgetAllocation.DivisionId = Convert.ToInt32(dr["ID"]);
                    objBE_SalesBudgetAllocation.DivisionName = dr["SALES_DIVISION"].ToString();
                    objBE_SalesBudgetAllocationColl.Add(objBE_SalesBudgetAllocation);
                }
                return objBE_SalesBudgetAllocationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_SalesBudgetAllocationColl GetData(BE_SalesBudgetAllocation objBE_SalesBudgetAllocation)
        {
            try
            {
                ds = new DataSet();

                BE_SalesBudgetAllocationColl objBE_SalesBudgetAllocationColl = new BE_SalesBudgetAllocationColl();


                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@SearchBy", objBE_SalesBudgetAllocation.SearchBy);
                ds = objDALCommon.DAL_GetData("usp_Get_Admin_ForSalesBudget", objSqlParam);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_SalesBudgetAllocation = new BE_SalesBudgetAllocation();

                    objBE_SalesBudgetAllocation.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_SalesBudgetAllocation.Name = dr["Name"].ToString();
                    objBE_SalesBudgetAllocation.DivisionName = dr["Division"].ToString();
                    objBE_SalesBudgetAllocation.Location = dr["Location"].ToString();
                    objBE_SalesBudgetAllocation.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBE_SalesBudgetAllocation.Balance = dr["Balance"].ToString() == "" ? (Decimal?)null : Convert.ToDecimal(dr["Balance"]);
                    objBE_SalesBudgetAllocation.EMailID = dr["EmailID"].ToString();
                    objBE_SalesBudgetAllocationColl.Add(objBE_SalesBudgetAllocation);
                }
                return objBE_SalesBudgetAllocationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_SalesBudgetAllocationColl GetUserData(BE_SalesBudgetAllocation objBE_SalesBudgetAllocation)
        {
            try
            {
                ds = new DataSet();

                BE_SalesBudgetAllocationColl objBE_SalesBudgetAllocationColl = new BE_SalesBudgetAllocationColl();


                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[0] = new SqlParameter("@SearchBy", objBE_SalesBudgetAllocation.SearchBy);
                objSqlParam[1] = new SqlParameter("@DivisionName", objBE_SalesBudgetAllocation.DivisionName);
                ds = objDALCommon.DAL_GetData("usp_Get_User_ForSalesBudget", objSqlParam);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_SalesBudgetAllocation = new BE_SalesBudgetAllocation();

                    objBE_SalesBudgetAllocation.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_SalesBudgetAllocation.Name = dr["Name"].ToString();
                    objBE_SalesBudgetAllocation.DivisionName = dr["Division"].ToString();
                    objBE_SalesBudgetAllocation.Location = dr["Location"].ToString();
                    objBE_SalesBudgetAllocation.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBE_SalesBudgetAllocation.Balance = dr["Balance"].ToString() == "" ? (Decimal?)null : Convert.ToDecimal(dr["Balance"]);
                    objBE_SalesBudgetAllocation.EMailID = dr["EmailID"].ToString();
                    objBE_SalesBudgetAllocationColl.Add(objBE_SalesBudgetAllocation);
                }
                return objBE_SalesBudgetAllocationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int SaveData(BE_SalesBudgetAllocationColl objBE_SalesBudgetAllocationColl)
        {
            int Id = 0;
            try
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("TokenNumber", typeof(string));
                dt.Columns.Add("StartDate", typeof(DateTime));
                dt.Columns.Add("EndDate", typeof(DateTime));
                dt.Columns.Add("Budget", typeof(decimal));
                dt.Columns.Add("AllocationFrom", typeof(string));
                dt.Columns.Add("CreatedBy", typeof(string));
                dt.Columns.Add("CreatedDate", typeof(DateTime));
                dt.Columns.Add("ModifiedBy", typeof(string));
                dt.Columns.Add("ModifiedDate", typeof(DateTime));

                foreach (BE_SalesBudgetAllocation objBE_SalesBudgetAllocation in objBE_SalesBudgetAllocationColl)
                {
                    DataRow dr = dt.NewRow();
                    dr["TokenNumber"] = objBE_SalesBudgetAllocation.TokenNumber;
                    dr["StartDate"] = objBE_SalesBudgetAllocation.StartDate;
                    dr["EndDate"] = objBE_SalesBudgetAllocation.EndDate;
                    dr["Budget"] = objBE_SalesBudgetAllocation.Budget;
                    dr["AllocationFrom"] = objBE_SalesBudgetAllocation.AllocationFrom;
                    dr["CreatedBy"] = objBE_SalesBudgetAllocation.CreatedBy;
                    dr["CreatedDate"] = objBE_SalesBudgetAllocation.CreatedDate;
                    dr["ModifiedBy"] = DBNull.Value;
                    dr["ModifiedDate"] = DBNull.Value;

                    dt.Rows.Add(dr);
                }


                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@Table_Mas_SalesUserBudget", dt);
                objDALCommon.DAL_SaveData("usp_Insert_Mas_SalesUserBudget", objSqlParam, 'N');
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return Id;
        }

        public int getbalance_amt(BE_BudgetAllocation objBE_BudgetAllocation1)
        {
            ds = new DataSet();

            SqlParameter[] objSqlParam = new SqlParameter[1];
            objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_BudgetAllocation1.TokenNumber);

            ds = objDALCommon.DAL_GetData("usp_Get_SalesBudgetSS", objSqlParam);
            if (ds.Tables[0].Rows[0].ToString() == "0")
            {
                return 0;
            }
            else
            {
                return 1;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;

namespace BL_Rewards.Admin
{
    public sealed class BL_Holidays
    {
        DataSet _ds;
        DAL_Common objDALCommon = new DAL_Common();

        //to getdropdownitem;
        public BE_HolidayManagementColl Get_AllLocations()
        {
            try
            {
                _ds = new DataSet();
                BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                _ds = objDALCommon.DAL_GetData("usp_Mas_Location_View");

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                    objBE_HolidayManagement.Locationcode = _dr["Location_PSA"].ToString();
                    objBE_HolidayManagement.Locationname = _dr["LocationName_PSA"].ToString();
                    objBE_HolidayManagement.CalendarID = _dr["CalendarID"].ToString() == "" ? (int?)null : Convert.ToInt32(_dr["CalendarID"]);
                    objBE_HolidayManagementColl.Add(objBE_HolidayManagement);
                }
                return objBE_HolidayManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        public BE_HolidayManagementColl Get_Mas_Calendar()
        {
            try
            {
                _ds = new DataSet();
               
                BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();

               
                _ds = objDALCommon.DAL_GetData("usp_Get_Mas_Calendar");

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                    objBE_HolidayManagement.CalendarID = Convert.ToInt32(_dr["ID"]);
                    objBE_HolidayManagement.Calendarname = _dr["CalendarName"].ToString();
                    objBE_HolidayManagement.Sattype = Convert.ToInt32(_dr["WeekEnd"].ToString());

                    objBE_HolidayManagementColl.Add(objBE_HolidayManagement);
                }
                return objBE_HolidayManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        public BE_HolidayManagement Get_RecordsByCalendarID(int CalendarID)
        {
            try
            {
                _ds = new DataSet();
                BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                //BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@CalenDarID", CalendarID);
                _ds = objDALCommon.DAL_GetData("usp_Get_RecordsByCalendarID",objSqlParam);

                foreach (DataRow dr in _ds.Tables[0].Rows)
                {
                    objBE_HolidayManagement.CalendarID = Convert.ToInt32(dr["ID"].ToString());
                    objBE_HolidayManagement.Calendarname = dr["CalendarName"].ToString();
                    objBE_HolidayManagement.Sattype = Convert.ToInt32(dr["WeekEnd"].ToString());

                    BE_Holidays objBE_Holidays;
                    foreach (DataRow dr1 in _ds.Tables[1].Rows)
                    {
                        objBE_Holidays = new BE_Holidays();
                        objBE_Holidays.ID = Convert.ToInt32(dr1["ID"].ToString());
                        objBE_Holidays.HolidayName = dr1["HolidayName"].ToString();
                        objBE_Holidays.HolidayDate = Convert.ToDateTime(dr1["HolidayDate"].ToString());
                        objBE_Holidays.CalendarID = Convert.ToInt32(dr1["CalendarID"].ToString());
                        objBE_HolidayManagement.HolidayColl.Add(objBE_Holidays);
                    }

                    foreach (DataRow dr2 in _ds.Tables[2].Rows)
                    {
                        objBE_HolidayManagement.LocationColl.Add(dr2["LocationCode"].ToString());
                    }

                    //objBE_HolidayManagementColl.Add(objBE_HolidayManagement);
                }
                return objBE_HolidayManagement;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        public int InsertUpdate_Mas_Calendar(BE_HolidayManagement objBE_HolidayManagement)
        {
            try
            {
                object _stat;
                int _status = 0;

                DataTable dtHolidays = new DataTable();
                dtHolidays.Columns.Add("HolidayName");
                dtHolidays.Columns.Add("HolidayDate");

                foreach (BE_Holidays objBE_Holidays in objBE_HolidayManagement.HolidayColl)
                {
                    DataRow dr;
                    dr = dtHolidays.NewRow();
                    dr["HolidayName"] = objBE_Holidays.HolidayName;
                    dr["HolidayDate"] = objBE_Holidays.HolidayDate.ToString("MM/dd/yyyy");
                    dtHolidays.Rows.Add(dr);
                }

                DataTable dtLocations = new DataTable();
                dtLocations.Columns.Add("LocationCode");

                foreach (string strLoc in objBE_HolidayManagement.LocationColl)
                {
                    DataRow dr;
                    dr = dtLocations.NewRow();
                    dr["LocationCode"] = strLoc;
                    dtLocations.Rows.Add(dr);
                }

                SqlParameter[] objSqlParam = new SqlParameter[8];
                objSqlParam[0] = new SqlParameter("@CalendarID", objBE_HolidayManagement.CalendarID);
                objSqlParam[1] = new SqlParameter("@CalendarName", objBE_HolidayManagement.Calendarname);
                objSqlParam[2] = new SqlParameter("@WeekEnd", objBE_HolidayManagement.Sattype);
                objSqlParam[3] = new SqlParameter("@CreatedBy", objBE_HolidayManagement.Createdby);
                objSqlParam[4] = new SqlParameter("@ModifiedBy", objBE_HolidayManagement.Modifiedby);

                objSqlParam[5] = new SqlParameter("@dtHolidays", dtHolidays);
                objSqlParam[6] = new SqlParameter("@dtLocations", dtLocations);

                objSqlParam[7] = new SqlParameter("@IsDuplicate", SqlDbType.Int, 10, ParameterDirection.Output, false, 10, 10, "", DataRowVersion.Default, 0);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_InsertUpdate_Mas_Calendar", objSqlParam, 'N');

                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

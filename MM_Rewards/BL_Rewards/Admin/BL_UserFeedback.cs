﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;
namespace BL_Rewards.Admin
{
   public sealed class BL_UserFeedback
   {
       DataSet _ds;
       DAL_Common objDALCommon = new DAL_Common();
       //TO save data into Mas_MailDetails
       public int SaveData(BE_UserFeedback objBE_UserFeedback)
       {
           try
           {
               int Id = 0;
               SqlParameter[] objSqlParam = new SqlParameter[4];
               objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_UserFeedback.Tokennumber);
               objSqlParam[1] = new SqlParameter("@FeedBackType", objBE_UserFeedback.Feedabacktype);
               objSqlParam[2] = new SqlParameter("@FeedBackDetails", objBE_UserFeedback.Feedbackdetails);                       
               objSqlParam[3] = new SqlParameter("@CreatedBy", objBE_UserFeedback.Createdby);
               
               Id = (int)objDALCommon.DAL_SaveData("usp_User_Feedback_Write", objSqlParam, 'N');

               return Id;
           }
           catch (Exception ex)
           {
               throw new ArgumentException(ex.Message);
           }
       }


    }
}

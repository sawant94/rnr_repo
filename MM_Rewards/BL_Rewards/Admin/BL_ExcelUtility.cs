﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using BE_Rewards.BE_Admin;
using BE_Rewards.BE_Rewards;
using System.Configuration;
namespace BL_Rewards.Admin
{
    public sealed class BL_ExcelUtility
    {
        //Dictionary<string, string> dicDuplicateAwards = new Dictionary<string, string>();
        //Dictionary<string, string> dicAwards = new Dictionary<string, string>();
        BL_Redemption objBL_Redemption;
        BL_LongServiceUpload objBL_LongService;
        int errflg = 0;
        public void SaveData(string strFilePath)
        {
            OleDbConnection con;
            string connectionString;
            if (strFilePath.Substring(strFilePath.LastIndexOf(".")) == ".xlsx")
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Excel 12.0 Xml;HDR=YES;", strFilePath);
            }
            else
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", strFilePath);
            }
            con = new OleDbConnection(connectionString);
            //string connStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + @";Extended Properties=""Excel 12.0 Xml;HDR=YES;""";
            //  OleDbConnection OledbConn = new OleDbConnection(connectionString);
            //connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
            try
            {
                // OledbConn.Open();
                con.Open();
                var sheets = from DataRow dr in con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows
                             select new { SheetName = dr["TABLE_NAME"].ToString() };
                con.Close();
                foreach (var s in sheets)
                {
                    OleDbDataAdapter da = new OleDbDataAdapter(string.Format("select * from [{0}]", s.SheetName), con);
                    DataTable dtMain = new DataTable();
                    da.Fill(dtMain);
                    foreach (DataRow dr in dtMain.Rows)
                    {
                        if (CheckBlank(dr))
                        {
                            string errMsg = ValidateRow(dr);
                            BE_AwardClosure objBE_AwardClosure = new BE_AwardClosure();
                            objBL_Redemption = new BL_Redemption();
                            objBE_AwardClosure.AwardID = dr["AwardID"].ToString();
                            objBE_AwardClosure.FileName = strFilePath.Substring(strFilePath.LastIndexOf("\\"));//dr[2].ToString();
                            objBE_AwardClosure.Status = "SUCCESS";
                            objBE_AwardClosure.Remarks = errMsg;
                            if (errMsg == "")
                            {
                                objBE_AwardClosure.Remarks = "Record inserted successfully!";
                                BE_Redemption objBE_Redemption = new BE_Redemption();
                                objBE_Redemption.AwardID = dr["AwardID"].ToString();//  = dr[0].ToString();
                                string ClosureDate = dr["ClosureDate"].ToString();// = dr[7].ToString();
                                if (ClosureDate != "")
                                    ClosureDate = ClosureDate.ToDate_SwapDayMonth();
                                objBE_Redemption.ClosureDate = (ClosureDate == "") ? (DateTime?)null : Convert.ToDateTime(ClosureDate);
                                objBE_Redemption.ClosingAmount = dr["ClosingAmount"].ToString() == "" ? (Decimal?)null : Convert.ToDecimal(dr["ClosingAmount"]);
                                // objBE_Redemption.VendorInvoiceValue = dr["VendorInvoiceValue"].ToString();
                                objBL_Redemption.SaveDataClosure(objBE_AwardClosure, objBE_Redemption);
                            }
                            else
                            {
                                objBE_AwardClosure.Status = "FAIL";
                                objBL_Redemption.SaveDataClosure(objBE_AwardClosure);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                throw new ArgumentException(ex.Message);
            }
            finally
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
        }
        private Boolean CheckBlank(DataRow dr)
        {
            bool b = false;
            foreach (var item in dr.ItemArray)
            {
                if (item.ToString() != "")
                {
                    b = true;
                    break;
                }
            }
            return b;
        }
        private string ValidateRow(DataRow dr)
        {
            string errMsg = "";
            try
            {
                if (dr["AwardID"].ToString() == "")//(dr[0].ToString() == "")
                    errMsg = "Award ID is missing!";
                else if (dr["ClosureDate"].ToString() == "" && dr["ClosingAmount"].ToString() == "")//(dr[7].ToString() == "" && dr[8].ToString() == "")
                    errMsg = "Closure Date and Closing Amount is missing!";
                else if (dr["ClosureDate"].ToString() == "")//(dr[7].ToString() == "")
                    errMsg = "Closure Date is missing!";
                else if (dr["ClosingAmount"].ToString() == "")//(dr[8].ToString() == "")
                    errMsg = "Closing Amount is missing!";
                else
                {
                    string ClosureDate = dr["ClosureDate"].ToString().ToDate_SwapDayMonth(); //dr[7].ToString();
                    // ClosureDate = ClosureDate.Substring(3, 2) + "/" + ClosureDate.Substring(0, 2) + "/" + ClosureDate.Substring(6, 4);
                    try
                    {
                        Convert.ToDateTime(ClosureDate);
                    }
                    catch (Exception ex)
                    {
                        errMsg = "Closure Date format invalid! Valid date format is dd/MM/yyyy";
                    }
                    try
                    {
                        Convert.ToDecimal(dr["ClosingAmount"].ToString()); //(dr[8].ToString());
                    }
                    catch (Exception ex)
                    {
                        errMsg = "Closing Amount invalid! Has to be a valid number.";
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return errMsg;
        }
        public int SaveLongService(string strFilePath, string strawardtypename, string strawardreason, int awardamt)
        {
            OleDbConnection con;
            string connectionString;
            if (strFilePath.Substring(strFilePath.LastIndexOf(".")) == ".xlsx")
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Excel 12.0 Xml;HDR=YES;", strFilePath);
            }
            else
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", strFilePath);
            }
            //connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Excel 12.0 Xml;HDR=YES;", strFilePath);
            con = new OleDbConnection(connectionString);
            try
            {
                con.Open();
                var sheets = from DataRow dr in con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows
                             select new { SheetName = dr["TABLE_NAME"].ToString() };
                con.Close();
                foreach (var s in sheets)
                {
                    OleDbDataAdapter da = new OleDbDataAdapter(string.Format("select * from [{0}]", s.SheetName), con);
                    DataTable dtMain = new DataTable();
                    da.Fill(dtMain);
                    foreach (DataRow dr in dtMain.Rows)
                    {
                        if (errflg == 0)
                        {
                            if (CheckBlank(dr))
                            {
                                string errMsg1 = ValidateLongServiceRow(dr);
                                BE_LongServiceUpload objBE_LongService = new BE_LongServiceUpload();
                                objBE_LongService.RecepientTokenNumber = dr["RecipientTokenNumber"].ToString();
                                objBE_LongService.Division = dr["Division"].ToString();
                                objBE_LongService.Location = dr["Location"].ToString();
                                //objBE_LongService.Designation = dr["Designation"].ToString();
                                objBE_LongService.AwardTypeName = strawardtypename;
                                objBE_LongService.AwardAmount = awardamt;
                                objBE_LongService.LongServiceReason = strawardreason;
                                if (errMsg1 == "")
                                {
                                    BL_LongServiceUpload objBL_LongService = new BL_LongServiceUpload();
                                    objBL_LongService.SaveLongServiceData(objBE_LongService);
                                    errflg = 0;
                                }
                                else
                                {
                                    errflg = 1;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                throw new ArgumentException(ex.Message);
            }
            finally
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return errflg;
        }
        private string ValidateLongServiceRow(DataRow dr)
        {
            string errMsg1 = "";
            try
            {
                if (dr["RecipientTokenNumber"].ToString() == "")
                    errMsg1 = "Recipient Token Number is missing!";
                else if (dr["Division"].ToString() == "")
                    errMsg1 = "Division is missing!";
                else if (dr["Location"].ToString() == "")
                    errMsg1 = "Location is missing!";
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return errMsg1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data;
using System.Data.SqlClient;
namespace BL_Rewards.Admin
{
    public class BL_BudgetTracking
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        BE_BudgetTrackingUtil objBE_BE_BudgetTrackingUtil;
        BE_BudgetTrackingUtilColl objBE_BE_BudgetTrackingUtilColl;

        public BE_BudgetTrackingUtilColl GetBudgetAwardType()
        {
            try
            {
                ds = new DataSet();
                objBE_BE_BudgetTrackingUtilColl = new BE_BudgetTrackingUtilColl();
                ds = objDALCommon.DAL_GetData("usp_Get_BugdetAwardType");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_BE_BudgetTrackingUtil = new BE_BudgetTrackingUtil();
                    objBE_BE_BudgetTrackingUtil.ID = Convert.ToInt32(dr["ID"]);
                    objBE_BE_BudgetTrackingUtil.AwardName = dr["AwardName"].ToString();
                    objBE_BE_BudgetTrackingUtilColl.Add(objBE_BE_BudgetTrackingUtil);
                }
                return objBE_BE_BudgetTrackingUtilColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int SaveData(BE_BudgetTrackingUtil objBE_BudgetTrackingUtil, out  int _ID)
        {
            try
            {
                object _stat;
                _ID = 0;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[9];
                objSqlParam[0] = new SqlParameter("@ID", objBE_BudgetTrackingUtil.ID);
                objSqlParam[1] = new SqlParameter("@AwardName", objBE_BudgetTrackingUtil.AwardName);
                objSqlParam[2] = new SqlParameter("@FromYear", objBE_BudgetTrackingUtil.FromYear);
                objSqlParam[3] = new SqlParameter("@Toyear", objBE_BudgetTrackingUtil.ToYear);
                objSqlParam[4] = new SqlParameter("@TotalBudget", objBE_BudgetTrackingUtil.totalAmount);
                objSqlParam[5] = new SqlParameter("@budgetforaward", objBE_BudgetTrackingUtil.Budgetforeachaward);
                objSqlParam[6] = new SqlParameter("@percentage", objBE_BudgetTrackingUtil.Percentage);
                // objSqlParam[7] = new SqlParameter("@CreatedBy", objBE_BudgetTrackingUtil.CreatedOn);
                // objSqlParam[8] = new SqlParameter("@ModifiedBy", objBE_BudgetTrackingUtil.ModifiedOn);
                objSqlParam[7] = new SqlParameter("@Tokennumber", objBE_BudgetTrackingUtil.TokenNumber);
                objSqlParam[8] = new SqlParameter("@IsDuplicate", SqlDbType.Int, 10, ParameterDirection.Output, false, 10, 10, "", DataRowVersion.Default, 0);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Mas_BudgetAwardType_Write", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);

                _ID = (int)objSqlParam[8].Value;
                return _ID;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

    }

}

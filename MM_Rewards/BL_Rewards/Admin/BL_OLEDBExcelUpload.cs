﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;

namespace BL_Rewards.Admin
{
  public class BL_OLEDBExcelUpload
  {
        #region Variable Declaration
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        
        string fileName="";
        string dirPath="";
        string rowInserted = "";
        int errCounter = 0, successCounter = 0, excelSheetNo = 0;
        BL_UserExcelUpload objBL_UserExcelUpload = new BL_UserExcelUpload();
      #endregion

        #region Get the User details by the Token Number.
        public BE_UserExcelUploadColl GetDetailsByTokenNumber(string tokennumber)
        {
            try
            {
                ds = new DataSet();                                            
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0]=new SqlParameter("@TokenNumber",tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_ExcelUpload_EmployeeDetails", objsqlparam);               
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();  
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBEUserExcelUpload.DivisionName = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.LocationName = dr["LocationName_PSA"].ToString();
                    objBEUserExcelUpload.OrgUnitName = dr["OrgUnitName"].ToString();
                    objBEUserExcelUpload.EmailID = dr["EmailID"].ToString();
                    objBEUserExcelUpload.FirstName = dr["FirstName"].ToString();
                    objBEUserExcelUpload.LastName = dr["LastName"].ToString();
                    objBEUserExcelUpload.EmpGradeCode_E = dr["EmpGradeCode_E"].ToString();
                    objBEUserExcelUpload.EmpGradeName_E = dr["EmpGradeName_E"].ToString();
                    objBEUserExcelUpload.Division_PA = dr["Division_PA"].ToString();
                    objBEUserExcelUpload.DivisionName_PA = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.EmployeeLevelCode_ES = dr["EmployeeLevelCode_ES"].ToString();
                    objBEUserExcelUpload.EmployeeLevelName_ES = dr["EmployeeLevelName_ES"].ToString();
                    objBEUserExcelUpload.Levels = dr["Levels"].ToString();
                    objBEUserExcelUpload.Location_PSA = dr["Location_PSA"].ToString();
                    objBEUserExcelUpload.LocationName_PSA = dr["LocationName_PSA"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Getthe Details after Successful Import
        public BE_UserExcelUploadColl GetDetailsAfterUpload()
        {
            try
            {
                ds = new DataSet();
                
                BE_UserExcelUploadColl objBE_UserExcelUploadColl  = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("usp_Get_LatestExcelUpload_EmployeeDetails");              
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBEUserExcelUpload.DivisionName = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.LocationName = dr["LocationName_PSA"].ToString();
                    objBEUserExcelUpload.OrgUnitName = dr["OrgUnitName"].ToString();
                    objBEUserExcelUpload.EmailID = dr["EmailID"].ToString();
                    objBEUserExcelUpload.FirstName = dr["FirstName"].ToString();
                    objBEUserExcelUpload.LastName = dr["LastName"].ToString();
                    objBEUserExcelUpload.EmpGradeCode_E = dr["EmpGradeCode_E"].ToString();
                    objBEUserExcelUpload.EmpGradeName_E = dr["EmpGradeName_E"].ToString();
                    objBEUserExcelUpload.Division_PA = dr["Division_PA"].ToString();
                    objBEUserExcelUpload.DivisionName_PA = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.EmployeeLevelCode_ES = dr["EmployeeLevelCode_ES"].ToString();
                    objBEUserExcelUpload.EmployeeLevelName_ES = dr["EmployeeLevelName_ES"].ToString();
                    objBEUserExcelUpload.Levels = dr["Levels"].ToString();
                    objBEUserExcelUpload.Location_PSA = dr["Location_PSA"].ToString();
                    objBEUserExcelUpload.LocationName_PSA = dr["LocationName_PSA"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);   
                    
                }
               
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region SaveExcel for Importing the Data in Table
        public string SaveExcel(SqlParameter objSqlParam , int InsertOrUpdate)
        {

            try
            {
                object _stat;                
                string var1="" , var2="";
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[4];
                objSqlParameter[0] = objSqlParam;             
                objSqlParameter[1] = new SqlParameter("@InserRowCount",DbType.Int32);
                objSqlParameter[1].Direction = ParameterDirection.Output;
                objSqlParameter[2] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[2].Direction = ParameterDirection.Output;
                objSqlParameter[3] = new SqlParameter("@InsertOrUpdateFlag", InsertOrUpdate);
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel", objSqlParameter, 'N');
                var1 = objSqlParameter[1].SqlValue.ToString();
                var2 = objSqlParameter[2].SqlValue.ToString();
                //_stat2 = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel", objSqlParameter, 'N');
                //var2 = objSqlParameter[1].SqlValue.ToString();
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);
               // rowAffect2 = _stat2 == null ? 0 : Convert.ToInt32(_stat2);

                if (objSqlParameter[1].SqlValue.ToString() == "" || objSqlParameter[1].SqlValue == null)
                    objSqlParameter[1].Value = 0;

                 if (objSqlParameter[2].SqlValue.ToString() == "" || objSqlParameter[2].SqlValue==  null)
                    objSqlParameter[2].Value = 0;
               
                if (rowAffect >= 0)
                {
                    if (InsertOrUpdate == 1)
                       Message = "Total Number of Updated Records are " + objSqlParameter[2].SqlValue.ToString();
                    else
                       Message = "Total Number of Inserted Records are " + objSqlParameter[1].SqlValue.ToString();
                }
                else
                {
                    Message = "Failure in Process Check the Excel Sheet and Try Again";
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Update the Record
        public string UpdateRecord(string TokenNumberUpdate, string TokenNumber, string FirstName, string LastName, string EmpGradeCode_E, string EmpGradeName_E,
                                   string EmployeeLevelCode_ES, string EmployeeLevelName_ES, string Levels, string Division_PA, string DivisionName_PA, string LocationName_PSA, string Location_PSA)
        {    
            try
            {
                object _stat;              
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[14];
                objSqlParameter[0] =new SqlParameter("@TokenNumberUpdate",TokenNumberUpdate);
                objSqlParameter[1] =new SqlParameter("@TokenNumber",TokenNumber);
                objSqlParameter[2] =new SqlParameter("@FirstName",FirstName);
                objSqlParameter[3] =new SqlParameter("@LastName",LastName);
                objSqlParameter[4] =new SqlParameter("@EmpGradeCode_E",EmpGradeCode_E);
                objSqlParameter[5] =new SqlParameter("@EmpGradeName_E",EmpGradeName_E);
                objSqlParameter[6] =new SqlParameter("@EmployeeLevelCode_ES",EmployeeLevelCode_ES);
                objSqlParameter[7] =new SqlParameter("@EmployeeLevelName_ES",EmployeeLevelName_ES);
                objSqlParameter[8] =new SqlParameter("@Levels",Levels);
                objSqlParameter[9] =new SqlParameter("@Division_PA",Division_PA);
                objSqlParameter[10] =new SqlParameter("@DivisionName_PA",DivisionName_PA);
                objSqlParameter[11] =new SqlParameter("@LocationName_PSA",LocationName_PSA);
                objSqlParameter[12] =new SqlParameter("@Location_PSA",Location_PSA);
                objSqlParameter[13] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[13].Direction = ParameterDirection.Output;
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel_Update", objSqlParameter, 'N');
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);

                if (objSqlParameter[13].SqlValue.ToString() == "" || objSqlParameter[13].SqlValue.ToString() == null)
                    objSqlParameter[13].Value = 0;

                if (rowAffect >= 0)
                {
                    Message = objSqlParameter[13].SqlValue.ToString();
                }
                else
                {
                    Message = objSqlParameter[13].SqlValue.ToString();
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Getting Data for Employee Group  DropDown
        public BE_UserExcelUploadColl GetEmployeeGroup()
        {
            try
            {
                ds = new DataSet();                
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("USP_Get_EmployeeGroup");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();                   
                    objBE_UserExcelUpload.EmpGradeCode_E = _dr["EmpGradeCode_E"].ToString();
                    objBE_UserExcelUpload.EmpGradeName_E = _dr["EmpGradeName_E"].ToString();
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

      #endregion

        #region Getting Data for Employee SUB Group  DropDown
        public BE_UserExcelUploadColl GetEmployeeSubGroup()
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("USP_Get_EmployeeSubGroup");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();
                    objBE_UserExcelUpload.EmployeeLevelCode_ES = _dr["Code"].ToString();
                    objBE_UserExcelUpload.EmployeeLevelName_ES = _dr["Desig"].ToString();
                    objBE_UserExcelUpload.Levels = _dr["Level"].ToString();
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        #endregion

        #region Getting Data for Division PA Name  DropDown
        public BE_UserExcelUploadColl GetDivisionPA()
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                //SqlParameter[] objsqlparam = new SqlParameter[1];
                //objsqlparam[0] = new SqlParameter("@TokenNumber", tokennumber);
                ds = objDALCommon.DAL_GetData("USP_Get_Division_PA_Details");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();
                    objBE_UserExcelUpload.Division_PA = _dr["Division_PA"].ToString();
                    objBE_UserExcelUpload.DivisionName_PA = _dr["DivisionName_PA"].ToString();                    
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        #endregion

        #region Getting Data for Location PA Name  DropDown
        public BE_UserExcelUploadColl GetLocationPSA()
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                //SqlParameter[] objsqlparam = new SqlParameter[1];
                //objsqlparam[0] = new SqlParameter("@TokenNumber", tokennumber);
                ds = objDALCommon.DAL_GetData("USP_Get_Location_PSA_Details");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();
                    objBE_UserExcelUpload.Location_PSA = _dr["Location_PSA"].ToString();
                    objBE_UserExcelUpload.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        #endregion

        #region Get All the records from Mas User table
        public DataTable GetMasUserTable()
        {
            try
            {
                ds = new DataSet();               
                ds = objDALCommon.DAL_GetData("USP_Get_MasUserTable");
                
                    DataTable dtMasUser = new DataTable();
                    dtMasUser.Columns.Add("TokenNumber", typeof(string));
                    foreach (DataRow _dr in ds.Tables[0].Rows)
                    {
                        DataRow _dr2 = dtMasUser.NewRow();
                        _dr2["TokenNumber"] = ds.Tables[0].Rows[0].ToString();
                        dtMasUser.Rows.Add(_dr2);

                    }
                    dtMasUser = ds.Tables[0];

                return dtMasUser;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        } 
      #endregion

      // Budget Code Area.......

        #region SaveExcel for Importing the Data in Table BUDGET Upload
        public string SaveBudgetExcel(SqlParameter objSqlParam, int InsertOrUpdate)
        {

            try
            {
                object _stat;
                string var1 = "", var2 = "";
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[4];
                objSqlParameter[0] = objSqlParam;
                objSqlParameter[1] = new SqlParameter("@InserRowCount", DbType.Int32);
                objSqlParameter[1].Direction = ParameterDirection.Output;
                objSqlParameter[2] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[2].Direction = ParameterDirection.Output;
                objSqlParameter[3] = new SqlParameter("@InsertOrUpdateFlag", InsertOrUpdate);
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel_Budget", objSqlParameter, 'N');
                var1 = objSqlParameter[1].SqlValue.ToString();
                var2 = objSqlParameter[2].SqlValue.ToString();
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);           

                if (objSqlParameter[1].SqlValue.ToString() == "" || objSqlParameter[1].SqlValue == null)
                    objSqlParameter[1].Value = 0;

                if (objSqlParameter[2].SqlValue.ToString() == "" || objSqlParameter[2].SqlValue == null)
                    objSqlParameter[2].Value = 0;

                if (rowAffect >= 0)
                {
                    if (InsertOrUpdate == 1)
                        Message = "Total Number of Updated Records are " + objSqlParameter[2].SqlValue.ToString();
                    else
                        Message = "Total Number of Inserted Records are " + objSqlParameter[1].SqlValue.ToString();
                }
                else
                {
                    Message = "Failure in Process Check the Excel Sheet and Try Again";
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Get All the records from Mas User Budget Table
        public DataTable GetMasBudgetTable()
        {
            try
            {
                ds = new DataSet();
                ds = objDALCommon.DAL_GetData("USP_Get_MasBudgetTable");

                DataTable dtMasUser = new DataTable();
                dtMasUser.Columns.Add("TokenNumber", typeof(string));
                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    DataRow _dr2 = dtMasUser.NewRow();
                    _dr2["TokenNumber"] = ds.Tables[0].Rows[0].ToString();
                    dtMasUser.Rows.Add(_dr2);

                }
                dtMasUser = ds.Tables[0];

                return dtMasUser;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region Get the Details after Successful Import of Budget Information
        public BE_UserExcelUploadColl GetBudgetDetailsAfterUpload()
        {
            try
            {
                ds = new DataSet();

                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("usp_Get_LatestExcelUpload_Budget_EmployeeDetails");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.StartDate = dr["StartDate"].ToString();
                    objBEUserExcelUpload.EndDate = dr["EndDate"].ToString();
                    objBEUserExcelUpload.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);

                }

                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Get the Budget details by the Token Number.
        public BE_UserExcelUploadColl GetBudgetDetailsByTokenNumber(string tokennumber)
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_ExcelUpload_EmployeeDetails_Budget", objsqlparam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.StartDate = dr["StartDate"].ToString();
                    objBEUserExcelUpload.EndDate = dr["EndDate"].ToString();
                    objBEUserExcelUpload.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Update the Budget Record
        public string UpdateBudgetRecord(string TokenNumberUpdate, string TokenNumber, string StartDate, string EndDate, decimal Budget)
        {
            try
            {
                object _stat;
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[6];
                objSqlParameter[0] = new SqlParameter("@TokenNumberUpdate", TokenNumberUpdate);
                objSqlParameter[1] = new SqlParameter("@TokenNumber", TokenNumber);
                objSqlParameter[2] = new SqlParameter("@StartDate", StartDate);
                objSqlParameter[3] = new SqlParameter("@EndDate", EndDate);
                objSqlParameter[4] = new SqlParameter("@Budget", Budget);
                objSqlParameter[5] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[5].Direction = ParameterDirection.Output;
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Budget_Excel_Update", objSqlParameter, 'N');
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);

                if (objSqlParameter[5].SqlValue.ToString() == "" || objSqlParameter[5].SqlValue.ToString() == null)
                    objSqlParameter[5].Value = 0;

                if (rowAffect >= 0)
                {
                    Message = objSqlParameter[5].SqlValue.ToString();
                }
                else
                {
                    Message = objSqlParameter[5].SqlValue.ToString();
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        public string InsertExcelRecord1(string fileLocation)
        {
            try
            {
                string strreturnvalue = "";
                string errMsg = "";
                bool IsValidEmail = true;
                fileName = "log.txt";
                dirPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FolderDirPath"].ToString());
                OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Excel 12.0 Xml;HDR=YES;", fileLocation));
                //OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}; Excel 8.0;HDR=YES;", fileLocation));
                con.Open();
                var sheets = from DataRow dr in con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows
                             select new { SheetName = dr["TABLE_NAME"].ToString() };
                con.Close();

                DateTime now = DateTime.Now;
                LogData(dirPath, fileName, " \t", "------------------------------" + now.ToString("F") + "-----------------------------------------\t");
                LogData(dirPath, fileName, " \t", " ");

                BE_UserExcelUpload objBEUserDataUpLoad = new BE_UserExcelUpload();
                BE_UserExcelUploadColl objBEUserDataUpLoadColl = new BE_UserExcelUploadColl();

                foreach (var s in sheets)
                {
                    if (!s.SheetName.Contains("_xlnm#_FilterDatabase"))
                    {
                        OleDbDataAdapter da = new OleDbDataAdapter(string.Format("select * from [{0}]", s.SheetName), con);
                        DataTable dtMain = new DataTable();
                        da.Fill(dtMain);
                        if (CheckExcelFormat(dtMain) == "valid")
                        {
                            foreach (DataRow _dr in dtMain.Rows)
                            {
                                if (CheckBlank(_dr))
                                {
                                    objBEUserDataUpLoad = new BE_UserExcelUpload();
                                    errMsg = ValidateRow(_dr, dirPath, fileName);
                                    IsValidEmail = isEmailValid(_dr[13].ToString());
                                    if (errMsg != "Token Number and EmailID Missing!")
                                    {
                                        if (!IsValidEmail && errMsg == "Token Number is missing!")
                                            errMsg = "Token Number is missing! and Invalid EmailID!";
                                        else if (!IsValidEmail && errMsg == "First Name is missing! and Required Field is Missing!")
                                        {
                                            errMsg = "First Name is missing! and Required Field is Missing!";

                                        }
                                        else if (!IsValidEmail && errMsg == "Names are missing! and Required Field is Missing!")
                                        {
                                            errMsg = "Names are missing! and Required Field is Missing!";
                                        }
                                        else if (!IsValidEmail)
                                            errMsg = "Invalid EmailID!";
                                    }
                                    //Data fetching from Row
                                    if (errMsg != "Token Number is missing!" && errMsg != "Token Number is missing! and Invalid EmailID!")
                                    {
                                        // DateTime DOB = DateTime.ParseExact(_dr[25].ToString(), "dd/MM/yyyy", null);  //Convert.ToDateTime(_dr[25].ToString());  // DateTime DOJ = DateTime.ParseExact(_dr[24].ToString(), "dd/MM/yyyy", null); //Convert.ToDateTime(_dr[24].ToString());                                          
                                        // UsEn.DateTimeFormat = DateTimeFormatInfo.// DateTime DOB = DateTime.Parse(_dr[25].ToString());
                                        CultureInfo UsEn = new CultureInfo("de-DE");
                                        DateTime DOB = new DateTime();
                                        DateTime DOJ = new DateTime();
                                        if (_dr[25].ToString() != "")
                                        {
                                            DOB = Convert.ToDateTime(_dr[25].ToString(), UsEn);
                                            DOB.ToString("dd/MM/yyyy");
                                        }
                                        if (_dr[24].ToString() != "")
                                        {
                                            DOJ = Convert.ToDateTime(_dr[24].ToString(), UsEn);
                                            DOJ.ToString("dd/MM/yyyy");
                                        }

                                        objBEUserDataUpLoad.EmployeeNumber = _dr[0].ToString();
                                        objBEUserDataUpLoad.EmployeeName = _dr[1].ToString();
                                        objBEUserDataUpLoad.FirstName = _dr[2].ToString();
                                        objBEUserDataUpLoad.LastName = _dr[3].ToString();
                                        objBEUserDataUpLoad.EmployeeGroup = _dr[4].ToString();
                                        objBEUserDataUpLoad.EG = _dr[5].ToString();
                                        objBEUserDataUpLoad.EmployeeSubgroup = _dr[6].ToString();
                                        objBEUserDataUpLoad.Counter = Convert.ToInt32(_dr[7].ToString());
                                        objBEUserDataUpLoad.ESG = _dr[8].ToString();
                                        objBEUserDataUpLoad.PersonnelArea = _dr[9].ToString();
                                        objBEUserDataUpLoad.PA = _dr[10].ToString();
                                        objBEUserDataUpLoad.PersonnelSubarea = _dr[11].ToString();
                                        objBEUserDataUpLoad.PSA = _dr[12].ToString();
                                        objBEUserDataUpLoad.EmailID = _dr[13].ToString();
                                        objBEUserDataUpLoad.EmployeeOrgUnitNumber = _dr[14].ToString();
                                        objBEUserDataUpLoad.EmployeeOrgUnitName = _dr[15].ToString();
                                        objBEUserDataUpLoad.BusinessUnit = _dr[16].ToString();
                                        objBEUserDataUpLoad.BUNo = _dr[17].ToString();
                                        objBEUserDataUpLoad.EmployeeCostCenterNumber = _dr[18].ToString();
                                        objBEUserDataUpLoad.EmployeeCostCenterName = _dr[19].ToString();
                                        objBEUserDataUpLoad.EmployeePositionNumber = _dr[20].ToString();
                                        objBEUserDataUpLoad.EmployeePositionName = _dr[21].ToString();
                                        objBEUserDataUpLoad.CompanyCode = _dr[22].ToString();
                                        objBEUserDataUpLoad.Gender = _dr[23].ToString();
                                        objBEUserDataUpLoad.DateofJoining = DOJ.ToString();
                                        objBEUserDataUpLoad.DateofBirth = DOB.ToString();
                                        objBEUserDataUpLoad.PayrollAreaText = _dr[26].ToString();
                                        objBEUserDataUpLoad.PayrollAreaCode = _dr[27].ToString();
                                        objBEUserDataUpLoad.MangerNumber = _dr[28].ToString();
                                        objBEUserDataUpLoad.MangerName = _dr[29].ToString();
                                        objBEUserDataUpLoad.ManagerPositionNumber = _dr[30].ToString();
                                        objBEUserDataUpLoad.ManagerPositionName = _dr[31].ToString();
                                        objBEUserDataUpLoad.EmploymentStatus = _dr[32].ToString();
                                        //        objBEUserDataUpLoad.Status = "SUCCESS";
                                        //        objBEUserDataUpLoad.Remarks = "Record saved successfully!";                                    
                                    }
                                    if (errMsg != "")
                                    {
                                        objBEUserDataUpLoad.EmployeeNumber = _dr[0].ToString();
                                        objBEUserDataUpLoad.FirstName = _dr[2].ToString();
                                        objBEUserDataUpLoad.LastName = _dr[3].ToString();
                                        //           objBEUserDataUpLoad.Status = "FAIL";
                                        //   objBEUserDataUpLoad.Remarks = errMsg;

                                        string Name = "";

                                        if (_dr[2].ToString() == "" & _dr[3].ToString() == "")
                                            Name = "Name Missing! ";
                                        else
                                            Name = _dr[2].ToString() + " " + _dr[3].ToString();


                                        LogData(dirPath, fileName, " \t  FAIL Record :  " + Name + ":" + errMsg, " FAIL Record :" + Name + ": " + errMsg + "                 " + now);


                                    }
                                    if (errMsg != "Token Number is missing!" && errMsg != "Invalid EmailID!" && errMsg != "Token Number and EmailID Missing!"
                                        && errMsg != "Token Number is missing! and Invalid EmailID!" && errMsg != "First Name is missing! and Required Field is Missing!"
                                        && errMsg != "First Name is missing! and Required Field is Missing!" && errMsg != "Names are missing! and Required Field is Missing!")
                                    {
                                        objBEUserDataUpLoadColl.Add(objBEUserDataUpLoad);
                                        successCounter = successCounter + 1;
                                    }
                                    else
                                    {
                                        errCounter = errCounter + 1;
                                    }
                                }
                            }
                            //Call to function and it will return the no. of row inserted.
                            rowInserted = UpLoadExcel(objBEUserDataUpLoadColl, dirPath, fileName, dtMain);
                            //if (rowInserted != "")
                            //{
                            //    // if (lblError.Text != "Please Upload Correct Excel File" && excelSheetNo != 1)
                            //    lblError.Text = "" + rowInserted + "";
                            //    txtSearchBox.Text = "";
                            //    ShowGrid();
                            //}
                            //else
                            //{
                            //    lblError.Text = "Failure in Process";
                            //    lblError.CssClass = "redClass";
                            //}
                            strreturnvalue = "0:"+rowInserted;
                        }
                        else
                        {
                            if (excelSheetNo == 1)
                            {
                                //lblError.Text = "Please Upload Correct Excel File";
                                //lblError.CssClass = "redClass";
                                string strerr="1:Please Upload Correct Excel File";
                                strreturnvalue = strerr;
                            }
                        }
                    }

                }
                return strreturnvalue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region function Upload Excel
        //In this function we are creating datatavble same fields like in Excel for importing then one by one row will create and data will be stored to table.
        public string UpLoadExcel(BE_UserExcelUploadColl objBEUserDataUpLoadColl, string dirpath, string FileName, DataTable dtMain)
        {
            try
            {
                object _stat;
                string messageforInsert = "", messageforUpdate = "";
                int _status = 0;
                DAL_Common objDALCommon = new DAL_Common();

                DataTable MasUserTable;
                DataTable ExcelTable;
                MasUserTable = objBL_UserExcelUpload.GetMasUserTable();
                ExcelTable = dtMain;

                var query = from MasUserTable2 in MasUserTable.AsEnumerable()
                            join ExcelTable2 in ExcelTable.AsEnumerable()
                            on MasUserTable2.Field<string>("TokenNumber") equals ExcelTable2.Field<string>("Employee Number")
                            where MasUserTable2.Field<string>("TokenNumber") == ExcelTable2.Field<string>("Employee Number")
                            select new
                            {
                                TokenNumber = MasUserTable2.Field<string>("TokenNumber")

                            };

                List<string> ExistTokenNumbers = new List<string>();
                foreach (var token in query)
                {
                    ExistTokenNumbers.Add(token.TokenNumber);
                }

                DataTable dtforInsert = new DataTable();
                dtforInsert.Columns.Add("FirstName", typeof(string));
                dtforInsert.Columns.Add("LastName", typeof(string));
                dtforInsert.Columns.Add("TokenNumber", typeof(string));
                dtforInsert.Columns.Add("ProcessCode", typeof(string));
                dtforInsert.Columns.Add("ProcessN", typeof(string));
                dtforInsert.Columns.Add("Division_PA", typeof(string));
                dtforInsert.Columns.Add("DivisionName_PA", typeof(string));
                dtforInsert.Columns.Add("Location_PSA", typeof(string));
                dtforInsert.Columns.Add("LocationName_PSA", typeof(string));
                dtforInsert.Columns.Add("DesigCode", typeof(string));
                dtforInsert.Columns.Add("Designation", typeof(string));
                dtforInsert.Columns.Add("EmailID", typeof(string));
                dtforInsert.Columns.Add("DOB", typeof(DateTime));
                dtforInsert.Columns.Add("Gender", typeof(string));
                dtforInsert.Columns.Add("DOJ", typeof(DateTime));
                dtforInsert.Columns.Add("EmpGradeCode_E", typeof(string));
                dtforInsert.Columns.Add("EmpGradeName_E", typeof(string));
                dtforInsert.Columns.Add("EmployeeLevelCode_ES", typeof(string));
                dtforInsert.Columns.Add("EmployeeLevelName_ES", typeof(string));
                dtforInsert.Columns.Add("OrgUnitCode", typeof(string));
                dtforInsert.Columns.Add("OrgUnitName", typeof(string));
                dtforInsert.Columns.Add("Levels", typeof(int));
                dtforInsert.Columns.Add("EmployeeName", typeof(string));
                dtforInsert.Columns.Add("EmployeeCostCenterNumber", typeof(string));
                dtforInsert.Columns.Add("EmployeeCostCenterName", typeof(string));
                dtforInsert.Columns.Add("CompanyCode", typeof(string));
                dtforInsert.Columns.Add("PayrollAreaText", typeof(string));
                dtforInsert.Columns.Add("PayrollAreaText1", typeof(string));
                dtforInsert.Columns.Add("MangerNumber", typeof(string));
                dtforInsert.Columns.Add("MangerName", typeof(string));
                dtforInsert.Columns.Add("ManagerPositionNumber", typeof(string));
                dtforInsert.Columns.Add("ManagerPositionName", typeof(string));
                dtforInsert.Columns.Add("EmploymentStatus", typeof(string));

                DataTable dtforUpdate = new DataTable();
                dtforUpdate.Columns.Add("FirstName", typeof(string));
                dtforUpdate.Columns.Add("LastName", typeof(string));
                dtforUpdate.Columns.Add("TokenNumber", typeof(string));
                dtforUpdate.Columns.Add("ProcessCode", typeof(string));
                dtforUpdate.Columns.Add("ProcessN", typeof(string));
                dtforUpdate.Columns.Add("Division_PA", typeof(string));
                dtforUpdate.Columns.Add("DivisionName_PA", typeof(string));
                dtforUpdate.Columns.Add("Location_PSA", typeof(string));
                dtforUpdate.Columns.Add("LocationName_PSA", typeof(string));
                dtforUpdate.Columns.Add("DesigCode", typeof(string));
                dtforUpdate.Columns.Add("Designation", typeof(string));
                dtforUpdate.Columns.Add("EmailID", typeof(string));
                dtforUpdate.Columns.Add("DOB", typeof(DateTime));
                dtforUpdate.Columns.Add("Gender", typeof(string));
                dtforUpdate.Columns.Add("DOJ", typeof(DateTime));
                dtforUpdate.Columns.Add("EmpGradeCode_E", typeof(string));
                dtforUpdate.Columns.Add("EmpGradeName_E", typeof(string));
                dtforUpdate.Columns.Add("EmployeeLevelCode_ES", typeof(string));
                dtforUpdate.Columns.Add("EmployeeLevelName_ES", typeof(string));
                dtforUpdate.Columns.Add("OrgUnitCode", typeof(string));
                dtforUpdate.Columns.Add("OrgUnitName", typeof(string));
                dtforUpdate.Columns.Add("Levels", typeof(int));
                dtforUpdate.Columns.Add("EmployeeName", typeof(string));
                dtforUpdate.Columns.Add("EmployeeCostCenterNumber", typeof(string));
                dtforUpdate.Columns.Add("EmployeeCostCenterName", typeof(string));
                dtforUpdate.Columns.Add("CompanyCode", typeof(string));
                dtforUpdate.Columns.Add("PayrollAreaText", typeof(string));
                dtforUpdate.Columns.Add("PayrollAreaText1", typeof(string));
                dtforUpdate.Columns.Add("MangerNumber", typeof(string));
                dtforUpdate.Columns.Add("MangerName", typeof(string));
                dtforUpdate.Columns.Add("ManagerPositionNumber", typeof(string));
                dtforUpdate.Columns.Add("ManagerPositionName", typeof(string));
                dtforUpdate.Columns.Add("EmploymentStatus", typeof(string));

                foreach (BE_UserExcelUpload objUserDataUpLoad in objBEUserDataUpLoadColl)
                {
                    if (ExistTokenNumbers.Count == 0)
                    {
                        DataRow _dr = dtforInsert.NewRow();

                        _dr["FirstName"] = objUserDataUpLoad.FirstName;
                        _dr["LastName"] = objUserDataUpLoad.LastName;
                        _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                        _dr["ProcessCode"] = objUserDataUpLoad.BUNo;
                        _dr["ProcessN"] = objUserDataUpLoad.BusinessUnit;
                        _dr["Division_PA"] = objUserDataUpLoad.PA;
                        _dr["DivisionName_PA"] = objUserDataUpLoad.PersonnelArea;
                        _dr["Location_PSA"] = objUserDataUpLoad.PSA;
                        _dr["LocationName_PSA"] = objUserDataUpLoad.PersonnelSubarea;
                        _dr["DesigCode"] = objUserDataUpLoad.EmployeePositionNumber;
                        _dr["Designation"] = objUserDataUpLoad.EmployeePositionName;
                        _dr["EmailID"] = objUserDataUpLoad.EmailID;

                        if (objUserDataUpLoad.DateofBirth != null)
                        {
                            _dr["DOB"] = Convert.ToDateTime(objUserDataUpLoad.DateofBirth);
                        }
                        else
                            _dr["DOB"] = DBNull.Value;
                        _dr["Gender"] = objUserDataUpLoad.Gender;

                        if (objUserDataUpLoad.DateofJoining != null)
                        {
                            _dr["DOJ"] = Convert.ToDateTime(objUserDataUpLoad.DateofJoining);
                        }
                        else
                            _dr["DOJ"] = DBNull.Value;


                        _dr["EmpGradeCode_E"] = objUserDataUpLoad.EG;
                        _dr["EmpGradeName_E"] = objUserDataUpLoad.EmployeeGroup;
                        _dr["EmployeeLevelCode_ES"] = objUserDataUpLoad.ESG;
                        _dr["EmployeeLevelName_ES"] = objUserDataUpLoad.EmployeeSubgroup;
                        _dr["OrgUnitCode"] = objUserDataUpLoad.EmployeeOrgUnitNumber;
                        _dr["OrgUnitName"] = objUserDataUpLoad.EmployeeOrgUnitName;
                        _dr["Levels"] = objUserDataUpLoad.Counter != null ? objUserDataUpLoad.Counter : 0;
                        _dr["EmployeeName"] = objUserDataUpLoad.EmployeeName;
                        _dr["EmployeeCostCenterNumber"] = objUserDataUpLoad.EmployeeCostCenterNumber;
                        _dr["EmployeeCostCenterName"] = objUserDataUpLoad.EmployeeCostCenterName;
                        _dr["CompanyCode"] = objUserDataUpLoad.CompanyCode;
                        _dr["PayrollAreaText"] = objUserDataUpLoad.PayrollAreaCode;
                        _dr["PayrollAreaText1"] = objUserDataUpLoad.PayrollAreaText;
                        _dr["MangerNumber"] = objUserDataUpLoad.MangerNumber;
                        _dr["MangerName"] = objUserDataUpLoad.MangerName;
                        _dr["ManagerPositionNumber"] = objUserDataUpLoad.ManagerPositionNumber;
                        _dr["ManagerPositionName"] = objUserDataUpLoad.ManagerPositionName;
                        _dr["EmploymentStatus"] = objUserDataUpLoad.EmploymentStatus;
                        //      _dr["Status"] = objUserDataUpLoad.Status;
                        // _dr["Remark"] = objUserDataUpLoad.Remarks;
                        dtforInsert.Rows.Add(_dr);
                    }
                    else
                    {

                        if (ExistTokenNumbers.Contains(objUserDataUpLoad.EmployeeNumber))
                        {
                            // Update

                            DataRow _dr = dtforUpdate.NewRow();

                            _dr["FirstName"] = objUserDataUpLoad.FirstName;
                            _dr["LastName"] = objUserDataUpLoad.LastName;
                            _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                            _dr["ProcessCode"] = objUserDataUpLoad.BUNo;
                            _dr["ProcessN"] = objUserDataUpLoad.BusinessUnit;
                            _dr["Division_PA"] = objUserDataUpLoad.PA;
                            _dr["DivisionName_PA"] = objUserDataUpLoad.PersonnelArea;
                            _dr["Location_PSA"] = objUserDataUpLoad.PSA;
                            _dr["LocationName_PSA"] = objUserDataUpLoad.PersonnelSubarea;
                            _dr["DesigCode"] = objUserDataUpLoad.EmployeePositionNumber;
                            _dr["Designation"] = objUserDataUpLoad.EmployeePositionName;
                            _dr["EmailID"] = objUserDataUpLoad.EmailID;

                            if (objUserDataUpLoad.DateofBirth != null)
                            {
                                _dr["DOB"] = Convert.ToDateTime(objUserDataUpLoad.DateofBirth);
                            }
                            else
                                _dr["DOB"] = DBNull.Value;
                            _dr["Gender"] = objUserDataUpLoad.Gender;

                            if (objUserDataUpLoad.DateofJoining != null)
                            {
                                _dr["DOJ"] = Convert.ToDateTime(objUserDataUpLoad.DateofJoining);
                            }
                            else
                                _dr["DOJ"] = DBNull.Value;


                            _dr["EmpGradeCode_E"] = objUserDataUpLoad.EG;
                            _dr["EmpGradeName_E"] = objUserDataUpLoad.EmployeeGroup;
                            _dr["EmployeeLevelCode_ES"] = objUserDataUpLoad.ESG;
                            _dr["EmployeeLevelName_ES"] = objUserDataUpLoad.EmployeeSubgroup;
                            _dr["OrgUnitCode"] = objUserDataUpLoad.EmployeeOrgUnitNumber;
                            _dr["OrgUnitName"] = objUserDataUpLoad.EmployeeOrgUnitName;
                            _dr["Levels"] = objUserDataUpLoad.Counter != null ? objUserDataUpLoad.Counter : 0;
                            _dr["EmployeeName"] = objUserDataUpLoad.EmployeeName;
                            _dr["EmployeeCostCenterNumber"] = objUserDataUpLoad.EmployeeCostCenterNumber;
                            _dr["EmployeeCostCenterName"] = objUserDataUpLoad.EmployeeCostCenterName;
                            _dr["CompanyCode"] = objUserDataUpLoad.CompanyCode;
                            _dr["PayrollAreaText"] = objUserDataUpLoad.PayrollAreaCode;
                            _dr["PayrollAreaText1"] = objUserDataUpLoad.PayrollAreaText;
                            _dr["MangerNumber"] = objUserDataUpLoad.MangerNumber;
                            _dr["MangerName"] = objUserDataUpLoad.MangerName;
                            _dr["ManagerPositionNumber"] = objUserDataUpLoad.ManagerPositionNumber;
                            _dr["ManagerPositionName"] = objUserDataUpLoad.ManagerPositionName;
                            _dr["EmploymentStatus"] = objUserDataUpLoad.EmploymentStatus;
                            //      _dr["Status"] = objUserDataUpLoad.Status;
                            // _dr["Remark"] = objUserDataUpLoad.Remarks;
                            dtforUpdate.Rows.Add(_dr);
                        }
                        else
                        {
                            //Insert

                            DataRow _dr = dtforInsert.NewRow();

                            _dr["FirstName"] = objUserDataUpLoad.FirstName;
                            _dr["LastName"] = objUserDataUpLoad.LastName;
                            _dr["TokenNumber"] = objUserDataUpLoad.EmployeeNumber;
                            _dr["ProcessCode"] = objUserDataUpLoad.BUNo;
                            _dr["ProcessN"] = objUserDataUpLoad.BusinessUnit;
                            _dr["Division_PA"] = objUserDataUpLoad.PA;
                            _dr["DivisionName_PA"] = objUserDataUpLoad.PersonnelArea;
                            _dr["Location_PSA"] = objUserDataUpLoad.PSA;
                            _dr["LocationName_PSA"] = objUserDataUpLoad.PersonnelSubarea;
                            _dr["DesigCode"] = objUserDataUpLoad.EmployeePositionNumber;
                            _dr["Designation"] = objUserDataUpLoad.EmployeePositionName;
                            _dr["EmailID"] = objUserDataUpLoad.EmailID;

                            if (objUserDataUpLoad.DateofBirth != null)
                            {
                                _dr["DOB"] = Convert.ToDateTime(objUserDataUpLoad.DateofBirth);
                            }
                            else
                                _dr["DOB"] = DBNull.Value;
                            _dr["Gender"] = objUserDataUpLoad.Gender;

                            if (objUserDataUpLoad.DateofJoining != null)
                            {
                                _dr["DOJ"] = Convert.ToDateTime(objUserDataUpLoad.DateofJoining);
                            }
                            else
                                _dr["DOJ"] = DBNull.Value;


                            _dr["EmpGradeCode_E"] = objUserDataUpLoad.EG;
                            _dr["EmpGradeName_E"] = objUserDataUpLoad.EmployeeGroup;
                            _dr["EmployeeLevelCode_ES"] = objUserDataUpLoad.ESG;
                            _dr["EmployeeLevelName_ES"] = objUserDataUpLoad.EmployeeSubgroup;
                            _dr["OrgUnitCode"] = objUserDataUpLoad.EmployeeOrgUnitNumber;
                            _dr["OrgUnitName"] = objUserDataUpLoad.EmployeeOrgUnitName;
                            _dr["Levels"] = objUserDataUpLoad.Counter != null ? objUserDataUpLoad.Counter : 0;
                            _dr["EmployeeName"] = objUserDataUpLoad.EmployeeName;
                            _dr["EmployeeCostCenterNumber"] = objUserDataUpLoad.EmployeeCostCenterNumber;
                            _dr["EmployeeCostCenterName"] = objUserDataUpLoad.EmployeeCostCenterName;
                            _dr["CompanyCode"] = objUserDataUpLoad.CompanyCode;
                            _dr["PayrollAreaText"] = objUserDataUpLoad.PayrollAreaCode;
                            _dr["PayrollAreaText1"] = objUserDataUpLoad.PayrollAreaText;
                            _dr["MangerNumber"] = objUserDataUpLoad.MangerNumber;
                            _dr["MangerName"] = objUserDataUpLoad.MangerName;
                            _dr["ManagerPositionNumber"] = objUserDataUpLoad.ManagerPositionNumber;
                            _dr["ManagerPositionName"] = objUserDataUpLoad.ManagerPositionName;
                            _dr["EmploymentStatus"] = objUserDataUpLoad.EmploymentStatus;
                            //      _dr["Status"] = objUserDataUpLoad.Status;
                            // _dr["Remark"] = objUserDataUpLoad.Remarks;
                            dtforInsert.Rows.Add(_dr);
                        }

                    }

                }
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@MasUser_Upload", dtforInsert);

                //function call Save data which will send table and stored procedure name it will return no. of row inserted.

                messageforInsert = objBL_UserExcelUpload.SaveExcel(objSqlParam[0], 0);    //// 0 for Insert and 1 for Update Records
                objSqlParam[0] = new SqlParameter("@MasUser_Upload", dtforUpdate);
                messageforUpdate = objBL_UserExcelUpload.SaveExcel(objSqlParam[0], 1);
                LogData(dirpath, FileName, " \t", " ");  //for New Line
                LogData(dirpath, FileName, "\n", "Number of Failed Records  : " + errCounter);
                LogData(dirpath, FileName, "\n", "Number of Success Records : " + successCounter);
                //LogData(dirpath, FileName, "UpLoaded RowCount : " + _status, "UpLoaded RowCount : " + _status);
                LogData(dirpath, FileName, " " + messageforInsert, "" + messageforInsert);
                LogData(dirpath, FileName, " " + messageforUpdate, "" + messageforUpdate);
                string message = messageforInsert + "<br /> " + messageforUpdate;
                return message;
            }
            catch (Exception ex)
            {
                //ThrowAlertMessage(ex.Message);
                //ThrowAlertMessage("Please Check the Excel Sheet And Format");
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion
        

        #region function Check Blank()
        private Boolean CheckBlank(DataRow dr)
        {
            bool b = false;
            foreach (var item in dr.ItemArray)
            {
                if (item.ToString() != "")
                {
                    b = true;
                    break;
                }
            }
            return b;
        }
        #endregion

        #region function Check Excel Format()
        private string CheckExcelFormat(DataTable dt)
        {
            excelSheetNo = excelSheetNo + 1;
            string excel = "blank";
            foreach (var item in dt.Columns)
            {
                if (dt.Columns[0].ToString() == "Employee Number" && dt.Columns[1].ToString() == "Employee Name" && dt.Columns[2].ToString() == "First Name")
                {
                    excel = "valid";
                    break;
                }

            }
            return excel;
        }
        #endregion

        #region LogData Function
        private static void LogData(string dirpath, string FileName, string strMessage, string strDisplayMessage = "")
        {
            StreamWriter logFile;
        A:
            try
            {
                string FilePath = dirpath + "\\" + FileName;

                if (!File.Exists(FilePath.ToString()))               //("Log.txt"))
                {
                    logFile = new StreamWriter(FilePath);
                }
                else
                {
                    logFile = File.AppendText(FilePath.ToString());
                }
                logFile.WriteLine(strDisplayMessage);



                logFile.Close();
            }
            catch (Exception ex)
            {
                goto A;
            }
        }
        #endregion

        #region Validate Row
        private string ValidateRow(DataRow _dr, string dirpath, string FileName)
        {
            string errMsg = "";
            try
            {
                if (_dr[2].ToString() == "" && _dr[3].ToString() == "")
                {
                    if (_dr[0].ToString() == "" || _dr[13].ToString() == "")
                    {
                        errMsg = "Names are missing! and Required Field is Missing!";
                    }
                    else
                    {
                        errMsg = "Names are Missing!";
                    }

                }
                else if (_dr[2].ToString() == "")
                {
                    if (_dr[0].ToString() == "" || _dr[13].ToString() == "")
                    {
                        errMsg = "First Name is missing! and Required Field is Missing!";
                    }
                    else
                    {
                        errMsg = "First Name is missing!";
                    }

                }
                else if (_dr[3].ToString() == "")
                    errMsg = "Last Name is missing!";
                else if (_dr[0].ToString() == "")
                {
                    if (_dr[13].ToString() == "")
                    { errMsg = "Token Number and EmailID Missing!"; }
                    else
                    { errMsg = "Token Number is missing!"; }
                }
                else if (_dr[9].ToString() == "" || _dr[10].ToString() == "")
                    errMsg = "Division field is missing!";
                else if (_dr[11].ToString() == "" || _dr[11].ToString() == "")
                    errMsg = "Location field is missing!";
                else if (_dr[8].ToString() == "" || _dr[6].ToString() == "")
                    errMsg = "Employee Level field is missing!";
                else
                {
                    string DOB = _dr[25].ToString();
                    try
                    {
                        CultureInfo UsEn = new CultureInfo("de-DE");
                        DateTime dt = Convert.ToDateTime(_dr[25].ToString(), UsEn);
                        //DateTime dt = DateTime.ParseExact(DOB, "dd.MM.yyyy",null);
                        DOB = dt.ToString("dd/MM/yyyy");
                    }
                    catch (Exception ex)
                    {
                        errMsg = "Date of Birth is invalid! Valid date format is dd/MM/yyyy";
                    }

                    string DOJ = _dr[24].ToString();
                    try
                    {
                        CultureInfo UsEn = new CultureInfo("de-DE");
                        DateTime dt = Convert.ToDateTime(_dr[24].ToString(), UsEn);
                        DOJ = dt.ToString("dd/MM/yyyy");
                    }
                    catch (Exception ex)
                    {
                        errMsg = "Date of Joining is invalid! Valid date format is dd/MM/yyyy";
                    }
                }


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return errMsg;
        }
        #endregion

        #region Email Validation
        private bool isEmailValid(string emailID)
        {
            bool isValid = false;
            string pattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            try
            {
                Regex myRegex = new Regex(pattern);
                isValid = myRegex.IsMatch(emailID);
            }
            catch (Exception exp)
            {
            }
            return isValid;
        }
        #endregion
  }
}

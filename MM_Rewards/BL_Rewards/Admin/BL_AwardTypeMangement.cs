﻿using System;
using System.Collections.Generic;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Data;
namespace BL_Rewards.Admin
{
    public class BL_AwardTypeMangement
    {
        DataSet _ds;
        DAL_Common objDALCommon = new DAL_Common();
        //to retrive AwardType
        public BE_AwardTypeManagementColl GetData()
        {
            try
            {
                _ds = new DataSet();
                BE_AwardTypeManagementColl objBE_AwardTypeManagementColl = new BE_AwardTypeManagementColl();

                _ds = objDALCommon.DAL_GetData("usp_Mas_AwardType_view");
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {

                    BE_AwardTypeMangement objBE_AwardTypeMangement = new BE_AwardTypeMangement();

                    objBE_AwardTypeMangement.Id = Convert.ToInt16(_dr["ID"].ToString());
                    objBE_AwardTypeMangement.AwardName = _dr["AwardName"].ToString();
                    objBE_AwardTypeMangement.Isactive =Convert.ToBoolean(_dr["IsActive"].ToString());
                    objBE_AwardTypeManagementColl.Add(objBE_AwardTypeMangement);


                }
                return objBE_AwardTypeManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //to save Award name To AwardType
        public int SaveData(BE_AwardTypeMangement objBEAwardTypeMangement, out  int _ID)
        {
            try
            {
                object _stat;
                 _ID = 0;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[6];
                objSqlParam[0] = new SqlParameter("@ID", objBEAwardTypeMangement.Id);
                objSqlParam[1] = new SqlParameter("@AwardName", objBEAwardTypeMangement.AwardName);
                objSqlParam[2] = new SqlParameter("@CreatedBy", objBEAwardTypeMangement.Createdby);
                objSqlParam[3] = new SqlParameter("@ModifiedBy", objBEAwardTypeMangement.ModifiedBy);
                objSqlParam[4] = new SqlParameter("@IsActive", objBEAwardTypeMangement.Isactive);
                objSqlParam[5] = new SqlParameter("@IsDuplicate", SqlDbType.Int, 10, ParameterDirection.Output, false, 10, 10, "", DataRowVersion.Default, 0);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Mas_AwardType_Write", objSqlParam,'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);

                _ID = (int)objSqlParam[5].Value;
                return _ID;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        
        //public int DeleteData(int id)
        //{
        //    try
        //    {
        //        object _stat;
        //        int _status = 0;
        //        SqlParameter[] objSqlParam = new SqlParameter[1];
        //        objSqlParam[0] = new SqlParameter("@ID", id);
        //        objDALCommon = new DAL_Common();
        //        _stat = objDALCommon.DAL_SaveData("usp_Mas_AwardType_Delete", objSqlParam, 'N');

        //        _status = _stat == null ? 0 : Convert.ToInt32(_stat);
        //        return _status;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }

        //}
    }
}

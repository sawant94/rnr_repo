﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;

namespace BL_Rewards.Admin
{
   public class BL_RNRUserManagement
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();

         // get rnr user detail by token number
        public BE_RNRUserManagementColl GetDetailsByTokenNumber(string tokennumber)
        {
            try
            {
                ds = new DataSet();
                BE_RNRUserManagement objBE_RNRUserManagement = new BE_RNRUserManagement();
                BE_RNRUserManagementColl objBE_RNRUserManagementColl = new BE_RNRUserManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0]=new SqlParameter("@TokenNumber",tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_RNR_EmployeeDetails", objsqlparam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_RNRUserManagement.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_RNRUserManagement.EmployeeName = dr["EmployeeName"].ToString();
                    objBE_RNRUserManagement.BusinessUnit = dr["BusinessUnit"].ToString();
                    objBE_RNRUserManagement.DivisionName = dr["DivisionName_PA"].ToString();
                    objBE_RNRUserManagement.LocationName = dr["LocationName_PSA"].ToString();
                    objBE_RNRUserManagement.OrgUnitName =dr["OrgUnitName"].ToString();
                    objBE_RNRUserManagementColl.Add(objBE_RNRUserManagement);
                }
                return objBE_RNRUserManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        //get BusinessUnit for rnr user by token number 
        public BE_RNRUserManagementColl GetBusinessUnit(string tokennumber)
        {
            try
            {
                ds = new DataSet();
                BE_RNRUserManagement objBE_RNRUserManagement ;
                BE_RNRUserManagementColl objBE_RNRUserManagementColl = new BE_RNRUserManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@tokenumber", tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_BusinessUnit_ForRNRMapping", objsqlparam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_RNRUserManagement = new BE_RNRUserManagement();
                    objBE_RNRUserManagement.BusinessUnit = dr["BusinessUnit"].ToString();
                    objBE_RNRUserManagement.IsEnable = Convert.ToBoolean(dr["IsEnable"]);

                    if (objBE_RNRUserManagement.IsEnable)
                    {
                        objBE_RNRUserManagement.IsSelected = Convert.ToBoolean(dr["IsSelected"]);
                    }
                    else
                    {
                        objBE_RNRUserManagement.IsSelected = true;
                    }
                    objBE_RNRUserManagementColl.Add(objBE_RNRUserManagement);
                }
                return objBE_RNRUserManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }


         //get Division for rnr user by token number 
        public BE_RNRUserManagementColl GetDivision(string tokennumber)
        {
            try
            {
                ds = new DataSet();
                BE_RNRUserManagement objBE_RNRUserManagement ;
                BE_RNRUserManagementColl objBE_RNRUserManagementColl = new BE_RNRUserManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@tokenumber", tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_Division_ForRNRMapping", objsqlparam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_RNRUserManagement = new BE_RNRUserManagement();
                    objBE_RNRUserManagement.DivisionName = dr["DivisionName_PA"].ToString();
                    objBE_RNRUserManagement.DivisionCode = dr["Division_PA"].ToString();                   
                    objBE_RNRUserManagement.IsEnable = Convert.ToBoolean(dr["IsEnable"]);
                    if (objBE_RNRUserManagement.IsEnable)
                    {
                        objBE_RNRUserManagement.IsSelected = Convert.ToBoolean(dr["IsSelected"]);
                    }
                    else
                    {
                        objBE_RNRUserManagement.IsSelected = true;
                    }
                    objBE_RNRUserManagementColl.Add(objBE_RNRUserManagement);
                }
                return objBE_RNRUserManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        //get location for rnr user by token number 
        public BE_RNRUserManagementColl GetLocation(string tokennumber)
        {
            try
            {
                ds = new DataSet();
                BE_RNRUserManagement objBE_RNRUserManagement;
                BE_RNRUserManagementColl objBE_RNRUserManagementColl = new BE_RNRUserManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@tokenumber", tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_Location_ForRNRMapping", objsqlparam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_RNRUserManagement = new BE_RNRUserManagement();
                    objBE_RNRUserManagement.LocationName = dr["LocationName_PSA"].ToString();
                    objBE_RNRUserManagement.LocationCode = dr["Location_PSA"].ToString();                   
                    objBE_RNRUserManagement.IsEnable = Convert.ToBoolean(dr["IsEnable"]);
                    if (objBE_RNRUserManagement.IsEnable)
                    {
                     objBE_RNRUserManagement.IsSelected = Convert.ToBoolean(dr["IsSelected"]);
                    }
                    else
                    {
                        objBE_RNRUserManagement.IsSelected = true;
                    }
                   objBE_RNRUserManagementColl.Add(objBE_RNRUserManagement);
                }
                return objBE_RNRUserManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        //get Department for rnr user by token number 
        public BE_RNRUserManagementColl GetDepartment(string tokennumber)
        {
            try
            {
                ds = new DataSet();
                BE_RNRUserManagement objBE_RNRUserManagement ;
                BE_RNRUserManagementColl objBE_RNRUserManagementColl = new BE_RNRUserManagementColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@tokenumber", tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_OrgUnit_ForRNRMapping", objsqlparam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_RNRUserManagement = new BE_RNRUserManagement();
                    objBE_RNRUserManagement.OrgUnitName = dr["OrgUnitName"].ToString();
                    objBE_RNRUserManagement.OrgUnitCode = dr["OrgUnitCode"].ToString();
                    objBE_RNRUserManagement.IsEnable = Convert.ToBoolean(dr["IsEnable"]);
                    if (objBE_RNRUserManagement.IsEnable)
                    {
                        objBE_RNRUserManagement.IsSelected = Convert.ToBoolean(dr["IsSelected"]);
                    }
                    else
                    {
                        objBE_RNRUserManagement.IsSelected = true;
                    }
                    objBE_RNRUserManagementColl.Add(objBE_RNRUserManagement);
                }
                return objBE_RNRUserManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        // save BusinessUnit
        public int SaveBusinessUnit(BE_RNRUserManagement objBE_RNRUserManagement)
        {

            try
            {
                object _stat;
                int _status = 0;
                DataTable dt = new DataTable();
                dt.Columns.Add("BusinessUnit");

                foreach (string i in objBE_RNRUserManagement.BUnit)
                {
                    DataRow dr = dt.NewRow();
                    dr["BusinessUnit"] = i;
                    dt.Rows.Add(dr);
                }


                SqlParameter[] objSqlParam = new SqlParameter[3];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_RNRUserManagement.TokenNumber);
                objSqlParam[1] = new SqlParameter("@CreatedBy", objBE_RNRUserManagement.CreatedBy);
                objSqlParam[2] = new SqlParameter("@BusinessUnit", dt);
                _stat = objDALCommon.DAL_SaveData("usp_Insert_RNR_User_Mapping_BusinessUnit", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

         // save Division
        public int SaveDivision(BE_RNRUserManagement objBE_RNRUserManagement)
        {
           
            try
            {
                object _stat;
                int _status = 0;
                DataTable dt = new DataTable();
                dt.Columns.Add("Division_PA");

                foreach (string  i in objBE_RNRUserManagement.Division)
                {
                    DataRow dr = dt.NewRow();
                    dr["Division_PA"] = i;
                    dt.Rows.Add(dr);
                }
                

                SqlParameter[] objSqlParam = new SqlParameter[3];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_RNRUserManagement.TokenNumber);
                objSqlParam[1] = new SqlParameter("@CreatedBy", objBE_RNRUserManagement.CreatedBy);
                objSqlParam[2] = new SqlParameter("@Division_PA", dt);
                _stat = objDALCommon.DAL_SaveData("usp_Insert_RNR_User_Mapping_Division", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
         
        }
        // save location 
        public int SaveLocation(BE_RNRUserManagement objBE_RNRUserManagement)
        {

            try
            {
                object _stat;
                int _status = 0;
                DataTable dt = new DataTable();
                dt.Columns.Add("Location_PSA");

                foreach (string i in objBE_RNRUserManagement.Location)
                {
                    DataRow dr = dt.NewRow();
                    dr["Location_PSA"] = i;
                    dt.Rows.Add(dr);
                }


                SqlParameter[] objSqlParam = new SqlParameter[3];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_RNRUserManagement.TokenNumber);
                objSqlParam[1] = new SqlParameter("@CreatedBy", objBE_RNRUserManagement.CreatedBy);
                objSqlParam[2] = new SqlParameter("@Location_PSA", dt);
                _stat = objDALCommon.DAL_SaveData("usp_Insert_RNR_User_Mapping_Location", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        // save department
        public int SaveDepartment(BE_RNRUserManagement objBE_RNRUserManagement)
        {

            try
            {
                object _stat;
                int _status = 0;
                DataTable dt = new DataTable();
                dt.Columns.Add("OrgUnitCode");

                foreach (string i in objBE_RNRUserManagement.Orgunit)
                {
                    DataRow dr = dt.NewRow();
                    dr["OrgUnitCode"] = i;
                    dt.Rows.Add(dr);
                }

                SqlParameter[] objSqlParam = new SqlParameter[3];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_RNRUserManagement.TokenNumber);
                objSqlParam[1] = new SqlParameter("@CreatedBy", objBE_RNRUserManagement.CreatedBy);
                objSqlParam[2] = new SqlParameter("@OrgUnitCode", dt);
                _stat = objDALCommon.DAL_SaveData("usp_Insert_RNR_User_Mapping_Department", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
    }
}

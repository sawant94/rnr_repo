﻿using System;
using System.Collections.Generic;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Data;
namespace BL_Rewards.Admin
{
    public class BL_SalesAwardType
    {
        DataSet _ds;
        DAL_Common objDALCommon = new DAL_Common();

        //to retrive AwardType
        public BE_SalesAwardTypeColl GetData()
        {
            try
            {
                _ds = new DataSet();
                BE_SalesAwardTypeColl objBE_SalesAwardTypeColl = new BE_SalesAwardTypeColl();

                _ds = objDALCommon.DAL_GetData("usp_Mas_SalesAwardType_view");
                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {

                    BE_SalesAwardType objBE_SalesAwardType = new BE_SalesAwardType();

                    objBE_SalesAwardType.Id = Convert.ToInt16(_dr["ID"].ToString());
                    objBE_SalesAwardType.AwardName = _dr["AwardName"].ToString();
                    objBE_SalesAwardType.Isactive = Convert.ToBoolean(_dr["IsActive"].ToString());
                    objBE_SalesAwardType.IsbudgetTracking = Convert.ToBoolean(_dr["IsbudgetTracking"].ToString());
                    objBE_SalesAwardTypeColl.Add(objBE_SalesAwardType);


                }
                return objBE_SalesAwardTypeColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //to save Award name To AwardType
        public int SaveData(BE_SalesAwardType objBE_SalesAwardType, out  int _ID)
        {
            try
            {
                object _stat;
                _ID = 0;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[7];
                objSqlParam[0] = new SqlParameter("@ID", objBE_SalesAwardType.Id);
                objSqlParam[1] = new SqlParameter("@AwardName", objBE_SalesAwardType.AwardName);
                objSqlParam[2] = new SqlParameter("@CreatedBy", objBE_SalesAwardType.Createdby);
                objSqlParam[3] = new SqlParameter("@ModifiedBy", objBE_SalesAwardType.ModifiedBy);
                objSqlParam[4] = new SqlParameter("@IsActive", objBE_SalesAwardType.Isactive);
                objSqlParam[5] = new SqlParameter("@IsBudgetTracking", objBE_SalesAwardType.IsbudgetTracking);
                objSqlParam[6] = new SqlParameter("@IsDuplicate", SqlDbType.Int, 10, ParameterDirection.Output, false, 10, 10, "", DataRowVersion.Default, 0);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Mas_SalesAwardType_Write", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);

                _ID = (int)objSqlParam[6].Value;
                return _ID;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_SalesAwardType objBE_SalesAwardTypeColl { get; set; }
    }
}

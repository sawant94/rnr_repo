﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;

namespace BL_Rewards.Admin
{
    public sealed class BL_HolidayManagement
    {
        DataSet _ds;
        DAL_Common objDALCommon = new DAL_Common();
        //to getdropdownitem;
        public BE_HolidayManagementColl Getdropdownitem()
        {
            try
            {
                _ds = new DataSet();
                BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                _ds = objDALCommon.DAL_GetData("usp_Mas_Location_View");

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                    objBE_HolidayManagement.Locationcode = _dr["Location_PSA"].ToString();
                    objBE_HolidayManagement.Locationname = _dr["LocationName_PSA"].ToString();
                    objBE_HolidayManagementColl.Add(objBE_HolidayManagement);
                }
                return objBE_HolidayManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        // TO retrived data for gridview
        public BE_HolidayManagementColl getHolidayList(string locationcode)
        {
            try
            {
                _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@LocationCode", locationcode);
                BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                _ds = objDALCommon.DAL_GetData("usp_GetMas_holidayList_view", objsqlparam);

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                    objBE_HolidayManagement.ID = Convert.ToInt16(_dr["Id"].ToString());
                    objBE_HolidayManagement.Holidaydate = _dr["HolidayDate"].ToString();
                    objBE_HolidayManagement.Holidayname = _dr["HolidayName"].ToString();
                    objBE_HolidayManagement.Sattype = Convert.ToInt16(_dr["SatType"].ToString());
                    objBE_HolidayManagementColl.Add(objBE_HolidayManagement);
                }
                return objBE_HolidayManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }



        }
        //to save data into Mas_HolidayList and Mas_SaturdayHolidayList
        public int saveData(BE_HolidayManagement objBE_HolidayManagement, out  int _status)
        {
            try
            {
                object _stat;
               _status = 0;

                SqlParameter[] objSqlParam = new SqlParameter[9];
                objSqlParam[0] = new SqlParameter("@ID", objBE_HolidayManagement.ID);
                objSqlParam[1] = new SqlParameter("@HolidayName", objBE_HolidayManagement.Holidayname);
                objSqlParam[2] = new SqlParameter("@HolidayDate", objBE_HolidayManagement.Holidaydate);
                objSqlParam[4] = new SqlParameter("@CreatedBy", objBE_HolidayManagement.Createdby);
                objSqlParam[5] = new SqlParameter("@ModifiedBy", objBE_HolidayManagement.Modifiedby);
               // objSqlParam[6] = new SqlParameter("@LocationCode", objBE_HolidayManagement.Locationcode);
                objSqlParam[7] = new SqlParameter("@SatType", objBE_HolidayManagement.Sattype);
                objSqlParam[8] = new SqlParameter("@IsDuplicate",SqlDbType.Int,10,ParameterDirection.Output,false,10,10,"",DataRowVersion.Default,0 );
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Mas_HolidayList_Write", objSqlParam, 'N');
              
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }


        }
        public int DeleteData(int id)
        {
            try
            {
                object _stat;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@ID", id);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_MasHolidayList_Delete", objSqlParam, 'N');

                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }



        }

        public BE_HolidayManagementColl GetHolidayList(string locationcode)
        {
            try
            {
                _ds = new DataSet();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@LocationCode", locationcode);
                BE_HolidayManagementColl objBE_HolidayManagementColl = new BE_HolidayManagementColl();
                _ds = objDALCommon.DAL_GetData("usp_GetMas_holidayList_view1");

                foreach (DataRow _dr in _ds.Tables[0].Rows)
                {
                    BE_HolidayManagement objBE_HolidayManagement = new BE_HolidayManagement();
                    objBE_HolidayManagement.ID = Convert.ToInt16(_dr["Id"].ToString());
                    objBE_HolidayManagement.Calendarname = _dr["Calendarname"].ToString();
                    objBE_HolidayManagement.NoOfLocations =  Convert.ToInt16(_dr["NoOfLocations"]);
                    
                    objBE_HolidayManagement.Holidaydate = _dr["HolidayDate"].ToString();
                    objBE_HolidayManagement.Holidayname = _dr["HolidayName"].ToString();
                    //objBE_HolidayManagement.Sattype = Convert.ToInt16(_dr["SatType"].ToString());
                    DataRow[] drLocationCode = _ds.Tables[1].Select("HolidayID=" + objBE_HolidayManagement.ID);

                    foreach (DataRow dr in drLocationCode)
                    {
                        objBE_HolidayManagement.LocationCodeColl.Add(dr["LocationCode"].ToString());
                    }
                    objBE_HolidayManagementColl.Add(objBE_HolidayManagement);
                }

              
                return objBE_HolidayManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }



        }

        public int SaveHoliday(BE_HolidayManagement objBE_HolidayManagement, out  int _status)
        {
            try
            {
                object _stat;
               _status = 0;

                SqlParameter[] objSqlParam = new SqlParameter[10];
                objSqlParam[0] = new SqlParameter("@ID", objBE_HolidayManagement.ID);
                objSqlParam[1] = new SqlParameter("@CalendarName", objBE_HolidayManagement.Calendarname);
                objSqlParam[2] = new SqlParameter("@NoOfLocations", objBE_HolidayManagement.NoOfLocations);
                objSqlParam[3] = new SqlParameter("@HolidayName", objBE_HolidayManagement.Holidayname);
                objSqlParam[4] = new SqlParameter("@HolidayDate", objBE_HolidayManagement.Holidaydate);
                objSqlParam[5] = new SqlParameter("@CreatedBy", objBE_HolidayManagement.Createdby);
                objSqlParam[6] = new SqlParameter("@ModifiedBy", objBE_HolidayManagement.Modifiedby);
                objSqlParam[7] = new SqlParameter("@LocationCode", objBE_HolidayManagement.Locationcode);
                objSqlParam[8] = new SqlParameter("@SatType", objBE_HolidayManagement.Sattype);
                objSqlParam[9] = new SqlParameter("@IsDuplicate",SqlDbType.Int,10,ParameterDirection.Output,false,10,10,"",DataRowVersion.Default,0 );
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_InsertUpdate_Mas_HolidayList", objSqlParam, 'N');
              
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);
                return _status;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }


        }

        
    }


}



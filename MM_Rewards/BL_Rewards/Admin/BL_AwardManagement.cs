﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;

namespace BL_Rewards.Admin
{
    public sealed class  BL_AwardManagement
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        // to populate selected 
        public BE_AwardManagement GetData(int ID)
        {
            try
            {
                ds = new DataSet();
               // BE_AwardManagement objAwardManagement = new BE_AwardManagement();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@ID", ID);
                ds = objDALCommon.DAL_GetData("usp_Get_AwardManagement", objsqlparam);
                BE_AwardManagement objBE_AwardManagement = new BE_AwardManagement();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    
                    objBE_AwardManagement.Id = Convert.ToInt32(dr["ID"]);
                    objBE_AwardManagement.AwardType = dr["AwardType"].ToString();
                    objBE_AwardManagement.AwardTypeText = dr["AwardTypeText"].ToString();
                    objBE_AwardManagement.Name = dr["Name"].ToString();
                    objBE_AwardManagement.Amount = Convert.ToDecimal(dr["Amount"]);
                    objBE_AwardManagement.Active = Convert.ToBoolean(dr["IsActive"]);
                    objBE_AwardManagement.TypeIsActive = Convert.ToBoolean(dr["TypeIsActive"]);
                    objBE_AwardManagement.CreatedBy = dr["CreatedBy"].ToString();
                    objBE_AwardManagement.Autoredeem = Convert.ToBoolean(dr["AutoRedeem"]);
                    if(dr["CreatedDate"].ToString()!="")
                        objBE_AwardManagement.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    objBE_AwardManagement.ModifiedBy = dr["ModifiedBy"].ToString();
                    if (dr["ModifiedDate"].ToString() != "")
                        objBE_AwardManagement.ModifiedDate = (Convert.ToString(dr["ModifiedDate"]) == "") ? (DateTime?)null : Convert.ToDateTime(dr["ModifiedDate"]);
                }

                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    objBE_AwardManagement.GiverLevel.Add(Convert.ToInt32(dr["EligibilityLevel"]));
                }

                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    objBE_AwardManagement.RecipientLevel.Add(Convert.ToInt32(dr["EligibilityLevel"]));
                }

                return objBE_AwardManagement;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        
        
        }
        //to populate gridview data
        public BE_AwardManagementColl GetData()
        {
            try
            {
                ds = new DataSet();

                BE_AwardManagement objBE_AwardManagement;
                BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();

                ds = objDALCommon.DAL_GetData("usp_Get_AwardManagement");                

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_AwardManagement = new BE_AwardManagement();

                    objBE_AwardManagement.Id = Convert.ToInt32(dr["ID"]);
                   // objBE_AwardManagement.AwardType = dr["AwardType"].ToString();
                    objBE_AwardManagement.AwardTypeText = dr["AwardType"].ToString();
                    objBE_AwardManagement.Name = dr["Name"].ToString();
                    objBE_AwardManagement.Amount = Convert.ToDecimal(dr["Amount"]);
                    objBE_AwardManagement.Active = Convert.ToBoolean(dr["IsActive"]);
                    objBE_AwardManagement.TypeIsActive = Convert.ToBoolean(dr["TypeIsActive"]);
                    objBE_AwardManagement.CreatedBy = dr["CreatedBy"].ToString();
                    objBE_AwardManagement.Autoredeem = Convert.ToBoolean(dr["AutoRedeem"]);
                    objBE_AwardManagement.IsSystem = Convert.ToBoolean(dr["IsSystem"]);
                    if (dr["CreatedDate"].ToString() != "")
                        objBE_AwardManagement.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    objBE_AwardManagement.ModifiedBy = dr["ModifiedBy"].ToString();
                    if (dr["ModifiedDate"].ToString() != "")
                        objBE_AwardManagement.ModifiedDate = (Convert.ToString(dr["ModifiedDate"]) == "") ? (DateTime?)null : Convert.ToDateTime(dr["ModifiedDate"]);

                    DataRow[] drgiverLevels = ds.Tables[1].Select("AwardID=" + objBE_AwardManagement.Id);

                    foreach (DataRow d in drgiverLevels)
                    {
                        objBE_AwardManagement.GiverLevel.Add(Convert.ToInt32(d["EligibilityLevel"].ToString()));
                    }
                    DataRow[] drRecipientLevels = ds.Tables[2].Select("AwardID=" + objBE_AwardManagement.Id);

                    foreach (DataRow d in drRecipientLevels)
                    {
                        objBE_AwardManagement.RecipientLevel.Add(Convert.ToInt32(d["EligibilityLevel"].ToString()));
                    }
                    objBE_AwardManagementColl.Add(objBE_AwardManagement);
                }
                return objBE_AwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        //to save data to awardmanagment
        public int SaveData(BE_AwardManagement objBE_AwardManagement) 
        {
            int Id = 0;
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("EligibilityLevel");

                foreach (int i in objBE_AwardManagement.GiverLevel)
                {
                    DataRow dr = dt.NewRow();
                    dr["EligibilityLevel"] = i;
                    dt.Rows.Add(dr);
                }
                DataTable dtrecipientlevel = new DataTable();
                dtrecipientlevel.Columns.Add("EligibilityLevel");

                foreach (int i in objBE_AwardManagement.RecipientLevel)
                {
                    DataRow dr = dtrecipientlevel.NewRow();
                    dr["EligibilityLevel"] = i;
                    dtrecipientlevel.Rows.Add(dr);
                }        

                SqlParameter[] objSqlParam = new SqlParameter[10];
                objSqlParam[0] = new SqlParameter("@ID", objBE_AwardManagement.Id);
                objSqlParam[1] = new SqlParameter("@AwardType", objBE_AwardManagement.AwardId);
                objSqlParam[2] = new SqlParameter("@Name", objBE_AwardManagement.Name);
                objSqlParam[3] = new SqlParameter("@Amount", objBE_AwardManagement.Amount);
                objSqlParam[4] = new SqlParameter("@CreatedBy", objBE_AwardManagement.CreatedBy);
                objSqlParam[5] = new SqlParameter("@ModifiedBy", objBE_AwardManagement.ModifiedBy);
                objSqlParam[6] = new SqlParameter("@IsActive", objBE_AwardManagement.Active);
                objSqlParam[7] = new SqlParameter("@IsAuto", objBE_AwardManagement.Autoredeem);
                objSqlParam[8] = new SqlParameter("@EmployeeLevels", dt);
                objSqlParam[9] = new SqlParameter("@RecipientLevels", dtrecipientlevel);
                Id = (int)objDALCommon.DAL_SaveData("usp_InsertUpdate_Mas_Awards", objSqlParam, 'N');

                //foreach (int i in objBE_AwardManagement.Levels)
                //{
                //    SqlParameter[] objSqlParamLevels = new SqlParameter[2];
                //    objSqlParamLevels[0] = new SqlParameter("@AwardID",objBE_AwardManagement.Id != 0?objBE_AwardManagement.Id: Id);
                //    objSqlParamLevels[1] = new SqlParameter("@Level", i);
                //    objDALCommon = new DAL_Common();
                //    objDALCommon.DAL_SaveData("usp_Insert_Award_Levels_Mapping", objSqlParamLevels, 'N');
                //}
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return Id;
        }

        //to get dropdown item
        public BE_AwardManagementColl Getdropdownitem()
        {
            try
            {
                ds = new DataSet();
                BE_AwardManagement objBE_AwardManagement;
                BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
                SqlParameter[] objSqlParam = new SqlParameter[1];
               objSqlParam[0]= new SqlParameter("@Isactive", SqlDbType.Char,1, ParameterDirection.Input, false, 10, 10, "", DataRowVersion.Default, 'Y');
                ds = objDALCommon.DAL_GetData("usp_Mas_AwardType_view",objSqlParam);
                
                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_AwardManagement = new BE_AwardManagement();

                    objBE_AwardManagement.Id =Convert.ToInt16(_dr["ID"].ToString());
                    objBE_AwardManagement.AwardType = _dr["AwardName"].ToString();
                    objBE_AwardManagement.IsSystem = Convert.ToBoolean(_dr["IsSystem"].ToString());
                    objBE_AwardManagementColl.Add(objBE_AwardManagement);
                }
                return objBE_AwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }


        //public BE_AwardManagementColl GetEmployeeLevelsName()
        //{
        //    try
        //    {
        //        ds = new DataSet();
        //        BE_AwardManagement objBE_AwardManagement;
        //        BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
        //        ds = objDALCommon.DAL_GetData("usp_GetEmployeeLevelsName");

        //        foreach (DataRow _dr in ds.Tables[0].Rows)
        //        {
        //            objBE_AwardManagement = new BE_AwardManagement();

        //            objBE_AwardManagement.EmployeeLevelsCode = _dr["EmployeeLevelCode_ES"].ToString();
        //            objBE_AwardManagement.EmployeeLevelsName = _dr["EmployeeLevelName_ES"].ToString();
        //            objBE_AwardManagement.StringLevels = _dr["Levels"].ToString();
        //            //   objBE_AwardManagement.Active = Convert.ToBoolean(_dr["IsActive"].ToString());
        //            objBE_AwardManagementColl.Add(objBE_AwardManagement);
        //        }
        //        return objBE_AwardManagementColl;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArgumentException(ex.Message);
        //    }
        //}
        /// <summary>
        /// return award giver and recipient  level For award management page 
        /// </summary>
        /// <returns>BE_AwardManagementColl </returns>
        public BE_AwardManagementColl GetEmployeeLevelsName()
        {
            try
            {
                ds = new DataSet();
                BE_AwardManagement objBE_AwardManagement;
                BE_AwardManagementColl objBE_AwardManagementColl = new BE_AwardManagementColl();
                ds = objDALCommon.DAL_GetData("usp_Get_Employee_ForAward");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_AwardManagement = new BE_AwardManagement();
                                     
                    objBE_AwardManagement.EmployeeLevelsName = _dr["EmployeeLevelName_ES"].ToString();
                    objBE_AwardManagement.StringLevels = _dr["EligiblityLevel"].ToString();
                    //   objBE_AwardManagement.Active = Convert.ToBoolean(_dr["IsActive"].ToString());
                    //  objBE_AwardManagement.EmployeeLevelsCode = _dr["EmployeeLevelCode_ES"].ToString();

                    objBE_AwardManagementColl.Add(objBE_AwardManagement);
                }
                return objBE_AwardManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public StringBuilder CreateTable(BE_AwardManagementColl objBE_AwardManagementColl)
        {
            //StringBuilder LevelTable = "<table><tr><td><b>Eligibility Levels</b></td><td><b>Levels</b></td></tr><content></table>";
            StringBuilder LevelTable = new StringBuilder();
            StringBuilder strContent1 = new StringBuilder();
            StringBuilder strContent2 = new StringBuilder();
            int Count = objBE_AwardManagementColl.Count != 0 ? objBE_AwardManagementColl.Count / 2 : 0;
            int i = 0;
            try
            {
                foreach (BE_AwardManagement objBE_AwardManagement in objBE_AwardManagementColl)
                {
                    i++;
                    if (i % 2 != 0)
                    {
                        strContent1.Append("<tr>");
                        strContent1.Append("<td>" + objBE_AwardManagement.EmployeeLevelsName + "</td><td> " + objBE_AwardManagement.StringLevels + " </td>");
                        strContent1.Append("</tr>");
                    }
                    else
                    {
                        strContent2.Append("<tr>");
                        strContent2.Append("<td>" + objBE_AwardManagement.EmployeeLevelsName + "</td><td> " + objBE_AwardManagement.StringLevels + " </td>");
                        strContent2.Append("</tr>");
                    }
                }


                LevelTable.Append("<div  class='grid01'><table><tr><th>Eligibility Levels</th><th>Levels</th></tr>");
                LevelTable.Append(strContent1);
                LevelTable.Append("</table></div>");
                LevelTable.Append("<div  class='grid01'><table><tr><th>Eligibility Levels</th><th>Levels</th></tr>");
                LevelTable.Append(strContent2);
                LevelTable.Append("</table></div>");

            }
            catch (Exception ex)
            {

                throw;
            }

            return LevelTable;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data.SqlClient;
using System.Data;
namespace BL_Rewards.Admin
{
    public class BL_SalesAwardTypeManagement
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
        BE_SalesAwardTypeManagement objBE_SalesAwardTypeManagement;
        BE_SalesAwardTypeManagementColl objBE_SalesAwardTypeManagementColl;

        public BE_SalesAwardTypeManagementColl GetData()
        {
            try
            {
                ds = new DataSet();
                objBE_SalesAwardTypeManagementColl = new BE_SalesAwardTypeManagementColl();
                ds = objDALCommon.DAL_GetData("usp_Get_SalesAwardManagement");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_SalesAwardTypeManagement = new BE_SalesAwardTypeManagement();
                    objBE_SalesAwardTypeManagement.Id = Convert.ToInt32(dr["ID"]);
                    objBE_SalesAwardTypeManagement.AwardName = dr["AwardName"].ToString();
                    objBE_SalesAwardTypeManagement.AwardTypeId = Convert.ToInt32(dr["AwardTypeId"]);
                    objBE_SalesAwardTypeManagement.AwardType = dr["AwardType"].ToString();
                    objBE_SalesAwardTypeManagement.DivisionId = Convert.ToInt32(dr["DivisionId"]);
                    objBE_SalesAwardTypeManagement.divisionName = dr["Division"].ToString();
                    objBE_SalesAwardTypeManagement.Amount = Convert.ToDecimal(dr["Amount"]);
                    objBE_SalesAwardTypeManagement.CreatedBy = dr["CreatedBy"].ToString();
                    objBE_SalesAwardTypeManagement.ModifiedBy = dr["ModifiedBy"].ToString();
                    objBE_SalesAwardTypeManagement.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    objBE_SalesAwardTypeManagementColl.Add(objBE_SalesAwardTypeManagement);
                }
                return objBE_SalesAwardTypeManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_SalesAwardTypeManagementColl GetDivision()
        {
            try
            {
                ds = new DataSet();
                objBE_SalesAwardTypeManagementColl = new BE_SalesAwardTypeManagementColl();
                ds = objDALCommon.DAL_GetData("usp_Get_Division");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_SalesAwardTypeManagement = new BE_SalesAwardTypeManagement();
                    objBE_SalesAwardTypeManagement.DivisionId = Convert.ToInt32(dr["ID"]);
                    objBE_SalesAwardTypeManagement.DivisionName = dr["SALES_DIVISION"].ToString();
                    objBE_SalesAwardTypeManagementColl.Add(objBE_SalesAwardTypeManagement);
                }
                return objBE_SalesAwardTypeManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_SalesAwardTypeManagementColl GetSalesAwardType()
        {
            try
            {
                ds = new DataSet();
                objBE_SalesAwardTypeManagementColl = new BE_SalesAwardTypeManagementColl();
                ds = objDALCommon.DAL_GetData("usp_Get_SalesAwardType");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_SalesAwardTypeManagement = new BE_SalesAwardTypeManagement();
                    objBE_SalesAwardTypeManagement.AwardTypeId = Convert.ToInt32(dr["ID"]);
                    objBE_SalesAwardTypeManagement.AwardTypeName = dr["AwardName"].ToString();
                    objBE_SalesAwardTypeManagementColl.Add(objBE_SalesAwardTypeManagement);
                }
                return objBE_SalesAwardTypeManagementColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int SaveData(BE_SalesAwardTypeManagement objBE_SalesAwardTypeManagement, out  int _ID)
        {
            try
            {
                object _stat;
                _ID = 0;
                int _status = 0;
                SqlParameter[] objSqlParam = new SqlParameter[9];
                objSqlParam[0] = new SqlParameter("@ID", objBE_SalesAwardTypeManagement.Id);
                objSqlParam[1] = new SqlParameter("@AwardName", objBE_SalesAwardTypeManagement.AwardName);
                objSqlParam[2] = new SqlParameter("@AwardTypeID", objBE_SalesAwardTypeManagement.AwardTypeId);
                objSqlParam[3] = new SqlParameter("@DivisionID", objBE_SalesAwardTypeManagement.DivisionId);
                objSqlParam[4] = new SqlParameter("@Amount", objBE_SalesAwardTypeManagement.Amount);
                objSqlParam[5] = new SqlParameter("@CreatedBy", objBE_SalesAwardTypeManagement.CreatedBy);
                objSqlParam[6] = new SqlParameter("@ModifiedBy", objBE_SalesAwardTypeManagement.ModifiedBy);
                objSqlParam[7] = new SqlParameter("@IsActive", objBE_SalesAwardTypeManagement.IsActive);
                objSqlParam[8] = new SqlParameter("@IsDuplicate", SqlDbType.Int, 10, ParameterDirection.Output, false, 10, 10, "", DataRowVersion.Default, 0);
                objDALCommon = new DAL_Common();
                _stat = objDALCommon.DAL_SaveData("usp_Mas_SalesAward_Write", objSqlParam, 'S');
                _status = _stat == null ? 0 : Convert.ToInt32(_stat);

                _ID = (int)objSqlParam[8].Value;
                return _ID;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BE_SalesAwardType objBE_SalesAwardTypeColl { get; set; }
    }
}

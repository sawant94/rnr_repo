﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM.DAL;
using BE_Rewards.BE_Admin;
using System.Data.SqlClient;
using System.Data;

namespace BL_Rewards.Admin
{
  public class BL_UserExcelUpload
  {
        #region Variable Declaration
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();
      #endregion

        #region Get the User details by the Token Number.
        public BE_UserExcelUploadColl GetDetailsByTokenNumber(string tokennumber)
        {
            try
            {
                ds = new DataSet();                                            
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0]=new SqlParameter("@TokenNumber",tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_ExcelUpload_EmployeeDetails", objsqlparam);               
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();  
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBEUserExcelUpload.DivisionName = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.LocationName = dr["LocationName_PSA"].ToString();
                    objBEUserExcelUpload.OrgUnitName = dr["OrgUnitName"].ToString();
                    objBEUserExcelUpload.EmailID = dr["EmailID"].ToString();
                    objBEUserExcelUpload.FirstName = dr["FirstName"].ToString();
                    objBEUserExcelUpload.LastName = dr["LastName"].ToString();
                    objBEUserExcelUpload.EmpGradeCode_E = dr["EmpGradeCode_E"].ToString();
                    objBEUserExcelUpload.EmpGradeName_E = dr["EmpGradeName_E"].ToString();
                    objBEUserExcelUpload.Division_PA = dr["Division_PA"].ToString();
                    objBEUserExcelUpload.DivisionName_PA = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.EmployeeLevelCode_ES = dr["EmployeeLevelCode_ES"].ToString();
                    objBEUserExcelUpload.EmployeeLevelName_ES = dr["EmployeeLevelName_ES"].ToString();
                    objBEUserExcelUpload.Levels = dr["Levels"].ToString();
                    objBEUserExcelUpload.Location_PSA = dr["Location_PSA"].ToString();
                    objBEUserExcelUpload.LocationName_PSA = dr["LocationName_PSA"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Getthe Details after Successful Import
        public BE_UserExcelUploadColl GetDetailsAfterUpload()
        {
            try
            {
                ds = new DataSet();
                
                BE_UserExcelUploadColl objBE_UserExcelUploadColl  = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("usp_Get_LatestExcelUpload_EmployeeDetails");              
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBEUserExcelUpload.DivisionName = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.LocationName = dr["LocationName_PSA"].ToString();
                    objBEUserExcelUpload.OrgUnitName = dr["OrgUnitName"].ToString();
                    objBEUserExcelUpload.EmailID = dr["EmailID"].ToString();
                    objBEUserExcelUpload.FirstName = dr["FirstName"].ToString();
                    objBEUserExcelUpload.LastName = dr["LastName"].ToString();
                    objBEUserExcelUpload.EmpGradeCode_E = dr["EmpGradeCode_E"].ToString();
                    objBEUserExcelUpload.EmpGradeName_E = dr["EmpGradeName_E"].ToString();
                    objBEUserExcelUpload.Division_PA = dr["Division_PA"].ToString();
                    objBEUserExcelUpload.DivisionName_PA = dr["DivisionName_PA"].ToString();
                    objBEUserExcelUpload.EmployeeLevelCode_ES = dr["EmployeeLevelCode_ES"].ToString();
                    objBEUserExcelUpload.EmployeeLevelName_ES = dr["EmployeeLevelName_ES"].ToString();
                    objBEUserExcelUpload.Levels = dr["Levels"].ToString();
                    objBEUserExcelUpload.Location_PSA = dr["Location_PSA"].ToString();
                    objBEUserExcelUpload.LocationName_PSA = dr["LocationName_PSA"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);   
                    
                }
               
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region SaveExcel for Importing the Data in Table
        public string SaveExcel(SqlParameter objSqlParam , int InsertOrUpdate)
        {

            try
            {
                object _stat;                
                string var1="" , var2="";
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[4];
                objSqlParameter[0] = objSqlParam;             
                objSqlParameter[1] = new SqlParameter("@InserRowCount",DbType.Int32);
                objSqlParameter[1].Direction = ParameterDirection.Output;
                objSqlParameter[2] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[2].Direction = ParameterDirection.Output;
                objSqlParameter[3] = new SqlParameter("@InsertOrUpdateFlag", InsertOrUpdate);
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel", objSqlParameter, 'N');
                var1 = objSqlParameter[1].SqlValue.ToString();
                var2 = objSqlParameter[2].SqlValue.ToString();
                //_stat2 = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel", objSqlParameter, 'N');
                //var2 = objSqlParameter[1].SqlValue.ToString();
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);
               // rowAffect2 = _stat2 == null ? 0 : Convert.ToInt32(_stat2);

                if (objSqlParameter[1].SqlValue.ToString() == "" || objSqlParameter[1].SqlValue == null)
                    objSqlParameter[1].Value = 0;

                 if (objSqlParameter[2].SqlValue.ToString() == "" || objSqlParameter[2].SqlValue==  null)
                    objSqlParameter[2].Value = 0;
               
                if (rowAffect >= 0)
                {
                    if (InsertOrUpdate == 1)
                       Message = "Total Number of Updated Records are " + objSqlParameter[2].SqlValue.ToString();
                    else
                       Message = "Total Number of Inserted Records are " + objSqlParameter[1].SqlValue.ToString();
                }
                else
                {
                    Message = "Failure in Process Check the Excel Sheet and Try Again";
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Update the Record
        public string UpdateRecord(string TokenNumberUpdate, string TokenNumber, string FirstName, string LastName, string EmpGradeCode_E, string EmpGradeName_E,
                                   string EmployeeLevelCode_ES, string EmployeeLevelName_ES, string Levels, string Division_PA, string DivisionName_PA, string LocationName_PSA, string Location_PSA)
        {    
            try
            {
                object _stat;              
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[14];
                objSqlParameter[0] =new SqlParameter("@TokenNumberUpdate",TokenNumberUpdate);
                objSqlParameter[1] =new SqlParameter("@TokenNumber",TokenNumber);
                objSqlParameter[2] =new SqlParameter("@FirstName",FirstName);
                objSqlParameter[3] =new SqlParameter("@LastName",LastName);
                objSqlParameter[4] =new SqlParameter("@EmpGradeCode_E",EmpGradeCode_E);
                objSqlParameter[5] =new SqlParameter("@EmpGradeName_E",EmpGradeName_E);
                objSqlParameter[6] =new SqlParameter("@EmployeeLevelCode_ES",EmployeeLevelCode_ES);
                objSqlParameter[7] =new SqlParameter("@EmployeeLevelName_ES",EmployeeLevelName_ES);
                objSqlParameter[8] =new SqlParameter("@Levels",Levels);
                objSqlParameter[9] =new SqlParameter("@Division_PA",Division_PA);
                objSqlParameter[10] =new SqlParameter("@DivisionName_PA",DivisionName_PA);
                objSqlParameter[11] =new SqlParameter("@LocationName_PSA",LocationName_PSA);
                objSqlParameter[12] =new SqlParameter("@Location_PSA",Location_PSA);
                objSqlParameter[13] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[13].Direction = ParameterDirection.Output;
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel_Update", objSqlParameter, 'N');
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);

                if (objSqlParameter[13].SqlValue.ToString() == "" || objSqlParameter[13].SqlValue.ToString() == null)
                    objSqlParameter[13].Value = 0;

                if (rowAffect >= 0)
                {
                    Message = objSqlParameter[13].SqlValue.ToString();
                }
                else
                {
                    Message = objSqlParameter[13].SqlValue.ToString();
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Getting Data for Employee Group  DropDown
        public BE_UserExcelUploadColl GetEmployeeGroup()
        {
            try
            {
                ds = new DataSet();                
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("USP_Get_EmployeeGroup");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();                   
                    objBE_UserExcelUpload.EmpGradeCode_E = _dr["EmpGradeCode_E"].ToString();
                    objBE_UserExcelUpload.EmpGradeName_E = _dr["EmpGradeName_E"].ToString();
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

      #endregion

        #region Getting Data for Employee SUB Group  DropDown
        public BE_UserExcelUploadColl GetEmployeeSubGroup()
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("USP_Get_EmployeeSubGroup");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();
                    objBE_UserExcelUpload.EmployeeLevelCode_ES = _dr["Code"].ToString();
                    objBE_UserExcelUpload.EmployeeLevelName_ES = _dr["Desig"].ToString();
                    objBE_UserExcelUpload.Levels = _dr["Level"].ToString();
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        #endregion

        #region Getting Data for Division PA Name  DropDown
        public BE_UserExcelUploadColl GetDivisionPA()
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                //SqlParameter[] objsqlparam = new SqlParameter[1];
                //objsqlparam[0] = new SqlParameter("@TokenNumber", tokennumber);
                ds = objDALCommon.DAL_GetData("USP_Get_Division_PA_Details");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();
                    objBE_UserExcelUpload.Division_PA = _dr["Division_PA"].ToString();
                    objBE_UserExcelUpload.DivisionName_PA = _dr["DivisionName_PA"].ToString();                    
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        #endregion

        #region Getting Data for Location PA Name  DropDown
        public BE_UserExcelUploadColl GetLocationPSA()
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUpload objBE_UserExcelUpload;
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                //SqlParameter[] objsqlparam = new SqlParameter[1];
                //objsqlparam[0] = new SqlParameter("@TokenNumber", tokennumber);
                ds = objDALCommon.DAL_GetData("USP_Get_Location_PSA_Details");

                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    objBE_UserExcelUpload = new BE_UserExcelUpload();
                    objBE_UserExcelUpload.Location_PSA = _dr["Location_PSA"].ToString();
                    objBE_UserExcelUpload.LocationName_PSA = _dr["LocationName_PSA"].ToString();
                    objBE_UserExcelUploadColl.Add(objBE_UserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        #endregion

        #region Get All the records from Mas User table
        public DataTable GetMasUserTable()
        {
            try
            {
                ds = new DataSet();               
                ds = objDALCommon.DAL_GetData("USP_Get_MasUserTable");
                
                    DataTable dtMasUser = new DataTable();
                    dtMasUser.Columns.Add("TokenNumber", typeof(string));
                    foreach (DataRow _dr in ds.Tables[0].Rows)
                    {
                        DataRow _dr2 = dtMasUser.NewRow();
                        _dr2["TokenNumber"] = ds.Tables[0].Rows[0].ToString();
                        dtMasUser.Rows.Add(_dr2);

                    }
                    dtMasUser = ds.Tables[0];

                return dtMasUser;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        } 
      #endregion

      // Budget Code Area.......

        #region SaveExcel for Importing the Data in Table BUDGET Upload
        public string SaveBudgetExcel(SqlParameter objSqlParam, int InsertOrUpdate)
        {

            try
            {
                object _stat;
                string var1 = "", var2 = "";
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[4];
                objSqlParameter[0] = objSqlParam;
                objSqlParameter[1] = new SqlParameter("@InserRowCount", DbType.Int32);
                objSqlParameter[1].Direction = ParameterDirection.Output;
                objSqlParameter[2] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[2].Direction = ParameterDirection.Output;
                objSqlParameter[3] = new SqlParameter("@InsertOrUpdateFlag", InsertOrUpdate);
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Excel_Budget", objSqlParameter, 'N');
                var1 = objSqlParameter[1].SqlValue.ToString();
                var2 = objSqlParameter[2].SqlValue.ToString();
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);           

                if (objSqlParameter[1].SqlValue.ToString() == "" || objSqlParameter[1].SqlValue == null)
                    objSqlParameter[1].Value = 0;

                if (objSqlParameter[2].SqlValue.ToString() == "" || objSqlParameter[2].SqlValue == null)
                    objSqlParameter[2].Value = 0;

                if (rowAffect >= 0)
                {
                    if (InsertOrUpdate == 1)
                        Message = "Total Number of Updated Records are " + objSqlParameter[2].SqlValue.ToString();
                    else
                        Message = "Total Number of Inserted Records are " + objSqlParameter[1].SqlValue.ToString();
                }
                else
                {
                    Message = "Failure in Process Check the Excel Sheet and Try Again";
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Get All the records from Mas User Budget Table
        public DataTable GetMasBudgetTable()
        {
            try
            {
                ds = new DataSet();
                ds = objDALCommon.DAL_GetData("USP_Get_MasBudgetTable");

                DataTable dtMasUser = new DataTable();
                dtMasUser.Columns.Add("TokenNumber", typeof(string));
                foreach (DataRow _dr in ds.Tables[0].Rows)
                {
                    DataRow _dr2 = dtMasUser.NewRow();
                    _dr2["TokenNumber"] = ds.Tables[0].Rows[0].ToString();
                    dtMasUser.Rows.Add(_dr2);

                }
                dtMasUser = ds.Tables[0];

                return dtMasUser;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region Get the Details after Successful Import of Budget Information
        public BE_UserExcelUploadColl GetBudgetDetailsAfterUpload()
        {
            try
            {
                ds = new DataSet();

                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                ds = objDALCommon.DAL_GetData("usp_Get_LatestExcelUpload_Budget_EmployeeDetails");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.StartDate = dr["StartDate"].ToString();
                    objBEUserExcelUpload.EndDate = dr["EndDate"].ToString();
                    objBEUserExcelUpload.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);

                }

                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Get the Budget details by the Token Number.
        public BE_UserExcelUploadColl GetBudgetDetailsByTokenNumber(string tokennumber)
        {
            try
            {
                ds = new DataSet();
                BE_UserExcelUploadColl objBE_UserExcelUploadColl = new BE_UserExcelUploadColl();
                SqlParameter[] objsqlparam = new SqlParameter[1];
                objsqlparam[0] = new SqlParameter("@TokenNumber", tokennumber);
                ds = objDALCommon.DAL_GetData("usp_Get_ExcelUpload_EmployeeDetails_Budget", objsqlparam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BE_UserExcelUpload objBEUserExcelUpload = new BE_UserExcelUpload();
                    objBEUserExcelUpload.TokenNumber = dr["TokenNumber"].ToString();
                    objBEUserExcelUpload.StartDate = dr["StartDate"].ToString();
                    objBEUserExcelUpload.EndDate = dr["EndDate"].ToString();
                    objBEUserExcelUpload.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBEUserExcelUpload.EmployeeName = dr["EmployeeName"].ToString();
                    objBE_UserExcelUploadColl.Add(objBEUserExcelUpload);
                }
                return objBE_UserExcelUploadColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion


        #region insert budget upload from Excel 
        public string InsertBudgetFromPortal(string Tokenno, DateTime StartDate, DateTime EndDate, decimal Budget)
        {
            try
            {
                object _stat;
               // int rowAffect = 0;
              //  string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[4];
              //  objSqlParameter[0] = new SqlParameter("@TokenNumberUpdate", TokenNumber);
                objSqlParameter[0] = new SqlParameter("@TokenNumber", Tokenno);
                objSqlParameter[1] = new SqlParameter("@StartDate", StartDate);
                objSqlParameter[2] = new SqlParameter("@EndDate", EndDate);
                objSqlParameter[3] = new SqlParameter("@Budget", Budget);
              //  objSqlParameter[5] = new SqlParameter("@UpdateRowCount", DbType.Int32);
               // objSqlParameter[5].Direction = ParameterDirection.Output;
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Budget_Excel_Insert", objSqlParameter, 'N');
             //   rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);

                //if (objSqlParameter[5].SqlValue.ToString() == "" || objSqlParameter[5].SqlValue.ToString() == null)
                //    objSqlParameter[5].Value = 0;

                //if (rowAffect >= 0)
                //{
                //    Message = objSqlParameter[5].SqlValue.ToString();
                //}
                //else
                //{
                //    Message = objSqlParameter[5].SqlValue.ToString();
                //}

                return Convert.ToString(_stat);


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        } 
        #endregion

        #region Update the Budget Record
        public string UpdateBudgetRecord(string TokenNumberUpdate, string TokenNumber, string StartDate, string EndDate, decimal Budget)
        {
            //try
            //{


            //   object _stat;
            //   // int rowAffect = 0;
            //  //  string Message = "";
            //    SqlParameter[] objSqlParameter = new SqlParameter[4];
            //  //  objSqlParameter[0] = new SqlParameter("@TokenNumberUpdate", TokenNumber);
            //    objSqlParameter[0] = new SqlParameter("@TokenNumber", TokenNumber);
            //    objSqlParameter[1] = new SqlParameter("@StartDate", StartDate);
            //    objSqlParameter[2] = new SqlParameter("@EndDate", EndDate);
            //    objSqlParameter[3] = new SqlParameter("@Budget", Budget);
            //  //  objSqlParameter[5] = new SqlParameter("@UpdateRowCount", DbType.Int32);
            //   // objSqlParameter[5].Direction = ParameterDirection.Output;
            //    _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Budget_Excel_Insert", objSqlParameter, 'N');
            // //   rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);

            //    //if (objSqlParameter[5].SqlValue.ToString() == "" || objSqlParameter[5].SqlValue.ToString() == null)
            //    //    objSqlParameter[5].Value = 0;

            //    //if (rowAffect >= 0)
            //    //{
            //    //    Message = objSqlParameter[5].SqlValue.ToString();
            //    //}
            //    //else
            //    //{
            //    //    Message = objSqlParameter[5].SqlValue.ToString();
            //    //}

            //    return Convert.ToString(_stat);
            //}
            try
            {
                object _stat;
                int rowAffect = 0;
                string Message = "";
                SqlParameter[] objSqlParameter = new SqlParameter[6];
                objSqlParameter[0] = new SqlParameter("@TokenNumberUpdate", TokenNumberUpdate);
                objSqlParameter[1] = new SqlParameter("@TokenNumber", TokenNumber);
                objSqlParameter[2] = new SqlParameter("@StartDate", StartDate);
                objSqlParameter[3] = new SqlParameter("@EndDate", EndDate);
                objSqlParameter[4] = new SqlParameter("@Budget", Budget);
                objSqlParameter[5] = new SqlParameter("@UpdateRowCount", DbType.Int32);
                objSqlParameter[5].Direction = ParameterDirection.Output;
                _stat = objDALCommon.DAL_SaveData("usp_Mas_User_Upload_Budget_Excel_Update", objSqlParameter, 'N');
                rowAffect = _stat == null ? 0 : Convert.ToInt32(_stat);

                if (objSqlParameter[5].SqlValue.ToString() == "" || objSqlParameter[5].SqlValue.ToString() == null)
                    objSqlParameter[5].Value = 0;

                if (rowAffect >= 0)
                {
                    Message = objSqlParameter[5].SqlValue.ToString();
                }
                else
                {
                    Message = objSqlParameter[5].SqlValue.ToString();
                }

                return Message;


            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE_Rewards.BE_Admin;
using MM.DAL;
using System.Data;
using System.Data.SqlClient;

namespace BL_Rewards.Admin
{
    public class BL_QuarterAwardApprover
    {
        //BE_QuarterAwardApprover
        DAL_Common objDALCommon = new DAL_Common();
        public DataSet GetAFLCMember()
        {
            DataSet _ds = new DataSet();
            _ds = objDALCommon.DAL_GetData("spGetNominees");
            return _ds;
        }
        public int SaveData(BE_QuarterAwardApprover objBE_QuarterAwardApprover)
        {
            try
            {
                int Id = 0;
                SqlParameter[] objSqlParam = new SqlParameter[10];
                objSqlParam[0] = new SqlParameter("@SubAFLCTokenNumber", objBE_QuarterAwardApprover.SubAFLCTokenNumber);
                objSqlParam[1] = new SqlParameter("@SubAFLCName", objBE_QuarterAwardApprover.SubAFLCName);
                objSqlParam[2] = new SqlParameter("@AFLCTokenNumber", objBE_QuarterAwardApprover.AFLCTokenNumber);
                objSqlParam[3] = new SqlParameter("@AFLCName", objBE_QuarterAwardApprover.AFLCName);
                objSqlParam[4] = new SqlParameter("@Role", objBE_QuarterAwardApprover.Role);
                objSqlParam[5] = new SqlParameter("@CreatedBy", objBE_QuarterAwardApprover.CreatedBy);

                Id = (int)objDALCommon.DAL_SaveData("usp_InsertAFLCDetails", objSqlParam, 'N');

                return Id;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

    }
}

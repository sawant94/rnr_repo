﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;
using BE_Rewards.BE_Admin;

namespace BL_Rewards.Admin
{
    public sealed class BL_BudgetAllocation
    {
        DataSet ds;
        DAL_Common objDALCommon = new DAL_Common();

        public BE_BudgetAllocationColl GetData(BE_BudgetAllocation objBE_BudgetAllocation)
        {
            try
             {
                ds = new DataSet();

                //BE_BudgetAllocation objBE_BudgetAllocation;
                BE_BudgetAllocationColl objBE_BudgetAllocationColl = new BE_BudgetAllocationColl();
                bool IsCEO = false;
                IsCEO = objBE_BudgetAllocation.IsCEO;
                if (objBE_BudgetAllocation.CheckButtonClick == 1)
                {
                    SqlParameter[] objSqlParam = new SqlParameter[2];
                    objSqlParam[0] = new SqlParameter("@IsCEO", objBE_BudgetAllocation.IsCEO);
                    objSqlParam[1] = new SqlParameter("@SearchBy", objBE_BudgetAllocation.SearchBy);
                    ds = objDALCommon.DAL_GetData("usp_Get_CEOs_ForBudget", objSqlParam);
                    //if (ds.Tables[0].Rows[0]["Balance"].ToString() == "0")
                    //{
                    //    Console.WriteLine("<script>alert('Balance 0 ')</script>");
                    //    //return 0;
                    //}

                }
                else if (objBE_BudgetAllocation.CheckButtonClick == 2) 
                {
                    SqlParameter[] objSqlParam = new SqlParameter[2];
                    objSqlParam[0] = new SqlParameter("@CEOTokenNumber", objBE_BudgetAllocation.TokenNumber);
                    objSqlParam[1] = new SqlParameter("@SearchBy", objBE_BudgetAllocation.SearchBy);
                    ds = objDALCommon.DAL_GetData("usp_Get_CEO_User", objSqlParam);

                }
                
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_BudgetAllocation = new BE_BudgetAllocation();

                    objBE_BudgetAllocation.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_BudgetAllocation.Name = dr["Name"].ToString();
                    objBE_BudgetAllocation.Division = dr["Division"].ToString();
                    objBE_BudgetAllocation.Location = dr["Location"].ToString();
                    objBE_BudgetAllocation.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBE_BudgetAllocation.Balance = dr["Balance"].ToString()=="" ? (Decimal?)null : Convert.ToDecimal(dr["Balance"]);
                    objBE_BudgetAllocation.IsCEO = objBE_BudgetAllocation.IsCEO;
                    //if (!IsCEO)
                    //{
                        objBE_BudgetAllocation.EMailID = dr["EmailID"].ToString();
                    //}
                    //objBE_BudgetAllocation.CreatedBy = dr["CreatedBy"].ToString();
                    //if (dr["CreatedDate"].ToString() != "")
                    //    objBE_BudgetAllocation.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    //objBE_BudgetAllocation.ModifiedBy = dr["ModifiedBy"].ToString();
                    //if (dr["ModifiedDate"].ToString() != "")
                    //    objBE_BudgetAllocation.ModifiedDate = (Convert.ToString(dr["ModifiedDate"]) == "") ? (DateTime?)null : Convert.ToDateTime(dr["ModifiedDate"]);
                                        

                    objBE_BudgetAllocationColl.Add(objBE_BudgetAllocation);
                }
                return objBE_BudgetAllocationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        //megha

      

        public BE_BudgetAllocationColl GetAllUsers(string SearchBy)
        {
            try
            {
                ds = new DataSet();

                BE_BudgetAllocation objBE_BudgetAllocation;
                BE_BudgetAllocationColl objBE_BudgetAllocationColl = new BE_BudgetAllocationColl();

               
                    SqlParameter[] objSqlParam = new SqlParameter[2];
                    objSqlParam[1] = new SqlParameter("@SearchBy", SearchBy);
                    ds = objDALCommon.DAL_GetData("usp_Get_Mas_User", objSqlParam);

              
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_BudgetAllocation = new BE_BudgetAllocation();

                    objBE_BudgetAllocation.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_BudgetAllocation.Name = dr["Name"].ToString();
                    objBE_BudgetAllocation.Division = dr["Division"].ToString();
                    objBE_BudgetAllocation.Location = dr["Location"].ToString();
                    objBE_BudgetAllocation.Levels = dr["Levels"].ToString() == "" ? (int?)null : Convert.ToInt32(dr["Levels"]);
                    objBE_BudgetAllocation.Designation = dr["Designation"].ToString();
                    objBE_BudgetAllocationColl.Add(objBE_BudgetAllocation);
                }

                return objBE_BudgetAllocationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }


        }

        public int SaveData(BE_BudgetAllocationColl objBE_BudgetAllocationColl)
        {
            int Id = 0;
            try
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("TokenNumber", typeof(string));
                dt.Columns.Add("StartDate",typeof(DateTime));
                dt.Columns.Add("EndDate", typeof(DateTime));
                dt.Columns.Add("Budget",typeof(decimal));
                dt.Columns.Add("AllocationTpe", typeof(string));
                dt.Columns.Add("AllocationFrom", typeof(string));
                dt.Columns.Add("CreatedBy", typeof(string));
                dt.Columns.Add("CreatedDate", typeof(DateTime));
                dt.Columns.Add("ModifiedBy", typeof(string));
                dt.Columns.Add("ModifiedDate", typeof(DateTime));

                foreach (BE_BudgetAllocation objBE_BudgetAllocation in objBE_BudgetAllocationColl)
                {
                    DataRow dr = dt.NewRow();
                    dr["TokenNumber"] = objBE_BudgetAllocation.TokenNumber;
                    dr["StartDate"] = objBE_BudgetAllocation.StartDate;
                    dr["EndDate"] = objBE_BudgetAllocation.EndDate;
                    dr["Budget"] = objBE_BudgetAllocation.Budget;
                    //dr["AllocationTpe"] = "C";
                    dr["AllocationTpe"] = objBE_BudgetAllocation.AllocationType;
                    dr["AllocationFrom"] = objBE_BudgetAllocation.AllocationFrom;
                    dr["CreatedBy"] = objBE_BudgetAllocation.CreatedBy;
                    dr["CreatedDate"] = objBE_BudgetAllocation.CreatedDate;
                    dr["ModifiedBy"] = DBNull.Value;
                    dr["ModifiedDate"] = DBNull.Value;
                   
                   // dr["EligibilityLevel"] = i;
                    dt.Rows.Add(dr);
                }


                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@Table_Mas_UserBudget", dt);
                objDALCommon.DAL_SaveData("usp_Insert_Mas_UserBudget", objSqlParam, 'N');

                //foreach (int i in objBE_BudgetAllocation.Levels)
                //{
                //    SqlParameter[] objSqlParamLevels = new SqlParameter[2];
                //    objSqlParamLevels[0] = new SqlParameter("@AwardID",objBE_BudgetAllocation.Id != 0?objBE_BudgetAllocation.Id: Id);
                //    objSqlParamLevels[1] = new SqlParameter("@Level", i);
                //    objDALCommon = new DAL_Common();
                //    objDALCommon.DAL_SaveData("usp_Insert_Award_Levels_Mapping", objSqlParamLevels, 'N');
                //}
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return Id;
        }

        public int SaveTransferData(BE_BudgetAllocationColl objBE_BudgetAllocationColl)
        {
            int Id = 0;
            try
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("TokenNumber", typeof(string));
                dt.Columns.Add("StartDate", typeof(DateTime));
               dt.Columns.Add("EndDate", typeof(DateTime));
                dt.Columns.Add("Budget", typeof(decimal));
                dt.Columns.Add("AllocationTpe", typeof(string));
                dt.Columns.Add("AllocationFrom", typeof(string));
                dt.Columns.Add("CreatedBy", typeof(string));
                dt.Columns.Add("CreatedDate", typeof(DateTime));
                dt.Columns.Add("ModifiedBy", typeof(string));
                dt.Columns.Add("ModifiedDate", typeof(DateTime));

                foreach (BE_BudgetAllocation objBE_BudgetAllocation in objBE_BudgetAllocationColl)
                {
                    DataRow dr = dt.NewRow();
                    dr["TokenNumber"] = objBE_BudgetAllocation.TokenNumber;
                    dr["StartDate"] = objBE_BudgetAllocation.StartDate;
                    dr["EndDate"] = objBE_BudgetAllocation.EndDate;
                    dr["Budget"] = objBE_BudgetAllocation.Budget;
                  // dr["AllocationTpe"] = "C";
                   dr["AllocationTpe"] = objBE_BudgetAllocation.AllocationType;
                    dr["AllocationFrom"] = objBE_BudgetAllocation.AllocationFrom;
                    dr["CreatedBy"] = objBE_BudgetAllocation.CreatedBy;
                    dr["CreatedDate"] = objBE_BudgetAllocation.CreatedDate;
                    dr["ModifiedBy"] = DBNull.Value;
                    dr["ModifiedDate"] = DBNull.Value;

                    // dr["EligibilityLevel"] = i;
                    dt.Rows.Add(dr);
                }

                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@Table_Mas_UserBudget", dt);
                objDALCommon.DAL_SaveData("usp_Insert_Mas_UserBudget", objSqlParam, 'N');
             
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return Id;
        }

        public void SaveData_UserLevels(BE_BudgetAllocation objBE_BudgetAllocation)
        {
            try
            {
                SqlParameter[] objSqlParam = new SqlParameter[2];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_BudgetAllocation.TokenNumber );
                objSqlParam[1] = new SqlParameter("@Levels", objBE_BudgetAllocation.Levels);
                objDALCommon.DAL_SaveData("usp_Update_Mas_User_Levels", objSqlParam, 'N');
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public int getbalance_amt(BE_BudgetAllocation objBE_BudgetAllocation1)
        {
             ds = new DataSet();

            
               
                    SqlParameter[] objSqlParam = new SqlParameter[1];
                    objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_BudgetAllocation1.TokenNumber);

                    ds = objDALCommon.DAL_GetData("usp_Get_Budget", objSqlParam);
                    if (ds.Tables[0].Rows[0].ToString() == "0")
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
           
        }

        public BE_BudgetAllocationColl GetDataForManager(BE_BudgetAllocation objBE_BudgetAllocation)
        {
            try
            {
                ds = new DataSet();
                BE_BudgetAllocationColl objBE_BudgetAllocationColl = new BE_BudgetAllocationColl();
                SqlParameter[] objSqlParam = new SqlParameter[1];
                objSqlParam[0] = new SqlParameter("@TokenNumber", objBE_BudgetAllocation.IsCEO);
                // objSqlParam[1] = new SqlParameter("@SearchBy", objBE_BudgetAllocation.SearchBy);
                ds = objDALCommon.DAL_GetData("usp_Get_Manager_ForBudget", objSqlParam);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBE_BudgetAllocation = new BE_BudgetAllocation();

                    objBE_BudgetAllocation.TokenNumber = dr["TokenNumber"].ToString();
                    objBE_BudgetAllocation.Name = dr["Name"].ToString();
                    objBE_BudgetAllocation.Division = dr["Division"].ToString();
                    objBE_BudgetAllocation.Location = dr["Location"].ToString();
                    objBE_BudgetAllocation.Budget = Convert.ToDecimal(dr["Budget"]);
                    objBE_BudgetAllocation.Balance = dr["Balance"].ToString() == "" ? (Decimal?)null : Convert.ToDecimal(dr["Balance"]);
                    //objBE_BudgetAllocation.IsCEO = objBE_BudgetAllocation.IsCEO;
                    //if (!IsCEO)
                    //{
                    objBE_BudgetAllocation.EMailID = dr["EmailID"].ToString();
                    //}
                    //objBE_BudgetAllocation.CreatedBy = dr["CreatedBy"].ToString();
                    //if (dr["CreatedDate"].ToString() != "")
                    //    objBE_BudgetAllocation.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    //objBE_BudgetAllocation.ModifiedBy = dr["ModifiedBy"].ToString();
                    //if (dr["ModifiedDate"].ToString() != "")
                    //    objBE_BudgetAllocation.ModifiedDate = (Convert.ToString(dr["ModifiedDate"]) == "") ? (DateTime?)null : Convert.ToDateTime(dr["ModifiedDate"]);


                    objBE_BudgetAllocationColl.Add(objBE_BudgetAllocation);
                }
                return objBE_BudgetAllocationColl;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}

﻿using System;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;
using System.Configuration;
using Liberary_MailSender;
using Liberary_MailSender.Classes;
using Liberary_MailSender.Entities;
using System.Diagnostics;
namespace MailSender.Classes
{
    //-----------------------------------------------------------------------------------------------------
    [DebuggerDisplay("TemplateFileName={TemplateFileName}, MileStone_AlertNo={MileStone_AlertNo}")]
    public class Alert_MileStone_T15 : MailBase
    {
        protected override string TemplatePath { get; set; }
        protected override string TemplateFileName { get; set; }
        public override MailDetails_Coll RecipientList { get; set; }
        public static string MileStone_AlertNo { get; set; }
        //-----------------------------------------------------------------------------------------------------
        public override void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "")
        {
            LogData("------------------Alert_MileStone_T15 Awards block start----------------");
            TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? ConfigurationSettings.AppSettings["AwardTypeTemplatePath"].ToString() : strTemplatePath;
            RecipientList = GetRecipientList();
            LogData("Count of Milestone Recipient:" + RecipientList.Count);
            TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
            Console.WriteLine("Sending ... ");
            MailDetails_Coll objMailDetails_Coll = SendMails();
            Console.WriteLine("Sending complete");
            if (MileStone_AlertNo == "1")
            {
                try
                {
                    LogData("------------------Alert_MileStone_T15 Awards Try block start----------------");
                    LogData("---------------------");
                    LogData("MileStone Awards (Before " + Convert.ToInt32(ConfigurationSettings.AppSettings["MileStoneAlert_First"]) + " days)");
                    if (objMailDetails_Coll != null)
                    {
                        int i = 0;
                        foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                        {
                            i++;
                            string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                            LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + "]");
                        }
                        LogData(" Number of Mails sent successfully is " + objMailDetails_Coll.SendSuccessfull.Count);
                        LogData(" Number of Mails sending failed is " + objMailDetails_Coll.SendFailed.Count);
                        Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
                        Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);
                        DataTable dtMailSent = new DataTable();
                        DataRow drMail;
                        DataColumn dc = new DataColumn();
                        dtMailSent.Columns.Add("RecipientTokenNumber", typeof(string));
                        dtMailSent.Columns.Add("HODTokenNumber", typeof(string));
                        dtMailSent.Columns.Add("Years", Type.GetType("System.String"));
                        dtMailSent.Columns.Add("DateOfMileStone", typeof(string));
                        foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                        {
                            //if (objMailDetails_Extras.Status == "1")
                            //{
                            drMail = dtMailSent.NewRow();
                            drMail["RecipientTokenNumber"] = Convert.ToString(objMailDetails_Extras.TokenNumber);
                            drMail["HODTokenNumber"] = Convert.ToString(objMailDetails_Extras.HODTokenNumber);
                            drMail["Years"] = Convert.ToInt32(objMailDetails_Extras.Years);
                            drMail["DateOfMileStone"] = objMailDetails_Extras.DateOfMileStone;
                            dtMailSent.Rows.Add(drMail);
                            // }
                        }
                        if (dtMailSent.Rows.Count > 0)
                        {
                            DAL_Common objCommon = new DAL_Common();
                            SqlParameter[] param = new SqlParameter[1];
                            param[0] = new SqlParameter("@MileStoneAwardDetails", dtMailSent);
                            objCommon.DAL_SaveData("usp_Insert_MileStone_Award_Details", param);
                        }
                    }
                    LogData("------------------Alert_MileStone_T15 Awards Try block End----------------");
                }
                catch (Exception ex)
                {
                    LogData("---------------------");
                    LogData("Exception for Alert_MileStone_T15 - ProcessAndSendMails 1");
                    LogData(" Exception : " + ex.Message + ex.StackTrace);// + " || Inner Exception : " + ex.InnerException + Environment.NewLine);
                }
            }
            else if (MileStone_AlertNo == "2")
            {
                try
                {
                    LogData("---------------------");
                    LogData("MileStone Awards (Before " + Convert.ToInt32(ConfigurationSettings.AppSettings["MileStoneAlert_Second"]) + " days)");
                    if (objMailDetails_Coll != null)
                    {
                        int i = 0;
                        foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                        {
                            i++;
                            string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                            LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + "]");
                        }
                        LogData(" Number of Mails sent successfully : " + objMailDetails_Coll.SendSuccessfull.Count);
                        LogData(" Number of Mails sending failed : " + objMailDetails_Coll.SendFailed.Count);
                        Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
                        Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);
                    }
                }
                catch (Exception ex)
                {
                    LogData("---------------------");
                    LogData("Exception for Alert_MileStone_T15 - ProcessAndSendMails 2");
                    LogData(" Exception : " + ex.Message);// + " || Inner Exception : " + ex.InnerException);
                }
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected MailDetails_Coll GetRecipientList()
        {
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            try
            {
                LogData("------------------Alert_MileStone_T15 GetRecipientList Awards Try block starts----------------");
                DataSet dsEmail = new DataSet();
                DAL_Common objCommon = new DAL_Common();
                if (MileStone_AlertNo == "1")
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@TType", Convert.ToInt32(ConfigurationSettings.AppSettings["MileStoneAlert_First"]));
                    dsEmail = objCommon.DAL_GetData("usp_GetMileStones", param);
                    foreach (DataRow dr in dsEmail.Tables[0].Rows)
                    {
                        objMailDetails = new MailDetails_Extras();
                        try
                        {
                            objMailDetails.Years = Convert.ToInt32(dr["Tenure"].ToString());
                            objMailDetails.DateOfMileStone = dr["MileStoneDate"].ToString();
                            objMailDetails.HODTokenNumber = dr["ReportingManagerNumber"].ToString();
                            objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                            objMailDetails.TokenNumber = dr["RecipientTokenNumber"].ToString();
                            objMailDetails.Subject = "Milestone completing in 20 days";// "MileStone completing in " + (ConfigurationSettings.AppSettings["MileStoneAlert_First"]) + " days";
                            objMailDetails.FromID = "r&r@mahindra.com";
                            objMailDetails.SenderName = "r&r@mahindra.com";
                            objMailDetails.ToID = dr["ReportingManagerEmailID"].ToString();
                            objMailDetails.RecipientName = dr["ReportingManager"].ToString();
                            //string strRNREmailID =
                            objMailDetails.CCID = dr["RNREmailID"].ToString() == "0" ? ConfigurationSettings.AppSettings["RNRDefaultID"].ToString() : dr["RNREmailID"].ToString();
                            TemplateFileName = "MileStone_Alert1.txt";
                            string BodyPart = GetBody();
                            string date = dr["MileStoneDate"].ToString().Substring(6, 2) + "/" + dr["MileStoneDate"].ToString().Substring(4, 2) + "/" + dr["MileStoneDate"].ToString().Substring(0, 4);
                            BodyPart = BodyPart.Replace("<%HODName%>", dr["ReportingManager"].ToString())
                            .Replace("<%RecipientName%>", dr["RecipientName"].ToString())
                            .Replace("<%Years%>", dr["Tenure"].ToString())
                            .Replace("<%AwardAmount%>", ConfigurationSettings.AppSettings["MileStone_AwardAmount"].ToString())
                            .Replace("<%AwardType%>", ConfigurationSettings.AppSettings["MileStone_AwardType_Name"].ToString())
                            .Replace("<%MileStoneDate%>", date)
                            .Replace("<%PersonalMsgLink%>", ConfigurationSettings.AppSettings["PersonalMsgLink"].ToString());
                            objMailDetails.Body = BodyPart;
                            objMailDetails_Coll.Add(objMailDetails);
                        }
                        catch (Exception ex)
                        {
                            objMailDetails.Status = ex.Message;
                            LogData("---------------------");
                            LogData("Exception for Alert_MileStone_T15 - GetRecipientList 1");
                            LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                        }
                    }
                }
                else if (MileStone_AlertNo == "2")
                {
                    try
                    {
                        SqlParameter[] param = new SqlParameter[1];
                        param[0] = new SqlParameter("@TType", Convert.ToInt32(ConfigurationSettings.AppSettings["MileStoneAlert_Second"]));
                        dsEmail = objCommon.DAL_GetData("usp_GetMileStoneAlert", param);
                        foreach (DataRow dr in dsEmail.Tables[0].Rows)
                        {
                            objMailDetails = new MailDetails_Extras();
                            objMailDetails.HODTokenNumber = dr["ReportingManagerNumber"].ToString();
                            objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                            objMailDetails.TokenNumber = dr["RecipientTokenNumber"].ToString();
                            objMailDetails.Subject = "MileStone completing in few days";
                            objMailDetails.FromID = "r&r@mahindra.com";
                            objMailDetails.SenderName = "r&r@mahindra.com";
                            objMailDetails.ToID = dr["ReportingManagerEmailID"].ToString();
                            objMailDetails.RecipientName = dr["ReportingManager"].ToString();
                            TemplateFileName = "MileStone_Alert2.txt";
                            string BodyPart = GetBody();
                            string date = dr["MileStoneDate"].ToString().Substring(6, 2) + "/" + dr["MileStoneDate"].ToString().Substring(4, 2) + "/" + dr["MileStoneDate"].ToString().Substring(0, 4);
                            BodyPart = BodyPart.Replace("<%HODName%>", dr["ReportingManager"].ToString())
                            .Replace("<%RecipientName%>", dr["RecipientName"].ToString())
                            .Replace("<%Years%>", dr["Tenure"].ToString())
                            .Replace("<%AwardAmount%>", ConfigurationSettings.AppSettings["MileStone_AwardAmount"].ToString())
                            .Replace("<%AwardType%>", ConfigurationSettings.AppSettings["MileStone_AwardType_Name"].ToString())
                            .Replace("<%MileStoneDate%>", date)
                            .Replace("<%PersonalMsgLink%>", ConfigurationSettings.AppSettings["PersonalMsgLink"].ToString());
                            objMailDetails.Body = BodyPart;
                            objMailDetails_Coll.Add(objMailDetails);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogData("---------------------");
                        LogData("Exception for Alert_MileStone_T15 - GetRecipientList 2");
                        LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
            return objMailDetails_Coll;
        }
    }
}


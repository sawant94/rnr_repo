﻿using System;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;
using System.Configuration;
using Liberary_MailSender;
using Liberary_MailSender.Classes;
using Liberary_MailSender.Entities;
using System.Diagnostics;
using System.Collections;
using EXE_MailSender.DTO;
using System.Collections.Generic;
using System.Linq;
using Exe_ErrorLog;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Text;
using System.Drawing;
namespace MailSender.Classes
{
    class HOD_Monthly_Report : MailBase
    {
        ErrorLog exe_errorlog = new ErrorLog();
        //-----------------------------------------------------------------------------------------------------
        public override void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "")
        {
            LogData("------------------HOD_Monthly_Report ProcessAndSendMails Awards Try block starts----------------");
            try
            {
                TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath") : strTemplatePath;
                RecipientList = GetRecipientList();
                TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
                Console.WriteLine("Sending ... ");
                MailDetails_Coll objMailDetails_Coll =  SendMails();
                Console.WriteLine("Sending complete");
                LogData("---------------------");
                LogData("HOD Monthly Report");
                int i = 0;
                foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                {
                    i++;
                    string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                    LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");
                }

                if (objMailDetails_Coll.SendSuccessfull.Count > 0)
                {
                    LogData(" ** Hod Mailers flag dates update     block starts----------------");
                    DAL_Common objCommon = new DAL_Common();                   
                    LogData("Update flag Date:");
                    try
                    {
                        LogData("** Hod Mailers flag dates update Try block starts----------------");
                        SqlParameter[] param = new SqlParameter[1];
                       // param[0] = new SqlParameter("@ID", strID);
                        objCommon.DAL_SaveData("UpdateNotificationTriggerDates", param);                    
                        LogData(" ** Hod Mailers flag dates update Try block Ends----------------");                        
                    }
                    catch (Exception exx)
                    {
                        exe_errorlog.ExceptionMethod("", "", exx);
                        LogData("---------------------");
                        LogData("Exception for Hod mailers - ProcessAndSendMails 3");
                        LogData(exx.Message + Environment.NewLine + exx.StackTrace);
                    }

                }
                LogData(" Number of Mails sent successfully : " + objMailDetails_Coll.SendSuccessfull.Count);
                LogData(" Number of Mails sending failed : " + objMailDetails_Coll.SendFailed.Count);
                Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
                Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);
                LogData("------------------HOD_Monthly_Report ProcessAndSendMails Awards Try block Ends----------------");
            }
            catch (Exception exx)
            {
                exe_errorlog.ExceptionMethod("", "", exx);
                LogData("---------------------");
                LogData("Exception for HOD_Monthly_Report - ProcessAndSendMails 2");
                LogData(exx.Message + Environment.NewLine + exx.StackTrace);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected MailDetails_Coll GetRecipientList()
        {
            LogData("------------------HOD_Monthly_Report GetRecipientList Awards Try block starts----------------");
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            DAL_Common objCommon = new DAL_Common();
            DataSet dsHODList = new DataSet();           
            dsHODList = objCommon.DAL_GetData("GetMangerandSubordinate");
            List<HOD> HODList = dsHODList.Tables[0].AsEnumerable()
            .Select(row => new HOD()
            {
                TokenNumber = Convert.ToString(row["TokenNumber"]),
                EmployeeName = Convert.ToString(row["EmployeeName"]),                
            }).ToList();
           
            try
            {
                string bodyPart = "";
                bodyPart = "<div style='width:800px;float:none;margin:0 auto;background-color:#eee; padding:15px;margin-top:30px;'><table style='width:100%;'><tr> <td style='text-align:left;font-size:16px;'> 	";
                string TableHeader1 = "	Nearly 90% of India Inc. employees say they will stick around longer if they feel appreciated for their work reveals a study by TimesJobs. The study, which surveyed more than 1,600 employees, further reveals that 82% employees are willing to work harder if their bosses appreciate them. Your unused budget since the last 6 months, can significantly improve the morale of your team ";
                string TableHeader2 = "	Did you know that companies which recognize employees regularly get 5.3 times more innovative ideas than the average (Study shown by OC Tanner Institute). You still have some significant balance left to reward your team.";
                string TableHeader3 = "	You can look means for recognition beyond your remaining budget balance. You can empower all employees to recognize and reward each other. By doing so, your people will feel acknowledged for who they are and what they do. They can do it formally through e-card in the portal and in public forums.";
                string TableHeader4 = "	We would like to thank you for recognizing your employees on timely basis. However, don’t stop here, go one level further & explore non-monetary ways of recognition such as e-cards in the portal/ appreciation cards and recognition in public forums.";


                TemplateFileName = "ImageFile.txt";
                bodyPart = GetBody();


                for (int i = 0; i < HODList.Count; i++)
                {
                    objMailDetails = new MailDetails_Extras();
                    objMailDetails.TokenNumber = HODList.ElementAt(i).TokenNumber;
                    objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                    objMailDetails.Subject = "Budget Utilization Status";
                    objMailDetails.FromID = "r&r@mahindra.com";
                    objMailDetails.SenderName = "r&r@mahindra.com";
                    objMailDetails.ToID = HODList.ElementAt(i).TokenNumber + "@MAHINDRA.COM";                   
                    objMailDetails.RecipientName = HODList.ElementAt(i).EmployeeName;

                    TemplateFileName = "ImageFile.txt";
                    bodyPart = GetBody();
             
                    DataSet dsBudgetDetails = new DataSet();                   
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@ManagerTokenNumber", SqlDbType.VarChar, 20);
                    param[0].Value = HODList.ElementAt(i).TokenNumber;
                    dsHODList = objCommon.DAL_GetData("Rpt_GetHODMonthlyMReport", param);
                    if (dsHODList.Tables[0].Rows.Count > 0)
                    {
                        List<BudgetUtilization> BudgetUtilizationDetails = dsHODList.Tables[0].AsEnumerable()
                        .Select(row => new BudgetUtilization()
                        {
                            HODName = Convert.ToString(row["HODName"]),
                            TokenNumber = Convert.ToString(row["TokenNumber"]),
                            EmployeeName = Convert.ToString(row["EmployeeName"]),
                            AwardsGivenInFinancialYear = Convert.ToInt32(row["AwardsGivenInFinancialYear"]),
                            BalanceBudget = Convert.ToInt32(row["BalanceBudget"]),
                            AwardAmountForTheMonth = Convert.ToInt32(row["AwardAmountForTheMonth"]),
                            Spot = Convert.ToInt32(row["SpotCash"]),
                            Excellerator = Convert.ToInt32(row["Excellerator"]),
                            Vouchers = Convert.ToInt32(row["Vouchers"]),                          
                            CoffeeWithYou = Convert.ToInt32(row["CoffeeWithYou"]),
                            Budget = Convert.ToInt32(row["Budget"]),
                            IsManager = Convert.ToBoolean(row["IsManager"]),
                            UtilizationFlag = Convert.ToInt32(row["flag"])
                        }).ToList();


                        for (int a = 0; a < BudgetUtilizationDetails.Count; a++)
                        {
                            if (BudgetUtilizationDetails.ElementAt(a).UtilizationFlag == 1)
                            {

                                bodyPart = bodyPart.Replace("#img", "R&R-Mailer_1.jpg");
                                bodyPart = bodyPart.Replace("#msg", TableHeader1);
                            }

                            else if (BudgetUtilizationDetails.ElementAt(a).UtilizationFlag == 2)
                            {

                                bodyPart = bodyPart.Replace("#img", "R&R-Mailer_2.jpg");
                                bodyPart = bodyPart.Replace("#msg", TableHeader2);
                            }

                            else if (BudgetUtilizationDetails.ElementAt(a).UtilizationFlag == 3)
                            {

                                bodyPart = bodyPart.Replace("#img", "R&R-Mailer_3.jpg");
                                bodyPart = bodyPart.Replace("#msg", TableHeader3);
                            }

                            else if (BudgetUtilizationDetails.ElementAt(a).UtilizationFlag == 4)
                            {
                                bodyPart = bodyPart.Replace("#img", "R&R-Mailer_4.jpg");
                                bodyPart = bodyPart.Replace("#msg", TableHeader4);
                            }
                           
                            bodyPart = bodyPart.Replace("#MYY", DateTime.Now.ToString("MMM") + ',' + DateTime.Now.Year.ToString().Substring(2, 2));
                            bodyPart = bodyPart.Replace("#MY", DateTime.Now.ToString("MMM") + ',' + DateTime.Now.Year.ToString());
                            bodyPart = bodyPart.Replace("#Sir", objMailDetails.RecipientName);
                            bodyPart = bodyPart.Replace("#0", BudgetUtilizationDetails.ElementAt(a).HODName);
                            bodyPart = bodyPart.Replace("#1", BudgetUtilizationDetails.ElementAt(a).Budget.ToString());
                            bodyPart = bodyPart.Replace("#2", BudgetUtilizationDetails.ElementAt(a).AwardsGivenInFinancialYear.ToString());
                            bodyPart = bodyPart.Replace("#3", BudgetUtilizationDetails.ElementAt(a).BalanceBudget.ToString());
                            bodyPart = bodyPart.Replace("#4", BudgetUtilizationDetails.ElementAt(a).Spot.ToString());
                            bodyPart = bodyPart.Replace("#5", BudgetUtilizationDetails.ElementAt(a).Excellerator.ToString());
                            bodyPart = bodyPart.Replace("#6", BudgetUtilizationDetails.ElementAt(a).Vouchers.ToString());
                            bodyPart = bodyPart.Replace("#8", BudgetUtilizationDetails.ElementAt(a).CoffeeWithYou.ToString());

                           // LogData("dsHODList - " + dsHODList.ToString());
                        }                       

                        objMailDetails.Body = bodyPart;
                        objMailDetails_Coll.Add(objMailDetails);
                       // LogData("objMailDetails_Coll = " + objMailDetails_Coll);
                    }
                }
                LogData("------------------HOD_Monthly_Report GetRecipientList Awards Try block Ends----------------");
            }
            catch (Exception ex)
            {                
                LogData(ex.Message);
                LogData(ex.StackTrace);
            }
            return objMailDetails_Coll;
        }
        string getAwardsCount(string input, string awardType)
        {
            string retval = "0";
            if (input.ToUpper().Contains(awardType.ToUpper()))
            {
                retval = input.Split(':')[1].ToString();
            }
            return retval;
        }
       
    }
}



﻿using System;
using System.Data;
using MM.DAL;
using System.Configuration;
using Liberary_MailSender;
using Liberary_MailSender.Classes;
using Liberary_MailSender.Entities;
using System.Net.Mail;
namespace MailSender.Classes
{
    //-----------------------------------------------------------------------------------------------------
    public class CoffeeWithBoss : MailBase
    {
        protected override string TemplatePath { get; set; }
        protected override string TemplateFileName { get; set; }
        public override MailDetails_Coll RecipientList { get; set; }
        //-----------------------------------------------------------------------------------------------------

        public override void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "")
        {
            try
            {
                LogData("------------------CoffeeWithBoss ProcessAndSendMails Awards Try block starts----------------");
                //TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? "" : strTemplatePath;
                TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? ConfigurationSettings.AppSettings["AwardTypeTemplatePath"].ToString() : strTemplatePath;
                RecipientList = GetRecipientList();
                TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
                Console.WriteLine("Sending ... ");
                MailDetails_Coll objMailDetails_Coll = SendMails();
                Console.WriteLine("Sending complete");
                LogData("---------------------");
                LogData("Coffee with Boss (7 and 14 Days Remainder)");
                int i = 0;
                foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                {
                    i++;
                    string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                    LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");
                }
                LogData(" Number of Mails sent successfully : " + objMailDetails_Coll.SendSuccessfull.Count);
                LogData(" Number of Mails sending failed : " + objMailDetails_Coll.SendFailed.Count);
                LogData("------------------CoffeeWithBoss ProcessAndSendMails Awards Try block Ends----------------");
                //Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
                //Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogData("-----------------------------------");
                LogData("Exception for ProcessAndSendMails method");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                //throw new Exception("", ex);
            }
        }
        //-----------------------------------------------------------------------------------------------------

        protected MailDetails_Coll GetRecipientList()
        {
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            DataSet dsEmail = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            try
            {
                LogData("------------------CoffeeWithBoss GetRecipientList Awards Try block starts----------------");
                dsEmail = objCommon.DAL_GetData("usp_CoffeeRemainder");
                foreach (DataRow dr in dsEmail.Tables[0].Rows)
                {
                    try
                    {
                        objMailDetails = new MailDetails_Extras();
                        objMailDetails.TokenNumber = dr["RecipientName"].ToString();
                        objMailDetails.AwardID = dr["AwardID"].ToString();
                        objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                        objMailDetails.ID = dr["ID"].ToString();
                        objMailDetails.Subject = "Remainder Mail for Coffee with Boss";
                        objMailDetails.FromID = "r&r@mahindra.com";
                        objMailDetails.SenderName = "r&r@mahindra.com";
                        objMailDetails.ToID = dr["RecipientEmail"].ToString();
                        objMailDetails.RecipientName = dr["RecipientName"].ToString();
                        // objMailDetails.CCID = dr["GiverName"].ToString();
                        if (dr["Days"].ToString() == "7")
                        {
                            TemplateFileName = "Coffee_7.txt";
                        }
                        else
                        {
                            TemplateFileName = "Coffee_14.txt";
                        }
                        string BodyPart = GetBody();
                        BodyPart = BodyPart.Replace("<%RecipientName%>", dr["RecipientName"].ToString().Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString()));
                        //BodyPart = BodyPart.Replace("<%RecipientName%>", dr["RecipientName"].ToString()).Replace("<%GiverName%>", dr["GiverName"].ToString()).Replace("<%AwardName%>", dr["AwardName"].ToString()).Replace("<%Amount%>", dr["AwardAmount"].ToString()).Replace("<%AwardedDate%>", dr["AwardedDate"].ToString()).Replace("<%ExpiryDate%>", dr["ExpiryDate"].ToString()).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                        objMailDetails.Body = BodyPart;
                        objMailDetails_Coll.Add(objMailDetails);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        LogData("-----------------------------------");
                        LogData("Exception for GetRecipientList method");
                        LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                        //throw new Exception("", ex);
                    }
                }
                LogData("------------------CoffeeWithBoss GetRecipientList Awards Try block Ends----------------");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogData("-----------------------------------");
                LogData("Exception for GetRecipientList out method");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                //throw new Exception("", ex);
            }
            return objMailDetails_Coll;
        }
    }
}


﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MM.DAL;
using Liberary_MailSender;
using Liberary_MailSender.Classes;
using Liberary_MailSender.Entities;
using MailSender.Core;
using Exe_ErrorLog;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Mail;
using TheArtOfDev.HtmlRenderer.Core.Entities;
using System.Drawing.Text;
using System.Diagnostics;
using EvoPdf;
using System.Net;
using System.Collections.Generic;
using System.Text;
namespace MailSender
{
    //-----------------------------------------------------------------------------------------------------
    public class Congo_3Days : MailBase
    {
        MailDetails objSendMail;
        ErrorLog exe_errorlog = new ErrorLog();
        protected override string TemplatePath { get; set; }
        protected override string TemplateFileName { get; set; }
        public override MailDetails_Coll RecipientList { get; set; }
        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// sends mails and the activies to be done on sending successful or failed and logging
        /// </summary>
        /// <param name="strTemplatePath"> Path where the template file can be found.Optional,if not provided then the template will be picked from the path set in the properties </param>
        /// <param name="strTemplateFileName"> Name of the template file.Optional,if not provided then the template will be picked from the path set in the properties TemplateFileName </param>
        ///
        public override void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "")
        {
            LogData("------------------Congo_3Days ProcessAndSendMails block starts----------------");
            string awardname = System.Configuration.ConfigurationSettings.AppSettings.Get("LongService_AwardType_Name").ToString();
            int longservicecount = 0, regularcount = 0;
            Console.WriteLine("Sending ... ");
            TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath") : strTemplatePath;
            RecipientList = GetRecipientList();
            TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
            MailDetails_Coll objMailDetails_Coll = SendMails();
            Console.WriteLine("Sending complete");
            int i = 0;
            try
            {
                LogData("------------------Congo_3Days ProcessAndSendMails Awards Try block starts----------------");
                foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                {
                    LogData("Token No : " + objMailDetails_Extras.TokenNumber + "and Mail Status : " + objMailDetails_Extras.Status + " and Award Name : " + objMailDetails_Extras.AwardType);//Pratik
                    try
                    {
                        if (objMailDetails_Extras.Status == "1")
                        {
                            //MailerContext m = new MailerContext(objMailDetails_Extras.GiverTokenNumber);
                            //m.GiveAward(objMailDetails_Extras.TokenNumber, objMailDetails_Extras.AwardType, objMailDetails_Extras.Reason);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogData("------------objMailDetails_Extras.Status is 0----------------");
                        LogData("------------Congo 3 days----------------");
                        LogData(ex.Message + ex.StackTrace);
                    }

                    #region mail sent based on award name
                    try
                    {
                        if (!(objMailDetails_Extras.AwardID.Contains(awardname)))
                        {

                            regularcount++;
                            if (regularcount == 1)
                            {
                                LogData("---------------------");
                                LogData("New Award(After 2 working days)");
                                i++;
                                string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                                LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + ", CCEmails = " + objMailDetails_Extras.CCEmails + "]");//, "Sending mail to " + dr["RecipientName"].ToString() + "...");
                            }
                            else
                            {
                                i++;
                                string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                                LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + ", CCEmails = " + objMailDetails_Extras.CCEmails + "]");//, "Sending mail to " + dr["RecipientName"].ToString() + "...");
                            }
                        }
                        else
                        {
                            longservicecount++;
                            if (longservicecount == 1)
                            {
                                LogData("---------------------");
                                LogData("Award for Silverstars");
                                i++;
                                string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                                LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");//, "Sending mail to " + dr["RecipientName"].ToString() + "...");
                            }
                            else
                            {
                                i++;
                                string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                                LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");//, "Sending mail to " + dr["RecipientName"].ToString() + "...");
                            }
                            regularcount = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogData("--------mail sent based on award name-------------");
                        LogData("Exception for Congo 3 Days");
                        LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                    }
                    #endregion
                }
                LogData("------------------Congo_3Days ProcessAndSendMails Awards Try block Ends----------------");
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData("Exception for Congo 3 Days - ProcessAndSendMails 1");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            if (objMailDetails_Coll.SendSuccessfull.Count > 0)
            {
                LogData("------------------Congo_3Days Update table email     block starts----------------");
                DAL_Common objCommon = new DAL_Common();
                //DataTable dtMailSent = new DataTable();
                DataRow drMail;
                DataColumn dc = new DataColumn();
                // dtMailSent.Columns.Add("ID", Type.GetType("System.Int32"));
                LogData("Update EMail Date IDs:");
                foreach (string strID in objMailDetails_Coll.SendSuccessfull)
                {
                    try
                    {
                        // LogData("------------------Congo_3Days Update table email Awards Try block starts----------------");
                        SqlParameter[] param = new SqlParameter[1];
                        param[0] = new SqlParameter("@ID", strID);
                        objCommon.DAL_SaveData("UpdateMailSent_New", param);
                        LogData(strID);
                        // LogData("------------------Congo_3Days Update table email Awards Try block Ends----------------");
                        // drMail = dtMailSent.NewRow();
                        //drMail["ID"] = Convert.ToInt32(strID);
                        //dtMailSent.Rows.Add(drMail);
                    }
                    catch (Exception exx)
                    {
                        exe_errorlog.ExceptionMethod("", "", exx);
                        LogData("---------------------");
                        LogData("Exception for Congo 3 Days - ProcessAndSendMails 2");
                        LogData(exx.Message + Environment.NewLine + exx.StackTrace);
                    }
                }
                //SqlParameter[] param = new SqlParameter[1];
                //param[0] = new SqlParameter("@MailSentFor", dtMailSent);
                //objCommon.DAL_SaveData("UpdateMailSent", param);
            }

            LogData(" Number of Mails sent successfully is " + objMailDetails_Coll.SendSuccessfull.Count);
            LogData(" Number of Mails sending failed is " + objMailDetails_Coll.SendFailed.Count);
            Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
            Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);


        }
        //-----------------------------------------------------------------------------------------------------
        #region Unused Points gift Hamper Mail Sending
        //-----------------------------------------------------------------------------------------------------
        //LogData("---------------------");
        //try
        //{
        //    RecipientList = GetUnusedHamperList();
        //    TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
        //    LogData("----------TemplateFileName -----------" + TemplateFileName);
        //    Console.WriteLine("Sending UnUsed Points Hamper Mail... ");
        //    LogData("Sending UnUsed Points Hamper Mail... ");
        //    MailDetails_Coll objMailDetails_Coll1 = SendMails();
        //    LogData("Sending UnUsed Points Hamper Mail Completed for unused points... ");
        //    if (objMailDetails_Coll1.SendSuccessfull.Count > 0)
        //    {
        //        DAL_Common objCommon = new DAL_Common();
        //        foreach (string strReq_ID in objMailDetails_Coll1.SendSuccessfull)
        //        {
        //            try
        //            {
        //                SqlParameter[] param = new SqlParameter[1];
        //                param[0] = new SqlParameter("@Request_id", strReq_ID);
        //                objCommon.DAL_SaveData("UpdateMailSent_UnsedPoint", param);
        //            }
        //            catch (Exception ex)
        //            {
        //                LogData("---------------------");
        //                LogData("Exception for Congo 3 Days, for UnUsed Points of GiftHamper for UpdateMailSent_UnsedPoint");
        //                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
        //            }
        //        }
        //    }
        //    Console.WriteLine("Sending complete");
        //}
        //catch (Exception ex)
        //{
        //    LogData("---------------------");
        //    LogData("Exception for Congo 3 Days, for UnUsed Points of GiftHamper  ");
        //    LogData(ex.Message + Environment.NewLine + ex.StackTrace);
        //}
        //LogData("----------UnUsed Points Hamper Mail completed-----------");
        //-----------------------------------------------------------------------------------------------------
        #endregion
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Get the List of the people to whom the mail should be triggered and sets it in the RecipientList property
        /// </summary>
        /// <returns>Collection of Reciepients of type MailDetails</returns>
        ///
        protected MailDetails_Coll GetRecipientList()
        {
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            DataSet dsEmail = new DataSet();
            DataSet dsgetEmails = new DataSet();
            DataSet dsgetPoints = new DataSet();
            DataSet dsget_GiverEmailId = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@Activity", ConfigurationSettings.AppSettings["NewAward"].ToString());
            //dsEmail = objCommon.DAL_GetData("GetThreeDayEmailList", objsqlparam);
            dsEmail = objCommon.DAL_GetData("GetThreeDayEmailListTest", objsqlparam);

            try
            {
                int i = 0;
                foreach (DataRow dr in dsEmail.Tables[0].Rows)
                {

                    /* Code added to  allow Milestone Award Receipients on Sat and Sun*/
                    //if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday || DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                    //{
                    //    if (dr["AwardType"].ToString().ToUpper() != "MILESTONE")
                    //    {
                    //        continue;
                    //    }
                    //}
                    /* Done */
                    string strCC = string.Empty;
                    objMailDetails = new MailDetails_Extras();
                    objMailDetails.TokenNumber = dr["TokenNumber"].ToString();
                    objMailDetails.AwardID = dr["AwardID"].ToString();
                    objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                    objMailDetails.ID = dr["ID"].ToString();
                    objMailDetails.Subject = "CONGRATULATIONS!!";
                    objMailDetails.FromID = "r&r@mahindra.com";
                    objMailDetails.SenderName = "r&r@mahindra.com";
                    // vanda start
                    if (dr["AwardType"].ToString().ToUpper() != ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                    {
                        objMailDetails.ToID = dr["EmailID"].ToString();
                    }

                    objMailDetails.RecipientName = dr["RecipientName"].ToString();
                    //objMailDetails.Reason = dr["ReasonForNomination"].ToString();
                    objMailDetails.Reason = dr["ReasonForCertificate"].ToString();

                    if (dr["CCEmailID"] != null)
                    {
                        objMailDetails.CCEmails = dr["CCEmailID"].ToString().Trim().Replace(";", ",");

                    }
                    if (!DBNull.Value.Equals(dr["AwardType"]))
                    {
                        objMailDetails.AwardType = dr["AwardType"].ToString();
                    }
                    else
                    {
                        objMailDetails.AwardType = string.Empty;
                    }
                    objMailDetails.GiverTokenNumber = dr["GiverToken"].ToString();
                    //if (!DBNull.Value.Equals(dr["AwardType"]) && ((dr["AwardType"].ToString().ToUpper() == "SPOT CASH") || (dr["AwardType"].ToString().ToUpper() == "EXCELLERATOR") || (dr["AwardType"].ToString().ToUpper() == ConfigurationManager.AppSettings["Vouchers"].ToString().ToUpper()))) //|| (dr["AwardType"].ToString().ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper()))) // This IF conditn is added by SSS on 24072013, since it was important to ensure only SPOT and EXCELLATOR awards mail contains CC Ids
                    //{
                    //    DAL_Common objCommon1 = new DAL_Common();
                    //    SqlParameter[] objsqlparam1 = new SqlParameter[1];
                    //    objsqlparam1[0] = new SqlParameter("@RTokenNumber", dr["GiverToken"].ToString());
                    //    dsgetEmails = objCommon1.DAL_GetData("usp_Get_CC_EmailIDs_byToken", objsqlparam1);
                    //    if (dsgetEmails.Tables[0].Rows[0]["Emails"].ToString().Trim(',') == string.Empty)
                    //    {
                    //        objMailDetails.CCEmails =dsgetEmails.Tables[0].Rows[0]["Emails"].ToString().Trim(',') + "," + strCC;
                    //    }
                    //    else
                    //    {
                    //       // objMailDetails.CCEmails = "portalhelpdesk@mahindra.com";
                    //    }
                    //}                   
                    string AwardType = Convert.ToString(dr["AwardType"]);

                    /* Added on 9 April 2014 */
                    Boolean blnZeroUnitpoints = false;
                    #region Gift hamper (no longer used)
                    //if (dr["AwardType"].ToString().ToUpper() == ConfigurationManager.AppSettings["Gift_Hamper"].ToString().ToUpper())
                    //{
                    //    TemplateFileName = AwardType + "_Auto.txt";
                    //    DAL_Common objCommonPoints = new DAL_Common();
                    //    SqlParameter[] objsqlparam2 = new SqlParameter[1];
                    //    objsqlparam2[0] = new SqlParameter("@AwardID", objMailDetails.ID);
                    //    dsgetPoints = objCommonPoints.DAL_GetData("usp_Get_UsedPoints", objsqlparam2);
                    //    if (dsgetPoints.Tables[0] != null && dsgetPoints.Tables[0].Rows.Count > 0)
                    //    {
                    //        // vanda start
                    //        try
                    //        {
                    //            TemplateFileName = AwardType + "_Auto.txt";
                    //            DAL_Common objCommonG_EmailID = new DAL_Common();
                    //            SqlParameter[] objsqlparam3 = new SqlParameter[1];
                    //            objsqlparam3[0] = new SqlParameter("@GTokenNumber", dr["GiverToken"].ToString());
                    //            dsget_GiverEmailId = objCommonG_EmailID.DAL_GetData("usp_Get_GiverEmailID", objsqlparam3);
                    //            objMailDetails.ToID = (dsget_GiverEmailId.Tables[0].Rows[0]["EmailID"].ToString());
                    //            TemplateFileName = AwardType + "_UnUsedPoints.txt";
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Console.WriteLine(ex.Message);
                    //        }
                    //        // vanda start
                    //    }
                    //    else
                    //    {
                    //        blnZeroUnitpoints = true;
                    //    }
                    //    //  TemplateFileName = AwardType + "_Auto.txt";
                    //}
                    #endregion

                    #region Voucher
                    //-----------------------------------------------------------------------------------------------------
                    // if (dr["AwardType"].ToString().ToUpper() == ConfigurationManager.AppSettings["Vouchers"].ToString().ToUpper())
                    //{
                    //    string MsgVoucher = "Thank You!";
                    //    TemplateFileName = AwardType + "_Auto.txt";
                    //    string BodyPartVoucher = GetBody();
                    //    if (objMailDetails.Reason != null)
                    //    {
                    //        BodyPartVoucher = BodyPartVoucher.Replace("<%reason%>", objMailDetails.Reason);
                    //        objMailDetails.Body = BodyPartVoucher;
                    //    }
                    //    else
                    //    {
                    //        BodyPartVoucher = BodyPartVoucher.Replace("<%reason%>", MsgVoucher.ToString());
                    //        objMailDetails.Body = BodyPartVoucher;
                    //    }
                    //}
                    //else
                    //{
                    //    if (dr["RedeemDate"].ToString() == "")
                    //    {
                    //        TemplateFileName = AwardType + "_Redeem.txt";
                    //    }
                    //    else
                    //    {
                    //        TemplateFileName = AwardType + "_Auto.txt";
                    //    }
                    //}                    


                    #endregion Voucher

                    #region Coffee
                    //-----------------------------------------------------------------------------------------------------
                    ////////string CoffeeDate = string.Empty;
                    ////////if (dr["CoffeeDate"] != null)
                    ////////{
                    ////////    if (dr["CoffeeDate"].ToString().Trim() != "")
                    ////////    {
                    ////////        try
                    ////////        {
                    ////////            CoffeeDate = Convert.ToDateTime(dr["CoffeeDate"]).ToString("dd/MM/yyyy");
                    ////////        }
                    ////////        catch (Exception objEx)
                    ////////        {
                    ////////            Console.Write(objEx.Message);
                    ////////        }
                    ////////    }
                    ////////}
                    ////////if (CoffeeDate != string.Empty)
                    ////////{
                    ////////    //BodyPart = BodyPart.Replace("<%VLINK%>", ConfigurationSettings.AppSettings["VendorLink"].ToString()).Replace("<%REDEEMLINK%>", ConfigurationSettings.AppSettings["ReedemChoiceLink"].ToString()).Replace("<%AwardType%>", dr["AwardType"].ToString()).Replace("<%AwardAmount%>", dr["AwardAmount"].ToString()).Replace("<%RecipientName%>", dr["RecipientName"].ToString()).Replace("<%CoffeeDate%>", CoffeeDate).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                    ////////    BodyPart = BodyPart.Replace("<%VLINK%>", ConfigurationSettings.AppSettings["VendorLink"].ToString()).Replace("<%REDEEMLINK%>", ConfigurationSettings.AppSettings["ReedemChoiceLink"].ToString()).Replace("<%MyAwards%>", ConfigurationSettings.AppSettings["ReedemChoiceLinkInternet"].ToString()).Replace("<%AwardType%>", dr["AwardType"].ToString()).Replace("<%AwardAmount%>", dr["AwardAmount"].ToString()).Replace("<%RecipientName%>", dr["RecipientName"].ToString()).Replace("<%CoffeeDate%>", CoffeeDate).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                    ////////}
                    //////////-----------------------------------------------------------------------------------------------------
                    #endregion Coffee
                    //////////-----------------------------------------------------------------------------------------------------
                    ////////else
                    ////////{
                    ////////    BodyPart = BodyPart.Replace("<%VLINK%>", ConfigurationSettings.AppSettings["VendorLink"].ToString()).Replace("<%REDEEMLINK%>", ConfigurationSettings.AppSettings["ReedemChoiceLink"].ToString()).Replace("<%MyAwards%>", ConfigurationSettings.AppSettings["ReedemChoiceLinkInternet"].ToString()).Replace("<%AwardType%>", dr["AwardType"].ToString()).Replace("<%AwardAmount%>", dr["AwardAmount"].ToString()).Replace("<%RecipientName%>", dr["RecipientName"].ToString()).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                    ////////}
                    ////////if (dr["GiverName"].ToString() != "")
                    ////////{
                    ////////    BodyPart = BodyPart.Replace("<%GiverHODName%>", dr["GiverName"].ToString());
                    ////////}
                    ////////BodyPart = BodyPart.Replace("<%reason%>", objMailDetails.Reason);



                    TemplateFileName = "SpotAwardCertificate.txt";
                    string BodyPart = GetBody();

                    BodyPart = BodyPart.Replace("#name", dr["RecipientName"].ToString());
                    BodyPart = BodyPart.Replace("#hodmsg", dr["ReasonForNomination"].ToString());
                    BodyPart = BodyPart.Replace("#GiverToken", dr["GiverName"].ToString());
                    BodyPart = BodyPart.Replace("#awardeddate", dr["AwardedDate"].ToString());
                    // BodyPart = BodyPart.Replace("#awrd", AwardType);


                    if (dr["AwardType"].ToString().ToUpper() == ConfigurationManager.AppSettings["Spot_Voucher"].ToString().ToUpper())
                    {
                        BodyPart = BodyPart.Replace("#awrd", "SPOT AWARD");
                        AwardType = "SPOT AWARD";
                    }
                    if (dr["AwardType"].ToString().ToUpper() == ConfigurationManager.AppSettings["Coffee_With_You"].ToString().ToUpper())
                    {
                        BodyPart = BodyPart.Replace("#awrd", "Coffee With You Award");
                        AwardType = "Coffee With You AWARD";
                    }
                    if (dr["AwardType"].ToString().ToUpper() == ConfigurationManager.AppSettings["Excellerator"].ToString().ToUpper())
                    {
                        BodyPart = BodyPart.Replace("#awrd", "Excellerator Award");
                        AwardType = "Excellerator AWARD";
                    }


                    // Create a HTML to Image converter object with default settings
                    HtmlToImageConverter htmlToImageConverter = new HtmlToImageConverter();
                    htmlToImageConverter.HtmlViewerWidth = 900;
                    htmlToImageConverter.HtmlViewerHeight = 650;
                    htmlToImageConverter.ConversionDelay = 0;
                    System.Drawing.Image[] imageTiles = null;
                    string baseUrl = "";
                    imageTiles = htmlToImageConverter.ConvertHtmlToImageTiles(BodyPart, baseUrl);
                    System.Drawing.Image outImage = imageTiles[0];
                    string token = dr["TokenNumber"].ToString();
                    string imageName = token + i.ToString() + ".jpg";
                    imageTiles[0].Save(ConfigurationManager.AppSettings["Certificate_Path"].ToString() + imageName);

                    // crop image ProcessAndSendMails save
                    Image img = Image.FromFile(ConfigurationManager.AppSettings["Certificate_Path"].ToString() + imageName);
                    Bitmap bmpImage = new Bitmap(img);
                    Bitmap bmpCrop = bmpImage.Clone(new Rectangle(17, 17, 817, 585), bmpImage.PixelFormat);
                    string attimagename = AwardType + "" + token + i.ToString() + ".jpg";
                    bmpCrop.Save(ConfigurationManager.AppSettings["Certificate_Path"].ToString() + attimagename);

                    // Attachment
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();
                        objMailDetails.Attachments = (new Attachment(ConfigurationManager.AppSettings["Certificate_Path"].ToString() + attimagename));
                    }

                    // Get body
                    TemplateFileName = "SpotAwardCertificateMailAttatchment.txt";
                    string BodyPart1 = GetBody();

                    BodyPart1 = BodyPart1.Replace("<%RecipientName%>", dr["RecipientName"].ToString());
                    BodyPart1 = BodyPart1.Replace("<%GiverHODName%>", dr["GiverName"].ToString());
                    BodyPart1 = BodyPart1.Replace("<%AwardType%>", dr["AwardType"].ToString());
                    BodyPart1 = BodyPart1.Replace("<%Amount%>", dr["AwardAmount"].ToString());
                    BodyPart1 = BodyPart1.Replace("#imgname", attimagename);
                    objMailDetails.Body = BodyPart1;
                    i++;
                    //apicall(attimagename, dr, dr["AwardType"].ToString());
                    if (blnZeroUnitpoints == false)
                    {
                        objMailDetails_Coll.Add(objMailDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData("Exception for Congo 3 Days - GetRecipientList");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
            }


            return objMailDetails_Coll;
        }
        public void apicall(string imgName, DataRow dataRow, string awardType)
        {
            string returnResponseText = "";
            try
            {
                string requestURL = "http://mivlad2rd2.corp.mahindra.com:8000/sap/bc/zzhr_send_lettr?sap-client=220";
                //string requestURL = "https://emss.mahindra.com/sap/bc/zzhr_send_lettr?sap-client=500";
                string fileName = ConfigurationSettings.AppSettings["Certificate_Path"].ToString() + imgName;
                WebClient wc = new WebClient();
                string pdfFilePath = ConfigurationSettings.AppSettings["Certificate_Path"].ToString() + imgName;
                byte[] bytes = System.IO.File.ReadAllBytes(pdfFilePath);
                //byte[] bytes = wc.DownloadData(fileName); // You need to do this download if your file is on any other server otherwise you can convert that file directly to bytes  
                Dictionary<string, object> postParameters = new Dictionary<string, object>();
                // Add your parameters here  
                postParameters.Add("file_name", new FormUpload.FileParameter(bytes, Path.GetFileName(fileName), "application/pdf"));

                postParameters.Add("award_date", Convert.ToString(dataRow["Datetime"]));
                postParameters.Add("award_emp_id", Convert.ToString(dataRow["TokenNumber"]));
                postParameters.Add("award_by", Convert.ToString(dataRow["GiverTokenNumber"]));
                postParameters.Add("award_by_name", Convert.ToString(dataRow["GiverName"]));
                postParameters.Add("award_comments", Convert.ToString(dataRow["ReasonForNomination"]));
                postParameters.Add("award_type", "RnR Letter");
                postParameters.Add("award_subtype", awardType);
                string userAgent = "Someone";
                string headerkey = "", headervalue = "";
                HttpWebResponse webResponse = FormUpload.MultipartFormPost(requestURL, userAgent, postParameters, headerkey, headervalue);
                // Process response  
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                returnResponseText = responseReader.ReadToEnd();
                SaveResponse(returnResponseText, Convert.ToString(dataRow["id"]), awardType);
                webResponse.Close();
            }
            catch (Exception exp)
            {
                SaveResponse(returnResponseText, Convert.ToString(dataRow["id"]), awardType);
            }
        }

        private void SaveResponse(string returnResponseText, string id, string awardType)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MM_Connection"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("spUpdateResponse", sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@returnResponse", SqlDbType.NVarChar, 100).Value = returnResponseText;
            sqlCommand.Parameters.Add("@Id", SqlDbType.NVarChar, 100).Value = id;
            sqlCommand.Parameters.Add("@awardType", SqlDbType.NVarChar, 100).Value = awardType;
            try
            {
                sqlConnection.Open();
                sqlCommand.CommandTimeout = 120;
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlConnection.Close();
            }
        }

        //-----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Get the List of the people to whom the mail should be triggered and sets it in the RecipientList property
        /// </summary>
        /// <returns>Collection of Reciepients of type MailDetails</returns>
        ///
        //-----------------------------------------------------------------------------------------------------
        #region Gift hamper
        //-----------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------

        protected MailDetails_Coll GetUnusedHamperList()
        {
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            DataSet dsgetPoints = new DataSet();
            DAL_Common objCommonPoints = new DAL_Common();
            try
            {
                dsgetPoints = objCommonPoints.DAL_GetData("usp_Get_UnusedHamperList");
                LogData("---------- usp_Get_UnusedHamperList getting records-----------");
                foreach (DataRow dr in dsgetPoints.Tables[0].Rows)
                {
                    objMailDetails = new MailDetails_Extras();
                    // objMailDetails.TokenNumber = dr["TokenNumber"].ToString();
                    // objMailDetails.AwardID = dr["AwardID"].ToString();
                    objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                    objMailDetails.ID = dr["Request_Id"].ToString();
                    objMailDetails.Subject = "Unused Points for the Gift Hamper Award!";
                    objMailDetails.FromID = "r&r@mahindra.com";
                    objMailDetails.SenderName = "r&r@mahindra.com";
                    objMailDetails.ToID = dr["nomEmail"].ToString();
                    objMailDetails.RecipientName = dr["Nom_Name"].ToString();
                    TemplateFileName = "Spot Gift Hamper_UnUsedPoints.txt";
                    string BodyPart = GetBody();
                    //BodyPart = BodyPart.Replace("<%GiverHODName%>", dr["Nom_Name"].ToString()).Replace("<%GiverName%>", dr["GiverName"].ToString()).Replace("<%AwardName%>", dr["AwardName"].ToString()).Replace("<%Amount%>", dr["AwardAmount"].ToString()).Replace("<%AwardedDate%>", dr["AwardedDate"].ToString()).Replace("<%ExpiryDate%>", dr["ExpiryDate"].ToString()).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                    BodyPart = BodyPart.Replace("<%GiverHODName%>", dr["Nom_Name"].ToString());
                    objMailDetails.Body = BodyPart;
                    objMailDetails_Coll.Add(objMailDetails);
                }
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData("Exception for Congo 3 Days - GetUnusedHamperList");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            LogData("----------GetUnusedHamperList() Complete-----------");
            return objMailDetails_Coll;
        }
        //-----------------------------------------------------------------------------------------------------
        #endregion gift hamper
        //-----------------------------------------------------------------------------------------------------
    }
}
public static class FormUpload
{
    private static readonly Encoding encoding = Encoding.UTF8;
    public static HttpWebResponse MultipartFormPost(string postUrl, string userAgent, Dictionary<string, object> postParameters, string headerkey, string headervalue)
    {
        string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
        string contentType = "multipart/form-data; boundary=" + formDataBoundary;

        byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

        return PostForm(postUrl, userAgent, contentType, formData, headerkey, headervalue);
    }
    private static HttpWebResponse PostForm(string postUrl, string userAgent, string contentType, byte[] formData, string headerkey, string headervalue)
    {
        HttpWebRequest request = WebRequest.Create(postUrl) as HttpWebRequest;

        if (request == null)
        {
            throw new NullReferenceException("request is not a http request");
        }

        // Set up the request properties.  
        request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("PUNJNA-CONT:Narendra@906"));
        request.Credentials = new NetworkCredential(" PUNJNA-CONT", "Narendra@906");
        request.Method = "POST";
        request.ContentType = contentType;
        request.UserAgent = userAgent;
        request.CookieContainer = new CookieContainer();
        request.ContentLength = formData.Length;

        // You could add authentication here as well if needed:  
        // request.PreAuthenticate = true;  
        // request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;  

        //Add header if needed  
        //request.Headers.Add(headerkey, headervalue);

        // Send the form data to the request.  
        using (Stream requestStream = request.GetRequestStream())
        {
            requestStream.Write(formData, 0, formData.Length);
            requestStream.Close();
        }

        return request.GetResponse() as HttpWebResponse;
    }

    private static byte[] GetMultipartFormData(Dictionary<string, object> postParameters, string boundary)
    {
        Stream formDataStream = new System.IO.MemoryStream();
        bool needsCLRF = false;

        foreach (var param in postParameters)
        {

            if (needsCLRF)
                formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));

            needsCLRF = true;

            if (param.Value is FileParameter) // to check if parameter if of file type   
            {
                FileParameter fileToUpload = (FileParameter)param.Value;

                // Add just the first part of this param, since we will write the file data directly to the Stream  
                string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n",
                    boundary,
                    param.Key,
                    fileToUpload.FileName ?? param.Key,
                    fileToUpload.ContentType ?? "application/octet-stream");

                formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

                // Write the file data directly to the Stream, rather than serializing it to a string.  
                formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
            }
            else
            {
                string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                    boundary,
                    param.Key,
                    param.Value);
                formDataStream.Write(encoding.GetBytes(postData), 0, encoding.GetByteCount(postData));
            }
        }

        // Add the end of the request.  Start with a newline  
        string footer = "\r\n--" + boundary + "--\r\n";
        formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));

        // Dump the Stream into a byte[]  
        formDataStream.Position = 0;
        byte[] formData = new byte[formDataStream.Length];
        formDataStream.Read(formData, 0, formData.Length);
        formDataStream.Close();

        return formData;
    }

    public class FileParameter
    {
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public FileParameter(byte[] file) : this(file, null) { }
        public FileParameter(byte[] file, string filename) : this(file, filename, null) { }
        public FileParameter(byte[] file, string filename, string contenttype)
        {
            File = file;
            FileName = filename;
            ContentType = contenttype;
        }
    }
}



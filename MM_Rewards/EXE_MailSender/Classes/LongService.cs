﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using MM.DAL;
using Liberary_MailSender;
using Liberary_MailSender.Classes;
using Liberary_MailSender.Entities;
using System.Data;
namespace EXE_MailSender.Classes
{
    //-----------------------------------------------------------------------------------------------------
    public class LongService : MailBase
    {
        MailDetails objSendMail;
        protected override string TemplatePath { get; set; }
        protected override string TemplateFileName { get; set; }
        public override MailDetails_Coll RecipientList { get; set; }
        //-----------------------------------------------------------------------------------------------------
        public override void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "")
        {
            LogData("------------------LongService ProcessAndSendMails Awards Try block starts----------------");
            try
            {
                Console.WriteLine("Sending...");
                TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? System.Configuration.ConfigurationSettings.AppSettings.Get("AwardTypeTemplatePath") : strTemplatePath;
                RecipientList = GetRecipientList();
                TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
                MailDetails_Coll objMailDetails_Coll = SendMails();
                Console.WriteLine("Sending complete");
                int i = 0;
                foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                {
                    i++;
                    string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                    LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");//, "Sending mail to " + dr["RecipientName"].ToString() + "...");
                }
                if (objMailDetails_Coll.SendSuccessfull.Count > 0)
                {
                    DAL_Common objCommon = new DAL_Common();
                    DataTable dtMailSent = new DataTable();
                    DataRow drMail;
                    DataColumn dc = new DataColumn();
                    dtMailSent.Columns.Add("ID", Type.GetType("System.Int32"));
                    foreach (string strID in objMailDetails_Coll.SendSuccessfull)
                    {
                        drMail = dtMailSent.NewRow();
                        drMail["ID"] = Convert.ToInt32(strID);
                        dtMailSent.Rows.Add(drMail);
                    }
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@MailSentFor", dtMailSent);
                    objCommon.DAL_SaveData("UpdateMailSent", param);
                }
                LogData(" Number of Mails sent successfully is " + objMailDetails_Coll.SendSuccessfull.Count);
                LogData(" Number of Mails sending failed is " + objMailDetails_Coll.SendFailed.Count);
                Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
                Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);
                LogData("------------------LongService ProcessAndSendMails Awards Try block Ends----------------");
            }
            catch (Exception ex)
            {
                //  LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");
                LogData(ex.Message);
                LogData(ex.StackTrace);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected MailDetails_Coll GetRecipientList()
        {
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            DataSet dsEmail = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            SqlParameter[] objsqlparam = new SqlParameter[1];
            objsqlparam[0] = new SqlParameter("@Activity", ConfigurationSettings.AppSettings["NewAward"].ToString());
            dsEmail = objCommon.DAL_GetData("GetLongServiceRecipients", objsqlparam);
            foreach (DataRow dr in dsEmail.Tables[0].Rows)
            {
                objMailDetails = new MailDetails_Extras();
                objMailDetails.TokenNumber = dr["TokenNumber"].ToString();
                objMailDetails.AwardID = dr["AwardID"].ToString();
                objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                objMailDetails.ID = dr["ID"].ToString();
                objMailDetails.Subject = "CONGRATULATIONS!!";
                objMailDetails.FromID = "r&r@mahindra.com";
                objMailDetails.SenderName = "r&r@mahindra.com";
                objMailDetails.ToID = dr["EmailID"].ToString();
                objMailDetails.RecipientName = dr["RecipientName"].ToString();
                //objMailDetails.Reason = dr["ReasonForNomination"].ToString();
                string AwardType = Convert.ToString(dr["AwardType"]);
                //if (dr["RedeemDate"].ToString() == "")
                //{
                //TemplateFileName = AwardType + "_Redeem.txt";
                //}
                //else
                //{
                TemplateFileName = AwardType + "_Auto.txt";
                //}
                string BodyPart = GetBody();
                BodyPart = BodyPart.Replace("<%VLINK%>", ConfigurationSettings.AppSettings["VendorLink"].ToString()).Replace("<%REDEEMLINK%>", ConfigurationSettings.AppSettings["ReedemChoiceLink"].ToString()).Replace("<%AwardType%>", dr["AwardType"].ToString()).Replace("<%AwardAmount%>", dr["AwardAmount"].ToString()).Replace("<%RecipientName%>", dr["RecipientName"].ToString()).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                //BodyPart = BodyPart.Replace("<%reason%>", objMailDetails.Reason);
                objMailDetails.Body = BodyPart;
                objMailDetails_Coll.Add(objMailDetails);
            }
            return objMailDetails_Coll;
        }
    }
}


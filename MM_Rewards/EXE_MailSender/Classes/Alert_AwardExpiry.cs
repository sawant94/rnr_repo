﻿using System;
using System.Data;
using MM.DAL;
using System.Configuration;
using Liberary_MailSender;
using Liberary_MailSender.Classes;
using Liberary_MailSender.Entities;
namespace MailSender.Classes
{
    //-----------------------------------------------------------------------------------------------------
    public class Alert_AwardExpiry : MailBase
    {
        protected override string TemplatePath { get; set; }
        protected override string TemplateFileName { get; set; }
        public override MailDetails_Coll RecipientList { get; set; }
        //-----------------------------------------------------------------------------------------------------
        public override void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "")
        {
            try
            {
                LogData("------------------Expiring Awards Try block Starts ----------------");
                //TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? "" : strTemplatePath;
                TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? ConfigurationSettings.AppSettings["AwardTypeTemplatePath"].ToString() : strTemplatePath;
                RecipientList = GetRecipientList();
                TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
                Console.WriteLine("Sending ... ");
                MailDetails_Coll objMailDetails_Coll = SendMails();
                Console.WriteLine("Sending complete");
                LogData("---------------------");
                LogData("Expiring Awards (Before 7 working days) and error while sending salary team");
                int i = 0;
                foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                {
                    i++;
                    string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                    LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");
                }
                LogData(" Number of Mails sent successfully : " + objMailDetails_Coll.SendSuccessfull.Count);
                LogData(" Number of Mails sending failed : " + objMailDetails_Coll.SendFailed.Count);
                Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
                Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);
                LogData("------------------Expiring Awards Try block End----------------");
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData("Exception for Alert_AwardExpiry - ProcessAndSendMails");
                LogData(" Exception : " + ex.Message + ex.StackTrace);// + " || Inner Exception : " + ex.InnerException + Environment.NewLine);
            }
            LogData("------------------Expiring Awards block End----------------");
        }
        //-----------------------------------------------------------------------------------------------------
        protected MailDetails_Coll GetRecipientList()
        {
            TemplateFileName = "Award_ExpiryMail.txt";
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            DataSet dsEmail = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            LogData("------------------GetRecipientList Expiring Awards Starts----------------");
            try
            {
                LogData("------------------GetRecipientList Expiring Awards Try block Starts----------------");
                dsEmail = objCommon.DAL_GetData("usp_ToBeExpiredAwards");
                foreach (DataRow dr in dsEmail.Tables[0].Rows)
                {
                    objMailDetails = new MailDetails_Extras();
                    objMailDetails.TokenNumber = dr["TokenNumber"].ToString();
                    objMailDetails.AwardID = dr["AwardID"].ToString();
                    objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                    objMailDetails.ID = dr["ID"].ToString();
                    objMailDetails.Subject = "Award To be Expired Shortly!!";
                    objMailDetails.FromID = "r&r@mahindra.com";
                    objMailDetails.SenderName = "r&r@mahindra.com";
                    objMailDetails.ToID = dr["EmailID"].ToString();
                    objMailDetails.RecipientName = dr["RecipientName"].ToString();
                    //TemplateFileName = "Award_ExpiryMail.txt";
                    string BodyPart = GetBody();
                    BodyPart = BodyPart.Replace("<%RecipientName%>", dr["RecipientName"].ToString()).Replace("<%GiverName%>", dr["GiverName"].ToString()).Replace("<%AwardName%>", dr["AwardName"].ToString()).Replace("<%Amount%>", dr["AwardAmount"].ToString()).Replace("<%AwardedDate%>", dr["AwardedDate"].ToString()).Replace("<%ExpiryDate%>", dr["ExpiryDate"].ToString()).Replace("<%MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                    objMailDetails.Body = BodyPart;
                    objMailDetails_Coll.Add(objMailDetails);
                }
                LogData("------------------GetRecipientList Expiring Awards Try block End----------------");

  ///////********* new functionality for send mail if error ocured while sending points to salary team  ***************//////////
                string salaryerrorbody = "<html><body><h3>Hi, </h3><p>Error occured for employee :  <b>#employee </b>while sending award amount to salary team </p> <p>Award type : #awardtype </p><p> Amount     : #amount</p><p> Date       : #date </p><br><br>Rgards,<br>RNR Team</body></html>";

                foreach (DataRow dr in dsEmail.Tables[1].Rows)
                {
                    objMailDetails = new MailDetails_Extras();                                 
                    objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];                   
                    objMailDetails.Subject = "Error Occured while sending award amount to Salary";
                    objMailDetails.FromID = "r&r@mahindra.com";
                    objMailDetails.SenderName = "r&r@mahindra.com";
                    objMailDetails.ToID = "portalhelpdesk@mahindra.com";                    
                    string datenow = DateTime.Now.ToString();
                    salaryerrorbody = salaryerrorbody.Replace("#employee", dr["TokenNo"].ToString() ).Replace("#awardtype", dr["AwardType"].ToString()).Replace("#amount", dr["AwardAmount"].ToString()).Replace("#date", datenow);
                    objMailDetails.Body = salaryerrorbody;
                    objMailDetails_Coll.Add(objMailDetails);
                }
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData("Exception for Alert_AwardExpiry - GetRecipientList");
                LogData(" Exception : " + ex.Message + ex.StackTrace);// + " || Inner Exception : " + ex.InnerException + Environment.NewLine);
            }
            LogData("------------------GetRecipientList Expiring Awards Starts----------------");
            return objMailDetails_Coll;
        }
    }
}

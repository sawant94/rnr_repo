﻿using System;
using System.Data;
using MM.DAL;
using System.Data.SqlClient;
using System.Configuration;
using Liberary_MailSender;
using Liberary_MailSender.Classes;
using Liberary_MailSender.Entities;
using System.Diagnostics;
namespace MailSender.Classes
{
    class BossDay_Celebration : MailBase
    {
        protected override string TemplatePath { get; set; }
        public override MailDetails_Coll RecipientList { get; set; }
        protected override string TemplateFileName { get; set; }
        //-----------------------------------------------------------------------------------------------------
        public override void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "")
        {
            try
            {
                LogData("------------------BossDay_Celebration ProcessAndSendMails Awards Try block starts----------------");
                TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? ConfigurationSettings.AppSettings["AwardTypeTemplatePath"].ToString() : strTemplatePath;
                RecipientList = GetRecipientList();
                TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;
                Console.WriteLine("Sending ... ");
                MailDetails_Coll objMailDetails_Coll = SendMails();
                Console.WriteLine("Sending complete");
                LogData("---------------------");
                LogData("BossDay Mail Report");
                int i = 0;
                foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                {
                    i++;
                    string message = objMailDetails_Extras.Status == "1" ? "Mail Sent SUCCESSFULLY " : "Sending FAILED - " + objMailDetails_Extras.Status;
                    LogData("\t " + i + ") " + message + " : " + objMailDetails_Extras.RecipientName + "(" + objMailDetails_Extras.ToID + ")" + " [ Token Number : " + objMailDetails_Extras.TokenNumber + ", AwardID = " + objMailDetails_Extras.AwardID + "]");
                }
                LogData(" Number of Mails sent successfully : " + objMailDetails_Coll.SendSuccessfull.Count);
                LogData(" Number of Mails sending failed : " + objMailDetails_Coll.SendFailed.Count);
                Console.WriteLine("Number of mails sent SUCCESSFULLY : " + objMailDetails_Coll.SendSuccessfull.Count);
                Console.WriteLine("Number of mails sending FAILED : " + objMailDetails_Coll.SendFailed.Count);
                DataTable dtMailSent = new DataTable();
                DataRow drMail;
                DataColumn dc = new DataColumn();
                dtMailSent.Columns.Add("ID", typeof(int));
                foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)
                {
                    if (objMailDetails_Extras.Status == "1")
                    {
                        drMail = dtMailSent.NewRow();
                        drMail["ID"] = Convert.ToInt32(objMailDetails_Extras.ID);
                        dtMailSent.Rows.Add(drMail);
                    }
                }
                if (dtMailSent.Rows.Count > 0)
                {
                    DAL_Common objCommon = new DAL_Common();
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@ID", dtMailSent);
                    objCommon.DAL_SaveData("Update_BossEcard", param);
                }
                LogData("------------------BossDay_Celebration ProcessAndSendMails Awards Try block Ends----------------");
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData("Exception for Bossday_ProcessMail");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        protected MailDetails_Coll GetRecipientList()
        {
            MailDetails_Extras objMailDetails;
            MailDetails_Coll objMailDetails_Coll = new MailDetails_Coll();
            try
            {
                LogData("------------------BossDay_Celebration GetRecipientList Awards Try block starts----------------");
                DataSet dsEmail = new DataSet();
                DAL_Common objCommon = new DAL_Common();
                dsEmail = objCommon.DAL_GetData("usp_GetBossDayDataToSendMail");
                foreach (DataRow dr in dsEmail.Tables[0].Rows)
                {
                    objMailDetails = new MailDetails_Extras();
                    try
                    {
                        string Message = String.Empty;
                        objMailDetails.SMTPServer = ConfigurationSettings.AppSettings["SMTP"];
                        objMailDetails.ID = dr["ID"].ToString();
                        objMailDetails.TokenNumber = dr["RecipientTokenNumber"].ToString();
                        objMailDetails.RecipientName = dr["RecipientName"].ToString();
                        objMailDetails.SenderName = dr["GiverName"].ToString();
                        objMailDetails.ToID = dr["TOID"].ToString();
                        objMailDetails.FromID = dr["FROMID"].ToString();
                        objMailDetails.Subject = "Happy Boss’ day from " + dr["GiverName"].ToString();
                        Message = dr["Message"].ToString();
                        Message = Message + "<br/>&nbsp;&nbsp;&nbsp;-&nbsp;" + dr["GiverName"].ToString();
                        objMailDetails.CardType = dr["CardType"].ToString();
                        if (objMailDetails.CardType.Trim() != "")
                            TemplateFileName = "BossDay1.html";
                        string BodyPart = GetBody();
                        BodyPart = BodyPart.Replace("#PATH#", ConfigurationSettings.AppSettings["ApplicationPath"].ToString() + "Web_Pages/Rewards/BossEcard.aspx?ecardid=" + objMailDetails.ID);
                        BodyPart = BodyPart.Replace("/Images/", ConfigurationSettings.AppSettings["ApplicationPath"].ToString() + "images/");
                        BodyPart = BodyPart.Replace("#Salutation#", "Dear " + objMailDetails.RecipientName);
                        BodyPart = BodyPart.Replace("#Message#", Message);
                        objMailDetails.Body = BodyPart;
                        objMailDetails_Coll.Add(objMailDetails);
                    }
                    catch (Exception ex)
                    {
                        objMailDetails.Status = ex.Message;
                    }
                }
                LogData("------------------BossDay_Celebration GetRecipientList Awards Try block Ends----------------");
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData("Exception for Bossday - GetRecipientList 2");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            return objMailDetails_Coll;
        }
    }
}


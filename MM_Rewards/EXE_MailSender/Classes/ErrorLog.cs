﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Configuration;
namespace Exe_ErrorLog
{
    //-----------------------------------------------------------------------------------------------------
    public class ErrorLog
    {
        //-----------------------------------------------------------------------------------------------------
        public void ExceptionMethod(string strPageName, string strmethod, Exception ex)
        {
            LogExceptionNormal(strPageName, strmethod, ex);
        }
        //-----------------------------------------------------------------------------------------------------
        private void LogExceptionNormal(string strPageName, string strmethod, Exception ex)
        {
            string msg = "";
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                string filename = ConfigurationManager.AppSettings["errorPath"].ToString() + "MM_Rewards_EXELog" + "-" + System.DateTime.Now.ToString("ddMMyyyy").Replace("/", "").Trim() + ".txt";
                if (!File.Exists(filename))
                {
                    fs = File.Create(filename);
                    sw = new StreamWriter(fs);
                    sw.WriteLine("--------------------------------------------------------");
                    sw.WriteLine("PageName : " + strPageName + "; Method Name :" + strmethod + "; Date: " + System.DateTime.Now.ToString());
                    sw.WriteLine("Error Trace : " + ex.StackTrace);
                    msg = ex.Message;
                    sw.WriteLine(msg);
                    sw.Close();
                    fs.Close();
                }
                else
                {
                    sw = new StreamWriter(filename, true);
                    sw.WriteLine("--------------------------------------------------------");
                    sw.WriteLine("PageName : " + strPageName + "; Method Name :" + strmethod + "; Date: " + System.DateTime.Now.ToString());
                    sw.WriteLine("Error Trace : " + ex.StackTrace);
                    msg = ex.Message;
                    sw.WriteLine(msg);
                    sw.Close();
                }
            }
            catch
            {
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }
    }
}


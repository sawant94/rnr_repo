﻿
using System.Net.Mail;
namespace Liberary_MailSender.Entities
{
    class MailDetails_Extras :MailDetails
    {
        public string RNREmailID { get; set; }
        public string TokenNumber { get; set; }
        public string AwardID { get; set; }
        public string HODTokenNumber { get; set; }
        public int Years { get; set; }
        public string DateOfMileStone { get; set; }
        public string Name { get; set; }
        public string CardType { get; set; }
        public string Reason { get; set; }
        public string AwardType { get; set; }
        public string GiverTokenNumber { get; set; }
        public string CCID { get; set; }
     
       
    }
}

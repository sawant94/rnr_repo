﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EXE_MailSender.DTO
{
    public class BudgetUtilization
    {
        public string HODName { get; set; }
        public string TokenNumber { get; set; }
        public string EmployeeName { get; set; }        
        public int AwardsGivenInFinancialYear { get; set; }
        public int BalanceBudget { get; set; }
        public int AwardAmountForTheMonth { get; set; }
        public int Spot { get; set; }
        public int Excellerator { get; set; }
        public int Vouchers { get; set; }
        public int GiftHamper { get; set; }
        public int CoffeeWithYou { get; set; }
        public int Budget { get; set; }
        public bool IsManager { get; set; }
        public int UtilizationFlag { get; set; }
    }
}

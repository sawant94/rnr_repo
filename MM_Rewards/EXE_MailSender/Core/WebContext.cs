﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BL = Idealake.Mahindra.RewardsSocial;
using DAL = Idealake.Mahindra.RewardsSocial.Data;
using System.Configuration;

namespace MailSender.Core
{
    public class MailerContext : DAL.DBContext
    {
     
      
        private string currentUserTokenNumber;
        public override BL.User CurrentUser
        {
            get
            {
                BL.User currentUser;
                //if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["demouser"]))
                //{
                //    currentUser = GetUserByTokenNumber(WebConfigurationManager.AppSettings["demouser"]);
                //}
                //else
                //{
                    //string currentUserName = HttpContext.Current.User.Identity.Name;
                    //if (HttpContext.Current.Session["TokenNumber"] != null)
                    //{
                    //    currentUser = GetUserByTokenNumber(HttpContext.Current.Session["TokenNumber"].ToString());
                    //}
                    //else
                    //{
                    //    string currentUserToken = currentUserName.Substring(currentUserName.IndexOf(@"\") + 1);
                    //    currentUser = GetUserByTokenNumber(currentUserToken);
                    //}
                //}
                currentUser = GetUserByTokenNumber(currentUserTokenNumber);

                return currentUser;
            }
        }

        public MailerContext(string CToken)
            : base(ConfigurationManager.ConnectionStrings["MMRewardsSocialDB"].ConnectionString)
        {
            currentUserTokenNumber = CToken;
        }
    }
}
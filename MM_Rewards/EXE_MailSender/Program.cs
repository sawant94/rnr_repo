﻿using System;
using System.IO;
using System.Reflection;
using Liberary_MailSender;
using System.Configuration;
using MailSender;
using MailSender.Classes;
using MM.DAL;
using System.Data.SqlClient;
using System.Globalization;
using System.Data;
using EXE_MailSender.Mahindra_webService;

namespace EXE_MailSender
{
    class Program //: MailBase
    {
        //-----------------------------------------------------------------------------------------------------
        public static void Main(string[] args)
        {
            string folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            MailBase.LogDirPath = folder + "\\Logs";
            MailBase.LogFileName = DateTime.Now.Date.ToString("MMM")
            + "_" + DateTime.Now.Date.Year + ".txt";
            if (!System.IO.Directory.Exists(MailBase.LogDirPath))
            {
                System.IO.Directory.CreateDirectory(MailBase.LogDirPath);
                Console.WriteLine("Creating the Log folder ... ");
                Console.WriteLine();
            }

            //**** to delete images after sent attachment
            //try
            //{
            //    MailBase.LogData("error at deleting files start");
            //    System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["Certificate_Path"].ToString());
            //    foreach (FileInfo file in di.GetFiles())
            //    {
            //        file.Delete();
            //    }
            //    foreach (DirectoryInfo dir in di.GetDirectories())
            //    {
            //        dir.Delete(true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MailBase.LogData("error at deleting files "+ ex +"");
            //}



            //-----------------------------------------------------------------------------------------------------
            #region Log Startup
            //-----------------------------------------------------------------------------------------------------
            MailBase.LogData("===========================================================================================================");
            MailBase.LogData(DateTime.Now.Date.ToString("dd MMM yyyy"));
            MailBase.LogData("Start Time : " + DateTime.Now.ToString("hh:mm:ss tt"));
            //-----------------------------------------------------------------------------------------------------
            #endregion
            //-----------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------
            #region Benefit Plus Gift Pending Request Sending
            //-----------------------------------------------------------------------------------------------------
            try
            {
                ///// Get_GHRequest();
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException.Message);
            }
            try
            {
                /////  Get_GVRequest();
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException.Message);
            }
            try
            {
                /////  Get_KindRequest();
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException.Message);
            }
            //-----------------------------------------------------------------------------------------------------
            #endregion
            //-----------------------------------------------------------------------------------------------------
            //Insert into Recipicent Award from Milestone Table
            //-----------------------------------------------------------------------------------------------------
            #region TransferData milestone activity stopped from portal
            //-----------------------------------------------------------------------------------------------------
            //////MailBase.LogData("Insert_RecipientAward_MileStone started.");
            //////try
            //////{
            //////    DAL_Common objCommon = new DAL_Common();
            //////    SqlParameter[] param = new SqlParameter[2];
            //////    param[0] = new SqlParameter("@AwardTypeName", ConfigurationSettings.AppSettings["MileStone_AwardType_Name"].ToString());
            //////    param[1] = new SqlParameter("@AwardAmount", ConfigurationSettings.AppSettings["MileStone_AwardAmount"].ToString());
            //////    objCommon.DAL_SaveData("usp_Insert_RecipientAward_MileStone", param);
            //////    Console.WriteLine("Inserting data into RecipientAwards ...");
            //////    Console.WriteLine();
            //////}
            //////catch (Exception ex)
            //////{
            //////    MailBase.LogData(ex.Message + ex.StackTrace);
            //////}
            //////MailBase.LogData("Insert_RecipientAward_MileStone ended.");
            //-----------------------------------------------------------------------------------------------------
            #endregion
            //-----------------------------------------------------------------------------------------------------
            MailBase.SendMail = ConfigurationSettings.AppSettings["sendMail"] == "0" ? false : true;
            MailBase.SendMailTo = ConfigurationSettings.AppSettings["sendMailTo"].ToString();
            MailBase objMailBase;
            //-----------------------------------------------------------------------------------------------------
            #region send Coffee with Boss Remainder Mails
            //-----------------------------------------------------------------------------------------------------
            //Commented by Rakesh
               //CoffeeRemainder();
            //-----------------------------------------------------------------------------------------------------
            #endregion
            //-----------------------------------------------------------------------------------------------------
               //-----------------------------------------------------------------------------------------------------
               #region Send Mails  --working
               //-----------------------------------------------------------------------------------------------------
            //To get today's day
            DayOfWeek Today = DateTime.Today.DayOfWeek;
            //  if (ConfigurationSettings.AppSettings["SendMailForRegularAwards"] == "1" && (Today != DayOfWeek.Sunday || Today != DayOfWeek.Saturday))
            //-----------------------------------------------------------------------------------------------------
            #region regular award --working
            //-----------------------------------------------------------------------------------------------------
            if (ConfigurationSettings.AppSettings["SendMailForRegularAwards"] == "1")
            {
                MailBase.LogData("SendMailForRegularAwards started");
                try
                {
                    Console.WriteLine("Sending 2 Days Congratulatory Mails...");
                    objMailBase = new Congo_3Days();
                    objMailBase.ProcessAndSendMails();
                    AllowReadLine();
                }
                catch (Exception obj_Exception)
                {
                    MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
                    Console.WriteLine(obj_Exception.Message);
                }
                //try
                //{
                //    Console.WriteLine("Sending Award Expiry Mails...");
                //    objMailBase = new Alert_AwardExpiry();
                //    objMailBase.ProcessAndSendMails();
                //    AllowReadLine();
                //}
                //catch (Exception obj_Exception)
                //{
                //    MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
                //    Console.WriteLine(obj_Exception.Message);
                //}
                MailBase.LogData("SendMailForRegularAwards Ended");
            }
            //-----------------------------------------------------------------------------------------------------

            #endregion

            #region Send Milestone utility stopped
            //-----------------------------------------------------------------------------------------------------
            ////////if (ConfigurationSettings.AppSettings["SendMailForMilestoneAwards"] == "1")
            ////////{
            ////////    MailBase.LogData("SendMailForMilestoneAwards started");
            ////////    try
            ////////    {
            ////////        MailBase.LogData("SendMailForMilestoneAwards(should be 1):" + ConfigurationSettings.AppSettings["SendMailForMilestoneAwards"]);
            ////////        MailBase.LogData("Sending MileStone Alert 1 to HOD started(20 Days).");
            ////////        Console.WriteLine("Sending MileStone Alert 1 to HOD...");
            ////////        Alert_MileStone_T15.MileStone_AlertNo = "1";
            ////////        objMailBase = new Alert_MileStone_T15();
            ////////        objMailBase.ProcessAndSendMails();
            ////////        AllowReadLine();
            ////////        MailBase.LogData("Sending MileStone Alert 1 to HOD ended(10 Days).");
            ////////    }
            ////////    catch (Exception obj_Exception)
            ////////    {
            ////////        MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
            ////////        Console.WriteLine(obj_Exception.Message);
            ////////    }
            ////////    try
            ////////    {
            ////////        MailBase.LogData("SendMailForMilestoneAwards(should be 1):" + ConfigurationSettings.AppSettings["SendMailForMilestoneAwards"]);
            ////////        MailBase.LogData("Sending MileStone Alert 1 to HOD ended(20 Days).");
            ////////        MailBase.LogData("Sending MileStone Alert 1 to HOD started(10 Days).");
            ////////        Console.WriteLine("Sending MileStone Alert 2 to HOD...");
            ////////        Alert_MileStone_T15.MileStone_AlertNo = "2";
            ////////        objMailBase = new Alert_MileStone_T15();
            ////////        objMailBase.ProcessAndSendMails();
            ////////        AllowReadLine();
            ////////        MailBase.LogData("Sending MileStone Alert 1 to HOD ended(10 Days).");
            ////////    }
            ////////    catch (Exception obj_Exception)
            ////////    {
            ////////        MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
            ////////        Console.WriteLine(obj_Exception.Message);
            ////////    }
            ////////    MailBase.LogData("SendMailForMilestoneAwards Ended");
            ////////}
            //-----------------------------------------------------------------------------------------------------
            #endregion

            #endregion

            #region send  Nudges by mailers report  --working
            //-----------------------------------------------------------------------------------------------------
            /*
            MailBase.LogData("send Nudges by mailers report started");
            try
            {
                if (DateTime.Today.Day == Convert.ToInt32(ConfigurationSettings.AppSettings["HODMonthlyReportDate"].ToString()))
                {
                    try
                    {
                        //objMailBase = new HOD_Monthly_Report();
                        //objMailBase.ProcessAndSendMails();
                        //  AllowReadLine();
                    }
                    catch (Exception obj_Exception)
                    {
                        MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
                        Console.WriteLine(obj_Exception.Message);
                    }
                }
                
            }
            catch (Exception obj_Exception)
            {
                MailBase.LogData("-----------Monthly Report---------- ");
                MailBase.LogData(DateTime.Now.Date.ToString("dd MMM yyyy"));
                MailBase.LogData("Start Time : " + DateTime.Now.ToString("hh:mm:ss tt"));
                MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
                Console.WriteLine(obj_Exception.Message);
            }
            MailBase.LogData("send monthly report Ended");
            */
            //-----------------------------------------------------------------------------------------------------
            #endregion
            
            //-----------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------
            #region Send Boss Day Mail
            //-----------------------------------------------------------------------------------------------------
            MailBase.LogData("Send Boss Day Mail started");
            if (ConfigurationSettings.AppSettings["SendMailForBossDay"].ToString() == "1")
            {
                try
                {
                    DateTime RunOn = new DateTime(Convert.ToInt16(ConfigurationSettings.AppSettings["BDYear"]), Convert.ToInt16(ConfigurationSettings.AppSettings["BDMonth"]), Convert.ToInt16(ConfigurationSettings.AppSettings["BDDay"]));
                    if (DateTime.Today == RunOn)
                    {
                        //objMailBase = new BossDay_Celebration();
                       ///* objMailBase.ProcessAndSendMails();
                        //AllowReadLine();
                    }
                }
                catch (Exception obj_Exception)
                {
                    MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
                    Console.WriteLine(obj_Exception.Message);
                }
            }
            MailBase.LogData("Send Boss Day Mail Ended");
            //-----------------------------------------------------------------------------------------------------
            #endregion
            //-----------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------

            //-----------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------
            #region Cleaning
            //-----------------------------------------------------------------------------------------------------
            MailBase.LogData("Cleaning started");
            if ((args.Length > 0 && args[0].ToString().ToLower() == "d") || ConfigurationSettings.AppSettings["AllowZip"].ToString() == "1")
            {
                MailBase.LogData("---------------------");
                MailBase.LogData("Cleaning process started : " + DateTime.Now.ToString("hh:mm:ss tt"));
                Console.WriteLine("---------------------");
                Console.WriteLine("Cleaning process started ...");
                MailBase.HouseCleaning();
            }
            //-----------------------------------------------------------------------------------------------------
            #endregion
            //-----------------------------------------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------------------
            #region Log End
            //-----------------------------------------------------------------------------------------------------
            MailBase.LogData("End Time : " + DateTime.Now.ToString("hh:mm:ss tt"));
            MailBase.LogData("");
            //-----------------------------------------------------------------------------------------------------
            #endregion
            //-----------------------------------------------------------------------------------------------------
        }
        //-----------------------------------------------------------------------------------------------------
        private static void CoffeeRemainder()
        {
            MailBase.LogData("CoffeeRemainder() started");
            MailBase objMailBase1 = null;
            try
            {
                //objMailBase1 = new CoffeeWithBoss();
                //objMailBase1.ProcessAndSendMails();
                //AllowReadLine();
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException.ToString());
            }
            finally
            {
                if (objMailBase1 != null)
                {
                    objMailBase1 = null;
                }
            }
            MailBase.LogData("CoffeeRemainder() Ended");
        }
        //-----------------------------------------------------------------------------------------------------
        private static void AllowReadLine()
        {
            if (ConfigurationSettings.AppSettings["AllowReadLine"].ToString() == "1")
            {
                Console.WriteLine("Press enter to continue ... ");
                Console.ReadLine();
            }
        }
        //----------------------------------Send the Pending requests------------------------------------------ 
        /*private static void Get_GHRequest()
        {
            try
            {
                DAL_Common objCommon = new DAL_Common();
                DataSet ds = new DataSet();
                // BE_AwardManagement objAwardManagement = new BE_AwardManagement();
                SqlParameter[] objsqlparam = new SqlParameter[0];
                ds = objCommon.DAL_GetData("usp_Get_GHPendingRequest", objsqlparam);
                MahindraAwardService objGiftHamper = new MahindraAwardService();
                BE_GiftHampers objBEGiftHampers = new BE_GiftHampers();
                try
                {
                    MailBase.LogData("----------- Spot Gift Hamper Award start---------- ");
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objBEGiftHampers.RequestId = dr["Request_Id"].ToString();
                        objBEGiftHampers.RequestDate = dr["Request_Date"].ToString();
                        objBEGiftHampers.NomName = dr["Nom_Name"].ToString();
                        objBEGiftHampers.NomEmpId = dr["Nom_EmpId"].ToString();
                        objBEGiftHampers.RecName = dr["Rec_Name"].ToString();
                        objBEGiftHampers.RecEmpId = dr["Rec_EmpId"].ToString();
                        objBEGiftHampers.DeliveryAddress = dr["Delivery_Address"].ToString();
                        objBEGiftHampers.Pincode = dr["PinCode"].ToString();
                        objBEGiftHampers.GiftAmount = dr["Gift_Amount"].ToString();
                        objBEGiftHampers.RequestStatus = dr["Request_Status"].ToString();
                        objBEGiftHampers.NomEmailID = dr["nomEmail"].ToString();
                        objBEGiftHampers.RecEmailID = dr["recEmail"].ToString();
                        /////  string str = objGiftHamper.SendRequestDetails(objBEGiftHampers);
                        /////  MailBase.LogData(objBEGiftHampers.RequestId + "=" + str);
                        /////  Console.WriteLine(objBEGiftHampers.RequestId + "=" + str);
                    }
                    MailBase.LogData("----------- Spot Gift Hamper Award End---------- ");
                }
                catch (Exception obj_Exception)
                {
                    MailBase.LogData("-----------Spot Gift Hamper Award Error---------- ");
                    MailBase.LogData(DateTime.Now.Date.ToString("dd MMM yyyy"));
                    MailBase.LogData("Start Time : " + DateTime.Now.ToString("hh:mm:ss tt"));
                    MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
                    Console.WriteLine(obj_Exception.Message);
                }
                finally
                {
                    if (objBEGiftHampers != null)
                    {
                        objBEGiftHampers = null;
                    }
                    if (objGiftHamper != null)
                    {
                        objGiftHamper = null;
                    }
                }
            }
            catch (Exception obj_Exception)
            {
                MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
                Console.WriteLine(obj_Exception.Message);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        private static void Get_GVRequest()
        {
            DAL_Common objCommon = new DAL_Common();
            DataSet ds = new DataSet();
            // BE_AwardManagement objAwardManagement = new BE_AwardManagement();
            SqlParameter[] objsqlparam = new SqlParameter[0];
            ds = objCommon.DAL_GetData("usp_Get_GVPendingRequest", objsqlparam);
            MahindraAwardService objGiftHamper = new MahindraAwardService();
            BE_GiftVouchers objBEGiftVouchers = new BE_GiftVouchers();
            try
            {
                MailBase.LogData("----------- Spot Voucher Award Start---------- ");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBEGiftVouchers.RequestId = dr["Request_Id"].ToString();
                    objBEGiftVouchers.RequestDate = dr["Request_Date"].ToString();
                    objBEGiftVouchers.NomName = dr["Nom_Name"].ToString();
                    objBEGiftVouchers.NomEmpId = dr["Nom_EmpId"].ToString();
                    objBEGiftVouchers.NomEmail = dr["Nom_Email"].ToString();
                    objBEGiftVouchers.RecName = dr["Rec_Name"].ToString();
                    objBEGiftVouchers.RecEmpId = dr["Rec_EmpId"].ToString();
                    objBEGiftVouchers.RecEmail = dr["Rec_Email"].ToString();
                    objBEGiftVouchers.AwardBusinessUnit = dr["Award_BusinessUnit"].ToString();
                    objBEGiftVouchers.AwardDivision = dr["Award_Division"].ToString();
                    objBEGiftVouchers.AwardLocation = dr["Award_Location"].ToString();
                    objBEGiftVouchers.GiftAmount = dr["Gift_Amount"].ToString();
                    objBEGiftVouchers.RequestStatus = dr["Request_Status"].ToString();
                    string str = objGiftHamper.SendVoucherRequestDetails(objBEGiftVouchers);
                    MailBase.LogData(objBEGiftVouchers.RequestId + "=" + str);
                    Console.WriteLine(objBEGiftVouchers.RequestId + "=" + str);
                }
                MailBase.LogData("----------- Spot Voucher Award End---------- ");
            }
            catch (Exception obj_Exception)
            {
                MailBase.LogData("-----------Spot Voucher Award Error---------- ");
                Console.WriteLine(obj_Exception.Message);
            }
            finally
            {
                if (objBEGiftVouchers != null)
                {
                    objBEGiftVouchers = null;
                }
                if (objGiftHamper != null)
                {
                    objGiftHamper = null;
                }
            }
        }
        //-----------------------------------------------------------------------------------------------------
        private static void Get_KindRequest()
        {
            DAL_Common objCommon = new DAL_Common();
            DataSet ds = new DataSet();
            SqlParameter[] objsqlparam = new SqlParameter[0];
            ds = objCommon.DAL_GetData("usp_Get_KindPendingRequest", objsqlparam);
            MahindraAwardService objSpotKind = new MahindraAwardService();
            BE_SpotKind objBESpotKind = new BE_SpotKind();
            try
            {
                MailBase.LogData("----------- Spot Kind Award ---------- ");
                MailBase.LogData("----------- Spot Kind Award Start---------- ");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    objBESpotKind.RequestId = dr["Request_Id"].ToString();
                    objBESpotKind.RequestDate = dr["Request_Date"].ToString();
                    objBESpotKind.NomName = dr["Nom_Name"].ToString();
                    objBESpotKind.NomEmpId = dr["Nom_EmpId"].ToString();
                    objBESpotKind.NomEmailID = dr["Nom_Email"].ToString();
                    objBESpotKind.RecName = dr["Rec_Name"].ToString();
                    objBESpotKind.RecEmpId = dr["Rec_EmpId"].ToString();
                    objBESpotKind.RecEmailID = dr["Rec_Email"].ToString();
                    objBESpotKind.AwardBusinessUnit = dr["Award_BusinessUnit"].ToString();
                    objBESpotKind.AwardDivision = dr["Award_Division"].ToString();
                    objBESpotKind.AwardLocation = dr["Award_Location"].ToString();
                    objBESpotKind.GiftAmount = dr["Gift_Amount"].ToString();
                    objBESpotKind.RequestStatus = dr["Request_Status"].ToString();
                    string str = objSpotKind.SendRequestDetailsKind(objBESpotKind);
                    MailBase.LogData(objBESpotKind.RequestId + "=" + str);
                    Console.WriteLine(objBESpotKind.RequestId + "=" + str);
                }
                MailBase.LogData("----------- Spot Kind Award End---------- ");
            }
            catch (Exception obj_Exception)
            {
                MailBase.LogData("-----------Spot Kind Award Error---------- ");
                MailBase.LogData(obj_Exception.Message + obj_Exception.StackTrace);
            }
            finally
            {
                if (objBESpotKind != null)
                {
                    objBESpotKind = null;
                }
                if (objSpotKind != null)
                {
                    objSpotKind = null;
                }
            }
        }
        */
    }
}

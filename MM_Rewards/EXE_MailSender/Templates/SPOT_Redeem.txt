﻿<html>
<head>
    <title>Spot Redeem</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <table id="Table_01" width="1000" height="1415" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_01.jpg" width="1000" height="207"
                    alt="">
            </td>
        </tr>
        <tr>
            <td>
                <img style="vertical-align: top" src="images/Emailer_02.jpg" width="87" height="436"
                    alt="">
            </td>
            <td style="width: 794px; vertical-align: top">
                <div style="width: 785px; height: 417px; font-family: arial; font-size: 17px; color: #323231;
                    padding: 12px 0 7px 7px; line-height: 35px;">
                    <span style="font-weight: bold; color: rgb(169, 50, 52); font-size: 42px; text-transform:capitalize;">Dear
                        <%RecipientName%>,</span><br />
                    <span style="display: block; font-size: 27px; color: #000; padding-top: 22px">Congratulations!!!</span><p
                        style="color: #606062; font-size: 22px">
                        On having received the <%AwardType%> award from <%GiverHODName%>.</p>
                    <p>
                        <%reason%>
                    </p>
                </div>
            </td>
            <td>
                <img style="vertical-align: top; float: right" src="images/Emailer_04.jpg" width="121"
                    height="436" alt="">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="images/Emailer_05.jpg" alt="" width="1000" height="288" usemap="#Map" style="vertical-align: top"
                    border="0">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_06.jpg" width="1000" height="184"
                    alt="">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img style="vertical-align: top" src="images/Emailer_07.jpg" width="1000" height="300"
                    alt="">
            </td>
        </tr>
    </table>
    <map name="Map">
         <area shape="rect" coords="387,50,483,74" target="_blank" href='<%REDEEMLINK%>'>
        <area shape="rect" coords="447,76,540,98" target="_blank" href='<% MyAwards%>'>
        <area shape="rect" coords="468,164,560,186" target="_blank" href='<%VLINK%>'>
    </map>
</body>
</html>

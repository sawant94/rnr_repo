﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MM.DAL;
using System.Data.SqlClient;

namespace BudgetAutomation
{
    class Program
    {
        static int countbhp = 0;
        static void Main(string[] args)
        {

             DataSet dsEmail = new DataSet();
                DAL_Common objCommon = new DAL_Common();               
                dsEmail = objCommon.DAL_GetData("usp_GetHODForBudget");
                    foreach (DataRow dr in dsEmail.Tables[0].Rows)
                    {
                        string TokenNumber = dr["TokenNumber"].ToString();
                        countbhp = 0;
                        int count500tobeinsert = 0;
                        if (Convert.ToInt32(dr["750BHP"].ToString()) < 9)
                        {
                            count500tobeinsert = GetBHPCount(TokenNumber, 500);
                            count500tobeinsert = count500tobeinsert + Convert.ToInt32(dr["RW"].ToString());
                        }
                        countbhp = 0;
                         int count750tobeinsert=0;
                        if (Convert.ToInt32(dr["750BHP"].ToString()) < 8)
                        {
                             count750tobeinsert = GetBHPCount(TokenNumber, 750);
                             count750tobeinsert = count750tobeinsert + Convert.ToInt32(dr["RW"].ToString());
                        }
                        int count1000tobeinsert = 0;
                         countbhp = 0;
                         if (Convert.ToInt32(dr["1000BHP"].ToString()) < 5)
                         {
                              count1000tobeinsert = GetBHPCount(TokenNumber, 1000);
                              count1000tobeinsert = count1000tobeinsert + Convert.ToInt32(dr["RW"].ToString());
                         }

                         int countCEtobeinser = 0;
                         countbhp = 0;
                         if (Convert.ToInt32(dr["1000BHP"].ToString()) < 3)
                         {
                             countCEtobeinser = GetBHPCount(TokenNumber, 100);
                             countCEtobeinser = countCEtobeinser + Convert.ToInt32(dr["RW"].ToString());
                         }

                         countbhp = 0;
                         int countspottobeinsert = 0;
                         if (Convert.ToInt32(dr["750BHP"].ToString()) < 9)//level
                         {
                             countspottobeinsert = GetBHPCount(TokenNumber, 600);
                             countspottobeinsert = countspottobeinsert + Convert.ToInt32(dr["RW"].ToString());
                         }


                         SqlParameter[] param = new SqlParameter[7];
                         param[0] = new SqlParameter("@TokenNumber", dr["TokenNumber"].ToString());
                         param[1] = new SqlParameter("@RW",  Convert.ToInt32(dr["RW"].ToString()));
                         param[2] = new SqlParameter("@EX500BHP", count500tobeinsert); //Convert.ToInt32(dr["500BHP"].ToString()));
                         param[3] = new SqlParameter("@EX750BHP", count750tobeinsert);
                         param[4] = new SqlParameter("@EX1000BHP", count1000tobeinsert);
                         param[5] = new SqlParameter("@CEBudget", countCEtobeinser);
                         param[6] = new SqlParameter("@SpotAward", countspottobeinsert);// Convert.ToInt32(dr["SpotAward"].ToString()));
                         objCommon.DAL_SaveData("usp_InsertReviewBudget", param);
                    }
        }

        private static int GetBHPCount(string TokenNumber,int bhppoints)
        {
            DataSet dsreportiesbhp = new DataSet();
            DAL_Common objCommon = new DAL_Common();
             SqlParameter[] param = new SqlParameter[2];
             param[0] = new SqlParameter("@HODToken", TokenNumber);
             param[1] = new SqlParameter("@BHPPoints", bhppoints);
             dsreportiesbhp = objCommon.DAL_GetData("usp_Get750BHPRW", param);           
             if (dsreportiesbhp.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsreportiesbhp.Tables[0].Rows)
                {
                   countbhp =  GetReportiesCount(dr["TokenNumber"].ToString());
                }
            }
            return countbhp;
        }


        private static int GetReportiesCount(string p)
        {
            DataSet dsSubemployeerw = new DataSet();
            DAL_Common objCommon = new DAL_Common();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@HODToken", p);
            dsSubemployeerw = objCommon.DAL_GetData("usp_GetReporteesCount", param);
            countbhp = countbhp + dsSubemployeerw.Tables[0].Rows.Count;

            if (dsSubemployeerw.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSubemployeerw.Tables[0].Rows)
                {
                    GetReportiesCount(dr["TokenNumber"].ToString());
                }
            }
            return countbhp;            
        }
    }
}

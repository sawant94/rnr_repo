﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Mail;

namespace Liberary_MailSender
{
    public class MailDetails
    {
        public string ID { get; set; }
        public string FromID{ get;set;}
        public string ToID{get;set;}
        public string CCID{get;set;}
        public string CCEmails { get; set; }/****Created by Kiran on 02 july 2013 for the requirement of sending CC Mails to HOD for SPOT and EXCELLATOR Awards Only.For all the other award types, this field is not used.**/
        public string SenderName { get; set; }
        public string RecipientName{get;set;}
        public string Subject{get;set;}
        public string Body{get;set;}
        public string SMTPServer{get;set;}
        public string Status { get; set; }
        public Attachment Attachments { get; set; }
       
    }
}

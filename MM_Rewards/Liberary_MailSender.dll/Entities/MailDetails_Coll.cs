﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Liberary_MailSender.Classes
{
    public class MailDetails_Coll :List<MailDetails>
    {
       private List<string> sendSuccessfull = new List<string>();
       private List<string> sendFailed = new List<string>();

        public List<string> SendSuccessfull
        {
            get { return sendSuccessfull; }
        }

        public List<string> SendFailed
        {
            get { return sendFailed; }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using Liberary_MailSender.Classes;
using System.IO.Compression;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using EXE_MailSender;
namespace Liberary_MailSender
{
    /// <summary>
    /// Abstract class for sending mail that has to be inherited into any other class that wants to send one or many mails
    /// </summary>
    ///
    public abstract class MailBase
    {
        #region fields
        private static bool sendMail = true;
        private static string sendMailTo = "";
        private static string template = "";
        private MailDetails_Coll recipientList = new MailDetails_Coll();
        // Neil
        public bool invalid = false;
        #endregion
        #region Properties
        /// <summary>
        /// Switches on or off the sending or thetriggering of the mail. true:ON ,false:OFF
        /// </summary>
        public static bool SendMail
        {
            get { return sendMail; }
            set { sendMail = value; }
        }
        /// <summary>
        /// If specified and SendMail is true,all mails are triggered to these EmailIDs
        /// </summary>
        public static string SendMailTo
        {
            get { return sendMailTo; }
            set { sendMailTo = value; }
        }
        /// <summary>
        /// The Path where the folder that contains the Log files will be created if specified
        /// </summary>
        public static string LogDirPath { get; set; }
        /// <summary>
        /// The name of the file where the data will be logged
        /// </summary>
        public static string LogFileName { get; set; }
        /// <summary>
        /// The path where the file containing the html template for the body will be stored
        /// </summary>
        protected virtual string TemplatePath { get; set; }
        /// <summary>
        /// The name of the template file
        /// </summary>
        protected virtual string TemplateFileName { get; set; }
        protected static string TemplateFileName1
        {
            get { return template; }
            set { template = value; }
        }
        /// <summary>
        /// Holds the collection of reciepient to whom the mail has to be triggered
        /// </summary>
        public virtual MailDetails_Coll RecipientList
        {
            get { return recipientList; }
            set { recipientList = value; }
        }
        #endregion
        /// <summary>
        /// Gets the body using the TemplateFileName and TemplateFilePath properties as a string
        /// </summary>
        /// <returns>string</returns>
        protected virtual string GetBody()
        {
            string BodyPart = "";
            try
            {
                StreamReader sr = new StreamReader(TemplatePath + TemplateFileName);
                StringBuilder sb = new StringBuilder();
                BodyPart = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return BodyPart;
        }
        /// <summary>
        /// Sends Mail to all the recipients contained in the property collection RecipientList
        /// </summary>
        /// <returns>MailDetails_Coll</returns>
        protected virtual MailDetails_Coll SendMails()
        {
            try
            {
                LogData("----------MailDetails_Coll sendmail try block starts-------------");
                if (RecipientList != null)
                {
                    int i = 0;
                    foreach (MailDetails objMailDetails in RecipientList)
                    {

                        try
                        {
                            i++;
                            try
                            {
                                if (Send(objMailDetails))
                                {
                                    if (!string.IsNullOrEmpty(objMailDetails.ID))
                                    {
                                        try
                                        {
                                            CheckEmail(objMailDetails.ToID);
                                            RecipientList.SendSuccessfull.Add(objMailDetails.ID);
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine(ex.Message);
                                            LogData("---------------------");
                                            LogData("Invalid Email iD" + objMailDetails.ToID);
                                            LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                                            throw new Exception("", ex);
                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(objMailDetails.ToID))
                                    {
                                        //code by neil on 12-02-2015 for checking email
                                        try
                                        {
                                            CheckEmail(objMailDetails.ToID);
                                            RecipientList.SendSuccessfull.Add(objMailDetails.ID);
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine(ex.Message);
                                            LogData("---------------------");
                                            LogData("Invalid Email iD" + objMailDetails.ToID);
                                            LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                                            throw new Exception("", ex);
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        CheckEmail(objMailDetails.ToID);
                                        RecipientList.SendFailed.Add(objMailDetails.ToID);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                        LogData("---------------------");
                                        LogData("Invalid Email iD" + objMailDetails.ToID);
                                        LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                                        throw new Exception("", ex);
                                    }
                                    //if (!string.IsNullOrEmpty(objMailDetails.ID))
                                    //{ 
                                    //    RecipientList.SendFailed.Add(objMailDetails.ID);
                                    //}
                                    //else if (!string.IsNullOrEmpty(objMailDetails.ToID))
                                    //{
                                    //    //code by neil on 12-02-2015 for checking email
                                    //    try
                                    //    {
                                    //        CheckEmail(objMailDetails.ToID);
                                    //        RecipientList.SendFailed.Add(objMailDetails.ToID);
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        Console.WriteLine(ex.Message);
                                    //        LogData("---------------------");
                                    //        LogData("Invalid Email iD" + objMailDetails.ToID);
                                    //        LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                                    //        throw new Exception("", ex);
                                    //    }
                                    //}
                                }
                            }
                            catch (Exception ex)
                            {

                                Console.WriteLine(ex.Message);
                                LogData("---------------------");
                                LogData("--- ID for mail not send ------" + objMailDetails.ID + "---------------");
                                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                                throw new Exception("", ex);
                            }

                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                            LogData("---------------------");
                            LogData(" 5 Exception for MailBase.cs - SendMails");
                            LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                            throw new Exception("", ex);
                        }
                    }
                }
                LogData("----------MailDetails_Coll sendmail try block Ends-------------");
                return RecipientList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogData("---------------------");
                LogData("6 Exception for MailBase.cs - SendMails");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                throw new Exception("", ex);
            }
        }
        /// <summary>
        /// an abstract method,has to be overridden by the child class,
        /// in this method,the developer is expected to do the following
        /// Set the properties TemplateFilePath,TemplateFileName,ReciepientList and call the SendMails method
        /// </summary>
        /// <param name="strTemplatePath"></param>
        /// <param name="strTemplateFileName"></param>
        public abstract void ProcessAndSendMails(string strTemplatePath = "", string strTemplateFileName = "");
        //This Code is made by Neil for email ID validation
        public void CheckEmail(string VerifyEmail)
        {
            try
            {
                RegexUtilities util = new RegexUtilities();
                string[] emailAddresses = { VerifyEmail };
                foreach (var emailAddress in emailAddresses)
                {
                    if (util.IsValidEmail(emailAddress))
                    {
                        LogData("Valid: {0}" + emailAddress);
                    }
                    else
                    {
                        LogData("Invalid: {0}" + emailAddress);
                    }
                }
            }
            catch (Exception ex)
            {
                LogData("Email Validate Function" + ex.Message + " - " + ex.StackTrace);
            }
            // Console.ReadKey();
        }
        /// <summary>
        /// Sends mail using the property values of the object of the MailDetails class
        /// </summary>
        /// <param name="objSendMail"></param>
        /// <returns> bool </returns>
        ///
        public bool Send(MailDetails objSendMail)
        {
            try
            {
                if (sendMail) // if sendmail from config is true
                {
                    try
                    {
                        string ToReciepientIfPresent = string.IsNullOrEmpty(sendMailTo) ? "" : sendMailTo;
                        ToReciepientIfPresent = ToReciepientIfPresent.Replace(';', ',');
                        if (!string.IsNullOrEmpty(ToReciepientIfPresent)) // if reciepient from Config file
                        {
                            objSendMail.ToID = ToReciepientIfPresent.Replace(';', ',');
                            objSendMail.CCID = string.IsNullOrEmpty(objSendMail.CCID) ? "" : ToReciepientIfPresent.Replace(';', ',');
                        }
                        bool blnSuccess = false;
                        MailMessage mailMsg = new MailMessage();

                        MailAddress fromAddr = new MailAddress(objSendMail.FromID.Trim(';'), objSendMail.SenderName);  // Change by Gaurav 12-5-14
                        //MailAddress fromAddr = new MailAddress("puneet_poojari@idealake.com");//, objSendMail.SenderName);
                        mailMsg.IsBodyHtml = true;
                        mailMsg.From = fromAddr;
                        //mailMsg.To.Add(new MailAddress(objSendMail.ToID.ToString(), objSendMail.RecipientName));
                        mailMsg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        if (objSendMail.Attachments != null) {

                            mailMsg.Attachments.Add(objSendMail.Attachments);
                           
                        }
                        // if (objSendMail.ToID.Contains(";"))
                        // {
                        try
                        {
                            mailMsg.To.Add(new MailAddress(objSendMail.ToID.ToString(), objSendMail.RecipientName));
                           // mailMsg.To.Add(new MailAddress(objSendMail.CCEmails.ToString(), objSendMail.RecipientName));
                        }
                        catch (Exception ex)
                        {
                            // LogData("This is contiune process for email failure");
                            //LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                        }
                        // }
                        // mailMsg.To.Add(objSendMail.ToID.Replace(';', ','));
                        //mailMsg.To.Add("kiran_mahale@idealake.com");
                        if (!string.IsNullOrEmpty(objSendMail.CCID))
                        {
                            //mailMsg.CC.Add(objSendMail.CCID.Replace(';', ','));
                        }
                        mailMsg.Subject = objSendMail.Subject;
                        mailMsg.Body = objSendMail.Body;
                        //
                        //CheckEmail();
                        //if (!string.IsNullOrEmpty(objSendMail.CCEmails))
                        //{
                        //    mailMsg.CC.Add(objSendMail.CCEmails.Trim(';'));   // Change by Gaurav
                        //}
                        if (!string.IsNullOrEmpty(objSendMail.CCEmails))
                        {
                            string[] objCCEmails = objSendMail.CCEmails.Split(',');
                            foreach (string strCCEmails in objCCEmails)
                            {
                                if (strCCEmails.Trim() != string.Empty)
                                {
                                    //LogData("CC Emails------------------");
                                    //LogData(strCCEmails);
                                    try
                                    {
                                        mailMsg.CC.Add(strCCEmails);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogData(ex.Message + " - " + ex.StackTrace);
                                        LogData("CC Not Send :- " + strCCEmails);
                                    }
                                    // mailMsg.CC.Add(strCCEmails);
                                }
                            }
                        }

                        string SMTPServer = objSendMail.SMTPServer;
                        SmtpClient objSmtpClient = new SmtpClient(SMTPServer);
                        try
                        {
                            if (Convert.ToString(mailMsg).ToString() == "")
                            {
                                LogData("--------Convert.ToString(mailMsg).ToString()------");
                            }
                            else
                            {
                                objSmtpClient.Send(mailMsg);
                                objSendMail.Status = "1";
                                blnSuccess = true;
                            }
                        }
                        catch (Exception GeneralEx)
                        {
                            LogData("---------------------");
                            LogData("Mail Not Send for :" + objSendMail.ToID.ToString());
                            LogData("1 Exception for MailBase - objSmtpClient.Send()");
                            LogData(GeneralEx.Message + Environment.NewLine + GeneralEx.StackTrace);
                            // objSendMail.Status = GeneralEx.Message;
                        }
                        finally
                        {
                            objSmtpClient = null;
                            mailMsg = null;
                            fromAddr = null;
                        }
                        // LogData("----------MailDetails_Coll Send(MailDetails objSendMail try block Ends-------------");
                        return blnSuccess;
                    }

                    catch (Exception ex)
                    {
                        LogData("---------------------");
                        LogData("2 Message From Send(MailDetails objSendMail");
                        LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                        throw new Exception("Message From Send(MailDetails objSendMail) : ", ex);
                    }
                }
                else // if sendmail from config is false
                {
                    objSendMail.Status = "Mail sending switched off";
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogData("---------------------");
                LogData(" 3 Message From Send(MailDetails objSendMail");
                LogData(ex.Message + Environment.NewLine + ex.StackTrace);
                throw new Exception("Message From Send(MailDetails objSendMail) : ", ex);
            }
        }
        #region Logging
        /// <summary>
        /// Writes the provided string on to a file at the LogDirPath with name Property LogFileName if specified
        /// </summary>
        /// <param name="LogMsg"></param>
        public static void LogData(string LogMsg)
        {
            StreamWriter logFile;
        A:
            try
            {
                string FilePath = LogDirPath + "\\" + LogFileName;
                if (!File.Exists(FilePath.ToString()))
                {
                    logFile = new StreamWriter(FilePath);
                }
                else
                {
                    logFile = File.AppendText(FilePath.ToString());
                }
                logFile.WriteLine(LogMsg);
                logFile.Close();
            }
            catch (Exception ex)
            {
                goto A;
            }
        }
        #endregion

        #region HouseCleaning
        /// <summary>
        /// Zips the earlier .txt Files(Log Files in case) at the given path and dumps them into a folder
        /// </summary>
        /// <param name="dirpath"></param> Path to directory of files to compress and decompress.
        public static void HouseCleaning()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(LogDirPath);
                string Log_Zip = LogDirPath + "\\Log_Zip";
                bool compressedFileExists = false;
                // Compress the directory's .txt files.
                foreach (FileInfo fi in di.GetFiles("*.txt"))
                {
                    string FileName = DateTime.Now.Date.ToString("MMM")
                    + "_" + DateTime.Now.Date.Year + ".txt";
                    // exclude the current file
                    if (fi.Name != FileName)
                    {
                        //Compress file
                        Compress(fi);
                        compressedFileExists = true;
                        // delete the original file
                        File.Delete(fi.FullName);
                    }
                }
                //if zip files exists create a folder to dump them if it is already not existing
                if (!System.IO.Directory.Exists(Log_Zip) && compressedFileExists)
                {
                    System.IO.Directory.CreateDirectory(Log_Zip);
                }
                // Move all zip files(excluding the current file) into the Zip Folder
                foreach (FileInfo fi in di.GetFiles("*.gz"))
                {
                    if (!File.Exists(Log_Zip + "\\" + fi.Name))
                    {
                        File.Move(fi.FullName, Log_Zip + "\\" + fi.Name);
                    }
                    else  // if a file with the same name already exists in the zip folder change the name
                    {
                        int count = 2;
                        while (File.Exists(Log_Zip + "\\" + fi.Name.Replace(".gz", "__[Part" + count + "].gz")))
                        {
                            count++;
                        }
                        File.Move(fi.FullName, Log_Zip + "\\" + fi.Name.Replace(".gz", "__[Part" + count + "].gz"));
                    }
                }
            }
            catch (Exception ex)
            {
                LogData(ex.Message);
            }
        }
        public static void Compress(FileInfo fi)
        {
            //FileStream outFile = new FileStream();
            // Get the stream of the source file.
            using (FileStream inFile = fi.OpenRead())
            {
                // Prevent compressing hidden and
                // already compressed files.
                if ((File.GetAttributes(fi.FullName)
                & FileAttributes.Hidden)
                != FileAttributes.Hidden & fi.Extension != ".gz")
                {
                    // Create the compressed file.
                    using (FileStream outFile = File.Create(fi.FullName + ".gz"))
                    {
                        using (GZipStream Compress =
                        new GZipStream(outFile,
                        CompressionMode.Compress))
                        {
                            // Copy the source file into
                            // the compression stream.
                            inFile.CopyTo(Compress);
                            Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                            fi.Name, fi.Length.ToString(), outFile.Length.ToString());
                        }
                    }
                }
            }
        }
        public static void Decompress(FileInfo fi)
        {
            // Get the stream of the source file.
            using (FileStream inFile = fi.OpenRead())
            {
                // Get original file extension, for example
                // "doc" from report.doc.gz.
                string curFile = fi.FullName;
                string origName = curFile.Remove(curFile.Length -
                fi.Extension.Length);
                //Create the decompressed file.
                using (FileStream outFile = File.Create(origName))
                {
                    using (GZipStream Decompress = new GZipStream(inFile,
                    CompressionMode.Decompress))
                    {
                        // Copy the decompression stream
                        // into the output file.
                        Decompress.CopyTo(outFile);
                        Console.WriteLine("Decompressed: {0}", fi.Name);
                    }
                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AwardWinnerDetailsRISE
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Program program = new Program();
                program.ErrorLog("AwardWinnersDetailsTransfer Utility Starts", "");

                string SPdate = DateTime.Today.ToString("yyyy-MM-dd");
                program.TransferAwardWinners(SPdate);

                program.ErrorLog("AwardWinnersDetailsTransfer Utility Ends", "");
            }
            catch (Exception ex)
            {
                Program program = new Program();
                program.ErrorLog("Main Method :", ex.ToString());
            }
        }

        public void TransferAwardWinners(string date)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MM_Connection"].ConnectionString))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("USP_Get_AwardWinnerRISE", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@CurrentDate", date);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string Id;
                        string AwardedBy;
                        string AwardedTo;
                        string Comment;
                        string AwardedDate;
                        //string institution;
                        string RISETransfer;

                        while (reader.Read())
                        {
                            Id = reader["Id"].ToString();
                            AwardedBy = reader["AwardedBy"].ToString();
                            AwardedTo = reader["AwardedTo"].ToString();
                            Comment = reader["Comment"].ToString();
                            AwardedDate = reader["AwardedDate"].ToString();
                            //institution = reader["Description"].ToString();
                            RISETransfer = reader["RISETransfer"].ToString();

                            if (RISETransfer == "")
                            {
                                Program program = new Program();
                                program.ErrorLog("", "");

                                program.ErrorLog("RISE Transfer for", "AwardedBy : " + AwardedBy + "  AwardedTo : " + AwardedTo + "  Comment : " + Comment + "  AwardedDate : " + AwardedDate);
                                //program.ErrorLog("", "");

                                string responce = TransferAwardDetails(AwardedBy, AwardedTo, Comment, AwardedDate);
                                program.ErrorLog("RISE Transfer for responce", responce);

                                if (responce != "")
                                {
                                    UpdateAwardTransferStatus(Id, responce);
                                }
                            }
                        }

                    }
                }
            }
        }

        public void UpdateAwardTransferStatus(string Id, string responce)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MM_Connection"].ConnectionString))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("USP_Update_RISETransferStatus", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Id", Id);
                    command.Parameters.AddWithValue("@RISETransferStatus", responce);
                    int reader = command.ExecuteNonQuery();
                }
            }
        }

        private string TransferAwardDetails(string AwardedBy, string AwardedTo, string Comment, string AwardedDate)
        {
            string responce = "";
            try
            {

                WebClient webClient = new WebClient();
                webClient.QueryString.Add("AwardedBy", AwardedBy);
                webClient.QueryString.Add("AwardedTo", AwardedTo);
                webClient.QueryString.Add("Comment", Comment);
                webClient.QueryString.Add("AwardedDate", AwardedDate);
                responce = webClient.DownloadString("http://183.87.127.174/projects/mahindrarise/pushAwards");


                //AwardedBy=123&AwardedTo=123&Comment=test&AwardedDate=2017-06-07
  //              HttpWebRequest request = (HttpWebRequest)
  //WebRequest.Create(Convert.ToString(ConfigurationManager.AppSettings["BaseURL"]));
               

  //              request.Method = "POST";
                

  //              string json = new JavaScriptSerializer().Serialize(new
  //              {
  //                  AwardedBy = AwardedBy,
  //                  AwardedTo = AwardedTo,
  //                  Comment = Comment,
  //                  AwardedDate = AwardedDate
  //              });

                
  //              request.ContentType = "application/json; charset=UTF-8";
  //              request.Accept = "application/json";

                //try
                //{
                //    var streamWriter = new StreamWriter(request.GetRequestStream());
                //    streamWriter.Write(json);
                //}
                //catch (WebException ex)
                //{
                //    ErrorLog("request.GetRequestStream() Method :", ex.ToString());
                //}

                //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                //{
                //    streamWriter.Write(json);
                //}
                // grab te response and print it out to the console along with the status code

                //try
                //{
                //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //    using (var streamReader = new StreamReader(response.GetResponseStream()))
                //    {
                //        responce = streamReader.ReadToEnd();
                //    }
                //}
                //catch (WebException ex)
                //{
                //    ErrorLog("request.GetResponse() Method :", ex.ToString());
                //}

            }
            catch (WebException ex)
            {
                string pageContent = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd().ToString();
                ErrorLog("TransferAwardDetails Method :", ex.ToString());
            }
            return responce;
        }



        public void ErrorLog(string methodName, string message)
        {
            StreamWriter sw = null;

            try
            {
                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                //string sPathName = @"C:\\MM_Rewards\\ReddempedLog\\";

                //string sPathName = @"E:\\MM_Rewards_Folders\\MM_Rewards\\PushPointKwench\\";
                string sPathName = ConfigurationManager.AppSettings["LogFolderPath"];

                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string sErrorTime = sDay + "-" + sMonth + "-" + sYear;

                if (!Directory.Exists(sPathName))
                    Directory.CreateDirectory(sPathName);

                sw = new StreamWriter(sPathName + "ErrorLog_" + sErrorTime + ".txt", true);

                sw.WriteLine(sLogFormat + methodName + "," + message);
                sw.Flush();

            }
            catch (Exception ex)
            {
                ErrorLog("ErrorLog Method :", ex.ToString());
            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }
        }
    }
}

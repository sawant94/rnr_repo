﻿using System;
using System.Data;
using MM.DAL;
using System.IO;
using System.Text;
using EvoPdf;
using System.Drawing;
using System.Net.Mail;
using System.Configuration;
using System.Web;
namespace EXE_MailSender_QuarterlyAward
{
    class Program
    {
        #region *** Properties ***
        static string ConnectionString = ConfigurationManager.ConnectionStrings["MM_Connection"].ConnectionString;
        static string MailDetails = ConfigurationManager.AppSettings["MailDetails"];
        static string QuarterCertificate = ConfigurationManager.AppSettings["Certificate"];
        static string ErrorLogSave_Path = ConfigurationManager.AppSettings["ErrorLogSave_Path"];
        static string MailLogs = ConfigurationManager.AppSettings["MailLogs"];

        #endregion

        static void Main(string[] args)
        {
            try
            {

                DataSet ds = new DataSet();
                DAL_Common objCommon = new DAL_Common();
                ds = objCommon.DAL_GetData("usp_EXE_MailSender_QuarterlyAward");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                        //string AwardType = string.Empty;
                        string strFileName = "QuarterAwardCertificate.txt";
                        //StreamReader sr = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("/MailDetails/" + strFileName));
                        StreamReader sr = new StreamReader(MailDetails + strFileName);

                        StringBuilder sb = new StringBuilder();

                        string BodyPart = sr.ReadToEnd();
                        sr.Close();
                        BodyPart = BodyPart.Replace("#Quarter", dr["Quarter"].ToString());
                        BodyPart = BodyPart.Replace("#Fyear", dr["FYear"].ToString());
                        BodyPart = BodyPart.Replace("#name", dr["RecipientName"].ToString());
                        BodyPart = BodyPart.Replace("#hodmsg", dr["ReasonForNomination"].ToString());
                        BodyPart = BodyPart.Replace("#awardeddate", dr["AwardedDate"].ToString());
                        BodyPart = BodyPart.Replace("#GiverName", dr["GiverName"].ToString());
                        BodyPart = BodyPart.Replace("#Designation1", dr["Designation1"].ToString());
                        BodyPart = BodyPart.Replace("#Designation2", dr["Designation2"].ToString());

                        HtmlToImageConverter htmlToImageConverter = new HtmlToImageConverter();
                        htmlToImageConverter.HtmlViewerWidth = 870;
                        htmlToImageConverter.HtmlViewerHeight =1000;
                        htmlToImageConverter.ConversionDelay = 0;
                        System.Drawing.Image[] imageTiles = null;
                        string baseUrl = "";

                        imageTiles = htmlToImageConverter.ConvertHtmlToImageTiles(BodyPart, baseUrl);
                        System.Drawing.Image outImage = imageTiles[0];
                        Bitmap bmp = new Bitmap(outImage);
                        //Rakesh
                        //string imageName = System.Web.HttpContext.Current.Server.MapPath("~/Certificate/") + dr["AwardType"].ToString() + "_" + dr["GiverToken"].ToString() + "_" + dr["TokenNumber"].ToString() + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".jpg";
                        string imageName = QuarterCertificate + dr["AwardType"].ToString() + "_" + dr["GiverToken"].ToString() + "_" + dr["TokenNumber"].ToString() + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".jpg";
                        bmp.Save(imageName);
                        bmp.Dispose();

                        // crop image ProcessAndSendMails save
                        System.Drawing.Image img = System.Drawing.Image.FromFile(imageName);
                        Bitmap bmpImage = new Bitmap(outImage);//(17, 17, 817, 660)
                        Bitmap bmpCrop = bmpImage.Clone(new Rectangle(17, 17, 817, 950), bmpImage.PixelFormat);
                        string imageNameBody = dr["AwardType"].ToString() + "Certificate" + dr["GiverToken"].ToString() + "_" + dr["TokenNumber"].ToString() + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".jpg";
                        //string Certificate = System.Web.HttpContext.Current.Server.MapPath("~/Certificate/") + imageNameBody;
                        string Certificate = QuarterCertificate + imageNameBody;
                        bmpCrop.Save(Certificate);
                        bmpCrop.Dispose();

                        string att = Certificate;
                        string strFileName1 = "QuarterAwardCertificateMailAttatchment.txt";

                        StreamReader sr1 = new StreamReader(MailDetails + strFileName1);
                        //StreamReader sr1 = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/MailDetails/" + strFileName1));

                        StringBuilder sb1 = new StringBuilder();
                        string BodyPart1 = sr1.ReadToEnd();
                        sr1.Close();
                        BodyPart1 = BodyPart1.Replace("<%RecipientName%>", dr["RecipientName"].ToString());
                        BodyPart1 = BodyPart1.Replace("<%GiverHODName%>", dr["GiverName"].ToString());
                        BodyPart1 = BodyPart1.Replace("<%AwardType%>", " Quarterly Divisional Excellence Award ");
                        BodyPart1 = BodyPart1.Replace("<%Amount%>", dr["AwardAmount"].ToString());
                        BodyPart1 = BodyPart1.Replace("#imgname", Certificate);
                        sendMail
                            (
                                "rewards_and_recognition@mahindra.com",
                                "rewards_and_recognition@mahindra.com",
                                dr["GiverToken"].ToString(),
                                dr["GiverName"].ToString(), 
                                dr["TokenNumber"].ToString() + "@MAHINDRA.COM",//hdnRecipientTokenNumber.Value
                                dr["RecipientName"].ToString(),//RecipientName,
                                dr["CCEmailID"].ToString(),//txtMailID.Text,
                                "CONGRATULATIONS!!",
                                BodyPart1,
                                att
                            );
                    }

                }                           
            catch (Exception ex)
            {
            }
        }

        public static bool sendMail(string strFromID, string strSenderName, string strFromIDEmail, string strSenderNameEmail, string strToID, string strRecipientName, string strCCID, string strSubject, string strMessage, string filePath)
        {
            bool blnSuccess = false;
            //THIS TRY CATCH IS FOR DEV SERVER UAT SINCE THERE ARE NO SMTP THERE
            try
            {
                string FromEmail = "rewards_and_recognition@mahindra.com";
                MailMessage mailMsg = new MailMessage();
                MailAddress fromAddr = new MailAddress(FromEmail, strSenderName);

                mailMsg.IsBodyHtml = true;
                mailMsg.From = fromAddr;
                mailMsg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                strToID = strToID.Replace(';', ',');
                Attachment att = new Attachment(filePath);
                att.ContentDisposition.Inline = true;
                mailMsg.Attachments.Add(att);
                mailMsg.To.Add(strToID);
               // mailMsg.To.Add("borsra-cont@mahindra.com");
                mailMsg.Subject = strSubject;
                mailMsg.Body = strMessage;
                //strCCID = "23120712@mahindra.com;borsra-cont@mahindra.com";
                if (!string.IsNullOrEmpty(strCCID))
                {
                    string[] strCC = strCCID.Split(';');
                    foreach (string str_CCId in strCC)
                    {
                        if (str_CCId.Trim() != string.Empty)
                        {
                            mailMsg.CC.Add(str_CCId);
                        }
                    }
                }
                
                string SMTPServer = ConfigurationSettings.AppSettings.Get("SMTP");
                SmtpClient objSmtpClient = new SmtpClient(SMTPServer);
                try
                {
                    objSmtpClient.Send(mailMsg);
                    Console.WriteLine("Email Sent to - " + strToID);
                    if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings.Get("MailLogs")))
                    {
                        string LogFileName = Path.Combine(ConfigurationSettings.AppSettings["MailLogs"].ToString(), DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt");
                        if (!Directory.Exists(Path.GetDirectoryName(LogFileName)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(LogFileName));
                        }
                        FileStream logFileStream = null;

                        try
                        {
                            string FilePath = Path.GetDirectoryName(LogFileName);

                            if (File.Exists(LogFileName) == false)
                            {
                                // If file doexs not exists create a new file at the specified path
                                logFileStream = File.Open(LogFileName, FileMode.Create, FileAccess.ReadWrite);
                            }
                            else
                            {
                                logFileStream = File.Open(LogFileName, FileMode.Append, FileAccess.Write);
                            }
                        }
                        finally
                        {
                            if (logFileStream != null)
                            {
                                using (StreamWriter MailLogWriter = new StreamWriter(logFileStream))
                                {
                                    MailLogWriter.WriteLine("-- -- -- -- -- -- -- -- -- -- -- -- ");
                                    MailLogWriter.WriteLine("Subject: " + strSubject);
                                    MailLogWriter.WriteLine("Date: " + DateTime.Now.ToString() + ", SenderEmailID: " + strFromIDEmail + ", Sender Name: " + strSenderNameEmail +", ReceiverEmailID: " + strToID + " Receiver: " + strRecipientName);
                                    MailLogWriter.WriteLine("-- -- -- -- -- -- -- -- -- -- -- -- ");
                                }
                                logFileStream.Close();
                            }
                        }
                        //END
                    }
                    
                }
                catch (Exception GeneralEx)
                {
                    throw new ArgumentException(GeneralEx.Message);
                }
                finally
                {
                    objSmtpClient = null;
                    mailMsg = null;
                    fromAddr = null;
                }
            }
            catch (Exception ex)
            {
                //THIS TRY CATCH IS FOR DEV SERVER UAT SINCE THERE ARE NO SMTP THERE
            }
            return blnSuccess;

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Configuration;
using System.Net.Mail;
//using Liberary_MailSender.Classes;
using System.IO.Compression;
using System.Diagnostics;
using System.Data.SqlClient;
using MM.DAL;

namespace EXE_Hifi_Mailer.classes
{
    public class hifiMailBase
    {
        DAL_Common objDALCommon = new DAL_Common();

        string sCon { get; set; }
        string sSQLSP { get; set; }

        //protected override string TemplatePath { get; set; }
        //protected override string TemplateFileName { get; set; }
        string strTemplatePath = ""; string strTemplateFileName = ""; string TemplatePath = "";
        string TemplateFileName = "";
        public hifiMailBase()
        {
            try
            {

           
            sCon = ConfigurationManager.ConnectionStrings["MM_Connection"].ToString();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        protected virtual string GetBody()
        {
            string BodyPart = "";
            string strTemplatePath = ""; string strTemplateFileName = "";
            try
            {
                TemplatePath = string.IsNullOrEmpty(strTemplatePath) ? ConfigurationSettings.AppSettings["AwardTypeTemplatePath"].ToString() : strTemplatePath;

                //StreamReader sr = new StreamReader(ConfigurationManager.AppSettings["SenderEmailId"].ToString());
                //  TemplatePath =  ConfigurationSettings.AppSettings["AwardTypeTemplatePath"].ToString();

                ///  RecipientList = GetRecipientList();
                TemplateFileName = string.IsNullOrEmpty(strTemplateFileName) ? TemplateFileName : strTemplateFileName;

                StreamReader sr = new StreamReader(TemplatePath + TemplateFileName);
                StringBuilder sb = new StringBuilder();
                BodyPart = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return BodyPart;
        }
        /// <summary>
        /// get data from the emailing queue table
        /// </summary>
        /// <remarks>
        /// gets records from the emailing queue table for whom emails are not sent
        /// <list type="bullet">
        /// <item>
        /// Table Used : EmailingQueue
        /// </item>
        /// <item>
        /// Stored Procedures Used : get_EmailingQueue.
        /// </item>
        /// </list>
        /// </remarks>
        /// <returns>DataSet</returns>
        DataSet get_EmailContentToSend()
        {
            DataSet dsContent = new DataSet();
            try
            {
               
                DAL_Common objCommon1 = new DAL_Common();
                SqlParameter[] objsqlparam1 = new SqlParameter[1];
                dsContent = objCommon1.DAL_GetData("get_EmailingQueue", objsqlparam1);

            }
            catch (Exception ex)
            {
                LogData("Error while getting data - Message :" + ex.Message + " StackTrace :" + ex.StackTrace);
            }
            return dsContent;
        }

        /// <summary>
        /// send emails for records fetched from the emailing queue table
        /// </summary>
        public void SendEmailsfromQueue()
        {
            LogData("Logging Started - Message : " + DateTime.Now);
           
            string message = string.Empty;
            string Subject = string.Empty;
            string emailTo = string.Empty;
            string emailFrom = string.Empty;
            string FailureMessage = string.Empty;
            bool MailSent = false;
          
            try
            {
             
                DataSet dsEmailDetails = get_EmailContentToSend();
           
                if (dsEmailDetails.Tables != null && dsEmailDetails.Tables.Count > 0)
                {
                   
                    #region get data to send mail
                    Console.WriteLine("Mailing recipient who have received HIFI...");

                    foreach (DataRow dr in dsEmailDetails.Tables[0].Rows)
                    {
                       
                        Subject = "";
                        message = "";
                        emailTo = "";
                        emailFrom = dr["EmailID"].ToString();
                        Subject = dr["Title"].ToString();
                        message = dr["Content"].ToString();
                        emailTo = dr["EmailID1"].ToString();
                       
                        MailMessage emailMessage = new MailMessage();
                        emailMessage.From = new MailAddress(emailFrom);
                        emailMessage.To.Add(emailTo);
                        emailMessage.Subject = Subject;
                        emailMessage.Body = message;
                        TemplateFileName = "hifiMail.txt";
                        
                        string BodyPart = GetBody();
                        BodyPart = BodyPart.Replace("<%RecipientName%>", dr["TFullname"].ToString()).Replace("<%message%>", dr["Content"].ToString()).Replace("<% MyAwards%>", ConfigurationSettings.AppSettings["MyAward"].ToString());
                        BodyPart = BodyPart.Replace("<%GiverHODName%>", dr["SFullname"].ToString());

                        emailMessage.Body = BodyPart;
                        emailMessage.IsBodyHtml = true;
                        try
                        {
                            string smtp_server = System.Configuration.ConfigurationManager.AppSettings.Get("Smpt_Server");
                            SmtpClient mailClient = new SmtpClient(smtp_server);
                            mailClient.Send(emailMessage);
                           
                            MailSent = true;
                            FailureMessage = "";
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("11");
                            FailureMessage = ex.Message;
                            MailSent = true;
                            LogData("Error while sending email - Message :" + ex.Message + " StackTrace :" + ex.StackTrace);
                        }


                    #endregion get data to send mail

                    #region Update Email Sending Status

                        try
                        {
                            sSQLSP = "usp_update_hifimailsent";
                            SqlParameter[] sqlparams = new SqlParameter[3];
                            // string id = dsEmailDetails.Tables[0].Rows[0]["Id"].ToString();
                            for (int i = 0; i <= dsEmailDetails.Tables[0].Rows.Count - 1; i++)
                            {
                                string id = dsEmailDetails.Tables[0].Rows[i]["Id"].ToString();
                                sqlparams[0] = new SqlParameter("@PostId", id);
                                sqlparams[1] = new SqlParameter("@hifiMailSent", SqlDbType.Bit);
                                sqlparams[1].Value = MailSent;
                                objDALCommon.DAL_SaveData("usp_update_hifimailsent", sqlparams, 'N');
                                WriteLog(id);


                            }
                        }
                        catch (Exception ex)
                        {
                            LogData("Error updating data in DB - Message :" + ex.Message + " StackTrace :" + ex.StackTrace);
                        }

                        Console.WriteLine("Updating HifiSent Status in User....");
                        Console.WriteLine("...");
                        Console.WriteLine("..");
                        Console.WriteLine(".");
                        //  emailMessage.Dispose();
                        #endregion


                    }

                }
                else
                {
                    Console.WriteLine("No data found");
                }
            }
            catch (Exception exx)
            {

                Console.WriteLine();
            }
        }

        public void LogData(string logMessage)
        {
            string strLogFolderPath = ConfigurationManager.AppSettings["LogFolderPath"];
            string FileName = DateTime.Now.ToString("ddMMMyyyy") + ".txt";
            StreamWriter writer;
            writer = File.AppendText(strLogFolderPath + FileName);
            writer.WriteLine("-----------------------------------------------------");
            writer.WriteLine("High Five Mail");

            try
            {

                if (!Directory.Exists(strLogFolderPath))
                {
                    Directory.CreateDirectory(strLogFolderPath);
                }
                if (!File.Exists(strLogFolderPath + FileName))
                {
                    File.Create(strLogFolderPath + FileName).Close();
                }

                writer.WriteLine(logMessage);
                writer.WriteLine("\r");
                writer.Flush();
                writer.Close();
                writer.Dispose();
            }
            catch (Exception ex)
            {
                writer.WriteLine(ex.Message);
            }
        }

        private void WriteLog(string Id)
        {


            string strLogFolderPath = ConfigurationManager.AppSettings["LogFolderPath"];
            string FileName = DateTime.Now.ToString("ddMMMyyyy") + ".txt";
            StreamWriter writer;//, err_writer ;


            //string strErrorLogPath = ConfigurationManager.AppSettings["ErrorLogPath"];
            //string errorlog= DateTime.Now.ToString("ddMMMyyyy") + ".txt";

            writer = File.AppendText(strLogFolderPath + FileName);
            writer.WriteLine("\r");

            DataSet dslog = new DataSet();
            DAL_Common objCommon1 = new DAL_Common();
            SqlParameter[] objsqlparam1 = new SqlParameter[1];
            try
            {

                objsqlparam1[0] = new SqlParameter("@PostId", Id);
                dslog = objCommon1.DAL_GetData("get_HifiEmailID", objsqlparam1);

                if (dslog != null)
                {

                    int j = 0; int successfully = 0; int failed = 0; string message; string message1;
                    //foreach (MailDetails_Extras objMailDetails_Extras in objMailDetails_Coll)             
                    for (int i = 0; i < dslog.Tables[0].Rows.Count; )
                    {

                        Boolean status = Convert.ToBoolean(dslog.Tables[0].Rows[i]["hifiMailSent"]);

                        if (status == true)
                        {
                            message = dslog.Tables[0].Rows[i]["EmailID"].ToString();
                            successfully = j++;
                            writer.WriteLine("Mails sent successfully To : " + message);
                        }

                        else
                        {
                            message1 = dslog.Tables[0].Rows[i]["EmailID"].ToString();
                            failed = j++;
                            writer.WriteLine("Mails sending failed  To: " + message1);

                        }
                        //writer.WriteLine(" Number of Mails sent successfully : " + successfully);
                        //writer.WriteLine(" Number of Mails sending failed : " + failed);

                        //Console.WriteLine("Number of mails sent SUCCESSFULLY : " + successfully);
                        //Console.WriteLine("Number of mails sending FAILED : " + failed);
                        i++;
                    }
                }
                else
                {
                    Console.WriteLine("No records found. Thankyou");
                    writer.WriteLine("No records found. Thankyou");
                }
            }
            catch (Exception exx)
            {

                writer.WriteLine(exx.Message);
            }
            writer.Flush();
            writer.Close();
            writer.Dispose();

        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class PostBase : BusinessObjectBase<DTO.Post>
    {
        protected IPostRepository repo;
        private IList<Comment> comments;
        private IList<Group> groups;
        private IList<Like> likes;


        private User source;
        private User target;
        public User Source
        {
            get
            {
                PropertyGet(i => i.SourceId);
                return LazyLoad(ref source, () => new User(Context, new DTO.User { ID = innerObject.SourceId }));
            }
        }



        public User Target
        {
            get
            {
                PropertyGet(i => i.TargetId);
                return LazyLoad(ref target, () => new User(Context, new DTO.User { ID = innerObject.TargetId }));
            }
        }

        public int PostType
        {
            get { return innerObject.PostTypeId; }
        }


        public string Title
        {
            get { return innerObject.Title; }
        }

        public string Content
        {
            get
            {
                return PropertyGet(i => i.Content);
            }
            set
            {
                PropertySet(value, i => i.Content, i => i.Content = value);
            }
        }

        public int LikeCount
        {
            get
            {
                return innerObject.LikeCount;
            }
        }

        public int CommentCount
        {
            get
            {
                return innerObject.CommentCount;
            }
        }

        public bool ishifiMailSent
        {
            get
            {
                return PropertyGet(i => i.ishifiMailSent);
            }
            set
            {
                PropertySet(value, i => i.ishifiMailSent, i => i.ishifiMailSent = value);
            }
        }

        public IEnumerable<Like> Likes
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        public IEnumerable<Comment> Comments
        {
            get
            {
                return LazyLoad(ref comments, () => new List<Comment>(from item in repo.GetCommentsForPost(this.DTO)
                                                                      select new Comment(Context, item)));
            }
        }

        public IEnumerable<Group> SharedWith
        {
            get
            {
                return LazyLoad(ref groups, () => new List<Group>(from item in repo.GetSharedGroupsForPost(innerObject)
                                                                  select new Group(Context, item)));

            }
        }

        public bool IsSharedWithAllFriends
        {
            get
            {
                return PropertyGet(i => i.IsSharedWithAllFriends);
            }
            set
            {
                PropertySet(value, i => i.IsSharedWithAllFriends, i => i.IsSharedWithAllFriends = value);
            }
        }
        //public bool hifiMailstatus
        //{
        //    get
        //    {
        //        return PropertyGet(i => i.hifiMailstatus);
        //    }
        //    set
        //    {
        //        PropertySet(value, i => i.hifiMailstatus, i => i.hifiMailstatus = value);
        //    }
        //}

        public DateTime CreatedOn
        {
            get
            {
                return PropertyGet(i => i.CreatedOn);
            }
        }

        protected override DTO.Post LoadObject()
        {
            return repo.GetPostById(ID);
        }

        protected override DTO.Post SaveObject()
        {
            return repo.UpdatePost(innerObject);
        }

        protected override DTO.Post CreateNewObject(out int newID)
        {
            DTO.Post result = repo.CreateNewPost(innerObject);
            newID = result.ID;
            return result;
        }

        protected override bool IsEmptyInnerObject()
        {
            return innerObject == null || innerObject.Content == null;
        }

        public void Like()
        {
            repo.LikePost(this.DTO, Context.CurrentUser.DTO);
            if (Context.CurrentUser.ID != Target.ID)
            {
                Context.CreateNotification(1, Target.ID, string.Format("{0} has liked your Post", Context.CurrentUser.FullName), ID);
            }

        }

        public void Comment(string message)
        {
            repo.AddCommentToPost(message, innerObject, Context.CurrentUser.DTO);
            if (Context.CurrentUser.ID != Target.ID)
            {
                Context.CreateNotification(2, Target.ID, string.Format("{0} has commented on your Post", Context.CurrentUser.FullName), ID);
            }
        }

        public void ShareWithOnlyMe()
        {
            IsSharedWithAllFriends = false;
            repo.DeleteSharedGroups(innerObject);
            Update();
        }

        public void ShareWithAllFriends()
        {
            IsSharedWithAllFriends = true;
            repo.DeleteSharedGroups(innerObject);
            Update();
        }

        public void ShareWith(IEnumerable<Group> group)
        {
            //IsSharedWithAllFriends = false;
            //repo.DeleteSharedGroups(innerObject);
            repo.SharePostWithGroups(innerObject, group);
            Update();
        }

        public void Delete()
        {
            throw new System.NotImplementedException();
        }


        public IEnumerable<Like> PostLikes
        {
            get
            {

                return (from item in repo.GetLikesByPostId(innerObject.ID)
                        select new Like(Context, item));
            }
        }

        protected internal PostBase(Context context, DTO.Post dtoObject)
            : base(context)
        {
            repo = context.PostRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }
    }
}

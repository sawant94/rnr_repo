﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class Group
    {
        public int ID { get; set; }
        public int OwnerId { get; set; }
        public string Title { get; set; }
        public string Photo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class Notification
    {
        public int ID { get; set; }
        public int TargetId{get; set; }
        public int NotificationType { get; set; }
        public string Message { get; set; }
        public string DetailURL { get; set; }
        public int? RelateItemId { get; set; }
        public bool UnRead { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Active { get; set; }
    }
}

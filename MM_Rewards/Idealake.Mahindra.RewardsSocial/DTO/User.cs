﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class User
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Photo  { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string TokenNumber { get; set; }
        public string Education { get; set; }
        public string Interest { get; set; }
        public int UnreadNotificationCount { get; set; }
        public bool hifimail { get; set; }
        
    }
}

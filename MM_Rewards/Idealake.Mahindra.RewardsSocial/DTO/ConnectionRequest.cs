﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class ConnectionRequest
    {
        public int ID { get; set; }
        public int SourceId { get; set; }
        public int TargetId { get; set; }
        public DateTime RequestedDate { get; set; }
        public int Status { get; set; }
    }
}

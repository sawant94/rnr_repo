﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class Post
    {
        public int ID { get; set; }
        public int PostTypeId { get; set; }
        public int SourceId { get; set; }
        public int TargetId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsSharedWithAllFriends { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LikeCount { get; set; }
        public int CommentCount { get; set; }
        public bool ishifiMailSent { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class RNRReport
    {
        //public int Hits { get; set; }
        public string BusinessUnit { get; set; }
        public int? CommentsReceived { get; set; }
        public int? FriendRequestsMade { get; set; }
        public int? LikesReceived { get; set; }
        public int? HighFivesReceived { get; set; }
        public int? SpotVouchersReceived { get; set; }
        public int? GiftHampersReceived { get; set; }
        public int? CoffeeHampersReceived { get; set; }
    }
}

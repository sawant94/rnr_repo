﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class Comment
    {
        public int ID
        {
            get;
            set;
        }

        public int PostId { get; set; }
        public int SourceId { get; set; }


        public string Content
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int LikeCount { get; set; }
    }
}

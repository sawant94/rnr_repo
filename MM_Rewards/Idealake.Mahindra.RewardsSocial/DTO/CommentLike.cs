﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial.DTO
{
    public class CommentLike
    {
        public int ID { get; set; }
        public string OwnerName { get; set; }
        public int CommentID { get; set; }

        public User LikeUser { get; set; }
        public int UserId { get; set; }
    }
}

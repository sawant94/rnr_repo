﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class User : BusinessObjectBase<DTO.User>
    {
        private IUserRepository repo;
        private List<User> friends;
        private List<ConnectionRequest> connectionRequests;
        private List<PostBase> posts, currentuserposts;
        private List<Notification> notification;
        private List<Group> groups;

        public string FullName
        {
              
            get
            {                        
                return PropertyGet(i => i.FullName);               
            }
            set
            {
                PropertySet(value, i => i.FullName, i => i.FullName = value);
            }
             
        }
        public string TokenNumber
        {
            get
            {
                return PropertyGet(i => i.TokenNumber);
            }
            set
            {
                PropertySet(value, i => i.TokenNumber, i => i.TokenNumber = value);
            }
        }
        public bool hifimail
        {
            get {

                return PropertyGet(i => i.hifimail);
            }
            set
            {
                PropertySet(value, i => i.hifimail, i => i.hifimail = value);
            }
        }

        public string Photo
        {
            get
            {
                return PropertyGet(i => i.Photo);
            }
            set
            {
                PropertySet(value, i => i.Photo, i => i.Photo = value);
            }
        }

        public string Department
        {
            get
            {
                return PropertyGet(i => i.Department);
            }
            set
            {
                PropertySet(value, i => i.Department, i => i.Department = value);
            }
        }

        public string Designation
        {
            get
            {
                return PropertyGet(i => i.Designation);
            }
            set
            {
                PropertySet(value, i => i.Designation, i => i.Designation = value);
            }
        }

        public string Education
        {
            get
            {
                return PropertyGet(i => i.Education);
            }
            set
            {
                PropertySet(value, i => i.Education, i => i.Education = value);
            }
        }

        public string Interest
        {
            get
            {
                return PropertyGet(i => i.Interest);
            }
            set
            {
                PropertySet(value, i => i.Interest, i => i.Interest = value);
            }
        }

        public bool IsCurrentUser
        {
            get { return ID == Context.CurrentUser.ID; }
        }

        public IEnumerable<User> Friends
        {
            get
            {
                //if (friends == null)
                //{
                //    friends = new List<User>(from item in repo.GetFriends(innerObject) 
                //                             select new User(Context,item));
                //}
                return LazyLoad(ref friends, () => new List<User>(from item in repo.GetFriends(innerObject)
                                                                  select new User(Context, item)));
            }
        }

        public int UnreadNotificationCount
        {
            get
            {
                return repo.GetUnreadNotificationCount(innerObject);
            }
        }
        public IEnumerable<Notification> Notifications
        {
            get
            {
                return LazyLoad(ref notification, () => new List<Notification>(from item in repo.GetNotificationsForCurrentUser(this.DTO)
                                                                               select new Notification(Context, item)));
            }

        }

        public IEnumerable<ConnectionRequest> ConnectionRequests
        {
            get
            {
                IEnumerable<ConnectionRequest> result;

                if (IsCurrentUser)
                {
                    result = LazyLoad(ref connectionRequests, () => new List<ConnectionRequest>(from item in repo.GetConnectionRequests(innerObject)
                                                                                                select new ConnectionRequest(Context, item)));
                }
                else
                {
                    result = Enumerable.Empty<ConnectionRequest>();
                }
                return result;


            }
        }

        public IEnumerable<PostBase> Posts
        {
            get
            {
                IEnumerable<PostBase> result;
                if (IsCurrentUser)
                {
                    result = LazyLoad(ref posts, () => new List<PostBase>(from item in repo.GetAllPostsForCurrentUser(this.DTO)
                                                                          select new PostBase(Context, item)));
                }
                else
                {
                    result = LazyLoad(ref posts, () => new List<PostBase>(from item in repo.GetPostsForTargetUser(Context.CurrentUser.DTO, this.DTO)
                                                                          select new PostBase(Context, item)));
                }

                return result;
            }

        }

        public IEnumerable<Group> Groups
        {
            get
            {
                IEnumerable<Group> result;
                if (IsCurrentUser)
                {
                    result = LazyLoad(ref groups, () => new List<Group>(from item in repo.GetGroupsForCurrentUser(innerObject)
                                                                        select new Group(Context, item)));
                }
                else
                {
                    result = Enumerable.Empty<Group>();
                }
                return result;
            }

        }

        public Group AddGroup(string groupName)
        {
            Group newGroup = new Group(Context, ID) { Title = groupName };
            newGroup.Update();
            return newGroup;
        }

        public ConnectionRequest GetConnectionRequestById(int id)
        {
            return ConnectionRequests.Where(i => i.ID == id).FirstOrDefault();
        }

        public bool CheckPendingRequest(DTO.User user, DTO.User currentUser)
        {
            return repo.CheckPendingRequest(user, currentUser);
        }

        public void DeleteFriend(User friend)
        {
            repo.DeleteFriend(this.DTO, friend.DTO);
        }

        public void DeleteGroup(Group group)
        {
            repo.DeleteGroup(group.DTO);
        }






        //public IEnumerable<PostBase> GetPostsForCurrentUserByPostType()
        //{
        //    IEnumerable<PostBase> result = null;
        //    var temp = (from item in repo.GetPostsForCurrentUserByPostType(this.DTO, posttype)
        //                select new PostBase(Context, item));

        //    if (temp != null)
        //    {
        //        result = temp;
        //    }

        //    return result;

        //}


        public IEnumerable<PostBase> CurrentUserPosts
        {
            get
            {
                IEnumerable<PostBase> result = null;
                if (IsCurrentUser)
                {
                    result = LazyLoad(ref currentuserposts, () => new List<PostBase>(from item in repo.GetPostsForCurrentUser(this.DTO)
                                                                                     select new PostBase(Context, item)));
                }
                else
                {
                    result = LazyLoad(ref currentuserposts, () => new List<PostBase>(from item in repo.GetPostsForTargetUser(Context.CurrentUser.DTO, this.DTO)
                                                                                     select new PostBase(Context, item)));
                }
                return result;
            }

        }

        protected override bool IsEmptyInnerObject()
        {
            return innerObject == null || innerObject.FullName == null;
        }

        protected override DTO.User LoadObject()
        {
            return repo.GetUserById(ID);
        }

        protected override DTO.User SaveObject()
        {
            return repo.UpdateUser(innerObject);
        }

        protected override DTO.User CreateNewObject(out int newID)
        {
            throw new NotImplementedException("New users cannot be created.");
        }



        internal User(Context context, DTO.User dtoObject)
            : base(context)
        {
            repo = context.UserRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }

        internal User(Context context)
            : this(context, new DTO.User() { ID = -1 })
        {

        }


    }
}

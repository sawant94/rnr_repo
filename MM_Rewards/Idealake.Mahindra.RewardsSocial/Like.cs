﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class Like : BusinessObjectBase<DTO.Like>
    {
        private IUserRepository repo;
        private User user;
        //public int Id
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}

        //public PostBase Post
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}

        //public DateTime CreatedOn
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}



        public User User
        {
            get
            {
                PropertyGet(i => i.UserId);
                return LazyLoad(ref user, () => new User(Context, new DTO.User() { ID = innerObject.UserId }));
            }
        }

        public int UserId
        {
            get
            {
                return PropertyGet(i => i.UserId);
            }
            set
            {
                PropertySet(value, i => i.UserId, i => i.UserId = value);
            }
        }
        public string OwnerLike
        {
            get
            {
                return PropertyGet(i => i.OwnerName);
            }

            set
            {
                PropertySet(value, i => i.OwnerName, i => i.OwnerName = value);
            }
        }

        public int PostId
        {
            get
            {
                return PropertyGet(i => i.PostID);
            }

            set
            {
                PropertySet(value, i => i.PostID, i => i.PostID = value);
            }
        }


        protected override DTO.Like LoadObject()
        {
            throw new NotImplementedException();
        }

        protected override DTO.Like SaveObject()
        {
            throw new NotImplementedException();
        }

        protected override DTO.Like CreateNewObject(out int newID)
        {
            throw new NotImplementedException();
        }

        internal Like(Context context, DTO.Like dtoObject)
            : base(context)
        {
            repo = context.UserRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }


    }
}

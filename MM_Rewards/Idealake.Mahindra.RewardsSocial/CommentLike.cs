﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Idealake.Mahindra.RewardsSocial.DTO;

namespace Idealake.Mahindra.RewardsSocial
{
    public class CommentLike : BusinessObjectBase<DTO.CommentLike>
    {
        private IUserRepository repo;
        private User user;
        //public int Id
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}

        //public PostBase Post
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}

        //public DateTime CreatedOn
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}



        public User User
        {
            get
            {
                PropertyGet(i => i.UserId);
                return LazyLoad(ref user, () => new User(Context, new DTO.User() { ID = innerObject.UserId }));
            }
        }

        public int UserId
        {
            get
            {
                return PropertyGet(i => i.UserId);
            }
            set
            {
                PropertySet(value, i => i.UserId, i => i.UserId = value);
            }
        }
        public string OwnerLike
        {
            get
            {
                return PropertyGet(i => i.OwnerName);
            }

            set
            {
                PropertySet(value, i => i.OwnerName, i => i.OwnerName = value);
            }
        }

        public int CommentID
        {
            get
            {
                return PropertyGet(i => i.CommentID);
            }

            set
            {
                PropertySet(value, i => i.CommentID, i => i.CommentID = value);
            }
        }


        protected override DTO.CommentLike LoadObject()
        {
            throw new NotImplementedException();
        }

        protected override DTO.CommentLike SaveObject()
        {
            throw new NotImplementedException();
        }

        protected override DTO.CommentLike CreateNewObject(out int newID)
        {
            throw new NotImplementedException();
        }

        internal CommentLike(Context context, DTO.CommentLike dtoObject)
            : base(context)
        {
            repo = context.UserRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }
    }
}

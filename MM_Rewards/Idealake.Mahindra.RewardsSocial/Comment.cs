﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class Comment : BusinessObjectBase<DTO.Comment>
    {
        protected IPostRepository repo;

        private PostBase post;
        public PostBase Post
        {
            get
            {
                return LazyLoad(ref post, () => new PostBase(Context, new DTO.Post { ID = innerObject.PostId }));
            }
        }

        public string Content
        {
            get
            {
                return PropertyGet(i => i.Content);
            }
            set
            {
                PropertySet(value, i => i.Content, i => i.Content = value);
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return PropertyGet(i => i.CreatedOn);
            }
            set
            {
                PropertySet(value, i => i.CreatedOn, i => i.CreatedOn = value);
            }
        }

        public User Source
        {
            get
            {
              return  new User(Context, new DTO.User { ID = innerObject.SourceId });
            }
        }

        public bool CanDelete
        {
            get
            {
                return Source.ID == Context.CurrentUser.ID || Post.Target.ID == Context.CurrentUser.ID;
            }
        }

        public void Like()
        {
            repo.LikeComment(this.DTO, Context.CurrentUser.DTO);
            if (Context.CurrentUser.ID != Post.Target.ID)
            {
                Context.CreateNotification(1, Post.Target.ID, string.Format("{0} has liked your Comment", Context.CurrentUser.FullName), ID);
            }

        }

        public int LikeCount
        {
            get
            {
                return innerObject.LikeCount;
            }
        }

        public bool Delete()
        {
            return repo.DeleteComment(innerObject);
        }

        public IEnumerable<CommentLike> CommentLikes
        {
            get
            {

                return (from item in repo.GetLikesByCommentId(innerObject.ID)
                        select new CommentLike(Context, item));
            }
        }

        protected override DTO.Comment LoadObject()
        {
            return repo.GetCommentById(ID);
        }

        protected override DTO.Comment SaveObject()
        {
            throw new NotImplementedException();
        }

        protected override DTO.Comment CreateNewObject(out int newID)
        {
            throw new NotImplementedException();
            //DTO.Comment comment = repo.AddCommentToPost(innerObject, Post.DTO, Context.CurrentUser.DTO);
        }



        protected internal Comment(Context context, DTO.Comment dtoObject)
            : base(context)
        {
            repo = context.PostRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }
    }
}

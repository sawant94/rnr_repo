﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public abstract class BusinessObjectBase<T> where T:new()
    {
        private Context currentContext;
        protected T innerObject;
        protected bool isDirty;

        public int ID { get; set; }

        public Context Context
        {
            get { return currentContext; }
        }

        protected abstract T LoadObject();
        protected abstract T SaveObject();
        protected abstract T CreateNewObject(out int newID);
        protected virtual bool IsEmptyInnerObject()
        {
            return innerObject == null;
        }

        protected void LazyLoad(bool force = false) 
        {
            if(ID!=-1)
            {
                if (IsEmptyInnerObject() || force)
                {
                    innerObject = LoadObject();
                    isDirty = false;
                }
            }
        }

        protected PT LazyLoad<PT>(ref PT variable, Func<PT> getValue) where PT: class
        {
            if (variable == null)
                variable = getValue();

            return variable;
        }

        protected PT PropertyGet<PT>(Func<T, PT> getprop)
        {
            LazyLoad();
            return getprop(innerObject);
        }

        protected void PropertySet<PT>(PT newValue, Func<T,PT> getProp, Action<T> setProp)
        {
            LazyLoad();
            if (!newValue.Equals(getProp(innerObject)))
            {
                setProp(innerObject);
                isDirty = true;
            }
        }

        public void Update()
        {
            if (ID != -1)
                innerObject = SaveObject();
            else
            {
                int newID = -1;
                innerObject = CreateNewObject(out newID);
                ID = newID;
            }
            isDirty = false;
        }

        public void Refresh()
        {
            LazyLoad(true);
        }

        public T DTO
        {
            get { return innerObject; }
        }

        public BusinessObjectBase(Context context)
        {
            currentContext = context;
        }
    }
}

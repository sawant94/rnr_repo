﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public interface IUserRepository
    {
        IEnumerable<DTO.User> SearchForUsers(string searchString);

        DTO.User GetUserById(int ID);

        DTO.User GetUserByTokenNumber(string tokenNumber);

        DTO.User UpdateUser(DTO.User user);

        //DTO.User UpdateUserHifiCurrentUser(bool hifimail);
        //DTO.User GetHifistatusCurrentUser(bool hifimail);

        IEnumerable<DTO.User> GetFriends(DTO.User user);

        DTO.ConnectionRequest ConnectUsers(User sourceUser, User targetUser);

        IEnumerable<DTO.ConnectionRequest> GetConnectionRequests(DTO.User user);

        bool AcceptConnectionRequest(DTO.ConnectionRequest connectionRequest);

        bool DeclineConnectionRequest(DTO.ConnectionRequest DTO);

        bool CheckPendingRequest(DTO.User user, DTO.User currentUser);

        //bool CreateNewConnection(DTO.ConnectionRequest connectionRequest);


        IEnumerable<DTO.Post> GetPostsForCurrentUser(DTO.User currentUser, int limit = 1000);

        IEnumerable<DTO.Post> GetAllPostsForCurrentUser(DTO.User currentUser, int limit = 1000);



        IEnumerable<DTO.Post> GetPostsForTargetUser(DTO.User currentUser, DTO.User targetUser, int limit = 1000);


        DTO.Notification GetNotificationById(int notificationID);

        DTO.Notification UpdateNotification(DTO.Notification notification);

        DTO.Notification CreateNewNotification(DTO.Notification notification);

        IEnumerable<DTO.Notification> GetNotificationsForCurrentUser(DTO.User currentUser);


        DTO.Group GetGroupById(int groupID);

        DTO.Group UpdateGroup(DTO.Group group);

        DTO.Group CreateNewGroup(DTO.Group group);

        IEnumerable<DTO.Group> GetGroupsForCurrentUser(DTO.User currentUser);

        IEnumerable<DTO.User> GetMembersForGroup(DTO.Group group);

        DTO.User AddMemberToGroup(DTO.Group innerObject, User newMember);

        void RemoveMemberFromGroup(DTO.Group innerObject, User oldMember);

        int GetUnreadNotificationCount(DTO.User user);


        void DeleteFriend(DTO.User currentUser, DTO.User user);

        void DeleteGroup(DTO.Group group);

        void ConnectUsers_BulkUpload(User sourceUser, User targetUser);

        IEnumerable<DTO.RNRReport> GetRNRReport(DateTime fromDate, DateTime toDate);
    }
}

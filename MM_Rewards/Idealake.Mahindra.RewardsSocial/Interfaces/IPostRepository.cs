﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public interface IPostRepository
    {
        DTO.Post GetPostById(int ID);

        DTO.Post UpdatePost(DTO.Post innerObject);

        DTO.Post CreateNewPost(DTO.Post innerObject);

        void LikePost(DTO.Post post, DTO.User user);

        DTO.Comment AddCommentToPost(string message, DTO.Post innerObject, DTO.User user);

        IEnumerable<DTO.Comment> GetCommentsForPost(DTO.Post post, int limit = 1000);

        DTO.Comment GetCommentById(int ID);

        bool DeleteComment(DTO.Comment innerObject);

        IEnumerable<DTO.Group> GetSharedGroupsForPost(DTO.Post post);

        void DeleteSharedGroups(DTO.Post post);

        void SharePostWithGroups(DTO.Post post, IEnumerable<Group> group);

        void LikeComment(DTO.Comment comment, DTO.User user);

        IEnumerable<DTO.Like> GetLikesByPostId(int postID);

        IEnumerable<DTO.CommentLike> GetLikesByCommentId(int commentID);
    }
}

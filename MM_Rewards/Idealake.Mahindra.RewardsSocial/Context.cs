﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public abstract class Context
    {
        private IUserRepository userRepository;
        private IPostRepository postRepository;

        internal IUserRepository UserRepository
        {
            get { return userRepository; }
        }

        internal IPostRepository PostRepository
        {
            get { return postRepository; }
        }

        public abstract User CurrentUser
        {
            get;
        }

        public void ConnectToUser(User targetUser)
        {
            DTO.ConnectionRequest connectionrequest = userRepository.ConnectUsers(CurrentUser, targetUser);
            CreateNotification(3, targetUser.ID, string.Format("{0} has sent you a Friend Request", CurrentUser.FullName), connectionrequest.ID);
        }

        public void ConnectUsers_BulkUpload(User sourceUser, User targetUser)
        {
            //DTO.ConnectionRequest connectionrequest = userRepository.ConnectUsers(sourceUser, targetUser);
            //ConnectionRequest cr = sourceUser.GetConnectionRequestById(Convert.ToInt32(connectionrequest.ID));
            //cr.Accept();
            userRepository.ConnectUsers_BulkUpload(sourceUser, targetUser);
        }

        public User GetUserByID(int id)
        {
            //return new User(userrepository, userrepository.GetUserById(id));
            return new User(this, new DTO.User() { ID = id });
        }

        public User GetUserByTokenNumber(string tokenNumber)
        {
            return new User(this, userRepository.GetUserByTokenNumber(tokenNumber));
        }



        public PostBase SendHighFiveMessage(User target, string message)
        {
            HighFivePost post = new HighFivePost(this, CurrentUser, target, DateTime.Now) { Content = message, IsSharedWithAllFriends = true , ishifiMailSent = false};
            post.ishifiMailSent = false;
            post.Update();

            // Generating Notification
            Notification hifiNotification = CreateNotification(0, target.ID, string.Format("{0} has given you a High Five!", CurrentUser.FullName), post.ID);

            return post;
        }

        internal Notification CreateNotification(int notificationType, int targetID, string message, int? relatedItemId)
        {
            Notification notification = new Notification(this, new DTO.Notification() { ID = -1, NotificationType = notificationType, TargetId = targetID, Message = message, RelateItemId = relatedItemId, UnRead = true, CreatedOn = DateTime.Now, Active = true });
            notification.Update();
            return notification;
        }

        public PostBase GetPostById(int ID)
        {
            DTO.Post post = postRepository.GetPostById(ID);
            return new PostBase(this, post);
        }

        public Comment GetCommentById(int ID)
        {
            DTO.Comment comment = postRepository.GetCommentById(ID);
            return new Comment(this, comment);
        }

        public Notification GetNotificationById(int ID)
        {
            DTO.Notification notification = userRepository.GetNotificationById(ID);
            return new Notification(this, notification);

        }

        public Group GetGroupByID(int ID)
        {
            DTO.Group group = userRepository.GetGroupById(ID);
            return new Group(this, group);
        }

        public IEnumerable<User> SearchForUsers(string searchString)
        {
            return from item in userRepository.SearchForUsers(searchString)
                   select new User(this, item);
        }

        public void GiveAward(string tokenNumberOfAwardee, string awardName, string comments)
        {
            User awardee = GetUserByTokenNumber(tokenNumberOfAwardee);

            if (awardee != null)
            {
                AwardPost newPost = new AwardPost(this, CurrentUser, awardee, DateTime.Now, awardName);



                newPost.Content = string.Format("{0}", comments);
                newPost.IsSharedWithAllFriends = true;
                newPost.ishifiMailSent = false;
                newPost.Update();



                Notification hifiNotification = CreateNotification(1, awardee.ID, string.Format("{0} has given you a Award", CurrentUser.FullName), newPost.ID);

            }
        }

        public IEnumerable<DTO.RNRReport> getRNRReport(DateTime fromDate, DateTime toDate)
        {
            IEnumerable<DTO.RNRReport> report = userRepository.GetRNRReport(fromDate,toDate);
            return report;

        }

        protected Context(
                        IUserRepository userrepository,
                        IPostRepository postrepository
            )
        {
            userRepository = userrepository;
            postRepository = postrepository;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class Group : BusinessObjectBase<DTO.Group>
    {
        private User owner;
        private IUserRepository repo;
        private List<User> members;

        public User Owner
        {

            get
            {
                return LazyLoad(ref owner, () => new User(Context, new DTO.User() { ID = innerObject.OwnerId } ));
            }
        }

        public string Title
        {
            get
            {
                return PropertyGet(i=> i.Title);
            }
            set
            {
                PropertySet(value, i => i.Title, i => i.Title = value);
            }
        }

        public string Photo
        {
            get
            {
                return PropertyGet(i => i.Photo);
            }
            set
            {
                PropertySet(value, i => i.Photo, i => i.Photo = value);
            }
        }

        public IEnumerable<User> Members
        {
            get
            {
                return LazyLoad(ref members, ()=> new List<User>(from item in repo.GetMembersForGroup(innerObject)
                                                                     select new User(Context, item)));
            }
        }

        public IEnumerable<User> Nonmembers
        {
            get
            {
                return Owner.Friends.Except(Members, new UserComparer());
            }

        }

        protected override bool IsEmptyInnerObject()
        {
            return innerObject==null || innerObject.Title == null;
        }

        protected override DTO.Group LoadObject()
        {
            return repo.GetGroupById(ID);
        }

        protected override DTO.Group SaveObject()
        {
            return repo.UpdateGroup(innerObject);
        }

        protected override DTO.Group CreateNewObject(out int newID)
        {
            DTO.Group result = repo.CreateNewGroup(innerObject);
            newID = result.ID;
            return result;
        }

        public void AddMember(User newMember)
        {
            repo.AddMemberToGroup(innerObject, newMember);
        }

        public void RemoveMember(User oldMember)
        {
            repo.RemoveMemberFromGroup(innerObject, oldMember);
        }

        internal Group(Context context, DTO.Group dtoObject)
            : base(context)
        {
            repo = context.UserRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }

        internal Group(Context context, int ownerId)
            : this(context, new DTO.Group() { ID = -1, OwnerId = ownerId })
        {

        }
    }
}

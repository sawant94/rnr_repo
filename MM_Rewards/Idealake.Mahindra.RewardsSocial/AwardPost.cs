﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class AwardPost : PostBase
    {
        public AwardPost(Context context, DTO.Post dtoObject)
            : base(context, dtoObject)
        {

        }

        public AwardPost(Context context, User source, User target, DateTime postDate, string awardName)
            : this(context, new DTO.Post() { ID = -1, SourceId = source.ID, TargetId = target.ID, CreatedOn = postDate, PostTypeId = 1, Title = awardName })
        {

        }
    }
}

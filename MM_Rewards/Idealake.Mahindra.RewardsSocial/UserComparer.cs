﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class UserComparer : EqualityComparer<User>
    {

        public override bool Equals(User x, User y)
        {
            return x.ID == y.ID;
        }

        public override int GetHashCode(User obj)
        {
            return obj.ID.GetHashCode();
        }
    }
}

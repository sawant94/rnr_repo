﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class Notification : BusinessObjectBase<DTO.Notification>
    {

        private IUserRepository repo;
        public User Target
        {
            get
            {
                return new User(Context, new DTO.User { ID = innerObject.TargetId });
            }

        }

        public int NotificationType
        {
            get
            {
                return PropertyGet(i => i.NotificationType);
            }

        }
        public string Message
        {
            get
            {
                return PropertyGet(i => i.Message);

            }
            set
            {
                PropertySet(value, i => i.Message, i => i.Message = value);
            }
        }

        public string DetailURL
        {
            get
            {
                return PropertyGet(i => i.DetailURL);
            }
            set
            {
                PropertySet(value, i => i.DetailURL, i => i.DetailURL = value);
            }

        }

        public bool UnRead
        {
            get
            {
                return PropertyGet(i => i.UnRead);

            }
            set
            {
                PropertySet(value, i => i.UnRead, i => i.UnRead = value);
            }
        }
        public int? RelateItemId
        {
            get
            {
                return PropertyGet(i => i.RelateItemId);

            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return PropertyGet(i => i.CreatedOn);

            }
        }

        public bool Active
        {
            get
            {
                return PropertyGet(i => i.Active);
            }
        }

        public void Action(int actionNumber)
        {
            if (NotificationType == 3 && actionNumber == 1)
            {
                ConnectionRequest request = Context.CurrentUser.GetConnectionRequestById(RelateItemId.Value);
                request.Accept();
                innerObject.Active = false;
                Update();

            }
            else if (NotificationType == 3 && actionNumber == 2)
            {
                ConnectionRequest request = Context.CurrentUser.GetConnectionRequestById(RelateItemId.Value);
                request.Reject();
                innerObject.Active = false;
                Update();
            }
        }
        protected override DTO.Notification LoadObject()
        {
            return repo.GetNotificationById(innerObject.ID);
        }

        protected override DTO.Notification SaveObject()
        {
            return repo.UpdateNotification(innerObject);
        }

        protected override DTO.Notification CreateNewObject(out int newID)
        {
            DTO.Notification result = repo.CreateNewNotification(innerObject);
            newID = result.ID;
            return result;
        }

        internal Notification(Context context, DTO.Notification dtoObject)
            : base(context)
        {
            repo = context.UserRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class HighFivePost : PostBase
    {
        public HighFivePost(Context context, DTO.Post dtoObject)
            : base(context, dtoObject)
        {

        }

        public HighFivePost(Context context, User source, User target, DateTime postDate)
            : this(context, new DTO.Post() { ID = -1, SourceId = source.ID, TargetId = target.ID, CreatedOn = postDate, PostTypeId = 2, Title = string.Empty })
        {

        }

        //public HifiMail(Context context, User hifimail)
        //    :this(context, )
        //{
        
        //}
    }
}

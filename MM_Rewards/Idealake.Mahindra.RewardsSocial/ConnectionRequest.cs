﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Idealake.Mahindra.RewardsSocial
{
    public class ConnectionRequest : BusinessObjectBase<DTO.ConnectionRequest>
    {

        private IUserRepository repo;
        public User Target
        {
            get
            {
                return new User(Context, new DTO.User { ID = innerObject.TargetId });
            }

        }

        public User Source
        {
            get
            {
                return new User(Context, new DTO.User { ID = innerObject.SourceId });
            }
        }



        public DateTime RequestedDate
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public int Status
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public bool Accept()
        {
            bool result;
            result= repo.AcceptConnectionRequest(DTO);

            if (result)
            {
                Context.CreateNotification(4, Source.ID, string.Format("{0} has accepted your friend request.", Target.FullName), null);
            }
            return result;
           
       
        }

        public bool Reject()
        {
            return repo.DeclineConnectionRequest(DTO);
        }
        
        protected override DTO.ConnectionRequest LoadObject()
        {
            throw new NotImplementedException();
        }

        protected override DTO.ConnectionRequest SaveObject()
        {
            throw new NotImplementedException();
        }

        protected override DTO.ConnectionRequest CreateNewObject(out int newID)
        {
            throw new NotImplementedException();
        }

        
        internal ConnectionRequest(Context context, DTO.ConnectionRequest dtoObject):base(context)
        {
            repo = context.UserRepository;
            innerObject = dtoObject;
            ID = dtoObject.ID;
        }




    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;


namespace PushPointsKwenchPortal
{
    class Program
    {
        KwenchEncryptionAutoReg knwench = new KwenchEncryptionAutoReg();
        string connstring = "Data Source=MMKNDFESWEB;Initial Catalog=MM_Rewards_1;User ID=mmdbuserlive;Password=mmdbuser@2012;Integrated Security=false";
        static void Main(string[] args)
        {
            Program pp = new Program();
         
            string date = DateTime.Today.ToString("dd-MMM-yyyy");
            pp.ErrorLog("Start", DateTime.Today.ToString());
            pp.RunStoredProc(date, "1");
            pp.RunStoredProcVouchers(date, "1");
     //    pp.RunStoredProc("11-Dec-2015", "1");
       //  pp.RunStoredProcVouchers("19-Nov-2015", "1");
        }

        public string getResponse(string url)
        {
            string responseString = "";
            try
            {

                Program ppp = new Program();
                ppp.ErrorLog("URL  :", url);
                //WebProxy proxyObject = new System.Net.WebProxy("http://afsrecognitionportal/", true);
                ////{
                ////    UseDefaultCredentials = false,
                ////    Credentials = new NetworkCredential("23069427","pratham,01")
                ////};
                //request.Proxy = proxyObject;
                //request.Credentials = CredentialCache.DefaultCredentials;
                //request.Method = "POST";
                //request.ContentLength = url.Length;
                //request.ContentType = "application/json";
                //request.PreAuthenticate = true;
                //request.KeepAlive = true;               

                //ServicePointManager.ServerCertificateValidationCallback +=  AcceptAllCertificatePolicy;
                //request.AllowAutoRedirect = false;
                //request.Timeout=1000000;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Proxy = new System.Net.WebProxy("10.2.152.4:80", true);
                request.Proxy.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
              
                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
              
               

                // Write the string to a file.

            }
            catch (Exception ex)
            {
                Program ppp = new Program();
                ppp.ErrorLog("Start", ex.ToString());
                // Response.Write("<script> alert('" + ex.ToString() + "');</script>");
            }
            return responseString;
        }

       
        private string Transferpoint(string ID,string employeeId, string email, string firstName, string lastName, string auditNo, int points)
        {
            string reddempage = "";
            string doc = "";
            try
            {
               // ErrorLog("Transferpoint called", employeeId);
                ErrorLog("Transferpoint Deatils", "employeeId: " + employeeId + "email :" + email + "firstName :" + firstName + "lastName :" + lastName + "auditNo :" + auditNo + "points :" + points);
                reddempage = knwench.GetTransferPointsUrl(employeeId, email, firstName, lastName, auditNo, points);
                doc = getResponse(reddempage);
                ErrorLog("Transferpoint Respoonse : ", employeeId + "Response :" + doc);
               InsertDATA(ID, employeeId, doc);

            }
            catch (Exception ex)
            {

                ErrorLog("Error Log Utility :", ex.ToString());
            }
            return doc;
        }

        private string TransferpointVouchers(string ID, string employeeId, string email, string firstName, string lastName, string auditNo, int points)
        {
            string reddempage = "";
            string doc = "";
            try
            {
                // ErrorLog("Transferpoint called", employeeId);
                ErrorLog("TransferpointVouchers", "employeeId: " + employeeId + "email :" + email + "firstName :" + firstName + "lastName :" + lastName + "auditNo :" + auditNo + "points :" + points);
                reddempage = knwench.GetTransferPointsUrl(employeeId, email, firstName, lastName, auditNo, points);
                doc = getResponse(reddempage);
                ErrorLog("TransferpointVouchers Respoonse : ", employeeId + "Response :" + doc);
                InsertDATAVouchers(ID, employeeId, doc);

            }
            catch (Exception ex)
            {

                ErrorLog("Error Log Utility TransferpointVouchers :", ex.ToString());
            }
            return doc;
        }
        public void ErrorLog(string Message, string methodName)
        {
            StreamWriter sw = null;

            try
            {
                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                string sPathName = @"C:\\MM_Rewards\\ReddempedLog\\";

                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string sErrorTime = sDay + "-" + sMonth + "-" + sYear;

                if (!Directory.Exists(sPathName))
                    Directory.CreateDirectory(sPathName);

                sw = new StreamWriter(sPathName + "ErrorLog_" + sErrorTime + ".txt", true);

                sw.WriteLine(sLogFormat + Message + "," + methodName);
                sw.Flush();

            }
            catch (Exception ex)
            {
                ErrorLog("Error Log Utility :" , ex.ToString());

            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }


        }

        public void InsertDATA(string ID, string EMPID, string Status)
        {
            using (SqlConnection con = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Insert_webservice_kindFailedRequest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@ID", SqlDbType.VarChar).Value = ID;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status;
                    cmd.Parameters.Add("@EMPID", SqlDbType.VarChar).Value = EMPID;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RunStoredProc(string Dates, string status)
        {

            //Create the connection object
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("usp_Get_webservice_kindFailedRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Date", Dates);
                    command.Parameters.AddWithValue("@Status", status);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string employeeId;
                        string Email;
                        string FirstName;
                        string lastName;
                        string auditNo;
                        int points;
                        string ID;
                        //read the data
                        while (reader.Read())
                        {
                            employeeId = reader["Rec_EmpId"].ToString();
                            Email = reader["Rec_Email"].ToString();
                            string fullName = reader["Rec_Name"].ToString();
                            var names = fullName.Split(' ');
                            FirstName = names[0];
                            lastName = names[1];
                            auditNo = reader["AwardID"].ToString();
                            ID = reader["ID"].ToString();
                            points = Convert.ToInt32(reader["Gift_Amount"].ToString());
                            if (reader["Request_Status"].ToString() == "")
                            {
                                Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                                //Console.WriteLine("Hi");
                                //    Console.ReadKey();

                                // Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                            }
                        }
                    }
                }
            }
        }

        //public void InsertDATAKIND(string Request_Id, string Request_Date, string Nom_EmpId, string Nom_Name, string Rec_EmpId, string Rec_Name, string Nom_Email, string Rec_Email, string Gift_Amount, string Request_Status, string Award_BusinessUnit, string Award_Division, string Award_Location, string AwardID)
        //{
        //    using (SqlConnection con = new SqlConnection(connstring))
        //    {
        //        using (SqlCommand cmd = new SqlCommand("usp_Insert_Webservice_Kind_Failed", con))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.Parameters.Add("@Request_Id", SqlDbType.VarChar).Value = Request_Id;
        //            cmd.Parameters.Add("@Request_Date", SqlDbType.VarChar).Value = Request_Date;
        //            cmd.Parameters.Add("@Nom_EmpId", SqlDbType.VarChar).Value = Nom_EmpId;
        //            cmd.Parameters.Add("@Nom_Name", SqlDbType.VarChar).Value = Nom_Name;
        //            cmd.Parameters.Add("@Rec_EmpId", SqlDbType.VarChar).Value = Rec_EmpId;
        //            cmd.Parameters.Add("@Rec_Name", SqlDbType.VarChar).Value = Rec_Name;
        //            cmd.Parameters.Add("@Nom_Email", SqlDbType.VarChar).Value = Nom_Email;
        //            cmd.Parameters.Add("@Rec_Email", SqlDbType.VarChar).Value = Rec_Email;
        //            cmd.Parameters.Add("@Gift_Amount", SqlDbType.VarChar).Value = Gift_Amount;
        //            cmd.Parameters.Add("@Request_Status", SqlDbType.VarChar).Value = Request_Status;
        //            cmd.Parameters.Add("@Award_BusinessUnit", SqlDbType.VarChar).Value = Award_BusinessUnit;
        //            cmd.Parameters.Add("@Award_Division", SqlDbType.VarChar).Value = Award_Division;
        //            cmd.Parameters.Add("@Award_Location", SqlDbType.VarChar).Value = Award_Location;
        //            cmd.Parameters.Add("@AwardID", SqlDbType.VarChar).Value = AwardID;
        //            con.Open();
        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //}
      

        //public void RunforNotInsertInKindTable(string Dates, string status)
        //{

        //    //Create the connection object
        //    using (SqlConnection conn = new SqlConnection(connstring))
        //    {
        //        // Open the SqlConnection.
        //        conn.Open();
        //        //Create the SQLCommand object
        //        using (SqlCommand command = new SqlCommand("usp_Get_RecipientAwards_kindFailedRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
        //        {
        //            //Pass the parameter values here
        //            command.Parameters.AddWithValue("@Date", Dates);
        //            command.Parameters.AddWithValue("@Status", status);
        //            using (SqlDataReader reader = command.ExecuteReader())
        //            {

        //                string Request_Id;
        //                string Request_Date;
        //                string Nom_EmpId;
        //                string Nom_Name;
        //                string Rec_EmpId;
        //                string Rec_Name;
        //                string Nom_Email;
        //                string Rec_Email;
        //                string Gift_Amount;
        //                string Request_Status;
        //                string Award_BusinessUnit;
        //                string Award_Division;
        //                string Award_Location;
        //                string AwardID;
        //                  string hdnAwardID;
        //                string ID;

        //                //read the data
        //                while (reader.Read())
        //                {


        //                    Request_Id=reader["ID"].ToString();                            
        //                    Nom_EmpId=reader["GiverTokenNumber"].ToString();
        //                     Rec_EmpId=reader["RecipientTokenNumber"].ToString();
        //                       Gift_Amount=reader["AwardAmount"].ToString();
        //                        Award_BusinessUnit;
        //                    Award_Division;
        //                    Award_Location;
        //                    AwardID = reader["RecipientTokenNumber"].ToString();

        //                  =AwardType



        //                    employeeId = 
        //                    Email = reader["Rec_Email"].ToString();
        //                    string fullName = reader["Rec_Name"].ToString();
        //                    var names = fullName.Split(' ');
        //                    FirstName = names[0];
        //                    lastName = names[1];
        //                    auditNo = reader["AwardID"].ToString();
        //                    ID = reader["ID"].ToString();
        //                    points = Convert.ToInt32(reader["Gift_Amount"].ToString());
        //                    MileStone

        //                    if (hdnAwardID.ToUpper().Contains("SPOT CASH"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "SP" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("EXCELLERATOR"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "EX" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("MILESTONE"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "M" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("SILVERSTARS"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "SL" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("SPOT VOUCHERS"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "GV" + Request_Id.ToString();

        //                    }
        //                    else
        //                    {
        //                         ID=  Request_Id.ToString();
                            
        //                    }
                       

        //                    if (reader["Request_Status"].ToString() == "")
        //                    {
        //                       InsertDATAKIND(ID, DateTime.Today.ToString("dd-MMM-yyyy"), Nom_EmpId, string Nom_Name, Rec_EmpId, string Rec_Name, string Nom_Email, string Rec_Email, string Gift_Amount, string Request_Status, string Award_BusinessUnit, string Award_Division, string Award_Location, string AwardID)
        //                        //Console.WriteLine("Hi");
        //                        //    Console.ReadKey();

        //                        // Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}


        public void InsertDATAVouchers(string ID, string EMPID, string Status)
        {
            using (SqlConnection con = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Insert_webservice_VouchersFailedRequest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@ID", SqlDbType.VarChar).Value = ID;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status;
                    cmd.Parameters.Add("@EMPID", SqlDbType.VarChar).Value = EMPID;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RunStoredProcVouchers(string Dates, string status)
        {

            //Create the connection object
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("usp_Get_webservice_VouchersFailedRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Date", Dates);
                    command.Parameters.AddWithValue("@Status", status);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string employeeId;
                        string Email;
                        string FirstName;
                        string lastName;
                        string auditNo;
                        int points;
                        string ID;
                        //read the data
                        while (reader.Read())
                        {
                            employeeId = reader["Rec_EmpId"].ToString();
                            Email = reader["Rec_Email"].ToString();
                            string fullName = reader["Rec_Name"].ToString();
                            var names = fullName.Split(' ');
                            FirstName = names[0];
                            lastName = names[1];
                            auditNo = reader["AwardID"].ToString();
                            ID = reader["ID"].ToString();
                            points = Convert.ToInt32(reader["Gift_Amount"].ToString());
                            if (reader["Request_Status"].ToString() == "")
                            {
                                TransferpointVouchers(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                         }
                        }
                    }
                }
            }
        }


        public static bool AcceptAllCertificatePolicy(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}


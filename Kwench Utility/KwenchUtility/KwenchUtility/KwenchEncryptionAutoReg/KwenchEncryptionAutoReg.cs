﻿/*
 * This file is used for generating the redirection URL for allowing automatic login to myPerks/Kwench application
 *
 * PROCESS
 * =======
 * 1. A String in JSON Format is created consisting of the following:
 *   a. companyId (fixed)
 *   b. A Kwench provided random string (stored in const cc)
 *   c. Email of the user
 *   d. Number of seconds elapsed since 2010 (to time restrict the validity of the resultant URL)
 *   e. First Name of the user
 *   f. Last Name of the user
 *   g. Employee Id of the user
 *   h. Audit No for transferring points
 *   i. No. of points transferred for the user
 *
 * 2. The JSON string is encrypted with 2048 bit RSA public Key provided by Kwench
 * 3. The encrypted array is converted to Base64 Encoding
 * 4. The Base64 string is UrlEncoded for transmission as a URL (configurable - make urlEncoding as false if not needed, Default is true)
 *      (Optional - If this is done by the Web Framework, don't do it otherwise it will encode the URL twice)
 *
 * USAGE
 * =====
 * 1. Place the kwench_public_key.der file on the server
 * 2. Provide the path in the publicKey variable
 * 3. Invoke the public methods in this class as per the instructions given there
 *
 * NOTE
 * ====
 * 1. Requires (System.Web) and (System.Security)
 * 2. The validation will only be done for the email domains provided by the company
 *
 * @Copyright Kwench
 * @Version 2.1
 *
 */
using System;
using System.Security.Cryptography;

using System.IO;
using System.Text;

using System.Web;




public class KwenchEncryptionAutoReg
{
   
    //TODO If this is set to true the resultant URL is encoded for browser, otherwise the only Base64 Encoded URL is returned
    private const bool urlEncoding = true;

    //TODO Provide the correct path of the RSA Public key file where it is stored on the server
    private const string publicKey = "C:/KwenchEncryptionAutoReg/kwench_public_key.der";      // Public Key for RSA Encryption - 2048 bits
 //   private const string publicKey = "D:/Anant Ghode/RNR/Kwench Utility/KwenchUtility/KwenchUtility/KwenchEncryptionAutoReg/kwench_public_key.der";

    // TODO Change Company Specfic code
    private const string cc = "pqaNO8Nkq95gJjGg";
    private const string domain = "mnm";
    private const string companyId = "830";
    private const string appPathMyPerks = ".myperks.in/myperks/sso?t=";
    private const string appPathMyPerksTransfer = ".myperks.in/myperks/sso?transferPoints&t=";

    /* Main Method to test the invocation of method */
    //public static void Main()
    //{
    //    Console.WriteLine(GetSignOnUrl("demo", "demo@mahindrarise.com", "Demo", "User"));
    //    Console.WriteLine(GetTransferPointsUrl("demo", "demo@mahindrarise.com", "Demo", "User", "1", 1));
    //}

    /* Generates a URL with timebound token allowing the user to access myPerks
     Redirect the user to this URL
    */
    public  string GetSignOnUrl(string employeeId, string email, string firstName, string lastName)
    {
        return GetReturnUrl(domain, companyId, appPathMyPerks, email, employeeId, firstName, lastName, "", 0);
    }

    /*
     * Call this URL from server directly to transfer the points to the user/register
     */
    public  string GetTransferPointsUrl(string employeeId, string email, string firstName, string lastName, string auditNo, int points)
    {
        return GetReturnUrl(domain, companyId, appPathMyPerksTransfer, email, employeeId, firstName, lastName, auditNo, points);
    }

    //----- Method returns the Url to which a User should be redirected
    private  string GetReturnUrl(string domainName, string companyId, string appPath, string email, string employeeId,
          string firstName, string lastName, string auditNo, int points)
    {
        // Create the Url
        string url = "https://";    // The authentication happens over secure HTTP
        url += domainName;
        url += appPath;

        // Add the company Id
        string tokenBuffer = "{";
        tokenBuffer += getJsonPair("cId", companyId);
        tokenBuffer += getJsonPair("cc", cc);
        tokenBuffer += getJsonPair("email", email);
        tokenBuffer += getJsonPair("eId", employeeId);
        tokenBuffer += getJsonPair("fn", firstName);
        tokenBuffer += getJsonPair("ln", lastName);

        if (auditNo != null)
        {
            tokenBuffer += getJsonPair("an", auditNo);
        }

        if (points > 0)
        {
            tokenBuffer += "\"pts\":\"" + points.ToString() + "\","; // Adds Points to the user
        }
        tokenBuffer += "\"sec\":\"" + GetSecondsSince2010().ToString() + "\"}"; // Adds Second elapsed since 2010
        //Console.WriteLine("PlainText to encrypt is: <" + tokenBuffer + ">");

        // Encrypt and encode the token
        string encryptedText = GetPublicKey(publicKey, tokenBuffer);

        // Append encrypted token to Url Buffer
        url += encryptedText;
      
        return url;
       
    }

    //--- Reads the public key provided by Kwench ---
    private  string GetPublicKey(string fileName, string stringToEncrypt)
    {
        RSACryptoServiceProvider rsa = DecodeX509PublicKey(fileName);
        byte[] plainTextBytes = Encoding.ASCII.GetBytes(stringToEncrypt);
        byte[] encryptedBytes = rsa.Encrypt(plainTextBytes, false);

        string encodedString = Convert.ToBase64String(encryptedBytes); // Base64 Encoding

        if (urlEncoding)
        {
            encodedString = HttpUtility.UrlEncode(encodedString);    // URL Encoding
        }
        return encodedString;
    }

    //------- Parses binary asn.1 X509 SubjectPublicKeyInfo; returns RSACryptoServiceProvider ---
    private  RSACryptoServiceProvider DecodeX509PublicKey(String fileName)
    {
        byte[] x509key = File.ReadAllBytes(fileName);

        // encoded OID sequence for  PKCS #1 rsaEncryption szOID_RSA_RSA = "1.2.840.113549.1.1.1"
        byte[] SeqOID = { 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00 };
        byte[] seq = new byte[15];

        // ---------  Set up stream to read the asn.1 encoded SubjectPublicKeyInfo blob  ------
        MemoryStream mem = new MemoryStream(x509key);
        BinaryReader binr = new BinaryReader(mem);    //wrap Memory Stream with BinaryReader for easy reading
        byte bt = 0;
        ushort twobytes = 0;

        try
        {
            twobytes = binr.ReadUInt16();
            if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                binr.ReadByte(); //advance 1 byte
            else if (twobytes == 0x8230)
                binr.ReadInt16();  //advance 2 bytes
            else
                return null;

            seq = binr.ReadBytes(15);    //read the Sequence OID
            if (!CompareBytearrays(seq, SeqOID)) //make sure Sequence for OID is correct
                return null;

            twobytes = binr.ReadUInt16();
            if (twobytes == 0x8103) //data read as little endian order (actual data order for Bit String is 03 81)
                binr.ReadByte(); //advance 1 byte
            else if (twobytes == 0x8203)
                binr.ReadInt16();  //advance 2 bytes
            else
                return null;

            bt = binr.ReadByte();
            if (bt != 0x00)   //expect null byte next
                return null;

            twobytes = binr.ReadUInt16();
            if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                binr.ReadByte(); //advance 1 byte
            else if (twobytes == 0x8230)
                binr.ReadInt16();  //advance 2 bytes
            else
                return null;

            twobytes = binr.ReadUInt16();
            byte lowbyte = 0x00;
            byte highbyte = 0x00;

            if (twobytes == 0x8102) //data read as little endian order (actual data order for Integer is 02 81)
                lowbyte = binr.ReadByte(); // read next bytes which is bytes in modulus
            else if (twobytes == 0x8202)
            {
                highbyte = binr.ReadByte();  //advance 2 bytes
                lowbyte = binr.ReadByte();
            }
            else
                return null;
            byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };   //reverse byte order since asn.1 key uses big endian order
            int modsize = BitConverter.ToInt32(modint, 0);

            byte firstbyte = binr.ReadByte();
            binr.BaseStream.Seek(-1, SeekOrigin.Current);

            if (firstbyte == 0x00)
            {  //if first byte (highest order) of modulus is zero, don't include it
                binr.ReadByte(); //skip this null byte
                modsize -= 1; //reduce modulus buffer size by 1
            }
            byte[] modulus = binr.ReadBytes(modsize);  //read the modulus bytes

            if (binr.ReadByte() != 0x02)       //expect an Integer for the exponent data
                return null;
            int expbytes = (int)binr.ReadByte();   // should only need one byte for actual exponent data (for all useful values)
            byte[] exponent = binr.ReadBytes(expbytes);

            // ------- create RSACryptoServiceProvider instance and initialize with public key -----
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters RSAKeyInfo = new RSAParameters();
            RSAKeyInfo.Modulus = modulus;
            RSAKeyInfo.Exponent = exponent;
            RSA.ImportParameters(RSAKeyInfo);
            return RSA;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            throw e;
        }
        finally
        {
            binr.Close();
        }
    }

    // -- Helper method to Compare Byte Arrays
    private  bool CompareBytearrays(byte[] a, byte[] b)
    {
        if (a.Length != b.Length)
            return false;
        int i = 0;
        foreach (byte c in a)
        {
            if (c != b[i])
                return false;
            i++;
        }
        return true;
    }

    // -- Returns the number of seconds elapsed since 2010
    private  long GetSecondsSince2010()
    {
        DateTime dt2010 = DateTime.Parse("01/01/2010 00:00:00 AM");
        long lTotalSecondsElapsed = 0;
        DateTime dtCurrent = DateTime.Now;
        TimeSpan tsdiff = dtCurrent.Subtract(dt2010);
        lTotalSecondsElapsed = lTotalSecondsElapsed + tsdiff.Days * (24 * 60 * 60);
        lTotalSecondsElapsed = lTotalSecondsElapsed + (tsdiff.Hours * 60 * 60);
        lTotalSecondsElapsed = lTotalSecondsElapsed + (tsdiff.Minutes * 60);
        lTotalSecondsElapsed = lTotalSecondsElapsed + tsdiff.Seconds;

        return lTotalSecondsElapsed;
    }

    private  string getJsonPair(string key, string value)
    {
        if (!string.IsNullOrEmpty(value))
        {
            return "\"" + key + "\":\"" + value + "\",";
        }
        return "";
    }
}

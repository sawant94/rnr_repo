﻿using System;
using System.IO;
using System.Globalization;
using System.Net;
using System.Net.Security;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlClient;
using System.Data;

namespace KwenchUtility
{
    class Program
    {
         KwenchEncryptionAutoReg knwench = new KwenchEncryptionAutoReg();
        string connstring = "Data Source=MMKNDFESWEB;Initial Catalog=MM_Rewards_1;User ID=mmdbuserlive;Password=mmdbuser@2012;Integrated Security=false";
        //string connstring = "Data Source=MVML-SPDEV2;Initial Catalog=MM_Rewards22JAN;User ID=sa;Password=sa;Integrated Security=false";
        static void Main(string[] args)
        {
            Program pp = new Program();
         
            string date = DateTime.Today.ToString("dd-MMM-yyyy");
            pp.ErrorLog("Start", DateTime.Today.ToString());
            try
            {

                //Reword redeeded but not trasfer to webservice_kind & webservice_Vouchers
                //Changed by Archana On 23-01-2016 return Remeeded Records not entered in webservice_kind
                string SPdate = DateTime.Today.ToString("yyyy-MM-dd");
                  pp.RunStoredProcKind(SPdate, "1");
                 pp.RunStoredProcVoucher(SPdate, "1");

            //  pp.RunStoredProcKind("2019-06-19", "1");
            // pp.RunStoredProcVoucher("2019-08-19", "1");       

               
            }
            catch (Exception ex)
            {

                pp.ErrorLog("ExecError", ex.ToString());
            }
          
            try
            {
                //Run Exec : getting Response for webservice_kind
                 pp.RunStoredProc(date, "1");
              // pp.RunStoredProc("19-Jun-2019", "1");
              
            }
            catch (Exception ex) 
            {
                pp.ErrorLog("SpotError", ex.ToString());
            }


            try
            {
                //Run Exec : getting Response for webservice_Vouchers
                   pp.RunStoredProcVouchers(date, "1");
              //  pp.RunStoredProcVouchers("19-Aug-2019", "1");
              
            }
            catch (Exception ex)
            {
                pp.ErrorLog("VouchersError", ex.ToString());
            }  
        }

                                                                                                           

        public string getResponse(string url)
        {
            string responseString = "";
            try
            {
              
                Program ppp = new Program();
                ppp.ErrorLog("URL  :", url);
              
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
             
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;            
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);              
                request.Proxy = new System.Net.WebProxy("10.2.152.4:80", true);            
                request.Proxy.Credentials = CredentialCache.DefaultCredentials;
            
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
             
                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            
            }
            catch (Exception ex)
            {
                Program ppp = new Program();
                ppp.ErrorLog("Start", ex.ToString());
                // Response.Write("<script> alert('" + ex.ToString() + "');</script>");
            }
            return responseString;
        }

       
        private string Transferpoint(string ID,string employeeId, string email, string firstName, string lastName, string auditNo, int points)
        {
            string reddempage = "";
            string doc = "";
            try
            {
               // ErrorLog("Transferpoint called", employeeId);
                ErrorLog("Transferpoint Deatils ", "employeeId: " + employeeId + "email :" + email + "firstName :" + firstName + "lastName :" + lastName + "auditNo :" + auditNo + "points :" + points);
                reddempage = knwench.GetTransferPointsUrl(employeeId, email, firstName, lastName, auditNo, points);
                doc = getResponse(reddempage);
                ErrorLog("Transferpoint Respoonse  : ", employeeId + "Response :" + doc);
               InsertDATA(ID, employeeId, doc);
            }
            catch (Exception ex)
            {

                ErrorLog("Error Log Utility :", ex.ToString());
            }
            return doc;
        }

        private string TransferpointVouchers(string ID, string employeeId, string email, string firstName, string lastName, string auditNo, int points)
        {
            string reddempage = "";
            string doc = "";
            try
            {
                // ErrorLog("Transferpoint called", employeeId);
                ErrorLog("TransferpointVouchers", "employeeId: " + employeeId + "email :" + email + "firstName :" + firstName + "lastName :" + lastName + "auditNo :" + auditNo + "points :" + points);
                reddempage = knwench.GetTransferPointsUrl(employeeId, email, firstName, lastName, auditNo, points);
                doc = getResponse(reddempage);
                ErrorLog("TransferpointVouchers Respoonse : ", employeeId + "Response :" + doc);
                InsertDATAVouchers(ID, employeeId, doc);
            }
            catch (Exception ex)
            {
                ErrorLog("Error Log Utility TransferpointVouchers :", ex.ToString());
            }
            return doc;
        }
        public void ErrorLog(string Message, string methodName)
        {
            StreamWriter sw = null;

            try
            {
                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                string sPathName = @"C:\\MM_Rewards\\ReddempedLog\\";

                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string sErrorTime = sDay + "-" + sMonth + "-" + sYear;

                if (!Directory.Exists(sPathName))
                    Directory.CreateDirectory(sPathName);

                sw = new StreamWriter(sPathName + "ErrorLog_" + sErrorTime + ".txt", true);

                sw.WriteLine(sLogFormat + Message + "," + methodName);
                sw.Flush();

            }
            catch (Exception ex)
            {
                ErrorLog("Error Log Utility :" , ex.ToString());

            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }


        }

        public void InsertDATA(string ID, string EMPID, string Status)
        {
            using (SqlConnection con = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Insert_webservice_kindFailedRequest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@ID", SqlDbType.VarChar).Value = ID;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status;
                    cmd.Parameters.Add("@EMPID", SqlDbType.VarChar).Value = EMPID;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RunStoredProcKind(string Date, string status)
        {
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("usp_Get_RecptAwards_kindPendingRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Date", Date);
                    command.Parameters.AddWithValue("@Status", status);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string Request_Id;
                        string Request_Date;
                        string Nom_EmpId;
                        string Nom_Name;
                        string Rec_EmpId;
                        string Rec_Name;
                        string Nom_Email;
                        string Rec_Email;
                        string Gift_Amount;
                        string Request_Status;
                        string Award_BusinessUnit;
                        string Award_Division;
                        string Award_Location;
                        string AwardID;
                        string hdnAwardID;
                        string ID;
                        string AwardType;
                        //read the data
                       while (reader.Read())
                        {

                            Request_Id=reader["ID"].ToString();                            
                            Nom_EmpId=reader["GiverTokenNumber"].ToString();
                             Rec_EmpId=reader["RecipientTokenNumber"].ToString();
                             Gift_Amount=reader["AwardAmount"].ToString();
                             Award_BusinessUnit=reader["Award_BusinessUnit"].ToString();
                            Award_Division=reader["Award_Division"].ToString();
                            Award_Location=reader["Award_Location"].ToString();
                            AwardID = reader["ID"].ToString();
                            Rec_Name = reader["Rec_Name"].ToString();
                            Nom_Email = reader["Nom_Email"].ToString();
                            Rec_Email = reader["Rec_Email"].ToString();
                          AwardType =reader["AwardType"].ToString();
                          //Request_Status = reader["Status"].ToString();
                          Request_Status = "";
                          hdnAwardID = reader["AwardType"].ToString();
                          Request_Date = DateTime.Today.ToString("dd-MMM-yyyy");
                            //employeeId = 
                            //Email = reader["Rec_Email"].ToString();
                           string fullName = reader["Rec_Name"].ToString();
                          var names = fullName.Split(' ');
                          Nom_Name = names[0] + " " + names[1];
                          
                            //auditNo = reader["AwardID"].ToString();
                            ID = reader["ID"].ToString();
                            //points = Convert.ToInt32(reader["Gift_Amount"].ToString());
                            //MileStone

                            if (hdnAwardID.ToUpper().Contains("SPOT CASH"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
                            {
                              ID=  "SP" + Request_Id.ToString();

                            }
                            else  if (hdnAwardID.ToUpper().Contains("EXCELLERATOR"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
                            {
                              ID=  "EX" + Request_Id.ToString();

                            }
                            else  if (hdnAwardID.ToUpper().Contains("MILESTONE"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
                            {
                              ID=  "M" + Request_Id.ToString();

                            }
                            else  if (hdnAwardID.ToUpper().Contains("SILVERSTARS"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
                            {
                              ID=  "SL" + Request_Id.ToString();

                            }
                            else  if (hdnAwardID.ToUpper().Contains("SPOT VOUCHERS"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
                            {
                              ID=  "GV" + Request_Id.ToString();

                            }
                            else
                            {
                                 ID=  Request_Id.ToString();
                            
                            }
                       

                            //if (reader["Request_Status"].ToString() == "")
                           if(Request_Status =="")
                            {
                                InsertDATAKIND(ID, DateTime.Today.ToString("dd-MMM-yyyy"), Nom_EmpId, Nom_Name, Rec_EmpId, Rec_Name, Nom_Email, Rec_Email, Gift_Amount, Request_Status, Award_BusinessUnit, Award_Division, Award_Location, AwardID);
                                ErrorLog("RunStoredProcKind : ", AwardID);
                               // Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                                //Console.WriteLine("Hi");
                                //    Console.ReadKey();

                                // Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                            }
                        }
                    }
                }
            }
        }

        public void RunStoredProcVoucher(string Date, string status)
        {
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("usp_Get_RecptAwards_VoucherPendingRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Date", Date);
                    command.Parameters.AddWithValue("@Status", status);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string Request_Id;
                        string Request_Date;
                        string Nom_EmpId;
                        string Nom_Name;
                        string Rec_EmpId;
                        string Rec_Name;
                        string Nom_Email;
                        string Rec_Email;
                        string Gift_Amount;
                        string Request_Status;
                        string Award_BusinessUnit;
                        string Award_Division;
                        string Award_Location;
                        string AwardID;
                        string hdnAwardID;
                        string ID;
                        string AwardType;
                        //read the data
                        while (reader.Read())
                        {

                            Request_Id = reader["ID"].ToString();
                            Nom_EmpId = reader["GiverTokenNumber"].ToString();
                            Rec_EmpId = reader["RecipientTokenNumber"].ToString();
                            Gift_Amount = reader["AwardAmount"].ToString();
                            Award_BusinessUnit = reader["Award_BusinessUnit"].ToString();
                            Award_Division = reader["Award_Division"].ToString();
                            Award_Location = reader["Award_Location"].ToString();
                            AwardID = reader["ID"].ToString();
                            Rec_Name = reader["Rec_Name"].ToString();
                            Nom_Email = reader["Nom_Email"].ToString();
                            Rec_Email = reader["Rec_Email"].ToString();
                            AwardType = reader["AwardType"].ToString();
                            //Request_Status = reader["Status"].ToString();
                            Request_Status = "";
                            hdnAwardID = reader["AwardType"].ToString();
                            Request_Date = DateTime.Today.ToString("dd-MMM-yyyy");
                            //employeeId = 
                            //Email = reader["Rec_Email"].ToString();
                            string fullName = reader["Rec_Name"].ToString();
                            var names = fullName.Split(' ');
                            Nom_Name = names[0] + " " + names[1];

                            //auditNo = reader["AwardID"].ToString();
                            ID = reader["ID"].ToString();
                            //points = Convert.ToInt32(reader["Gift_Amount"].ToString());
                            //MileStone

                            ID = "GV" + Request_Id.ToString();


                            //if (reader["Request_Status"].ToString() == "")
                            if (Request_Status == "")
                            {
                                InsertDATAVOUCHER(ID, DateTime.Today.ToString("dd-MMM-yyyy"), Nom_EmpId, Nom_Name, Rec_EmpId, Rec_Name, Nom_Email, Rec_Email, Gift_Amount, Request_Status, Award_BusinessUnit, Award_Division, Award_Location, AwardID);
                                ErrorLog("RunStoredProcVoucher : ", AwardID);
                                // Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                                //Console.WriteLine("Hi");
                                //    Console.ReadKey();

                                // Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                            }
                        }
                    }
                }
            }
        }

        public void RunStoredProc(string Dates, string status)
        {

            //Create the connection object
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("usp_Get_webservice_kindFailedRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Date", Dates);
                    command.Parameters.AddWithValue("@Status", status);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string employeeId;
                        string Email;
                        string FirstName;
                        string lastName;
                        string auditNo;
                        int points;
                        string ID;
                        //read the data
                        while (reader.Read())
                        {
                            employeeId = null;
                            Email = null;
                            auditNo = null;
                            ID = null;
                            points = 0;

                            employeeId = reader["Rec_EmpId"].ToString();
                            Email = reader["Rec_Email"].ToString();
                            string fullName = reader["Rec_Name"].ToString();
                            var names = fullName.Split(' ');
                            FirstName = names[0];
                            lastName = names[1];
                            auditNo = reader["AwardID"].ToString();
                            ID = reader["ID"].ToString();
                            points = Convert.ToInt32(reader["Gift_Amount"].ToString());
                            if (reader["Request_Status"].ToString() == "")
                            {
                               
                                Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                               
                            }
                        }
                    }
                }
            }
        }

        public void InsertDATAKIND(string Request_Id, string Request_Date, string Nom_EmpId, string Nom_Name, string Rec_EmpId, string Rec_Name, string Nom_Email, string Rec_Email, string Gift_Amount, string Request_Status, string Award_BusinessUnit, string Award_Division, string Award_Location, string AwardID)
        {
            using (SqlConnection con = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Insert_Webservice_Kind_Failed", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Request_Id", SqlDbType.VarChar).Value = Request_Id;
                    cmd.Parameters.Add("@Request_Date", SqlDbType.VarChar).Value = Request_Date;
                    cmd.Parameters.Add("@Nom_EmpId", SqlDbType.VarChar).Value = Nom_EmpId;
                    cmd.Parameters.Add("@Nom_Name", SqlDbType.VarChar).Value = Nom_Name;
                    cmd.Parameters.Add("@Rec_EmpId", SqlDbType.VarChar).Value = Rec_EmpId;
                    cmd.Parameters.Add("@Rec_Name", SqlDbType.VarChar).Value = Rec_Name;
                    cmd.Parameters.Add("@Nom_Email", SqlDbType.VarChar).Value = Nom_Email;
                    cmd.Parameters.Add("@Rec_Email", SqlDbType.VarChar).Value = Rec_Email;
                    cmd.Parameters.Add("@Gift_Amount", SqlDbType.VarChar).Value = Gift_Amount;
                    cmd.Parameters.Add("@Request_Status", SqlDbType.VarChar).Value = Request_Status;
                    cmd.Parameters.Add("@Award_BusinessUnit", SqlDbType.VarChar).Value = Award_BusinessUnit;
                    cmd.Parameters.Add("@Award_Division", SqlDbType.VarChar).Value = Award_Division;
                    cmd.Parameters.Add("@Award_Location", SqlDbType.VarChar).Value = Award_Location;
                    cmd.Parameters.Add("@Award_Id", SqlDbType.VarChar).Value = AwardID;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    ErrorLog("InsertDATAKIND : ", AwardID);
                }
            }
        }

        public void InsertDATAVOUCHER(string Request_Id, string Request_Date, string Nom_EmpId, string Nom_Name, string Rec_EmpId, string Rec_Name, string Nom_Email, string Rec_Email, string Gift_Amount, string Request_Status, string Award_BusinessUnit, string Award_Division, string Award_Location, string AwardID)
        {
            using (SqlConnection con = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Insert_Webservice_Voucher_Failed", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Request_Id", SqlDbType.VarChar).Value = Request_Id;
                    cmd.Parameters.Add("@Request_Date", SqlDbType.VarChar).Value = Request_Date;
                    cmd.Parameters.Add("@Nom_EmpId", SqlDbType.VarChar).Value = Nom_EmpId;
                    cmd.Parameters.Add("@Nom_Name", SqlDbType.VarChar).Value = Nom_Name;
                    cmd.Parameters.Add("@Rec_EmpId", SqlDbType.VarChar).Value = Rec_EmpId;
                    cmd.Parameters.Add("@Rec_Name", SqlDbType.VarChar).Value = Rec_Name;
                    cmd.Parameters.Add("@Nom_Email", SqlDbType.VarChar).Value = Nom_Email;
                    cmd.Parameters.Add("@Rec_Email", SqlDbType.VarChar).Value = Rec_Email;
                    cmd.Parameters.Add("@Gift_Amount", SqlDbType.VarChar).Value = Gift_Amount;
                    cmd.Parameters.Add("@Request_Status", SqlDbType.VarChar).Value = Request_Status;
                    cmd.Parameters.Add("@Award_BusinessUnit", SqlDbType.VarChar).Value = Award_BusinessUnit;
                    cmd.Parameters.Add("@Award_Division", SqlDbType.VarChar).Value = Award_Division;
                    cmd.Parameters.Add("@Award_Location", SqlDbType.VarChar).Value = Award_Location;
                    cmd.Parameters.Add("@Award_Id", SqlDbType.VarChar).Value = AwardID;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    ErrorLog("InsertDATAVOUCHER : ", AwardID);
                }
            }
        }

        //public void RunforNotInsertInKindTable(string Dates, string status)
        //{

        //    //Create the connection object
        //    using (SqlConnection conn = new SqlConnection(connstring))
        //    {
        //        // Open the SqlConnection.
        //        conn.Open();
        //        //Create the SQLCommand object
        //        using (SqlCommand command = new SqlCommand("usp_Get_RecipientAwards_kindFailedRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
        //        {
        //            //Pass the parameter values here
        //            command.Parameters.AddWithValue("@Date", Dates);
        //            command.Parameters.AddWithValue("@Status", status);
        //            using (SqlDataReader reader = command.ExecuteReader())
        //            {

        //                string Request_Id;
        //                string Request_Date;
        //                string Nom_EmpId;
        //                string Nom_Name;
        //                string Rec_EmpId;
        //                string Rec_Name;
        //                string Nom_Email;
        //                string Rec_Email;
        //                string Gift_Amount;
        //                string Request_Status;
        //                string Award_BusinessUnit;
        //                string Award_Division;
        //                string Award_Location;
        //                string AwardID;
        //                  string hdnAwardID;
        //                string ID;

        //                //read the data
        //                while (reader.Read())
        //                {


        //                    Request_Id=reader["ID"].ToString();                            
        //                    Nom_EmpId=reader["GiverTokenNumber"].ToString();
        //                     Rec_EmpId=reader["RecipientTokenNumber"].ToString();
        //                       Gift_Amount=reader["AwardAmount"].ToString();
        //                        Award_BusinessUnit;
        //                    Award_Division;
        //                    Award_Location;
        //                    AwardID = reader["RecipientTokenNumber"].ToString();

        //                  =AwardType



        //                    employeeId = 
        //                    Email = reader["Rec_Email"].ToString();
        //                    string fullName = reader["Rec_Name"].ToString();
        //                    var names = fullName.Split(' ');
        //                    FirstName = names[0];
        //                    lastName = names[1];
        //                    auditNo = reader["AwardID"].ToString();
        //                    ID = reader["ID"].ToString();
        //                    points = Convert.ToInt32(reader["Gift_Amount"].ToString());
        //                    MileStone

        //                    if (hdnAwardID.ToUpper().Contains("SPOT CASH"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "SP" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("EXCELLERATOR"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "EX" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("MILESTONE"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "M" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("SILVERSTARS"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "SL" + Request_Id.ToString();

        //                    }
        //                    else  if (hdnAwardID.ToUpper().Contains("SPOT VOUCHERS"))   // || hdnAwardID.Value.ToUpper().Contains("EXCELLERATOR") || hdnAwardID.Value.ToUpper().Contains("MILESTONE"))
        //                    {
        //                      ID=  "GV" + Request_Id.ToString();

        //                    }
        //                    else
        //                    {
        //                         ID=  Request_Id.ToString();
                            
        //                    }
                       

        //                    if (reader["Request_Status"].ToString() == "")
        //                    {
        //                       InsertDATAKIND(ID, DateTime.Today.ToString("dd-MMM-yyyy"), Nom_EmpId, string Nom_Name, Rec_EmpId, string Rec_Name, string Nom_Email, string Rec_Email, string Gift_Amount, string Request_Status, string Award_BusinessUnit, string Award_Division, string Award_Location, string AwardID)
        //                        //Console.WriteLine("Hi");
        //                        //    Console.ReadKey();

        //                        // Transferpoint(ID, employeeId, Email, FirstName, lastName, auditNo, points);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}


        public void InsertDATAVouchers(string ID, string EMPID, string Status)
        {
            using (SqlConnection con = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Insert_webservice_VouchersFailedRequest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@ID", SqlDbType.VarChar).Value = ID;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status;
                    cmd.Parameters.Add("@EMPID", SqlDbType.VarChar).Value = EMPID;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RunStoredProcVouchers(string Dates, string status)
        {

            //Create the connection object
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                // Open the SqlConnection.
                conn.Open();
                //Create the SQLCommand object
                using (SqlCommand command = new SqlCommand("usp_Get_webservice_VouchersFailedRequest", conn) { CommandType = System.Data.CommandType.StoredProcedure })
                {
                    //Pass the parameter values here
                    command.Parameters.AddWithValue("@Date", Dates);
                    command.Parameters.AddWithValue("@Status", status);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string employeeId;
                        string Email;
                        string FirstName;
                        string lastName;
                        string auditNo;
                        int points;
                        string ID;
                        //read the data
                        while (reader.Read())
                        {

                            employeeId = null;
                            Email = null;
                            auditNo = null;
                            ID = null;
                            points = 0;

                            employeeId = reader["Rec_EmpId"].ToString();
                            Email = reader["Rec_Email"].ToString();
                            string fullName = reader["Rec_Name"].ToString();
                            var names = fullName.Split(' ');
                            FirstName = names[0];
                            lastName = names[1];
                            auditNo = reader["AwardID"].ToString();
                            ID = reader["ID"].ToString();
                            points = Convert.ToInt32(reader["Gift_Amount"].ToString());
                            if (reader["Request_Status"].ToString() == "")
                            {
                                ErrorLog("ErrorVTrack : ", "Step1V");
                                TransferpointVouchers(ID, employeeId, Email, FirstName, lastName, auditNo, points);
                         }
                        }
                    }
                }
            }
        }


        public static bool AcceptAllCertificatePolicy(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    
    }
}
